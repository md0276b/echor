#***************************************************************************************************************************************
#       Multiple Factor Analysis (MFA) with FactoMineR tutorial
#         Marine environement raster time series case study
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.9.3
# V1.0 08/09/2020

# 0. Introduction ------
# *************** -------
# Tutorial on Multiple Factor Analysis (MFA) 
# of marine environement raster time series with FactoMineR

# Objectives: 
# ***************
# spatio-temporal analysis of raster (gridded maps) time series
# i) defining areas with stable environmental conditions over time
# ii) assessing eventual temporal trends in environmental conditions

# References: 
# ***************
# Doray et al. (2018) https://archimer.ifremer.fr/doc/00411/52236/
# Pagès, J., 2014. Multiple Factor Analysis by Example Using R, Chapman and Hall/CRC.

# 1. Set up --------
# *********************
# 1.1. Load libraries ----------
library(EchoR)
library(FactoMineR)
library(rasterVis)
library(latticeExtra)
library(vegan)
library(maps)
library(maptools)
library(sp)
library(viridisLite)
library(ggplot2)
library(scales)
library(NbClust)
library(factoextra)
library(gridExtra)

# 1.2. Import gridmap data ----
# *********************************************************** ----
# Gridmap data processed using this methodology by
# Doray et al. (2018) # can be downloaded at: 
# SEANOE: https://doi.org/10.17882/53389

  # Set paths
  #***********************
  # path to folder with gridmap data
  ddrive='~/Documents/Data/PELGAS/'
  # path to grid files
  #***********************  
  path.grids=paste(ddrive,"gridMapsPELGAS2000-2015.csv",sep='')
  # Results path
  #************************
  path.export.hydro=paste(ddrive,'MFAresults/Envt/',sep='')
  dir.create(path.export.hydro,showWarnings=FALSE)
  
  # Import files 
  gridmaps=read.table(file=path.grids,header=T,sep=";")
  head(gridmaps)
  #Select hydrology gridmaps
  unique(gridmaps$Component)
  hydro.gridmaps=gridmaps[gridmaps$Component%in%
                            c("Hydrology","EcologicalSurveyDate"),]
  # Add cell IDs
  Idf=data.frame(X=sort(unique(hydro.gridmaps$X)))
  Idf$I=seq(dim(Idf)[1])
  Jdf=data.frame(Y=sort(unique(hydro.gridmaps$Y)))
  Jdf$J=seq(dim(Jdf)[1])
  hydro.gridmaps=merge(hydro.gridmaps,Idf,by.x="X",by.y="X")
  hydro.gridmaps=merge(hydro.gridmaps,Jdf,by.x="Y",by.y="Y")
  hydro.gridmaps$cell=paste(hydro.gridmaps$I,hydro.gridmaps$J,sep='-')
  # Adjust column names
  names(hydro.gridmaps)[names(hydro.gridmaps)=='X']="Xgd"
  names(hydro.gridmaps)[names(hydro.gridmaps)=='Y']="Ygd"
  names(hydro.gridmaps)[names(hydro.gridmaps)=='Zmean']="Zvalue"
  head(hydro.gridmaps)

# 2. Prepare datasets for MFA ------------------------
#******************************************************************** ----

  # 2.1. Select variables -------------
  #******************************
  hydro.gridmapss=hydro.gridmaps[hydro.gridmaps$Parameter%in%c("BotTemp","DefEpot","Heq","ProfHmel",
                                   "ProfPycn","SurfSal","SurfTemp"),]
  lvar=unique(hydro.gridmapss$Parameter)
  
  # 2.2. Select years ---------------
  #***************************
  hydro.gridmapss=hydro.gridmapss[hydro.gridmapss$Year%in%seq(2000,2015,1),]
  sort(unique(hydro.gridmapss$Year))
  head(hydro.gridmapss)
  
  # 2.3. Select cells in survey area --------------------
  #*******************************
  pipo.hydro=hydro.gridmapss[hydro.gridmapss$Year==2004&
                               hydro.gridmapss$Parameter=="BotTemp",]
  plot(pipo.hydro$Xgd,pipo.hydro$Ygd)
  plot(pipo.hydro$Xgd,pipo.hydro$Ygd,cex=0.1+pipo.hydro$Zvalue/100)
  data("PELGASpolygon")
  poly=PELGASpolygon
  lines(poly)
  coast()
  head(pipo.hydro)
  library(splancs) 
  pts=as.points(pipo.hydro$Xgd,pipo.hydro$Ygd)
  testInPoly=inout(pts,poly)
  plot(pts)
  points(pts[testInPoly,],pch=16)
  coast()
  cellsel.hydro=data.frame(x=pts[,1],y=pts[,2],sel=testInPoly,I=pipo.hydro$I,J=pipo.hydro$J)
  cellsel.hydro$cell=paste(cellsel.hydro$I,cellsel.hydro$J,sep='-')
  head(cellsel.hydro)
  cellsel.hydro[cellsel.hydro$y>48,"sel"]=FALSE
  cellsel.hydro[cellsel.hydro$x<(-5),"sel"]=FALSE
  cellsel.hydro[cellsel.hydro$x>(-1.2),"sel"]=FALSE
  cellsel.hydro[cellsel.hydro$cell=='8-20',"sel"]=FALSE
  plot(cellsel.hydro$x,cellsel.hydro$y)
  points(cellsel.hydro[cellsel.hydro$sel,'x'],cellsel.hydro[cellsel.hydro$sel,'y'],pch=16)
  coast()
  legend('topleft',legend=c('Cells centers','Selected cells'),pch=c(1,16))
  
  # 2.4. Check for NA values and interpolate if need be ----
  #***********************************
  cellidsel=cellsel.hydro[cellsel.hydro$sel,'cell']
  # cells with NA values in region of interest
  gfhbsna=hydro.gridmapss[hydro.gridmapss$cell%in%cellidsel&
                            is.na(hydro.gridmapss$Zvalue),]
  # cells in region of interest
  gfhbs2=hydro.gridmapss[hydro.gridmapss$cell%in%cellidsel,]
  # cells without NA values in region of interest
  gfhbsnona=hydro.gridmapss[hydro.gridmapss$cell%in%cellidsel&
                              !is.na(hydro.gridmapss$Zvalue),]

  # Interpolate missing values per year
  lyears=unique(gfhbsna$Year)
  lvarsna=unique(gfhbsna$Parameter)
  for (i in 1:length(lyears)){
    plot(cellsel.hydro$x,cellsel.hydro$y,main=lyears[i])
    #text(cellsel.hydro$x,cellsel.hydro$y,cellsel.hydro$cell)
    points(cellsel.hydro[cellsel.hydro$sel,'x'],cellsel.hydro[cellsel.hydro$sel,'y'],pch=16)
    points(gfhbsna[gfhbsna$Year==lyears[i],'Xgd'],gfhbsna[gfhbsna$Year==lyears[i],'Ygd'],pch=16,col=2)
    coast()
    lvarna=unique(gfhbsna[gfhbsna$Year==lyears[i],'Parameter'])
    for (j in 1:length(lvarna)){
      gfhbss=gfhbs2[gfhbs2$Year==lyears[i]&gfhbs2$Parameter==lvarna[j],]
      mvalpos=gfhbss[is.na(gfhbss$Zvalue),]
      head(gfhbss)
      # interpolation
      #***********************
      library(RGeostats)
      #data.db <- db.create(gfhbss[,c('Xgd','Ygd','Zvalue')],flag.grid=TRUE,x0=c(min(gfhbss$Xgd),min(gfhbss$Ygd)),
      #                     dx=c(0.25,0.25),nx=c(length(unique(gfhbss$Xgd)),length(unique(gfhbss$Ygd))))
      data.db <- db.create(gfhbss[,c('Xgd','Ygd','Zvalue')],flag.grid=FALSE,ndim=2,autoname=F)
      data.db
      # Visualizing the data set (proportional representation)
      plot(data.db,pch=21,col="black",bg="blue",title="Samples location")
      # Calculate experimental omni-directional variogram
      data.vario <- vario.calc(data.db,lag=1,nlag=20)
      data.vario
      plot(data.vario,npairpt=1,npairdw=TRUE,title="Omni-directional variogram")
      # Fitting the omni-directional variogram with an isotropic model (loaded)
      data.model <- model.auto(data.vario,
                               title = "Modelling omni-directional variogram")
      data.model
      # Creating neighborhood structures
      moving.neigh <- neigh.create(ndim = 2, type = 2, 
                                 flag.sector = FALSE, flag.aniso = FALSE, flag.rotation = FALSE, 
                                 nmini = 2, nmaxi = 60, nsect = 1, nsmax = 1, 
                                 width = 1, skip=1, rotmat=NA,radius=NA)
      # Cross-validation
      data.db <- xvalid(data.db,data.model,moving.neigh,modify.target=F)
      hist(db.extract(data.db,"Xvalid.Zvalue.esterr"),nclass=30,
           main="Histogram of Zvalue",xlab="Cross-validation error",col="blue")
      # Create a new grid
      Nx=length(unique(cellsel.hydro[cellsel.hydro$sel,'x']))
      Ny=length(unique(cellsel.hydro[cellsel.hydro$sel,'y']))
      grid.db <- db.grid.init(data.db,nodes=c(Nx,Ny),origin=c(min(gfhbss$Xgd),min(gfhbss$Ygd)))
      # Estimation with the whole data set (Moving Neighborhood)
      grid.db <- kriging(data.db,grid.db,data.model,moving.neigh,
                         radix="KM.All")
      plot(grid.db,name.image="z1",col=topo.colors(20),
           title="Estimation - All data (Moving Neighborhood)")
      # Checking the Moving Neighborhood
      grid.db <- neigh.test(data.db,grid.db,data.model,moving.neigh,
                            radix="Moving")
      plot(grid.db,name.image="z1",title="Moving Neighborhood: Number of samples",
           pos.legend=1)
      plot(data.db,pch=21,col="black",add=TRUE)
      plot(grid.db,name.image="z2",
           title="Moving Neighborhood: Largest distance data-target",pos.legend=1)
      plot(data.db,pch=21,col="black",add=TRUE)
      plot(grid.db,name.image="z3",
           title="Moving: Neighborhood: Smallest distance data-target",
           pos.legend=1)
      plot(data.db,pch=21,col="black",add=TRUE)
      plot(grid.db,name.image="KM.All.Zvalue.stdev",
           title="Moving: Neighborhood: estimation error",
           pos.legend=1)
      plot(data.db,pch=21,col="black",add=TRUE)
      uk.res=grid.db@items[,2:5]
      uk.ress=uk.res[uk.res$x1%in%mvalpos$Xgd&uk.res$x2%in%mvalpos$Ygd,]
      mvalpos=merge(mvalpos,uk.ress[,c('x1','x2','KM.All.Zvalue.estim')],by.x=c('Xgd','Ygd'),by.y=c('x1','x2'))
      mvalpos$Zvalue=mvalpos$KM.All.Zvalue.estim
      if (j==1){
        gfhbsintj=mvalpos[,names(mvalpos)!='KM.All.Zvalue.estim']
      }else{
        gfhbsintj=rbind(gfhbsintj,mvalpos[,names(mvalpos)!='KM.All.Zvalue.estim'])
      }
    }
    if (i==1){
      gfhbsintij=gfhbsintj
    }else{
      gfhbsintij=rbind(gfhbsintij,gfhbsintj)
    }
  }
  dim(gfhbsintij)
  gfhbsint=rbind(gfhbsnona,gfhbsintij)
  dim(gfhbs2);dim(gfhbsint)
  id2=paste(gfhbs2$cell,gfhbs2$Year,gfhbs2$Parameter)
  id1=paste(gfhbsint$cell,gfhbsint$Year,gfhbsint$Parameter)
  id2[!id2%in%id1]
  unique(gfhbsint$Parameter)
  
  # Export interpolated data
  #***********************
  write.table(gfhbsint,paste(path.grids,'interHydro0015.csv',sep=''),sep=';',row.names = FALSE)
  
  # 2.5. Reshape data frame for analysis with year as grouping variable --------------
  # individuals (rows) = grid cells
  # variables (columns) = parameter values recorded in grid cells
  # groups (table) = a year (survey)
  #***************************
  a.hydro <- reshape(gfhbsint[,c("I","J","Zvalue","Parameter","Year")], 
                     timevar = "Parameter", idvar = c("I","J","Year"), 
                     direction = "wide")
  head(a.hydro)
  names(a.hydro)
  a.hydro=a.hydro[order(a.hydro$Year),]
  
  big.hydro <- reshape(a.hydro, timevar = "Year", idvar = c("I","J"), 
                       direction = "wide")
  head(big.hydro)
  names(big.hydro)
  dim(big.hydro)
  
  # 2.6. Prepare and standardise dataset ------------------
  #*************************************
  big.hydro.MFA=big.hydro[,-seq(2)]
  row.names(big.hydro.MFA)=paste(big.hydro$I,big.hydro$J,sep='-')
  # simplify column names
  names(big.hydro.MFA)=gsub('Zvalue.','',names(big.hydro.MFA))
  names(big.hydro.MFA)
  head(big.hydro.MFA)

  # center-scale variables
  big.hydro.MFAs=apply(big.hydro.MFA,2,scale,center=TRUE,scale=TRUE)
  head(big.hydro.MFAs)
  class(big.hydro.MFAs)
  dimnames(big.hydro.MFAs)[[1]]=dimnames(big.hydro.MFA)[[1]]
  
  #Remove zeroes columns 
  scols=apply(big.hydro.MFAs,2,sum,na.rm=TRUE)
  srows=apply(big.hydro.MFAs,1,sum,na.rm=TRUE)
  big.hydro.MFAs2=big.hydro.MFAs[,scols!=0]
  sum(scols==0)
  
  #check that there is no NA left
  big.hydro.MFAs.na=data.frame(big.hydro.MFAs[!complete.cases(big.hydro.MFAs),])
  big.hydro.MFAs.na$cell=row.names(big.hydro.MFAs.na)
  head(big.hydro.MFAs.na)
 
  # generate no. of data in each group (year)
  cwz=dimnames(big.hydro.MFAs)[[2]]
  nct=unlist(strsplit(cwz,split='[.]'))[seq(1,2*length(cwz),2)]
  nyears=unlist(strsplit(cwz,split='[.]'))[seq(2,2*length(cwz),2)]
  tnnc=table(nct,nyears)
  tnnc.df=data.frame(table(nct,nyears))
  stnnc.hydro=apply(tnnc,2,sum)
  head(tnnc.df)
  tnnc.dfa=aggregate(tnnc.df$Freq,list(nct=tnnc.df$nct),sum)
  summary(tnnc.dfa$x)
  # define group names
  hgroup.names=c("00", "01", "02", "03", "04", "05", 
                "06", "07", "08", "09","10", "11", "12", "13","14","15")


# 3. Perform MFA ---------------
#************************************************** ----
  # 3.1. MFA -----------
  #**********************************
  dim(big.hydro.MFAs)
  res.hydro.MFA<-MFA(big.hydro.MFAs, group=stnnc.hydro, 
                     type=rep("c",length(hgroup.names)), ncp=22, 
                     name.group=hgroup.names, 
                     num.group.sup=NULL, graph=TRUE)
  names(res.hydro.MFA)
  summary(res.hydro.MFA)  

  # 3.2. Explained variance ------------------
  #*********************************  
  summary(res.hydro.MFA)  
  par(bg='white')
  barplot(res.hydro.MFA$eig[,2],main="% of variance explained",
          names.arg=1:nrow(res.hydro.MFA$eig))
  barplot(res.hydro.MFA$eig[,3],main="Cumulative % of variance explained",
          names.arg=1:nrow(res.hydro.MFA$eig))
  # MFA rank at which cumulative explained variance = 0.95
  res.hydro.MFA$eig[round(res.hydro.MFA$eig[,3])==95,]

  # 3.3. Plots of individuals in MFA1-2-3 planes ---------
  #*************************************
  # Plot of individuals in MFA1-2 plane 
  p1 = fviz_mfa_ind(res.hydro.MFA, col.ind = "contrib") #with contribution in color scale
  p2 = fviz_mfa_ind(res.hydro.MFA, col.ind = "cos2") #with quality of representation in color scale
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA2-3 plane
  p1 = fviz_mfa_ind(res.hydro.MFA, col.ind = "contrib",axes = c(2, 3))
  p2 = fviz_mfa_ind(res.hydro.MFA, col.ind = "cos2",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-3 plane
  p1 = fviz_mfa_ind(res.hydro.MFA, col.ind = "contrib",axes = c(1, 3))
  p2 = fviz_mfa_ind(res.hydro.MFA, col.ind = "cos2",axes = c(1, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-2-3 planes, with contribution in color scale
  p1 = fviz_mfa_ind(res.hydro.MFA, col.ind = "contrib")
  p2 = fviz_mfa_ind(res.hydro.MFA, col.ind = "contrib",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)

# 4. Contributions of variables to MFA axes --------------
#*********************************************************** ----

  par(bg='grey50')
  plot.MFA(res.hydro.MFA, axes=c(1, 2), choix="var", new.plot=TRUE, lab.var=TRUE, 
           habillage="group", select = "coord 10")
  par(bg='white')
  
  # 4.1. select variables with above average contribution to MFA axes ----
  #*************************************
  var.contrib=res.hydro.MFA$quanti.var$contrib
  head(var.contrib)
  summary(var.contrib[,1])
  var.contribs1=var.contrib[var.contrib[,1]>mean(var.contrib[,1]),1]
  var.contribs2=var.contrib[var.contrib[,2]>mean(var.contrib[,2]),2]
  var.contribs3=var.contrib[var.contrib[,3]>mean(var.contrib[,3]),3]
  
  # 4.2. Variables correlation with MFA axes ----------
  #******************************************
  var.cor=res.hydro.MFA$quanti.var$contrib
  graphics.off()
  ddres1=dimdesc(res.hydro.MFA, axes = 1:3, proba = 0.05)
  names(ddres1)
  
  # 4.2.1. Variables correlation with MFA axis1
  ddres1.1=ddres1$Dim.1$quanti
  ddres1.1=as.data.frame(ddres1.1[complete.cases(ddres1.1),])
  ddres1.1cor.hydro=ddres1.1[abs(ddres1.1$correlation)>=0.6,]
  ddres1.1cor.hydro$ct=unlist(strsplit(row.names(ddres1.1cor.hydro),split='[.]'))[seq(1,2*dim(ddres1.1cor.hydro)[1],2)]
  ddres1.1cor.hydro$year=unlist(strsplit(row.names(ddres1.1cor.hydro),split='[.]'))[seq(2,2*dim(ddres1.1cor.hydro)[1],2)]
  ddres1.1cor.hydro=ddres1.1cor.hydro[order(ddres1.1cor.hydro$ct),]
  ddres1.1cor.hydro$spid=row.names(ddres1.1cor.hydro)
  ddres1.1cor.hydro$sp=unlist(strsplit(row.names(ddres1.1cor.hydro),split='[.]'))[seq(1,2*length(row.names(ddres1.1cor.hydro)),2)]
  ddres1.1cor.hydro$signif=abs(ddres1.1cor.hydro$correlation)>=0.6
  head(ddres1.1cor.hydro)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.1cor.hydro)%in%names(var.contribs1))
  
  # 4.2.2. Variables correlation with MFA axis2
  ddres1.2=ddres1$Dim.2$quanti
  ddres1.2=as.data.frame(ddres1.2[complete.cases(ddres1.2),])
  ddres1.2cor.hydro=ddres1.2[abs(ddres1.2[,1])>=0.6,]
  ddres1.2cor.hydro$ct=unlist(strsplit(row.names(ddres1.2cor.hydro),split='[.]'))[seq(1,2*dim(ddres1.2cor.hydro)[1],2)]
  ddres1.2cor.hydro$year=unlist(strsplit(row.names(ddres1.2cor.hydro),split='[.]'))[seq(2,2*dim(ddres1.2cor.hydro)[1],2)]
  ddres1.2cor.hydro=ddres1.2cor.hydro[order(ddres1.2cor.hydro$correlation),]
  ddres1.2cor.hydro$spid=row.names(ddres1.2cor.hydro)
  ddres1.2cor.hydro$sp=unlist(strsplit(row.names(ddres1.2cor.hydro),split='[.]'))[seq(1,2*length(row.names(ddres1.2cor.hydro)),2)]
  ddres1.2cor.hydro$aCor=abs(ddres1.2cor.hydro$correlation)
  head(ddres1.2cor.hydro)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.2cor.hydro)%in%names(var.contribs2))
  
  # 4.2.3. Variables correlation with MFA axis 3 
  ddres1.3=ddres1$Dim.3$quanti
  ddres1.3=as.data.frame(ddres1.3[complete.cases(ddres1.3),])
  ddres1.3cor.hydro=ddres1.3[abs(ddres1.3[,1])>=0.6,]
  ddres1.3cor.hydro$ct=unlist(strsplit(row.names(ddres1.3cor.hydro),split='[.]'))[seq(1,2*dim(ddres1.3cor.hydro)[1],2)]
  ddres1.3cor.hydro$year=unlist(strsplit(row.names(ddres1.3cor.hydro),split='[.]'))[seq(2,2*dim(ddres1.3cor.hydro)[1],2)]
  ddres1.3cor.hydro=ddres1.3cor.hydro[order(ddres1.3cor.hydro$correlation),]
  ddres1.3cor.hydro$spid=row.names(ddres1.3cor.hydro)
  ddres1.3cor.hydro$sp=unlist(strsplit(row.names(ddres1.3cor.hydro),split='[.]'))[seq(1,2*length(row.names(ddres1.3cor.hydro)),2)]
  ddres1.3cor.hydro$aCor=abs(ddres1.3cor.hydro$correlation)
  head(ddres1.3cor.hydro)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.3cor.hydro)%in%names(var.contribs2))
  head(ddres1.1cor.hydro)
  unique(ddres1.1cor.hydro$sp)
  ddres1.1cor.hydro[ddres1.1cor.hydro$sp=='Heq','sp']='Fwh'
  ddres1.1cor.hydro[ddres1.1cor.hydro$sp=='ProfHmel','sp']='mixZ'
  unique(ddres1.2cor.hydro$sp)
  ddres1.2cor.hydro[ddres1.2cor.hydro$sp=='Heq','sp']='Fwh'
  ddres1.2cor.hydro[ddres1.2cor.hydro$sp=='ProfHmel','sp']='mixZ'
  ddres1.2cor.hydro[ddres1.2cor.hydro$sp=='ProfPycn','sp']='pycZ'
  ddres1.2cor.hydro[ddres1.2cor.hydro$sp=='DefEpot','sp']='Dpe'
  unique(ddres1.3cor.hydro$sp)
  ddres1.3cor.hydro[ddres1.3cor.hydro$sp=='DefEpot','sp']='Dpe'
  
  # 4.3. Summary plot ----
  #*************************************
  x11()
  p1 <- ggplot(ddres1.1cor.hydro, aes(sp,year,size=abs(correlation),colour=correlation),shape=signif)+geom_point()+
    ggtitle("Dim.1")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()+ scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.6?"))+
    theme(axis.title.y = element_blank(),
          axis.title.x = element_blank(),plot.title = element_text(hjust = 0.5)) + guides(size=FALSE)
  
  p2 <- ggplot(ddres1.2cor.hydro, aes(sp,year,size=abs(correlation),colour=correlation),shape=signif)+geom_point()+
    ggtitle("Dim.2")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()+ scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.6?"))+
    theme(axis.title.y = element_blank(),
          axis.title.x = element_blank(),plot.title = element_text(hjust = 0.5)) + guides(size=FALSE)
  
  p3 <- ggplot(ddres1.3cor.hydro, aes(sp,year,size=abs(correlation),colour=correlation),shape=signif)+geom_point()+
    ggtitle("Dim.3")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()+ scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.6?"))+
    theme(axis.title.y = element_blank(),plot.title = element_text(hjust = 0.5),
          axis.title.x = element_blank()) + guides(size=FALSE)
  multiplot(p1,p2,p3,cols=3)
  # Save figure
  dev.print(png, file=paste(path.export.hydro,"Figure14_hydroMFA123corVar.png",sep=""), 
            units='cm',width=30, height=15,res=300)
  dev.copy2eps(file=paste(path.export.hydro,"Figure14_hydroMFA123corVar.eps",sep=""))
  
#*********************************************************************** ----
# 5. Spatial analysis of MFA results -------------
#*********************************************************************** ----

  # 5.1. Maps of mean individuals MFA coordinates, ----
  #************************************************************
  # Objectives:
  #*********************
  # i) assess  environmental gradients
  # ii) assess within-cell temporal variability (inertia)
  # iii) define areas with stable and contrasted environmental conditions
  # over time ("characteristic areas")
  # Methods:
  #*********************
  # Characteristic areas are defined by selecting cells with above average
  # contribution and/oir quality of representation relatively to MFA1 to 3, over MFA1 to 3, and over MFA1 to 61
  # see function help page for details: ?MFA.rasterPlot
  
  MFAhydrores.rasters=MFA.rasterPlot(MFAres=res.hydro.MFA,nMFA=3,
                                     cellsel=cellsel.hydro,
                                     xyCell=pipo.hydro,
                                     ptitle='',
                                     path.export=path.export.hydro,
                                         plotit=list(MFA123coord=TRUE,
                                                     MFA123contrib=TRUE,
                                                     MFA123timevar=TRUE,
                                                     MFA123cos2=TRUE,
                                                     MFA123coordMcos2=TRUE,
                                                     MFA123coordMcontrib=TRUE,
                                                     MFA123pind=FALSE),
                                     ux11=TRUE,funSel=mean,
                                     anomalit=FALSE,
                                     Ndim=list(c(1),c(2),c(3),seq(3),
                                               seq(dim(res.hydro.MFA$ind)[2])),
                                     xlab='',ylab='',fwidth=30,
                                     fheight=15,main='',
                                     bathy.plot = TRUE,newbathy = FALSE)
  names(MFAhydrores.rasters)
  MFAinertia=MFAhydrores.rasters$MFAinertia
  MFAinertia.raster=MFAhydrores.rasters$MFAinertia.raster
  # quality of representation of individuals
  cos2df.hydro=res.hydro.MFA$ind$cos2
  # individuals contributions to MFA planes
  contrib2df.hydro=res.hydro.MFA$ind$contrib
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals quality of representation on MFA planes
  MFAcoordMcos2.hydro.rasterStack=MFAhydrores.rasters$MFAcoordMcos2.rasterStack
  
  # Plot MFA123 inertia maps
  # = inter-annual variability of MFA loadings in grid-cells
  #************************
  MFAinertia.raster.hydro=MFAhydrores.rasters$MFAinertia.raster
  raster.levelplot.PELGAS(MFAinertia.raster.hydro[[1:3]],ux11=TRUE,
                          lptheme=viridisTheme,path1=path.export.hydro,
                          fid1=paste('1-3_MFAcoordInertia',sep=''),
                          margin = FALSE,xlab='',ylab='',nattri='',
                          fwidth=30,fheight=15,fres=300)
  
  # 5.2. Define "characteristic areas", -----------
  # where cell maps quality of representation (cos2) in MFA planes is higher 
  # than the results of "funSel" applied on contribution maps
  #*************************************************************************
  # 5.2.1. Extract cells with highest cos2 ----
  MFAhydrocoordMcos2.hydro.rasterStack=MFAhydrores.rasters$MFAcoordMcos2.rasterStack
  # Define clumps of cells
  MFAhydrocoordMcos2.hydro.rasterStackc1=clump(raster(MFAhydrocoordMcos2.hydro.rasterStack,1),
                                         direction=8)
  plot(MFAhydrocoordMcos2.hydro.rasterStackc1)
  MFAhydrocoordMcos2.hydro.rasterStackc2=clump(raster(MFAhydrocoordMcos2.hydro.rasterStack,2),
                                         direction=8)
  plot(MFAhydrocoordMcos2.hydro.rasterStackc2)
  MFAhydrocoordMcos2.hydro.rasterStackc3=clump(raster(MFAhydrocoordMcos2.hydro.rasterStack,3),
                                         direction=4)
  plot(MFAhydrocoordMcos2.hydro.rasterStackc3,col=rainbow(10))
  # Mean MFA values in zones
  zdiff.MFA1=zonal(raster(MFAhydrocoordMcos2.hydro.rasterStack,1), MFAhydrocoordMcos2.hydro.rasterStackc1, 'mean')
  thrMFA1=abs(zdiff.MFA1[1,2]-zdiff.MFA1[2,2])
  zdiff.MFA2=zonal(raster(MFAhydrocoordMcos2.hydro.rasterStack,2), MFAhydrocoordMcos2.hydro.rasterStackc2, 'mean')
  thrMFA2=abs(zdiff.MFA2[1,2]-zdiff.MFA2[2,2])
  #zdiff.MFA3=zonal(raster(MFAhydrocoordMcos2.hydro.rasterStack,3), MFAhydrocoordMcos2.hydro.rasterStackc3, 'mean')
  #thrMFA3=abs(mean(zdiff.MFA3[c(1:4,6:10),2])-zdiff.MFA3[5,2])
  
  # Extract cell coordinates and MFA clusters
  MFAhydrocoordMcos2.hydro.rasterStack1.xyz <- rasterToPoints(MFAhydrocoordMcos2.hydro.rasterStackc1)
  MFAhydrocoordMcos2.hydro.rasterStack2.xyz <- rasterToPoints(MFAhydrocoordMcos2.hydro.rasterStackc2)
  MFAhydrocoordMcos2.hydro.rasterStack3.xyz <- rasterToPoints(MFAhydrocoordMcos2.hydro.rasterStackc3)
  head(MFAhydrocoordMcos2.hydro.rasterStack1.xyz)
  head(cellsel.hydro)
  MFAhydrocoordMcos2.hydro.rasterStack1.xyz=merge(MFAhydrocoordMcos2.hydro.rasterStack1.xyz,
                                                  cellsel.hydro,by.x=c('x','y'),
                                            by.y=c('x','y'))
  MFAhydrocoordMcos2.hydro.rasterStack2.xyz=merge(MFAhydrocoordMcos2.hydro.rasterStack2.xyz,
                                                  cellsel.hydro,by.x=c('x','y'),
                                            by.y=c('x','y'))
  MFAhydrocoordMcos2.hydro.rasterStack3.xyz=merge(MFAhydrocoordMcos2.hydro.rasterStack3.xyz,
                                                  cellsel.hydro,by.x=c('x','y'),
                                            by.y=c('x','y'))

  # 5.2.2. Mean hydro conditions in characteristic areas  ----
  #***********************************
  # MFA1: coast vs. offshore, salinity gradient
  CLres.hydro.MFA1=merge(MFAhydrocoordMcos2.hydro.rasterStack1.xyz,
                         gfhbsint[,c('I','J','Parameter','Year','Zvalue')],
                         by.x=c('I','J'),by.y=c('I','J'))
  CLres.hydro.MFA1[CLres.hydro.MFA1$clumps==1,'clumps']='Coast'
  CLres.hydro.MFA1[CLres.hydro.MFA1$clumps==2,'clumps']='Offshore'
  head(CLres.hydro.MFA1)
  # plot hydro variables distributions in MFA1 areas
  library(lattice)
  bwplot(Zvalue~clumps|Parameter,CLres.hydro.MFA1,scales=list(y='free'),
         horizontal=FALSE)
  # MFA2: North vs. South, bottom temperature gradient
  CLres.hydro.MFA2=merge(MFAhydrocoordMcos2.hydro.rasterStack2.xyz,
                         gfhbsint[,c('I','J','Parameter','Year','Zvalue')],
                         by.x=c('I','J'),by.y=c('I','J'))
  CLres.hydro.MFA2[CLres.hydro.MFA2$clumps==1,'clumps']='North'
  CLres.hydro.MFA2[CLres.hydro.MFA2$clumps==2,'clumps']='South'
  head(CLres.hydro.MFA2)
  library(lattice)
  bwplot(Zvalue~clumps|Parameter,CLres.hydro.MFA2,scales=list(y='free'),
         horizontal=FALSE)
  # MFA3: center vs. periphery: less clear ...
  CLres.hydro.MFA3=merge(MFAhydrocoordMcos2.hydro.rasterStack3.xyz,
                         gfhbsint[,c('I','J','Parameter','Year','Zvalue')],
                         by.x=c('I','J'),by.y=c('I','J'))
  CLres.hydro.MFA3[CLres.hydro.MFA3$clumps%in%c(6,7,8),'clumps']=3
  CLres.hydro.MFA3$clumps=paste('border',CLres.hydro.MFA3$clumps,sep='.')
  CLres.hydro.MFA3[CLres.hydro.MFA3$clumps%in%c('border.5'),'clumps']='center.5'
  head(CLres.hydro.MFA3)
  library(lattice)
  bwplot(Zvalue~clumps|Parameter,CLres.hydro.MFA3,scales=list(y='free'),
         horizontal=FALSE)
  
#**********************************************************************
# 6. Temporal analysis of MFA results ----------
#********************************************************************** ----
  # 6.1. Years (group) mean positions in MFA space ------------
  #***************************************************************
  # From Pagès (2014):
  # Graphs like this are interpreted in a similar way as correlation circles: in
  # both cases the coordinate of a point is interpreted as a relationship measurement
  # with a maximum value of 1. But this new graph has two specificities: the
  # groups of variables are unstandardised and their coordinates are always positive.
  # They therefore appear in a square (with a side of 1 and with points [0,0]
  # and [1,1] as vertices) known as a relationship square. Pages (2014), p141
    # Therefore, when compared with an intuitive approach to interpretation
  # (for a PCA user), one highly practical advantage of this representation is its
  # relationship with the representations of individuals and variables already
  # provided: in MFA, data are considered from different points of view, but in
  # one single framework. Pages (2014), p141
  
  # 6.1. Extract group (years) MFA coordinates and contributions -----
  #************************************************
  names(res.hydro.MFA)
  res.hydro.MFA.group=res.hydro.MFA$group
  gcoord.hydro=data.frame(res.hydro.MFA.group$coord[,1:3])
  gcontrib.hydro=res.hydro.MFA.group$contrib[,1:3]
  gcontrib.hydro=data.frame(gcontrib.hydro)
  names(gcontrib.hydro)=paste('contrib',names(gcontrib.hydro),sep='.')
  gcontrib.hydro$year=row.names(gcontrib.hydro)
  gcoord.hydro$year=row.names(gcoord.hydro)
  gcoord.hydro=merge(gcoord.hydro,gcontrib.hydro,by.x='year',by.y='year')
  row.names(gcoord.hydro)=gcoord.hydro$year
  gcoord.hydro$contrib.Dim.1.2=gcoord.hydro$contrib.Dim.1+gcoord.hydro$contrib.Dim.2
  gcoord.hydro$contrib.Dim.2.3=gcoord.hydro$contrib.Dim.2+gcoord.hydro$contrib.Dim.3
  head(gcoord.hydro)  
  gcoord.hydro$year=paste('20',gcoord.hydro$year,sep='')
  range(gcoord.hydro$Dim.1)
  range(gcoord.hydro$Dim.2)
  range(gcoord.hydro$Dim.3)
  
  # 6.2. Plot of groups (years) mean positions in MFA space: -----------
  #************************************************
  p12 <- ggplot(gcoord.hydro,aes(Dim.1,Dim.2,label=year))+geom_text(aes(colour = contrib.Dim.1.2))+
    scale_colour_continuous(name="Contribution (%)")
  p23 <- ggplot(gcoord.hydro,aes(Dim.2,Dim.3,label=year))+geom_text(aes(colour = contrib.Dim.2.3))+
    scale_colour_continuous(name="Contribution (%)")
  multiplot(p12,p23)

  # 6.3. Eventually, study years partial axes correlations with MFA axes -------------
  #************************************************
  # We have already insisted on the need to connect the inertias from the MFA
  # and those in the separate PCAs (see Tables 4.4 and 4.5). It is also useful to
  # connect the factors of the MFA with those of the separate PCAs (also known
  # as partial axes), both to understand better the effects of weighting and to enrich
  # interpretation of the analyses. In order to do this, the latter are projected as
  # supplementary variables (see Figure 4.5). Pages 2014, p88
  
  # Partial axes correlations
  res.hydro.MFA.partial.axes=res.hydro.MFA$partial.axes
  ddres.pyears.hydro=data.frame(res.hydro.MFA.partial.axes$cor)
  head(ddres.pyears.hydro)
  ddres.pyears.hydro[row.names(ddres.pyears.hydro)=='Dim1.14',]
  ddres.pyears.hydro.13=data.frame(res.hydro.MFA.partial.axes$cor[,1:3])
  ddres.pyears.hydro.13s=ddres.pyears.hydro.13[round(abs(ddres.pyears.hydro.13$Dim.1))>=0.6|round(abs(ddres.pyears.hydro.13$Dim.2))>=0.6|
                                     round(abs(ddres.pyears.hydro.13$Dim.3))>=0.6,]
  ddres.pyears.hydro.13s$Dim=factor(gsub('[.]','',substr(row.names(ddres.pyears.hydro.13s),1,5)),ordered=TRUE,levels=paste('Dim',seq(10),sep=''))
  ddres.pyears.hydro.13s$Year=gsub('[.]','',substr(row.names(ddres.pyears.hydro.13s),6,8))
  ddres.pyears.hydro.13s.long=reshape(ddres.pyears.hydro.13s,varying=list(1:3),direction='long',timevar='MFA',v.names='correlation')
  ddres.pyears.hydro.13s.long$MFA=paste('Dim',ddres.pyears.hydro.13s.long$MFA,sep='')
  ddres.pyears.hydro.13s.long$signif=abs(round(ddres.pyears.hydro.13s.long$correlation))>=0.6
  ddres.pyears.hydro.13s.long$MFA=paste('MFA',ddres.pyears.hydro.13s.long$MFA,sep='.')
  head(ddres.pyears.hydro.13s.long)  
  # Plot partial axes correlations with MFA axes
  p <- ggplot(ddres.pyears.hydro.13s.long[ddres.pyears.hydro.13s.long$Dim%in%c('Dim1','Dim2','Dim3'),], 
              aes(Dim,Year,size=abs(correlation),colour=correlation))+geom_point()
  p+ scale_colour_gradientn(colours = viridis(3)) + facet_grid(.~MFA)+
    theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(1)),axis.title.y = element_blank(),
          axis.title.x = element_blank()) + guides(size=FALSE)

  # 7. Spatio-temporal analysis of MFA results ---------------
  #*********************************************** ----------
  # 7.1. Maps of partial individuals (map cell:year pairs) coordinates ----
  # in MFA space
  #***********************************************
  MFAcoord.hydro=res.hydro.MFA$ind$coord
  pindCoord.hydro=res.hydro.MFA$ind$coord.partiel
  names(res.hydro.MFA$ind)
  MFAyears.hydro=unlist(strsplit(row.names(pindCoord.hydro),split='[.]'))[seq(2,(2*length(row.names(pindCoord.hydro))),2)]
  lyears.hydro=unique(MFAyears.hydro)
  
  # 7.1.1. Partial individuals maps, one plot per year ----  
  #**********************************
  for (i in 1:length(lyears.hydro)){
    dfi=pindCoord.hydro[MFAyears.hydro==lyears.hydro[i],]-MFAcoord.hydro
    MFA.rasterPlot(MFAres=dfi,nMFA=3,cellsel.hydro,pipo.hydro,
                   path.export=NULL,
                       plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                   MFA123contrib=FALSE,
                                   MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                   MFA123coordMcontrib=FALSE,MFA123pind=FALSE),funSel='mean',
                       ptitle=paste('20',lyears[i],sep=''),
                   lptheme = viridisTheme(),newbathy = FALSE)
  }
  # 7.1.2. Mosaics of partial individuals coordinates anomalies maps -----
  # one mosaic plot per MFA1,2,3, with all years
  #****************************************
  res=MFA.rasterPlot(MFAres=res.hydro.MFA,nMFA=3,cellsel.hydro,
                     pipo.hydro,path.export=NULL,
                         plotit=list(MFA123coord=TRUE,MFA123contrib=FALSE,MFA123timevar=FALSE,
                                     MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                     MFA123coordMcontrib=FALSE,MFA123pind=TRUE),funSel='mean',
                         ptitle=paste(''),lptheme=BuRdTheme,
                     anomalit=TRUE,newbathy=FALSE)
  
  # -> shows atypical years with above/below average MFA values in particular areas

 
