#*******************************************************************************************************
# CONVERSION DE DONNEES TUTTI GENERIQUE DANS UN FORMAT LISIBLE PAR DES HUMAINS 
#*******************************************************************************************************
# Chargement de la librairie EchoR
#--------------------------------
library(EchoR)

# Definitions des chemins
#--------------------------------
donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
prefix='C:/Users/mdoray/documents/'
prefix=paste(donnees2,'Campagnes/',sep='')
#prefix='Z:/'
# prefix='M:/'
# prefix='R:/Campagnes/'

cruise='PELGAS2016'

# chemin vers dossier de l export casino evts
path.cas.echant2=paste(prefix,cruise,'/Casino/',sep='')

# 2.0 Import des evts casino (pour completer les positions geographiques manquantes dans l'export tutti)
#------------------------------
casimp=casino.import(path.cas=paste(path.cas.echant2,'casino_pelgas2016_evts.csv',sep=''),
                     stp.gears = c("76x70", "57x52","MIK"),stp.events=c('DFIL','DPLON','DEBFIL'),
                     cruiseCode='PG16',runlag=2,dstart="02/05/2016 00:00:00",check.strates=FALSE)
names(casimp)
stp=casimp$stp
#extrait le fichier jackpot
jackpot=casimp$jackpot
#extrait les erreurs du fichier jackpot
errors=casimp$errors
#extrait les operations de peche
#head(jackpot)
head(stp)
operation.peche=jackpot[jackpot$action%in%c('HQ','DEBFIL'),]
operation.peche=stp
names(operation.peche)[names(operation.peche)=='NOSTA']='NumStation'
names(operation.peche)[names(operation.peche)=='LONF']='lon'
names(operation.peche)[names(operation.peche)=='LATF']='lat'
names(operation.peche)

# check trawl positions
#--------------------
plot(operation.peche$lon,operation.peche$lat)
coast()
unique(operation.peche$NumStation)
length(unique(operation.peche$NumStation))
# extrait le casino
casevt=casimp$cas
#export du fichier jackpot
#write.table(jackpot,'T:/PELGAS2015/Casino/jackpototo.csv',sep=';',row.names=FALSE)
head(casevt)
ctd=casevt[casevt$Code.Appareil=='BATHY'&casevt$Code.Action=='DEBFIL',]

# 3.0 Import des donnees de peche
#------------------------------
# De Tutti
#-------------------

path.tutti=paste(prefix,'PELGAS2016/Peche/Tutti_Export_generique/LeDernier/',sep='')
path.catch=paste(path.tutti,'csv/catch.csv',sep='')
path.operations=paste(path.tutti,'csv/operation.csv',sep='')
path.param=paste(path.tutti,'csv/parameter.csv',sep='')
path.param=paste(path.tutti,'csv/parameter.csv',sep='')
path.sampleCategory=paste(path.tutti,'csv/sampleCategory.csv',sep='')

# path.catch="T:/PELGAS2015/Peche/tutti/ExportGenerique/les_anciens/exportCruise-PELGAS_2015_05_17_14h16TU/exportCruise-100000/catch.csv"
# path.operations="T:/PELGAS2015/Peche/tutti/ExportGenerique/les_anciens/exportCruise-PELGAS_2015_05_17_14h16TU/exportCruise-100000/operation.csv"
# path.param="T:/PELGAS2015/Peche/tutti/ExportGenerique/les_anciens/exportCruise-PELGAS_2015_05_17_14h16TU/exportCruise-100000/parameter.csv"
# #   
# spCor=data.frame(spns=c('Scomber colias','Lithognathus mormyrus','Myliobatis aquila','Aurelia','Rhizostoma'),
#                  spcode=c('SCOM-JAP','LITH-MOR','MYLI-AQU','AURE-SPP','RHIZ-SPP'))

lsp.fish=c("ENGR-ENC", "SARD-PIL", "SPRA-SPR", "TRAC-TRU", "TRAC-MED", 
           "SCOM-SCO", "SCOM-JAP", "MICR-POU","CAPR-APE","MERL-MCC")

peche.data=Tutti2EchoR(path.catch=path.catch,path.operations=path.operations,
                       path.param=path.param,path.export=NULL,spCor=NULL,
                       operation.peche=operation.peche,path.sampleCategory = path.sampleCategory,
                       lsp = lsp.fish)

# extraction des donnees
names(peche.data)
TotalSamples4EchoR=peche.data$TotalSamples4EchoR
lspALL=sort(unique(TotalSamples4EchoR$baracoudacode))
head(TotalSamples4EchoR)

#names(TotalSamples4EchoR)
length(unique(TotalSamples4EchoR$operationID))
SubSamples4EchoR=peche.data$SubSamples4EchoR
length(unique(SubSamples4EchoR$operationID))
#names(SubSamples4EchoR)
metadata=peche.data$metadata
metadata.pros=metadata[substr(metadata$Code_Station,2,2)==5,]
metadata.pros$Engin="Chaluts boeufs pelagiques"
head(metadata)
unique(metadata$STRATE)
operations=peche.data$OPERATION
#head(operations)
unique(operations$Strate_Id)
catch=peche.data$CATCH
param=peche.data$param
Peche.wide=peche.data$Peche.wide
param.wide=peche.data$param.wide
param.wide=param.wide[order(param.wide$Code_station),]
unique(param.wide$GEAR)

# Peche.wide &co
dim(Peche.wide)
head(Peche.wide)
dim(Peche.wide)
Peche.wide$STRATE=substr(Peche.wide$STRATE,1,3)
Peche.wide$pro=substr(Peche.wide$NOSTA,2,2)==5
dim(Peche.wide[!Peche.wide$pro,])
param.wide=peche.data$param.wide
operations=peche.data$OPERATION
metadata=peche.data$metadata

# tutti micronecton
#------------------------
lsp.microN=c("AEQO-AEQ","AURE-AUR","BERO-OVA","CHRY-HYS","CYAN-LAM","FM-SALPD","GELA-AUT","GELA-CYM","GELA-RHY",
             "MB-CNIDA","PLEB-PIL","SALP-FUS","SALP-MAX","SOES-ZON","CRYG-LIN","POLY-HEN","MAUR-MUE")

peche.microN=Tutti2EchoR(path.catch=path.catch,path.operations=path.operations,
                         path.param=path.param,path.export=NULL,spCor=NULL,
                         operation.peche=operation.peche,path.sampleCategory = path.sampleCategory,
                         lsp = lsp.microN)

microN.wide=peche.microN$Peche.wide
microN.wide$STRATE=substr(microN.wide$STRATE,1,3)
microN.wide[is.na(microN.wide$GEAR),]
unique(microN.wide$GEAR)
microN.wide[is.na(microN.wide$GEAR),]
operations.mN=peche.microN$OPERATION
miks=operations.mN[operations.mN$Engin=='MIK 1600 Âµm, cylindre diamÃ¨tre 2 m',]
unique(miks$Code_Station)

# tutti autres
#------------------------
lsp.misc=lspALL[!lspALL%in%c(lsp.fish,lsp.microN)]

peche.misc=Tutti2EchoR(path.catch=path.catch,path.operations=path.operations,
                       path.param=path.param,path.export=NULL,spCor=NULL,
                       operation.peche=operation.peche,path.sampleCategory = path.sampleCategory,
                       lsp = lsp.misc)

misc.wide=peche.misc$Peche.wide
misc.wide$STRATE=substr(misc.wide$STRATE,1,3)
head(misc.wide)