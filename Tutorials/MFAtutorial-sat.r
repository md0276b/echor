library(EchoR)
library(FactoMineR)
library(rasterVis)
library(latticeExtra)
library(vegan)
library(maps)
library(maptools)
library(sp)
library(viridisLite)
library(ggplot2)
library(scales)
library(NbClust)
library(factoextra)
library(gridExtra)
library(marmap)
data(gradient_colors)

#***************************************************************************************************************************************
#                                               Multiple Factor Analysis with FactoMineR tutorial
#                                                           Remote sensing data
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.5

# 1. Import sat images in raster format -----
#******************************************************************
  
  echosonde='/run/user/1000/gvfs/smb-share:domain=ifr,server=echosonde,share=data,user=mdoray/'
  donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
  path.export.grid.sat=paste(echosonde,'Satellite/Atlantic/',sep='')
  year=2000:2018
  
  # import Chl-a
  idi=paste('mSSChlaYearMonth',paste(min(year),max(year),sep='-'),sep = "_")
  filei3 = paste(path.export.grid.sat,'Chla/raster/',paste('rasterStack',idi,sep='_'),'.grd',sep='')
  idi=paste('sdSSChlaYearMonth',paste(min(year),max(year),sep='-'),sep = "_")
  filei4 = paste(path.export.grid.sat,'Chla/raster/',paste('rasterStack',idi,sep='_'),'.grd',sep='')
  idi=paste('q90SSChlaYearMonth',paste(min(year),max(year),sep='-'),sep = "_")
  filei5 = paste(path.export.grid.sat,'Chla/raster/',paste('rasterStack',idi,sep='_'),'.grd',sep='')
  # read in raster bricks
  mchla.rb.yearmonth=brick(filei3)
  sdchla.rb.yearmonth=brick(filei4)
  q90chla.rb.yearmonth=brick(filei5)
  plot(q90chla.rb.yearmonth)

  # import SST
  idi=paste('mSSTYearMonth',paste(min(year),max(year),sep='-'),sep = "_")
  filei6 = paste(path.export.grid.sat,'SST/raster/',paste('rasterStack',idi,sep='_'),'.grd',sep='')
  idi=paste('sdSSTYearMonth',paste(min(year),max(year),sep='-'),sep = "_")
  filei7 = paste(path.export.grid.sat,'SST/raster/',paste('rasterStack',idi,sep='_'),'.grd',sep='')
  # read in raster bricks
  msst.rb.yearmonth=brick(filei6)
  sdsst.rb.yearmonth=brick(filei7)
  plot(msst.rb.yearmonth,col=gradient.colors)
  
# 2. Prepare datasets for MFA ------------------------
#********************************************************************

  # 2.1. Select area -------------
  #******************************
  mchla.rb.yearmonth.dp <- crop(mchla.rb.yearmonth, extent(-10.7,8,35.8,51))
  plot(mchla.rb.yearmonth.dp)
  sdchla.rb.yearmonth.dp <- crop(sdchla.rb.yearmonth, extent(-10.7,8,35.8,51))
  q90chla.rb.yearmonth.dp <- crop(q90chla.rb.yearmonth, extent(-10.7,8,35.8,51))
  msst.rb.yearmonth.dp <- crop(msst.rb.yearmonth, extent(-10.7,8,35.8,51))
  plot(msst.rb.yearmonth.dp,col=gradient.colors)
  sdsst.rb.yearmonth.dp <- crop(sdsst.rb.yearmonth, extent(-10.7,8,35.8,51))
  #mchla.rb.yearmonth.BoB <- crop(mchla.rb.yearmonth, extent(-9,0,43,49))
  #mchla.rb.yearmonth.BoB <- crop(mchla.rb.yearmonth, extent(-9,0,43,49))
  #mchla.rb.yearmonth.GoL <- crop(mchla.rb.yearmonth, extent(3,8,41,45))
  #mchla.rb.yearmonth.atl <- crop(mchla.rb.yearmonth, extent(-10.7,0,35.8,52))
  #mchla.rb.yearmonth.ES <- crop(mchla.rb.yearmonth, extent(-3,-2,47,47.5))
  
  # get bathy
  dp.bathy=getNOAA.bathy(-10.7,8,35.8,51)
  plot(dp.bathy)
  dp.bathy.raster=marmap::as.raster(dp.bathy)
  dp.bathy.raster.rs=resample(dp.bathy.raster,rae, fun='bilinear')
  dp.bathy.raster.rs=trim(dp.bathy.raster.rs)
  plot(dp.bathy.raster.rs)
  dp.bathy.raster.5msup=mask(dp.bathy.raster.rs,dp.bathy.raster.rs>(-5),maskvalue=FALSE)
  dp.bathy.raster.5minf=mask(dp.bathy.raster.rs,dp.bathy.raster.rs<(-5),maskvalue=FALSE)
  plot(dp.bathy.raster.5minf)
  plot(dp.bathy.raster.5msup)
  dp.bathy.raster.10minf=mask(dp.bathy.raster.rs,dp.bathy.raster.rs<(-10),maskvalue=FALSE)
  plot(dp.bathy.raster.10minf)
  
  # Remove cells shallower than 5 m
  mchla.rb.yearmonth.dps=mask(mchla.rb.yearmonth.dp,dp.bathy.raster.5minf)
  sdchla.rb.yearmonth.dps=mask(sdchla.rb.yearmonth.dp,dp.bathy.raster.5minf)
  q90chla.rb.yearmonth.dps=mask(q90chla.rb.yearmonth.dp,dp.bathy.raster.5minf)
  plot(mchla.rb.yearmonth.dps)
  
  # 2.1.2. Extract raster cell coordinates ----
  xyrb=coordinates(mchla.rb.yearmonth.dp)
  
  # Check data range and distribution
  summary(mchla.rb.yearmonth.dps)
  quantile98=function(x,...){quantile(x,probs=.98,na.rm=TRUE)}
  q98mchla.rb.yearmonth=cellStats(mchla.rb.yearmonth.dps,stat=quantile98,na.rm=TRUE)
  summary(q98mchla.rb.yearmonth)
  maxmchla.rb.yearmonth=cellStats(mchla.rb.yearmonth.dps,stat=max,na.rm=TRUE)
  summary(maxmchla.rb.yearmonth)
  
  # 2.1.2. Extract raster cell values and log Chla----
  mchla.yearmonth.dps=getValues(mchla.rb.yearmonth.dps)
  dim(mchla.yearmonth.dps)
  hist(mchla.yearmonth.dps)
  mchla.yearmonth.dp=log(mchla.yearmonth.dps+1)
  hist(mchla.yearmonth.dps)
  dimnames(mchla.yearmonth.dp)[[2]]=paste('mchl',substr(dimnames(mchla.yearmonth.dp)[[2]],4,8),sep='.')
  sdchla.yearmonth.dp=getValues(sdchla.rb.yearmonth.dps)
  sdchla.yearmonth.dp=log(sdchla.yearmonth.dp+1)
  dimnames(sdchla.yearmonth.dp)[[2]]=paste('schl',substr(dimnames(sdchla.yearmonth.dp)[[2]],4,8),sep='.')
  q90chla.yearmonth.dp=getValues(q90chla.rb.yearmonth.dps)
  q90chla.yearmonth.dp=log(q90chla.yearmonth.dp+1)
  dimnames(q90chla.yearmonth.dp)[[2]]=paste('qchl',substr(dimnames(q90chla.yearmonth.dp)[[2]],4,8),sep='.')
  msst.yearmonth.dp=getValues(msst.rb.yearmonth.dp)
  dimnames(msst.yearmonth.dp)[[2]]=paste('msst',substr(dimnames(msst.yearmonth.dp)[[2]],4,8),sep='.')
  sdsst.yearmonth.dp=getValues(sdsst.rb.yearmonth.dp)
  dimnames(sdsst.yearmonth.dp)[[2]]=paste('ssst',substr(dimnames(sdsst.yearmonth.dp)[[2]],4,8),sep='.')
  chlasst.yearmonth.dp=cbind(mchla.yearmonth.dp,sdchla.yearmonth.dp,q90chla.yearmonth.dp,
                             msst.yearmonth.dp,sdsst.yearmonth.dp)
  dim(chlasst.yearmonth.dp)
  
  # check
  plot(xyrb[,1],xyrb[,2])
  df=data.frame(xyrb,sdchla.yearmonth.dp)
  head(df)
  plot(df$x,df$y,cex=df$schl.00.01)
  df2=merge(xyrb,sdchla.yearmonth.dp,by.x='row.names',by.y='row.names')
  names(df2)
  plot(df2$x,df2$y,cex=df2$schl.00.01)
  
  # Remove NA's (land)
  sel=complete.cases(chlasst.yearmonth.dp)
  length(sel)
  dim(chlasst.yearmonth.dp)
  chlasst.yearmonth.dps=chlasst.yearmonth.dp[sel,]
  dim(chlasst.yearmonth.dps)
  xyrbs=xyrb[sel,]
  df3=merge(xyrbs,chlasst.yearmonth.dps,by.x='row.names',by.y='row.names')
  names(df3)
  plot(df3$x,df3$y,cex=df3$schl.00.01)
  # Define cellsel
  cellsel=data.frame(x=df3$x,y=df3$y,sel=TRUE,I=NA,J=NA,cell=df3$Row.names)
  
  # 2.3. Reshape data frame for analysis with year as grouping variable --------------
  #***************************
  dim(mchla.yearmonth.dp)
  ym=substr(dimnames(chlasst.yearmonth.dps)[[2]],6,10)
  lym=unique(ym)
  for (i in 1:length(lym)){
    chlasst.yearmonth.dpsi=chlasst.yearmonth.dps[,ym%in%lym[i]]
    dimnames(chlasst.yearmonth.dpsi)
    if (i==1){
      big.sat.MFA=chlasst.yearmonth.dpsi
    }else{
      big.sat.MFA=cbind(big.sat.MFA,chlasst.yearmonth.dpsi)
    }
  }
  dimnames(big.sat.MFA)

  # 2.5. Prepare and standardise dataset ------------------
  #*************************************
  # center and scale variables
  big.sat.MFAs=apply(big.sat.MFA,2,scale,center=TRUE,scale=TRUE)
  head(big.sat.MFAs)
  class(big.sat.MFAs)

  # 2.6. Select cells in survey area before MFA --------------
  #*************************************

  # 2.7. Remove constant columns 
  #*************************************
  # with zeroes
  # scols=apply(big.fssd.MFAs,2,sum,na.rm=TRUE)
  # srows=apply(big.fssd.MFAs,1,sum,na.rm=TRUE)
  # big.fssd.MFAs2=big.fssd.MFAs[,scols!=0]
  # sum(scols==0)
  # scols2=apply(big.fssd.MFAs2,2,sum,na.rm=TRUE)
  # summary(scols2)
  # # other constant columns
  # vcols=apply(big.fssd.MFAs2,2,var,na.rm=TRUE)
  # ccols=dimnames(big.fssd.MFAs2)[[2]][vcols==0]
  # big.fssd.MFAs2=big.fssd.MFAs2[,!dimnames(big.fssd.MFAs2)[[2]]%in%ccols]
  # dim(big.fssd.MFAs2)
  
  # 2.8. Generate no. of data in each group --------------
  #***************************************
  cwz=dimnames(big.sat.MFAs)[[2]]
  varn=unlist(strsplit(cwz,split='[.]'))[seq(1,3*length(cwz),3)]
  yearn=unlist(strsplit(cwz,split='[.]'))[seq(2,3*length(cwz),3)]
  monthn=unlist(strsplit(cwz,split='[.]'))[seq(3,3*length(cwz),3)]
  yearmonths=substr(dimnames(big.sat.MFAs)[[2]],6,10)
  tnnc=table(varn,yearmonths)
  tnnc.df=data.frame(table(varn,yearmonths))
  head(tnnc.df)
  stnnc=apply(tnnc,2,sum)
  head(tnnc.df)
  tnnc.dfa=aggregate(tnnc.df$Freq,list(nct=tnnc.df$yearmonths),sum)
  summary(tnnc.dfa$x)

# 3. Run MFA -----------------
#******************************************************
  # 3.1. MFA ------------
  #*********************************
  nMAF.max=85
  dimnames(big.sat.MFAs)
  res.sat.MFA<-MFA(big.sat.MFAs, group=stnnc, type=rep("s",length(stnnc)), 
                    ncp=nMAF.max,name.group=tnnc.dfa$nct, 
                    num.group.sup=NULL, graph=TRUE)
  names(res.sat.MFA)
  
  # 3.2. Explained variance -----------
  #**********************************  
  summary(res.sat.MFA)  
  par(bg='white')
  barplot(res.sat.MFA$eig[,2],main="% of variance explained",names.arg=1:nrow(res.sat.MFA$eig))
  barplot(res.sat.MFA$eig[,3],main="Cumulative % of variance explained",names.arg=1:nrow(res.sat.MFA$eig))
  res.sat.MFA$eig[round(res.sat.MFA$eig[,3])==95,]
  dim(big.sat.MFAs)
 
  # 3.3. Plots of individuals in MFA1-2-3 planes ---------
  #************************************
  # Plot of individuals in MFA1-2 plane 
  p1 = fviz_mfa_ind(res.sat.MFA, col.ind = "contrib") #with contribution in color scale
  p2 = fviz_mfa_ind(res.sat.MFA, col.ind = "cos2") #with quality of representation in color scale
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA2-3 plane
  p1 = fviz_mfa_ind(res.sat.MFA, col.ind = "contrib",axes = c(2, 3))
  p2 = fviz_mfa_ind(res.sat.MFA, col.ind = "cos2",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-3 plane
  p1 = fviz_mfa_ind(res.sat.MFA, col.ind = "contrib",axes = c(1, 3))
  p2 = fviz_mfa_ind(res.sat.MFA, col.ind = "cos2",axes = c(1, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-2-3 planes, with contribution in color scale
  p1 = fviz_mfa_ind(res.sat.MFA, col.ind = "contrib")
  p2 = fviz_mfa_ind(res.sat.MFA, col.ind = "contrib",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)

# 4. Contributions of variables to MFA axes ----------
#************************************

  par(bg='grey50')
  plot.MFA(res.sat.MFA, axes=c(1, 2), choix="var", new.plot=TRUE, lab.var=TRUE, 
           habillage="group", select = "coord 10")
  par(bg='white')
  
  # 4.1. select variables with above average contribution to MFA axes -----------
  #*************************************
  var.contrib=res.sat.MFA$quanti.var$contrib
  head(var.contrib)
  summary(var.contrib[,1])
  var.contribs1=var.contrib[var.contrib[,1]>mean(var.contrib[,1]),1]
  var.contribs2=var.contrib[var.contrib[,2]>mean(var.contrib[,2]),2]
  var.contribs3=var.contrib[var.contrib[,3]>mean(var.contrib[,3]),3]
  
  # 4.2. Variables correlation with MFA axes ------------
  #************************************
  var.cor=res.sat.MFA$quanti.var$contrib
  graphics.off()
  ddres1=dimdesc(res.sat.MFA, axes = 1:4, proba = 0.05)
  names(ddres1)
  
  # 4.2.1. Variables correlation with MFA axis1
  ddres1.1=ddres1$Dim.1$quanti
  ddres1.1=as.data.frame(ddres1.1[complete.cases(ddres1.1),])
  ddres1.1cor=ddres1.1[abs(ddres1.1$correlation)>=0.5,]
  head(ddres1.1cor)
  ddres1.1cor$varn=unlist(strsplit(row.names(ddres1.1cor),split='[.]'))[seq(1,3*dim(ddres1.1cor)[1],3)]
  ddres1.1cor$year=unlist(strsplit(row.names(ddres1.1cor),split='[.]'))[seq(2,3*dim(ddres1.1cor)[1],3)]
  ddres1.1cor$month=unlist(strsplit(row.names(ddres1.1cor),split='[.]'))[seq(3,3*dim(ddres1.1cor)[1],3)]
  ddres1.1cor$yearMonth=paste(ddres1.1cor$year,ddres1.1cor$month,sep='-')
  #ddres1.1cor=ddres1.1cor[order(ddres1.1cor$ct),]
  ddres1.1cor$spid=row.names(ddres1.1cor)
  ddres1.1cor$signif=abs(ddres1.1cor$correlation)>=0.5
  head(ddres1.1cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.1cor)%in%names(var.contribs1))
  # Plot variables well correlated with MFA axis1 (abs(cor)>0.5)
  p <- ggplot(ddres1.1cor, aes(year,month,size=abs(correlation),colour=correlation),
              shape=signif)+geom_point()+ggtitle("cor(variables,MFA1)")+ 
    scale_colour_gradientn(colours = viridis(10)) + theme_bw()+ 
    scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.5?"))
  p + facet_grid(~varn) + theme(axis.text.x=element_text(angle=45, hjust=1,size=rel(1)),
                                  axis.title.y = element_blank(),
                                  axis.title.x = element_blank()) + guides(size=FALSE)
  
  # 4.2.2. Variables correlation with MFA axis2
  ddres1.2=ddres1$Dim.2$quanti
  ddres1.2=as.data.frame(ddres1.2[complete.cases(ddres1.2),])
  ddres1.2cor=ddres1.2[abs(ddres1.2[,1])>=0.5,]
  ddres1.2cor$varn=unlist(strsplit(row.names(ddres1.2cor),split='[.]'))[seq(1,3*dim(ddres1.2cor)[1],3)]
  ddres1.2cor$year=unlist(strsplit(row.names(ddres1.2cor),split='[.]'))[seq(2,3*dim(ddres1.2cor)[1],3)]
  ddres1.2cor$month=unlist(strsplit(row.names(ddres1.2cor),split='[.]'))[seq(3,3*dim(ddres1.2cor)[1],3)]
  ddres1.2cor$yearMonth=paste(ddres1.2cor$year,ddres1.2cor$month,sep='-')
  #ddres1.2cor=ddres1.2cor[order(ddres1.2cor$ct),]
  ddres1.2cor$spid=row.names(ddres1.2cor)
  ddres1.2cor$signif=abs(ddres1.2cor$correlation)>=0.5
  head(ddres1.2cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.2cor)%in%names(var.contribs1))
  # Plot variables well correlated with MFA axis1 (abs(cor)>0.5)
  p <- ggplot(ddres1.2cor, aes(year,month,size=abs(correlation),colour=correlation),
              shape=signif)+geom_point()+ggtitle("cor(variables,MFA2)")+ 
    scale_colour_gradientn(colours = viridis(10)) + theme_bw()+ 
    scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.5?"))
  p + facet_grid(~varn) + theme(axis.text.x=element_text(angle=45, hjust=1,size=rel(1)),
                                axis.title.y = element_blank(),
                                axis.title.x = element_blank()) + guides(size=FALSE)
  
  # 4.2.3. Variables correlation with MFA axis 3 
  ddres1.3=ddres1$Dim.3$quanti
  ddres1.3=as.data.frame(ddres1.3[complete.cases(ddres1.3),])
  ddres1.3cor=ddres1.3[abs(ddres1.3[,1])>=0.5,]
  ddres1.3cor$varn=unlist(strsplit(row.names(ddres1.3cor),split='[.]'))[seq(1,3*dim(ddres1.3cor)[1],3)]
  ddres1.3cor$year=unlist(strsplit(row.names(ddres1.3cor),split='[.]'))[seq(2,3*dim(ddres1.3cor)[1],3)]
  ddres1.3cor$month=unlist(strsplit(row.names(ddres1.3cor),split='[.]'))[seq(3,3*dim(ddres1.3cor)[1],3)]
  ddres1.3cor$yearMonth=paste(ddres1.3cor$year,ddres1.3cor$month,sep='-')
  #ddres1.3cor=ddres1.3cor[order(ddres1.3cor$ct),]
  ddres1.3cor$spid=row.names(ddres1.3cor)
  ddres1.3cor$signif=abs(ddres1.3cor$correlation)>=0.5
  head(ddres1.3cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.3cor)%in%names(var.contribs1))
  # Plot variables well correlated with MFA axis1 (abs(cor)>0.5)
  p <- ggplot(ddres1.3cor, aes(year,month,size=abs(correlation),colour=correlation),
              shape=signif)+geom_point()+ggtitle("cor(variables,MFA3)")+ 
    scale_colour_gradientn(colours = viridis(10)) + theme_bw()+ 
    scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.5?"))
  p + facet_grid(~varn) + theme(axis.text.x=element_text(angle=45, hjust=1,size=rel(1)),
                                axis.title.y = element_blank(),
                                axis.title.x = element_blank()) + guides(size=FALSE)

  # 4.3. Summary plots of variable correlations with MFA axes --------------
  #**************************************************
  ddres1.1corPlusa.ct=aggregate(ddres1.1cor[ddres1.1cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.1cor[ddres1.1cor$correlation>=0.5,'year']),length)
  ddres1.2corPlusa.ct=aggregate(ddres1.2cor[ddres1.2cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.2cor[ddres1.2cor$correlation>=0.5,'year']),length)
  ddres1.2corMoinsa.ct=aggregate(ddres1.2cor[ddres1.2cor$correlation<=0.5,'correlation'],
                                 list(ct=ddres1.2cor[ddres1.2cor$correlation<=0.5,'year']),length)
  ddres1.3corPlusa.ct=aggregate(ddres1.3cor[ddres1.3cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.3cor[ddres1.3cor$correlation>=0.5,'year']),length)
  par(mfrow=c(2,2),mar=c(3,12,3,1),bg='white')
  barplot(ddres1.1corPlusa.ct$x,names.arg=ddres1.1corPlusa.ct$ct,main='cor(Variable:year,MFA1)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.1corMoins.res,1,sum),main='cor(Variable:year,MFA1)<=-0.5',horiz=TRUE,las=2)
  barplot(ddres1.2corPlusa.ct$x,names.arg=ddres1.2corPlusa.ct$ct,main='cor(Variable:year,MFA2)>=0.5',horiz=TRUE,las=2)
  barplot(ddres1.2corMoinsa.ct$x,names.arg=ddres1.2corMoinsa.ct$ct,main='cor(Variable:year,MFA2)<=0.5',horiz=TRUE,las=2)
  barplot(ddres1.3corPlusa.ct$x,names.arg=ddres1.3corPlusa.ct$ct,main='cor(Variable:year,MFA3)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.3corMoins.res,1,sum),main='cor(Variable:year,MFA3)<=-0.5',horiz=TRUE,las=2)
  par(mfrow=c(1,1))

#***************************************************************
# 5. Maps of MFA coordinates ---------------
#***************************************************************

  # Path to folder to export maps
  path.export='/users/mathieu/Documents/temp/MFAsat/'

  # 5.1. Plot maps of mean individuals MFA coordinates, 
  # filtered by contribution and/oir quality of representation and 
  # within-cell inertia on MFA1 to 3, over MFA1 to 3, and over MFA1 to 61
  # see function help page for details: ?BoB.MFA.rasterPlot
  #***********************************************************
  
  MFAres.rasters.sat=MFA.rasterPlot(MFAres=res.sat.MFA,nMFA=2,cellsel=cellsel,xyCell=xyrbs,
                                    ptitle='',path.export=path.export,ux11=TRUE,
                                    plotit=list(MFA123coord=TRUE,MFA123timevar=TRUE,
                                                MFA123contrib=TRUE,MFA123cos2=TRUE,
                                                MFA123coordMcos2=TRUE,
                                                MFA123coordMcontrib=TRUE,MFA123pind=TRUE),
                                    funSel=median,layout=c(2, 1),
                                    Ndim=list(c(1),c(2),seq(2),seq(nMAF.max)),
                                    bathy.plot=TRUE,newbathy=FALSE)
  
  # quality of representation of individuals
  cos2df=MFAres.rasters.sat$ind$cos2
  # individuals contributions to MFA planes
  contrib2df=MFAres.rasters.sat$ind$contrib
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals quality of representation on MFA planes
  MFAcoordMcos2.sat.rasterStack=MFAres.rasters.sat$MFAcoordMcos2.rasterStack
  
  # Plot MFA123 inertia maps
  #************************
  MFAinertia.sat.raster=MFAres.rasters.sat$MFAinertia.raster
  raster.levelplot(MFAinertia.sat.raster[[1:2]],ux11=TRUE,lptheme=viridisTheme,
                          path1=path.export,fid1=paste('1-2_MFAcoordInertia',sep=''),
                          margin = FALSE,xlab='',ylab='',nattri='',fwidth=30,fheight=15,fres=300)
  
  # 5.2. Define "characteristic areas", ------
  # where cell maps contribution to MFA planes is higher than the results of "funSel" applied on contribution maps
  #*****************************************************
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals contributions to MFA planes 
  MFAcoordMcontrib.sat.rasterStack=MFAres.rasters.sat$MFAcoordMcontrib.rasterStack
  # identify 'clumps' of cells in MFA planes to define "characteristic areas"
  MFAcoordMcontrib.sat.rasterStackc1=clump(raster(MFAcoordMcontrib.sat.rasterStack,1))
  plot(MFAcoordMcontrib.sat.rasterStackc1)
  MFAcoordMcontrib.sat.rasterStackc2=clump(raster(MFAcoordMcontrib.sat.rasterStack,2))
  plot(MFAcoordMcontrib.sat.rasterStackc2)
  #MFAcoordMcontrib.sat.rasterStackc3=clump(raster(MFAcoordMcontrib.sat.rasterStack,3),directions=4)
  #plot(MFAcoordMcontrib.sat.rasterStackc3)
  # Compute mean MFA coordinates in characteristic areas
  zonal(raster(MFAcoordMcontrib.sat.rasterStack,1), MFAcoordMcontrib.sat.rasterStackc1, 'mean')
  zonal(raster(MFAcoordMcontrib.sat.rasterStack,2), MFAcoordMcontrib.sat.rasterStackc2, 'mean')
  #zonal(raster(MFAcoordMcontrib.rasterStack,3), MFAcoordMcontrib.rasterStackc3, 'mean')
  # Extract cell coordinates and belongings to MFA characteristic areas and merge to data
  MFAcoordMcontrib.sat.rasterStack1.xyz <- rasterToPoints(MFAcoordMcontrib.sat.rasterStackc1)
  MFAcoordMcontrib.sat.rasterStack2.xyz <- rasterToPoints(MFAcoordMcontrib.sat.rasterStackc2)
  #MFAcoordMcontrib.sat.rasterStack3.xyz <- rasterToPoints(MFAcoordMcontrib.sat.rasterStackc3)
  head(MFAcoordMcontrib.sat.rasterStack1.xyz)
  MFAcoordMcontrib.sat.rasterStack1.xyz=merge(MFAcoordMcontrib.sat.rasterStack1.xyz,cellsel,by.x=c('x','y'),
                                          by.y=c('x','y'))
  MFAcoordMcontrib.sat.rasterStack2.xyz=merge(MFAcoordMcontrib.sat.rasterStack2.xyz,cellsel,by.x=c('x','y'),
                                          by.y=c('x','y'))
  #MFAcoordMcontrib.rasterStack3.xyz=merge(MFAcoordMcontrib.rasterStack3.xyz,cellsel,by.x=c('x','y'),
  #                                        by.y=c('x','y'))
  
  # Add belongings to characteristic areas to raw data
  clumpsdf1=MFAcoordMcontrib.sat.rasterStack1.xyz
  clumpsdf2=MFAcoordMcontrib.sat.rasterStack2.xyz
  #clumpsdf3=MFAcoordMcontrib.rasterStack3.xyz
  
    
  # Gives names to characteristic areas for plots

  # 5.3. Compute mean clumps composition -----
  #***************************************
  #MFA1: 
  #*******************
  CLres.fssd.ALL.MFA1a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=CLres.fssd.ALL.MFA1)
  # with mackerel and official palette
  # Relative biomass
  p <- ggplot(CLres.fssd.ALL.MFA1a, aes(cs,pmZ,fill = factor(sp))) +geom_bar(stat = "identity") + coord_flip()+
    scale_fill_manual(values=c('brown2','green','grey','blue','orange','red','black','magenta2','yellow'),
                      guide = guide_legend(title = "Species"))
  p + facet_grid(cz~clust)+ theme(axis.title.y = element_blank(),axis.title.x = element_blank())
  
  #MFA2 
  #*******************
  CLres.fssd.ALL.MFA2a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=CLres.fssd.ALL.MFA2)
  # with mackerel and official palette
  # Relative biomass
  p <- ggplot(CLres.fssd.ALL.MFA2a, aes(cs,pmZ,fill = factor(sp))) +geom_bar(stat = "identity") + coord_flip()+
    scale_fill_manual(values=c('brown2','green','grey','blue','orange','red','black','magenta2','yellow'),
                      guide = guide_legend(title = "Species"))
  p + facet_grid(cz~clust)+ theme(axis.title.y = element_blank(),axis.title.x = element_blank())
  
  # MFA3: Coastal N&S vs. Fer à Cheval
  #********************
  CLres.fssd.ALL.MFA3a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=CLres.fssd.ALL.MFA3)
  # with mackerel and official palette
  # Relative biomass
  p <- ggplot(CLres.fssd.ALL.MFA3a, aes(cs,pmZ,fill = factor(sp))) +geom_bar(stat = "identity") + coord_flip()+
    scale_fill_manual(values=c('brown2','green','grey','blue','orange','red','black','magenta2','yellow'),
                      guide = guide_legend(title = "Species"))
  p + facet_grid(cz~clust)+ theme(axis.title.y = element_blank(),axis.title.x = element_blank())

#*********************************************************************
# 6. Temporal analysis of MFA results ----------
  #*********************************************************************

  # 6.1. Years:month (group) mean positions in MFA space ---------------
  #********************************************************
  # From Pagès (2014):
  # Graphs like this are interpreted in a similar way as correlation circles: in
  # both cases the coordinate of a point is interpreted as a relationship measurement
  # with a maximum value of 1. But this new graph has two specificities: the
  # groups of variables are unstandardised and their coordinates are always positive.
  # They therefore appear in a square (with a side of 1 and with points [0,0]
  # and [1,1] as vertices) known as a relationship square. Pages (2014), p141
  
  # Therefore, when compared with an intuitive approach to interpretation
  # (for a PCA user), one highly practical advantage of this representation is its
  # relationship with the representations of individuals and variables already
  # provided: in MFA, data are considered from different points of view, but in
  # one single framework. Pages (2014), p141
  names(res.sat.MFA)
  res.sat.MFA.group=res.sat.MFA$group
  gcoord=data.frame(res.sat.MFA.group$coord[,1:2])
  gcontrib=res.sat.MFA.group$contrib[,1:2]
  gcontrib=data.frame(gcontrib)
  names(gcontrib)=paste('contrib',names(gcontrib),sep='.')
  gcontrib$year=substr(row.names(gcontrib),1,2)
  gcontrib$month=substr(row.names(gcontrib),4,5)
  gcontrib$yearMonth=row.names(gcontrib)
  gcoord$year=substr(row.names(gcoord),1,2)
  gcoord$month=substr(row.names(gcoord),4,5)
  gcoord$yearMonth=row.names(gcoord)
  gcoord=merge(gcoord,gcontrib,by.x=c('yearMonth'),by.y=c('yearMonth'))
  row.names(gcoord)=gcoord$yearMonth
  gcoord$contrib.Dim.1.2=gcoord$contrib.Dim.1+gcoord$contrib.Dim.2
  #gcoord$contrib.Dim.2.3=gcoord$contrib.Dim.2+gcoord$contrib.Dim.3
  head(gcoord)  
  gcoord$year=paste('20',gcoord$year,sep='')
  range(gcoord$Dim.1)
  range(gcoord$Dim.2)
  #range(gcoord$Dim.3)
  
  # Plot of groups mean positions in MFA space:
  #**********************************************
  p12 <- ggplot(gcoord,aes(Dim.1,Dim.2,label=yearMonth))+geom_text(aes(colour = contrib.Dim.1.2))+
    scale_colour_continuous(name="Contribution (%)")
  print(p12)
  #p23 <- ggplot(gcoord,aes(Dim.2,Dim.3,label=year))+geom_text(aes(colour = contrib.Dim.2.3))+
  #  scale_colour_continuous(name="Contribution (%)")
  #multiplot(p12,p23)

  # 6.2. Eventually, study years partial axes correlations with MFA axes ------------
  #****************************************************
  # We have already insisted on the need to connect the inertias from the MFA
  # and those in the separate PCAs (see Tables 4.4 and 4.5). It is also useful to
  # connect the factors of the MFA with those of the separate PCAs (also known
  # as partial axes), both to understand better the effects of weighting and to enrich
  # interpretation of the analyses. In order to do this, the latter are projected as
  # supplementary variables (see Figure 4.5). Pages 2014, p88
  
  # Partial axes correlations
  res.sat.MFA.partial.axes=res.sat.MFA$partial.axes
  ddres.pyears.sat=data.frame(res.sat.MFA.partial.axes$cor)
  head(ddres.pyears.sat)
  ddres.pyears.sat[row.names(ddres.pyears.sat)=='Dim1.14',]
  ddres.pyears.sat.13=data.frame(res.sat.MFA.partial.axes$cor[,1:2])
  ddres.pyears.sat.13s=ddres.pyears.sat.13[round(abs(ddres.pyears.sat.13$Dim.1))>=0.6|
                                             round(abs(ddres.pyears.sat.13$Dim.2))>=0.6,]
                                     #round(abs(ddres.pyears.sat.13$Dim.3))>=0.6,]
  ddres.pyears.sat.13s$Dim=factor(gsub('[.]','',substr(row.names(ddres.pyears.sat.13s),1,5)),
                                  ordered=TRUE,levels=paste('Dim',seq(10),sep=''))
  ddres.pyears.sat.13s$YearMonth=substr(row.names(ddres.pyears.sat.13s),6,10)
  ddres.pyears.sat.13s.long=reshape(ddres.pyears.sat.13s,varying=list(1:2),direction='long',
                                    timevar='MFA',v.names='correlation')
  ddres.pyears.sat.13s.long$MFA=paste('Dim',ddres.pyears.sat.13s.long$MFA,sep='')
  ddres.pyears.sat.13s.long$signif=abs(round(ddres.pyears.sat.13s.long$correlation))>=0.6
  ddres.pyears.sat.13s.long$MFA=paste('MFA',ddres.pyears.sat.13s.long$MFA,sep='.')
  head(ddres.pyears.sat.13s.long)  
  # Plot partial axes correlations with MFA axes
  p <- ggplot(ddres.pyears.sat.13s.long[ddres.pyears.sat.13s.long$Dim%in%c('Dim1','Dim2'),], 
              aes(Dim,YearMonth,size=abs(correlation),colour=correlation))+geom_point()
  p+ scale_colour_gradientn(colours = viridis(3)) + facet_grid(.~MFA)+
    theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(1)),axis.title.y = element_blank(),
          axis.title.x = element_blank()) + guides(size=FALSE)

  # 6.3. Maps of partial individuals (map cell:year pairs) coordinates in MFA space --------
  #***********************************************************
  MFAcoord.sat=res.sat.MFA$ind$coord
  pindCoord.sat=data.frame(res.sat.MFA$ind$coord.partiel)
  names(res.sat.MFA$ind)
  head(pindCoord.sat)
  lcells=unlist(strsplit(row.names(pindCoord.sat),split='[.]'))[seq(1,3*length(row.names(pindCoord.sat)),3)]
  lyears=unlist(strsplit(row.names(pindCoord.sat),split='[.]'))[seq(2,3*length(row.names(pindCoord.sat)),3)]
  lmonths=unlist(strsplit(row.names(pindCoord.sat),split='[.]'))[seq(3,3*length(row.names(pindCoord.sat)),3)]
  mpindCoord.sat.year=aggregate(pindCoord.sat,list(lcells,lyears),mean)
  names(mpindCoord.sat.year)[1:2]=c('cell','year')
  head(mpindCoord.sat.year)
  mpindCoord.sat.month=aggregate(pindCoord.sat,list(lcells,lmonths),mean)
  names(mpindCoord.sat.month)[1:2]=c('cell','month')
  head(mpindCoord.sat.month)
  MFAyearsMonths.sat=substr(row.names(pindCoord.sat),3,7)
  lyears.sat=unique(lyears)
  lmonths.sat=unique(lmonths)
  dim(pindCoord.sat);dim(MFAcoord.sat)
  
  # 6.3.1. Maps of partial individuals mean coordinates on MFA1,2,3 ---------   
  #****************************************************
  # 6.3.1.1. one plot per year -------
  #*****************************
  for (i in 1:length(lyears.sat)){
    dfi=mpindCoord.sat.year[mpindCoord.sat.year$year==lyears.sat[i],]
    row.names(dfi)=dfi$cell
    dfi=dfi[order(as.numeric(dfi$cell)),-seq(2)]-MFAcoord.sat
    dim(MFAcoord)
    head(dfi)
    MFA.rasterPlot(MFAres=dfi,nMFA=2,cellsel,xyCell=xyrbs,path.export=NULL,
                       plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                   MFA123contrib=FALSE,
                                   MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                   MFA123coordMcontrib=FALSE,MFA123pind=FALSE),funSel='mean',
                       ptitle=paste('20',lyears.sat[i],sep=''),lptheme = viridisTheme(),newbathy=FALSE)
  }
  
  # 6.3.1.1. one plot per month -------
  #*****************************
  for (i in 1:length(lmonths.sat)){
    dfi=mpindCoord.sat.month[mpindCoord.sat.month$month==lmonths.sat[i],]
    row.names(dfi)=dfi$cell
    dfi=dfi[order(as.numeric(dfi$cell)),-seq(2)]-MFAcoord.sat
    dim(MFAcoord)
    head(dfi)
    MFA.rasterPlot(MFAres=dfi,nMFA=2,cellsel,xyCell=xyrbs,path.export=NULL,
                   plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                               MFA123contrib=FALSE,
                               MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                               MFA123coordMcontrib=FALSE,MFA123pind=FALSE),funSel='mean',
                   ptitle=paste(lmonths.sat[i],sep=''),lptheme = viridisTheme(),newbathy=FALSE)
  }
  
  # 6.3.2. Maps of partial individuals coordinates anomalies, one mosaic plot per MFA1,2,3, with all years
  #*******************************
  res=MFA.rasterPlot(MFAres=res.sat.MFA,nMFA=2,cellsel,xyCell=xyrbs,path.export=NULL,
                         plotit=list(MFA123coord=TRUE,MFA123contrib=FALSE,MFA123timevar=FALSE,
                                     MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                     MFA123coordMcontrib=FALSE,MFA123pind=TRUE),funSel='mean',
                         ptitle=paste(''),lptheme=BuRdTheme,anomalit=TRUE,newbathy=FALSE)

 
