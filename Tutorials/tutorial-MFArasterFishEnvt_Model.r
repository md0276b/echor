#***************************************************************************************************************************************
#              Fish habitat modeling based on 
#     Multiple Factor Analysis (MFA) of raster time series:
#                   MFA(fish) ~ MFA(hydro)
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.9.3
# V1.0 08/09/2020

library(FactoMineR)
library(EchoR)

# 1. Spatial variability: habitat definition ----------
#******************************* ---------
# 1.1. Get hydro and fish MFA results averaged over time (per cell~indovidual)----
# (MFAtutorial-rasterFish and MFAtutorial-rasterEnvt results must be present in workspace)
res.hydro.MFA.iMFA=data.frame(res.hydro.MFA$ind$coord)
names(res.hydro.MFA.iMFA)=paste(names(res.hydro.MFA.iMFA),'e',sep='.')
res.fssd.MFA.iMFA=data.frame(res.fssd.MFA$ind$coord)
names(res.fssd.MFA.iMFA)=paste(names(res.fssd.MFA.iMFA),'f',sep='.')
head(res.fssd.MFA.iMFA)
res.PELGAS.iMFA.envt.fish=merge(res.fssd.MFA.iMFA,res.hydro.MFA.iMFA,
                                 by.x='row.names',by.y='row.names')
#write.table(res.PELGAS.iMFA.envt.fish,row.names=FALSE)

# 1.2. Visual inspection of fish and hydro MFA relationships ----
plot(res.PELGAS.iMFA.envt.fish$Dim.1.f,res.PELGAS.iMFA.envt.fish$Dim.1.e,
     xlab='MFA1 fish',ylab='MFA1 hydro')
# -> no relationship
plot(res.PELGAS.iMFA.envt.fish$Dim.1.f,res.PELGAS.iMFA.envt.fish$Dim.2.e,
     xlab='MFA1 fish',ylab='MFA2 hydro')
# -> non linear relationship
plot(res.PELGAS.iMFA.envt.fish$Dim.2.f,res.PELGAS.iMFA.envt.fish$Dim.1.e,
     xlab='MFA2 fish',ylab='MFA1 hydro')
plot(res.PELGAS.iMFA.envt.fish$Dim.2.f,res.PELGAS.iMFA.envt.fish$Dim.2.e,
     xlab='MFA2 fish',ylab='MFA2 hydro')
# -> no relationship

# 1.3. Fish MFA1 modelling -----------
# 1.3.1. Check fish MFA1 distribution
hist(res.PELGAS.iMFA.envt.fish$Dim.1.f)
# ->  left skewed, not normal
# Make fish MFA1 strictly positive
res.PELGAS.iMFA.envt.fish$Dim.1.f.pos=res.PELGAS.iMFA.envt.fish$Dim.1.f+4
# Non linear modelling: GAM
library(mgcv)
gam.PELGAS.fMFA1.space=gam(Dim.1.f.pos~s(Dim.1.e)+s(Dim.2.e),
                           family = Gamma(link="log"),
                           data=res.PELGAS.iMFA.envt.fish)
summary(gam.PELGAS.fMFA1.space)
plot(gam.PELGAS.fMFA1.space,pages=1,residuals=TRUE)
par(mfrow=c(2,2))
gam.check(gam.PELGAS.fMFA1.space)
par(mfrow=c(1,1))

gam.PELGAS.fMFA1.space2=gam(Dim.1.f.pos~s(Dim.2.e),
                           family = Gamma(link="log"),
                           data=res.PELGAS.iMFA.envt.fish)
summary(gam.PELGAS.fMFA1.space2)
plot(gam.PELGAS.fMFA1.space2,pages=1,residuals=TRUE)
par(mfrow=c(2,2))
gam.check(gam.PELGAS.fMFA1.space2)
par(mfrow=c(1,1))

# -> hydro MFA1&2 explain 1.3 and 84.6% of the distribution of MFA1 fish communnity 
# (anchovy and spanish mackerel), respectively
# -> anchovy and spanish mackerel spring habitats 
# characterised by higher hydro MFA2, i.e. higher bottom temperature 

# 1.4. Fish MFA2 modelling -----------
# 1.4.1. Check fish MFA1 distribution
hist(res.PELGAS.iMFA.envt.fish$Dim.2.f)
# ->  right skewed, not normal
# Make fish MFA2 strictly positive
res.PELGAS.iMFA.envt.fish$Dim.2.f.pos=res.PELGAS.iMFA.envt.fish$Dim.2.f+8
# General linear modelling: GLM
glm.PELGAS.fMFA2.space=glm(Dim.2.f.pos~Dim.1.e+Dim.2.e,
                            family = Gamma(link="log"),
                            data=res.PELGAS.iMFA.envt.fish)
summary(glm.PELGAS.fMFA2.space)
par(mfrow=c(2,2))
plot(glm.PELGAS.fMFA2.space)
par(mfrow=c(1,1))

glm.PELGAS.fMFA2.space2=glm(Dim.2.f.pos~Dim.1.e,
                           family = Gamma(link="log"),
                           data=res.PELGAS.iMFA.envt.fish)
summary(glm.PELGAS.fMFA2.space2)
par(mfrow=c(2,2))
plot(glm.PELGAS.fMFA2.space2)
par(mfrow=c(1,1))
hist(residuals(glm.PELGAS.fMFA2.space2))

# -> hydro MFA1 explain 38% of the distribution of MFA2 fish communnity 
# (small clupeiforms)
# -> anchovy, sardine and sprat spring habitats
# characterised by lower hydro MFA1, i.e. lower surface salinity 

# 2. Temporal variability----------
#****************************** ------------
# 2.1. Get fish and hydro MFA results averaged over space (per group ~ year) ----
res.hydro.MFA.gMFA=data.frame(res.hydro.MFA$group$coord)
lyearse=as.numeric(paste('20',dimnames(res.hydro.MFA.gMFA)[[1]],sep=''))
names(res.hydro.MFA.gMFA)=paste(names(res.hydro.MFA.gMFA),'e',sep='.')
res.fssd.MFA.gMFA=data.frame(res.fssd.MFA$group$coord)
names(res.fssd.MFA.gMFA)=paste(names(res.fssd.MFA.gMFA),'f',sep='.')
head(res.fssd.MFA.gMFA)
res.PELGAS.gMFA.envt.fish=merge(res.fssd.MFA.gMFA,res.hydro.MFA.gMFA,by.x='row.names',by.y='row.names')
res.PELGAS.gMFA.envt.fish$years=as.numeric(paste('20',row.names(res.hydro.MFA.gMFA),sep=''))

# 2.2. Visual inspection of hydro MFA time series ---------
plot(lyearse,res.hydro.MFA.gMFA[,1],type='l',ylim=c(0,1),ylab='MFA',
     main='Hydro MFA',xlab='')
lines(lyearse,res.hydro.MFA.gMFA[,2],lty=2)
legend('bottomleft',lty=seq(2),legend=c('MFA1','MFA2'))

# -> Hydro MFA1 averaged over whole area stable over time
# -> Hydro MFA2 averaged over whole area dropped after 2007

lm.e1g=lm(Dim.1.e~years,data=res.PELGAS.gMFA.envt.fish)
summary(lm.e1g)
par(mfrow=c(2,2))
plot(lm.e1g)
par(mfrow=c(1,1))

lm.e2g=lm(Dim.2.e~years,data=res.PELGAS.gMFA.envt.fish)
summary(lm.e2g)
par(mfrow=c(2,2))
plot(lm.e2g)
par(mfrow=c(1,1))

# -> No linear trend in hydro MFA time series

# 2.3. Visual inspection of fish MFA time series ---------
lyearsf=as.numeric(paste('20',dimnames(res.fssd.MFA.gMFA)[[1]],sep=''))
plot(lyearsf,res.fssd.MFA.gMFA[,1],type='l',ylim=c(0,1),ylab='MFA',
     main='Fish MFA',xlab='')
lines(lyearsf,res.fssd.MFA.gMFA[,2],lty=2)

# -> Fish MFA1 averaged over whole area stable over time, 
# except for a drop in 2012
# -> Fish MFA2 averaged over whole area stable over time

# 2.4. Modelling fish MFA with hydro MFA averaged over space --------- 
lm.PELGAS.fMFA1.time=lm(Dim.1.f~Dim.1.e+Dim.2.e,res.PELGAS.gMFA.envt.fish)
summary(lm.PELGAS.fMFA1.time)
par(mfrow=c(2,2))
plot(lm.PELGAS.fMFA1.time)
par(mfrow=c(1,1))
# -> global temporal variability of fish MFA1 not explained by hydro MFA1,2 
lm.PELGAS.fMFA2.time=lm(Dim.2.f~Dim.1.e+Dim.2.e,res.PELGAS.gMFA.envt.fish)
summary(lm.PELGAS.fMFA2.time)
par(mfrow=c(2,2))
plot(lm.PELGAS.fMFA2.time)
par(mfrow=c(1,1))
# -> global temporal variability of fish MFA2 not explained by hydro MFA1,2 

# Conclusions
#*********************
# -> when averaged over whole area, no relationship over time 
# between fish MFA1,2 and environment MFA1,2
# -> fish~environment relationships are local, within habitats
