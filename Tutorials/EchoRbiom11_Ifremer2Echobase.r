cruise='PELGAS2018'

library(EchoR)

# 1. Define paths -----------
#****************************
donnees2="/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/"
#donnees2="R:/"

path.export=paste(donnees2,'Campagnes/',cruise,'/EvaluationBiomasse/Resultats/38kHz/Export/',sep='')
#path.export='R:/Campagnes/PELGAS2012/EvaluationBiomasse/R/Rexports/'

# Path to Casino export file
path.cas=paste(donnees2,'Campagnes/',cruise,
               '/EvaluationBiomasse/Donnees/Casino/casino_pelgas2018_evts_final.csv',sep='')

# 2. Check acoustic data -----------
#****************************
mantota2.wide=mantota2.wide38

#laytot=read.table(paste(path.frv,cruise,"_laytot.csv",sep=''),sep=';',header=TRUE) 
head(mantot)  
#mantot=mantot38
#laytot=laytot38
head(laytot)
#rm('laytot')
#survey.poly=pelgas12.poly
#survey.poly$surface=area12.1  

# 3. Create calibration input file ------------
#****************************
PELGAS2017.calibration=data.frame(topiaId='',accuracyEstimate=0.27,
                                  aquisitionMethod='ER60',
                                  comments='Mission PELGAS2017 - 22 avril 2017 - Bertheaume',date='22-04-2017 07:00:00',
                                  processingMethod='ER60',
                                  report='http://madida.ifremer.fr/madida/documents/etalonnage/Pelgas2017_EK80_calibration__v1.png',
                                  acousticInstrument='')

# 4. compute survey polygon by merging regions -----------
#****************************
# if error while calling "areahull" function from alphahull package, correct function:
areaahull=function (x, timeout = 5){
  area <- R.utils::withTimeout(try(areaahulleval(x), silent = TRUE), 
                          timeout = timeout)
  if (!is.numeric(area)) {
    warning("Problem in area computation (Returns NA)")
    area <- NA
  }
  if (is.numeric(area) & area < 0) {
    warning("Problem in area computation (Returns NA)")
    area <- NA
  }
  return(area)
}

IDsCLAS=lfs.shp[lfs.shp$STRATE=='CLAS','ID']
sprCLAS=survey.poly.proc(polys=polys.CLAS,IDs=IDsCLAS,simplify=TRUE,alpha=0.9)
# if "Error: isTRUE(gpclibPermitStatus()) is not TRUE", run:
# install.packages("gpclib", type="source")
# install.packages('rgeos', type='source')
# install.packages('rgdal', type='source')
# if still an error, try increasing alpha
survey.polyCLAS=sprCLAS$survey.poly
poly.dfCLAS=sprCLAS$poly.dfa
poly.dfCLAS$TYPE='CLAS'
poly.dfCLAS$CAMPAGNE=cruise

IDsSURF=lfs.shp[lfs.shp$STRATE=='SURF','ID']
sprSURF=survey.poly.proc(polys=polys.SURF,IDs=IDsSURF,simplify=TRUE,alpha=0.9)
# if error, try increasing alpha
survey.polySURF=sprSURF$survey.poly
plot(survey.polySURF,main='SURF')
par(mfrow=c(1,1))
poly.dfSURF=sprSURF$poly.dfa
poly.dfSURF$TYPE='SURF'
poly.dfSURF$CAMPAGNE=cruise

par(mfrow=c(1,2))
plot(survey.polyCLAS,main='CLAS')
plot(survey.polySURF,main='SURF')
par(mfrow=c(1,1))

poly.df=rbind(poly.dfCLAS,poly.dfSURF[poly.dfSURF$region!=0,])
dfu=unique(poly.df[,c('region','AREA.pbs','TYPE')])
dfa=aggregate(dfu$AREA.pbs,list(region=dfu$region,type=dfu$TYPE),sum)
dfa$polyTot=dfa$region==0
dfa2=aggregate(dfa$x,list(type=dfa$type,polyTot=dfa$polyTot),sum)
dfa2$x[3]/dfa2$x[1]

# 5. Biometry data ------------
#****************************

paramBioANE=paste(donnees2,'Campagnes/',cruise,'/Peche/paramBio/csv/param_bio_ANCHOIS.csv',sep='')
paramBioPIL=paste(donnees2,'Campagnes/',cruise,'/Peche/paramBio/csv/param_bio_SARDINE.csv',sep='')

# 6. Create ancillary instrumentation file -----------
#****************************
ancillary=data.frame(voyage=rep('PELGAS2017',2),vessel=rep('THALASSA II',2),
                     ancillaryInstrumentation=c('Seabird SBE 21 thermo-fluo-salinometer',
                                                'Vaisala MILOS 500 weather station'))

# 7. Maps ----------
#***********
#import gridmap data
gridmaps.db=read.table(paste(donnees2,
                          'Campagnes/PELGAS/Data/gridFiles/gridMapsPELGAS/gridMapsPELGASfishSp2000-2017.csv',sep=''),
                    sep=';',header=TRUE)

head(gridmaps.db)
gridmaps.dbs=gridmaps.db[paste(gridmaps.db$Survey,gridmaps.db$Year,sep='')==as.character(cruise),]
head(gridmaps.dbs)
unique(gridmaps.dbs$Component)
table(gridmaps.dbs$Parameter)
path.fishmap=gridmaps.dbs[gridmaps.dbs$Component=='Fish',]
unique(path.fishmap$Parameter)
path.otherMaps=gridmaps.dbs[gridmaps.dbs$Component!='Fish',]
path.otherMaps=NULL
unique(path.otherMaps$Parameter)

# 8. PELGAS2015 specific args ---------
#********************************************
# 8.1. Voyage, transit, transect codes -----
#******************** -----
# simplified transects? = 1 transect per transit
simplifyVTT=TRUE
# Casino column names used in the function
cas=read.csv2(path.cas,sep='\t',header=TRUE)
cas=read.csv(path.cas,sep='\t',header=TRUE)
casn=read.csv(path.cas,header=FALSE,sep='\t')
casn=casn[1,]
head(cas)
RcasinoNames=data.frame(Rname=names(cas),casName=as.character(t(casn)))
RcasinoNames=RcasinoNames[!is.na(RcasinoNames$casName),]

lcodes=data.frame(codename=c('numStationName','xname','yname','zname','codeCamp','codeTransit','codeInter'),
                  code=c('Num Station','Longitude','Latitude',
                         'Sonde ref. (m)','CAMPAGNE','TRANSIT','INTER'))
# lcodes not in Casino names
lcodes$code[!lcodes$code%in%RcasinoNames$casName]

# Geographical strata
strata='Golfe de Gascogne'
# Reference hauls'suffix
chamens.sufix='.CREF'
# Port names and international codes
portTable=data.frame(name=c('Brest','Lorient','Santander','Concarneau','La Rochelle'),
                     code=c('BSH29','BSH4257','BSH189','BSH4265','BSH108'))
# codes for casino operations on research vessel
sciVesselCode='PEL_'
# Research vessel's name
vesselName='THALASSA II'
# Casino codes for non events (survey metadata)
nonEventsCodes=c('CAMP','')
# Casino codes for comments to be skipped
nonActions=c('NEXT','COM','DCONFIG','FCONFIG','NCONFIG','DPARASITESME70',
             'FPARASITESME70','DPARASITESER60','FPARASITESER60','ECHO',
             'DEBPOI','DEBSSL','FINSSL','FINPOI','INTERAD')
# Transects names prefix (in "STRATE" casino field)
radialeCode='Rad '
# Casino codes of ancillary sensors performing continuous sampling 
surfInstruments=c('CUFIX')
# Casino text export separator
sep.cas='\t'
# Table with casino gear codes (Code.Appareil), operations start (spts) and end (epts) codes to be extracted, 
# with generic gear categories (code.app) and types (gear.type) 
septs.df=data.frame(Code.Appareil=c('ACOU','CUFIX','76x70','57x52','942OBS','EROCENROL',
                                    'MIK','MEPEL','CARRE','MINOF','CAMPAGNE','TRANSIT',
                                    'INTER','932OBS','725OBS','PTM','PS'),
                    spts=c('DEBURAD','STR','DFIL','DFIL','DFIL','DFIL','DEBFIL','DFIL',
                           'DFIL','DEBVIR','DEBUCAMP','DEBULEG','FINRAD','DFIL','DFIL',
                           'DFIL','DFIL'),
                    epts=c('FINRAD','STP','DVIR','DVIR','DVIR','DVIR','FINVIR','DVIR',
                           'FIN','FINVIR','FINCAMP','FINLEG','DEBURAD','DVIR','DVIR',
                           'DVIR','DVIR'),
                    code.app=c('ACOU','HYDROBIO','FISH','FISH','FISH','FISH','FISH',
                               'FISH','FISH','HYDROBIO','CAMPAGNE','CAMPAGNE','ACOU',
                               'FISH','FISH','FISH','FISH'),
                    gear.type=c('ACOU','CUFES','TRAWL','TRAWL','TRAWL','EROCENROL',
                                'MICRON','MICRON','ZOOPL','ZOOPL','CAMPAGNE','CAMPAGNE',
                                'ACOU','TRAWL','TRAWL','TRAWL','TRAWL'),
                    Echobasegearcode=c(NA,NA,'76x70','57x52','PTM','57x52 ENROL','MIK',
                                       'MEPEL','CARRE','MINOF',NA,NA,NA,'69','PS','PTM',
                                       'PS'),
                    importGear=c(FALSE,FALSE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,FALSE,
                                 rep(FALSE,3),rep(TRUE,4)))
# gears to be removed from transects
blApp=c('CUFIX','PROFILEUR','MINOF')
# Check presence of all fields in EchoBase reference tables?
check.ref=TRUE
# database login to check reference tables
host='acoustica'
dbname="Echobase-PELGAS"
user='echobase'
password='echobase'
port=5432

# 8.2. Biotic ----
#******************** -----
# 8.2.1. Import macronekton data -----
path.results=paste(prefix,cruise,'/EvaluationBiomasse/Resultats/38kHz/V1.0/',sep='')
PecheiEB=read.table(paste(path.results,'Pechei.csv',sep=''),header=TRUE,sep=';')
mensEB=read.table(paste(path.results,'mens.csv',sep=''),header=TRUE,sep=';')
LW.EB=read.table(paste(path.results,'LW.csv',sep=''),header=TRUE,sep=';')
PecheiEB[PecheiEB$NOSTA=='W5017',]
# 8.2.2. Add micronekton data ----
# Total samples
PecheiEB.MN=read.table(paste(path.results,'peche.microN.csv',sep=''),header=TRUE,sep=';')
PecheiEB.MN$meanWeight=NA
PecheiEB.MN=dfnames.converter(PecheiEB.MN,template='peche')
PecheiEB=plyr::rbind.fill(PecheiEB,PecheiEB.MN[,])
PecheiEB$CAMPAGNE=cruise
sort(unique(PecheiEB$GENR_ESP))
# Subsamples
mensEB.MN=read.table(paste(path.results,'mens.microN.csv',sep=''),header=TRUE,sep=';')
mensEB.MN=dfnames.converter(mensEB.MN,template='mens')
mensEB=plyr::rbind.fill(mensEB,mensEB.MN)
mensEB$CodEsp2=paste(mensEB$GENR_ESP,mensEB$CATEG,sep='-')
mensEB$CAMPAGNE=cruise
head(mensEB)
# 8.2.3. Check SizeCategory -----
# In total samples
unique(PecheiEB$SIGNEP)
PecheiEB[PecheiEB$SIGNEP=='H','SIGNEP']=0
PecheiEB[is.na(PecheiEB$SIGNEP),'SIGNEP']=0
PecheiEB$CodEsp=paste(PecheiEB$GENR_ESP,PecheiEB$SIGNEP,sep='-')
rac=aggregate.catches(PecheiEB,spname='CodEsp',stname='NOSTA')
PecheiEB=rac$Pechei
PecheiEB$CodEsp=paste(PecheiEB$GENR_ESP,PecheiEB$STRATE,PecheiEB$SIGNEP,sep='-')
# In subsamples
unique(mensEB$CATEG)
mensEB[mensEB$CATEG=='H','CATEG']=0
mensEB[is.na(mensEB$CATEG),'CATEG']=0
mensEB=dfnames.converter(mensEB,template='mens')
# remove juvenile sardine subsamples from micronekton database
mensEB=mensEB[!(mensEB$operationId%in%c('W0255','W0260','W0273')&
                  mensEB$baracoudaCode=='SARD-PIL'&
                  mensEB$round==1),]
mensEB=aggregate.subsamples(mens4Echobase=mensEB)
mensEB=dfnames.converter(x=mensEB,template='mens')
# update species codes
mensEB$CodEsp2=paste(mensEB$GENR_ESP,mensEB$CATEG,mensEB$Lcm,sep='-')
# check duplicated subsamples
mens.id=paste(mensEB$NOSTA,mensEB$GENR_ESP,mensEB$CATEG,mensEB$Lcm)
mens.id=paste(mensEB[!is.na(mensEB$Lcm),'NOSTA'],
              mensEB[!is.na(mensEB$Lcm),'GENR_ESP'],
              mensEB[!is.na(mensEB$Lcm),'CATEG'],
              mensEB[!is.na(mensEB$Lcm),'Lcm'])
sort(mens.id[duplicated(mens.id)])
# 8.2.4. Check species in EB species table -----
library(RPostgreSQL)
drv <- dbDriver("PostgreSQL")
dbListConnections(drv)
dbGetInfo(drv)
con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                 password=password,port=port)
sqlSpecies="SELECT * FROM Species"
species <- dbGetQuery(con,sqlSpecies)
dbDisconnect(con)
viv=unique(PecheiEB$baracoudaCode)%in%species$baracoudacode
if (FALSE%in%(viv)){
  stop('Species ',paste(unique(PecheiEB$baracoudaCode)[!viv],collapse=' '),
       ' not found in Echobase species table. Please check.')
}else{
  cat('All species in total samples input found in Echobase reference table','\n')
}
sort(unique(PecheiEB$GENR_ESP))
# Eventually, export table with missing species
miss.sp=unique(PecheiEB$baracoudaCode)[!viv]
if (length(miss.sp)>0){
  path.tuttisp=paste(path.results,'species.csv',sep='')
  tuttisp=read.table(path.tuttisp,sep=';',header=TRUE)
  tuttisps=tuttisp[tuttisp$Code_Campagne%in%miss.sp,]
  # Check worms codes
  # install.packages("XML", repos = "http://cran.r-project.org", dependencies = TRUE)
  # install.packages(c("XMLSchema", "SSOAP"), repos = c("http://packages.ropensci.org", "http://cran.rstudio.com"))
  # devtools::install_github("ropensci/taxizesoap")
  library(taxizesoap)
  spWormCheck=tuttisps[,'Nom_Scientifique']
  # check species names in Worms database
  wormsi=get_wormsid(searchterm = spWormCheck,accepted = FALSE)
  wormsi.df=data.frame(sp=spWormCheck,wormsCode=c(wormsi),
                       codesp=tuttisps[,'Code_Campagne'])
  # export new species
  write.table(wormsi.df,paste(path.results,'newSpeciesEB.csv',sep=''),
              row.names = FALSE,sep=';')
}
# trawls gear.type
trawlCode='TRAWL'
# Check that all subsamples have total samples? If not compute total samples by aggregating subsamples
TotCatchOpeCor=TRUE
# Acceptable "strate" values for trawl hauls.
# If NULL, acceptable strates will be imported from EchoBase 
strate.ok=NULL
# Remove total catch data from export files if they cannot be corrected?
removeBadCatch=FALSE
# try to correct null or NA weight at lengths?
correct.nullWatLength=TRUE
# remove subsamples without weightatlength?
removeBadSubSamples=FALSE
# Casino column names in R format for gear metadata
names(cas)
cnames=c("Code.Action","Date","Strate","Observation","Num.Station","Heure",
         "Sonde.ref...m.","Latitude","Longitude","Code.Appareil",
         "FUNES.L..filee.Tribord..m.","CINNA.Vit..fond..nd.",
         "MARPORT.Ouv..verticale..m.","MARPORT.Ouv..horizontale..m.",
         "MARPORT.Altitude.corde.d.eau...fond..m.",
         "FUNES.L..filee.Babord..m.","FUNES.Tension.babord..daN.","CINNA.Cap..deg.",
         "MARPORT.Vit..longitudinale..m.s.","CINNA.Cap.derive..deg.",
         "CINNA.Vitesse.derive..nd.","METBA.Dir..vent.vrai..deg.",
         "METBA.Vit..vent.vrai..nd.")
# missing names
cnames[!cnames%in%names(cas)]

# 8.3. Acoustic ----
#******************** ----
lay.format='csv'
milli.correct=TRUE
neg.nasc.remove=TRUE
CorrectChannelIdent=NULL
beamGainCor='frequency'
beamSACor='frequency'
powerCor=NULL
algAsensCor='frequency'
atwAsensCor='frequency'
areaCor=NULL
volCor=NULL
CorrectAbsorption='frequency'
CorrectSoundCelerity='frequency'
validChannels=data.frame(id=c(seq(46,51,1),seq(91,111,1),136),type=c(rep('mono',6),rep('multi',length(seq(91,111,1))),'mono'),
                         sel=c(FALSE,TRUE,rep(FALSE,26)))
convertMan2lay=TRUE
laytot=NULL

# 9. Export survey data in EchoBase format -------------
#**************************** -----
# Process all data

ifremeR2Echobase(simplifyVTT=simplifyVTT,path.cas=path.cas,path.export=path.export,
                 PecheiEB=PecheiEB,mensEB=mensEB,
                 laytot=laytot,LW=LW.EB,LA=LA,paramBioANE=paramBioANE,paramBioPIL=paramBioPIL,
                 EspDev=EspDev,ESDUDEVs=ESDUDEVs,calibration=PELGAS2017.calibration,
                 chamens.long=chamens.long,mantot=mantot,biomres=biomres,
                 biomres.pond=biomres.pond,Biomres.echotype.codesp.size=Biomres.echotype.codesp.size,
                 Biomres.sp.age=Biomres.sp.age,path.fishmap=path.fishmap,path.otherMaps=path.otherMaps,
                 survey.polys=poly.df,CV.df=CV.df,Ind.sp.all=Ind.sp.all,deszones=deszones,
                 ancillary=ancillary,
                 cruise=cruise,convertMan2lay=convertMan2lay,lcodes=lcodes,
                 sep.cas=sep.cas,sciVesselCode=sciVesselCode,vesselName=vesselName,
                 nonEventsCodes=nonEventsCodes,nonActions=nonActions,radialeCode=radialeCode,
                 surfInstruments=surfInstruments,septs.df=septs.df,cnames=cnames,
                 blApp=blApp,vir.codes=vir.codes,strate.ok=strate.ok,lspcodes=lspcodes,
                 removeBadCatch=removeBadCatch,
                 fil.codes=fil.codes,gear.codes=gear.codes,CorrectChannelIdent=CorrectChannelIdent,
                 beamGainCor=beamGainCor,beamSACor=beamSACor,powerCor=powerCor,algAsensCor=algAsensCor,
                 atwAsensCor=atwAsensCor,areaCor=areaCor,volCor=volCor,CorrectAbsorption=CorrectAbsorption,
                 CorrectSoundCelerity=CorrectSoundCelerity,check.ref=check.ref,TotCatchOpeCor=TotCatchOpeCor,
                 neg.nasc.remove=neg.nasc.remove,host=host,dbname=dbname,user=user,password=password,
                 port=port,exceptGear=exceptGear,validChannels=validChannels)

# OR process only VTT and total and sub-samples data

ifremeR2Echobase(simplifyVTT=TRUE,path.cas=path.cas,path.export=path.export,
                 PecheiEB=PecheiEB,mensEB=mensEB,
                 laytot=NULL,LW=LW.EB,LA=NULL,paramBioANE=NULL,paramBioPIL=NULL,
                 EspDev=NULL,ESDUDEVs=NULL,calibration=NULL,
                 chamens.long=NULL,mantot=NULL,biomres=NULL,
                 biomres.pond=NULL,Biomres.echotype.codesp.size=NULL,
                 Biomres.sp.age=NULL,path.fishmap=NULL,path.otherMaps=NULL,
                 survey.polys=NULL,CV.df=NULL,Ind.sp.all=NULL,deszones=NULL,
                 casino.check=FALSE,ancillary=NULL,
                 cruise=cruise,convertMan2lay=convertMan2lay,lcodes=lcodes,
                 sep.cas=sep.cas,sciVesselCode=sciVesselCode,vesselName=vesselName,
                 nonEventsCodes=nonEventsCodes,nonActions=nonActions,radialeCode=radialeCode,
                 surfInstruments=surfInstruments,septs.df=septs.df,cnames=cnames,
                 blApp=blApp,strate.ok=strate.ok,
                 removeBadCatch=removeBadCatch,removeBadSubSamples=removeBadSubSamples,
                 correct.nullWatLength=correct.nullWatLength,
                 CorrectChannelIdent=CorrectChannelIdent,beamGainCor=beamGainCor,beamSACor=beamSACor,
                 powerCor=powerCor,algAsensCor=algAsensCor,
                 atwAsensCor=atwAsensCor,areaCor=areaCor,volCor=volCor,CorrectAbsorption=CorrectAbsorption,
                 CorrectSoundCelerity=CorrectSoundCelerity,check.ref=check.ref,TotCatchOpeCor=TotCatchOpeCor,
                 neg.nasc.remove=neg.nasc.remove,host=host,dbname=dbname,user=user,password=password,
                 port=port,validChannels=validChannels)





# import discarded samples

dts=read.table(paste(path.export,cruise,'_totalSamplesDiscarded.csv',sep=''),header=TRUE,sep=';')
head(dts)
kts=read.table(paste(path.export,cruise,'_totalSamples4Echobase.csv',sep=''),header=TRUE,sep=';')
head(kts)
kts2=dfnames.converter(kts)
dts=dfnames.converter(dts[,names(kts2)])
head(dts)
write.table(dts,paste(path.export,cruise,'_totalSamplesDiscarded4Echobase.csv',sep=''),
            row.names=FALSE,sep=';')
