#*******************************************************************************************************
# MAPPING: BLOCK AVERAGING AND CONVERSION TO RASTER FORMAT
#*******************************************************************************************************

# 0. To install echor package, see: 
# https://forge.ifremer.fr/plugins/mediawiki/wiki/echor/index.php/Help#EchoR_easy_install

# Load package
#****************************************************
library(EchoR)

# 1. Set paths ----------
#****************************************************
# Path to raw data
#path.data='D:/R/echor/EchoRpackage/EchoR/data/gridMapDataTest.txt'
# Path to gridmap folder
path.grids=getwd()
path.grids='C:/Temp/'

# 2. Import raw data ----
#************************************
# rawData=read.table(path.data,header=TRUE)
# or use the built in test dataset:
# 
data("gridMapDataTest")
# 
rawData=gridMapDataTest
head(rawData)

# 3. Make gridMaps (1)
# with default polygon for WGACEGG spring surveys--------
#****************************************************
# Select variable to be mapped
# column name in input dataframe
vart='NASC'
# variable name to be displayed in gridmaps
varname='NASC'

# 3.1. Define grid, polygon and mask --------------
# (default: WGACEGG08 GRID and MASK: 
# generic, no change values for xg,yg,ni,u)
#******************************************
# Define grid limits
x1=-10.2;x2=-1;y1=35.8;y2=49
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius (in number of cells)
u=1
# Define iteration number
ni=200

define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,shelf=TRUE,
                      poligonit=FALSE)

# To define a polygon in which to make the gridmaps,
# put polygon longitudes in a xpol vector and 
# polygon latitudes in ypol vector
# if no polygon is provided, the default WGACEGG polygon is used
# see define.grid.poly.mask help page for details and examples

data(PELGASpolygon)
xpol=PELGASpolygon$x
ypol=PELGASpolygon$y

# Plot grid and polygon
plot(matxg,matyg,main='Defined grid and polygon',
     xlab='',ylab='')
#add polygon
sel=inout(pts=cbind(as.vector(matxg),as.vector(matyg)),
          poly=cbind(xpol,ypol),bound=T,quiet=T)
lines(xpol,ypol)
coast()
points(matxg[sel],matyg[sel],pch=16)
legend('topleft',c('Discarded','Selected'),pch=c(1,16))

# 3.2. Block averaging procedure per species and survey (year) -----
# grid files and maps are exported in the "path.grid" folder
# the tname, xname, yname, sname and spname column names are required
# see the gridNplot function help page for details
#*************************************************

res1=gridNplot(df=rawData,vart=vart,varname=varname,
          tname='year',xname='x',yname='y',sname='survey',
          path.grids=path.grids,p=0.98,spname='sp',
          zfilter="quantile",newbathy=FALSE,
          default.grid=FALSE,deep=-450,shallow=-50,bstep=450,
          bcol='grey50',drawlabels=TRUE,cex.labs=2,tcls=-0.3,
          lon1=x1,lon2=x2,lat1=y1,lat2=y2,xycor=FALSE,mini=-1,maxi=1,
          centerit=FALSE)

# 4. List and bind grid files and convert to rasters objects-----------
#*********************************************************
# 4.1. List the grid files in "path.grids" folder
#************************************
lffssd=list.files(path.grids,pattern='*.txt')
#lfs=strsplit(lf,split='[.]',)
lffssds=data.frame(t(data.frame(strsplit(lffssd,split='[.]'))))
lp=as.character(lffssds[,1])
lffssds2=data.frame(t(data.frame(strsplit(lp,split='_'))))
row.names(lffssds2)=seq(length(lffssds2[,1]))
lffssds=cbind(lffssds,lffssds2)
head(lffssds)
names(lffssds)=c('pat','ext','method','sp','var','filter')
lffssds$path=lffssd
row.names(lffssds)=seq(length(lffssds[,1]))
lyears=NULL

# 4.2. Import files and bind maps
#************************************
# Eventually define a subset of years to process
# in "lyears" vector
lyears=2015:2016
if (is.null(lyears)){
  lyears=unique(rawData$year)
}
# Define the name of the merged dataset:
nname='AC_SPRING_IBBB'

for (i in 1:dim(lffssds)[1]){
  pat=paste(path.grids,lffssds$path[i],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  mati$sp=lffssds$sp[i]
  #select surveys in lyears
  mati=mati[mati$Year%in%lyears,]
  head(mati)
  mati$Survey=nname
  if (i==1){
    rasterDB=mati
  }else{
    rasterDB=rbind(rasterDB,mati)
  }
  # convert gridfiles to raster stack format, 
  # compute mean and SD maps and plot everything:
  # see the grid2rasterStack function help page for details
  resras=grid2rasterStack(pat=mati,path1=path.grids,
                          varid=paste(lffssds$sp[i],
                                      lffssds$var[i],
                                      sep=' '),
                          anomaly=TRUE)
} 

# 5. Make gridMaps (2) with custom polygon ------
#****************************************************
data("gridMapDataTest")
rawData=gridMapDataTest
head(rawData)

# 5.2. Define grid -----
x1=-6;x2=-1;y1=43;y2=49
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=2
# Define iteration number
ni=200
# Define grid
# NB: set shelf and poligonit arguments to FALSE to keep the grid unaltered
define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,shelf=FALSE,
                      poligonit=FALSE)

# 5.3. Define custom polygon ----
#*****************
# Only cells within the polygon will be retained in the grid maps
# 5.3.1. Option 1: define polygon manually: -----
#*****************
dfu=unique(rawData[,c('x','y')])
# open a new graphical device to be able to use the "locator" function in it:
x11()
# plot the data:
plot(dfu$x,dfu$y)
coast()
# use the "locator" function to draw the polygon and store it in list 'p'
p=locator(type='l')
# Store the coordinates of list p in 'xpol' and 'ypol' vectors
# xpol and ypol are used in the block averaging procedure to define polygon vertices
xpol=p$x
ypol=p$y

# 5.3.2. Option 2: define polygon automatically, using alphahull package: -----
#*****************
dfu=unique(rawData[,c('x','y')])
library(alphahull)
# The ahull function calculates the alpha-convex hull (bounding polygon) 
# of a set of points:
apoly=ahull(dfu$x,dfu$y,alpha=.7)
# check polygon:
plot(apoly)
coast()
indx=apoly$arcs[,"end1"]
ahpoints <- dfu[indx,]                  # extract the boundary points from XY
ahpoints <- rbind(ahpoints,ahpoints[1,])   # add the closing point
plot(dfu$x,dfu$y)
lines(ahpoints$x,ahpoints$y)
# Store the coordinates of list p in 'xpol' and 'ypol' vectors
# xpol and ypol are used in the block averaging procedure to define polygon vertices
xpol=ahpoints$x
ypol=ahpoints$y
# Plot grid and polygon
plot(matxg,matyg,main='Defined grid and polygon',
     xlab='',ylab='')
points(dfu$x,dfu$y,pch=2,col='grey50')
#add polygon
sel=inout(pts=cbind(as.vector(matxg),as.vector(matyg)),
          poly=cbind(xpol,ypol),bound=T,quiet=T)
lines(xpol,ypol)
coast()
points(matxg[sel],matyg[sel],pch=16)
legend('topleft',c('Discarded','Selected','Samples'),pch=c(1,16,2))
# Select variable to be mapped
# column name in input dataframe
vart='NASC'
# variable name to be displayed in gridmaps
varname='NASC'
# 5.4. Block average with custom polygon -----
# plot data, with NOAA bathymetry. Outputs (txt and images) are stored on disk
res1=gridNplot(df=rawData,vart=vart,varname=varname,
               tname='year',xname='x',yname='y',sname='survey',
               path.grids=path.grids,p=0.98,spname='sp',
               zfilter="quantile",newbathy=FALSE,
               default.grid=FALSE,deep=-450,shallow=-50,bstep=450,
               bcol='grey50',drawlabels=TRUE,cex.labs=2,tcls=-0.3,
               lon1=x1,lon2=x2,lat1=y1,lat2=y2)

# 6. Tip for block averaging non biological data ----
#**************
# the gridNplot function requires a 'spname' column in input dataframe
# if your dataset do not contain species, create a dummy 'species' column
# and pass its name as 'spname' to the function

# 7. Plotting gridmaps
# *************************
# 7.1. generate fake raw data
for (i in 1:4){
  x=runif(100,-4,-2)
  y=runif(100,44,47)
  z=rnorm(100,45,10)
  # Format input dataframe
  dfi=data.frame(LONG=x,LAT=y,z,CodEsp='sptest',
                 year=2000+i,CAMPAGNE='ctest')
  if (i==1){
    df=dfi
  }else{
    df=rbind(df,dfi) 
  }
}
lyears=unique(df$year)
# plot data
par(mfrow=c(2,2))
for (i in 1:length(lyears)){
  dfi=df[df$year==lyears[i],]
  plot(dfi$LONG,dfi$LAT,cex=z/30,xlab='',ylab='',
       xlim=c(-6,-1),ylim=c(43,49),main=unique(dfi$year))
  coast()
}
par(mfrow=c(1,1))

path.grids=paste(getwd(),'/',sep='')

# 7.2. Define grid limits
x1=-6;x2=-1;y1=43;y2=49
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=2
# Define iteration number
ni=200
# Define grid
define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,
                      poligonit=FALSE)

# 7.3. Block average and plot data. Outputs (txt and images) 
# are stored on disk. 
# Set 'bathy.plot' to TRUE to add NOAA bathymetry using marmap package
test=gridNplot(df,vart='z',varname='vtest',
               path.grids=path.grids,bathy.plot=FALSE,
               deep=-450,shallow=-50,bstep=450,
               bcol='grey50',drawlabels=TRUE)

# 7.4. Option 1: plot gridded data from file saved on disk: mean stedv 
# and no. of data in cells maps and mosaic plots
# Set 'bathy.plot' to TRUE to add NOAA bathymetry.

res=grid.plot(pat=paste(getwd(),'/grid_sptest_vtest_Filternone.txt',sep=''),
          input='gridFile',
          ux11=TRUE,pat2=NULL,namz='vtest',nams='stdev',namn='no of samples',
          deep=-450,shallow=-50,bstep=450,bcol='grey50',drawlabels=TRUE,
          default.grid=FALSE,bathy.plot=FALSE,
          plotit=list(zmean=TRUE,zstd=TRUE,nz=TRUE,plot1=TRUE,
                      mosaic=TRUE))

# Option 2: plot gridded data in gridDataframe format: mean stedv and 
# no. of data in cells maps

# Extract gridded data in gridDataframe format 
gridDataframe=test$gridDataframe
head(gridDataframe)  

# 7.5. Mean, standard deviation and no. of samples yearly maps, without mosaic ggplots:
# Set 'bathy.plot' to TRUE to add NOAA bathymetry.

mat=grid.plot(pat=gridDataframe,input='gridDataframe',ux11=TRUE,pat2=NULL,
              namz='vtest',nams='stdev',namn='no of samples',
              deep=-450,shallow=-50,bstep=450,bcol='grey50',drawlabels=TRUE,
              default.grid=FALSE,bathy.plot=FALSE,
              plotit=list(zmean=TRUE,zstd=TRUE,nz=TRUE,plot1=TRUE,
                          mosaic=FALSE))

# 7.6. To plot only mosaic ggplots of mean Z, Z stedv and Nsamples:
# Set 'bathy.plot' to TRUE to add NOAA bathymetry.

mat=grid.plot(pat=gridDataframe,input='gridDataframe',ux11=TRUE,pat2=NULL,
              namz='vtest',nams='stdev',namn='no of samples',deep=-450,shallow=-50,
              bstep=450,bcol='grey50',drawlabels=TRUE,default.grid=FALSE,
              bathy.plot=FALSE,
              plotit=list(zmean=TRUE,zstd=TRUE,nz=TRUE,plot1=FALSE,
                          mosaic=TRUE))

# If plotit$mosaic==TRUE, the function returns a dataframe with all yearly maps 
# and average and SD maps of mean, SD and Nsamples:
# Plot mean z and no. of samples in cells
names(mat)
# Extract dataframe:
gmdf.msd=mat$gmdf.msd.meanZ$gmdf.msd
head(gmdf.msd)
# Select mean maps:
unique(gmdf.msd$Year)
gmdf.msd.mean=gmdf.msd[gmdf.msd$Year=="mean.2001-2004",]
# Plot mean, stdev and number of samples maps, averaged over all years:
resi=grid.plot(pat=gmdf.msd.mean,input='gridDataframe',ux11=TRUE,
               bathy.plot=FALSE,namz='vtest',nams='stdev',namn='no of samples',
               plotit=list(zmean=TRUE,zstd=TRUE,nz=TRUE,plot1=TRUE,mosaic=FALSE))

# Provide path in pat2 argument and set ux11 to TRUE to save plots in pat2: 

mat=grid.plot(pat=gridDataframe,input='gridDataframe',ux11=TRUE,pat2=path.grids,
              namz='mean',nams='stdev',namn='no of samples',
              bathy.plot=FALSE,
              deep=-450,shallow=-50,bstep=450,bcol='grey50',drawlabels=TRUE,
              default.grid=FALSE)

# 7.7. Option 3: plot gridded data in Echobase format: mean stedv 
# and no. of data in cells maps

# Convert grid dataframe to Echobase format
EchobaseDF=data.frame(x=gridDataframe$Xgd,y=gridDataframe$Ygd,
                      value.meanMapcellBiomass=gridDataframe$Zvalue,
                      value.stdevMapcellBiomass=gridDataframe$Zstdev,
                      value.NsampleMapcell=gridDataframe$Nsample,
                      voyage=gridDataframe$Survey,year=gridDataframe$Year)

mat=grid.plot(pat=EchobaseDF,input='Echobase',ux11=TRUE,pat2=NULL,namz='mean',
              nams='stdev',namn='no of samples',deep=-450,shallow=-50,bstep=450,
              bcol='grey50',drawlabels=TRUE,default.grid=FALSE,
              bathy.plot=FALSE)

# display the legend in the upper right corner              
mat=grid.plot(pat=EchobaseDF,input='Echobase',ux11=TRUE,pat2=NULL,namz='mean',
              nams='stdev',namn='no of samples',deep=-450,shallow=-50,bstep=450,
              bcol='grey50',drawlabels=TRUE,
              default.grid=FALSE,splot=c(.88,.89, .71,.91),bathy.plot=FALSE,
              plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                          mosaic=FALSE))

# 7.8. Discrete z case (e.g. cluster belongings)
#*****************************
# generate fake discrete data
x=rnorm(100,-4,1)
y=rnorm(100,46.1,1)
z=replicate(100,sample(seq(6),1))

# plot data
plot(x,y,cex=z/10)
coast()

# Format input dataframe
df=data.frame(LONG=x,LAT=y,z,CodEsp='sptest',year=2000,CAMPAGNE='test')
path.grids=getwd()

# Define grid limits
x1=-6;x2=-1;y1=43;y2=49
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=2
# Define iteration number
ni=200
# Define grid
define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,poligonit=FALSE)

testd=gridNplot(df,vart='z',varname='vtestd',path.grids=path.grids,
               bathy.plot=FALSE,discrete=TRUE,
               deep=-450,shallow=-50,bstep=450,bcol='grey50',drawlabels=TRUE)

# Plot discrete data from file
mat=grid.plot(pat=paste(getwd(),'/grid_sptest_vtestd_Filternone.txt',sep=''),
          input='gridFile',ux11=TRUE,pat2=NULL,namz='mean',nams='stdev',
          namn='no of samples',
          deep=-450,shallow=-50,bstep=450,bcol='grey50',drawlabels=TRUE,
          default.grid=FALSE,bathy.plot=FALSE,
          plotit=list(zmean=TRUE,zstd=TRUE,nz=TRUE,plot1=TRUE,
                      mosaic=FALSE))

# Extract gridded data in gridDataframe format 
gridDataframed=testd$gridDataframe
head(gridDataframed)  

# Plot gridded data in gridDataframe format: mean stedv and 
# no. of data in cells maps

# mean, standard deviation and no. of samples yearly maps, with mosaic ggplots:
# Set 'bathy.plot' to TRUE to add NOAA bathymetry.

mat=grid.plot(pat=gridDataframed,input='gridDataframe',ux11=TRUE,pat2=NULL,
              namz='vtest',nams='stdev',namn='no of samples',
              deep=-450,shallow=-50,bstep=450,bcol='grey50',drawlabels=TRUE,
              default.grid=FALSE,bathy.plot=FALSE,
              plotit=list(zmean=TRUE,zstd=TRUE,nz=TRUE,plot1=FALSE,
                          mosaic=TRUE))
}

