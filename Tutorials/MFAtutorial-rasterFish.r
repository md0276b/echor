#***************************************************************************************************************************************
#           Multiple Factor Analysis with FactoMineR tutorial
#           Marine fish biomass raster time series case study
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.9.3
# V1.0 08/09/2020

# 0. Introduction ------
# *************** -------
# Tutorial on Multiple Factor Analysis (MFA) 
# of marine fish biomass raster time series with FactoMineR

# Objectives: 
# ***************
# spatio-temporal analysis of raster (gridded maps) time series
# i) defining areas with stable fish communities over time
# ii) assessing eventual temporal trends/atypical years in fish community distribution

# References: 
# ***************
# Doray et al. (2018) https://archimer.ifremer.fr/doc/00411/52236/
# Pagès, J., 2014. Multiple Factor Analysis by Example Using R, Chapman and Hall/CRC.

# 1. Set up -----------
# ****************** ----------
# 1.1. Load libraries ---------

library(EchoR)
library(FactoMineR)
library(rasterVis)
library(latticeExtra)
library(vegan)
library(maps)
library(maptools)
library(sp)
library(viridisLite)
library(ggplot2)
library(scales)
library(NbClust)
library(factoextra)
library(gridExtra)

# 1.2. Import gridmap data ----
# *********************************************************** ----
# Gridmap data processed using this methodology by
# Doray et al. (2018) # can be downloaded at: 
# SEANOE: https://doi.org/10.17882/53389

# Set paths
#***********************
# path to folder with gridmap data
ddrive='~/Documents/Data/PELGAS/'
# path to grid files
#***********************  
path.grids=paste(ddrive,"gridMapsPELGAS2000-2015.csv",sep='')
# Results path
#************************
path.export.fish=paste(ddrive,'MFAresults/Fish/',sep='')
dir.create(path.export.fish,showWarnings=FALSE)

# Import files 
gridmaps=read.table(file=path.grids,header=T,sep=";")
head(gridmaps)
#Select adult fish biomass gridmaps
unique(gridmaps$Component)
fish.gridmaps=gridmaps[gridmaps$Component%in%
                          c("Fish")&!gridmaps$Parameter%in%
                         c("Ee10m3","Sp10m3","O10m3"),]
unique(fish.gridmaps$Parameter)
head(fish.gridmaps)

# => fish biomass is split by:
# i) species ('GENU-SPE')
# ii) depth stratum ('Bottom': near seabed; 'Surface': near surface)
# iii) size class (cm)

# Add cell IDs
Idf=data.frame(X=sort(unique(fish.gridmaps$X)))
Idf$I=seq(dim(Idf)[1])
Jdf=data.frame(Y=sort(unique(fish.gridmaps$Y)))
Jdf$J=seq(dim(Jdf)[1])
FishBiomassSpSizeDepth=merge(fish.gridmaps,Idf,by.x="X",by.y="X")
FishBiomassSpSizeDepth=merge(FishBiomassSpSizeDepth,Jdf,by.x="Y",by.y="Y")
FishBiomassSpSizeDepth$cell=paste(FishBiomassSpSizeDepth$I,
                                  FishBiomassSpSizeDepth$J,sep='-')
# Adjust column names
names(FishBiomassSpSizeDepth)[names(FishBiomassSpSizeDepth)=='X']="Xgd"
names(FishBiomassSpSizeDepth)[names(FishBiomassSpSizeDepth)=='Y']="Ygd"
names(FishBiomassSpSizeDepth)[names(FishBiomassSpSizeDepth)=='Zmean']="Zvalue"
names(FishBiomassSpSizeDepth)[names(FishBiomassSpSizeDepth)=='Parameter']="sp"
head(FishBiomassSpSizeDepth)

# 2. Prepare data for MFA ------------------------
#*************************************************************** -------
  # 2.1. Select common species -------------
  #******************************
  head(FishBiomassSpSizeDepth)
  unique(FishBiomassSpSizeDepth$Year)
  FishBiomassSpSizeDepth$spi=substr(FishBiomassSpSizeDepth$sp,1,8)
  FishBiomassSpSizeDepthp=FishBiomassSpSizeDepth[FishBiomassSpSizeDepth$Zvalue>0&!is.na(FishBiomassSpSizeDepth$Zvalue),]
  # Species occurence
  table(FishBiomassSpSizeDepthp$spi,FishBiomassSpSizeDepthp$Year)
  OccurenceFrequencySp=apply(table(FishBiomassSpSizeDepthp$spi,FishBiomassSpSizeDepthp$Year)>0,1,sum)/length(unique(FishBiomassSpSizeDepthp$Year))
  sort(OccurenceFrequencySp)
  # Select common species
  FishBiomassSpSizeDepths=FishBiomassSpSizeDepth[FishBiomassSpSizeDepth$spi%in%c('ENGR-ENC','SARD-PIL','SPRA-SPR','MICR-POU',
                                                                     'SCOM-SCO','SCOM-JAP','TRAC-TRU','TRAC-MED',
                                                                             'CAPR-APE'),]
  head(FishBiomassSpSizeDepths)
  
  # 2.2. Select years ---------------
  #***************************
  FishBiomassSpSizeDepths=FishBiomassSpSizeDepths[FishBiomassSpSizeDepths$Year!=2016,]
  sort(unique(FishBiomassSpSizeDepths$Year))
  head(FishBiomassSpSizeDepths)

  # 2.3. Select cells to process --------------------
  #*******************************
  pipo=FishBiomassSpSizeDepth[FishBiomassSpSizeDepth$Year==2004&
                                FishBiomassSpSizeDepth$sp=="ENGR-ENC Surface (10,15]",]
  dim(pipo)
  plot(pipo$Xgd,pipo$Ygd)
  plot(pipo$Xgd,pipo$Ygd,cex=0.1+pipo$Zvalue/10)
  data("PELGASpolygon")
  poly=PELGASpolygon
  lines(poly)
  coast()
  library(splancs) 
  # Select cells in PELGAS polygon
  pts=as.points(pipo$Xgd,pipo$Ygd)
  testInPoly=inout(pts,poly)
  plot(pts)
  points(pts[testInPoly,],pch=16)
  coast()
  cellsel=data.frame(x=pts[,1],y=pts[,2],sel=testInPoly,I=pipo$I,J=pipo$J)
  cellsel$cell=paste(cellsel$I,cellsel$J,sep='-')
  head(cellsel)
  cellsel[cellsel$y>48,"sel"]=FALSE
  cellsel[cellsel$x<(-5),"sel"]=FALSE
  cellsel[cellsel$x>(-1.2),"sel"]=FALSE
  cellsel[cellsel$cell=='8-20',"sel"]=FALSE
  plot(cellsel$x,cellsel$y)
  points(cellsel[cellsel$sel,'x'],cellsel[cellsel$sel,'y'],pch=16)
  coast()
  legend('topleft',legend=c('Cells centers','Selected cells'),pch=c(1,16))
  
  # 2.4. Reshape data frame for analysis with year as grouping variable --------------
  # individuals (rows) = grid cells
  # variables (columns) = parameter values recorded in grid cells (fish biomass)
  # groups (table) = a year (survey)
  #***************************
  a.fssd <- reshape(FishBiomassSpSizeDepths[,c("I","J","Zvalue","sp","Year")], timevar = "sp", idvar = c("I","J","Year"), direction = "wide")
  head(a.fssd)
  names(a.fssd)
  a.fssd=a.fssd[order(a.fssd$Year),]
  
  big.fssd <- reshape(a.fssd, timevar = "Year", idvar = c("I","J"), direction = "wide")
  head(big.fssd)
  names(big.fssd)
  dim(big.fssd)

  # 2.5. Prepare and standardise dataset ------------------
  #*************************************
  names(big.fssd)
  big.fssd.MFA=big.fssd[,-seq(2)]
  row.names(big.fssd.MFA)=paste(big.fssd$I,big.fssd$J,sep='-')
  # simplify column names
  names(big.fssd.MFA)=gsub('Zvalue.','',names(big.fssd.MFA))
  names(big.fssd.MFA)
  dim(big.fssd.MFA)
  # # convert NA to zero
  NA2null=function(x,procInf=FALSE){
    x[is.na(x)]=0
    if (procInf){
      x[is.infinite(x)]=0
    }
    x
  }
  big.fssd.MFA=apply(big.fssd.MFA,2,NA2null)
  dim(big.fssd.MFA)
  # log variables to reduce skewness
  big.fssd.MFAs=apply(big.fssd.MFA,2,function(x){x=log(x+1);x})
  dim(big.fssd.MFAs)
  dimnames(big.fssd.MFAs)[[1]]=dimnames(big.fssd.MFA)[[1]]
  # center and scale variables
  big.fssd.MFAs=apply(big.fssd.MFAs,2,scale,center=TRUE,scale=TRUE)
  dim(big.fssd.MFAs)
  dimnames(big.fssd.MFAs)[[1]]=dimnames(big.fssd.MFA)[[1]]
  
  # 2.6. Select cells in survey area before MFA ---------
  #**********************************************
  big.fssd.MFAs=big.fssd.MFAs[order(dimnames(big.fssd.MFAs)[[1]]),]
  cellsel=cellsel[order(cellsel$cell),]
  
  big.fssd.MFAs=big.fssd.MFAs[cellsel$sel,]
  dim(big.fssd.MFAs)

  # 2.7. Remove constant columns ------------ 
  #************************************
  # with zeroes
  scols=apply(big.fssd.MFAs,2,sum,na.rm=TRUE)
  srows=apply(big.fssd.MFAs,1,sum,na.rm=TRUE)
  big.fssd.MFAs2=big.fssd.MFAs[,scols!=0]
  sum(scols==0)
  scols2=apply(big.fssd.MFAs2,2,sum,na.rm=TRUE)
  summary(scols2)
  # other constant columns
  vcols=apply(big.fssd.MFAs2,2,var,na.rm=TRUE)
  ccols=dimnames(big.fssd.MFAs2)[[2]][vcols==0]
  big.fssd.MFAs2=big.fssd.MFAs2[,!dimnames(big.fssd.MFAs2)[[2]]%in%ccols]
  dim(big.fssd.MFAs2)
  
  # 2.8. Generate no. of data in each group -----------
  #*************************************
  cwz=dimnames(big.fssd.MFAs2)[[2]]
  nct=unlist(strsplit(cwz,split='[.]'))[seq(1,2*length(cwz),2)]
  nyears=unlist(strsplit(cwz,split='[.]'))[seq(2,2*length(cwz),2)]
  tnnc=table(nct,nyears)
  tnnc.df=data.frame(table(nct,nyears))
  stnnc=apply(tnnc,2,sum)
  head(tnnc.df)
  tnnc.dfa=aggregate(tnnc.df$Freq,list(nct=tnnc.df$nct),sum)
  summary(tnnc.dfa$x)


# 3. Perform MFA --------------
#***************************************************** -----------
  # 3.1. MFA ----------
  #*************************************
  res.fssd.MFA<-MFA(big.fssd.MFAs2, group=stnnc, 
                    type=rep("c",length(stnnc)), 
                    ncp=61, 
                    name.group=c("00", "01", "02", "03", "04", "05","06",
                                 "07", "08", "09","10", "11", "12", "13","14","15"), 
                    num.group.sup=NULL, graph=TRUE)
  names(res.fssd.MFA)
  
  # 3.2. Explained variance -----------
  #*******************************  
  summary(res.fssd.MFA)  
  par(bg='white')
  barplot(res.fssd.MFA$eig[,2],main="% of variance explained",names.arg=1:nrow(res.fssd.MFA$eig))
  barplot(res.fssd.MFA$eig[,3],main="Cumulative % of variance explained",names.arg=1:nrow(res.fssd.MFA$eig))
  res.fssd.MFA$eig[round(res.fssd.MFA$eig[,3])==95,]
  dim(big.fssd.MFAs2)
 
  # 3.3. Plots of individuals in MFA1-2-3 planes --------------
  #********************************************
  # Plot of individuals in MFA1-2 plane 
  p1 = fviz_mfa_ind(res.fssd.MFA, col.ind = "contrib") #with contribution in color scale
  p2 = fviz_mfa_ind(res.fssd.MFA, col.ind = "cos2") #with quality of representation in color scale
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA2-3 plane
  p1 = fviz_mfa_ind(res.fssd.MFA, col.ind = "contrib",axes = c(2, 3))
  p2 = fviz_mfa_ind(res.fssd.MFA, col.ind = "cos2",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-3 plane
  p1 = fviz_mfa_ind(res.fssd.MFA, col.ind = "contrib",axes = c(1, 3))
  p2 = fviz_mfa_ind(res.fssd.MFA, col.ind = "cos2",axes = c(1, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-2-3 planes, with contribution in color scale
  p1 = fviz_mfa_ind(res.fssd.MFA, col.ind = "contrib")
  p2 = fviz_mfa_ind(res.fssd.MFA, col.ind = "contrib",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)

# 4. Contributions of variables to MFA axes -------------
#**************************************************** ------------
  # 4.0. Correlation circle --------------
  #********************************* -
  par(bg='grey50')
  plot.MFA(res.fssd.MFA, axes=c(1, 2), choix="var", new.plot=TRUE, 
           lab.var=TRUE,habillage="group", select = "coord 10")
  par(bg='white')
  # -> a bit cluttered ...
  
  # 4.1. select variables with above average contribution to MFA axes -------
  #********************************* 
  var.contrib=res.fssd.MFA$quanti.var$contrib
  head(var.contrib)
  summary(var.contrib[,1])
  var.contribs1=var.contrib[var.contrib[,1]>mean(var.contrib[,1]),1]
  var.contribs2=var.contrib[var.contrib[,2]>mean(var.contrib[,2]),2]
  var.contribs3=var.contrib[var.contrib[,3]>mean(var.contrib[,3]),3]
  
  # 4.2. Variables correlation with MFA axes -------------
  #***************************************
  var.cor=res.fssd.MFA$quanti.var$contrib
  graphics.off()
  ddres1=dimdesc(res.fssd.MFA, axes = 1:4, proba = 0.05)
  names(ddres1)
  
  # 4.2.1. Variables correlation with MFA axis1
  ddres1.1=ddres1$Dim.1$quanti
  ddres1.1=as.data.frame(ddres1.1[complete.cases(ddres1.1),])
  ddres1.1cor=ddres1.1[abs(ddres1.1$correlation)>=0.5,]
  ddres1.1cor$ct=unlist(strsplit(row.names(ddres1.1cor),split='[.]'))[seq(1,2*dim(ddres1.1cor)[1],2)]
  ddres1.1cor$year=unlist(strsplit(row.names(ddres1.1cor),split='[.]'))[seq(2,2*dim(ddres1.1cor)[1],2)]
  ddres1.1cor=ddres1.1cor[order(ddres1.1cor$ct),]
  ddres1.1cor$spid=row.names(ddres1.1cor)
  ddres1.1cor$sp=substr(as.character(ddres1.1cor$spid),1,8)
  ddres1.1cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.1cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.1cor$cs=gsub('[.]','',substr(as.character(ddres1.1cor$spid),17,24))
  ddres1.1cor$cs=gsub(' ','',ddres1.1cor$cs)
  unique(ddres1.1cor$cs)
  ddres1.1cor$signif=abs(ddres1.1cor$correlation)>=0.5
  head(ddres1.1cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.1cor)%in%names(var.contribs1))
  # Plot variables well correlated with MFA axis1 (abs(cor)>0.5)
  # Figure 7a in doray et al. 2018
  p <- ggplot(ddres1.1cor, aes(cs,sp,size=abs(correlation),colour=correlation),shape=signif)+geom_point()+ggtitle("cor(variables,MFA1)")+ 
    scale_colour_gradientn(colours = viridis(10)) + theme_bw()+ scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.6?"))
  p + facet_grid(cz~year) + theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(0.5)),axis.title.y = element_blank(),
                                  axis.title.x = element_blank()) + guides(size=FALSE)

  # 4.2.2. Variables correlation with MFA axis2
  ddres1.2=ddres1$Dim.2$quanti
  ddres1.2=as.data.frame(ddres1.2[complete.cases(ddres1.2),])
  ddres1.2cor=ddres1.2[abs(ddres1.2[,1])>=0.5,]
  ddres1.2cor$ct=unlist(strsplit(row.names(ddres1.2cor),split='[.]'))[seq(1,2*dim(ddres1.2cor)[1],2)]
  ddres1.2cor$year=unlist(strsplit(row.names(ddres1.2cor),split='[.]'))[seq(2,2*dim(ddres1.2cor)[1],2)]
  ddres1.2cor=ddres1.2cor[order(ddres1.2cor$correlation),]
  ddres1.2cor$spid=row.names(ddres1.2cor)
  ddres1.2cor$sp=substr(as.character(ddres1.2cor$spid),1,8)
  ddres1.2cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.2cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.2cor$cs=gsub('[.]','',substr(as.character(ddres1.2cor$spid),17,24))
  ddres1.2cor$cs=gsub(' ','',ddres1.2cor$cs)
  unique(ddres1.2cor$cs)
  ddres1.2cor$aCor=abs(ddres1.2cor$correlation)
  head(ddres1.2cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.2cor)%in%names(var.contribs2))
  # Plot variables well correlated with MFA axis2 (abs(cor)>0.5)
  # Figure 7b in doray et al. 2018
  p <- ggplot(ddres1.2cor, aes(cs,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA2)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p + facet_grid(cz~year) + theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(0.5)),axis.title.y = element_blank(),
                                  axis.title.x = element_blank()) + guides(size=FALSE)
  
  # 4.2.3. Variables correlation with MFA axis 3 
  ddres1.3=ddres1$Dim.3$quanti
  ddres1.3=as.data.frame(ddres1.3[complete.cases(ddres1.3),])
  ddres1.3cor=ddres1.3[abs(ddres1.3[,1])>=0.5,]
  ddres1.3cor$ct=unlist(strsplit(row.names(ddres1.3cor),split='[.]'))[seq(1,2*dim(ddres1.3cor)[1],2)]
  ddres1.3cor$year=unlist(strsplit(row.names(ddres1.3cor),split='[.]'))[seq(2,2*dim(ddres1.3cor)[1],2)]
  ddres1.3cor=ddres1.3cor[order(ddres1.3cor$correlation),]
  ddres1.3cor$spid=row.names(ddres1.3cor)
  ddres1.3cor$sp=substr(as.character(ddres1.3cor$spid),1,8)
  ddres1.3cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.3cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.3cor$cs=gsub('[.]','',substr(as.character(ddres1.3cor$spid),17,24))
  ddres1.3cor$cs=gsub(' ','',ddres1.3cor$cs)
  unique(ddres1.3cor$cs)
  ddres1.3cor$aCor=abs(ddres1.3cor$correlation)
  head(ddres1.3cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.3cor)%in%names(var.contribs3))
  # Plot variables well correlated with MFA axis3 (abs(cor)>0.5)
  # Figure 7c in doray et al. 2018
  p <- ggplot(ddres1.3cor, aes(cs,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA3)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p + facet_grid(cz~year) + theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(0.5)),axis.title.y = element_blank(),
                                  axis.title.x = element_blank()) + guides(size=FALSE)

  # Summary plots of variable correlations with MFA axes
  #******************************************************
  ddres1.1corPlusa.ct=aggregate(ddres1.1cor[ddres1.1cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.1cor[ddres1.1cor$correlation>=0.5,'ct']),length)
  ddres1.2corPlusa.ct=aggregate(ddres1.2cor[ddres1.2cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.2cor[ddres1.2cor$correlation>=0.5,'ct']),length)
  ddres1.2corMoinsa.ct=aggregate(ddres1.2cor[ddres1.2cor$correlation<=0.5,'correlation'],
                                 list(ct=ddres1.2cor[ddres1.2cor$correlation<=0.5,'ct']),length)
  ddres1.3corPlusa.ct=aggregate(ddres1.3cor[ddres1.3cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.3cor[ddres1.3cor$correlation>=0.5,'ct']),length)
  par(mfrow=c(2,2),mar=c(3,12,3,1),bg='white')
  barplot(ddres1.1corPlusa.ct$x,names.arg=ddres1.1corPlusa.ct$ct,main='cor(Variable:year,MFA1)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.1corMoins.res,1,sum),main='cor(Variable:year,MFA1)<=-0.5',horiz=TRUE,las=2)
  barplot(ddres1.2corPlusa.ct$x,names.arg=ddres1.2corPlusa.ct$ct,main='cor(Variable:year,MFA2)>=0.5',horiz=TRUE,las=2)
  barplot(ddres1.2corMoinsa.ct$x,names.arg=ddres1.2corMoinsa.ct$ct,main='cor(Variable:year,MFA2)<=0.5',horiz=TRUE,las=2)
  barplot(ddres1.3corPlusa.ct$x,names.arg=ddres1.3corPlusa.ct$ct,main='cor(Variable:year,MFA3)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.3corMoins.res,1,sum),main='cor(Variable:year,MFA3)<=-0.5',horiz=TRUE,las=2)
  par(mfrow=c(1,1))

#*******************************************************************
# 5. Spatial analysis of MFA results ------------
#******************************************************************* --------

  # 5.1. Maps of mean individuals MFA coordinates, ----
  #************************************************************
  # Objectives:
  #*********************
  # i) assess  gradients in fish community distribution
  # ii) assess within-cell temporal variability (inertia)
  # iii) define areas with stable and contrasted fish communities
  # over time ("characteristic areas")
  # Methods:
  #*********************
  # Characteristic areas are defined by selecting cells with above average
  # contribution and/oir quality of representation relatively to MFA1 to 3, over MFA1 to 3, and over MFA1 to 61
  # see function help page for details: ?MFA.rasterPlot
  # Figure 8 & 9 in Doray et al. 2018
  
  MFAres.rasters=MFA.rasterPlot(MFAres=res.fssd.MFA,nMFA=3,
                                cellsel=cellsel,xyCell=pipo,
                                ptitle='',
                                path.export=path.export.fish,ux11=TRUE,
                                plotit=list(MFA123coord=TRUE,MFA123timevar=TRUE,
                                                MFA123contrib=TRUE,MFA123cos2=TRUE,
                                                MFA123coordMcos2=TRUE,
                                                MFA123coordMcontrib=TRUE,MFA123pind=FALSE),
                                funSel=median,layout=c(3, 1),
                                Ndim=list(c(1),c(2),c(3),seq(3),seq(61)),
                                newbathy = FALSE)
  
  # quality of representation of individuals
  cos2df=res.fssd.MFA$ind$cos2
  # individuals contributions to MFA planes
  contrib2df=res.fssd.MFA$ind$contrib
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals quality of representation on MFA planes
  MFAcoordMcos2.rasterStack=MFAres.rasters$MFAcoordMcos2.rasterStack
  
  # 5.2. Plot MFA123 inertia maps -------------
  # = inter-annual variability of MFA loadings in grid-cells
  #************************
  MFAinertia.raster=MFAres.rasters$MFAinertia.raster
  raster.levelplot.PELGAS(MFAinertia.raster[[1:3]],ux11=TRUE,
                          lptheme=viridisTheme,
                          path1=path.export,
                          fid1=paste('1-3_MFAcoordInertia',sep=''),
                          margin = FALSE,xlab='',ylab='',nattri='',
                          fwidth=30,fheight=15,fres=300)
  
  # 5.3. Define "characteristic areas", -----------
  # where cell maps contribution to MFA planes is higher
  # than the results of "funSel" applied on contribution maps
  # Characteristic areas = fish core distribution areas
  #*******************************************************
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals contributions to MFA planes 
  MFAcoordMcontrib.rasterStack=MFAres.rasters$MFAcoordMcontrib.rasterStack
  # identify 'clumps' of cells in MFA planes to define "characteristic areas"
  MFAcoordMcontrib.rasterStackc1=clump(raster(MFAcoordMcontrib.rasterStack,1))
  plot(MFAcoordMcontrib.rasterStackc1)
  MFAcoordMcontrib.rasterStackc2=clump(raster(MFAcoordMcontrib.rasterStack,2))
  plot(MFAcoordMcontrib.rasterStackc2)
  MFAcoordMcontrib.rasterStackc3=clump(raster(MFAcoordMcontrib.rasterStack,3),directions=4)
  plot(MFAcoordMcontrib.rasterStackc3)
  # Compute mean MFA coordinates in characteristic areas
  zonal(raster(MFAcoordMcontrib.rasterStack,1), MFAcoordMcontrib.rasterStackc1, 'mean')
  zonal(raster(MFAcoordMcontrib.rasterStack,2), MFAcoordMcontrib.rasterStackc2, 'mean')
  zonal(raster(MFAcoordMcontrib.rasterStack,3), MFAcoordMcontrib.rasterStackc3, 'mean')
  # Extract cell coordinates and belongings to MFA characteristic areas and merge to data
  MFAcoordMcontrib.rasterStack1.xyz <- rasterToPoints(MFAcoordMcontrib.rasterStackc1)
  MFAcoordMcontrib.rasterStack2.xyz <- rasterToPoints(MFAcoordMcontrib.rasterStackc2)
  MFAcoordMcontrib.rasterStack3.xyz <- rasterToPoints(MFAcoordMcontrib.rasterStackc3)
  head(MFAcoordMcontrib.rasterStack1.xyz)
  head(cellsel)
  MFAcoordMcontrib.rasterStack1.xyz=merge(MFAcoordMcontrib.rasterStack1.xyz,cellsel,by.x=c('x','y'),
                                          by.y=c('x','y'))
  MFAcoordMcontrib.rasterStack2.xyz=merge(MFAcoordMcontrib.rasterStack2.xyz,cellsel,by.x=c('x','y'),
                                          by.y=c('x','y'))
  MFAcoordMcontrib.rasterStack3.xyz=merge(MFAcoordMcontrib.rasterStack3.xyz,cellsel,by.x=c('x','y'),
                                          by.y=c('x','y'))
  
  # Add belongings to characteristic areas to raw data
  clumpsdf1=MFAcoordMcontrib.rasterStack1.xyz
  clumpsdf2=MFAcoordMcontrib.rasterStack2.xyz
  clumpsdf3=MFAcoordMcontrib.rasterStack3.xyz
  
  # Gives names to characteristic areas for plots
  CLres.fssd.ALL.MFA1=merge(clumpsdf1,FishBiomassSpSizeDepths[,c('I','J','sp','Year','Zvalue')])
  CLres.fssd.ALL.MFA1[CLres.fssd.ALL.MFA1$clumps==1,'clumps']='NW'
  CLres.fssd.ALL.MFA1[CLres.fssd.ALL.MFA1$clumps==2,'clumps']='SE'
  CLres.fssd.ALL.MFA2=merge(clumpsdf2,FishBiomassSpSizeDepths[,c('I','J','sp','Year','Zvalue')])
  unique(CLres.fssd.ALL.MFA2$clumps)
  CLres.fssd.ALL.MFA2[CLres.fssd.ALL.MFA2$clumps==1,'clumps']='OFFS'
  CLres.fssd.ALL.MFA2[CLres.fssd.ALL.MFA2$clumps==2,'clumps']='GIRO'
  CLres.fssd.ALL.MFA3=merge(clumpsdf3,FishBiomassSpSizeDepths[,c('I','J','sp','Year','Zvalue')])
  unique(CLres.fssd.ALL.MFA3$clumps)
  CLres.fssd.ALL.MFA3[CLres.fssd.ALL.MFA3$clumps==1,'clumps']='CSTN'
  CLres.fssd.ALL.MFA3[CLres.fssd.ALL.MFA3$clumps==2,'clumps']='FERA'
  CLres.fssd.ALL.MFA3[CLres.fssd.ALL.MFA3$clumps==3,'clumps']='CSTS'
  
  # 5.4. Mean fish communities composition in characteristic areas ------------
  #**************************************
  # Figure 10 in Doray et al. 2018
  #MFA1: SE vs. NW 
  #********************
  CLres.fssd.ALL.MFA1a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=
                                            CLres.fssd.ALL.MFA1)
  # with mackerel and official palette
  # Relative biomass
  p <- ggplot(CLres.fssd.ALL.MFA1a, aes(cs,pmZ,fill = factor(sp))) +geom_bar(stat = "identity") + coord_flip()+
    scale_fill_manual(values=c('brown2','green','grey','blue','orange','red','black','magenta2','yellow'),
                      guide = guide_legend(title = "Species"))
  p + facet_grid(cz~clust)+ theme(axis.title.y = element_blank(),axis.title.x = element_blank())
  
  #MFA2: Gironde vs. offshore north 
  #***********************
  CLres.fssd.ALL.MFA2a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=
                                            CLres.fssd.ALL.MFA2)
  # with mackerel and official palette
  # Relative biomass
  p <- ggplot(CLres.fssd.ALL.MFA2a, aes(cs,pmZ,fill = factor(sp))) +geom_bar(stat = "identity") + coord_flip()+
    scale_fill_manual(values=c('brown2','green','grey','blue','orange','red','black','magenta2','yellow'),
                      guide = guide_legend(title = "Species"))
  p + facet_grid(cz~clust)+ theme(axis.title.y = element_blank(),axis.title.x = element_blank())
  
  # MFA3: Coastal N&S vs. Fer à Cheval 
  #************************
  CLres.fssd.ALL.MFA3a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=
                                            CLres.fssd.ALL.MFA3)
  # with mackerel and official palette
  # Relative biomass
  p <- ggplot(CLres.fssd.ALL.MFA3a, aes(cs,pmZ,fill = factor(sp))) +geom_bar(stat = "identity") + coord_flip()+
    scale_fill_manual(values=c('brown2','green','grey','blue','orange','red','black','magenta2','yellow'),
                      guide = guide_legend(title = "Species"))
  p + facet_grid(cz~clust)+ theme(axis.title.y = element_blank(),axis.title.x = element_blank())

#*****************************************************************
# 6. Temporal analysis of MFA results ---------------
#***************************************************************** --------
  # 6.1. Years (group) mean positions in MFA space -------------
  #***************************************************
  # From Pagès (2014):
  # Graphs like this are interpreted in a similar way as correlation circles: in
  # both cases the coordinate of a point is interpreted as a relationship measurement
  # with a maximum value of 1. But this new graph has two specificities: the
  # groups of variables are unstandardised and their coordinates are always positive.
  # They therefore appear in a square (with a side of 1 and with points [0,0]
  # and [1,1] as vertices) known as a relationship square. Pages (2014), p141
  # Therefore, when compared with an intuitive approach to interpretation
  # (for a PCA user), one highly practical advantage of this representation is its
  # relationship with the representations of individuals and variables already
  # provided: in MFA, data are considered from different points of view, but in
  # one single framework. Pages (2014), p141
  res.fssd.MFA.group=res.fssd.MFA$group
    gcoord=data.frame(res.fssd.MFA.group$coord[,1:3])
  gcontrib=res.fssd.MFA.group$contrib[,1:3]
  gcontrib=data.frame(gcontrib)
  names(gcontrib)=paste('contrib',names(gcontrib),sep='.')
  gcontrib$year=row.names(gcontrib)
  gcoord$year=row.names(gcoord)
  gcoord=merge(gcoord,gcontrib,by.x='year',by.y='year')
  row.names(gcoord)=gcoord$year
  gcoord$contrib.Dim.1.2=gcoord$contrib.Dim.1+gcoord$contrib.Dim.2
  gcoord$contrib.Dim.2.3=gcoord$contrib.Dim.2+gcoord$contrib.Dim.3
  gcoord$year=paste('20',gcoord$year,sep='')
  range(gcoord$Dim.1)
  range(gcoord$Dim.2)
  range(gcoord$Dim.3)
  
  # Plot of groups (years) mean positions in MFA space: 
  # Figure 11 in Doray et al. 2018
  #*********************************************
  p12 <- ggplot(gcoord,aes(Dim.1,Dim.2,label=year))+geom_text(aes(colour = contrib.Dim.1.2))+
    scale_colour_continuous(name="Contribution (%)")
  p23 <- ggplot(gcoord,aes(Dim.2,Dim.3,label=year))+geom_text(aes(colour = contrib.Dim.2.3))+
    scale_colour_continuous(name="Contribution (%)")
  multiplot(p12,p23)
  # => 2011 atypical year according to MFA2

  # 6.2. Eventually, study years partial axes correlations with MFA axes ---------
  #******************************************
  # We have already insisted on the need to connect the inertias from the MFA
  # and those in the separate PCAs (see Tables 4.4 and 4.5). It is also useful to
  # connect the factors of the MFA with those of the separate PCAs (also known
  # as partial axes), both to understand better the effects of weighting and to enrich
  # interpretation of the analyses. In order to do this, the latter are projected as
  # supplementary variables (see Figure 4.5). Pages 2014, p88
  
  # Partial axes correlations
  res.fssd.MFA.partial.axes=res.fssd.MFA$partial.axes
  ddres.pyears=data.frame(res.fssd.MFA.partial.axes$cor)
  ddres.pyears[row.names(ddres.pyears)=='Dim1.14',]
  ddres.pyears.13=data.frame(res.fssd.MFA.partial.axes$cor[,1:3])
  ddres.pyears.13s=ddres.pyears.13[round(abs(ddres.pyears.13$Dim.1))>=0.6|round(abs(ddres.pyears.13$Dim.2))>=0.6|
                                     round(abs(ddres.pyears.13$Dim.3))>=0.6,]
  ddres.pyears.13s$Dim=factor(gsub('[.]','',substr(row.names(ddres.pyears.13s),1,5)),ordered=TRUE,levels=paste('Dim',seq(10),sep=''))
  ddres.pyears.13s$Year=gsub('[.]','',substr(row.names(ddres.pyears.13s),6,8))
  ddres.pyears.13s.long=reshape(ddres.pyears.13s,varying=list(1:3),direction='long',timevar='MFA',v.names='correlation')
  ddres.pyears.13s.long$MFA=paste('Dim',ddres.pyears.13s.long$MFA,sep='')
  ddres.pyears.13s.long$signif=abs(round(ddres.pyears.13s.long$correlation))>=0.6
  ddres.pyears.13s.long$MFA=paste('MFA',ddres.pyears.13s.long$MFA,sep='.')
  # Plot partial axes correlations with MFA axes
  p <- ggplot(ddres.pyears.13s.long[ddres.pyears.13s.long$Dim%in%c('Dim1','Dim2','Dim3'),], 
              aes(Dim,Year,size=abs(correlation),colour=correlation))+geom_point()
  p+ scale_colour_gradientn(colours = viridis(3)) + facet_grid(.~MFA)+
    theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(1)),axis.title.y = element_blank(),
          axis.title.x = element_blank()) + guides(size=FALSE)
  # => No clear patterns

  # 7. Spatio-temporal analysis of MFA results ---------------
  #*********************************************** ----------
  # 7.1. Maps of partial individuals (map cell:year pairs) coordinates ----
  # in MFA space
  #***********************************************
  MFAcoord=res.fssd.MFA$ind$coord
  pindCoord.fssd=res.fssd.MFA$ind$coord.partiel
  MFAyears=unlist(strsplit(row.names(pindCoord.fssd),split='[.]'))[seq(2,(2*length(row.names(pindCoord.fssd))),2)]
  lyears=unique(MFAyears)
  
  # 7.1.1. Partial individuals maps, one plot per year ----  
  #**********************************
  for (i in 1:length(lyears)){
    dfi=pindCoord.fssd[MFAyears==lyears[i],]-MFAcoord
    res=MFA.rasterPlot(MFAres=dfi,nMFA=3,cellsel,pipo,path.export=NULL,
                       plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                   MFA123contrib=FALSE,
                                   MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                   MFA123coordMcontrib=FALSE,MFA123pind=FALSE),funSel='mean',
                       ptitle=paste('20',lyears[i],sep=''),
                   lptheme = viridisTheme(),newbathy = FALSE)
  }
  # 7.1.2. Mosaics of partial individuals coordinates anomalies maps -----
  # one mosaic plot per MFA1,2,3, with all years
  #****************************************
  res=MFA.rasterPlot(MFAres=res.fssd.MFA,nMFA=3,cellsel,pipo,
                     path.export=NULL,
                         plotit=list(MFA123coord=TRUE,MFA123contrib=FALSE,MFA123timevar=FALSE,
                                     MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                     MFA123coordMcontrib=FALSE,MFA123pind=TRUE),funSel='mean',
                         ptitle=paste(''),lptheme=BuRdTheme,
                     anomalit=TRUE,newbathy = FALSE)
  # => MFA2 strongly below average in 2011 in Gironde mouth, 
  # whereas it is usually above average: this explains why 2011 was an
  # atypical year
 
