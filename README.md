# EchoR

The goals of EchoR are: 
* to handle pre-processed fisheries acoustics data collected during sea surveys; 
* to produce and analyse standard ec(h)osystemic indicators and maps based on fisheries acoustics data.

## Installation

### From Gitlab (recommended)

To install the released version of EchoR from https://gitlab.ifremer.fr/md0276b/echor:

1. install "remotes" R package by executing in a R console:

`install.packages("remotes")` 

2. install the EchoR package last release by executing in a R console:

`remotes::install_gitlab(repo="md0276b/echor",host="https://gitlab.ifremer.fr")`

To install the (possibly unstable) development version, run in an R console:

remotes::install_gitlab("md0276b/echor@dev",host = "gitlab.ifremer.fr")

### From a package archive file

1. Install EchoR dependencies by running in a R console:
`install.packages(c("gridBase","PBSmapping","splancs","sp","Rmixmod", "fields","maps","mapdata","ncdf","ncdf4",
"marmap","maptools","RPostgreSQL","alphahull","plyr","foreign","abind","rgdal","R.utils","stringi","colorspace",
"raster","rasterVis","latticeExtra"))`  
See the wiki in case of troubles when installing rgdal

2. Go to the [Project 'Releases' page](https://gitlab.ifremer.fr/md0276b/echor/releases) and download an EchoR binary package (.zip format under Window, tar.gz format under Linux)
3. Install EchoR from the archive file by either entering the following command in the R prompt: 
`install.packages(path_to_file, repos = NULL, type="source")`
or use e.g. the RStudio graphical interface

## See the Wiki section for more support

If you want to contribute code, please email mathieu.doray[at]ifremer.fr 


