#****************
#Functions for acoustic biomass assessment
#****************
#ECHO-INTEGRATION FACTOR
#****************

#*******************************************************************************
#Front-end function for Xe computations
#*******************************************************************************
	#************************************
	#Xe computations, various flavors:
	#************************
	#XeB=TRUE: Xe in kg, based on mean length
	#XeN=TRUE: Xe in no. of fish, based on mean length
	#XeN=TRUE&XeN=TS.fL==TRUE: Xe in no. of fish, based on length frequency
	#************************************

	Xe.compute=function(captures.comps2,compute.it=list(XeB=TRUE,XeN=TRUE),
		EspDev=NULL,mens2sas=NULL,DEVit=FALSE,TS.fL=FALSE,Unit.dev='sA',nid='NOCHAL'){

		lyears=sort(as.character(unique(captures.comps2$CAMPAGNE)))
		Nyears=length(lyears)

  #*******************************************************************************
  #Xe's per haul in weight, NOT splitted by echotype, computed based on mean length
  #*******************************************************************************
		if (!DEVit){

			#Xe's per haul
			#****************

			if (compute.it$XeB==TRUE){	

				#Xe's in kg per haul and species/size class, 
				#based on mean length
				#****************
				for (i in 1:Nyears){
					capturesi=captures.comps2[
						captures.comps2$CAMPAGNE==lyears[i],]
					if (!is.null(EspDev)){
						EspDevi=EspDev[EspDev$CAMPAGNE==
							as.character(lyears[i]),]
						codesp.pbi=capturesi[!is.element(
							capturesi$CodEsp,EspDevi$CodEsp),]
					}	
					Xei=Xe.NOCHAL(Peche=capturesi)
					Xei$CAMPAGNE=lyears[i]
					Xei=merge(Xei,unique(capturesi[,c(nid,
						'LATF','LONF')]),by.x='NOCHAL',
						by.y='NOCHAL')
					if (i==1){
						XeB.df=Xei
						basis=data.frame(year=lyears[i],
							capturesi)
						if (!is.null(EspDev)){
							if (dim(codesp.pbi)[1]>0){
								codesp.pb.db=data.frame(
									year=lyears[i],
									codesp.pbi)
							}else{
								codesp.pb.db=basis[NULL,]
							}
						}
					}else{
						XeB.df=rbind(XeB.df,Xei)
						if (!is.null(EspDev)){
							if (dim(codesp.pbi)[1]>0){
								codesp.pb.db=rbind(codesp.pb.db,
									data.frame(year=lyears[i],
										codesp.pbi))
							}
						}
					}
				}
				XeB=XeB.df
			}else XeB=NULL
	#*******************************************************************************
  #Xe's per haul in numbers, NOT splitted by echotype, computed based on mean length frequencies
  #*******************************************************************************  
			if (compute.it$XeN==TRUE&!TS.fL){
		
				#Xe's in no. of fish per haul and species/size class, 
				#based on mean length
				#****************

				for (i in 1:Nyears){
					capturesi=captures.comps2[captures.comps2$CAMPAGNE==lyears[i],]
					if (!is.null(EspDev)){
						EspDevi=EspDev[EspDev$CRUISE==
							as.character(lyears[i]),]
						codesp.pbi=capturesi[!is.element(
							capturesi$CodEsp,EspDevi$CodEsp),]
					}
					Xei=Xe.NOCHAL(Peche=capturesi,TSkg=FALSE)
					Xei$CAMPAGNE=lyears[i]
					Xei=merge(Xei,unique(capturesi[,c('NOCHAL',
						'LATF','LONF')]),
						by.x='NOCHAL',by.y='NOCHAL')
					if (i==1){
						XeN.df=Xei
						if (!is.null(EspDev)){
							if(dim(codesp.pbi)[1]>0){
								codesp.pb.db=data.frame(
									year=lyears[i],
									codesp.pbi)
							}else{
								basis=data.frame(year=
									lyears[i],capturesi)
								codesp.pb.db=basis[NULL,]
							}
						}
					}else{
						XeN.df=rbind(XeN.df,Xei)
						if (!is.null(EspDev)){
							if (dim(codesp.pbi)[1]>0){
								codesp.pb.db=rbind(
									codesp.pb.db,
									data.frame(year=
									lyears[i],codesp.pbi))
							}
						}
					}
				}
				XeN=XeN.df
#*******************************************************************************
  #Xe's per haul in numbers, NOT splitted by echotype, computed based on length frequencies
  #*******************************************************************************  

			}else if (compute.it$XeN==TRUE&TS.fL){

				#Xe's in no. of fish per haul and species, 
				#based on length-frequencies
				#****************

				for (i in 1:Nyears){
					capturesi=captures.comps2[
						captures.comps2$CAMPAGNE==lyears[i],]
					mensi=mens2sas[mens2sas$CAMPAGNE==lyears[i],]

					Xei=Xe.NOCHAL(Peche=capturesi,Mens=mensi,
						TSkg=FALSE,TS.fL=TS.fL)
					Xei$CAMPAGNE=lyears[i]
					Xei=merge(Xei,unique(capturesi[,c('NOSTA',
						'NOCHAL','LATF','LONF')]),
						by.x='NOSTA',by.y='NOSTA')
					if (i==1){
						XeNL.df=Xei
						basis=data.frame(year=lyears[i],
							capturesi)
						codesp.pb.db=basis[NULL,]
					}else{
						XeNL.df=rbind(XeNL.df,Xei)
					}
				}
				XeN=XeNL.df
			}else{XeN=NULL}

#*******************************************************************************
#Xe's per haul in numbers, splitted by echotype, computed based on mean length
#*******************************************************************************  

		}else if (DEVit){
			
			#Xe's per echotype and haul
			#****************

			if (compute.it$XeB){	
				#Xe's in kg per haul, echotype, 
				#and species/size class, 
				#based on mean length
				#****************
				for (i in 1:Nyears){
					capturesi=captures.comps2[
						captures.comps2$CAMPAGNE==lyears[i],]
					EspDevi=EspDev[EspDev$CAMPAGNE==
						as.character(lyears[i]),]
					codesp.pbi=capturesi[!is.element(
						capturesi$CodEsp,EspDevi$CodEsp),]
					Ndevi=sort(as.numeric(gsub('D','',unique(EspDevi$DEV))))
					cat(lyears[i],'\n')

					list.XeBi=lapply(X=sort(as.numeric(Ndevi)),FUN=Xe.k.NOCHAL,
						Pechei=capturesi,EspDevi=EspDevi,
						unite=Unit.dev)
					names(list.XeBi)=paste('D',sort(as.numeric(Ndevi)),sep='')

					if (i==1){
						list.XeB=list(list.XeBi)
					}else{
						list.XeB=c(list.XeB,list(list.XeBi))
					}
				}
				XeB=list.XeB
			}else{XeB=NULL}
	
			if (compute.it$XeN){
				#Xe's in no. of fish per haul, echotype, 
				#and species/size class, 
				#based on mean length
				#****************
				for (i in 1:Nyears){
					capturesi=captures.comps2[
						captures.comps2$CAMPAGNE==lyears[i],]
					EspDevi=EspDev[EspDev$CAMPAGNE==
						as.character(lyears[i]),]
					codesp.pbi=capturesi[!is.element(
						capturesi$CodEsp,EspDevi$CodEsp),]
					Ndevi=sort(as.numeric(gsub('D','',unique(EspDevi$DEV))))
					cat(lyears[i],'\n')

					list.XeNi=lapply(X=sort(as.numeric(Ndevi)),FUN=Xe.k.NOCHAL,
						Peche=capturesi,EspDev=EspDevi,unite=Unit.dev,TSkg=FALSE)
					names(list.XeNi)=paste('D',sort(as.numeric(Ndevi)),sep='')
					if (i==1){
						list.XeN=list(list.XeNi)
					}else{
						list.XeN=c(list.XeN,list(list.XeNi))
					}
				}
				XeN=list.XeN
			}else XeN=NULL
		}

	list(XeB=XeB,XeN=XeN)
	}

	#************************************
	#Xe's in wide format
	#************************
	#************************************

	multi.Xespand=function(XeN.df=NULL,XeB.df=NULL,XeNL.df=NULL){

	#****************
	#'classical' hauls
	#****************
	if (!is.null(XeNL.df)){
		head(XeNL.df)
		XeNL.CLAS=XeNL.df[XeNL.df$CodEsp%in%c('ENGR-ENC-CLAS','SARD-PIL-CLAS',
			'TRAC-TRU-CLAS','MICR-POU-CLAS','SCOM-SCO-CLAS','SPRA-SPR-CLAS'),]
		names(XeNL.CLAS)[names(XeNL.CLAS)=='Xe']='XeNl'	
		head(XeNL.CLAS)
		XeNL.CLAS.ALL=XpandIt(Xe.df=XeNL.CLAS,zname='XeNl')
		XeNL.CLAS.wide=XeNL.CLAS.ALL$wide
		XeNL.CLAS.long=XeNL.CLAS.ALL$long
		XeNL.CLAS.long=merge(XeNL.CLAS.long,unique(captures.comps2[,
			c('CAMPAGNE','NOCHAL','NOSTA','STRATE')]),
			by.x=c('CAMPAGNE','NOCHAL'),by.y=c('CAMPAGNE','NOCHAL'))
	}else if (!is.null(XeB.df)&!is.null(XeN.df)){
		head(XeN.df)
		XeN.CLAS=XeN.df[XeN.df$CodEsp%in%c('ENGR-ENC-CLAS-0','SARD-PIL-CLAS-0',
			'TRAC-TRU-CLAS-0','TRAC-TRU-CLAS-P','TRAC-TRU-CLAS-G',
			'MICR-POU-CLAS-0','SCOM-SCO-CLAS-0','SPRA-SPR-CLAS-0'),]
		names(XeN.CLAS)[names(XeN.CLAS)=='Xe']='XeN'	

		XeB.CLAS=XeB.df[XeB.df$CodEsp%in%c('ENGR-ENC-CLAS-0','SARD-PIL-CLAS-0',
			'TRAC-TRU-CLAS-0','TRAC-TRU-CLAS-P','TRAC-TRU-CLAS-G',
			'MICR-POU-CLAS-0','SCOM-SCO-CLAS-0','SPRA-SPR-CLAS-0'),]
		names(XeB.CLAS)[names(XeB.CLAS)=='Xe']='XeB'	

		XeN.CLAS.ALL=XpandIt(Xe.df=XeN.CLAS,zname='XeN')
		XeB.CLAS.ALL=XpandIt(Xe.df=XeB.CLAS,zname='XeB')

		XeN.CLAS.wide=XeN.CLAS.ALL$wide
		XeB.CLAS.wide=XeB.CLAS.ALL$wide
		XeN.CLAS.long=XeN.CLAS.ALL$long
		XeB.CLAS.long=XeB.CLAS.ALL$long

		#add NOSTA and CLAS and merge XeN and XeB
		#****************

		#wide format
		#*********
		dim(XeN.CLAS.wide);dim(Xe.CLAS.wide)
		head(XeN.CLAS.wide)
		Xe.CLAS.wide=merge(XeN.CLAS.wide,XeB.CLAS.wide,
			by.x=c('CAMPAGNE','NOCHAL','LATF','LONF'),
			by.y=c('CAMPAGNE','NOCHAL','LATF','LONF'))
		names(Xe.CLAS.wide)
		dim(Xe.CLAS.wide)
		Xe.CLAS.wide=merge(Xe.CLAS.wide,unique(captures.comp[,
			c('CAMPAGNE','NOCHAL','NOSTA','STRATE')]),
			by.x=c('CAMPAGNE','NOCHAL'),by.y=c('CAMPAGNE','NOCHAL'))
		names(Xe.CLAS.wide)
		dim(Xe.CLAS.wide)

		Xe.CLAS.wide$CAMPAGNE=factor(as.character(Xe.CLAS.wide$CAMPAGNE))

		#long format
		#---
		XeN.CLAS.long=merge(XeN.CLAS.long,unique(captures.comps2[,
			c('CAMPAGNE','NOCHAL','NOSTA','STRATE')]),
			by.x=c('CAMPAGNE','NOCHAL'),by.y=c('CAMPAGNE','NOCHAL'))
		head(XeN.CLAS.long)

		XeB.CLAS.long=merge(XeB.CLAS.long,unique(captures.comps2[,
			c('CAMPAGNE','NOCHAL','NOSTA','STRATE')]),
			by.x=c('CAMPAGNE','NOCHAL'),by.y=c('CAMPAGNE','NOCHAL'))
		head(XeB.CLAS.long)
	}
	if (!is.null(XeNL.df)&!is.null(XeB.df)){
		Xe.CLAS.long=rbind(XeNL.CLAS.long,XeN.CLAS.long,XeB.CLAS.long)
	}else if (!is.null(XeNL.df)&is.null(XeB.df)){
		Xe.CLAS.long=XeNL.CLAS.long	
	}else if (!is.null(XeN.df)&!is.null(XeB.df)){
		Xe.CLAS.long=rbind(XeN.CLAS.long,XeB.CLAS.long)	
	}
	#'surface' hauls
	#****************
	if (!is.null(XeNL.df)){
		XeNL.SURF=XeNL.df[XeNL.df$CodEsp%in%c('ENGR-ENC-SURF','SARD-PIL-SURF',
			'TRAC-TRU-SURF','MICR-POU-SURF','SCOM-SCO-SURF','SPRA-SPR-SURF'),]
		names(XeNL.SURF)[names(XeNL.SURF)=='Xe']='XeNl'
		XeNL.SURF.ALL=XpandIt(Xe.df=XeNL.SURF,zname='XeNl')	
		XeNL.SURF.wide=XeNL.SURF.ALL$wide		
		XeNL.SURF.long=XeNL.SURF.ALL$long
		XeNL.SURF.long=merge(XeNL.SURF.long,unique(captures.comps2[,
			c('CAMPAGNE','NOCHAL','NOSTA','STRATE')]),	
			by.x=c('CAMPAGNE','NOCHAL'),by.y=c('CAMPAGNE','NOCHAL'))
	}else if (!is.null(XeN.df)&!is.null(XeB.df)){	
		XeN.SURF=XeN.df[XeN.df$CodEsp%in%c('ENGR-ENC-SURF-0','SARD-PIL-SURF-0',
			'TRAC-TRU-SURF-0','TRAC-TRU-SURF-P','TRAC-TRU-SURF-G',
			'MICR-POU-SURF-0','SCOM-SCO-SURF-0','SPRA-SPR-SURF-0'),]
		names(XeN.SURF)[names(XeN.SURF)=='Xe']='XeN'
	
		XeB.SURF=XeB.df[XeB.df$CodEsp%in%c('ENGR-ENC-SURF-0','SARD-PIL-SURF-0',
			'TRAC-TRU-SURF-0','TRAC-TRU-SURF-P','TRAC-TRU-SURF-G',
			'MICR-POU-SURF-0','SCOM-SCO-SURF-0','SPRA-SPR-SURF-0'),]
		names(XeB.SURF)[names(XeB.SURF)=='Xe']='XeB'
		XeN.SURF.ALL=XpandIt(Xe.df=XeN.SURF,zname='XeN')
		XeB.SURF.ALL=XpandIt(Xe.df=XeB.SURF,zname='XeB')
		XeN.SURF.wide=XeN.SURF.ALL$wide
		XeB.SURF.wide=XeB.SURF.ALL$wide
		XeN.SURF.long=XeN.SURF.ALL$long
		XeB.SURF.long=XeB.SURF.ALL$long
		#add NOSTA and CLAS and merge XeN and XeB
		#****************
		#wide format
		#---
		Xe.SURF.wide=merge(XeN.SURF.wide,XeB.SURF.wide,
			by.x=c('CAMPAGNE','NOCHAL','LATF','LONF'),
			by.y=c('CAMPAGNE','NOCHAL','LATF','LONF'))
		names(Xe.SURF.wide)
		dim(XeN.SURF.wide);dim(Xe.SURF.wide)
		Xe.SURF.wide=merge(Xe.SURF.wide,unique(captures.comps2[,
			c('CAMPAGNE','NOCHAL','NOSTA','STRATE')]),
			by.x=c('CAMPAGNE','NOCHAL'),by.y=c('CAMPAGNE','NOCHAL'))
		names(Xe.SURF.wide)
		dim(Xe.SURF.wide)

		Xe.SURF.wide$CAMPAGNE=factor(as.character(Xe.SURF.wide$CAMPAGNE))

		#long format
		#---
		XeN.SURF.long=merge(XeN.SURF.long,unique(captures.comps2[,
			c('CAMPAGNE','NOCHAL','NOSTA','STRATE')]),
			by.x=c('CAMPAGNE','NOCHAL'),by.y=c('CAMPAGNE','NOCHAL'))
		head(XeN.SURF.long)

		XeB.SURF.long=merge(XeB.SURF.long,unique(captures.comps2[,
			c('CAMPAGNE','NOCHAL','NOSTA','STRATE')]),
			by.x=c('CAMPAGNE','NOCHAL'),by.y=c('CAMPAGNE','NOCHAL'))
		head(XeB.SURF.long)
	}
	if (!is.null(XeNL.df)&!is.null(XeB.df)){
		Xe.SURF.long=rbind(XeNL.SURF.long,XeN.SURF.long,XeB.SURF.long)
	}else if (!is.null(XeNL.df)&is.null(XeB.df)){
		Xe.SURF.long=XeNL.SURF.long
	}else if (!is.null(XeN.df)&!is.null(XeB.df)){
		Xe.SURF.long=rbind(XeN.SURF.long,XeB.SURF.long)	
	}
		#All hauls, long format
		#****************
		Xe.long=rbind(Xe.CLAS.long,Xe.SURF.long)
		head(Xe.long)
		dim(Xe.long)
		Xe.long$CAMPAGNE=factor(as.character(Xe.long$CAMPAGNE))
	Xe.long
	}

#*******************************************************************************
#Back-end functions for Xe computations
#*******************************************************************************
#*******************************************************************************
#Xe computation for deviation k
#let Qe be the biomass of species e, A the surface, Em the mean sA, then:
#Qe = A*Em*Xe
#ke: species proportion in hauls
#************************---
	#k=2

	Xe.NOCHAL=function(Peche,Mens=NULL,unite='sA',TSkg=TRUE,TS.fL=FALSE,
		method='P') {
	  #Xe computation without echotype k
	if (TS.fL){
		#****************
		#TS in number, weighted by length frequency distribution 
		#****************
		  if (is.null(Mens)){
			stop('Please provide a length measurements file')
		  }
		#****************
		#TS in number of fish
		#****************
		#species proportions in haul catch = species number/total number
    Ntot.sp=aggregate(Peche$NT,list(as.character(Peche$NOSTA)),sum,
			na.rm=T)
		names(Ntot.sp)=c('NOSTA','Ntot')
		Peche=merge(Peche,Ntot.sp,by.x='NOSTA',by.y='NOSTA')
		Peche$ke=Peche$NT/Peche$Ntot	
  
		# sigma_sp:
 	 NLtot.spL=aggregate(Mens$NBIND,list(as.character(Mens$NOSTA)),
			sum,na.rm=T)
		names(NLtot.spL)=c('NOSTA','Ntot')
		Mens$sigma.spi=4*pi*10^((Mens$CAC*log10(Mens$cL)-Mens$BAC)/10)
		Mens=merge(Mens,NLtot.spL,by.x='NOSTA',by.y='NOSTA')
		Mens$wi.sigma.spi=Mens$sigma.spi*Mens$NBIND/Mens$Ntot
		Mensa=aggregate(Mens$wi.sigma.spi,
			list(Mens$NOSTA,Mens$GENR_ESP),sum)
		names(Mensa)=c('NOSTA','GENR_ESP','sigma.spi')
		pmens=merge(Peche[,c('NOSTA','GENR_ESP','ke','STRATE')],Mensa,
			by.x=c('NOSTA','GENR_ESP'),by.y=c('NOSTA','GENR_ESP'))
		#combined:
    		pmens$ks=pmens$ke*pmens$sigma.spi
		deno=aggregate(pmens$ks,list(pmens$NOSTA),sum,na.rm=T)
		names(deno)=c('NOSTA','kstot')
		pmens=merge(pmens,deno,by.x='NOSTA',by.y='NOSTA')		
		pmens$Xe=pmens$ke/pmens$kstot
		pmens$CodEsp=paste(pmens$GENR_ESP,pmens$STRATE,sep='-')
		Xe.res=pmens
	}else if (!TSkg&method=='M'){
		#****************
		#TS in number of fish, my method
		#****************
		#species proportions in haul catch = species number/total number
   	Ntot.sp=aggregate(Pechei$NT,list(as.character(Pechei$NOSTA)),sum,
			na.rm=T)
		names(Ntot.sp)=c('NOSTA','Ntot')
    lCH.pb=Ntot.sp[is.infinite(Ntot.sp$Ntot)|is.na(Ntot.sp$Ntot),]
    Pechei.pb=Pechei[Pechei$NOSTA%in%lCH.pb$NOSTA,]
		Xe.res=merge(Pechei[,c('NOSTA','NOCHAL','NT','LM','CAC','BAC')],Ntot.sp,
			by.x='NOSTA',by.y='NOSTA')
		Xe.res$ke=Xe.res$NT/Xe.res$Ntot	
		# sigma_sp:
 	 Xe.res$sigma.spi=4*pi*10^((Xe.res$CAC*log10(Xe.res$LM)-Xe.res$BAC)/10)
		#combined:
    		Xe.res$ks=Xe.res$ke*Xe.res$sigma.spi
		deno=aggregate(Xe.res$ks,list(Xe.res$NOSTA),sum,na.rm=T)
		names(deno)=c('NOSTA','kstot')
		Xe.res=merge(Xe.res,deno,by.x='NOSTA',by.y='NOSTA')		
		Xe.res$Xe=Xe.res$ke/Xe.res$kstot
	}else if (method!='M'){
		#****************
		#Peter's method
		#****************
	   	sta.k=sort(unique(Peche$NOCHAL))
		   ke=0
		#****************
		if (unite=='sA') {
		#****************
			#****************
			#if TS in weight
			#****************
			if (TSkg){
			#species proportions = species weight/total weight
			   pt.sta=tapply(Peche$PT,Peche$NOCHAL,sum,na.rm=T)
			   pt.sta=pt.sta[!is.na(pt.sta)]
				for (i in 1:length(Peche$NOCHAL)) {
				     j=match(Peche$NOCHAL[i],sta.k)
				     ke=c(ke,Peche$PT[i]/pt.sta[j])
				} 
				ke=ke[-1] 
			# sa et sigma :
				sigmak=4*pi*Peche$MOULE*1000*(Peche$LM^2)*10^
					(-Peche$BAC/10)
				#Xe in kg: MOULE in no. of fish per kg = 
				#1/(fish mean weight in kg) 
				#Xe in g: multiply by MOULE*1000
				tsk=10*log10(sigmak/(4*pi))
			    	ks=ke*sigmak
				sigma=sigmak
			#****************
			#if TS in number of fish
			#****************
			}else{
			#species proportions = species number/total number
			   pt.sta=tapply(Peche$NT,Peche$NOCHAL,sum,na.rm=T)
			   pt.sta=pt.sta[!is.na(pt.sta)]
				for (i in 1:length(Peche$NOCHAL)) {
				     j=match(Peche$NOCHAL[i],sta.k)
				     ke=c(ke,Peche$NT[i]/pt.sta[j])
				} 
				ke=ke[-1] 
				# sa et sigma :
				sigma1=4*pi*(Peche$LM^2)*10^(-Peche$BAC/10) 
				ts1=10*log10(sigma1/(4*pi))
			    	ks=ke*sigma1
				sigma=sigma1
			}
		}
		#****************			
		if  (unite=='En') {
		#****************
				## energ et Fe :
				TSk=20*log10(Peche$LM)-Peche$BAC+10*log10(Peche$MOULE)
    				fe=0.00585*( 10**(-(35+TSk)/10) )  
		    	    	ks=ke/fe
		}
	##
		   deno.sta=tapply(ks,Peche$NOCHAL,sum,na.rm=T)  ; 
			deno.sta=deno.sta[!is.na(deno.sta)]
		   xe=0
		   for (i in 1:length(Peche$NOCHAL)) {
		     j=match(Peche$NOCHAL[i],sta.k)
		     xe=c(xe,ke[i]/deno.sta[j])
		   } 
		   xe=xe[-1]
		Xe.res=data.frame(NOCHAL=Peche$NOCHAL,CodEsp=Peche$CodEsp,ke=ke,
			Xe=xe,sigma.sp=sigma,mweight=1/Peche$MOULE) 
	} 
	Xe.res
	}

	ke.check=function(k,list.Xe){
		Xek=list.Xe[[k]]
		ske=aggregate(Xek$ke,list(Xek$Nosta),sum)
		mske=mean(ske[,2])
		mske
	}

	#************************---
	#Xe per codEsp/haul
	#************************---
k=1
	Xe.k.NOCHAL=function(Pechei,EspDevi,k,unite='sA',TSkg=TRUE) {

	  cat('Dev.',k,"\n")
	  
	  #selection of species in deviation k
	  EspDev.k=EspDevi[EspDevi$DEV==paste('D',k,sep=''),]
	  sel.k=is.element(Pechei$CodEsp,EspDev.k$CodEsp)
	  
	  if ((length(unique(sel.k))==1)&&(unique(sel.k)==FALSE)) {
  		Peche.k=Pechei 
	  	ke=rep(0,length(Pechei$NOCHAL))
		  xe=rep(0,length(Pechei$NOCHAL))
		  sigma=0
	  }else {
	    Peche.k=Pechei[sel.k,]	
	    sta.k=sort(unique(Peche.k$NOCHAL))
	    ke=0
	    #****************
	    if (unite=='sA') {
	      #****************
	      #****************
	      #if TS in weight
	      #****************
	      if (TSkg){
	        #species proportions = species weight/total weight
	        pt.sta=tapply(Peche.k$PT,Peche.k$NOCHAL,sum,na.rm=T)
	        pt.sta=pt.sta[!is.na(pt.sta)]
	        for (i in 1:length(Peche.k$NOCHAL)) {
	          j=match(Peche.k$NOCHAL[i],sta.k)
	          ke=c(ke,Peche.k$PT[i]/pt.sta[j])
	        } 
	        ke=ke[-1] 
	        # sa et sigma :
	        sigmak=4*pi*Peche.k$MOULE*1000*(Peche.k$LM^2)*10^
	          (-Peche.k$BAC/10)
	        #Xe in kg: MOULE in no. of fish per kg = 
	        #1/(fish mean weight in kg) 
	        #Xe in g: multiply by MOULE*1000
	        tsk=10*log10(sigmak/(4*pi))
	        ks=ke*sigmak
	        sigma=sigmak
	        #****************
	        #if TS in number of fish
	        #****************
	      }else{
	        #species proportions = species weight/total weight
	        pt.sta=tapply(Peche.k$NT,Peche.k$NOCHAL,sum,na.rm=T)
	        pt.sta=pt.sta[!is.na(pt.sta)]
	        for (i in 1:length(Peche.k$NOCHAL)) {
	          j=match(Peche.k$NOCHAL[i],sta.k)
	          ke=c(ke,Peche.k$NT[i]/pt.sta[j])
	        } 
	        ke=ke[-1] 
	        # sa et sigma :
	        sigma1=4*pi*(Peche.k$LM^2)*10^(-Peche.k$BAC/10) 
	        ts1=10*log10(sigma1/(4*pi))
	        ks=ke*sigma1
	        sigma=sigma1
	      }
	    }
	    #sigmak-sigma1*Peche.k$MOULE
	    
	    #****************			
	    if  (unite=='En') {
	      #****************
	      ## energ et Fe :
	      TSk=20*log10(Peche.k$LM)-Peche.k$BAC+10*log10(Peche.k$MOULE)
	      fe=0.00585*( 10**(-(35+TSk)/10) )  
	      ks=ke/fe
	    }
	    ##
	    deno.sta=tapply(ks,Peche.k$NOCHAL,sum,na.rm=T)  ; 
	    deno.sta=deno.sta[!is.na(deno.sta)]
	    xe=0
	    for (i in 1:length(Peche.k$NOCHAL)) {
	      j=match(Peche.k$NOCHAL[i],sta.k)
	      xe=c(xe,ke[i]/deno.sta[j])
	    } 
	    xe=xe[-1]
		
	  } 
	Xe.res=data.frame(NOCHAL=Peche.k$NOCHAL,CodEsp=Peche.k$CodEsp,ke=ke,
		Xe=xe,sigma.sp=sigma,mweight=1/Peche.k$MOULE)
	Xe.res
	}
  
  
k=1
i=1
Xe.dframe.it=function(list.Xe,lNdev){
    lyears=names(lNdev)
    for (k in 1:length(lyears)){
      Xek=list.Xe[[k]]
      Ndev=lNdev[[k]]
      N=length(Ndev)
		  for (i in 1:N){
		  	Xei=Xek[[i]]
			  Xei$NDEV=paste('D',Ndev[i],sep='')
        Xei$cruise=lyears[k]
		  	if (i==1) {
				  Xe.dbi=Xei
  			}else{
		  		Xe.dbi=rbind(Xe.dbi,Xei)
		  	}
	  	}
      if (k==1) {
			  Xe.db=Xe.dbi
  		}else{
		  	Xe.db=rbind(Xe.db,Xe.dbi)
		  }
    }  
	  Xe.db
	}
  
  Xe.check=function(list.Xe,Ndev){
		N=length(Ndev)
		for (i in 1:N){
			Xei=list.Xe[[i]]
			Xei$NDEV=paste('D',Ndev[i],sep='')
			if (i==1) {
				Xe.db=Xei
			}else{
				Xe.db=rbind(Xe.db,Xei)
			}
		}
	Xe.db
	}

	XpandIt=function(Xe.df,zname){
		Xe.df.wide=reshape(Xe.df[,c('CAMPAGNE','NOCHAL','LATF','LONF',
			'CodEsp',zname)],idvar=c("CAMPAGNE","NOCHAL","LATF","LONF"),
			timevar="CodEsp",direction="wide")
		head(Xe.df.wide)
		col.NArem=names(Xe.df.wide)[!names(Xe.df.wide)%in%c('CAMPAGNE',
			'NOCHAL','LATF','LONF')]

		for (i in 1:length(col.NArem)){
			Xe.df.wide[is.na(Xe.df.wide[,col.NArem[i]]),
			col.NArem[i]]=0
		}
		head(Xe.df.wide)

		Xe.df.long=reshape(Xe.df.wide,direction="long",
			idvar=c('CAMPAGNE','NOCHAL','LATF','LONF'),
			varying=list(names(Xe.df.wide)[!names(Xe.df.wide)%in%c('CAMPAGNE',
			'NOCHAL','LATF','LONF')]),v.names='value',timevar='variable',
			times=names(Xe.df.wide)[!names(Xe.df.wide)%in%c('CAMPAGNE',
			'NOCHAL','LATF','LONF')])
		head(Xe.df.long)
		row.names(Xe.df.long)=seq(dim(Xe.df.long)[1])
		varsplit=t(data.frame(strsplit(Xe.df.long$variable,split="\\.")))
		Xe.df.long$CodEsp=varsplit[,2]
		Xe.df.long$tvar=varsplit[,1]
		list(wide=Xe.df.wide,long=Xe.df.long)
	}



