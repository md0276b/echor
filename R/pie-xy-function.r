	pie.xy=function(x,y,z,pcol=seq(dim(z)[2]),draw1=TRUE,pradius=1,bar=FALSE,
	                plot.grid=FALSE,mtitle='',smax=1,pcoast=TRUE,bathy=FALSE,
	                manfa=NULL,ylim=NULL,xlim=NULL,xname='long',yname='lat',
	                zname='depth',ux11=TRUE,aspi=NA,addit=FALSE,...){
    
		library(gridBase)
	  library(grid)

		oldpar <- par(no.readonly = TRUE)

		z=as.matrix(z)
		
		Nx=length(x)
		Ny=length(y)

		if (draw1){
		  if (addit){
		    
		  }else{
		    if (ux11) x11()
		    if (bathy){
		      BiscayBathyPlot()
		    }else{
		      if (length(ylim)==1){
		        ylim=c(min(y)-((max(y)-min(y))/10),max(y)+((max(y)-min(y))/10))
		      }
		      if (length(xlim)==1){
		        xlim=c(min(x)-abs((max(x)-min(x))/10),max(x)+abs((max(x)-min(x))/10))
		      }
		      par(bg='white')
		      plot(x, y,xlim=xlim,ylim=ylim,type = "n",main=mtitle,asp=aspi)
		      #,...)
		    }
		  }
		}  
  		if (pcoast) coast()
		  if (!is.null(manfa)){
        manfa=manfa[order(manfa[,xname]),]
		    polygon(c(min(manfa[,xname]),manfa[,xname],max(manfa[,xname])),
                c(-250,-manfa[,zname],-250),col='red4')
		  }
  		vps <- baseViewports()
		  #grid.show.layout(vps, vp=viewport(width=1.25, height=1.25))
  		pushViewport(vps$inner, vps$figure, vps$plot)
	  	grid.segments(x0 = unit(c(rep(0, Nx), x), rep(c("npc", "native"),
	  		each = Nx)), x1 = unit(c(x, x), rep("native", Nx*2)), 
	  		y0 = unit(c(y,rep(0, Ny)), rep(c("native", "npc"), each = Ny)),
	  		y1 = unit(c(y,y), rep("native", Ny*2)), 
	  		gp = gpar(lty = "dashed", col = "grey"),draw=plot.grid)

		  maxpiesize <- unit(smax, "inches")
		  totals <- apply(z, 1, sum)
		  if (sum(totals)>0){
		    sizemult <- totals/max(totals)
		    
		    for (i in 1:Nx) {
		      if (sizemult[i]>1e-3){
		        pushViewport(viewport(x = unit(x[i], "native"), y = unit(y[i],
		                                                                 "native"), width = sizemult[i]*maxpiesize, 
		                              height = sizemult[i]*maxpiesize))
		        grid.rect(gp = gpar(col = "white", fill = 0, 
		                            lty = 0))
		        #par(plt = gridPLT(), new = TRUE,mar = rep(0, 4))
		        par(plt = gridPLT(), new = TRUE)
		        if (bar) {
		          barplot(z[i,],col=pcol,axes=F,xlab='',ylab='',
		                  names.arg = '')
		        }else {
		          pie(z[i,],radius = pradius,labels = rep("", 2),
		              col=pcol)
		        }
		        popViewport()
		      }else{
		        points(x[i],y[i],cex=0.1)
		      }
		    }
		    popViewport(3)
		    par(oldpar)
		  }else{
		    cat('Only zeroes, nothing plotted','\n')
		  }
	}

	#Example
	#--------------
#   uncomment lines below and run them for an example
# 	x <- c(0.88, 1, 0.67, 0.34)
# 	y <- c(0.87, 0.43, 0.04, 0.94)
# 	z <- matrix(runif(4 * 2), ncol = 2)
# 
#   x11()
# 	pie.xy(x,y,z,pcol=c('green1','red2'))
# 	pie.xy(x,y,z,bar=T)
# 
# 	graphics.off()

			
