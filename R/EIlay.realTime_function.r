
EIlay.realTime=function(path.lay,dstart=NULL,dend=NULL,plotit=list(SA=TRUE,SV=TRUE,map=TRUE),ylimSv=c(-80,-20),
                        SAmax=1e5,filter.SA=TRUE){
  
  #list .csv files in folder path
  ##---------------------------
  lf.lay=list.files(path.lay,pattern='*.csv')
  N=length(lf.lay)
  lfs.lay=data.frame(t(data.frame(strsplit(lf.lay,split='_',))))
  row.names(lfs.lay)=seq(length(lfs.lay[,1]))
  Ncol=dim(lfs.lay)[2]
  names(lfs.lay)=c('label','date','time','channel','f','type')[1:Ncol]
  lfs.lay$ID=paste(lfs.lay$date,lfs.lay$time,lfs.lay$f,sep='-')
  lfs.lay$t=strptime(paste(lfs.lay$date,lfs.lay$time),'%Y%m%d %H%M%S')
  length(lf.lay)
  length(lfs.lay$t>strptime(as.character(dstart),"%Y-%m-%d %H:%M:%S"))
  
  lf.lays=lf.lay
  if (!is.null(dstart)){
    lf.lays=lf.lays[lfs.lay$t>strptime(as.character(dstart),"%Y-%m-%d %H:%M:%S")]
  }
  if (!is.null(dend)){
    lf.lays=lf.lays[lfs.lay$t<strptime(as.character(dend),"%Y-%m-%d %H:%M:%S")]
  }
  if (length(lf.lays)==0){
    lf.lays=lf.lay
    cat('No EIlay files pre-subsetting','\n')
  } 
  
  N=length(lf.lays)
  
  #import files in folder path
  ##---------------------------
  for (i in 1:N){
    pathi=paste(path.lay,lf.lays[i],sep='')
    layi=read.table(pathi,sep=';',header=TRUE)
    head(layi)    
    if (i==1){
      lays=layi
    }else{
      lays=rbind(lays,layi)
    }
  }  
  
  head(lays)
  
  # Creates Sv and sA matrices
  
  SVtotal <- lays [lays$MOVIES_EILayer.cellset.celltype == 4,
                   c('MOVIES_EILayer.sndset.freq','MOVIES_EILayer.shipnav.lat',
                     'MOVIES_EILayer.shipnav.long','MOVIES_EILayer.cellset.datestart','MOVIES_EILayer.eilayer.sv')]
  names(SVtotal)=c('f','y','x','t','Sv')
  
  SAtotal <- lays [lays$MOVIES_EILayer.cellset.celltype == 4,
                   c('MOVIES_EILayer.sndset.freq','MOVIES_EILayer.shipnav.lat',
                     'MOVIES_EILayer.shipnav.long','MOVIES_EILayer.cellset.datestart','MOVIES_EILayer.eilayer.sa')]
  names(SAtotal)=c('f','y','x','t','sA')
  
  SVsurf1 <- lays [lays$MOVIES_EILayer.cellset.celltype == 0,
                   c('MOVIES_EILayer.sndset.freq','MOVIES_EILayer.shipnav.lat',
                     'MOVIES_EILayer.shipnav.long','MOVIES_EILayer.cellset.datestart','MOVIES_EILayer.eilayer.sv')]
  names(SVsurf1)=c('f','y','x','t','Sv')
  
  SVsurf2 <- lays [lays$MOVIES_EILayer.cellset.cellnum %in% c(0,1,2),
                   c('MOVIES_EILayer.sndset.freq','MOVIES_EILayer.shipnav.lat',
                     'MOVIES_EILayer.shipnav.long','MOVIES_EILayer.cellset.datestart','MOVIES_EILayer.eilayer.sv')]
  names(SVsurf2)=c('f','y','x','t','Sv')
  
  # sum surface layers
  if (dim(SVsurf1)[1]>0){
    SVtotal=aggregate(10^(SVsurf1$Sv/10),list(f=SVsurf1$f,y=SVsurf1$y,x=SVsurf1$x,t=SVsurf1$t),sum)
    names(SVtotal)[5]='sv'
    SVtotal$Sv=10*log10(SVtotal$sv)
    head(SVtotal)
    SVsurf=aggregate(10^(SVsurf2$Sv/10),list(f=SVsurf2$f,y=SVsurf2$y,x=SVsurf2$x,t=SVsurf2$t),sum)
    names(SVsurf)[5]='sv'
    SVsurf$Sv=10*log10(SVsurf$sv)  
  }else{
    SVtotal=SVtotal[FALSE,]
    SVsurf=SVtotal[FALSE,]
    cat('No surface layers in the EI files','\n')
  }  
  
  SAsurf1 <- lays [lays$MOVIES_EILayer.cellset.celltype == 0,
                   c('MOVIES_EILayer.sndset.freq','MOVIES_EILayer.shipnav.lat',
                     'MOVIES_EILayer.shipnav.long','MOVIES_EILayer.cellset.datestart','MOVIES_EILayer.eilayer.sa')]
  names(SAsurf1)=c('f','y','x','t','sA')
  head(SAsurf1)
  SAsurf1[SAsurf1$sA<0,'sA']=0
  
  SAsurf2 <- lays [lays$MOVIES_EILayer.cellset.cellnum %in% c(0,1,2),
                   c('MOVIES_EILayer.sndset.freq','MOVIES_EILayer.shipnav.lat',
                     'MOVIES_EILayer.shipnav.long','MOVIES_EILayer.cellset.datestart','MOVIES_EILayer.eilayer.sa')]
  names(SAsurf2)=c('f','y','x','t','sA')
  SAsurf2[SAsurf2$sA<0,'sA']=0
  
  # sum surface layers
  if (dim(SAsurf1)[1]>0){
    SAtotal=aggregate(SAsurf1$sA,list(f=SAsurf1$f,y=SAsurf1$y,x=SAsurf1$x,t=SAsurf1$t),sum)
    names(SAtotal)[5]='sA'
    head(SAtotal)
    SAsurf=aggregate(SAsurf2$sA,list(f=SAsurf2$f,y=SAsurf2$y,x=SAsurf2$x,t=SAsurf2$t),sum)
    names(SAsurf)[5]='sA'
    head(SAsurf)
  }else{
    SAtotal=SAtotal[FALSE,]
    SAsurf=SAtotal[FALSE,]
  } 
  
  SVbott <- lays [lays$MOVIES_EILayer.cellset.celltype == 1,
                  c('MOVIES_EILayer.sndset.freq','MOVIES_EILayer.shipnav.lat',
                    'MOVIES_EILayer.shipnav.long','MOVIES_EILayer.cellset.datestart','MOVIES_EILayer.eilayer.sv')]
  names(SVbott)=c('f','y','x','t','Sv')
  head(SVbott)
  summary(SVbott$Sv)
  
  SAbott <- lays [lays$MOVIES_EILayer.cellset.celltype == 1,
                  c('MOVIES_EILayer.sndset.freq','MOVIES_EILayer.shipnav.lat',
                    'MOVIES_EILayer.shipnav.long','MOVIES_EILayer.cellset.datestart','MOVIES_EILayer.eilayer.sa')]
  names(SAbott)=c('f','y','x','t','sA')
  SAbott[SAbott$sA<0,'sA']=0
  
  if (dim(SVsurf)[1]>0){
    head(SVsurf)
    SVsurfw=reshape(SVsurf[,names(SVsurf)!='sv'],v.names = "Sv", idvar = c("t","x","y"),timevar = "f", direction = "wide")
    SVsurfw$t=strptime(as.character(SVsurfw$t),"%Y-%d-%m %H:%M:%OS")
    head(SVsurfw)
  }
  
  SVbottw=reshape(SVbott,v.names = "Sv", idvar = c("t","x","y"),timevar = "f", direction = "wide")
  SVbottw$t=strptime(as.character(SVbottw$t),"%Y-%d-%m %H:%M:%OS")
  head(SVbottw)
  
  SVtotalw=reshape(SVtotal[,names(SVtotal)!='sv'],v.names = "Sv", idvar = c("t","x","y"),timevar = "f", 
                   direction = "wide")
  SVtotalw$t=strptime(as.character(SVtotalw$t),"%Y-%d-%m %H:%M:%OS")
  head(SVtotalw)
  
  range(SVtotalw$t)
  
  head(SAsurf)
  SAsurfw=reshape(SAsurf,v.names = "sA", idvar = c("t","x","y"),timevar = "f", direction = "wide")
  SAsurfw$t=strptime(as.character(SAsurfw$t),"%Y-%d-%m %H:%M:%OS")
  head(SAsurfw)
  
  SAbottw=reshape(SAbott,v.names = "sA", idvar = c("t","x","y"),timevar = "f", direction = "wide")
  SAbottw$t=strptime(as.character(SAbottw$t),"%Y-%d-%m %H:%M:%OS")
  head(SAbottw)
  
  head(SAtotal)
  SAtotalw=reshape(SAtotal,v.names = "sA", idvar = c("t","x","y"),timevar = "f", direction = "wide")
  SAtotalw$t=strptime(as.character(SAtotalw$t),"%Y-%d-%m %H:%M:%OS")
  head(SAtotal)
  
  range(SAsurfw$t)
  
  # graphique
  
  SVtotalw=SVtotalw[order(SVtotalw$t),]
  SVbottw=SVbottw[order(SVbottw$t),]
  SVsurfw=SVsurfw[order(SVsurfw$t),]
  
  #tdeb='2013-04-28 05:00:00'
  #tend=NULL
  SVtotalws=SVtotalw
  SAtotalws=SAtotalw
  SVbottws=SVbottw
  SAbottws=SAbottw
  SVsurfws=SVsurfw
  SAsurfws=SAsurfw
  
  if (!is.null(dstart)){
    SVtotalws=SVtotalws[SVtotalws$t>strptime(as.character(dstart),"%Y-%m-%d %H:%M:%S"),]
    SAtotalws=SAtotalws[SAtotalws$t>strptime(as.character(dstart),"%Y-%m-%d %H:%M:%S"),]
    SVbottws=SVbottws[SVbottws$t>strptime(as.character(dstart),"%Y-%m-%d %H:%M:%S"),]
    SAbottws=SAbottws[SAbottws$t>strptime(as.character(dstart),"%Y-%m-%d %H:%M:%S"),]
    SVsurfws=SVsurfws[SVsurfws$t>strptime(as.character(dstart),"%Y-%m-%d %H:%M:%S"),]
    SAsurfws=SAsurfws[SAsurfws$t>strptime(as.character(dstart),"%Y-%m-%d %H:%M:%S"),]
    head(SVsurfws)  
    summary(SAsurfws$sA.38)  
    summary(SAsurfws$sA.120)  
  }else{
    dstart=min(SVtotalws$t)
  }
  if (!is.null(dend)){
    SVtotalws=SVtotalws[SVtotalws$t<strptime(as.character(dend),"%Y-%m-%d %H:%M:%S"),]
    SAtotalws=SAtotalws[SAtotalws$t<strptime(as.character(dend),"%Y-%m-%d %H:%M:%S"),]
    SVbottws=SVbottws[SVbottws$t<strptime(as.character(dend),"%Y-%m-%d %H:%M:%S"),]
    SAbottws=SAbottws[SAbottws$t<strptime(as.character(dend),"%Y-%m-%d %H:%M:%S"),]
    SVsurfws=SVsurfws[SVsurfws$t<strptime(as.character(dend),"%Y-%m-%d %H:%M:%S"),]
    SAsurfws=SAsurfws[SAsurfws$t<strptime(as.character(dend),"%Y-%m-%d %H:%M:%S"),]
  }else{
    dend=max(SVtotalws$t)  
  }
  
  if (filter.SA){
    SAtotalws.be=SAtotalws[SAtotalws$sA.38>1e5,]
    SAtotalws=SAtotalws[SAtotalws$sA.38<1e5,]
    SAbottws.be=SAbottws[SAbottws$sA.38>1e5,]
    SAbottws=SAbottws[SAbottws$sA.38<1e5,]
    SAsurfws.be=SAsurfws[SAsurfws$sA.38>1e5,]
    SAsurfws=SAsurfws[SAsurfws$sA.38<1e5,]
  }
  
  
  if (plotit$SV){
    x11(20,15); par(mfrow=c(2,1), las=2) 
    plot (SVtotalws$t, SVtotalws$Sv.38, type='l', col=1, lwd=2, ylim=ylimSv, 
          main=paste('38 kHz,',dstart,'-',dend),lty=1,xlab='',ylab='Sv (dB)')
    lines (SVbottws$t, SVbottws$Sv.38, col=2, lwd=2, lty=1)
    lines (SVsurfws$t, SVsurfws$Sv.38, col=3, lwd=2)
    legend('topleft',c('SV total','SV fond','SV surface'), col=c(1,2,3), lty=1,lwd=2)
    
    plot (SVtotalws$t, SVtotalws$Sv.120, type='l', col=1, lwd=2, ylim=ylimSv, 
          main=paste('120 kHz,',dstart,'-',dend),lty=1,xlab='',ylab='Sv (dB)')
    lines (SVbottws$t, SVbottws$Sv.120, col=2, lwd=2, lty=1)
    lines (SVsurfws$t, SVsurfws$Sv.120, col=3, lwd=2)
    legend('topleft',c('SV total','SV fond','SV surface'), col=c(1,2,3), lty=1,lwd=2)
    
    x11(20,15); par(mfrow=c(2,1), las=2) 
    plot (SVtotalws$t, 10*log10(cumsum(10^(SVtotalws$Sv.38/10))), type='l', col=1, lwd=2, ylim=c(-80,0), 
          main=paste('38 kHz,',dstart,'-',dend),lty=1,xlab='',ylab='Sv cumulative sum (dB)')
    lines (SVbottws$t, 10*log10(cumsum(10^(SVbottws$Sv.38/10))), col=2, lwd=2, lty=1)
    lines (SVsurfws$t, 10*log10(cumsum(10^(SVsurfws$Sv.38/10))), col=3, lwd=2)
    legend('topleft',c('SV total','SV fond','SV surface'), col=c(1,2,3), lty=1,lwd=2)
    
    plot (SVtotalws$t, 10*log10(cumsum(10^(SVtotalws$Sv.120/10))), type='l', col=1, lwd=2, ylim=c(-80,0), 
          main=paste('120 kHz,',dstart,'-',dend),lty=1,xlab='',ylab='Sv cumulative sum (dB)')
    lines (SVbottws$t, 10*log10(cumsum(10^(SVbottws$Sv.120/10))), col=2, lwd=2, lty=1)
    lines (SVsurfws$t, 10*log10(cumsum(10^(SVsurfws$Sv.120/10))), col=3, lwd=2)
    legend('topleft',c('SV total','SV fond','SV surface'), col=c(1,2,3), lty=1,lwd=2)
    
  }
  if (plotit$SA){
    x11(20,15); par(mfrow=c(2,1), las=2) 
    par(mfrow=c(2,1))
    sA.max=max(max(SAsurfw[!is.na(SAsurfw$sA.38),'sA.38']),max(SAbottw[!is.na(SAbottw$sA.38),'sA.38']))
    if (sA.max>1e5) sA.max=SAmax
    plot (SAtotalws$t, SAtotalws$sA.38, type='l', col=1, lwd=2,main=paste('38 kHz,',dstart,'-',dend),lty=1,
          ylim=c(0,sA.max),ylab='NASC, m^2.NM^-2',xlab='time')
    lines (SAbottws$t, SAbottws$sA.38, col=2, lwd=2, lty=1)
    lines (SAsurfws$t, SAsurfws$sA.38, col=3, lwd=2)
    legend('topleft',c('sA total','sA fond','sA surface'), col=c(1,2,3), lty=1,lwd=2)
    sA.max=max(max(SAsurfws[!is.na(SAsurfws$sA.120),'sA.120']),max(SAbottws[!is.na(SAbottws$sA.120),'sA.120']))
    if (sA.max>1e5) sA.max=SAmax
    plot (SAtotalws$t, SAtotalws$sA.120, type='l', col=1, lwd=2,main=paste('120 kHz,',dstart,'-',dend),lty=1,
          ylim=c(0,sA.max),ylab='NASC, m^2.NM^-2',xlab='time')
    lines (SAbottws$t, SAbottws$sA.120, col=2, lwd=2, lty=1)
    lines (SAsurfws$t, SAsurfws$sA.120, col=3, lwd=2)
    legend('topleft',c('sA total','sA fond','sA surface'), col=c(1,2,3), lty=1,lwd=2)
    
    x11(20,15); par(mfrow=c(2,1), las=2) 
    plot (SAtotalws$t, cumsum(SAtotalws$sA.38), type='l', col=1, lwd=2, ylim=c(0,SAmax), 
          main=paste('38 kHz,',dstart,'-',dend),lty=1,xlab='',ylab='sA cumulative sum (m^2/MN^2)')
    lines (SAbottws$t, cumsum(SAbottws$sA.38), col=2, lwd=2, lty=1)
    lines (SAsurfws$t, cumsum(SAsurfws$sA.38), col=3, lwd=2)
    legend('topleft',c('SA total','SA fond','SA surface'), col=c(1,2,3), lty=1,lwd=2)
    
    plot (SAtotalws$t, cumsum(SAtotalws$sA.120), type='l', col=1, lwd=2, ylim=c(0,SAmax), 
          main=paste('120 kHz,',dstart,'-',dend),lty=1,xlab='',ylab='sA cumulative sum (m^2/MN^2)')
    lines (SAbottws$t, cumsum(SAbottws$sA.120), col=2, lwd=2, lty=1)
    lines (SAsurfws$t, cumsum(SAsurfws$sA.120), col=3, lwd=2)
    legend('topleft',c('SA total','SA fond','SA surface'), col=c(1,2,3), lty=1,lwd=2)
    
  }
  
  if (plotit$map){
    x11()
    plot (SAtotalws$x, SAtotalws$y,cex=0.1+SAtotalws$sA.38/1000, type='p', col=1, 
          main=paste('38 kHz,',dstart,'-',dend),xlab='',ylab='')
  }
  
  list(SVtotal,SVsurf,SVbott,SAtotal,SAsurf,SAbott,SVtotalws,SVsurfws,SVbottws,SAtotalws,SAsurfws,SAbottws)
  
}

