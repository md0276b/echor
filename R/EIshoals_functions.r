# path.shli='/home/mathieu/Documents/R/data/results_PG01R11_20150313_115204_id50_120kHz_shl.csv'
# 
# shl1=import.shl.csv(path.shli)
# shl2=import.shl.csv(path.shli)
# shls=rbind(shl1,shl2)

import.shl.csv=function(path.shli){
  
  cat(path.shli,'\n')
  
  #Import shl files and merge with lay files
  #----------------
  shl0=read.csv(file=path.shli,sep=";",header=TRUE,row.names=NULL)
  head(shl0)
  names(shl0)
  lesus=unique(shl0$MOVIES_EIShoal.cellset.dateend)
  dim(shl0)
  #remove NA columns
  shl0=shl0[,!names(shl0)%in%c('X','X.1')]
  names0=names(shl0)
  #simplify column names
  lnames0=strsplit(names(shl0),split='\\.')
  library(plyr)
  for (i in 1:length(lnames0)){
    li=data.frame(t(lnames0[[i]]))
    if (i==1){
      lnames=data.frame(li)
    }else{
      lnames=rbind.fill(lnames,li)
    }
  }
  lnames$nnames=paste(as.character(lnames$X2),as.character(lnames$X3),sep='.')
  lnames[!is.na(lnames$X4),'nnames']=paste(lnames[!is.na(lnames$X4),'nnames'],
                                           lnames[!is.na(lnames$X4),'X4'],sep='.')
  lnames$nnames[1]='procDate'
  shl=shl0
  names(shl)=lnames$nnames
  head(shl)
  shl
}  

# lnames=data.frame(names0,lnames)
# write.table(lnames,'/home/mathieu/Documents/svn/trunk/EchoRpackage/EchoR/data/shlCsvNames.csv',sep=';',
#             row.names=FALSE)

