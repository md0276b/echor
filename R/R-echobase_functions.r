#**************************************************************************************************
#Function to produce Echobase text input files, based on data in Ifremer format (casino, EchoR)
#**************************************************************************************************

ifremeR2Echobase=function(path.cas,PecheiEB=NULL,mensEB=NULL,laytot=NULL,LW=NULL,LA=NULL,
                          paramBio=NULL,LW.tsa=NULL,EspDev=NULL,ESDUDEVs=NULL,
                          calibration=NULL,chamens.long=NULL,mantot=NULL,
                          biomres=NULL,biomres.pond=NULL,Biomres.echotype.codesp.size=NULL,
                          Biomres.sp.age=NULL,path.fishmap=NULL,path.otherMaps=NULL,
                          survey.polys=NULL,CV.df=NULL,Ind.sp.all=NULL,deszones=deszones,ancillary=NULL,
                          casino.check=TRUE,
                          convertMan2lay=FALSE,sep.cas='\t',path.export,cruise,
                          simplifyVTT=TRUE,
                          sciVesselCode='PEL',
                          vesselName='THALASSA II',nonEventsCodes=c('CAMP',''),
                          nonActions=c('NEXT','COM','DCONFIG','FCONFIG','NCONFIG','DPARASITESME70','FPARASITESME70',
                                       'DPARASITESER60','FPARASITESER60','ECHO','DEBPOI','DEBSSL','FINSSL','FINPOI','INTERAD'),
                          radialeCode='Rad ',surfInstruments=c('CUFIX'),
                          septs.df=data.frame(Code.Appareil=c('ACOU','CUFIX','76x70','57x52','942OBS','EROCENROL','MIK','MEPEL',
                                                              'CARRE','MINOF','CAMPAGNE','TRANSIT','INTER','932OBS','725OBS','PTM','PS'),
                                              spts=c('DEBURAD','STR','DFIL','DFIL','DFIL','DFIL','DEBFIL','DFIL','DFIL','DEBVIR',
                                                     'DEBUCAMP','DEBULEG','FINRAD','DFIL','DFIL','DFIL','DFIL'),
                                              epts=c('FINRAD','STP','DVIR','DVIR','DVIR','DVIR','FINVIR','FVIR','FVIR','FINVIR','FINCAMP',
                                                     'FINLEG','DEBURAD','DVIR','DVIR','DVIR','DVIR'),
                                              code.app=c('ACOU','HYDROBIO','FISH','FISH','FISH','FISH','FISH','FISH','FISH','HYDROBIO',
                                                         'CAMPAGNE','CAMPAGNE','ACOU','FISH','FISH','FISH','FISH'),
                                              gear.type=c('ACOU','CUFES','TRAWL','TRAWL','TRAWL','EROCENROL','MICRON','MICRON','ZOOPL','ZOOPL',
                                                          'CAMPAGNE','CAMPAGNE','ACOU','TRAWL','TRAWL','TRAWL','TRAWL'),
                                              Echobasegearcode=c(NA,NA,'76x70','57x52','PTM','57x52 ENROL','MIK','MEPEL','CARRE','MINOF',NA,NA,NA,
                                                                 '69','PS','PTM','PS'),
                                              importGear=c(FALSE,FALSE,rep(TRUE,4),rep(FALSE,7),rep(TRUE,4))),trawlCode='TRAWL',
                          lcodes=data.frame(codename=c('numStationName','xname','yname','zname','codeCamp','codeTransit','codeInter'),
                                            code=c('Num Station','Longitude','Latitude','CINNA Sonde vert. (m)','CAMPAGNE','TRANSIT',
                                                   'INTER')),blApp=c('CUFIX','PROFILEUR'),
                          lay.format='csv',strata='Golfe de Gascogne',chamens.sufix='.CREF',check.ref=TRUE,
                          host='acoustica',dbname="Echobase-test",user='echobase',
                          password='echobase',port=5432,CorrectChannelIdent=NULL,
                          beamGainCor=NULL,beamSACor=NULL,powerCor=NULL,algAsensCor=NULL,
                          atwAsensCor=NULL,CorrectAbsorption=NULL,CorrectSoundCelerity=NULL,
                          areaCor=1,volCor=NA,milli.correct=TRUE,
                          validChannels=data.frame(id=c(seq(46,51,1),seq(91,111,1),136),
                                                                      type=c(rep('mono',6),rep('multi',length(seq(91,111,1))),'mono'),
                                                                      sel=c(FALSE,TRUE,rep(FALSE,26))),
                          TotCatchOpeCor=TRUE,neg.nasc.remove=TRUE,strate.ok=c('CLAS','SURF','NULL'),
                          removeBadCatch=FALSE,removeBadSubSamples=TRUE,correct.nullWatLength=TRUE,
                          portTable=data.frame(name=c('Brest','Lorient','Santander','Concarneau','La Rochelle'),
                                                                    code=c('BSH29','BSH4257','BSH189','BSH4265','BSH108')),
                          cnames=c("Code.Action","Date","Strate","Observation","Num.Station","Heure",
                                   "CINNA.Sonde.vert...m.","Latitude","Longitude","Code.Appareil",
                                   "FUNES.L..filee.Tribord..m.","CINNA.Vit..fond..nd.",
                                   "MARPORT.Ouv..verticale..m.","MARPORT.Ouv..horizontale..m.",
                                   "MARPORT.Dist..des.panneaux..m.",
                                   "FUNES.L..filee.Babord..m.","FUNES.Tension.babord..daN.",
                                   "CINNA.Cap..deg.",
                                   "MARPORT.Vit..longitudinale..m.s.","CINNA.Cap.derive..deg.",
                                   "CINNA.Vitesse.derive..nd.","METBA.Dir..vent.vrai..deg.",
                                   "METBA.Vit..vent.vrai..nd.","MARPORT.Altitude.corde.de.fond..m."),
                          fMovies='M3D',LW.check=FALSE){
  # 1. Extract codes ----
  #******************** ----
  lcodes$codename=as.character(lcodes$codename)
  lcodes$code=as.character(lcodes$code)
  for (i in 1:dim(lcodes)[1]){
    assign(lcodes$codename[i],lcodes$code[i])
  }

  #*********************************************************
  #2. Voyage, transit, transect ----
  #********************************************************* ------ 
  if (!is.null(path.cas)){
    #2.1. Import casino log file ----
    #*******************************
    if (casino.check){
      cat('Checking Casino file consistency...','\n')
      cio=casino.import(path.cas,check.strates=TRUE,stp.gears=unique(septs.df[septs.df$gear.type=='TRAWL','Code.Appareil']),
                        start.events=unique(septs.df[septs.df$gear.type%in%c('TRAWL','ACOU'),'spts']),
                        seq.stop=unique(septs.df[septs.df$gear.type%in%c('TRAWL','ACOU'),'epts']),
                        gear.events=c(as.character(unique(septs.df[septs.df$gear.type%in%c('TRAWL'),'spts'])),
                                      as.character(unique(septs.df[septs.df$gear.type%in%c('TRAWL'),'epts']))),
                        survey.events=c(as.character(unique(septs.df[septs.df$gear.type%in%c('CAMPAGNE'),'spts'])),
                                        as.character(unique(septs.df[septs.df$gear.type%in%c('CAMPAGNE'),'epts']))),
                        get.jackpot=FALSE)
    }
    cat('Importing Casino log file...','\n')
    cas=read.csv(path.cas,sep=sep.cas,header=TRUE)
    casn=read.csv(path.cas,header=FALSE,sep=sep.cas)
    casn=casn[1,]
    names(cas)
     # convert xname, yname and zname in casino name format to RcasinoName format
    RcasinoNames=data.frame(Rname=names(cas),casName=as.character(t(casn)))
    RcasinoNames=RcasinoNames[!is.na(RcasinoNames$casName),]
    if (sum(RcasinoNames$casName%in%xname)==0) stop('Check Casino xname')
    if (sum(RcasinoNames$casName%in%yname)==0) stop('Check Casino yname')
    if (sum(RcasinoNames$casName%in%zname)==0) stop('Check Casino zname')
    if (sum(RcasinoNames$casName%in%numStationName)==0){
      if (sum(RcasinoNames$Rname%in%numStationName)!=0){
        numStationName=as.character(RcasinoNames[RcasinoNames$Rname==numStationName,'casName'])
      }else{
        stop('Check Casino num station name')
      } 
    }
    rxname=as.character(RcasinoNames[RcasinoNames$casName==xname,'Rname'])
    ryname=as.character(RcasinoNames[RcasinoNames$casName==yname,'Rname'])
    rzname=as.character(RcasinoNames[RcasinoNames$casName==zname,'Rname'])
    RcasinoNames[,1]=as.character(RcasinoNames[,1])
    RcasinoNames[,2]=as.character(RcasinoNames[,2])
    #names(cas)[14]='N..Station'
    #
    nstname=RcasinoNames[RcasinoNames$casName==numStationName,'Rname']
    vir.codes=unique(septs.df[septs.df$gear.type==trawlCode,'epts'])
    fil.codes=unique(septs.df[septs.df$gear.type==trawlCode,'spts'])
    Nhauls=length(cas[cas$Code.Appareil%in%unique(septs.df[septs.df$gear.type==trawlCode,'Code.Appareil'])&
                        cas$Code.Action%in%unique(septs.df[septs.df$gear.type==trawlCode,'epts']),nstname])
    cat('No of trawl hauls:',Nhauls,'\n')
    
    #names(cas)
    #*********************************************************
    #2.2. CommonData: Voyage and Transit ----- 
    #*********************************************************
    #2.2.1. Voyage input file ---------
    #******************
    eVoyage=as.character(unlist(septs.df[septs.df$Code.Appareil==codeCamp,
                                         c('spts','epts')]))
    Voyagei=cas[cas$Code.Appareil=='CAMP'&cas$Code.Action%in%eVoyage,]
    if (length(unique(Voyagei$N..Station))>1) stop ('Voyage has non-unique name')
    if (dim(Voyagei)[1]!=2) stop('Missing cruise start or end, please check input file')
    
    Voyage=data.frame(name=Voyagei[1,14])
    Voyage$startDate=Voyagei[Voyagei$Code.Action==eVoyage[1],'Date']
    Voyage$startDate=paste(Voyagei[Voyagei$Code.Action==eVoyage[1],'Date'],
                           Voyagei[Voyagei$Code.Action==eVoyage[1],'Heure'])
    Voyage$startDate=format(strptime(Voyage$startDate,"%d/%m/%Y %H:%M:%OS",tz="GMT"),
                            "%Y-%m-%d %H:%M:%OS")
    Voyage$startDate=paste(Voyage$startDate,'.0000',sep='')
    #Voyage$startDate=format(strptime(Voyage$startDate,"%d/%m/%Y"),
    #                         "%Y-%m-%d")
    Voyage$endDate=paste(Voyagei[Voyagei$Code.Action==eVoyage[2],'Date'],
                         Voyagei[Voyagei$Code.Action==eVoyage[2],'Heure'])
    Voyage$endDate=format(strptime(Voyage$endDate,"%d/%m/%Y %H:%M:%OS",tz="GMT"),
                          "%Y-%m-%d %H:%M:%OS")
    Voyage$endDate=paste(Voyage$endDate,'.0000',sep='')
    #Voyage$endDate=format(strptime(Voyage$endDate,"%d/%m/%Y"),
    #                        "%Y-%m-%d")
    Voyage$startPort=Voyagei[Voyagei$Code.Action==eVoyage[1],'Strate']
    Voyage$endPort=Voyagei[Voyagei$Code.Action==eVoyage[2],'Strate']
    startPortc=merge(Voyage[,c('name','startPort')],portTable,by.x='startPort',by.y='name')
    if (dim(startPortc)[1]==0) stop('Voyage startPort not found in portTable, please add it')
    Voyage$startPort=startPortc$code
    endPortc=merge(Voyage[,c('name','endPort')],portTable,by.x='endPort',by.y='name')
    if (dim(endPortc)[1]==0) stop('Voyage endPort not found in portTable, please add it')
    Voyage$endPort=endPortc$code
    head(Voyage)
    cruise=as.character(unique(Voyage$name))
    #Export Voyage in Echobase format
    #create export folder if does not exist
    library(R.utils)
    mkdirs(path.export)
    
    write.table(Voyage,paste(path.export,cruise,'_Voyage4Echobase.csv',sep=''),
                row.names=FALSE,sep=';')
    
    cat('-> Voyage data exported','\n')
    
    #2.1.2. Transit input file -----------
    #******************
    eTransit=as.character(unlist(septs.df[septs.df$Code.Appareil==codeTransit,
                                          c('spts','epts')]))
    Transiti=cas[cas$Code.Appareil=='CAMP'&cas$Code.Action%in%eTransit,]
    tel=table(as.character(Transiti$Code.Action))
    if (dim(tel)!=0){
      if ((tel[1]!=tel[2])&!simplifyVTT){
        stop('Missing transit start or end, please check input file')
      }else if ((tel[1]!=tel[2])&!simplifyVTT){
        Transit=Voyage
        Transit=data.frame(voyage=Transit$name,description='simplified transit: transit=voyage',
                           Transit[,c('startDate','endDate','startPort','endPort')])
        names(Transit)=c("voyage","description","startTime","endTime","startLocality","endLocality")
        cat('Dummy transits created','\n')
      }else if ((tel[1]==tel[2])){
        Transit=data.frame(voyage=Voyage$name,
                           description=Transiti[Transiti$Code.Action==eTransit[1],'Identificateur.Operation'])
        Transit$startTime=paste(Transiti[Transiti$Code.Action==eTransit[1],'Date'],
                                Transiti[Transiti$Code.Action==eTransit[1],'Heure'])
        Transit$startTime=format(strptime(Transit$startTime,"%d/%m/%Y %H:%M:%OS",tz="GMT"),
                                 "%Y-%m-%d %H:%M:%OS")
        Transit$startTime=paste(Transit$startTime,'.0000',sep='')
        Transit$endTime=paste(Transiti[Transiti$Code.Action==eTransit[2],'Date'],
                              Transiti[Transiti$Code.Action==eTransit[2],'Heure'])
        Transit$endTime=format(strptime(Transit$endTime,"%d/%m/%Y %H:%M:%OS",tz="GMT"),
                               "%Y-%m-%d %H:%M:%OS")
        Transit$endTime=paste(Transit$endTime,'.0000',sep='')
        Transit$startLocality=Transiti[Transiti$Code.Action==eTransit[1],'Strate']
        Transit$endLocality=Transiti[Transiti$Code.Action==eTransit[2],'Strate']
        Transit=Transit[order(Transit$startTime),]
        startPortc=merge(Transit[,c('voyage','startLocality','startTime')],portTable,by.x='startLocality',by.y='name')
        startPortc=startPortc[order(startPortc$startTime),]
        if (dim(startPortc)[1]<dim(Transit)[1]) stop('Transit startPort not found in portTable, please add it')
        Transit$startLocality=startPortc$code
        endPortc=merge(Transit[,c('voyage','endLocality','startTime')],portTable,by.x='endLocality',by.y='name')
        endPortc=endPortc[order(endPortc$startTime),]
        if (dim(endPortc)[1]<dim(Transit)[1]) stop('Transit endPort not found in portTable, please add it')
        Transit$endLocality=endPortc$code
        cat('Transits created based on Casino metadata','\n')
      }  else if (simplifyVTT){
        Transit=Voyage
        Transit=data.frame(voyage=Transit$name,description='simplified transit: transit=voyage',
                           Transit[,c('startDate','endDate','startPort','endPort')])
        names(Transit)=c("voyage","description","startTime","endTime","startLocality","endLocality")
        cat('Dummy transits created','\n')
      }
    }else{
      Transit=Voyage
      Transit=data.frame(voyage=Transit$name,description='simplified transit: transit=voyage',
                         Transit[,c('startDate','endDate','startPort','endPort')])
      names(Transit)=c("voyage","description","startTime","endTime","startLocality","endLocality")
      cat('No transit metadata in Casino, dummy transits created based on voyage infos','\n')
    }
    head(Transit)
    #Export Transit in Echobase format
    write.table(Transit,paste(path.export,cruise,'_Transit4Echobase.csv',sep=''),
                row.names=FALSE,sep=';')
    
    cat('-> Transit data exported','\n')
    
    #*********************************************************
    # 2.1.3. Transect input file (vessel specific)--------
    #*********************************************************
    # Main vessel name == vesselName
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #Transect input file, Thalassa transects 
    #******************
    #select transects
    #******************
    gear.codes=septs.df[
      septs.df$importGear,c('Code.Appareil','Echobasegearcode')]
    exceptGear=unique(septs.df[!septs.df$importGear,c('Echobasegearcode')])
    exceptGear=as.character(exceptGear[!is.na(exceptGear)])
    if (length(exceptGear)==0) exceptGear=NULL

    if (simplifyVTT){
      # Create simplied acoustic transects: same as transit
      lapp=septs.df[,c('Code.Appareil','code.app')]
      lapps=lapp[lapp$Code.Appareil%in%unique(cas$Code.Appareil),]
      lapps=merge(lapps,gear.codes,by.x='Code.Appareil',by.y='Code.Appareil',
                  all.x=TRUE)
      lapps$Echobasegearcode=as.character(lapps$Echobasegearcode)
      lapps[is.na(lapps$Echobasegearcode),'Echobasegearcode']=
        as.character(lapps[is.na(lapps$Echobasegearcode),'Code.Appareil'])
      lappss=lapps[lapps$Code.Appareil%in%unique(cas$Code.Appareil),'Echobasegearcode']
      for (i in 1:length(lappss)){
        Transectfsi=Transit[,-c(5,6)]
        names(Transectfsi)=c('voyage','comments','timeCoverageStart','timeCoverageEnd')
        Transectfsi$title=paste('Simplified transect, ',lappss[i],'-',
                                Transectfsi$timeCoverageStart,'-',vesselName)
        Transectfsi$transectAbstract=lappss[i]
        Transectfsi$stratum='ALL'
        Transectfsi$vesselName='THALASSA II'
        Transectfsi$dateCreated=Transectfsi$timeCoverageStart
        Transectfsi$geospatialLonMin=min(as.numeric(gsub(',','.',cas[,rxname]))-0.1,
                                         na.rm=TRUE)
        Transectfsi$geospatialLatMin=min(as.numeric(gsub(',','.',cas[,ryname]))-0.1,
                                         na.rm=TRUE)
        Transectfsi$geospatialLonMax=max(as.numeric(gsub(',','.',cas[,rxname]))+0.1,
                                         na.rm=TRUE)
        Transectfsi$geospatialLatMax=max(as.numeric(gsub(',','.',cas[,ryname]))+0.1,
                                         na.rm=TRUE)
        Transectfsi$geospatialVerticalMax=200
        Transectfsi$geospatialVerticalMin=10
        Transectfsi$linestring=paste(paste(Transectfsi$geospatialLonMin,Transectfsi$geospatialLatMin),
                                    paste(Transectfsi$geospatialLonMax,Transectfsi$geospatialLatMax),sep=',')
        Transectfsi=Transectfsi[,c('title','transectAbstract','stratum','comments','voyage','vesselName','dateCreated',
                                 'timeCoverageStart','timeCoverageEnd','geospatialLonMin','geospatialLatMin','geospatialVerticalMin',
                                 'geospatialLonMax','geospatialLatMax','geospatialVerticalMax','linestring')] 
        if (i==1){
          Transectfs=Transectfsi
        }else{
          Transectfs=rbind(Transectfs,Transectfsi)
        }
      }
      Transecti=cas[!cas$Code.Appareil%in%nonEventsCodes&!cas$Code.Action%in%nonActions,]
      septs.df$epts=as.character(septs.df$epts)
      septs.df$spts=as.character(septs.df$spts)
      septs.dfs=septs.df[septs.df$gear.type=='TRAWL',]
      Transectif=cas[cas$Code.Action%in%c(septs.dfs$spts,septs.dfs$epts),]
      lvessels=c(as.character(unique(Transectif$Observation)))
      lvessels=lvessels[lvessels!=""]
      lstrates=as.character(unique(Transectif$Strate))
      tope=strptime(paste(cas$Date,cas$Heure),"%d/%m/%Y %H:%M:%S",tz='GMT')
      Transectp=cas[cas$Code.Action%in%c(septs.df$epts,septs.df$spts)&
                      cas$Observation%in%lvessels,]
      if (length(lvessels)>0){
        # create fishing transects for vessels with name different from vesselName (if any)
        for (i in 1:length(lvessels)){
          vesselOpe=Transectp[Transectp$Observation==lvessels[i],]
          tvesseli=strptime(paste(vesselOpe$Date,vesselOpe$Heure),"%d/%m/%Y %H:%M:%S",tz='GMT')
          Transectfsi=Transectfs[Transectfs$transectAbstract=='PTM'&
                                   Transectfs$vesselName==vesselName,]
          Transectfsi$vesselName=lvessels[i]
          Transectfsi$title=paste(Transectfsi$title,lvessels[i])
          Transectfsi=Transectfsi[1,]
          Transectfsi$timeCoverageStart=paste(min(tvesseli),'.0000',sep='')
          Transectfsi$timeCoverageEnd=paste(max(tvesseli),'.0000',sep='')
          Transectfs=rbind(Transectfs,Transectfsi)
        }  
      }
      if (check.ref){
        # Check that vessel names are in EchoBase ref table
        #check depthStratum in Echobase reference table
        # Connection to a PostgreSQL database (on a server)
        #********************
        library(RPostgreSQL)
        drv <- dbDriver("PostgreSQL")
        #summary(drv)
        dbListConnections(drv)
        dbGetInfo(drv)
        con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                         password=password,port=port)
        sqlVessels="SELECT * FROM Vessel"
        refVessels <- dbGetQuery(con,sqlVessels)
        dbDisconnect(con)
        mvessel=Transectfs$vesselName[!Transectfs$vesselName%in%refVessels$name]
        if (length(mvessel)>0){
          cat('Vessel(s) not found in EchoBase: ',paste(unique(mvessel),collapse='\n'))
          Transectfs=Transectfs[Transectfs$vesselName%in%refVessels$name,]
          cat('=> Transects with vessel(s) not found in EchoBase removed','\n')
        }
      }
      # Remove Thalassa pair trawls transects
      Transectfs=Transectfs[!(Transectfs$transectAbstract=='PTM'&
                                Transectfs$vesselName==vesselName),]
      
      if (length(lstrates)>1){
        #create one transect per depth stratum for fishing transects
        # not needed
      }
      cat('Dummy transects created','\n')
      #Export Transect in Echobase format
      head(Transectfs)
      write.table(Transectfs,paste(path.export,cruise,'_Transect4Echobase.csv',sep=''),
                  row.names=FALSE,sep=';')
      cat('-> Transect data exported','\n')
      cat('Unique vessels in transects:',unique(Transectfs$vesselName),'\n')
    }else{
      # Select events that are: operation events, non CUFES actions ('NEXT')
      Transecti=cas[!cas$Code.Appareil%in%nonEventsCodes&!cas$Code.Action%in%nonActions,]
      septs.df$epts=as.character(septs.df$epts)
      septs.df$spts=as.character(septs.df$spts)
      septs.dfs=septs.df[!septs.df$Code.Appareil%in%c(codeCamp,codeInter,codeTransit),]
      Transecti=cas[cas$Code.Action%in%c(septs.dfs$spts,septs.dfs$epts),]
      unique(Transecti$Code.Action)
      unique(Transecti$Code.Appareil)
      head(Transecti)
      
      #Correct acoustic stratum 
      #******************
      #unique(Transecti$Strate)
      Transecti$Strate=as.character(Transecti$Strate)
      Transecti$Strate=gsub(radialeCode,'R',Transecti$Strate,ignore.case = TRUE)
      Transecti$Strate=gsub(gsub(' ','',radialeCode),'R',Transecti$Strate,ignore.case = TRUE)
      #Transecti$Strate=gsub('R ','R0',Transecti$Strate,ignore.case = TRUE)
      Transecti$Strate=gsub('--','-',Transecti$Strate,ignore.case = TRUE)
      Transecti$Strate=gsub('--','-',Transecti$Strate,ignore.case = TRUE)
      Transecti$Strate=gsub('---','-',Transecti$Strate,ignore.case = TRUE)
      #Transecti$Stratei=gsub(radialeCode,'R',Transecti$Strate,ignore.case = TRUE)
      Transecti$stratum=substr(Transecti$Strate,1,3)
      Transecti$stratum=gsub(' ','',Transecti$stratum)
      Transecti[Transecti$Code.Appareil%in%surfInstruments,'stratum']='SUR'
      Transecti$ID=paste(Transecti$Code.Appareil,Transecti[,nstname],
                         Transecti$Strate)
      Transecti[Transecti$Code.Appareil=='CUFIX','ID']=paste(Transecti[Transecti$Code.Appareil=='CUFIX',
                                                                       'Identificateur.Operation'],Transecti[Transecti$Code.Appareil=='CUFIX','stratum'])
      #Transecti$ID=paste(Transecti$Identificateur.Operation,Transecti$stratum)
      Transecti$vesselName=vesselName
      Transecti$vesselName=as.character(Transecti$vesselName)
      #Adds pro vessel names (if any: pro fishing vessel have different "Identificateur.Operation" prefix)
      Transecti[!grepl(sciVesselCode,Transecti$Identificateur.Operation),'vesselName']=
        as.character(Transecti[!grepl(sciVesselCode,Transecti$Identificateur.Operation),'Observation'])
      #Adds x, y and z fields
      Transecti$x=as.numeric(gsub(",",".",as.character(Transecti[,rxname])))
      Transecti$y=as.numeric(gsub(",",".",as.character(Transecti[,ryname])))
      Transecti$z=as.numeric(gsub(",",".",as.character(Transecti[,rzname])))
      Transecti$xy1=paste(Transecti$x*111000,Transecti$y*111000)
      unique(Transecti$vesselName)
      # remove trailing spaces in strate
      #Transecti$Strate=gsub(' ','',Transecti$Strate)
      
      #select unique transect labels 
      #******************
      #A transect is defined by a day, an instrument and a stratum belonging
      #names(Transecti)
      #   Transectiu=unique(Transecti[Transecti$stratum!='',c('Code.Appareil',
      #                                                       'Identificateur.Operation','N..Station','stratum',
      #                                                       'vesselName')])
      Transectiu=unique(Transecti[Transecti$stratum!='',c('Code.Appareil',
                                                          nstname,
                                                          'Strate','vesselName')])
      #   Transectiu$ID=paste(Transectiu$Identificateur.Operation,
      #                       Transectiu$stratum)
      
      Transectiu$ID=paste(Transectiu$Code.Appareil,
                          Transectiu[,nstname],
                          Transectiu$Strate)
      #Transectiu=Transectiu[order(Transectiu$ID),]
      #duplicated IDs:
      Transectiud=Transectiu[duplicated(Transectiu$ID),]
      Transectiu[Transectiu$ID%in%Transectiud$ID,]
      
      if (dim(Transectiud)[1]>0) stop (paste('Non unique transect descriptors in:',
                                             Transectiud$Identificateur.Operation),'\n')
      
      #select transect start and end points 
      #******************
      #unique(Transecti$Code.Action)
      #Transects startpoints
      names(Transecti)
      dim(Transecti)
      Transecti=merge(Transecti,septs.df,by.x='Code.Appareil',by.y='Code.Appareil')
      Transectis=Transecti[as.character(Transecti$Code.Action)==as.character(Transecti$spts),]
      names(Transectis)[2:3]=c('dates','heures')
      names(Transectis)[names(Transectis)=='x']='xs'
      names(Transectis)[names(Transectis)=='y']='ys'
      names(Transectis)[names(Transectis)=='z']='zs'
      dim(Transectis)
      head(Transectis)
      #Transects endpoints
      Transectif=Transecti[as.character(Transecti$Code.Action)==as.character(Transecti$epts),]
      names(Transectif)[2:3]=c('datef','heuref')
      names(Transectif)[names(Transectif)=='x']='xf'
      names(Transectif)[names(Transectif)=='y']='yf'
      names(Transectif)[names(Transectif)=='z']='zf'
      dim(Transectif)
      Transectisf=merge(Transectis[,c('dates','heures','xs','ys','zs','ID',
                                      'vesselName')],
                        Transectif[,c('datef','heuref','xf','yf','zf','ID',
                                      'vesselName')],
                        by.x=c('ID','vesselName'),by.y=c('ID','vesselName'),
                        all.x=TRUE)
      Transectisf[duplicated(Transectisf$ID),]
      unique(Transectisf$ID)
      Transectisf[is.na(Transectisf$datef),]
      
      Transectisf$dateCreated=strptime(paste(as.character(Transectisf$dates),
                                             as.character(Transectisf$heures)),"%d/%m/%Y %H:%M:%S",tz="GMT")
      Transectisf$dateCreated=paste(Transectisf$dateCreated,'.0000',sep='')
      Transectisf$timeCoverageStart=strptime(paste(as.character(Transectisf$dates),
                                                   as.character(Transectisf$heures)),"%d/%m/%Y %H:%M:%S",tz="GMT")
      Transectisf$timeCoverageStart=paste(Transectisf$timeCoverageStart,'.0000',
                                          sep='')
      Transectisf$timeCoverageEnd=strptime(paste(as.character(Transectisf$datef),
                                                 as.character(Transectisf$heuref)),"%d/%m/%Y %H:%M:%S",tz="GMT")
      Transectisf$timeCoverageEnd=paste(Transectisf$timeCoverageEnd,'.0000',
                                        sep='')
      
      scufix=Transectis[Transectis$Code.Appareil=='CUFIX',]
      ecufix=Transectif[Transectif$Code.Appareil=='CUFIX',]
      #head(scufix)
      
      Transectisfs=Transectisf[!grepl('CUFIX',Transectisf$ID),]
      
      #head(Transectisfs)
      unique(Transectisf$vesselName)
      tpbs=Transectisfs[is.na(Transectisfs$datef),]
      unique(Transectisfs$vesselName)
      
      if (dim(tpbs)[1]>0) stop ('Please check transects with missing breakpoints:',
                                paste(tpbs$ID,'\n'),'\n')
      
      #Adds acoustic inter-transects 
      #******************
      #Inter transects are between acoustic transects (day*instrument*stratum)
      #Creates inter-transect based on transect breakpoints: start transect=end inter-transect etc.. 
      IT.epts=as.character(septs.df[septs.df$Code.Appareil==codeInter,'epts'])
      ITransectif=Transectis[Transectis$Code.Action==IT.epts,]
      names(ITransectif)[2:3]=c('datef','heuref')
      names(ITransectif)[names(ITransectif)=='xs']='xf'
      names(ITransectif)[names(ITransectif)=='ys']='yf'
      names(ITransectif)[names(ITransectif)=='zs']='zf'
      #ITransectif[ITransectif$ID=='ACOU  R12 M->C2',]
      #
      IT.spts=as.character(septs.df[septs.df$Code.Appareil==codeInter,'spts'])
      ITransectis=Transectif[Transectif$Code.Action==IT.spts,]
      names(ITransectis)[2:3]=c('dates','heures')
      names(ITransectis)[names(ITransectis)=='xf']='xs'
      names(ITransectis)[names(ITransectis)=='yf']='ys'
      names(ITransectis)[names(ITransectis)=='zf']='zs'
      
      ITransectisf=merge(ITransectis[,c('dates','heures','xs','ys','zs','ID',
                                        'vesselName')],
                         ITransectif[,c('datef','heuref','xf','yf','zf','ID',
                                        'vesselName')],
                         by.x=c('ID','vesselName'),by.y=c('ID','vesselName'),
                         all.x=TRUE)
      ITransectisfd=ITransectisf[duplicated(ITransectisf$ID),]
      head(ITransectisf)
      unique(ITransectisf$vesselName)
      #ITransectisf[ITransectisf$ID=='ACOU  R11 M->C2',]
      
      if (dim(ITransectisfd)[1]>0) stop (paste('Non unique inter-transect descriptors in:',
                                               paste(ITransectisfd$ID,'\n')),'\n')
      
      ITransectisf$timeCoverageEnd=format(strptime(paste(as.character(ITransectisf$datef),
                                                         as.character(ITransectisf$heuref)),"%d/%m/%Y %H:%M:%S",tz="GMT")-1)
      ITransectisf$timeCoverageEnd=paste(ITransectisf$timeCoverageEnd,'.0000',
                                         sep='')
      ITransectisf$timeCoverageStart=format(strptime(paste(as.character(ITransectisf$dates),
                                                           as.character(ITransectis$heures)),"%d/%m/%Y %H:%M:%S",tz="GMT")+1)
      ITransectisf$timeCoverageStart=paste(ITransectisf$timeCoverageStart,'.0000',
                                           sep='')
      ITransectisf$dateCreated=ITransectisf$timeCoverageStart
      names(ITransectisf)
      ITransectisf$ID=paste(ITransectisf$ID,"INTER")
      ITransectisf=ITransectisf[,names(Transectisf)]
      #head(ITransectisf)
      unique(ITransectisf$vesselName)
      itpbs=ITransectisf[is.na(ITransectisf$datef),]
      unique(ITransectisf$vesselName)
      
      if (dim(itpbs)[1]>0) stop (paste('Please check transects with missing breakpoints:',
                                       paste(itpbs$ID,'\n')),'\n')
      
      #Binds transects and inter-transects breakpoints 
      #********************************
      names(Transectisf)
      names(ITransectisf)
      Transectisf=rbind(Transectisf,ITransectisf)
      head(ITransectisf)
      unique(Transectisf$vesselName)
      #unique(Transectisf$ID)
      
      #Adds inter-transect to transect list 
      #********************************
      names(Transectiu)
      names(ITransectisf)
      #   ITransectiu=data.frame(Code.Appareil='ACOU',
      #                          Identificateur.Operation='INTERAD',
      #                          N..Station=NA,
      #                          stratum=paste('I',ITransectisf$stratum,sep=''),
      #                          ID=ITransectisf$ID,vesselName=ITransectisf$vesselName)
      ITransectiu=data.frame(Code.Appareil='ACOU',
                             nstname=NA,
                             Strate='INTER',
                             ID=ITransectisf$ID,vesselName=ITransectisf$vesselName)
      names(ITransectiu)[2]=nstname
      #sort(ITransectisf$ID)
      Transectiu=rbind(Transectiu,ITransectiu[,names(Transectiu)])
      head(Transectiu)
      unique(Transectiu$vesselName)
      
      #min/max over transects 
      #********************************
      Transectmin=aggregate(Transecti[,c('x','y','z')],list(Transecti$ID),min,
                            na.rm=TRUE)
      names(Transectmin)=c('ID','geospatialLonMin','geospatialLatMin',
                           'geospatialVerticalMin')
      Transectmax=aggregate(Transecti[,c('x','y','z')],list(Transecti$ID),max,
                            na.rm=TRUE)
      names(Transectmax)=c('ID','geospatialLonMax','geospatialLatMax',
                           'geospatialVerticalMax')
      #min/max over inter-transects 
      #********************************
      ITransectmin=data.frame(ID=ITransectisf$ID)
      ITransectmin$geospatialLonMin=apply(ITransectisf[,c('xs','xf')],1,min)
      ITransectmin$geospatialLatMin=apply(ITransectisf[,c('ys','yf')],1,min)
      ITransectmin$geospatialVerticalMin=apply(ITransectisf[,c('zs','zf')],1,min)
      ITransectmax=data.frame(ID=ITransectisf$ID)
      ITransectmax$geospatialLonMax=apply(ITransectisf[,c('xs','xf')],1,max)
      ITransectmax$geospatialLatMax=apply(ITransectisf[,c('ys','yf')],1,max)
      ITransectmax$geospatialVerticalMax=apply(ITransectisf[,c('zs','zf')],1,max)
      
      #Create linestrings 
      #********************************
      TransectLS=aggregate(Transecti[,c('xy1')],list(Transecti$ID),
                           paste,collapse=',')
      names(TransectLS)=c('ID','linestring')
      
      ITransectisf$xys=paste(ITransectisf$xs,ITransectisf$ys)
      ITransectisf$xyf=paste(ITransectisf$xf,ITransectisf$yf)
      ITransectisf$linestring=paste(ITransectisf$xys,ITransectisf$xyf,sep=',')
      
      unique(Transectisf$vesselName)
      
      TransectLS=rbind(TransectLS,ITransectisf[,c('ID','linestring')])
      
      #Binds transect and inter-transect min/max 
      #********************************
      Transectmin=rbind(Transectmin,ITransectmin)
      Transectmax=rbind(Transectmax,ITransectmax)
      
      # check transect min/max 
      #********************************
      tzminpb=Transectmin[is.infinite(Transectmin$geospatialVerticalMin),]
      if (dim(tzminpb)[1]>0) stop('Invalid depths in metadata file, please check')
      tzmaxpb=Transectmax[is.infinite(Transectmax$geospatialVerticalMin),]
      if (dim(tzmaxpb)[1]>0) stop('Invalid depths in metadata file, please check')
      
      #Create transects 
      #********************************
      #head(Transectiu)
      Transect=data.frame(title=Transectiu[,'ID'])
      Transect$transectAbstract=Transectiu$Code.Appareil
      Transect$stratum=Transectiu$Strate
      Transect$title=as.character(Transect$title)
      #Format 'comments'
      Transect[!Transectiu$Code.Appareil%in%c('ACOU','CUFIX'),'comments']=
        paste(Transectiu[!Transectiu$Code.Appareil%in%c('ACOU','CUFIX'),
                         'Code.Appareil'],
              Transectiu[!Transectiu$Code.Appareil%in%c('ACOU','CUFIX'),
                         nstname],
              Transectiu[!Transectiu$Code.Appareil%in%c('ACOU','CUFIX'),
                         'stratum'],sep=',')
      Transect[Transectiu$Code.Appareil%in%c('ACOU'),'comments']=
        paste(Transectiu[Transectiu$Code.Appareil%in%c('ACOU'),'Code.Appareil'],
              Transectiu[Transectiu$Code.Appareil%in%c('ACOU'),'stratum'],sep=',')
      Transect[Transectiu$Code.Appareil%in%c('CUFIX'),'comments']='3n.mi.CUFES transect'
      #Adds voyage name
      Transect$voyage=Voyage$name
      head(Transect)
      #Merge transects, break points and min/max
      unique(Transectisf$vesselName)
      unique(Transectisf$ID)
      unique(Transect$title)
      Transect2=merge(Transect,Transectisf,by.x='title',by.y='ID',all.x=TRUE)
      unique(Transect2$vesselName)
      Transect2[is.na(Transect2$vesselName),]
      Transect2=merge(Transect2,Transectmin,by.x='title',by.y='ID')
      Transect2=merge(Transect2,Transectmax,by.x='title',by.y='ID')
      Transect2=merge(Transect2,TransectLS,by.x='title',by.y='ID')
      names(Transect2)
      #remove unwanted columns
      Transectf=Transect2[,!names(Transect2)%in%c('dates','datef','heures','heuref',
                                                  'xs','ys','xf','yf','zs','zf')]
      #remove rows with NA (CUFIX and...)
      #Transectf[is.na(Transectf$timeCoverageEnd),'title']
      #Transectfs=Transectf[!is.na(Transectf$transectAbstract$vesselName)&Transectf$transectAbstract!='CUFIX',]
      Transectfs=Transectf[!Transectf$transectAbstract%in%blApp,]
      
      head(Transectfs)
      names(Transectfs)
      unique(Transectfs$vesselName)
      Transectfs[is.na(Transectfs$vesselName),]
      
      if (check.ref){
        #check that all vessel name are in Echobase reference table
        unique(Transectfs$vesselName)
        Transectfs[is.na(Transectfs$vesselName),]
        # 1.1. Connection to a PostgreSQL database (on a server)
        library(RPostgreSQL)
        
        drv <- dbDriver("PostgreSQL")
        #summary(drv)
        dbListConnections(drv)
        dbGetInfo(drv)
        
        con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                         password=password,port=port)
        #Import all vessels
        sqlVessels="SELECT vessel.name FROM public.vessel"
        Vessels <- dbGetQuery(con,sqlVessels)
        dbDisconnect(con)
        viv=unique(Transectfs$vesselName)%in%Vessels$name
        if (FALSE%in%(viv)) stop('Vessel name ',unique(Transectfs$vesselName)[!viv],' not found in Echobase vessel table. Please check.')
      } 
      # look for transects without breakpoints
      tpb=Transectfs[is.na(Transectfs$vesselName),]
      if (dim(tpb)[1]>0) stop (paste('Please check transects with missing breakpoints:',
                                     tpb$ID),'\n')  
      #Extract pros transects 
      #********************************
      
      Transect.pros=Transectfs[Transectfs$vesselName!='THALASSA II',]
      
      head(Transectfs)
      unique(Transectfs$vesselName)
      
      # Check transect time coverage 
      #********************************
      tdt=strptime(Transectfs$timeCoverageStart,"%Y-%m-%d %H:%M:%OS",tz="GMT")
      tft=strptime(Transectfs$timeCoverageEnd,"%Y-%m-%d %H:%M:%OS",tz="GMT")
      tdv=strptime(Voyage$startDate,"%Y-%m-%d %H:%M:%OS",tz="GMT")
      tfv=strptime(Voyage$endDate,"%Y-%m-%d %H:%M:%OS",tz="GMT")
      if (min(tdt)<min(tdv)) stop("Transect",min(tdt),"before start of voyage")
      if (max(tft)>max(tfv)) stop("Transect",max(tft),"after end of voyage")  
      
      # check that transect titles are unique 
      #********************************
      unique(Transectfs$title)
      nutt=unique(Transectfs$title[table(Transectfs$title)>1])
      if (length(nutt)>0){
        stop('Non unique transect titles found: ',nutt)
      }
      
      #Export Transect in Echobase format
      head(Transectfs)
      write.table(Transectfs,paste(path.export,cruise,'_Transect4Echobase.csv',sep=''),
                  row.names=FALSE,sep=';')
      cat('-> Transect data exported','\n')
      cat('Unique vessels in transects:',unique(Transectfs$vesselName),'\n')
    }
    
    #2.1.4. Check transects------ 
    #********************************
    Transectfs[is.na(Transectfs$timeCoverageEnd),1:10]
    Transectfs[Transectfs$GeospatialLonMax==999,]
    Transectfs[Transectfs$GeospatialLatMax==999,]
    Transectfs[Transectfs$GeospatialLonMin==999,]
    Transectfs[Transectfs$GeospatialLatMin==999,]
    
    btmlon=Transectfs[Transectfs$geospatialLonMin<(-50),]
    if (dim(btmlon)[1]>0){
      stop('Check longitude in: ',paste(btmlon[,"title"],collapse='\n'))
    }
    head(Transectfs)
    
    #Plot all transects
    #graphics.off()
    
    #x11()
    plot(c(Transectfs$geospatialLonMin,Transectfs$geospatialLonMax),
         c(Transectfs$geospatialLatMin,Transectfs$geospatialLatMax),type='n',
         xlab='',ylab='',main=paste(cruise,'transects'))
    rect(Transectfs$geospatialLonMin,Transectfs$geospatialLatMin,
         Transectfs$geospatialLonMax,Transectfs$geospatialLatMax,
         col=Transectfs$abstract)
    coast()
    
    #Plot transects by categories
    #define gear codes, based on lApp argument
    lapp=data.frame(app=unique(as.character(Transectfs$transectAbstract)))
    lapp=merge(lapp,septs.df[,c('Code.Appareil','code.app')],
               by.x='app',by.y='Code.Appareil')
    lc=unique(lapp$code.app)
    N=length(lc)
    #x11()
    for (i in 1:N){
      Transectfsi=Transectfs[Transectfs$transectAbstract%in%lapp[lapp$code.app==lc[i],'app'],]
      if (i==1) par(mfrow=c(2,2))
      
      plot(c(Transectfsi$geospatialLonMin,Transectfsi$geospatialLonMax),
           c(Transectfsi$geospatialLatMin,Transectfsi$geospatialLatMax),type='n',
           xlab='',ylab='',main=lc[i])
      rect(Transectfsi$geospatialLonMin,Transectfsi$geospatialLatMin,
           Transectfsi$geospatialLonMax,Transectfsi$geospatialLatMax,
           col=Transectfsi$abstract)
      coast()
    }
    par(mfrow=c(1,1))
    
    #Plot one transect
    
    #   Transectfi=Transectf[Transectf$stratum%in%c('R23'),]
    #   plot(c(Transectf$geospatialLonMin,Transectf$geospatialLonMax),
    #        c(Transectf$geospatialLatMin,Transectf$geospatialLatMax),type='p',
    #        xlab='',ylab='',main='PELGAS11 transects')
    #   rect(Transectfi$geospatialLonMin,Transectfi$geospatialLatMin,
    #        Transectfi$geospatialLonMax,Transectfi$geospatialLatMax,
    #        col=Transectfi$abstract)
    #   text(Transectfi$geospatialLonMin,Transectfi$geospatialLatMin,
    #        Transectfi$timeCoverageStart)                               
    
    #Positions check
    par(mfrow=c(2,2))
    hist(Transectfs$geospatialLatMin)
    hist(Transectfs$geospatialLatMax)
    hist(Transectfs$geospatialLonMin)
    hist(Transectfs$geospatialLonMax)
    par(mfrow=c(1,1))
    #Depths check
    par(mfrow=c(2,2))
    hist(Transectfs$geospatialVerticalMin)
    hist(Transectfs$geospatialVerticalMax)
    plot(Transectfs$geospatialVerticalMin,Transectfs$geospatialVerticalMax)
    par(mfrow=c(1,1))  
  }

  #********************************************************* ----
  # 3. Ancillary instrumentation --------------
  #********************************************************* -----
  if (!is.null(ancillary)){
    #Export Transect in Echobase format
    write.table(ancillary,paste(path.export,cruise,'_Ancillary4Echobase.csv',sep=''),
                row.names=FALSE,sep=';')
    cat('-> Ancillary instrumentation data exported','\n')
  }
  
  #*********************************************************
  # 4. Echosounder calibration -----
  #********************************************************* -----
  if (!is.null(calibration)){
    #Export calibration in Echobase format
    write.table(calibration,paste(path.export,cruise,'_Calibration4Echobase.csv',sep=''),
                row.names=FALSE,sep=';')
    cat('-> Echosounder calibration data exported','\n')
  }
  
  #*********************************************************
  # 5. CommonData: operations and fishing -------
  #********************************************************* -----
  # 5.1. Operations data --------------
  #******************************** -----
  if (!is.null(path.cas)){
    # 5.1.1. import operations---- 
    #***********************
    if (is.null(strate.ok)){
      #check depthStratum in Echobase reference table
      # Connection to a PostgreSQL database (on a server)
      #********************
      #if (check.ref){
        library(RPostgreSQL)
        drv <- dbDriver("PostgreSQL")
        #summary(drv)
        dbListConnections(drv)
        dbGetInfo(drv)
        con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                         password=password,port=port)
        sqlDepthStratum="SELECT * FROM DepthStratum"
        depthStratumEB <- dbGetQuery(con,sqlDepthStratum)
        dbDisconnect(con)
        strate.ok=depthStratumEB$id
      #}
    }
    
    rbarac=format4baracouda(cruise=Voyage$name,vessel=vesselName,
                            path.stations=path.cas,proNb=5,
                            vir.codes=vir.codes,fil.codes=fil.codes,
                            lgear.codes=gear.codes$Code.Appareil,
                            zname=zname,rzname=rzname,sep.cas=sep.cas,
                            strate.ok=strate.ok,
                            septs.df=septs.df,cnames=cnames)
    
    names(rbarac)
    baracope=rbarac$operations
    head(baracope)
    dim(baracope)
    unique(baracope$NOSTA)
    table(as.character(baracope$TYPECHAL))
    head(baracope)
    baracope$NOSTA=gsub(' ','',baracope$NOSTA)
    if (sum(nchar(baracope$NOSTA)>5)>0){
      stop('Invalid operation ID found in: ',baracope$NOSTA[nchar(baracope$NOSTA)>5])
    }
    if (check.ref){
      # Check that vessel names are in EchoBase ref table
      #check depthStratum in Echobase reference table
      # Connection to a PostgreSQL database (on a server)
      #********************
      library(RPostgreSQL)
      drv <- dbDriver("PostgreSQL")
      #summary(drv)
      dbListConnections(drv)
      dbGetInfo(drv)
      con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                       password=password,port=port)
      sqlVessels="SELECT * FROM Vessel"
      refVessels <- dbGetQuery(con,sqlVessels)
      dbDisconnect(con)
      mvessel=baracope$NAVIRE[!baracope$NAVIRE%in%refVessels$name]
      if (length(mvessel)>0){
        stop('Vessel(s) not found in EchoBase: ',paste(unique(mvessel),collapse='\n'))
      }
    }    
    # check that gear codes are not duplicated
    if (dim(gear.codes)[1]>dim(unique(gear.codes))[1]){
      stop('Duplicated gears in gear.code argument, please check','\n')
    }
    
    #Convert casino codes to baracouda codes
    gearTranslate=data.frame(Echobasegearcode=gear.codes$Echobasegearcode,
                             casinogearcode=gear.codes$Code.Appareil)
    dim(baracope)
    baracope=merge(baracope,gearTranslate,by.x='TYPECHAL',by.y='casinogearcode')
    dim(baracope)
    baracope$STRATE=as.character(baracope$STRATE)
    baracope[baracope$STRATE=='NULL','STRATE']='NUL'
    
    #Creates operations
    Operations=data.frame(vesselName=baracope$NAVIRE,operationId=baracope$NOSTA,
                          depthStratumId=baracope$STRATE,
                          gearShootingStartTime=paste(baracope$DATE,baracope$HF),
                          midHaulLatitude=(as.numeric(baracope$LATFDD)+
                                             as.numeric(baracope$LATVDD))/2,
                          midHaulLongitude=(as.numeric(baracope$LGFDD)+
                                              as.numeric(baracope$LGVDD))/2,
                          gearShootingStartLatitude=as.numeric(baracope$LATFDD),
                          gearShootingStartLongitude=as.numeric(baracope$LGFDD),
                          gearShootingEndTime=paste(baracope$DATE,baracope$HV),
                          gearHaulingEndLatitude=as.numeric(baracope$LATVDD),
                          gearHaulingEndLongitude=as.numeric(baracope$LGVDD),
                          # gearHaulingEndLatitude=as.numeric(baracope$LATVDD),
                          # gearHaulingEndLongitude=as.numeric(baracope$LGVDD),
                          gearCode=baracope$Echobasegearcode)
    Operations$gearShootingStartTime=paste(format(strptime(
      Operations$gearShootingStartTime,"%d/%m/%Y %H:%M:%S",tz="GMT")),'.0000',sep='')
    Operations$gearShootingEndTime=paste(format(strptime(
      Operations$gearShootingEndTime,"%d/%m/%Y %H:%M:%S",tz="GMT")),'.0000',sep='')
    
    dim(Operations)
    unique(Operations$vesselName)
    unique(Operations$gearCode)
    #table(baracope$baragearcode)
    #table(as.character(baracope$TYPECHAL))
    unique(Operations$vesselName)
    head(Operations)
    if (sum(Operations$operationId=='')>0){
      stop('Missing operation id in: ',
           paste(Operations$gearShootingStartTime[Operations$operationId==''],collapse=' '))
    }
    # check positions range 
    #********************
    if (sum(abs(Operations$gearShootingStartLatitude)>180)>0){
      stop ('Invalid latitude found in:',paste(Operations[abs(Operations$gearShootingStartLatitude)>180,'operationId'],collapse=' '))
    }
    if (sum(abs(Operations$gearShootingStartLongitude)>180)>0){
      stop ('Invalid longitude found in:',paste(Operations[abs(Operations$gearShootingStartLongitude)>180,'operationId'],collapse=' '))
    }
    if (sum(abs(Operations$gearHaulingEndLatitude)>180)>0){
      stop ('Invalid latitude found in:',paste(Operations[abs(Operations$gearHaulingEndLatitude)>180,'operationId'],collapse=' '))
    }
    if (sum(abs(Operations$gearHaulingEndLongitude)>180)>0){
      stop ('Invalid longitude found in:',paste(Operations[abs(Operations$gearHaulingEndLongitude)>180,'operationId'],collapse=' '))
    }
    if (sum(abs(Operations$midHaulLatitude)>180)>0){
      stop ('Invalid latitude found in:',paste(Operations[abs(Operations$midHaulLatitude)>180,'operationId'],collapse=' '))
    }
    if (sum(abs(Operations$midHaulLongitude)>180)>0){
      stop ('Invalid longitude found in:',paste(Operations[abs(Operations$midHaulLongitude)>180,'operationId'],collapse=' '))
    }
    
    #********************
    #5.1.2. OperationMetadataValue  ------------
    #********************
    OperationMetadataValue=rbind(data.frame(vesselName=baracope$NAVIRE,
                                            operationId=baracope$NOSTA,metadataType='MeanWaterDepth',
                                            operationMetadataValue=(as.numeric(baracope$SONDF)+
                                                                      as.numeric(baracope$SONDV))/2),
                                 data.frame(vesselName=baracope$NAVIRE,
                                            operationId=baracope$NOSTA,metadataType='WaterDepthShoot',
                                            operationMetadataValue=as.numeric(baracope$SONDF)),
                                 data.frame(vesselName=baracope$NAVIRE,
                                            operationId=baracope$NOSTA,metadataType='WaterDepthHaul',
                                            operationMetadataValue=as.numeric(baracope$SONDV)))           
    head(OperationMetadataValue)
    # 5.1.2.1. Check and correct operation metadata values ----
    if (check.ref){
      # check that all gearmetadata are in EchoBase
      library(RPostgreSQL)
      drv <- dbDriver("PostgreSQL")
      #summary(drv)
      con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                       password=password,port=port)
      #Import all gearmetadata
      sqlopemetadata="SELECT name FROM operationmetadata"
      opemetadata <- dbGetQuery(con,sqlopemetadata)
      viv=unique(OperationMetadataValue$metadataType)%in%opemetadata$name
      if (FALSE%in%(viv)) stop('Operation metadata',
                               unique(OperationMetadataValue$metadataType)[!viv],
                               'not found in Echobase reference table. Please check.')
      dbDisconnect(con)
    }
    unique(OperationMetadataValue$metadataType)
    summary(OperationMetadataValue$operationMetadataValue)
    OperationMetadataValue[!is.na(OperationMetadataValue$operationMetadataValue)&
                             OperationMetadataValue$operationMetadataValue<=0,
                           'operationMetadataValue']=NA
    OperationMetadataValue=OperationMetadataValue[!is.na(
      OperationMetadataValue$operationMetadataValue),]
    head(OperationMetadataValue)
    
    #********************
    #5.1.3. GearMetadataValue -------------
    #********************
    head(baracope)
    GearMetadataValue=rbind(data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='CableLength',
                                       gearMetadataValue=as.numeric(baracope$LGB)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='MinSpeed',
                                       gearMetadataValue=as.numeric(baracope$VMIN)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='MaxSpeed',
                                       gearMetadataValue=as.numeric(baracope$VMAX)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='AverageSpeed',
                                       gearMetadataValue=as.numeric(baracope$VBATF)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='VesselHeading',
                                       gearMetadataValue=as.numeric(baracope$CAP)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='DriftHeading',
                                       gearMetadataValue=as.numeric(baracope$DIRCOURS)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='DriftSpeed',
                                       gearMetadataValue=as.numeric(baracope$COURS)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='TrawlVerticalOpening',
                                       gearMetadataValue=as.numeric(baracope$OUVV)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='TrawlHorizontalOpening',
                                       gearMetadataValue=as.numeric(baracope$OUVA)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='TrawlDoorsOpening',
                                       gearMetadataValue=as.numeric(baracope$OUVP)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='WindDirection',
                                       gearMetadataValue=as.numeric(baracope$DIRVENT)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='WindSpeed',
                                       gearMetadataValue=as.numeric(baracope$VENT)),
                            data.frame(vesselName=baracope$NAVIRE,
                                       operationId=baracope$NOSTA,
                                       gearCode=baracope$Echobasegearcode,
                                       metadataType='TrawlDepth',
                                       gearMetadataValue=as.numeric(baracope$ZCHAL)))
    head(baracope)
    head(GearMetadataValue)
    # 5.1.3.1. Check and correct gear metadata values ----
    if (check.ref){
      # check that all gearmetadata are in EchoBase
      library(RPostgreSQL)
      drv <- dbDriver("PostgreSQL")
      #summary(drv)
      con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                       password=password,port=port)
      #Import all gearmetadata
      sqlgearmetadata="SELECT name FROM gearmetadata"
      gearmetadata <- dbGetQuery(con,sqlgearmetadata)
      viv=unique(GearMetadataValue$metadataType)%in%gearmetadata$name
      if (FALSE%in%(viv)) stop('Gear metadata',
                               unique(GearMetadataValue$metadataType)[!viv],
                               'not found in Echobase reference table. Please check.')
      dbDisconnect(con)
    }
    # Correct infinite values
    GearMetadataValue[!is.na(GearMetadataValue$gearMetadataValue)&
                        is.infinite(GearMetadataValue$gearMetadataValue),
                      'gearMetadataValue']=NA
    GearMetadataValue[!is.na(GearMetadataValue$gearMetadataValue)&
                        GearMetadataValue$gearMetadataValue==-1,
                      'gearMetadataValue']=NA
    GearMetadataValue[!is.na(GearMetadataValue$gearMetadataValue)&
                        GearMetadataValue$gearMetadataValue==-10000000,
                      'gearMetadataValue']=NA
    GearMetadataValue[!is.na(GearMetadataValue$gearMetadataValue)&
                        !GearMetadataValue$metadataType%in%
                        c('DriftHeading','VesselHeading','DriftSpeed','WindSpeed',
                          'WindDirection')&GearMetadataValue$gearMetadataValue==0,
                      'gearMetadataValue']=NA
    GearMetadataValue[!is.na(GearMetadataValue$gearMetadataValue)&
                        GearMetadataValue$metadataType=='TrawlDepth'&
                        GearMetadataValue$gearMetadataValue>200,
                      'gearMetadataValue']=NA
    # Remove pros missing values
    GearMetadataValue[GearMetadataValue$vesselName!=vesselName,
                      'gearMetadataValue']=NA
    aggregate(GearMetadataValue$gearMetadataValue,
              list(GearMetadataValue$metadataType),summary)
    table(GearMetadataValue$metadataType)
    table(as.character(GearMetadataValue$operationId))
    # Remove NA values
    GearMetadataValue=GearMetadataValue[!is.na(GearMetadataValue$gearMetadataValue),]
      
    #5.1.4. Export Operation data in Echobase format ---- 
    #********************
    write.table(Operations,paste(path.export,cruise,'_Operations4Echobase.csv',sep=''),
                row.names=FALSE,sep=';')
    cat('-> Operation data exported','\n')
    write.table(OperationMetadataValue,
                paste(path.export,cruise,'_OperationMetadataValue4Echobase.csv',sep=''),
                row.names=FALSE,sep=';')
    cat('-> Operation metadata exported','\n')
    write.table(GearMetadataValue,paste(path.export,cruise,'_GearMetadataValue4Echobase.csv',sep=''),
                row.names=FALSE,sep=';')
    cat('-> Gear metadata exported','\n')
  } 
  #******************** ------  
  # 5.2. Fishing data -----
  #******************** ------
  # 5.2.0.1. Export LW equations ----------
  #******************** -----
  if (class(LW)!='NULL'){
    if (class(LW)=='character'){
      LW0=read.table(LW,header=TRUE,sep=';')
    }else if (class(LW)=='data.frame'){
      LW0=LW
    }
    #dev.off()
    #LW0=LW.compute(catch=mens.cor0[mens.cor0$GENR_ESP%in%lspcodes,],
    #               codespname='CodEsp2',compute.LW=TRUE,
    #               Lname='Lcm',Wname='PM',wname='NBIND',plotit=FALSE,
    #               Ncol=4,Wscaling=1000,cruise=cruise)
    head(LW0)
    #LW0$sp=substr(LW0$CodEsp2,1,8)
    #LW0$sc=substr(LW0$CodEsp2,10,10)
    LW4Echobase=LW0[,c('CAMPAGNE','CATEG','a','b','GENR_ESP')]
    names(LW4Echobase)=c('voyage','sizeCategory','aParameter','bParameter',
                         'baracoudaCode')
    LW4Echobase$voyage=cruise
    LW4Echobase$strata=strata
    head(LW4Echobase)
    LW4Echobase=LW4Echobase[complete.cases(LW4Echobase),]
    
    write.table(LW4Echobase,paste(path.export,cruise,'_LW4Echobase.csv',sep=''),sep=';',
                row.names=FALSE)
    cat('-> Length-weight data exported','\n')
  }else{
    LW4Echobase=NA
  }
  
  #********************
  # 5.2.0.2. Export LA equations ---------
  #******************** ------
  if (class(LA)!='NULL'){
    if (class(LA)=='character'){
      LA0=read.table(LA,header=TRUE,sep=';')
    }else{
      LA0=LA
    }  
    LA4Echobase=LA0[,c('CAMPAGNE','GENRE_ESP','AGE','TAILLE',
                       'POURCENTAGE')]
    head(LA4Echobase)
    names(LA4Echobase)=c('voyage','baracoudaCode','age','length',
                         'percentAtAge')
    LA4Echobase$voyage=cruise
    LA4Echobase$metadata='Ifremer reading'
    LA4Echobase$strata=strata
    names(LA4Echobase)
    
    write.table(LA4Echobase,
                paste(path.export,cruise,'_LA4Echobase.csv',sep=''),sep=';',
                row.names=FALSE)
    cat('-> Length-age data exported','\n')
    
    if (check.ref){
      #check that size categories in data are in Echobase reference table
      # 1.1. Connection to a PostgreSQL database (on a server)
      #********************
      library(RPostgreSQL)
      drv <- dbDriver("PostgreSQL")
      #summary(drv)
      dbListConnections(drv)
      dbGetInfo(drv)
      con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                       password=password,port=port)
      #Import all data
      sqlSizeCat="SELECT name FROM SizeCategory"
      SizeCats <- dbGetQuery(con,sqlSizeCat)
      viv=unique(LA4Echobase$sizeCategory)%in%SizeCats$name
      if (FALSE%in%(viv)) stop('Ages-at-length Size category',
                               unique(LA4Echobase$sizeCategory)[!viv],
                               'not found in Echobase SizeCategory table. Please check.')
      dbDisconnect(con)
    } 
    
  }else{
    LA4Echobase=NA
  }  
  
  if (class(PecheiEB)!='NULL'&class(mensEB)!='NULL'){
    #*********************************************************
    #Export fishing Data
    #*********************************************************
    #Corrected total samples from R biomass assessment code 
    #********************
    #5.2.1. totalSamples ------ 
    #******************** -------------
    if (class(PecheiEB)=='character'){
      Pechei0=read.table(PecheiEB,header=TRUE,sep=';')
    }else{
      Pechei0=PecheiEB
    }
    sort(unique(Pechei0$GENR_ESP))
    if (identical('LW0',character(0))){
      stop('Please provide Length-weight relationship to correct total catches')
    }
    #Pechei0[Pechei0$NOSTA=='X0196'&Pechei0$GENR_ESP=='PLEB-PIL',]
    #sort(unique(Pechei0$GENR_ESP))
    if (exists("Pechei.discard")){
      rm("Pechei.discard")
    }
    #Pechei0[Pechei0$NOSTA=='W0322',]
    pids0=paste(Pechei0$NOSTA,Pechei0$GENR_ESP,
               Pechei0$SIGNEP,sep='-')
    Pechei0[duplicated(pids0),]
    
    Pechei.cors=check.catches(captures.comp=Pechei0,
                              path.checkit=path.export,
                              correctit=TRUE,removeit=TRUE,LW=LW0,
                              LW.tsa=LW.tsa)
    names(Pechei.cors)
    Pechei.cor=Pechei.cors$captures.OK
    #Pechei.cor[Pechei.cor$NOSTA=='W0322',]
    Pechei.cor[is.na(Pechei.cor$LM),'LM']=0
    Pechei4Echobase=Pechei.cor[,c('NOSTA','GENR_ESP','SIGNEP','PT','NT','LM','PM','MOULE','PTRI')]
    names(Pechei4Echobase)=c("operationId","baracoudaCode","sizeCategory",
                             "sampleWeight","numberSampled","meanLength",
                             "meanWeight","noPerKg","sortedWeight")
    Pechei4Echobase[,c("sampleWeight","numberSampled","meanLength","meanWeight","noPerKg","sortedWeight")]=
      apply(Pechei4Echobase[,c("sampleWeight","numberSampled","meanLength","meanWeight","noPerKg","sortedWeight")],2,as.numeric)
    names(Operations)
    sort(unique(Pechei4Echobase$baracoudaCode))
    #Pechei4Echobase[Pechei4Echobase$operationId=='W0322',]
    if (!removeBadCatch){
      Pechei4EchobasePB=Pechei.cors$captures.PB
      Pechei4EchobasePB=Pechei4EchobasePB[,c('NOSTA','GENR_ESP','SIGNEP','PT','NT','LM','PM','MOULE','PTRI')]
      #Pechei4EchobasePB[Pechei4EchobasePB$operationId=='W0322',]
      names(Pechei4EchobasePB)=c("operationId","baracoudaCode","sizeCategory",
                                 "sampleWeight","numberSampled","meanLength",
                                 "meanWeight","noPerKg","sortedWeight")
      head(Pechei4EchobasePB)
      Pechei4EchobasePB[,c("sampleWeight","numberSampled","meanLength","meanWeight","noPerKg","sortedWeight")]=
        apply(Pechei4EchobasePB[,c("sampleWeight","numberSampled","meanLength","meanWeight","noPerKg","sortedWeight")],2,as.numeric)
      Pechei4EchobasePB[,c("sampleWeight","numberSampled","meanLength","meanWeight","noPerKg","sortedWeight")]=
        apply(Pechei4EchobasePB[,c("sampleWeight","numberSampled","meanLength","meanWeight","noPerKg","sortedWeight")],2,na.null)
      Pechei4EchobasePB[is.infinite(Pechei4EchobasePB$numberSampled),"numberSampled"]=0
      Pechei4EchobasePB[is.infinite(Pechei4EchobasePB$noPerKg),"noPerKg"]=0
      Pechei4EchobasePB[is.infinite(Pechei4EchobasePB$meanLength),"meanLength"]=0
      Pechei4EchobasePB[is.infinite(Pechei4EchobasePB$meanWeight),"meanWeight"]=0
      Pechei4EchobasePB=Pechei4EchobasePB[order(Pechei4EchobasePB$operationId),]
      names(Pechei4Echobase)
      Pechei4Echobase=rbind(Pechei4Echobase,Pechei4EchobasePB)
    }else{
      Pechei.discard=Pechei.cors$captures.PB
    }
    #Pechei4Echobase[Pechei4Echobase$operationId=='W0322',]
    sort(unique(Pechei4Echobase$baracoudaCode))
    head(Pechei4Echobase)
    pids=paste(Pechei4Echobase$operationId,Pechei4Echobase$baracoudaCode,
               Pechei4Echobase$sizeCategory,sep='-')
    Pechei4Echobase[duplicated(pids),]
    # 5.2.1.1. Check nos. of operations -----
    length(unique(Pechei4Echobase$operationId))
    length(unique(Pechei0$NOSTA))
    ops.check=check.ops(Operations,Pechei2Echobase=Pechei4Echobase,
                        exceptGear = exceptGear)
    #check size categories 
    cat('Unique size categories in catches:',
        as.character(unique(Pechei4Echobase$sizeCategory)),'\n')
    #Pechei4Echobase[Pechei4Echobase$operationId=='Q0563',]
    
    if (check.ref){
      # 5.2.1.2. Check size categories in Echobase reference table -----
      # 1.1. Connection to a PostgreSQL database (on a server)
      #********************
      library(RPostgreSQL)
      drv <- dbDriver("PostgreSQL")
      #summary(drv)
      dbListConnections(drv)
      dbGetInfo(drv)
      con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                       password=password,port=port)
      #Import all sizecategories
      #********************
      sqlSizeCat="SELECT name FROM SizeCategory"
      SizeCats <- dbGetQuery(con,sqlSizeCat)
      viv=unique(Pechei4Echobase$sizeCategory)%in%SizeCats$name
      if (FALSE%in%(viv)){
        stop('Total catches size category ',
             unique(Pechei4Echobase$sizeCategory)[!viv],
             ' not found in Echobase SizeCategory table. Please check.')
      }else{
        cat('All size categories in catch input found in Echobase','\n')
      }  
      #Import all species
      #********************
      sqlSpecies="SELECT * FROM Species"
      species <- dbGetQuery(con,sqlSpecies)
      dbDisconnect(con)
      # 5.2.1.3. Check species in total samples -----
      if (''%in%unique(Pechei4Echobase$baracoudaCode)){
        cat('!!!!!!! Species with null name found in total samples in:',
             paste(unique(Pechei4Echobase[Pechei4Echobase$baracoudaCode=='','operationId']),collapse=' '),
             '. TotalSamples removed !!!!!!!','\n')
        Pechei4Echobase=Pechei4Echobase[Pechei4Echobase$baracoudaCode!='',]
        Pechei.discardi=Pechei4Echobase[Pechei4Echobase$baracoudaCode=='',]
        Pechei.discardi=dfnames.converter(x=Pechei.discardi,template='peche') 
        if (dim(Pechei.discard)[1]>0){
          Pechei.discard=plyr::rbind.fill(Pechei.discard,Pechei.discardi)
        }else{
          Pechei.discard=Pechei.discardi
        }
      }
      viv=unique(Pechei4Echobase$baracoudaCode)%in%species$baracoudacode
      if (FALSE%in%(viv)){
        stop('Species ',paste(unique(Pechei4Echobase$baracoudaCode)[!viv],collapse=' '),
             ' not found in Echobase species table. Please check.')
      }else{
        cat('All species in total samples input found in Echobase reference table','\n')
      }
      sort(unique(Pechei4Echobase$baracoudaCode))
      viv=unique(Pechei4Echobase$baracoudaCode)%in%species$baracoudacode
      sps=species[species$baracoudacode%in%Pechei4Echobase$baracoudaCode,
                  c('baracoudacode','wormscode','genusspecies')]
      spsNA=sps[sps$wormscode=='NA',]
      species=species[order(species$baracoudacode),]
      if (dim(spsNA)[1]>0){
        stop('Species ',paste(spsNA$baracoudacode,collapse=' '),
             ' in total samples have NA Worms code. Please check.')
      }else{
        cat('All species in catch input found in Echobase','\n')
      } 
    }
    Pechei4Echobase.nasp=Pechei4Echobase[is.na(Pechei4Echobase$baracoudaCode),]
    if (dim(Pechei4Echobase.nasp)[1]>0){
      stop('NA found in species name in totalsample file')
    }    
    unique(Pechei4Echobase$baracoudaCode)
    head(Pechei4Echobase)
    unique(Pechei4Echobase$operationId)
    #Pechei4Echobase[Pechei4Echobase$operationId=='W0322',]
    
    # Remove 'CHAL-NUL' species
    Pechei4Echobase=Pechei4Echobase[Pechei4Echobase$baracoudaCode!='CHAL-NUL',]
    Pechei.discardi=Pechei4Echobase[Pechei4Echobase$baracoudaCode=='CHAL-NUL',]
    Pechei.discardi=dfnames.converter(x=Pechei.discardi,template='peche') 
    if (exists("Pechei.discard")){
      if (dim(Pechei.discard)[1]>0){
      Pechei.discard=plyr::rbind.fill(Pechei.discard,Pechei.discardi)
      }else{
        Pechei.discard=Pechei.discardi
      }
    }
    # 5.2.1.4. Check total catches-operation links  -----
    #************************************
    head(Pechei4Echobase)
    lop.missing=unique(Pechei4Echobase$operationId)[!unique(Pechei4Echobase$operationId)%in%
                                                      unique(Operations$operationId)]
    if (length(lop.missing)>0){
      stop(paste('Total samples without operation:',lop.missing,'\n'),'\n')
    }
    #Pechei4Echobase[Pechei4Echobase$operationId=="X0168",]
    # 5.2.1.5. Export total catches ----- 
    #********************
    # check LW
    # head(Pechei4Echobase)
    # EBtotalSamples.LW=LW.compute(catch=Pechei4Echobase,lspcodes.mens=NULL,
    #                              codespname='baracoudaCode',Lname='meanLength',Wname='meanWeight',
    #                              gname=NA,plotit=TRUE,compute.LW=TRUE,Wscaling=1,ux11=FALSE,Nmin=10)
    #Pechei4Echobase[Pechei4Echobase$operationId=='X0196'&Pechei4Echobase$baracoudaCode=='PLEB-PIL',]
    write.table(Pechei4Echobase,paste(path.export,cruise,'_totalSamples4Echobase.csv',sep=''),sep=';',
                row.names=FALSE)
    if (exists("Pechei.discard")){
      write.table(Pechei.discard,paste(path.export,cruise,'_totalSamplesDiscarded.csv',sep=''),
                row.names=FALSE,sep=';')
    }  
    #     write.table(Pechei4EchobasePB,paste(path.export,cruise,'_totalSamplesR4EchobasePB.csv',sep=''),sep=';',
    #                 row.names=FALSE)
    cat('-> Total samples data exported','\n')
    
    # Pechei0[Pechei0$NOSTA=='X0138'&
    #           Pechei0$GENR_ESP=='SCOM-SCO',]
    # Pechei4Echobase[Pechei4Echobase$operationId=='X0168'&
    #                   Pechei4Echobase$baracoudaCode=='SCOM-SCO',]

    # 5.2.2. subsamples at-lengths data ---------
    #******************** ----------
    if (class(mensEB)=='character'){
      mens.cor0=read.table(mensEB,header=TRUE,sep=';')
    }else{
      mens.cor0=mensEB
    }    
    names(mens.cor0) 
    length(unique(mens.cor0$NOSTA))
    mens.cor0[mens.cor0$NOSTA=='X999',]
    mens4Echobase=mens.cor0[,c('NOSTA','GENR_ESP','CATEG','SEXE',
                               'POIDSECHANT','NECHANT','Lcm','NBIND',
                               'POIDSTAILLE','UNITE','INC')]
    names(mens4Echobase)=c('operationId','baracoudaCode',
                           'sizeCategory','sexCategory',
                           'sampleWeight','numberSampled',
                           'lengthClass','numberAtLength',
                           'weightAtLength','units','round')
    head(mens4Echobase)
    dim(mens4Echobase)
    unique(mens4Echobase$operationId)
    # 5.2.2.1. remove duplicated rows -----
    mens4Echobase=mens4Echobase[order(mens4Echobase$operationId,
                                      mens4Echobase$baracoudaCode,mens4Echobase$sizeCategory),]
    mens4Echobase=unique(mens4Echobase)
    head(mens4Echobase)
    ss.ids=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,mens4Echobase$sizeCategory,
                 mens4Echobase$lengthClass)
    #dids=ss.ids[substr(ss.ids,1,5)=='V0308']
    #mens4Echobase[mens4Echobase$operationId=='V0308'&mens4Echobase$baracoudaCode=='AMMO-TOB',]
    #mens4Echobase[mens4Echobase$operationId=='T0261',]
    if (length(ss.ids)>length(unique(ss.ids))){
      cat('Duplicated sub-samples aggregated:',paste(ss.ids[duplicated(ss.ids)],collapse=' '),'\n')
      mens4Echobasea=aggregate(mens4Echobase[,c('sampleWeight','numberSampled',
                                                'numberAtLength',
                                                'weightAtLength')],
                              list(operationId=mens4Echobase$operationId,
                                   baracoudaCode=mens4Echobase$baracoudaCode,
                                   sizeCategory=mens4Echobase$sizeCategory,
                                   lengthClass=mens4Echobase$lengthClass,
                                   sexCategory=mens4Echobase$sexCategory),
                              sum,na.rm=TRUE)
      head(mens4Echobasea)
      mens4Echobaseamax=aggregate(mens4Echobase[,c('sampleWeight','numberSampled')],
                                  list(operationId=mens4Echobase$operationId,
                                       baracoudaCode=mens4Echobase$baracoudaCode,
                                       sizeCategory=mens4Echobase$sizeCategory,
                                       sexCategory=mens4Echobase$sexCategory),
                                  max,na.rm=TRUE)
      names(mens4Echobaseamax)[5:6]=paste(names(mens4Echobaseamax)[5:6],'Max',sep='')
      mens4Echobasea=merge(mens4Echobasea,mens4Echobaseamax)
      head(mens4Echobasea)
      mens4Echobasea$sampleWeight=mens4Echobasea$sampleWeightMax
      mens4Echobasea$numberSampled=mens4Echobasea$numberSampledMax
      mens4Echobasea[mens4Echobasea$weightAtLength==0,'weightAtLength']=NA
      mens4Echobase=merge(unique(mens4Echobase[,c('operationId','baracoudaCode',
                                                  'sizeCategory',
                                                  'sexCategory','units','round')]),
                          mens4Echobasea,by.x=c('operationId','baracoudaCode',
                                                'sizeCategory','sexCategory'),
        by.y=c('operationId','baracoudaCode','sizeCategory','sexCategory'))
      mens4Echobase=mens4Echobase[,!names(mens4Echobase)%in%c('sampleWeightMax','numberSampledMax')]
      #mens4Echobase[mens4Echobase$operationId=='V0308'&mens4Echobase$baracoudaCode=='AMMO-TOB',]
    }else{
      cat('No duplicates found in sub-samples','\n')
    }
    ss.ids=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,mens4Echobase$sizeCategory,
                 mens4Echobase$lengthClass)
    length(ss.ids)
    length(unique(ss.ids))
    if (length(ss.ids)>length(unique(ss.ids))){
      stop('Duplicated sub-samples in:',paste(ss.ids[duplicated(ss.ids)],collapse=' '))
    }  
    head(mens4Echobase)
    unique(mens4Echobase$operationId)
    
    # 5.2.2.2. Remove null number at length -----------
    # Remove null or NA number at length or length class
    nullmens4Echobase=mens4Echobase[(is.na(mens4Echobase$numberAtLength))|
                                      (mens4Echobase$numberAtLength==0)|
                                      is.na(mens4Echobase$lengthClass),]
    
    if (dim(nullmens4Echobase)[1]>0){
      cat('=> null subsamples removed in:','\n')
      print(nullmens4Echobase)
      rdi=mens4Echobase[(is.na(mens4Echobase$numberAtLength))|
                                    (mens4Echobase$numberAtLength==0)|
                                    is.na(mens4Echobase$lengthClass),]
      mens4Echobase=mens4Echobase[(!is.na(mens4Echobase$numberAtLength))&
                                    (mens4Echobase$numberAtLength!=0)&
                                    !is.na(mens4Echobase$lengthClass),]
      if  (dim(rdi)[1]>0&!exists("rd")){
        rd=rdi
      }else if (dim(rd)[1]>0){
        rd=plyr::rbind.fill(rd,mens.discardi)
      }
    }

    # 5.2.2.7. Check infinite weightAtLength 
    # mensinf=mens4Echobase[is.infinite(mens4Echobase$weightAtLength),]
    # if (dim(mensinf)[1]>0){
    #   cat('Subsamples with infinite weight at length:','\n')
    #   print(mensinf)
    #   cat('=> Subsamples with infinite weight at length removed','\n')
    #   mens4Echobase=mens4Echobase[!is.infinite(mens4Echobase$weightAtLength),]
    #   mens.discardi=mens4Echobase[is.infinite(mens4Echobase$weightAtLength),]
    #   #mens.discardi=dfnames.converter(x=mens.discardi)
    #   #names(mens.discardi)
    #   if  (dim(mens.discardi)[1]>0&!exists("mens.discard")){
    #     mens.discard=mens.discardi
    #   }else if (dim(mens.discard)[1]>0){
    #     mens.discard=plyr::rbind.fill(mens.discard,mens.discardi)
    #   }
    # }
    
    # 5.2.2.8. Check NA weightAtLength --------------
    mensWna=mens4Echobase[is.infinite(mens4Echobase$weightAtLength)|
                            is.na(mens4Echobase$weightAtLength)|
                            mens4Echobase$weightAtLength==0,]
    mens4Echobase0=mens4Echobase[(!is.infinite(mens4Echobase$weightAtLength))&
                                   (!is.na(mens4Echobase$weightAtLength))&
                                   (mens4Echobase$weightAtLength!=0),]
    if (dim(mensWna)[1]>0){
      cat('Subsamples with NA weight at length:','\n')
      print(mensWna)
      head(mens4Echobase)
      
      if (correct.nullWatLength){
        if (identical('LW0',character(0))){
          stop('Please provide Length-weight relationship to correct sub-samples')
        }
        mensWna=merge(mensWna,LW0[,c('GENR_ESP','a','b')],
                      by.x=c('baracoudaCode'),
                      by.y=c('GENR_ESP'),all.x=TRUE)
        mensWna[!is.na(mensWna$a),'weightAtLength']=
          (mensWna[!is.na(mensWna$a),'numberAtLength']*mensWna[!is.na(mensWna$a),'a']*
          (mensWna[!is.na(mensWna$a),'lengthClass']^(mensWna[!is.na(mensWna$a),'b'])))/
            1000
        #mensWna[mensWna$baracoudaCode=="SCOM-COL"&mensWna$operationId=="U0198",]
        if (removeBadSubSamples){
          mens.discardi=mensWna[(is.na(mensWna$weightAtLength))|
                                  (mensWna$weightAtLength==0),names(mens4Echobase0)]
          if (dim(mens.discardi)[1]>0&!exists("mens.discard")){
            mens.discard=mens.discardi
          }else if (dim(mens.discard)[1]>0){
            mens.discard=plyr::rbind.fill(mens.discard,mens.discardi)
          }
          mensWna=mensWna[(!is.na(mensWna$weightAtLength))&
                            (mensWna$weightAtLength!=0),]
          mens4Echobase=rbind(mens4Echobase0,mensWna[,names(mens4Echobase0)])
          cat('=> Subsamples with missing weight at length corrected or removed','\n')
        }else{
          mensWna[is.na(mensWna$weightAtLength)|
                    is.infinite(mensWna$weightAtLength),'weightAtLength']=0
          mens4Echobase=rbind(mens4Echobase0,mensWna[,names(mens4Echobase0)])
          cat('=> Subsamples with missing weight at length corrected
              and remaining missing weight set to zero','\n')
        }
      }else if (removeBadSubSamples){
        cat('=> Subsamples with missing weight at length removed','\n')
        mens.discardi=mens4Echobase[(!is.na(mens4Echobase$weightAtLength))|
                                      (mens4Echobase$weightAtLength==0),]
        if (dim(mens.discardi)[1]>0&!exists("mens.discard")){
          mens.discard=mens.discardi
        }else if (dim(mens.discard)[1]>0){
          mens.discard=plyr::rbind.fill(mens.discard,mens.discardi)
        }
        mens4Echobase=mens4Echobase[(!is.na(mens4Echobase$weightAtLength))&
                                      (mens4Echobase$weightAtLength!=0),]
      }else{
        mensWna[is.na(mensWna$weightAtLength)|
                  is.infinite(mensWna$weightAtLength),'weightAtLength']=0
        mens4Echobase=rbind(mens4Echobase0,mensWna[,names(mens4Echobase0)])
        cat('=> Subsamples with missing weight at length set to zero','\n')
      }
    }
    head(mens4Echobase)
    if (LW.check){
      mens4Echobase$meanWeight=mens4Echobase$weightAtLength/mens4Echobase$numberAtLength
      EBsubSamples.LW=LW.compute(catch=mens4Echobase,lspcodes.mens=NULL,
                                 codespname='baracoudaCode',Lname='lengthClass',
                                 Wname='meanWeight',wname='numberAtLength',
                                 plotit=TRUE,compute.LW=TRUE,Wscaling=1000,ux11=FALSE)
      mtext('1st subsample export')
    }
    
    # 5.2.2.3. check sub/total samples links ----
    #5.2.2.4. Check duplicated total samples ------
    #5.2.2.5. Check operations / total samples links -------- 
    # and remove operations without total catches if any
    #5.2.2.6. Add operations / total samples links --------
    #************************************
    res.check=checkSubTotalSamplesOperationsCoherence(
      mens4Echobase,Pechei4Echobase,Operations,OperationMetadataValue,
      GearMetadataValue,exceptGear=exceptGear,
      TotCatchOpeCor=TotCatchOpeCor,path.export=path.export)
    mens4Echobase=res.check[[1]]
    Pechei4Echobase=res.check[[2]]
    Operations=res.check[[3]]
    OperationMetadataValue=res.check[[4]]
    GearMetadataValue=res.check[[5]]
    
    if (check.ref){
      # 5.2.2.9. check size categories ---------------
      # 1.1. Connection to a PostgreSQL database (on a server)
      #********************
      library(RPostgreSQL)
      
      drv <- dbDriver("PostgreSQL")
      #summary(drv)
      dbListConnections(drv)
      dbGetInfo(drv)
      
      con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                       password=password,port=port)
      #Import all data
      sqlSizeCat="SELECT name FROM SizeCategory"
      SizeCats <- dbGetQuery(con,sqlSizeCat)
      dbDisconnect(con)
      viv=unique(mens4Echobase$sizeCategory)%in%SizeCats$name
      if (FALSE%in%(viv)){
        stop('Catches-at-length size category ',
             unique(mens4Echobase$sizeCategory)[!viv],
             ' not found in Echobase SizeCategory table. Please check.')
      }else{
        cat('All size categories in catch-at-length input found in Echobase','\n')
      }
      # 5.2.2.10. check species ---------------      
      if (''%in%unique(mens4Echobase$baracoudaCode)){
        cat('!!!!!!!!!! Species with null name found in subs-samples in:',
            paste(unique(mens4Echobase[mens4Echobase$baracoudaCode=='','operationId']),collapse=' '),
            '. Subsamples removed !!!!!!!!!!!!!!!','\n')
        mens4Echobase=mens4Echobase[mens4Echobase$baracoudaCode!='',]
        mens.discardi=mens4Echobase[mens4Echobase$baracoudaCode=='',]
        if  (dim(mens.discardi)[1]>0&!exists("mens.discard")){
          mens.discard=mens.discardi
        }else if (dim(mens.discard)[1]>0){
          mens.discard=plyr::rbind.fill(mens.discard,mens.discardi)
        }
      }
      viv=unique(mens4Echobase$baracoudaCode)%in%species$baracoudacode
      sps=species[species$baracoudacode%in%mens4Echobase$baracoudaCode,
                  c('baracoudacode','wormscode','genusspecies')]
      spsNA=sps[sps$wormscode=='NA',]
      species=species[order(species$baracoudacode),]
      if (sum(!viv)>0){
        stop('Species ',paste(unique(mens4Echobase$baracoudaCode)[!viv],collapse=' '),
             'in sub-samples have NA Worms code. Please check.')
      }else{
        cat('All species in subsamples input found in Echobase','\n')
      } 
    } 

    # 5.2.2.11. re-calculate subsamples total weight and number ---- 
    # for internal consistency 
    #********************
    sscc=subsampleConsistencyCheck(mens4Echobase)
    mens4Echobase=sscc$mens4Echobase
    
    if (LW.check){
      mens4Echobase$meanWeight=mens4Echobase$weightAtLength/mens4Echobase$numberAtLength
      EBsubSamples.LW=LW.compute(catch=mens4Echobase,lspcodes.mens=NULL,
                                 codespname='baracoudaCode',Lname='lengthClass',
                                 Wname='meanWeight',wname='numberAtLength',
                                 plotit=TRUE,compute.LW=TRUE,Wscaling=1000,ux11=FALSE)
      mtext('Corrected subsamples (total-sub) export')
    }

    # mens4Echobase[mens4Echobase$operationId=='X0168'&
    #                 mens4Echobase$baracoudaCode=='SCOM-SCO',]
    # Pechei4Echobase[Pechei4Echobase$operationId=='X0168'&
    #                   Pechei4Echobase$baracoudaCode=='SCOM-SCO',]
    
    if (LW.check){
      mens4Echobase$meanWeight=mens4Echobase$weightAtLength/mens4Echobase$numberAtLength
      EBsubSamples.LW=LW.compute(catch=mens4Echobase,lspcodes.mens=NULL,
                                 codespname='baracoudaCode',Lname='lengthClass',
                                 Wname='meanWeight',wname='numberAtLength',
                                 plotit=TRUE,compute.LW=TRUE,Wscaling=1000,ux11=FALSE)
      mtext('Corrected subsamples (final sub) export')
    }
    
    # 5.2.2.12. Export subsamples -------
    #********************
    df=mens4Echobase
    df$id=paste(df$operationId,df$baracoudaCode,
                df$sizeCategory,df$lengthClass)
    length(df$id);length(unique(df$id))
    
    dfd=df[df$id%in%df$id[duplicated(df$id)],]
    mens4Echobase=unique(mens4Echobase)
    # for compliance with Atlantos format, remove "units" column
    mens4Echobase=mens4Echobase[,names(mens4Echobase)!='units']
    head(mens4Echobase)
    
    mens4Echobase.nasp=mens4Echobase[is.na(mens4Echobase$baracoudaCode),]
    if (dim(mens4Echobase.nasp)[1]>0){
      stop('NA found in species name in subsample file')
    }
    ssnames=c("operationId","baracoudaCode","sizeCategory",
              "sexCategory","sampleWeight","numberSampled",
              "lengthClass","numberAtLength","weightAtLength","round")
    mens4Echobase=mens4Echobase[,ssnames]
    write.table(mens4Echobase,
                paste(path.export,cruise,'_subSamples4Echobase.csv',sep=''),sep=';',
                row.names=FALSE)
    cat('-> Sub-samples data exported','\n')
    if (exists("mens.discard")){
      write.table(mens.discard,paste(path.export,cruise,'_subSamplesDiscarded.csv',sep=''),
                  row.names=FALSE,sep=';')
    }
    mens4Echobase.id=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,
                           mens4Echobase$sizeCategory,mens4Echobase$lengthClass)
    if (length(mens4Echobase.id[duplicated(mens4Echobase.id)])>0){
      stop('Duplicated subsamples before biometry')
    }
    
    #Pechei4Echobase[Pechei4Echobase$NOSTA=='X0196'&
    #                  Pechei4Echobase$GENR_ESP=='PLEB-PIL',]
    
    #8. Biometry -----------
    #************************** -----------
    # 8.1. Import Biometry ----- 
    #********************
    if (class(paramBio)!='NULL'){
      if (class(paramBio)!='NULL'){
        if (class(paramBio)=='character'){
          parambio=read.table(paramBio,sep=';',header=TRUE,skip=0)
          if (names(paramBio0)[1]=='survey'){
            head(parambio)
            # names(paramBio0)=c('survey','navire','engin','code_espece','date',
            #                       'num_station','num_chalut','zone','Depth','stratum',
            #                       'ref','LTmm1','Weightg','Sex','Maturity',
            #                       'parasites','graisse','Age','nat_bord')
            parambio$baracoudaCode=parambio$code_espece
            parambio$LTmm1=10*as.numeric(gsub(',','.',parambio$LTmm1))
            parambio$Weightg=as.numeric(parambio$Weightg)
          }else{
            parambio=read.table(paramBio,sep=';',header=TRUE,skip=1)
            head(parambio)
            parambio=parambio[,1:10]
            names(parambio)=c('ref','date','NOCHAL','NOSTA','Depth','LTmm1','Weightg',
                                  'Sex','Maturity','Age')
            parambio$baracoudaCode=parambio$code_espece
          }
        }else{
          parambio=paramBio
          parambio$baracoudaCode=parambio$code_espece
        }     
      }
      parambio$numFish=seq(dim(parambio)[1])
      head(parambio)
      # convert sex codes
      if ('M'%in%unique(parambio$Sex)){
        parambio$Sex=as.character(parambio$Sex)
        parambio[parambio$Sex=='M','Sex']=1
        parambio[parambio$Sex=='F','Sex']=2
        cat('Sex codes converted to EchoBase format','\n')
      }
      # path.biometryRef=paste(donnees2,'Campagnes/PELGAS/Data/Biometries/BiometryReferences.txt',sep='')
      # biometryRef=read.table(path.biometryRef,sep=';',header=TRUE)
      # head(biometryRef)
      
      if (check.ref){
        #check sampledatatypes in Echobase reference table
        # 1.1. Connection to a PostgreSQL database (on a server)
        #********************
        library(RPostgreSQL)
        drv <- dbDriver("PostgreSQL")
        #summary(drv)
        dbListConnections(drv)
        dbGetInfo(drv)
        con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                         password=password,port=port)
        sqlSampleDataType="SELECT * FROM SampleDataType"
        sampleDataType <- dbGetQuery(con,sqlSampleDataType)
        dbDisconnect(con)
        head(sampleDataType)
      }
      # biometryScale=merge(biometryRef,sampleDataType,
      #                     by.x='Code_Param_Biometrie_Echelle',
      #                     by.y='raptribiometrydatatype')
      
      parambio.long=reshape(
        parambio[,c('numFish','LTmm1','Weightg','Sex','Maturity','Age')],
        idvar=c('numFish'),varying=list(2:6),v.names="dataValue",
        direction="long",
        times=names(
          parambio[,c('numFish','LTmm1','Weightg','Sex','Maturity','Age')])[2:6],
                            timevar="name")
      head(parambio.long)
      parambio.longs=parambio.long[!is.na(parambio.long$dataValue),]
      parambio.longs=merge(
        parambio.longs,
        unique(parambio[,c('numFish','baracoudaCode','num_station')]),
                           by.x=c('numFish'),by.y=c('numFish'))
      head(parambio.longs)
      #convert string dataValues to numeric
      # if (class(parambio.longs$dataValue)=='character') stop('non numeric dataValues in biometries')
      # parambio.longs[parambio.longs$dataValue%in%c('H'),'dataValue']=2
      # parambio.longs[parambio.longs$dataValue%in%c('O'),'dataValue']=1
      # parambio.longs$dataValue=as.numeric(parambio.longs$dataValue)
      # unique(parambio.longs$dataValue)
      
      #format output files
      Biometry=parambio.longs[,c('num_station','baracoudaCode','numFish','dataValue','name')]
      names(Biometry)=c('operationId','baracoudaCode','numFish','dataValue','name')
      Biometry$dataLabel=NA
      head(Biometry)
      unique(Biometry$operationId)
      unique(Pechei4Echobase$operationId)
      # 8.2. Check biometry consistency with total and subsamples -------
      #************************
      mens4Echobase.id=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,
                             mens4Echobase$sizeCategory,mens4Echobase$lengthClass)
      mens4Echobase.id[duplicated(mens4Echobase.id)]
      
      bccr=Biometry.consistency.check(Biometry=Biometry,
                                      Pechei4Echobase=Pechei4Echobase,
                                      mens4Echobase=mens4Echobase,
                                      path.export=path.export,cruise=cruise,
                                      LW.check=LW.check,
                                      removeBadCatch=removeBadCatch,
                                      ssnames=ssnames)
      names(bccr)
      Biometry=bccr$Biometry
      mens4Echobase=bccr$mens4Echobase
      head(mens4Echobase)
      # 8.3. Export biometries ----
      #************************
      write.table(Biometry,
                  paste(path.export,cruise,'_biometry4Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)
      cat('-> Biometry data exported','\n')
      
      # 8.4. Biotic data consistency check -----
      #********************

      # Check sub-samples consistency
      #********************
      ss.ids=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,
                   mens4Echobase$sizeCategory,mens4Echobase$lengthClass)
      length(ss.ids);length(unique(ss.ids))
      if (length(ss.ids)>length(unique(ss.ids))){
        stop('Duplicated sub-samples in:',paste(ss.ids[duplicated(ss.ids)],collapse=' '))
      }
      # Re-calculate subsamples total weight and number 
      # for internal consistency 
      sscc=subsampleConsistencyCheck(mens4Echobase)
      names(sscc)
      mens4Echobase=sscc$mens4Echobase
      head(mens4Echobase)
      mens4Echobasea2=sscc$mens4Echobasea2
      # Export subsamples
      write.table(mens4Echobase,
                  paste(path.export,cruise,'_subSamples4Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)
      
      # Check total-samples consistency
      #********************
      ss.ids1=paste(Pechei4Echobase$operationId,
                    Pechei4Echobase$baracoudaCode,
                    Pechei4Echobase$sizeCategory)
      length(ss.ids1);length(unique(ss.ids1))
      if (length(ss.ids1)>length(unique(ss.ids1))){
        stop('Duplicated total-samples in:',paste(ss.ids1[duplicated(ss.ids1)],collapse=' '))
      }
      # Correct subsamples numbers and weights in total samples
      head(Pechei4Echobase)
      head(mens4Echobasea2)
      dim(Pechei4Echobase)
      Pechei4Echobase=merge(Pechei4Echobase,
                            mens4Echobasea2[,c('operationId','baracoudaCode',
                                               'sizeCategory',
                                               'sampleWeight2',
                                               'numberSampled2')],
                            by.x=c('operationId','baracoudaCode',
                                   'sizeCategory'),
                            by.y=c('operationId','baracoudaCode',
                                   'sizeCategory'),all.x=TRUE)
      dim(Pechei4Echobase)
      Pechei4Echobase[is.na(Pechei4Echobase$sampleWeight2),]
      plot(Pechei4Echobase$sortedWeight,Pechei4Echobase$sampleWeight2)
      # replace non NA sortedWeights and sampleWeight2 
      # by re-calculated subsamples sorted weights
      Pechei4Echobase[!is.na(Pechei4Echobase$sortedWeight)&
                        !is.na(Pechei4Echobase$sampleWeight2)&
                        Pechei4Echobase$sortedWeight<
                        Pechei4Echobase$sampleWeight2,
                      'sortedWeight']=
        Pechei4Echobase[!is.na(Pechei4Echobase$sortedWeight)&
                          !is.na(Pechei4Echobase$sampleWeight2)&
                          Pechei4Echobase$sortedWeight<
                          Pechei4Echobase$sampleWeight2,
                        'sampleWeight2']
      Pechei4Echobase[is.na(Pechei4Echobase$sortedWeight),]
      Pechei4Echobase=Pechei4Echobase[,!names(Pechei4Echobase)%in%
                                        c('sampleWeight2')]
      head(Pechei4Echobase)
      # Replace sampleWeight by sortedWeight if sortedWeight>sampleWeight
      Pechei4Echobase[Pechei4Echobase$sortedWeight>
                        Pechei4Echobase$sampleWeight,'sampleWeight']=
        Pechei4Echobase[Pechei4Echobase$sortedWeight>
                          Pechei4Echobase$sampleWeight,'sortedWeight']
      # Replace numberSampled by numberSampled2 if numberSampled2>numberSampled
      Pechei4Echobase[!is.na(Pechei4Echobase$numberSampled2)&
                        (Pechei4Echobase$numberSampled2>
                        Pechei4Echobase$numberSampled),'numberSampled']=
        Pechei4Echobase[!is.na(Pechei4Echobase$numberSampled2)&
                          (Pechei4Echobase$numberSampled2>
                          Pechei4Echobase$numberSampled),'numberSampled2']
      Pechei4Echobase=Pechei4Echobase[,!names(Pechei4Echobase)%in%
                                        c('numberSampled2')]
      #cat('-> Total samples sorted weights corrected','\n')
      # Export totalSamples
      write.table(Pechei4Echobase,
                  paste(path.export,cruise,'_totalSamples4Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)
      
      # Check biometry consistency
      #********************
      ss.ids2=paste(Biometry$operationId,Biometry$baracoudaCode,
                   Biometry$sizeCategory,Biometry$numFish,
                   Biometry$name)
      length(ss.ids2);length(unique(ss.ids2))
      if (length(ss.ids2)>length(unique(ss.ids2))){
        stop('Duplicated biometry in:',paste(ss.ids2[duplicated(ss.ids2)],
                                             collapse=' '))
      }
      cat('==> Biotic data consistency checked','\n')
    }
  }else{
    Pechei4Echobase=NA
    mens4Echobase=NA
    Biometry=NA
  }
  
  #********************************************************* ----------
  # 9. Export echo-integration data --------
  #********************************************************* -------------
  # 9.1. Create EI file based on laytot file -----  
  if (class(laytot)!='NULL'){
      if (lay.format=='lay'){
        #9.1.1. Import lay files in lay format ----
        #********************
        head(laytot)
        path.lay=laytot
        lf.lay=list.files(path.lay,pattern='*.lay')
        pel11.lay.38=import.lay.folder2(path.lay,mergesu=T,add.laydef=TRUE,
                                        IDfile=FALSE)
        names(pel11.lay.38)
        pel11.sa.38=pel11.lay.38$sa.db
        head(pel11.sa.38)
        pel11.Ni.38=pel11.lay.38$Ni.db
        head(pel11.Ni.38)
        pel11.Nt.38=pel11.lay.38$Nt.db
        head(pel11.Nt.38)
        nsl=pel11.sa.38[1,'NSL']
        nbl=pel11.sa.38[1,'NBL']  
        
        #Select ESUs infos
        #********************
        esu.long=pel11.sa.38[,c('t','x','y','dpth')]
        esu.long$st=c(strptime('2011-04-26 08:24:02',"%Y-%m-%d %H:%M:%OS",tz="GMT"),
                      esu.long$t[1:(length(esu.long$t)-1)])
        
        #Reshape lay data into long format
        #********************
        head(pel11.sa.38)
        dim(pel11.sa.38)
        dim(pel11.sa.38[!complete.cases(pel11.sa.38),])
        head(pel11.sa.38.na)
        pel11.sa.38.na=pel11.sa.38[!complete.cases(pel11.sa.38),]
        pel11.sa.38.na$dpth
        
        sa.long=reshape.lay(lay=pel11.sa.38,lname='sA',vnames=c('SL','BL'))
        saS.long=sa.long$layS.long
        head(saS.long)
        saB.long=sa.long$layB.long
        head(saB.long)
        
        Ni.long=reshape.lay(lay=pel11.Ni.38,lname='Ni',vnames=c('SL','BL'))
        NiS.long=Ni.long$layS.long
        head(NiS.long)
        NiB.long=Ni.long$layB.long
        head(NiB.long)
        
        Nt.long=reshape.lay(lay=pel11.Nt.38,lname='Nt',vnames=c('SL','BL'))
        NtS.long=Nt.long$layS.long
        head(NtS.long)
        NtB.long=Nt.long$layB.long
        head(NtB.long)
        
        z.long=reshape.lay(lay=pel11.sa.38,lname='z',vnames=c('zSL','zBL'))
        zS.long=z.long$layS.long
        head(zS.long)
        zB.long=z.long$layB.long
        head(zB.long)
        
        #Merge surface data
        #********************
        S.long=merge(saS.long,zS.long,by.x=c('t','time','cellType'),
                     by.y=c('t','time','cellType'))
        head(S.long)
        dim(S.long)
        S.long=merge(S.long,NiS.long,by.x=c('t','time','cellType'),
                     by.y=c('t','time','cellType'))
        head(S.long)
        dim(S.long)
        S.long=merge(S.long,NtS.long,by.x=c('t','time','cellType'),
                     by.y=c('t','time','cellType'))
        head(S.long)
        dim(S.long)
        #Merge bottom data
        #********************
        B.long=merge(saB.long,zB.long,by.x=c('t','time','cellType'),
                     by.y=c('t','time','cellType'))
        head(B.long)
        dim(B.long)
        B.long=merge(B.long,NiB.long,by.x=c('t','time','cellType'),
                     by.y=c('t','time','cellType'))
        head(B.long)
        dim(B.long)
        B.long=merge(B.long,NtB.long,by.x=c('t','time','cellType'),
                     by.y=c('t','time','cellType'))
        head(B.long)
        dim(B.long)
        
        #Gather data to be imported in Echobase in a single dataframe 
        #********************
        Mfile=rbind(unique(S.long),unique(B.long))
        dim(B.long);dim(unique(B.long))
        head(Mfile)
        unique(Mfile$time)
        dim(Mfile)
        length(unique(Mfile$t))
        length(pel11.sa.38$t)
        Mfile$lEIthr=-60
        Mfile$uEIthr=0
        dim(Mfile)
        Mfile=merge(Mfile,esu.long,by.x='t',by.y='t')
        dim(Mfile)
        cellnumtype=data.frame(time=c(paste('SL',seq(nsl),sep=''),
                                      paste('BL',seq(nbl),sep=''),'Ltot'),
                               cellnum=seq(0,(nbl+nsl),1))
        Mfile=merge(Mfile,cellnumtype,by.x='time',by.y='time')
        dim(Mfile)
        Mfile=Mfile[order(Mfile$t,Mfile$cellnum),]
        head(Mfile)
        Mfile$sz=Mfile$z
        Mfile[Mfile$cellType==0,'sz']=c(0,Mfile[Mfile$cellType==0,'z'][1:(length(Mfile[Mfile$cellType==0,'z'])-1)])
        Mfile[Mfile$cellType==1,'sz']=c(0,Mfile[Mfile$cellType==1,'z'][1:(length(Mfile[Mfile$cellType==1,'z'])-1)])
        Mfile[Mfile$cellnum==0,'sz']=10
        Mfile[Mfile$cellnum==10,'sz']=0
        Mfile$x=-Mfile$x
        head(Mfile[Mfile$cellType==1,c('z','sz','cellnum')])
        length(unique(Mfile$t))
        Mfile$dataQuality=1
        
        # convert to .csv format
        Mcsv.bases=EIlay2csv(Mfile)
        
      }else if (lay.format=='csv'){
        # 9.1.2. OR import lay files in csv format ==========
        if (class(laytot)=='character'){
          Mcsv.base=read.table(laytot,header=TRUE,sep=';')
        }else{
          # import lay files in dataframe format 
          Mcsv.base=laytot
        }
      }
      # convert to EB EIlay format 
      #**************************
      Mcsv.bases.comps2=convert2EBlay(Mcsv.base=Mcsv.base,
                                      validChannels=validChannels,
                                      neg.nasc.remove=neg.nasc.remove,
                                      CorrectChannelIdent=CorrectChannelIdent,
                                      CorrectAbsorption=CorrectAbsorption,
                                      beamGainCor=beamGainCor,
                                      beamSACor=beamSACor,powerCor=powerCor,
                                      algAsensCor=algAsensCor,
                                      atwAsensCor=atwAsensCor,
                                      CorrectSoundCelerity=CorrectSoundCelerity,
                                      areaCor=areaCor,volCor=volCor,
                                      milli.correct=milli.correct,
                                      fMovies=fMovies,Transectfs=Transectfs,
                                      mantot=mantot)
      
      # Check that all EDSUS have the same number of elementary cells
      names(Mcsv.bases.comps2)
      lesus=unique(Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\dateend"])
      NecEsus=table(Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\dateend"])
      NecEsus2=table(NecEsus)
      if (length(NecEsus2)>1){
        Nerror=names(NecEsus2)[NecEsus2==min(NecEsus2)]
        esus.errors=NecEsus[NecEsus==Nerror]
        nesus.errors=names(esus.errors)
        bad.esus=Mcsv.bases.comps2[Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\dateend"]%in%
                            nesus.errors,]
        dim(bad.esus)
        dim(unique(bad.esus))
        stop("Incorrect number of elementary cells in ESUs: ",
             paste(nesus.errors,collapse=' / '))
      }else{
        cat('All EDSUs have same number of elementary cells','\n')
      }

      # 9.1.3. Export EI file -----------
      # ************************
      #Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\datestart"]
      dim(Mcsv.bases.comps2)
      write.table(Mcsv.bases.comps2,
                  paste(path.export,cruise,'_lay4Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)
      cat('-> EIlayer data exported','\n')
      
      # Export only 38 kHz
      Mcsv.bases.comps2.38=Mcsv.bases.comps2[
        Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel,]
      length(unique(Mcsv.bases.comps2.38[,'MOVIES_EILayer\\cellset\\dateend']))
      head(Mcsv.bases.comps2.38)
      #Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\dateend'][table(Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\dateend'])==0]
      write.table(Mcsv.bases.comps2.38,
                  paste(path.export,cruise,'_lay384Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)
      cat('-> 38kHz EIlayer data exported','\n')
      
      # Plot all esdus 
      # ************************
      Mcsv.base38.4=Mcsv.bases.comps2.38[Mcsv.bases.comps2.38[,'MOVIES_EILayer\\cellset\\celltype']==4,]
      plot(Mcsv.base38.4[,'MOVIES_EILayer\\cellset\\long'],
           Mcsv.base38.4[,'MOVIES_EILayer\\cellset\\lat'],
           cex=log(Mcsv.base38.4[,'MOVIES_EILayer\\eilayer\\sa']+1)/10,pch=16)
      coast()
      
      Mcsv.bases.comps2EK60=Mcsv.bases.comps2[Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']%in%
                                                validChannels[validChannels$type=='mono','id'],]
      Mcsv.bases.comps2ME70=Mcsv.bases.comps2[!Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']%in%
                                                validChannels[validChannels$type=='multi','id'],]
      write.table(Mcsv.bases.comps2EK60,
                  paste(path.export,cruise,'_layEK604Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)
      write.table(Mcsv.bases.comps2ME70,
                  paste(path.export,cruise,'_layME704Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)

    }else if (class(laytot)=='NULL'&convertMan2lay&
              class(mantot)!='NULL'){
        if (class(mantot)!='NULL'){
            # 9.2. OR create missing lay files based on mantot file -----
            EIesdus=unique(mantot$dateend)
            cat('Creating lay file based on mantot file','\n')
            for (i in 1:length(EIesdus)){
              mantotsi=mantot[mantot$dateend==EIesdus[i],]
              newlayi=man2lay(manil=mantotsi)
              if (i==1){
                newlay=newlayi
              }else{
                newlay=rbind(newlay,newlayi,verbose=FALSE)
              }
            }
            Mcsv.base=newlay
            rm('newlay')
            gc()
            
            # convert to EB EIlay format
            #**************************
            names(Mcsv.base)=gsub('[\\]','.',names(Mcsv.base))
            names(laytot)
            
            Mcsv.bases.comps2=convert2EBlay(Mcsv.base=Mcsv.base,validChannels=validChannels,
                                            neg.nasc.remove=neg.nasc.remove,CorrectChannelIdent=CorrectChannelIdent,
                                            CorrectAbsorption=CorrectAbsorption,beamGainCor=beamGainCor,
                                            beamSACor=beamSACor,powerCor=powerCor,algAsensCor=algAsensCor,
                                            atwAsensCor=atwAsensCor,CorrectSoundCelerity=CorrectSoundCelerity,
                                            areaCor=areaCor,volCor=volCor,milli.correct=milli.correct,
                                            fMovies=fMovies,Transectfs=Transectfs,mantot=mantot)
            
            # Remove bottom cells
            #*************************
            # names(Mcsv.bases.comps2)
            # Mcsv.bases.comps2=Mcsv.bases.comps2[Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\celltype"]!=1,]
            
            # 9.2.1. Export EI file -----------
              # ************************
              #Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\datestart"]
              dim(Mcsv.bases.comps2)
              write.table(Mcsv.bases.comps2,
                          paste(path.export,cruise,'_lay4Echobase.csv',sep=''),sep=';',
                          row.names=FALSE)
              cat('-> EIlayer data exported','\n')
              
              # Export only 38 kHz
              Mcsv.bases.comps2.38=Mcsv.bases.comps2[
                Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel,]
              length(unique(Mcsv.bases.comps2.38[,'MOVIES_EILayer\\cellset\\dateend']))
              head(Mcsv.bases.comps2.38)
              #Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\dateend'][table(Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\dateend'])==0]
              write.table(Mcsv.bases.comps2.38,
                          paste(path.export,cruise,'_lay384Echobase.csv',sep=''),sep=';',
                          row.names=FALSE)
              cat('-> 38kHz EIlayer data exported','\n')
              
              # Plot all esdus 
              # ************************
              Mcsv.base38.4=Mcsv.bases.comps2.38[Mcsv.bases.comps2.38[,'MOVIES_EILayer\\cellset\\celltype']==4,]
              plot(Mcsv.base38.4[,'MOVIES_EILayer\\cellset\\long'],
                   Mcsv.base38.4[,'MOVIES_EILayer\\cellset\\lat'],
                   cex=log(Mcsv.base38.4[,'MOVIES_EILayer\\eilayer\\sa']+1)/10,pch=16)
              coast()
              
              Mcsv.bases.comps2EK60=Mcsv.bases.comps2[Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']%in%
                                                        validChannels[validChannels$type=='mono','id'],]
              Mcsv.bases.comps2ME70=Mcsv.bases.comps2[!Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']%in%
                                                        validChannels[validChannels$type=='multi','id'],]
              write.table(Mcsv.bases.comps2EK60,
                          paste(path.export,cruise,'_layEK604Echobase.csv',sep=''),sep=';',
                          row.names=FALSE)
              write.table(Mcsv.bases.comps2ME70,
                          paste(path.export,cruise,'_layME704Echobase.csv',sep=''),sep=';',
                          row.names=FALSE)
        }else{
            stop('Please provide a valid mantot file to generate the EI by layer file','\n')
        }
    }else{
      cat('No laytot or mantot file provided','\n')
    }
  
  #********************************************************* ----------
  # 10. Export echotype definitions -------------
  #********************************************************* -------------
  if (class(EspDev)!='NULL'){
    if (class(EspDev)=='character'){
      EspDev0=read.table(EspDev,header=TRUE,sep=';')
    }else{
      EspDev0=EspDev
    }  
    meta=get.metadata(EspDev0)
    lyears=cruise
    lNdev=meta$lNdev
    EspDev0$CAMPAGNE=cruise
    Ndevi=lNdev[[1]]
    head(EspDev)
    echotypes=unique(EspDev0[,c("CAMPAGNE","DEV","STRATE","DESCR","GENR_ESP","SIGNEP")])
    head(echotypes)
    names(echotypes)=c('voyage','echotypeName','depthStratumId',
                       'meaning','baracoudaCode','sizeCategory')
    echotypes$voyage=cruise
    # adds "ALL" echotype with all species and depth strata
    echall=data.frame(voyage=unique(echotypes$voyage),echotypeName='ALL',
                      depthStratumId='ALL',meaning="All species and depth strata",
                      baracoudaCode=unique(echotypes$baracoudaCode),
                      sizeCategory=unique(echotypes$sizeCategory))
    echotypes=rbind(echotypes,echall)
    
    # make sure that echotype description are unique
    lecho=unique(echotypes$echotypeName)
    echotypes$meaning=as.character(echotypes$meaning)
    for (i in 1:length(lecho)){
      echotypes[echotypes$echotypeName==lecho[i],'meaning']=echotypes[echotypes$echotypeName==lecho[i],'meaning'][1]
    }
    # Set all size categories to zero for compatibility with EB
    echotypes$sizeCategory=0
    echotypes=unique(echotypes)
    if (check.ref){
      # Select only species in species table
      # 8.2.4. Check species in EB species table -----
      library(RPostgreSQL)
      drv <- dbDriver("PostgreSQL")
      dbListConnections(drv)
      dbGetInfo(drv)
      con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                       password=password,port=port)
      sqlSpecies="SELECT * FROM Species"
      species <- dbGetQuery(con,sqlSpecies)
      dbDisconnect(con)
      speciess=species[species$icesexport,]
      viv=unique(echotypes$baracoudaCode)%in%speciess$baracoudacode
      if (FALSE%in%(viv)){
        cat('/!\ Species ',paste(unique(echotypes$baracoudaCode)[!viv],
                             collapse=' '),
             ' not found in Echobase species ICES export table removed from echotype table',
            '\n')
        echotypes=echotypes[echotypes$baracoudaCode%in%speciess$baracoudacode,]
      }else{
        cat('All species in echotypes table found in Echobase reference table','\n')
      }
    }
    write.table(echotypes,
                paste(path.export,cruise,'_Echotypes4Echobase.csv',sep=''),
                sep=';',row.names=FALSE)
    cat('-> Echotype data exported','\n')
  }else{
    echotypes=NA
  } 
  
  #********************************************************* 
  #11. Export results -------------------
  #********************************************************* ------------
  #11.1. Results per echotype and esdu: --------------
  # scrutinising and reference hauls results
  # ************************ ------------

  if (class(chamens.long)!='NULL'|class(EspDev)!='NULL'|
      class(ESDUDEVs)!='NULL'){
    if (class(chamens.long)!='NULL'){
      if (class(chamens.long)=='character'){
        chamens.long=read.table(chamens.long,header=TRUE,sep=';')
      }
      head(chamens.long)
      names(chamens.long)[names(chamens.long)=='NOSTA.mens']='ReferenceStationBiometry'
      if (fMovies=='Mplus2012'){
        if (milli.correct){
          chamens.long$dateendeb=format(strptime(chamens.long$TC,"%Y/%d/%m %H:%M:%OS"),
                                        "%Y-%m-%d %H:%M:%S")
          chamens.long$dateendeb=paste(chamens.long$dateendeb,".0000",sep='')
        }else{
          chamens.long$dateendeb=format(strptime(chamens.long$TC,"%Y/%d/%m %H:%M:%OS"),
                                        "%Y-%m-%d %H:%M:%OS4")
        }
      }else if (fMovies=='M3D'){
        if (milli.correct){
          chamens.long$dateendeb=format(strptime(chamens.long$TC,"%Y/%m/%d %H:%M:%OS"),
                                        "%Y-%m-%d %H:%M:%S")
          chamens.long$dateendeb=paste(chamens.long$dateendeb,".0000",sep='')
        }else{
          chamens.long$dateendeb=format(strptime(chamens.long$TC,"%Y/%m/%d %H:%M:%OS"),
                                        "%Y-%m-%d %H:%M:%OS4")
        }
      }else if (fMovies=='Mplus2014'){
        if (milli.correct){
          chamens.long$dateendeb=format(strptime(chamens.long$TC,"%Y/%m/%d %H:%M:%OS"),
                                        "%Y-%m-%d %H:%M:%S")
          chamens.long$dateendeb=paste(chamens.long$dateendeb,".0000",sep='')
        }else{
          chamens.long$dateendeb=format(strptime(chamens.long$TC,"%Y/%m/%d %H:%M:%OS"),
                                        "%Y-%m-%d %H:%M:%OS4")
        }
      }        
      head(chamens.long)
      #names(chamens.long)[names(chamens.long)=='dateendeb']='TC'
    }
    if ((class(ESDUDEVs)!='NULL')&(exists("Pechei0"))){
      if (class(ESDUDEVs)=='character'){
        ESDUDEVs0=read.table(ESDUDEVs,header=TRUE,sep=';')
      }else{
        ESDUDEVs0=ESDUDEVs
      }  
      head(ESDUDEVs0)
      dim(ESDUDEVs0)
      if (fMovies=='Mplus2012'){
        if (milli.correct){
          ESDUDEVs0$dateendeb=format(strptime(ESDUDEVs0$TC,"%Y/%d/%m %H:%M:%OS"),
                                     "%Y-%m-%d %H:%M:%S")
          ESDUDEVs0$dateendeb=paste(ESDUDEVs0$dateendeb,".0000",sep='')
        }else{
          ESDUDEVs0$dateendeb=format(strptime(ESDUDEVs0$TC,"%Y/%d/%m %H:%M:%OS"),
                                     "%Y-%m-%d %H:%M:%OS4")
        }
      }else if (fMovies=='M3D'){
        if (milli.correct){
          ESDUDEVs0$dateendeb=format(strptime(ESDUDEVs0$TC,"%Y/%m/%d %H:%M:%OS"),
                                     "%Y-%m-%d %H:%M:%S")
          ESDUDEVs0$dateendeb=paste(ESDUDEVs0$dateendeb,".0000",sep='')
        }else{
          ESDUDEVs0$dateendeb=format(strptime(ESDUDEVs0$TC,"%Y/%m/%d %H:%M:%OS"),
                                     "%Y-%m-%d %H:%M:%OS4")
        }
      }else if (fMovies=='Mplus2014'){
        if (milli.correct){
          ESDUDEVs0$dateendeb=format(strptime(ESDUDEVs0$TC,"%Y/%m/%d %H:%M:%OS"),
                                     "%Y-%m-%d %H:%M:%S")
          ESDUDEVs0$dateendeb=paste(ESDUDEVs0$dateendeb,".0000",sep='')
        }else{
          ESDUDEVs0$dateendeb=format(strptime(ESDUDEVs0$TC,"%Y/%m/%d %H:%M:%OS"),
                                     "%Y-%m-%d %H:%M:%OS4")
        }
      }
      #11.1.1. NASC and ref.  hauls per echotypes -----------
      # ************************
      ldev=paste('D',Ndevi,sep='')
      ldevs=ldev[ldev%in%names(ESDUDEVs0)]
      lESDUDEVs.DEVs=reshape(ESDUDEVs0[,names(ESDUDEVs0)%in%
                                         c('dateendeb',ldevs)],
                             idvar="dateendeb", 
        varying=list(ldevs),v.names="NASC",timevar="echotype",
        direction='long',times=ldevs)
      head(lESDUDEVs.DEVs)
      lESDUDEVs.RefHauls=reshape(ESDUDEVs0[,names(ESDUDEVs0)%in%
                                             c('dateendeb',
                                               paste(ldevs,'.CREF',sep=''))],
                                 idvar="dateendeb", 
                                 varying=list(paste(ldevs,'.CREF',sep='')),
                                 v.names="NOCHAL",timevar="echotype",
                                 direction='long',
                                 times=paste(ldevs,sep=''))
      head(lESDUDEVs.RefHauls)
      dim(lESDUDEVs.DEVs);dim(lESDUDEVs.RefHauls)
      lESDUDEVs=merge(lESDUDEVs.DEVs,lESDUDEVs.RefHauls,
                      by.x=c('dateendeb','echotype'),
                      by.y=c('dateendeb','echotype'))
      lESDUDEVs=merge(lESDUDEVs,unique(Pechei0[,c('NOSTA','NOCHAL')]),
                      by.x='NOCHAL',by.y='NOCHAL')
      head(lESDUDEVs)
      dim(lESDUDEVs)
      names(lESDUDEVs)[names(lESDUDEVs)=='NOSTA']='ReferenceStationCatch'
      head(lESDUDEVs)
      #     lESDUDEVs=merge(lESDUDEVs,chamens.long[,c('dateendeb','echotype','ReferenceStationBiometry')],
      #                     by.x=c('dateendeb','echotype'),by.y=c('dateendeb','echotype'))
      dim(lESDUDEVs)
      head(lESDUDEVs)
      
      #11.1.3. Format results per echotype and EDSU file------ 
      # ************************
      #     resEsduEchotype=lESDUDEVs[,c('dateendeb','echotype','NASC',
      #                                             'ReferenceStationCatch','ReferenceStationBiometry')]
      resEsduEchotype=lESDUDEVs[,c('dateendeb','echotype','NASC',
                                   'ReferenceStationCatch')]
      #     if ((dim(lESDUDEVs)[1]!=dim(resEsduEchotype)[1])|(dim(resEsduEchotype)[1]!=dim(chamens.long)[1])){
      #       stop('Number of ESDUS are different between chamens and ESDUs')
      #     }
      names(resEsduEchotype)[1]='name'
      dim(resEsduEchotype)
      resEsduEchotype.wo0=resEsduEchotype[resEsduEchotype$NASC!=0,]
      dim(resEsduEchotype.wo0)
      length(unique(resEsduEchotype$name))
      #Adds dataquality flag 
      resEsduEchotype$dataQuality=1
      resEsduEchotype$voyage=cruise
      head(resEsduEchotype)
      head(ESDUDEVs0)
      resEsduEchotype2=merge(resEsduEchotype,
                             unique(ESDUDEVs0[,c('dateendeb','LAT','LONG')]),
                             by.x='name',by.y='dateendeb')
      dim(resEsduEchotype);dim(resEsduEchotype2)
      
      #11.1.4. Check that all results are associated with an existing esdu --------
      # ************************
      names(Mcsv.bases.comps2)
      esdus=Mcsv.bases.comps2[Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel&
                                Mcsv.bases.comps2['MOVIES_EILayer\\cellset\\celltype']==4,
                              'MOVIES_EILayer\\cellset\\dateend']
      esdus=format(strptime(esdus,"%Y-%m-%d %H:%M:%OS"),
                   "%Y-%m-%d %H:%M:%OS4")
      resdus=unique(resEsduEchotype$name)
      length(esdus)
      length(resdus)
      # ecomp=cbind(sort(esdus),sort(resdus))
      # head(ecomp)
      cat('No. of ESDUs in results per ESDU file:',length(unique(resEsduEchotype$name)),'\n')
      cat('No. of ESDUs in EIlay file:',length(esdus),'\n')
      mresdus=F%in%(resdus%in%esdus)
      if (mresdus){
        cat('ESDUs in result per ESDU files missing in lay files','\n')
      }else{
        cat('All ESDUs in result per ESDU files are in lay files','\n')
      }
      #result esdus not in esdus 
      mesdus=unique(resdus)[!unique(resdus)%in%unique(esdus)]
      length(mesdus)
      mresEsduEchotype=resEsduEchotype2[resEsduEchotype2$name%in%mesdus,]
      length(unique(mresEsduEchotype$name))
      head(mresEsduEchotype)
      dim(mresEsduEchotype)
      #     resEsduEchotype[resEsduEchotype$name=='2012-05-18 20:08:42.0000',]
      #     c4=Mcsv.bases.comp[Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\celltype']==4,]
      #     c4[c4[,'MOVIES_EILayer\\cellset\\datestart']=='2012-05-18 20:08:42.0000',]
      
      if (dim(mresEsduEchotype)[1]>0){ # if results without EDSUS
        #******************************
        # 11.1.4.1. Eventually, create missing lay file based on man file --------
        #******************************      
        #names(Mcsv.bases.comps2)
        mantots.mesdu=mantotc[paste(mantotc$dateend,'.0000',sep='')%in%mresEsduEchotype$name,]
        if (length(unique(mantotc$softChannelId))>1|unique(mantotc$softChannelId)!=cchannel){
          stop('Non unique frequency in scrutinising data, please check','\n')
        }
        mantots.mesdu$name=paste(mantots.mesdu$dateend,'.0000',sep='')
        par(mfrow=c(1,1))
        plot(Mcsv.bases.comps2[Mcsv.bases.comps2[,"MOVIES_EILayer\\sndset\\softChannelId"]==cchannel&
                                 Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\celltype"]==4,
                               'MOVIES_EILayer\\cellset\\long'],
             Mcsv.bases.comps2[Mcsv.bases.comps2[,"MOVIES_EILayer\\sndset\\softChannelId"]==cchannel&
                                 Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\celltype"]==4,
                               'MOVIES_EILayer\\cellset\\lat'],pch=1,main=cruise,xlab='',ylab='')
        points(mresEsduEchotype$LONG,mresEsduEchotype$LAT,pch=16,col=2)
        legend('bottomleft',legend=c('ESDUs','ESDUs in results and not in EI files'),pch=c(1,16),col=seq(2))
        #head(mantots.mesdu)
        
        if (lay.format=='csv'){
          if (class(mantot)!='NULL'){
            # Create missing lay files based on man file
            for (i in 1:length(mesdus)){
              mantots.mesdui=mantots.mesdu[mantots.mesdu$name==mesdus[i],]
              unique(mantots.mesdui$channelName)
              lay.mesdui=man2lay(manil=mantots.mesdui,MEIl=unique(Mcsv.bases.comps2$MOVIES_EILayer))
              if (i==1){
                lay.mesdu=lay.mesdui
              }else{
                lay.mesdu=rbind(lay.mesdu,lay.mesdui)
              }
            }
            # convert lay.mesdu to EB format 
            lay.mesdu=convert2EBlay(Mcsv.base=lay.mesdu,validChannels=validChannels,
                                    neg.nasc.remove=neg.nasc.remove,CorrectChannelIdent=CorrectChannelIdent,
                                    CorrectAbsorption=CorrectAbsorption,beamGainCor=beamGainCor,
                                    beamSACor=beamSACor,powerCor=powerCor,algAsensCor=algAsensCor,
                                    atwAsensCor=atwAsensCor,CorrectSoundCelerity=CorrectSoundCelerity,
                                    areaCor=areaCor,volCor=volCor,milli.correct=milli.correct,
                                    fMovies=fMovies,Transectfs=Transectfs,mantot=mantot)
            
            # add new EDSU to lay file
            # ************************
            names(Mcsv.bases.comps2)
            names(lay.mesdu)
            unique(lay.mesdu[,'MOVIES_EILayer\\cellset\\dateend'])
            unique(lay.mesdu[,'MOVIES_EILayer\\sndset\\softChannelId'])
            dim(Mcsv.bases.comps2)
            Mcsv.bases.comps2=rbind(Mcsv.bases.comps2,lay.mesdu[,names(Mcsv.bases.comps2)])
            dim(Mcsv.bases.comps2)
            
            # check that all results have an esdu
            # ************************
            esdus2=Mcsv.bases.comps2[Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel&
                                       Mcsv.bases.comps2['MOVIES_EILayer\\cellset\\celltype']==4,
                                     'MOVIES_EILayer\\cellset\\dateend']
            esdus2=format(strptime(esdus2,"%Y-%m-%d %H:%M:%OS"),
                          "%Y-%m-%d %H:%M:%OS4")
            resdus=unique(resEsduEchotype$name)
            cat('No. of ESDUs in results per ESDU file:',length(unique(resEsduEchotype$name)),'\n')
            cat('No. of ESDUs in EIlay file:',length(esdus2),'\n')
            mresdus2=sum(!resdus%in%esdus2)
            resdus[!resdus%in%esdus2]
            if (mresdus2>0){
              cat('ESDUs in result per ESDU files missing in lay files','\n')
            }else{
              cat('All ESDUs in result per ESDU files are in lay files','\n')
            }
            #11.1.4.2. Export new EI lay file------
            # ************************
            write.table(Mcsv.bases.comps2,
                        paste(path.export,cruise,'_lay4Echobase.csv',sep=''),sep=';',
                        row.names=FALSE)   
            cat('-> Corrected EIlayer data exported','\n')
            # Export only 38 kHz
            Mcsv.bases.comps2.38=Mcsv.bases.comps2[
              Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel,]
            # correct sounder parameters
            Mcsv.bases.comps2.38=SounderParametersCorrection(Mcsv.bases.compi=Mcsv.bases.comps2.38,
                                                             CorrectChannelIdent=CorrectChannelIdent,
                                                             CorrectAbsorption=CorrectAbsorption,beamGainCor=beamGainCor,
                                                             beamSACor=beamSACor,powerCor=powerCor,algAsensCor=algAsensCor,
                                                             atwAsensCor=atwAsensCor,CorrectSoundCelerity=CorrectSoundCelerity,
                                                             areaCor=areaCor,volCor=volCor)
            length(unique(Mcsv.bases.comps2.38[,'MOVIES_EILayer\\cellset\\dateend']))
            head(Mcsv.bases.comps2.38)
            #Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\dateend'][table(Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\dateend'])==0]
            write.table(Mcsv.bases.comps2.38,
                        paste(path.export,cruise,'_lay384Echobase.csv',sep=''),sep=';',
                        row.names=FALSE)
            cat('-> Corrected 38kHz EIlayer data exported','\n')
          }else{
            stop('Please provide check .lay files to complete the ESDUs list')       
          }
        }else{
          stop('Please provide check .lay files to complete the ESDUs list')
        }
      }else{
        cat('All results per echotype ESDUs found in EI layer ESDUs','\n')
      }
      
      # 11.1.5. Check that results per echotype are unique 
      # ************************
      head(resEsduEchotype)
      id=paste(resEsduEchotype$name,resEsduEchotype$echotype)
      dids=id[duplicated(id)]
      if (length(dids)>0) stop('Non unique results per esdu/echotype')
      
      # 11.1.6. Add cell num to cell name for compliance with EB 4.0 
      # ************************
      resEsduEchotype$name=paste(resEsduEchotype$name,16,sep='_')
      resEsduEchotype=resEsduEchotype[,c('voyage','name','echotype','NASC','ReferenceStationCatch','dataQuality')]
      
      # Export sA and hauls per esdu and echotype 
      # ************************
      head(resEsduEchotype)
      dim(resEsduEchotype)
      write.table(resEsduEchotype,paste(path.export,cruise,'_resEsduEchotype4Echobase.csv',
                                        sep=''),sep=';',row.names=FALSE)
      cat('-> Results per ESDU and echotype exported','\n')
    }else{
      resEsduEchotype=NA
    } 
  }
  # ************************
  # 11.2. Results per echotype, species category and esdu -------
  # ************************ ------------
  #Export the infamous "biomres": results per echotype, species category and esdu
  # ************************                                        
  if (class(biomres)!='NULL'&class(biomres.pond)!='NULL'){
    
    if (class(biomres)=='character'){
      biomres0=read.table(biomres,header=TRUE,sep=';')
    }else{
      biomres0=biomres
    }
    if (class(biomres.pond)=='character'){
      biomres.pond0=read.table(biomres.pond,header=TRUE,sep=';')
    }else{
      biomres.pond0=biomres.pond
    }
    head(biomres0)
    head(biomres.pond0)
    if (sum(is.na(as.numeric(substr(biomres0$DHTU_ESDU,1,4))))>0){
      biomres0$dateendeb=format(strptime(biomres0$DHTU_ESDU,"%d/%m/%Y %H:%M:%S",tz='GMT'),
                                "%Y-%m-%d %H:%M:%S")
    }else{
      biomres0$dateendeb=format(strptime(biomres0$DHTU_ESDU,"%Y/%m/%d %H:%M:%S",tz='GMT'),
                                "%Y-%m-%d %H:%M:%S")
    }
    biomres0$dateendeb=paste(biomres0$dateendeb,".0000",sep='')
    biomres.pond0$dateendeb=format(strptime(biomres.pond0$DHTU_ESDU,"%Y/%m/%d %H:%M:%OS",
                                            tz='GMT'),"%Y-%m-%d %H:%M:%S")
    biomres.pond0$dateendeb=paste(biomres.pond0$dateendeb,".0000",sep='')
    dim(biomres0)
    head(biomres0)
    resEsduEchotypeSp=merge(biomres0[,names(biomres0)%in%
                                       c('dateendeb','LIBELLE_DEVIATION',
                                       'GENRE_ESP','CATEGORIE',
                                       'CAMPAGNE','STATION',
                                       'Biomasse_W.tonnes.','Taille_Moy',
                                       'Poids_moy','Biomasse_NB.Milliers.',
                                       'PrAcoust','Energie_esp.sA.')],
                            biomres.pond0[,names(biomres.pond0)%in%
                                            c('dateendeb','LIBELLE_DEVIATION',
                                              'GENRE_ESP','CATEGORIE',
                                              'Biomasse_Wpond','Biomasse_Npond')],
                            by.x=c('dateendeb','LIBELLE_DEVIATION',
                                   'GENRE_ESP','CATEGORIE'),
                            by.y=c('dateendeb','LIBELLE_DEVIATION',
                                   'GENRE_ESP','CATEGORIE'))
    dim(resEsduEchotypeSp)
    head(resEsduEchotypeSp)
    names(resEsduEchotypeSp)=c('name','echotype','baracoudaCode',
                               'sizeCategory','voyage','ReferenceStationCatch',
                               'Biomass','MeanLength','MeanWeight','Abundance',
                               'SigmaSp','NASC','pondBiomass',
                               'pondAbundance')
    #     if (milli.correct){
    #       resEsduEchotypeSp$name=format(strptime(resEsduEchotypeSp$name,"%Y/%d/%m %H:%M:%OS"),
    #                           "%Y-%m-%d %H:%M:%S")
    #       resEsduEchotypeSp$name=paste(resEsduEchotypeSp$name,".0000",sep='')
    #     }else{
    #       resEsduEchotypeSp$name=format(strptime(resEsduEchotypeSp$name,"%Y/%d/%m %H:%M:%OS"),
    #                                     "%Y-%m-%d %H:%M:%OS4")
    #     }
    resEsduEchotypeSp$dataQuality=1
    head(resEsduEchotypeSp)
    names(resEsduEchotypeSp)
    
    #Check that all results are associated with an existing esdu 
    # ************************
    NASCesdus=Mcsv.bases.comps2[Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\celltype']==4,
                                'MOVIES_EILayer\\cellset\\dateend']
    NASCesdus=format(strptime(NASCesdus,"%Y-%m-%d %H:%M:%OS"),
                     "%Y-%m-%d %H:%M:%OS4")
    (NesduRes=length(unique(resEsduEchotypeSp$name)))
    (NesduNASC=length(NASCesdus))
    (NesduResNotInNASC=sum(resEsduEchotypeSp$name%in%NASCesdus)-
      length(resEsduEchotypeSp$name))
    if (NesduResNotInNASC>0) {
      stop('ESDUs in results per specie/echotype not in EI ESDUs, please check')
    }else{
      cat('All results per species/echotype ESDUs are associated to EI layer ESDUs','\n')
    }  
    (NesduNASCNotInRes=sum(NASCesdus%in%resEsduEchotypeSp$name)-
      NesduNASC)
    resEsduEchotypeSp[!resEsduEchotypeSp$name%in%NASCesdus,]
    
    # check that results per echotype and sp are unique
    # ************************
    head(resEsduEchotypeSp)
    id=paste(resEsduEchotypeSp$name,resEsduEchotypeSp$echotype,resEsduEchotypeSp$baracoudaCode,
             resEsduEchotypeSp$sizeCategory)
    dids=id[duplicated(id)]
    if (length(dids)>0) stop('Non unique results per esdu/echotype/species')
    
    # Add cell num to cell name for compliance with EB 4.0
    # ************************
    resEsduEchotypeSp$name=paste(resEsduEchotypeSp$name,16,sep='_')
    
    #Export results per species, esdu and echotype
    # ************************
    write.table(resEsduEchotypeSp,
                paste(path.export,cruise,'_resEsduEchotypeSp4Echobase.csv',
                      sep=''),sep=';',row.names=FALSE)
    cat('-> Results per ESDU and species exported','\n')
  }else{
    resEsduEchotypeSp=NA
  }
  
  #11.3. Results-at-length per echotype, species category and esdu ----------
  # ************************  --------------
  if (class(Biomres.echotype.codesp.size)!='NULL'){
    if (class(Biomres.echotype.codesp.size)=='character'){
      Biomres.sp.size0=read.table(Biomres.echotype.codesp.size,header=TRUE,sep=';')
    }else{
      Biomres.sp.size0=Biomres.echotype.codesp.size
    }
    #If possible, add biometry reference hauls...
    #Add date/time in Echobase format
    Biomres.sp.size0$dateendeb=format(strptime(Biomres.sp.size0$TC,
                                               "%Y/%m/%d %H:%M:%S",tz='GMT'),"%Y-%m-%d %H:%M:%S")
    Biomres.sp.size0$dateendeb=paste(Biomres.sp.size0$dateendeb,".0000",sep='')
    # ************************
    #Export results per esdu, echotype, species and size class
    # ************************                                        
    head(Biomres.sp.size0)
    resEsduSpSize=Biomres.sp.size0[,c('dateendeb','DEV','sp','sizeCat','L','W','N')]
    names(resEsduSpSize)=c('name','echotype','baracoudaCode','sizeCategory',
                           'lengthClass','Biomass','Abundance')
    resEsduSpSize$lengthClassMeaning='mid point of 1 cm size bin'
    #     if (milli.correct){
    #       resEsduSpSize$name=format(strptime(resEsduSpSize$name,"%Y/%d/%m %H:%M:%OS"),
    #                                     "%Y-%m-%d %H:%M:%S")
    #       resEsduSpSize$name=paste(resEsduSpSize$name,".0000",sep='')
    #     }else{
    #       resEsduSpSize$name=format(strptime(resEsduSpSize$name,"%Y/%d/%m %H:%M:%OS"),
    #                                 "%Y-%m-%d %H:%M:%OS4")
    #     }
    resEsduSpSize$dataQuality=1
    resEsduSpSize$voyage=cruise
    head(resEsduSpSize)
    dim(resEsduSpSize)
    
    # Add cell num to cell name for compliance with EB 4.0
    # ************************
    resEsduSpSize$name=paste(resEsduSpSize$name,16,sep='_')
    
    #Check that all results are associated with an existing esdu
    # ************************
    NASCesdus=Mcsv.bases.comps2[Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\celltype']==4,
                                'MOVIES_EILayer\\cellset\\dateend']
    NASCesdus=format(strptime(NASCesdus,"%Y-%m-%d %H:%M:%OS"),
                     "%Y-%m-%d %H:%M:%OS4")
    (NesduRes=length(unique(resEsduSpSize$name)))
    (NesduNASC=length(NASCesdus))
    (NesduResNotInNASC=sum(resEsduSpSize$name%in%NASCesdus)-
      length(resEsduSpSize$name))
    if (NesduResNotInNASC>0){
      stop('ESDUs in results per specie/size not in EI ESDUs, please check')
    }else{
      cat('All results per specie/size ESDUs are associated to EI layer ESDUs','\n')
    }  
    resEsduSpSize[!resEsduSpSize$name%in%NASCesdus,]
    
    # check that results per esdu and sp and size are unique
    # ************************
    dim(resEsduSpSize)
    id=paste(resEsduSpSize$name,resEsduSpSize$echotype,resEsduSpSize$baracoudaCode,resEsduSpSize$sizeCategory,
             resEsduSpSize$lengthClass)
    dids=id[duplicated(id)]
    if (length(dids)>0) stop('Non unique results per esdu/species/size')
    
    #Export results per species, esdu and size class
    # ************************
    write.table(resEsduSpSize,paste(path.export,cruise,'_resEsduEchotypeSpSize4Echobase.csv',
                                    sep=''),sep=';',row.names=FALSE) 
    cat('-> Results per ESDU and size exported','\n')
  }
  
  # 11.3. Results-at-age per ESDU, species ------
  # ************************ --------------
  if (class(Biomres.sp.age)!='NULL'){
    if (class(Biomres.sp.age)=='character'){
      Biomres.sp.age0=read.table(Biomres.sp.age,header=TRUE,sep=';')
    }else{
      Biomres.sp.age0=Biomres.sp.age
    }
    #Add date/time in Echobase format
    Biomres.sp.age0$dateendeb=format(strptime(Biomres.sp.age0$TC,
                                              "%Y/%m/%d %H:%M:%S",tz='GMT'),"%Y-%m-%d %H:%M:%S")
    Biomres.sp.age0$dateendeb=paste(Biomres.sp.age0$dateendeb,".0000",sep='')
    # ************************
    #Export results per esdu, species and age class
    # ************************                                       
    head(Biomres.sp.age0)
    resEsduSpAge=Biomres.sp.age0[,c('dateendeb','sp','Age','N')]
    names(resEsduSpAge)=c('name','baracoudaCode','ageCategory',
                          'Abundance')
    resEsduSpAge$ageCategoryMeaning='fish age-group (years)'
    #     if (milli.correct){
    #       resEsduSpAge$name=format(strptime(resEsduSpAge$name,"%Y/%d/%m %H:%M:%OS"),
    #                                 "%Y-%m-%d %H:%M:%S")
    #       resEsduSpAge$name=paste(resEsduSpAge$name,".0000",sep='')
    #     }else{
    #       resEsduSpAge$name=format(strptime(resEsduSpAge$name,"%Y/%d/%m %H:%M:%OS"),
    #                                "%Y-%m-%d %H:%M:%OS4")
    #     }  
    resEsduSpAge$dataQuality=1
    resEsduSpAge$voyage=cruise
    head(resEsduSpAge)
    names(resEsduSpAge)
    dim(resEsduSpAge)
    
    #Check that all results are associated with an existing esdu
    # ************************
    NASCesdus=Mcsv.bases.comps2[Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\celltype']==4,
                                'MOVIES_EILayer\\cellset\\dateend']
    NASCesdus=format(strptime(NASCesdus,"%Y-%m-%d %H:%M:%OS"),
                     "%Y-%m-%d %H:%M:%OS4")
    (NesduRes=length(unique(resEsduSpAge$name)))
    (NesduNASC=length(NASCesdus))
    (NesduResNotInNASC=sum(resEsduSpSize$name%in%NASCesdus)-
      length(resEsduSpSize$name))
    if (NesduResNotInNASC>0){
      stop('ESDUs in results per specie/echotype not in EI ESDUs, please check')
    }else{
      cat('All results per specie/age ESDUs are associated to EI layer ESDUs','\n')
    }  
    (NesduNASCNotInRes=sum(NASCesdus%in%resEsduSpSize$name)-
      NesduNASC)
    resEsduSpAge[!resEsduSpAge$name%in%NASCesdus,]
    
    # check that results per esdu and sp and age are unique
    # ************************
    head(resEsduSpAge)
    dim(resEsduSpAge)
    id=paste(resEsduSpAge$name,resEsduSpAge$baracoudaCode,resEsduSpAge$ageCategory)
    dids=id[duplicated(id)]
    if (length(dids)>0) stop('Non unique results per esdu/species/size')
    
    # Add cell num to cell name for compliance with EB 4.0
    # ************************
    resEsduSpAge$name=paste(resEsduSpAge$name,16,sep='_')
    
    #Export results per species, esdu and age class
    # ************************
    write.table(resEsduSpAge,paste(path.export,cruise,'_resEsduSpAge4Echobase.csv',
                                   sep=''),sep=';',row.names=FALSE)
    cat('-> Results per ESDU and age exported','\n')
  }else{
    resEsduSpAge=NA
  }
  
  # 11.4. Results per regions ------------
  # ************************ --------------
  # 11.4.1. Export regions ----------------
  # ************************
  
  if (class(survey.polys)!='NULL'&class(CV.df)!='NULL'&class(biomres.pond)!='NULL'&
      class(Ind.sp.all)!='NULL'){
    
    if (class(survey.polys)=='character'){
      survey.polys0=read.table(survey.polys,header=TRUE,sep=';')
    }else{
      survey.polys0=survey.polys
    }
    head(survey.polys0)
    unique(survey.polys0$region)
    poly.dfs=survey.polys0[,c('region','x','y','CAMPAGNE','TYPE','AREA.pbs')]
    poly.dfs$z=30
    names(poly.dfs)=c('name','LongitudeEnv','LatitudeEnv','voyage',
                      'depthStratum','surface','Depth')
    # define surface and bottom celltypes
    poly.dfs$cellType=paste("Region",poly.dfs$depthStratum,sep='')
    # define survey polygon celltype='Region'
    poly.dfs[poly.dfs$name==0,'cellType']='Region'
    poly.dfs[poly.dfs$depthStratum=='CLAS','Depth']=70
    poly.dfs[poly.dfs$cellType=='Region','Depth']=50
    poly.dfs$regionEnvCoordinates=paste(poly.dfs$LongitudeEnv,
                                        poly.dfs$LatitudeEnv,poly.dfs$Depth)
    regions=poly.dfs[,c('voyage','name','cellType',
                        'regionEnvCoordinates',
                        'surface')]
    regions$dataQuality=1
    head(regions)
    
    #Export regions 
    # ************************
    write.table(regions,paste(path.export,cruise,'_regions4Echobase.csv',
                              sep=''),sep=';',row.names=FALSE)
    cat('-> Regions exported','\n')
    
    # ************************
    #11.4.2. Export results per regions ---------
    # ************************
    if (class(CV.df)=='character'){
      CV.df0=read.table(CV.df,header=TRUE,sep=';')
    }else{
      CV.df0=CV.df
    }
    if (class(Ind.sp.all)=='character'){
      Ind.sp.all0=read.table(Ind.sp.all,header=TRUE,sep=';')
    }else{
      Ind.sp.all0=Ind.sp.all
    }
    head(CV.df0)
    names(CV.df0)
    
    #Export results per post-stratification regions
    # ************************
    
    CV.dfs=CV.df0[,c('CAMPAGNE','region','AREA','CodEsp','sp','DEV','varXe',
                     'varsA','mXe','msA','mwXe','NsA','Nhauls',
                     'wXes','mwB','ew','wCV','mB','ea','CV',
                     'sigmaE2tot','wsigmaE2tot','prop.vara',
                     'prop.varw')]
    CV.dfs$biom=CV.dfs$mB*CV.dfs$AREA
    CV.dfs$wbiom=CV.dfs$mwB*CV.dfs$AREA
    CV.dfs$sizeCategory=substr(CV.dfs$CodEsp,15,15)  
    
    resPostRegions=CV.dfs[,c('CAMPAGNE','region','sp','sizeCategory',
                             'DEV','varXe',
                             'varsA','mXe','msA','mwXe','NsA','Nhauls',
                             'mwB','ew','wCV','mB','ea','CV',
                             'sigmaE2tot','wsigmaE2tot','prop.vara',
                             'prop.varw','biom','wbiom')]
    names(resPostRegions)=c('voyage','name','baracoudaCode',
                            'sizeCategory','echotype','VarianceXe',
                            'VarianceNASC','MeanXe','MeanNASC',
                            'NASCWeightedMeanXe','Nesdu','Nhauls',
                            'MeanNASCWeightedBiomassDensity',
                            'NASCWeightedEstimationVariance',
                            'NASCWeightedEstimationCV',
                            'MeanBiomassDensity',
                            'EstimationVariance',
                            'EstimationCV',
                            'TotalEstimationVariance',
                            'TotalNASCWeightedEstimationVariance',
                            'ProportionOfTotalEstimationVariance',
                            'ProportionOfNASCWeightedTotalEstimationVariance',
                            'Biomass','NASCWeightedBiomass')
    resPostRegions$dataQuality=1
    head(resPostRegions)
    npsr=names(resPostRegions)
    unique(resPostRegions$echotype)
    unique(resPostRegions$name)
    # shorten region names only if regions names numeric in regions df  
    if (max(nchar(regions$name))<3){
      resPostRegions$name=substr(resPostRegions$name,6,7)
    }
    head(resPostRegions)
    unique(resPostRegions$name)
    
    #check total biomass
    aggregate(CV.dfs[,c('biom','wbiom')],list(CV.dfs$sp),sum,na.rm=TRUE)
    
    #Results for the entire survey polygon
    # ************************
    unique(Ind.sp.all0$sp)
    head(Ind.sp.all0)
    names(Ind.sp.all0)
    Ind.sp.all0$CAMPAGNE=cruise
    Ind.sp.all0$region=0
    Ind.sp.all0$sizeCategory=0
    Ind.sp.all0$echotype="ALL"
    names(Ind.sp.all0)
    Ind.sp.all0$Btot_stdev=sqrt(Ind.sp.all0$varBtot)
    Ind.sp.all0$Ntot_stdev=sqrt(Ind.sp.all0$varNtot)
    Ind.sp.all0$Wbcomm_stdev=sqrt(Ind.sp.all0$varWbcomm)
    names(Ind.sp.all0)[names(Ind.sp.all0)=='delta1']='Delta1'
    Ind.sp.all0=Ind.sp.all0[-c(10,11,12),]
    
    resAllRegions=Ind.sp.all0[,c('CAMPAGNE','region','sp','sizeCategory',
                                 'echotype','mwB','wsigmaE2','wCV','mB','sigmaE2','CV',
                                 'biom','wbiom','abun','wabun','biomW.esdu','biomN.esdu',
                                 'N','N_stdev','W','W_stdev','logN','Wbar','Lbar','Lvar','Lo.0.05','Lo.0.25','Lo.0.75','Lo.0.95',
                                 'xcg','ycg','I','Imax',"Iso","xaxe1.1","xaxe1.2","yaxe1.1","yaxe1.2","xaxe2.1",
                                 "xaxe2.2","yaxe2.1","yaxe2.2","Npatch","PA","SA","EA","microS","Ntot","Ntot_stdev",
                                 "Btot","Btot_stdev","Gtot","Delta1","Wbcomm","Wbcomm_stdev")]
    onames=names(resAllRegions)
    nnames=c('voyage','name','baracoudaCode','sizeCategory','echotype',
             'MeanNASCWeightedBiomassDensity',
             'TotalNASCWeightedEstimationVariance',
             'NASCWeightedEstimationCV',
             'MeanBiomassDensity','TotalEstimationVariance',
             'EstimationCV','Biomass','NASCWeightedBiomass','Abundance','NASCWeightedAbundance',
             'EsduBasedBiomass','EsduBasedAbundance','N','N_stdev','W','W_stdev','lnN','Wbar','Lbar','Lvar','l0.05','l0.25',
             'l0.75','l0.95','xcg','ycg','I','Imax',"Iso","xaxe1.1","xaxe1.2","yaxe1.1","yaxe1.2","xaxe2.1",
             "xaxe2.2","yaxe2.1","yaxe2.2","Npatch","PA","SA","EA","microS","Ntot","Ntot_stdev",
             "Btot","Btot_stdev","Gtot","Delta1","Wbcomm","Wbcomm_stdev")
    names(resAllRegions)=nnames
    resAllRegions$dataQuality=1
    cbind(onames,nnames)    
    
    # common part
    dfc1=resPostRegions[,npsr[npsr%in%nnames]]
    dfc2=resAllRegions[,npsr[npsr%in%nnames]]    
    head(dfc1)
    head(dfc2)
    
    # adds post-stratification region results: owrk in progress...
    #     comi=data.frame(matrix(rep(NA,dim(comCindices)[2]*dim(Ind.sp.all)[1]),ncol=dim(comCindices)[2]))
    #     names(comi)=names(comCindices)
    #     comi=comi[,-1]
    #     spi=data.frame(matrix(rep(NA,dim(comCindices)[1]*dim(Ind.sp.all)[2]),ncol=dim(Ind.sp.all)[2]))
    #     names(spi)=names(Ind.sp.all)
    #     spi=spi[,-1]
    #     cspi=data.frame(sp=comCindices$Com,spi,comCindices[,-1])
    #     cspi$N=cspi$Ntot
    #     cspi$W=cspi$Btot
    #     Ind.sp.all=rbind(data.frame(Ind.sp.all,comi),cspi)
    
    head(resAllRegions)
    head(resPostRegions)
    unique(resAllRegions$name)
    unique(resAllRegions$baracoudaCode)
    names(resAllRegions)
    names(resPostRegions)
    #
    library(plyr)
    resALLRegions=rbind.fill(resPostRegions,resAllRegions)
    head(resALLRegions)
    unique(resALLRegions$baracoudaCode)
    resALLRegions=resALLRegions[resALLRegions$baracoudaCode!='nocl',]
    unique(resALLRegions$name)
    
    #Export results per regions
    # ************************
    write.table(resALLRegions,
                paste(path.export,cruise,'_resRegions4Echobase.csv',
                      sep=''),sep=';',row.names=FALSE)
    cat('-> Results per regions exported','\n')
    
    # ************************
    #11.4.3. Export esdu belongings to regions----------
    # ************************
    if (class(biomres.pond)=='character'){
      biomres.pond0=read.table(biomres.pond,header=TRUE,sep=';')
    }else{
      biomres.pond0=biomres.pond
    }
    if (class(biomres.pond)=='character'){
      deszones0=read.table(deszones,header=TRUE,sep=';')
    }else{
      deszones0=deszones
    }
    head(biomres.pond0)
    esduRegions=biomres.pond0[,c('CAMPAGNE','DHTU_ESDU','POST_STRATE_SCENARIO')]
    esduRegions=merge(esduRegions,deszones0,by.x='POST_STRATE_SCENARIO',
                      by.y='ID')
    names(esduRegions)[1:3]=c('regionName','voyage','esduName')
    if (milli.correct){
      if (fMovies=='Mplus2012'){
        esduRegions$esduName=format(strptime(esduRegions$esduName,"%Y/%d/%m %H:%M:%OS"),
                                    "%Y-%m-%d %H:%M:%S")
        esduRegions$esduName=paste(esduRegions$esduName,".0000",sep='')
      }else if (fMovies=='M3D'){
        esduRegions$esduName=format(strptime(esduRegions$esduName,"%Y/%m/%d %H:%M:%OS"),
                                    "%Y-%m-%d %H:%M:%S")
        esduRegions$esduName=paste(esduRegions$esduName,".0000",sep='')
      }
    }else{
      if (fMovies=='Mplus2012'){
        esduRegions$esduName=format(strptime(esduRegions$esduName,"%Y/%d/%m %H:%M:%OS4"),
                                    "%Y-%m-%d %H:%M:%S")
      }else if (fMovies=='M3D'){
        esduRegions$esduName=format(strptime(esduRegions$esduName,"%Y/%m/%d %H:%M:%OS4"),
                                    "%Y-%m-%d %H:%M:%S")
      }  
    }
    esduRegions=unique(esduRegions[,c('voyage','regionName','esduName')])
    #adds belongings to survey polygon
    esduRegions0=data.frame(voyage=cruise,regionName=0,esduName=unique(esduRegions$esduName))
    head(esduRegions)
    #esduRegions=esduRegions[!esduRegions$esduName%in%c('2012-05-21 05:17:39.6000'),]
    
    #change esdu datestart to dateend
    # ************************
    names(Mcsv.bases.comps2)
    dim(esduRegions)
    esduRegions1=merge(esduRegions,unique(Mcsv.bases.comps2[,c("MOVIES_EILayer\\cellset\\datestart",
                                                               "MOVIES_EILayer\\cellset\\dateend")]),
                       by.x='esduName',by.y="MOVIES_EILayer\\cellset\\datestart")
    dim(esduRegions1)
    esduRegions1$esduName=esduRegions1[,c("MOVIES_EILayer\\cellset\\dateend")]
    names(esduRegions1)
    esduRegions=esduRegions1[,1:3]
    esduRegions$esduName
    # !!! Shorten region names if region name numeric in regions df
    if (max(nchar(regions$name))<3){
      esduRegions$regionName=substr(esduRegions$regionName,6,7)
    }
    head(esduRegions)
    unique(esduRegions$regionName)
    
    #11.4.4. Check that all results are associated with an existing esdu--------
    # ************************
    NASCesdus=Mcsv.bases.comps2[Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\celltype']==4&
                                  Mcsv.bases.comps2[,'MOVIES_EILayer\\sndset\\softChannelId']==47,
                                'MOVIES_EILayer\\cellset\\dateend']
    NASCesdus=format(strptime(NASCesdus,"%Y-%m-%d %H:%M:%OS"),
                     "%Y-%m-%d %H:%M:%OS4")
    (NesduRes=length(unique(esduRegions$esduName)))
    (NesduNASC=length(NASCesdus))
    (NesduResNotInNASC=sum(esduRegions$esduName%in%NASCesdus)-
      length(esduRegions$esduName))
    if (NesduResNotInNASC>0) {
      stop('ESDUs in ESDUs belongings not in EI ESDUs, please check')
    }else{
      cat('All ESDUs in ESDUs belongings found in EI ESDUs','\n')
    }  
    (NesduNASCNotInRes=sum(NASCesdus%in%esduRegions$esduName)-
      NesduNASC)
    esduRegions[!esduRegions$esduName%in%NASCesdus,]
    
    # Add cell num to cell name for compliance with EB 4.0
    # ************************
    esduRegions$esduName=paste(esduRegions$esduName,16,sep='_')
    
    # Sort on region names
    esduRegions=esduRegions[order(esduRegions$regionName),]
    
    #Export esdu belongings to regions
    # ************************
    write.table(esduRegions,
                paste(path.export,cruise,'_esduRegions4Echobase.csv',
                      sep=''),sep=';',row.names=FALSE)
    cat('-> ESDU belongings to regions exported','\n')
  }
  
  # ************************
  # 11.5. Export maps -----
  # ************************ -----------
  # 11.5.1. Fish maps -----
  #*****************************
  if (class(path.fishmap)!='NULL'){
    if (class(path.fishmap)=='character'){
      #path.map="~/.gvfs/donnees2 sur nantes/Campagnes/PELGAS2011/EvaluationBiomasse/R/Rexports/gridMaps"
      lf1=list.files(path.fishmap,pattern='*.txt')
      lf2=data.frame(t(data.frame(strsplit(lf1,split='[.]'))))
      names(lf2)=c('name','ext')
      lf3=data.frame(t(data.frame(strsplit(as.character(lf2[,1]),split='_'))))
      names(lf3)=c('method','filter','thr','sp','var')
      lf=data.frame(path=lf1,lf2,lf3)  
      row.names(lf)=seq (dim(lf)[1])
      lsp=as.character(lf$sp)
      vari=as.character(unique(lf$var))
      #Capitalizing - toupper( every first letter of a word ) 
      vari=paste(toupper(substring(vari, 1, 1)), substring(vari, 2),
                 sep = "", collapse = " ")
      
      for (i in 1:length(lsp)){
        cat(lsp[i],'\n')
        mapi=read.table(paste(path.fishmap,as.character(lf$path[i]),sep=''),sep=';',header=TRUE)
        head(mapi)
        #grid.plot(pat=mapi,input='gridDataframe')
        plot(mapi$Xgd,mapi$Ygd,cex=mapi$Zvalue/11,main=as.character(lf$sp[i]),xlab='',ylab='')
        coast()
        mapi$sp=as.character(lf$sp[i])
        mapi$sizeCategory=0
        mapi$dpth=0
        mapi$dlag=0
        N=length(mapi$Xgd)
        mapi$xlag=abs(unique(min(mapi$Xgd[1:(N-1)]-mapi$Xgd[2:N])))
        mapi$ylag=abs(unique(min(mapi$Ygd[1:(N-1)]-mapi$Ygd[2:N])))
        mapi$Survey=cruise
        mapi$name=paste(mapi$Xgd,mapi$Ygd,mapi$dpth)
        mapi$ageCategory=''
        head(mapi)  
        mapecho=data.frame(mapi[,c('Survey','Xgd','Ygd','Zvalue','Zstdev','Nsample','xlag','ylag','sp','sizeCategory','dpth','dlag','name','ageCategory')])
        head(mapecho)
        names(mapecho)=c('voyage','gridCellLongitude','gridCellLatitude',
                         'meanMapcellBiomass','stdevMapcellBiomass','NsampleMapcell',
                         'gridLongitudeLag','gridLatitudeLag',
                         'baracoudaCode','sizeCategory',
                         'gridCellDepth','gridDepthLag','name',
                         'ageCategory')
        mapecho=mapecho[,c('voyage','name','baracoudaCode',
                           'sizeCategory','ageCategory',
                           'gridCellLongitude',
                           'gridCellLatitude','gridCellDepth',
                           'gridLongitudeLag','gridLatitudeLag',
                           'gridDepthLag','meanMapcellBiomass',
                           'stdevMapcellBiomass','NsampleMapcell')]
        
        
        mapecho$dataQuality=1
        head(mapecho)
        assign(paste(cruise,as.character(lf$sp[i]),vari,'map',sep='.'),mapecho)
        if (i==1){
          mapsecho=mapecho
        }else{
          mapsecho=rbind(mapsecho,mapecho)
        } 
      }
    }else if (class(path.fishmap)=='data.frame'){
      mapi=path.fishmap
      names(mapi)[names(mapi)=="X"]="Xgd"
      names(mapi)[names(mapi)=="Y"]="Ygd"
      names(mapi)[names(mapi)=="Zmean"]="Zvalue"
      names(mapi)[names(mapi)=="Nsamples"]="Nsample"
      names(mapi)[names(mapi)=="Parameter"]="sp"
      if ('sizeCategory'%in%names(mapi)){
        if (check.ref){
          library(RPostgreSQL)
          drv <- dbDriver("PostgreSQL")
          dbListConnections(drv)
          dbGetInfo(drv)
          con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                           password=password,port=EBport)
          sqlSizeCat="SELECT * FROM SizeCategory"
          sizeCats <- dbGetQuery(con,sqlSizeCat)
          dbDisconnect(con)
          head(sizeCats)
        }
        if (sum(!unique(mapi$sizeCategory)%in%sizeCats$name)>0){
          mscfm=unique(mapi$sizeCategory)[!unique(mapi$sizeCategory)%in%sizeCats$name]
          stop('Fish maps Size Category not found in Size Category ref table: ',
               paste(mscfm,collapse=' '))
        }
      }else{
        mapi$sizeCategory=0
      }
      if ('dpth'%in%names(mapi)){
        mapi$dpth=as.numeric(as.character(mapi$dpth))
        if (sum(is.na(mapi$dpth))>0){
          stop('NAs in fish maps depths, please check')
        }
      }else{
        mapi$dpth=0
      }  
      mapi$dlag=0
      N=length(mapi$Xgd)
      mapi$xlag=abs(unique(min(mapi$Xgd[1:(N-1)]-mapi$Xgd[2:N])))
      mapi$ylag=abs(unique(min(mapi$Ygd[1:(N-1)]-mapi$Ygd[2:N])))
      mapi$Survey=cruise
      mapi$name=paste(mapi$Xgd,mapi$Ygd,mapi$dpth)
      mapi$ageCategory=''
      head(mapi)  
      mapsecho=data.frame(mapi[,c('Survey','Xgd','Ygd','Zvalue','Zstdev','Nsample','xlag','ylag','sp','sizeCategory','dpth','dlag','name','ageCategory')])
      head(mapsecho)
      names(mapsecho)=c('voyage','gridCellLongitude','gridCellLatitude',
                       'meanMapcellBiomass','stdevMapcellBiomass','NsampleMapcell',
                       'gridLongitudeLag','gridLatitudeLag',
                       'baracoudaCode','sizeCategory',
                       'gridCellDepth','gridDepthLag','name',
                       'ageCategory')
      mapsecho=mapsecho[,c('voyage','name','baracoudaCode',
                         'sizeCategory','ageCategory',
                         'gridCellLongitude',
                         'gridCellLatitude','gridCellDepth',
                         'gridLongitudeLag','gridLatitudeLag',
                         'gridDepthLag','meanMapcellBiomass',
                         'stdevMapcellBiomass','NsampleMapcell')]
      mapsecho$dataQuality=1
      head(mapsecho)
    }
    #mapsecho=mapsecho[rev(order(mapsecho$name,mapsecho$baracoudaCode)),]
    #dev.off()
    head(mapsecho)
    # /!\ Sort map according to 'name'
    mapsecho=mapsecho[order(mapsecho$name),]
    
    #mapsechos=mapsecho[as.character(mapsecho$baracoudaCode)=='PELA-GIC',]
    #plot(mapsechos$gridCellLongitude,mapsechos$gridCellLatitude,cex=0.1+log(mapsechos$meanMapcellBiomass+1))
    
    #Export map file in EB format
    #*****************************
    write.table(mapsecho,
                paste(path.export,cruise,'_FishMap4Echobase.csv',
                      sep=''),sep=';',row.names=FALSE)
    cat('-> Fish maps exported','\n')
  } 
  # 11.5.2. Other maps -------
  #*****************************
  if (class(path.otherMaps)!='NULL'){
    #path.map="~/.gvfs/donnees2 sur nantes/Campagnes/PELGAS2011/EvaluationBiomasse/R/Rexports/gridMaps"
    if (class(path.otherMaps)=='data.frame'){
      mapi=path.otherMaps
      names(mapi)[names(mapi)=="X"]="Xgd"
      names(mapi)[names(mapi)=="Y"]="Ygd"
      names(mapi)[names(mapi)=="Zmean"]="Zvalue"
      names(mapi)[names(mapi)=="Nsamples"]="Nsample"
      names(mapi)
      mapi$Parameter=as.character(mapi$Parameter)
      mapiw=reshape(mapi[,c("Survey","Year","Xgd","Ygd","Zvalue","Parameter")],
                    v.names = "Zvalue", idvar = c("Survey","Year","Xgd","Ygd"),
                    timevar = "Parameter", direction = "wide")
      names(mapiw)=gsub('Zvalue.','',names(mapiw))
      mapiw$dpth=0
      mapiw$dlag=0
      N=length(mapiw$Xgd)
      mapiw$xlag=abs(unique(min(mapiw$Xgd[1:(N-1)]-mapiw$Xgd[2:N])))
      mapiw$ylag=abs(unique(min(mapiw$Ygd[1:(N-1)]-mapiw$Ygd[2:N])))
      mapiw$Survey=cruise
      mapiw$name=paste(mapiw$Xgd,mapiw$Ygd,mapiw$dpth)
      head(mapiw)
      lnbase=c('Survey','Xgd','Ygd','xlag','ylag','dpth','dlag','name')
      mapsechoo=mapiw[,c(lnbase,names(mapiw)[!names(mapiw)%in%c(lnbase,'Year')])]
      head(mapsechoo)
      names(mapsechoo)[1:8]=c('voyage','gridCellLongitude','gridCellLatitude',
                        'gridLongitudeLag','gridLatitudeLag',
                        'gridCellDepth','gridDepthLag','name')
      lnbase2=c('voyage','name','gridCellLongitude','gridCellLatitude','gridCellDepth',
                'gridLongitudeLag','gridLatitudeLag','gridDepthLag')
      mapsechoo=mapsechoo[,c('voyage','name','gridCellLongitude',
                           'gridCellLatitude','gridCellDepth',
                           'gridLongitudeLag','gridLatitudeLag',
                           'gridDepthLag',names(mapsechoo)[!names(mapsechoo)%in%lnbase2])]
      
      mapsechoo$dataQuality=1
      head(mapsechoo)
      names(mapsechoo)
    }else{
      # !!! create fake data !!!!
      mapsechoo=mapsecho[mapsecho$baracoudaCode=='PELA-GIC',]
      mapsechoo=mapsechoo[,!names(mapsechoo)%in%c('baracoudaCode','sizeCategory','ageCategory',
                                                  'meanMapcellBiomass','stdevMapcellBiomass','NsampleMapcell')]
      mapsechoo$SurfaceTemperature=rnorm(length(mapsechoo$name),12,1)
      mapsechoo$BottomTemperature=rnorm(length(mapsechoo$name),10,1)
      head(mapsechoo)
      mapsechoo=mapsechoo[order(mapsechoo$name),]
    }
    #mapsecho=mapsecho[rev(order(mapsecho$name,mapsecho$baracoudaCode)),]
    plot(mapsechoo$gridCellLongitude,mapsechoo$gridCellLatitude,cex=0.1+log(mapsechoo[,10]+1))
    #Export map file in EB format
    #*****************************
    write.table(mapsechoo,
                paste(path.export,cruise,'_OtherMap4Echobase.csv',
                      sep=''),sep=';',row.names=FALSE)
    cat('-> Other maps exported','\n')
  }
}


#This function reshape cells data from EchoBase into wide format 
#with proper numeric fields

cells4humans=function(df,v.names="data_value",idvar="EIcell_id",
                      timevar="data_type",
                      snames=c("voyage_name","esdu_name","cell_type",
                               "cell_name","esdu_data_type",idvar,
                               "vessel_name","transceivermodel",
                               "processingtemplate","processingdescription",
                               "acousticdensityunit","cell_type",
                               "TimeEnd","TimeStart"),
                      correct.positions=FALSE){
  #Cells in wide format
  #*****************************
  df.wide=reshape(df,v.names =v.names , idvar = idvar,
                  timevar =timevar , direction = "wide")
  head(df.wide)
  names(df.wide)=gsub(paste(v.names,'.',sep=''),'',names(df.wide))
  if (correct.positions){
    #Position correction
    df.wide$LongitudeStart=correct.positions(
      df=df.wide$LongitudeStart,xname="LongitudeStart",asNewColumn=FALSE)
    df.wide$LatitudeStart=correct.positions(
      df=df.wide$LatitudeStart,yname="LatitudeStart",asNewColumn=FALSE)
  }
 
  #Set data column format to numeric
  #*****************************
  df.wide[,!names(df.wide)%in%snames]=
    apply(df.wide[,!names(df.wide)%in%snames],2,as.numeric)
  df.wide
}

#This function convert data from WGACEGG grid files to echobase map format

Grid2Echobase=function(path,zname,voyage,species,path.export=NULL){
  # Import data in grid format
  #*****************************
  mapi=read.table(path.map,sep=';',header=TRUE)
  # Compute grid lags
  #*****************************
  mapi$dx=abs(median(mapi$Xgd[-length(mapi$Xgd)]-mapi$Xgd[-1]))
  mapi$dy=abs(min(mapi$Ygd[-length(mapi$Ygd)]-mapi$Ygd[-1]))
  head(mapi)
  # Plot grid
  #*****************************
  #plot(mapi$Xgd,mapi$Ygd,cex=mapi$Zvalue/1000)
  #coast()
  # Select non-NA 'zname' data
  #*****************************
  mapis=mapi[!is.na(mapi$Zvalue),]
  head(mapis)
  
  # Adds Echobase fields
  #*****************************
  mapiSa=mapis[,c('Xgd','Ygd','Xsample','Ysample','Nsample','Zvalue','Zstdev','dx','dy')]
  head(mapiSa)
  mapiSa$sp=species
  mapiSa$sizeCategory=0
  mapiSa$dpth=0
  mapiSa$dlag=0
  mapiSa$name=paste(mapiSa$Xgd,mapiSa$Ygd,mapiSa$dpth)
  mapiSa$ageCategory=''
  head(mapiSa)  
  # Format outputs
  #*****************************
  # cell mean longitudes
  mapiXs=data.frame(voyage=voyage,mapiSa[,!names(mapiSa)%in%c('Ysample','Nsample','Zvalue','Zstdev')])
  head(mapiXs)
  names(mapiXs)=c('voyage','gridCellLongitude','gridCellLatitude',
                  'Xsample','gridLongitudeLag','gridLatitudeLag',
                  'baracoudaCode','sizeCategory',
                  'gridCellDepth','gridDepthLag','name',
                  'ageCategory')
  mapiXs=mapiXs[,c('voyage','name','baracoudaCode',
                   'sizeCategory','ageCategory',
                   'gridCellLongitude',
                   'gridCellLatitude','gridCellDepth',
                   'gridLongitudeLag','gridLatitudeLag',
                   'gridDepthLag','Xsample')]
  mapiXs$dataQuality=1
  # cell mean latitudes    
  mapiYs=data.frame(voyage=voyage,mapiSa[,!names(mapiSa)%in%c('Xsample','Nsample','Zvalue','Zstdev')])
  names(mapiYs)=c('voyage','gridCellLongitude','gridCellLatitude',
                  'Ysample','gridLongitudeLag','gridLatitudeLag',
                  'baracoudaCode','sizeCategory',
                  'gridCellDepth','gridDepthLag','name',
                  'ageCategory')
  mapiYs=mapiYs[,c('voyage','name','baracoudaCode',
                   'sizeCategory','ageCategory',
                   'gridCellLongitude',
                   'gridCellLatitude','gridCellDepth',
                   'gridLongitudeLag','gridLatitudeLag',
                   'gridDepthLag','Ysample')]
  mapiYs$dataQuality=1
  # cell no. of samples    
  mapiN=data.frame(voyage=voyage,mapiSa[,!names(mapiSa)%in%c('Xsample','Ysample','Zvalue','Zstdev')])
  names(mapiN)=c('voyage','gridCellLongitude','gridCellLatitude',
                 'Nsample','gridLongitudeLag','gridLatitudeLag',
                 'baracoudaCode','sizeCategory',
                 'gridCellDepth','gridDepthLag','name',
                 'ageCategory')
  mapiN=mapiN[,c('voyage','name','baracoudaCode',
                 'sizeCategory','ageCategory',
                 'gridCellLongitude',
                 'gridCellLatitude','gridCellDepth',
                 'gridLongitudeLag','gridLatitudeLag',
                 'gridDepthLag','Nsample')]
  mapiN$dataQuality=1  
  # cell mean 'zname'      
  mapimZ=data.frame(voyage=voyage,mapiSa[,!names(mapiSa)%in%c('Xsample','Ysample','Nsample','Zstdev')])
  head(mapimZ)
  names(mapimZ)=c('voyage','gridCellLongitude','gridCellLatitude',
                  paste('Mean',zname,sep=''),'gridLongitudeLag','gridLatitudeLag',
                  'baracoudaCode','sizeCategory',
                  'gridCellDepth','gridDepthLag','name',
                  'ageCategory')
  mapimZ=mapimZ[,c('voyage','name','baracoudaCode',
                   'sizeCategory','ageCategory',
                   'gridCellLongitude',
                   'gridCellLatitude','gridCellDepth',
                   'gridLongitudeLag','gridLatitudeLag',
                   'gridDepthLag',paste('Mean',zname,sep=''))]
  mapimZ$dataQuality=1
  # cell stdev 'zname'      
  mapisZ=data.frame(voyage=voyage,mapiSa[,!names(mapiSa)%in%c('Xsample','Ysample','Nsample','Zvalue')])
  names(mapisZ)=c('voyage','gridCellLongitude','gridCellLatitude',
                  paste('Stdev',zname,sep=''),'gridLongitudeLag','gridLatitudeLag',
                  'baracoudaCode','sizeCategory',
                  'gridCellDepth','gridDepthLag','name',
                  'ageCategory')
  mapisZ=mapisZ[,c('voyage','name','baracoudaCode',
                   'sizeCategory','ageCategory',
                   'gridCellLongitude',
                   'gridCellLatitude','gridCellDepth',
                   'gridLongitudeLag','gridLatitudeLag',
                   'gridDepthLag',paste('Stdev',zname,sep=''))]
  mapisZ$dataQuality=1
  
  if (!is.null(path.export)){
    write.table(mapiXs,
                paste(path.export,paste(voyage,zname,species,'meanXsMap4Echobase.csv',sep='-'),
                      sep=''),sep=';',row.names=FALSE)
    write.table(mapiYs,
                paste(path.export,paste(voyage,zname,species,'meanYsMap4Echobase.csv',sep='-'),
                      sep=''),sep=';',row.names=FALSE)
    write.table(mapiN,
                paste(path.export,paste(voyage,zname,species,'NsampleMap4Echobase.csv',sep='-'),
                      sep=''),sep=';',row.names=FALSE)  
    write.table(mapimZ,
                paste(path.export,paste(voyage,zname,species,'meanMap4Echobase.csv',sep='-'),
                      sep=''),sep=';',row.names=FALSE)
    write.table(mapisZ,
                paste(path.export,paste(voyage,zname,species,'stdevMap4Echobase.csv',sep='-'),
                      sep=''),sep=';',row.names=FALSE) 
  }
  
  list(meansX=mapiXs,meansY=mapiYs,Nsample=mapiN,meanZ=mapimZ,stdevZ=mapisZ)
}

image.plot.grid=function(dfs1,nmain='NULL'){
  dfs1=dfs1[order(dfs1$x),]
  dfs1.wide <- reshape(dfs1[,c('x','y','value')], v.names = "value", idvar = "y",
                       timevar = "x", direction = "wide")
  head(dfs1.wide)
  dim(dfs1.wide)
  dim(dfs1)
  x=sort(unique(dfs1$x))
  y=sort(unique(dfs1$y))
  z=data.matrix(dfs1.wide[,-1])
  dimnames(z)[[1]]=dfs1.wide$y
  dimnames(z)[[2]]=substr(dimnames(z)[[2]],7,10)
  z=z[order(dfs1.wide$y),]
  library(fields)
  image.plot(x,y,t(z),xlab='',ylab='',main=nmain)
  coast()
  z
}  


# This function converts echo-integration by layer data in baracouda format to Echobase compliant Movies csv format

EIlayBarac2EB=function(path.nc,pathex,missingEI){
  
  #convert to Echobase format
  #Import columns to be filled
  #*****************************
  nmi=read.table(path.nc,header=TRUE,sep=',')
  nmi=as.character(nmi[,1])
  nmi=gsub('/','\\',nmi,fixed=TRUE)
  nmi
  #Import example file
  #*****************************
  csvex=read.table(path,header=TRUE,sep=';')
  head(csvex)
  Mcsv.base=matrix(rep(NA,dim(missing.EI)[1]*dim(csvex)[2]),ncol=dim(csvex)[2])
  Mcsv.base=data.frame(Mcsv.base)
  names(Mcsv.base)=gsub('.','\\',as.character(names(csvex)),fixed=TRUE)
  #Fill DataAcquisition
  Mcsv.base[,'MOVIES_EILayer\\sndset\\channelName']=38000
  Mcsv.base[,'MOVIES_EILayer\\sndset\\absorptionCoef']=8.47  
  Mcsv.base[,'MOVIES_EILayer\\sndset\\pulseduration']=1.024
  Mcsv.base[,'MOVIES_EILayer\\sndset\\soundcelerity']='variable'
  Mcsv.base[,'MOVIES_EILayer\\sndset\\softChannelId']=47
  Mcsv.base[,'MOVIES_EILayer\\sndset\\transmissionPower']=2000
  #Fill DataProcessing
  Mcsv.base['MOVIES_EILayer']=format(Sys.time(), "%Y/%m/%d %H:%M:%OS4")
  Mcsv.base[,'MOVIES_EILayer\\cellset\\thresholdlow']=-60
  Mcsv.base['MOVIES_EILayer\\cellset\\thresholdup']=0
  Mcsv.base[,'MOVIES_EILayer\\sndset\\soundcelerity']='variable'
  #Fill Esdu/data
  Mcsv.base[,'MOVIES_EILayer\\cellset\\lat']=missing.EI$LATDMC
  Mcsv.base[,'MOVIES_EILayer\\cellset\\long']=missing.EI$LGDMC
  Mcsv.base[,'MOVIES_EILayer\\shipnav\\depth']=missing.EI$SONDE
  Mcsv.base[,'MOVIES_EILayer\\cellset\\depthstart']=missing.EI$HMAX_TRANCHE
  Mcsv.base[,'MOVIES_EILayer\\cellset\\depthend']=missing.EI$HMIN_TRANCHE
  dend=c(format(missing.EI$DHTU_ESDU[-1]),'06/06/1998 15:22:47')
  missing.EI$DHTU_ESDU[length(missing.EI$DHTU_ESDU)]
  Mcsv.base[,'MOVIES_EILayer\\cellset\\dateend']=paste(dend,'.0000',sep='')
  Mcsv.base[,'MOVIES_EILayer\\cellset\\datestart']=paste(format(missing.EI$DHTU_ESDU),
                                                         '.0000',sep='')
  Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']=missing.EI$ENERGIE
  unique(missing.EI$Nb_Echant_Integre)
  unique(missing.EI$Nb_Ecant_Total)
  #Mcsv.base[,'MOVIES_EILayer\\eilayer\\ni']=missing.EI$Nb_Echant_Integre
  #Mcsv.base[,'MOVIES_EILayer\\eilayer\\nt']=missing.EI$Nb_Ecant_Total
  lt=c(paste('S',seq(10),sep=''),paste('F',seq(4),sep=''),'TOTAL')
  ebl=seq(0,(length(lt)-1),1)
  tlt=data.frame(bara=lt,echo=ebl)
  names(Mcsv.base)
  ntl2=merge(missing.EI[,c('DHTU_ESDU','LIBELLE_TRANCHE')],tlt,by.x='LIBELLE_TRANCHE',
             by.y='bara',sort=FALSE)
  ntl2$t=paste(format(ntl2$DHTU_ESDU),'.0000',sep='')
  names(ntl2)
  names(Mcsv.base)
  Mcsv.base[,'MOVIES_EILayer\\cellset\\celltype']=0
  Mcsv.base[missing.EI$LIBELLE_TRANCHE=='TOTAL',
            'MOVIES_EILayer\\cellset\\celltype']=4
  Mcsv.base[substr(missing.EI$LIBELLE_TRANCHE,1,1)=='F',
            'MOVIES_EILayer\\cellset\\celltype']=1
  Mcsv.base[,'MOVIES_EILayer\\cellset']=missing.EI$LIBELLE_TRANCHE
  dim(Mcsv.base)
  names(Mcsv.base)
  Mcsv.base=merge(Mcsv.base,ntl2[,c('t','LIBELLE_TRANCHE','echo')],
                  by.x=c('MOVIES_EILayer\\cellset\\datestart',
                         'MOVIES_EILayer\\cellset'),
                  by.y=c('t','LIBELLE_TRANCHE'))
  dim(Mcsv.base)
  names(Mcsv.base)
  Mcsv.base[,'MOVIES_EILayer\\cellset\\cellnum']=Mcsv.base[,'echo']
  Mcsv.base[,'MOVIES_EILayer\\cellset\\area']=1
  Mcsv.base$dataQuality=1
  #Mcsv.base[,c('MOVIES_EILayer\\cellset','MOVIES_EILayer\\cellset\\cellnum')]
  Mcsv.base[,'MOVIES_EILayer\\cellset']=NA
  
  #Select columns not entirely padded with NA
  nNAcols=colSums(apply(Mcsv.base,2,is.na))
  Mcsv.bases=Mcsv.base[,nNAcols!=dim(Mcsv.base)[1]]
  names(Mcsv.base)
  Mcsv.base=Mcsv.base[,names(Mcsv.base)!='echo']
  names(Mcsv.base)
  
  Mcsv.base

}  



#Function for generating baracouda compliant files based on raw data files
#*****************************
#Inputs:
#*****************************
# cruise: cruise name
# vessel: vessel name (default 'Thalassa')
# path.stations: path to metadata file: casino .csv file, if is NA (the default), 
#  no data are imported
# path.param.sounder: path to silly file with dum sounder parameters
# path.lay: path to folder with Echointegration by layer files, if is NA 
#  (the default), no data are imported
# path.man: path to folder with supervised Echointegration files, if is NA 
#  (the default), no data are imported
# path.export: path to folder where to export baracouda-style files, if is NA 
#  (the default), no data are exported
# strate.ok: valid depth strata for hauls
# cruise='PELGAS11'
# path.stations=path.cas

format4baracouda=function(cruise,vessel=NA,path.stations=NA,
                          path.param.sounder=NA,path.lay=NA,path.man=NA,path.export=NA,
                          strate.ok=c('CLAS','SURF','NULL'),lgear.codes=c('76x70','57x52','MIK',
                                                                         '942OBS','725OBS','PTM',
                                                                         'PS'),
                          vir.codes=c('DVIR'),fil.codes=c('DFIL'),
                          proNb=5,zname='CINNA Sonde vert (m)',rzname="CINNA.Sonde.vert..m.",
                          sep.cas='\t',septs.df=NULL,
                          cnames=c("Code.Action","Date","Strate","Observation","Num.Station","Heure",
                                   rzname,"Latitude","Longitude","Code.Appareil",
                                   "FUNES.L..filee.Tribord..m.","CINNA.Vit..fond..nd.",
                                   "MARPORT.Ouv..verticale..m.","MARPORT.Ouv..horizontale..m.",
                                   "MARPORT.Dist..des.panneaux..m.",
                                   "FUNES.L..filee.Babord..m.","FUNES.Tension.babord..daN.","CINNA.Cap..deg.",
                                   "MARPORT.Vit..longitudinale..m.s.","CINNA.Cap.derive..deg.",
                                   "CINNA.Vitesse.derive..nd.","METBA.Dir..vent.vrai..deg.",
                                   "METBA.Vit..vent.vrai..nd.","MARPORT.Altitude.corde.de.fond..m.")){
  
  library(foreign)
  
  if (!is.na(path.stations)){
    #*****************************	
    #Import station metadata -----
    #*****************************
    
    cas=read.csv(path.stations,header=TRUE,sep=sep.cas)
    names(cas)
    if (!rzname%in%names(cas)){
      stop('Seabed depth column name not in metadata file, please check')
    }else{
      # if (is.null(cnames)){
      #   cnames=c("Code.Action","Date","Strate","Observation","N..Station","Heure",
      #            rzname,"Latitude.deg...min.milli.","Longitude.deg...min.milli.","Code.Appareil",
      #            "Long..filee.m.","Vit..fond.noeuds.","Ouv..verticale.m.","Distance..3.m.",
      #            "Distance..2.m.","Long..filee.Ba..m.","Angle.longitudinal.degres.","Cap.navire.degres.",
      #            "Vit..longitudinale.m.","Cap.derive.degres.","Vit..derive.noeuds.",
      #            "Dir..vent.vrai.degres.","Vent.vrai.noeuds.")
      # }
      mcn=cnames[!cnames%in%names(cas)]
      if (length(mcn)>0){
        stop('Column header not found in metadata file:','\n',
             paste(mcn,collapse = '\n'))
        # cnames=c("Code.Action","Date","Strate","Observation","N..Station","Heure",
        #          rzname,"Latitude.deg...min.milli.","Longitude.deg...min.milli.","Code.Appareil",
        #          "FUNES.Long..filee.Tr..m.","CINNA.Vit.fond..N.","TRAWL.Ouv..verticale..m.",
        #          "TRAWL.Dist..horizontale.Tr..m.",
        #          "TRAWL.Altitude.corde.d.eau..m.","FUNES.Long..filee.Ba..m.",
        #          "TRAWL.Gisement.Ba..degres.","CINNA.Cap..deg.",
        #          "TRAWL.Vit..longitudinale..m.","CINNA.Derive..deg.","CINNA.Vitesse.derive..N.",
        #          "Dir..vent.vrai.degres.","Vent.vrai.noeuds.")
        # mcn2=cnames[!cnames%in%names(cas)]
        # if (length(mcn2)>0){
        #   cnames=c("Code.Action","Date","Strate","Observation","Num.Station","Heure",
        #            rzname,"Latitude","Longitude","Code.Appareil",
        #            "FUNES.L..filee.Tribord..m.","CINNA.Vit..fond..nd.",
        #            "MARPORT.Ouv..verticale..m.","MARPORT.Ouv..horizontale..m.",
        #            "MARPORT.Altitude.corde.d.eau...fond..m.",
        #            "FUNES.L..filee.Babord..m.","FUNES.Tension.babord..daN.","CINNA.Cap..deg.",
        #            "MARPORT.Vit..longitudinale..m.s.","CINNA.Cap.derive..deg.",
        #            "CINNA.Vitesse.derive..nd.","METBA.Dir..vent.vrai..deg.",
        #            "METBA.Vit..vent.vrai..nd.")
        #   mcn3=cnames[!cnames%in%names(cas)]
        #   if (length(mcn3)>0){
        #     cnames=c("Code.Action","Date","Strate","Observation","Num.Station","Heure",
        #              rzname,"Latitude","Longitude","Code.Appareil",
        #              "FUNES.L..filee.Tribord..m.","CINNA.Vit..fond..nd.",
        #              "TRAWL.Ouv..verticale..m.","TRAWL.Ouv..horizontale..m.",
        #              "TRAWL.Altitude.corde.de.fond..m.",
        #              "FUNES.L..filee.Babord..m.","FUNES.Tension.babord..daN.","CINNA.Cap..deg.",
        #              "TRAWL.Vit..longitudinale..m.s.","CINNA.Cap.derive..deg.",
        #              "CINNA.Vitesse.derive..nd.","METBA.Dir..vent.vrai..deg.",
        #              "METBA.Vit..vent.vrai..nd.")
        #     mcn4=cnames[!cnames%in%names(cas)]
        #     if (length(mcn4)>0) stop('Column header not found in metadata file',
        #                              paste(mcn4,collapse = '\n'))
        #   } 
        # }
      }
     cass=cas[cas$Code.Appareil%in%lgear.codes,cnames]      
    }
    names(cass)
    names(cass)=c('ACTION','DATE','STRATE','Observation','NOSTA','H','SONDE','LAT','LON',
                  'TYPECHAL','LFUNE','VFOND','OUVV','OUVA','OUVP','LGB','ANGLE',
                  'CAP','VCHALF','DIRCOURS','COURS','DIRVENT','VENT','ZCHAL')
    #convert numeric columns to character
    cass[,c('SONDE','LFUNE','VFOND','OUVV','OUVA','OUVP','LGB','ANGLE',
            'CAP','VCHALF','DIRCOURS','COURS','DIRVENT','VENT','ZCHAL')]=
      apply(cass[,c('SONDE','LFUNE','VFOND','OUVV','OUVA','OUVP','LGB','ANGLE',
                    'CAP','VCHALF','DIRCOURS','COURS','DIRVENT','VENT','ZCHAL')],
            2,as.character)
    #replace all commas by points
    cass$LAT=gsub(',','.',cass$LAT)
    cass$LON=gsub(',','.',cass$LON)
    cass$SONDE=gsub(',','.',cass$SONDE)
    cass$LFUNE=gsub(',','.',cass$LFUNE)
    cass$VFOND=gsub(',','.',cass$VFOND)
    cass$OUVV=gsub(',','.',cass$OUVV)
    cass$OUVA=gsub(',','.',cass$OUVA)
    cass$OUVP=gsub(',','.',cass$OUVP)
    cass$LGB=gsub(',','.',cass$LGB)
    cass$ANGLE=gsub(',','.',cass$ANGLE)
    cass$CAP=gsub(',','.',cass$CAP)
    cass$VCHALF=gsub(',','.',cass$VCHALF)
    cass$DIRCOURS=gsub(',','.',cass$DIRCOURS)
    cass$COURS=gsub(',','.',cass$COURS)
    cass$DIRVENT=gsub(',','.',cass$DIRVENT)
    cass$VENT=gsub(',','.',cass$VENT)
    cass$ZCHAL=gsub(',','.',cass$ZCHAL)
    #convert numeric columns to numeric
    cass[,c('SONDE','LFUNE','VFOND','OUVV','OUVA','OUVP','LGB','ANGLE',
            'CAP','VCHALF','DIRCOURS','COURS','DIRVENT','VENT','ZCHAL')]=
      apply(cass[,c('SONDE','LFUNE','VFOND','OUVV','OUVA','OUVP','LGB','ANGLE',
                    'CAP','VCHALF','DIRCOURS','COURS','DIRVENT','VENT','ZCHAL')],
            2,as.numeric)
    cass$vesselName=vessel
    cass$vesselName=as.character(cass$vesselName)
    #Adds pro vessel names
    cass[substr(cass[,'NOSTA'],2,2)==5,'vesselName']=
      as.character(cass[substr(cass[,'NOSTA'],2,2)==proNb,'Observation'])
    dim(cass)
    head(cass)
    length(unique(cass$NOSTA))
    length(unique(cass[cass$ACTION=='DVIR','NOSTA']))
 
    #Compute min/mean/max parameters and merge with events -----
    #***************************** 
    mincass=aggregate(cass[,c('VFOND','LGB')],list(cass$NOSTA),min,na.rm=TRUE)
    names(mincass)=c('NOSTA','VMIN','MINF')
    maxcass=aggregate(cass[,c('VFOND','LGB','OUVV','OUVA','OUVP',
                              'ANGLE','ZCHAL')],list(cass$NOSTA),max,na.rm=TRUE)
    names(maxcass)=c('NOSTA','VMAX','MAXF','OUVV','OUVA','OUVP',
                     'ANGLE','ZCHAL')
    mcass=aggregate(cass[,c('SONDE','LGB','LFUNE','VFOND','CAP','VCHALF',
                            'DIRCOURS','COURS','DIRVENT','VENT')],
                    list(cass$NOSTA),mean,na.rm=TRUE)
    names(mcass)=c('NOSTA','SONDE','LGB','LFUNE','VBATF','CAP',
                   'VCHALF','DIRCOURS','COURS','DIRVENT','VENT')
    stations2=merge(mincass,maxcass)	
    dim(stations2)
    stations2=merge(stations2,mcass)	
    dim(stations2)
    # df with most reasonable estimates of operations metadata:
    # -  min 'VMIN','MINF'
    # -  max 'VMAX','MAXF','OUVV','OUVA','OUVP','ANGLE','ZCHAL'
    # -  mean 'SONDE','LGB','LFUNE','VBATF','CAP','VCHALF','DIRCOURS',
    #    'COURS','DIRVENT','VENT'
    head(stations2)
    
    if (!is.null(septs.df)){
      head(cass)
      unique(cass$TYPECHAL)
      cass.dfilef=merge(cass[,c('TYPECHAL','ACTION','NOSTA','H','SONDE','LAT','LAT','LON','LON',
                                'DATE','STRATE','TYPECHAL','vesselName')],
                        septs.df,by.x='TYPECHAL',by.y='Code.Appareil')
      cass.dfil=cass.dfilef[as.character(cass.dfilef$ACTION)==as.character(cass.dfilef$spts),]
      cass.dfil=cass.dfil[,c('NOSTA','H','SONDE','LAT','LAT','LON','LON')]
      names(cass.dfil)=c('NOSTA','HF','SONDF','LATF','LATFDD','LGF','LGFDD')
      cass.dvir=cass.dfilef[as.character(cass.dfilef$ACTION)==as.character(cass.dfilef$epts),]
      cass.dvir=cass.dvir[,c('NOSTA','H','SONDE','LAT','LAT','LON','LON')]
      names(cass.dvir)=c('NOSTA','HV','SONDV','LATV','LATVDD','LGV','LGVDD')
      metad=unique(cass.dfilef[as.character(cass.dfilef$ACTION)==as.character(cass.dfilef$epts),
                        c('NOSTA','DATE','STRATE','TYPECHAL','vesselName')])
    }else{
      cass.dfil=cass[cass$ACTION%in%fil.codes,c('NOSTA','H',
                                                'SONDE','LAT','LAT','LON','LON')]
      names(cass.dfil)=c('NOSTA','HF','SONDF','LATF','LATFDD','LGF','LGFDD')
      dim(cass.dfil)
      stations2$NOSTA[!stations2$NOSTA%in%cass.dfil$NOSTA]
      cass.dvir=cass[cass$ACTION%in%vir.codes,c('NOSTA','H',
                                                'SONDE','LAT','LAT','LON','LON')]
      dim(cass.dvir)
      names(cass.dvir)=c('NOSTA','HV','SONDV','LATV','LATVDD','LGV','LGVDD')
      metad=unique(cass[cass$ACTION%in%fil.codes,
                        c('NOSTA','DATE','STRATE','TYPECHAL','vesselName')])
    }
    cass.fv=merge(cass.dfil,cass.dvir)
    dim(cass.fv)
    head(cass.fv)
    # check duplicated hauls -------
    #*****************************
    haulim=merge(cass.fv,stations2)
    dim(haulim)
    if (T%in%duplicated(haulim$NOSTA)){
      cat('Duplicated haul events:',as.character(
        haulim$NOSTA[duplicated(haulim$NOSTA)]),"\n")
      cat('Second occurence discarded',"\n")
    }
    haulim=haulim[!duplicated(haulim$NOSTA),]
    dim(haulim)
  
    if (T%in%duplicated(metad$NOSTA)){
      cat('Duplicated haul metadata:',as.character(
        metad$NOSTA[duplicated(metad$NOSTA)]),"\n")
      cat('Second occurence discarded',"\n")
    }
    metad=metad[!duplicated(metad$NOSTA),]
    dim(metad)
    dim(haulim)
    stations2f=merge(haulim,metad,by.x='NOSTA',by.y='NOSTA')
    dim(stations2f)
    head(stations2f)
    unique(stations2f$TYPECHAL)
    unique(stations2f$STRATE)
    
    #Check 'strate' field ------
    #*****************************
    stations2f$STRATE=as.character(stations2f$STRATE)
    stations2f$STRATE=gsub(' ','',stations2f$STRATE)
    strate.errors=stations2f[!stations2f$STRATE%in%strate.ok,]

    if (dim(strate.errors)[1]>0){
      cat("Invalid 'strate' fields in hauls:",
          as.character(unique(strate.errors$NOSTA)),'\n')
      stop('Please correct errors before proceeding')
    }
    
    #Order hauls and create 'NOCHAL' field
    #*****************************
    stations2f=stations2f[order(stations2f$NOSTA),]
    stations2f$NOCHAL=seq(length(unique(stations2f$NOSTA)))
    names(stations2f)
    dim(stations2f)
    #names(stations)
    nstations=data.frame(MISSION=paste(cruise,'_',sep=''),
                         NAVIRE=stations2f$vesselName,
                         JJ=substr(stations2f$DATE,1,2),MM=substr(stations2f$DATE,4,5),
                         AA=substr(stations2f$DATE,7,8),DATE=stations2f$DATE,REFCIEM='',
                         STRATE=stations2f$STRATE,NOCHAL=stations2f$NOCHAL,
                         NOSTA=stations2f$NOSTA,CAPTOT=NA,J_N=NA,ESDU=NA,stations2f[,
                                                                                    c("HF","SONDF","LATF","LATFDD","LGF","LGFDD","HV","SONDV",
                                                                                      "LATV","LGV","LATVDD","LGVDD","TYPECHAL","LFUNE","VMAX","VMIN")],
                         D1=NA,D2=NA,D3=NA,D4=NA,D5=NA,D6=NA,
                         stations2f[,c("OUVV","OUVA","OUVP","LGB")],LEST=NA,
                         stations2f[,c("MINF","MAXF","ANGLE")],
                         AMURE=NA,stations2f[,c("CAP","VCHALF","VBATF","DIRCOURS","COURS",
                                                "DIRVENT","VENT")],DIRHOULE=NA,
                         HAUTHOULE=NA,ZCHAL=stations2f[,c("ZCHAL")])
    names(nstations)
    nstations$NAVIRE
    nstations$TYPECHAL  
    #dim(stations)
    
    #nstations$LATFDD=gsub('\\.',',',nstations$LATFDD)
    #nstations$LGFDD=gsub('\\.',',',nstations$LGFDD)
    #nstations$LATVDD=gsub('\\.',',',as.character(nstations$LATVDD))
    #nstations$LGVDD=gsub('\\.',',',as.character(nstations$LGVDD))
    #nstations$LATF=gsub('\\.',',',nstations$LATF)
    #nstations$LGF=gsub('\\.',',',nstations$LGF)
    #nstations$LATV=gsub('\\.',',',as.character(nstations$LATV))
    #nstations$LGV=gsub('\\.',',',as.character(nstations$LGV))
    
    #Export pc file ----
    if (!is.na(path.export)){
      write.dbf(nstations,paste(path.export,cruise,'_pc.DBF',sep=''))
    }
  }else{
    nstations=NULL
  }
  #*****************************	
  #Import sounder parameters -----
  #*****************************
  if (!is.na(path.param.sounder)){
    snd.param=read.dbf(path.param.sounder)
    head(snd.param)
  }else{
    snd.param=NULL
  }
  
  if (!is.na(path.lay)&!is.na(path.man)){
    
    #*****************************	
    #Import echo-integration by layer data ----
    #*****************************	
    pel.lay=import.lay.folder2(path.lay=path.lay,mergesu=T,add.laydef=TRUE,
                               IDfile=FALSE)
    names(pel.lay)
    pel.sa=pel.lay$sa.db
    head(pel.sa)
    head(sadev)
    pel.sa$NESU
    dim(pel.sa)
    head(pel.sa)
    names(pel.sa)
    pel.sa[duplicated(pel.sa$TC),]
    pel.sa[duplicated(pel.sa$ts),]
    pel.sa$TC=gsub('-','',pel.sa$t)
    
    #check for duplicated ESDUs
    #*****************************	
    duplisa=pel.sa[pel.sa$TC%in%pel.sa[duplicated(pel.sa$TC),'TC'],]
    duplisa=duplisa[order(duplisa$TC),]
    if (dim(duplisa)[1]>0) stop(paste('ESDUs',duplisa$TC,
                                      'duplicated in EIlayer file',duplisa$ID,'\n'))
    dim(unique(duplisa))
    
    #Format raw EI data to be merged with supervised EI data
    #*****************************	
    sas=pel.sa[,c('TC','SL1','zSL0','zSL1','SL2','zSL1','zSL2',
                  'SL3','zSL2','zSL3','SL4','zSL3','zSL4',
                  'SL5','zSL4','zSL5','SL6','zSL5','zSL6',
                  'SL7','zSL6','zSL7','SL8','zSL7','zSL8',
                  'SL9','zSL8','zSL9','SL10','zSL9','zSL10',
                  'BL1','zBL1','zBL2','BL2','zBL2','zBL3',
                  'BL3','zBL3','zBL4','BL4','zBL4')]
    sas$AMAXF4=30
    sas=data.frame(sas,pel.sa[,c('SLtot','log','dpth','ESUt','ESUl')],
                   SNUM=-120,pel.sa[,c('lEIthr','uEIthr','speed','Nemis')],
                   BOTTERRSA=0,BOTTERRNEC=0)
    
    names(sas)=c("TC","S1","IMINS1","IMAXS1","S2","IMINS2","IMAXS2",    
                 "S3","IMINS3","IMAXS3","S4","IMINS4","IMAXS4","S5","IMINS5",
                 "IMAXS5","S6","IMINS6","IMAXS6","S7","IMINS7","IMAXS7",
                 "S8","IMINS8","IMAXS8","S9","IMINS9","IMAXS9","S10","IMINS10",
                 "IMAXS10","F1","AMINF1","AMAXF1","F2","AMINF2","AMAXF2","F3",
                 "AMINF3","AMAXF3","F4","AMINF4","AMAXF4","TOTAL","IREF",
                 "RECSOND","ESUTYPE","ESUCUT","SNUM","SEIMIN","SEIMAX","SPEED",
                 "NBEMISS","BOTTERRSA","BOTTERRNEC") 
    
    #check for duplicated ESDUs
    #*****************************	
    length(sas$TC)
    length(unique(sas$TC))
    length(pel.sa$ts)
    length(unique(pel.sa$ts))
    
    duplisa=sas[sas$TC%in%sas[duplicated(sas$TC),'TC'],]
    duplisa=duplisa[order(duplisa$TC),]
    if (dim(duplisa)[1]>0) stop(paste('ESDUs',unique(duplisa$TC),
                                      'duplicated in EIlayer files','\n'))
    dim(unique(duplisa))
    
    #*****************************	
    #Import supervized echo-integration by layer data ----
    #*****************************	
    
    man.import=import.man.folder(path.man)
    names(man.import)
    sadev=man.import$sa.db
    names(sadev)
    sadev$LATDMC=paste(sadev$ym,sadev$Llat,sep='')
    sadev$LGDMC=paste(sadev$xm,sadev$Llong,sep='')
    sadev$TC=gsub('-','',sadev$t)
    sadev$R=substr(sadev$ID,2,3)
    DEVdef=man.import$DEVdef
    #DEVdef=DEVdef[1:4,]
    head(sadev)
    
    #check for duplicated ESDUs
    #*****************************	
    dupliman=sadev[sadev$TC%in%sadev[duplicated(sadev$TC),'TC'],]
    dupliman=dupliman[order(dupliman$TC),]
    if (dim(dupliman)[1]>0) stop(paste('ESDUs',dupliman$TC,
                                       'duplicated in EI supervised files',dupliman$ID,'\n'))
    dim(unique(dupliman))
    
    #Select columns in Fishview format
    #*****************************	
    eidev2=data.frame(sadev[,c('NESU','LATDMC','LGDMC','y','x','h',
                               'dpth','date','TC','R')],FLAG=1,D1=sadev$D1,D1F='',D2=sadev$D2,
                      D2F='',D3=sadev$D3,D3F='',D4=sadev$D4,D4F='')
    
    if (dim(DEVdef)[1]>4){
      eidev2=data.frame(eidev2,sadev[,
                                     paste('D',seq(5,dim(DEVdef)[1],1),sep='')])
      for (i in 5:dim(DEVdef)[1]){
        eidev2$NC=''
        names(eidev2)[names(eidev2)=='NC']=paste('D',i,'F',sep='')
      }
    }
    #eidev2$POISSONS=sadev$TOTAL
    eidev2$POISSONS=eidev2$D1+eidev2$D2+eidev2$D3+eidev2$D4
    eidev2$CHA_REF=NA
    names(eidev2)[c(1,4:8)]=c("ESDU","LAT","LONG","H","PROF","DATE")
    head(eidev2)	
    
    #*****************************	
    #Merge supervised EI and EI layer data ----
    #*****************************	
    
    dim(eidev2)
    dim(sas)
    neidev=merge(eidev2,sas,by.x='TC',by.y='TC')
    neidev[duplicated(neidev$TC),]
    dim(neidev)
    head(neidev)
    Ndev=dim(DEVdef)[1]	
    if (Ndev==4){	
      neidev2=data.frame(neidev[,2:9],TC=neidev$TC,neidev[,10:19],
                         neidev[,c('POISSONS','CHA_REF')])
    }else if (Ndev>4){
      metad=neidev[,2:9]
      D4s=neidev[,10:19]
      laydef=neidev[,36:89]
      Dsup=neidev[,20:((20+(Ndev-4)*2)-1)]
      final=neidev[,34:35]
      neidev2=data.frame(metad,TC=neidev$TC,D4s,laydef,Dsup,final)
    }
    neidev2$H=as.character(neidev2$H)
    head(neidev2)
    #*****************************	
    #Export files ----
    #*****************************
    if (!is.na(path.export)){
      write.dbf(neidev2,paste(path.export,cruise,'_ec.DBF',sep=''))
      write.dbf(DEVdef,paste(path.export,cruise,'EspDev.DBF',sep=''))
    }
  }else{
    DEVdef=NULL
    neidev2=NULL
  }
  list(DEVdef=DEVdef,ec=neidev2,pc=nstations,operations=nstations)
}	

GridMaps4Echobase=function(mission='%',voyage='%',path.export=NULL,host,dbname,user,
                           password,port,filenamei='Coser',newbathy=TRUE,ux11=FALSE,
                           ...){
  
  # 1.1. Connection to a PostgreSQL database (on a server)
  #*****************************
  library(RPostgreSQL)
  
  drv <- dbDriver("PostgreSQL")
  
  con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                   password=password,port=port)
   
  # extract map data (longitude and latitude positions)
  #*****************************
  sqlMapData=paste("SELECT MISSION.NAME, VOYAGE.NAME, CELL.NAME, DATA.DATAVALUE, DATAMETADATA.NAME FROM PUBLIC.VOYAGE AS VOYAGE,
                   PUBLIC.MISSION AS MISSION, PUBLIC.CELL AS CELL, PUBLIC.DATA AS DATA, 
                   PUBLIC.DATAMETADATA AS DATAMETADATA WHERE VOYAGE.MISSION = MISSION.TOPIAID AND CELL.VOYAGE = VOYAGE.TOPIAID AND 
                   DATA.CELL = CELL.TOPIAID AND DATA.DATAMETADATA = DATAMETADATA.TOPIAID AND MISSION.NAME like'",mission,"'",
                   "AND VOYAGE.NAME like'",voyage,"'",sep='')
  MapData <- dbGetQuery(con,sqlMapData)
  dbDisconnect(con)
  if (dim(MapResults)[1]==0) stop('Check fish map cells in database')
  
  # format map data
  #*****************************
  head(MapData)
  names(MapData)=c('mission','voyage','cell','value','data')
  MapDatas=MapData[MapData$data%in%c('GridCellLongitude','GridCellLatitude'),]
  head(MapDatas)
  xy.df=unique(merge(MapDatas[MapDatas$data=='GridCellLongitude',c('cell','value','voyage')],
                     MapDatas[MapDatas$data=='GridCellLatitude',c('cell','value','voyage')],
                     by.x=c('cell','voyage'),by.y=c('cell','voyage')))
  head(xy.df)
  xy.df[,c('value.x','value.y')]=apply(xy.df[,c('value.x','value.y')],2,as.character)
  xy.df[,c('value.x','value.y')]=apply(xy.df[,c('value.x','value.y')],2,as.numeric)
  
  #xy.df[,2]
  names(xy.df)[names(xy.df)%in%c('value.x','value.y')]=c('x','y')
  unique(xy.df$voyage)
  # plot map cell positions
  #*****************************
  plot(as.numeric(as.character(MapDatas[MapDatas$data=='GridCellLongitude','value'])),
       as.numeric(as.character(MapDatas[MapDatas$data=='GridCellLatitude','value'])),xlab='',ylab='',main='Grid vertices')
  coast()
  
  # extract fish map results: 
  #*****************************
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv,host=host,dbname=dbname,user=user,
                   password=password,port=port)
  sqlMapResults=paste("SELECT mission.name AS mission, voyage.name AS voyage, cell.name AS cellname, celltype.id AS celltype, 
                      result.resultvalue, datametadata.name AS datametadata, species.baracoudacode FROM public.voyage AS voyage, public.mission AS mission,
                      public.transit AS transit, public.transect AS transect, public.dataacquisition AS dataacquisition, public.cell AS cell, 
                      public.celltype AS celltype, public.result AS result, public.datametadata AS datametadata, public.category AS category, 
                      public.speciescategory AS speciescategory, public.species AS species WHERE voyage.mission = mission.topiaid AND 
                      transit.voyage = voyage.topiaid AND transect.transit = transit.topiaid AND dataacquisition.transect = transect.topiaid AND
                      cell.voyage = voyage.topiaid AND cell.celltype = celltype.topiaid AND result.cell = cell.topiaid AND
                      result.datametadata = datametadata.topiaid AND result.category = category.topiaid AND 
                      category.speciescategory = speciescategory.topiaid AND speciescategory.species = species.topiaid AND celltype.id = 'Map'
                      AND MISSION.NAME like'",mission,"'","AND VOYAGE.NAME like'",voyage,"'",sep='')
  MapResults <- dbGetQuery(con,sqlMapResults)
  dbDisconnect(con)
  if (dim(MapResults)[1]==0) stop('Check fish map results in database')
  head(MapResults)
  
  # format map results
  #*****************************
  # merge map cell data and results
  names(MapResults)=c('mission','voyage','cell','celltype','value','parameter','sp')
  MapResults2=merge(MapResults,xy.df,by.x=c('cell','voyage'),by.y=c('cell','voyage'))
  
  # convert values to numeric
  MapResults2$value=as.numeric(as.character(MapResults2$value))
  head(MapResults2)
  # reshape dataframe
  wMapResults=reshape(MapResults2,v.names='value',idvar = c("cell","mission","voyage","celltype","sp","x","y"),
                      timevar = "parameter", direction = "wide")
  head(wMapResults)
  uc=unique(wMapResults$voyage)
  for (i in 1:length(uc)){
    n1=grep(FALSE,is.letters(unique(wMapResults$voyage)[i]))[1]
    mi=substr(unique(wMapResults$voyage)[i],1,(n1-1))
    if (i==1){missions=mi
    }else{missions=c(missions,mi)}
  }
  if (length(unique(missions))>1) stop('All data must come from the same mission')
  wMapResults$mission=unique(missions)
  wMapResults$year=substr(wMapResults$voyage,n1,nchar(wMapResults$voyage))
  
  # 3. Plot average maps over the series
  #*****************************
  # Define output path
  
  # set path.maps to NULL if you do not want to save the plots
  
  # species list
  lsp=unique(wMapResults$sp)
  
  for (i in 1:length(lsp)){
    # select 1 species
    wMapResultsi=wMapResults[wMapResults$sp==lsp[i],]
    head(wMapResultsi)
    # Compute mean and stdev maps per file over the series
    #*****************************--
    #Plot maps per voyage over the series
    # define titles
    namz=paste(unique(wMapResultsi$mission),'meanBiomass',sep='_')
    nams=paste(unique(wMapResultsi$mission),'stedvBiomass',sep='_')
    namn=paste(unique(wMapResultsi$mission),'NoOfSamples',sep='_')
    # plot maps
    mat1=grid.plot(pat=wMapResultsi,input='Echobase',pat2=path.export,a=0,ux11=TRUE,namz=namz,namn=namn,nams=nams,
                   filenamei='id_year_type',newbathy = newbathy)

    # convert Echobase data to gridFile  format
    mat=Echobase2grid(mat=wMapResultsi)
    head(mat)
    #Compute mean and stdev maps over the series
    mmap=aggregate(mat[,c('Zvalue','Nsample')],list(Xgd=mat$Xgd,Ygd=mat$Ygd,I=mat$I,J=mat$J),mean,na.rm=TRUE)
    names(mmap)[5:6]=c("Zvalue",'Nsample')
    sdmap=aggregate(mat$Zvalue,list(Xgd=mat$Xgd,Ygd=mat$Ygd,I=mat$I,J=mat$J),sd,na.rm=TRUE)
    names(sdmap)[5]=c("Zstdev")
    msdmap=merge(mmap,sdmap,by.x=c('Xgd','Ygd','I','J'),by.y=c('Xgd','Ygd','I','J'))
    head(msdmap)
    n1=grep(FALSE,is.letters(unique(mat$Survey)[1]))[1]
    msdmap$Survey=substr(unique(mat$Survey)[1],1,(n1-1))
    msdmap$Year=paste(min(mat$Year),max(mat$Year),sep='-')
    msdmap=msdmap[order(msdmap$J,msdmap$I),]
    msdmap$mission=msdmap$Survey
    msdmap$sp=lsp[i]
    if (length(unique(mat$Year))==1){
      cat('Data series with only one year : standard deviation maps values set to 1','\n')
      msdmap$Zstdev=1
    }  
    head(msdmap)
    # save mean and sd file
    write.table(msdmap,paste(path.export,unique(msdmap[,c('Survey')]),unique(msdmap[,c('Year')]),lsp[i],'-MeanSD.csv',sep=''),
                sep=';',row.names=FALSE)
    
    #Plot mean maps over the series
    # define titles
    namz=paste(unique(msdmap$Survey),'mean biomass')
    nams=paste(unique(msdmap$Survey),'stedv biomass')
    namn=paste(unique(msdmap$Survey),'No. of samples')
    # plot maps
    mat=grid.plot(pat=msdmap,input='gridDataframe',pat2=path.export,a=0,ux11=ux11,
                  namz=namz,namn=namn,nams=nams,filenamei=filenamei,...)
    
    if (i==1){
      msdmap.db=msdmap
    }else{
      msdmap.db=rbind(msdmap.db,msdmap)
    }
  }
  
  graphics.off()
}

#function to add metadata to results

Add.EB.meta=function(df,esduRef,tname='TC'){
  df$TC2=format(strptime(df[,tname],"%Y/%d/%m %H:%M:%OS",tz="GMT"),
                "%Y-%m-%d %H:%M:%S")
  df$TC2=paste(df$TC2,".0000",sep='')
  cat('tname is datestart:','\n')
  print(table(df$TC2%in%esduRef$datestarteb))
  cat('tname is dateend:','\n')
  print(table(df$TC2%in%esduRef$dateendeb))
  # chamens.wide TC=datestart -> adds dateend
  dim(df)
  df2=merge(df,esduRef[,c('datestarteb','dateendeb','RAD')],by.x='TC2','datestarteb')
  dim(df2)
  df2
}

# function to convert an Echo-integration by layer file in Movies .lay format to .csv format
#*****************************

EIlay2csv=function(Mfile){
  
  #Import columns to be filled
  #*****************************
  data(colonneMovies)
  nmi=colonneMovies
  nmi=as.character(nmi[,1])
  nmi=gsub('/','\\',nmi,fixed=TRUE)
  
  #Import example file
  #*****************************
  data(results_20120207_161208_lay)
  csvex=results_20120207_161208_lay
  Mcsv.base=matrix(rep(NA,dim(Mfile)[1]*dim(csvex)[2]),ncol=dim(csvex)[2])
  Mcsv.base=data.frame(Mcsv.base)
  names(Mcsv.base)=gsub('.','\\',as.character(names(csvex)),fixed=TRUE)
  
  #Fill DataAcquisition
  Mcsv.base[,'MOVIES_EILayer\\sndset\\channelName']=38000
  Mcsv.base[,'MOVIES_EILayer\\sndset\\absorptionCoef']=8.47  
  Mcsv.base[,'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity']=21.8
  Mcsv.base[,'MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity']=21.8
  Mcsv.base[,'MOVIES_EILayer\\sndset\\beamEquTwoWayAngle']=6.94
  Mcsv.base[,'MOVIES_EILayer\\sndset\\pulseduration']=1.024
  Mcsv.base[,'MOVIES_EILayer\\sndset\\beamGain']=25.27
  Mcsv.base[,'MOVIES_EILayer\\sndset\\beamSACorrection']=-0.529999
  Mcsv.base[,'MOVIES_EILayer\\sndset\\soundcelerity']='variable'
  Mcsv.base[,'MOVIES_EILayer\\sndset\\softChannelId']=47
  Mcsv.base[,'MOVIES_EILayer\\sndset\\transmissionPower']=2000
  
  #Fill DataProcessing
  Mcsv.base['MOVIES_EILayer']='2011/06/23 10:09:00.0000'
  Mcsv.base[,'MOVIES_EILayer\\cellset\\thresholdlow']=-60
  Mcsv.base[,'MOVIES_EILayer\\sndset\\beamEquTwoWayAngle']=-20.6
  Mcsv.base['MOVIES_EILayer\\cellset\\thresholdup']=0
  Mcsv.base[,'MOVIES_EILayer\\sndset\\soundcelerity']='variable'
  
  #Fill Esdu/data
  Mcsv.base[,'MOVIES_EILayer\\cellset\\lat']=Mfile$y
  Mcsv.base[,'MOVIES_EILayer\\cellset\\long']=Mfile$x
  Mcsv.base[,'MOVIES_EILayer\\shipnav\\depth']=Mfile$dpth
  Mcsv.base[,'MOVIES_EILayer\\cellset\\depthstart']=Mfile$sz
  Mcsv.base[,'MOVIES_EILayer\\cellset\\depthend']=Mfile$z
  #Mcsv.base[,'MOVIES_EILayer\\cellset\\dateend']=paste(format(Mfile$t,
  #    "%d/%m/%Y %H:%M:%OS"),'.0000',sep='')
  Mcsv.base[,'MOVIES_EILayer\\cellset\\dateend']=paste(format(Mfile$t),
                                                       '.0000',sep='')
  #Mcsv.base[,'MOVIES_EILayer\\cellset\\datestart']=paste(format(Mfile$st,
  #    "%d/%m/%Y %H:%M:%OS"),'.0000',sep='')
  Mcsv.base[,'MOVIES_EILayer\\cellset\\datestart']=paste(format(Mfile$st),
                                                         '.0000',sep='')
  Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']=Mfile$sA
  Mcsv.base[,'MOVIES_EILayer\\eilayer\\ni']=Mfile$Ni
  Mcsv.base[,'MOVIES_EILayer\\eilayer\\nt']=Mfile$Nt
  #Mcsv.base[,'MOVIES_EILayer\\cellset\\thresholdlow']=Mfile$lEIthr    
  #Mcsv.base['MOVIES_EILayer\\cellset\\thresholdup']=Mfile$uEIthr
  Mcsv.base['MOVIES_EILayer\\cellset\\cellnum']=Mfile$cellnum
  Mcsv.base['MOVIES_EILayer\\cellset\\celltype']=Mfile$cellType
  Mcsv.base['MOVIES_EILayer\\cellset\\area']=1
  #Mcsv.base['MOVIES_EILayer\\cellset\\volume']=Mcsv.base[,'MOVIES_EILayer\\cellset\\depthend']-
  #  Mcsv.base[,'MOVIES_EILayer\\cellset\\depthstart']
  Mcsv.base$dataQuality=Mfile$dataQuality
  
  #Select columns not entirely padded with NA
  nNAcols=colSums(apply(Mcsv.base,2,is.na))
  Mcsv.bases=Mcsv.base[,nNAcols!=dim(Mcsv.base)[1]]
  
  #Check movies vs. Echobase names 
  dim(Mcsv.base)
  head(Mcsv.base)
  nacol=apply(Mcsv.base[1,],2,is.na)
  Mcsv.bases=Mcsv.base[,!nacol]
  length(names(Mcsv.bases));length(nmi)
  #names not in echobase
  names(Mcsv.bases)[!names(Mcsv.bases)%in%nmi]
  #names not in Movies
  nmi[!nmi%in%names(Mcsv.bases)]
  
  Mcsv.base.sana=Mcsv.base[is.na(Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']),]
  head(Mcsv.base)
  dim(Mcsv.base)
  dim(Mcsv.base[is.na(Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']),])
  head(Mcsv.bases)
  Mcsv.bases
}


#Mcsv.bases.compi=Mcsv.bases.comps2.38

SounderParametersCorrection=function(Mcsv.bases.compi,CorrectChannelIdent=NULL,CorrectAbsorption=NULL,beamGainCor=NULL,
                                     beamSACor=NULL,powerCor=NULL,algAsensCor=NULL,atwAsensCor=NULL,areaCor=NULL,volCor=NULL,
                                     CorrectSoundCelerity=NULL){
  cat('Channel',unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\softChannelId']),'\n')
  if (!is.null(CorrectChannelIdent)){
    Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\softChannelId']=CorrectChannelIdent
    cat('Non unique acoustic instrument IDs detected in EI file, replaced by "CorrectChannelIdent"','\n')
  }  
  if (length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']))>1){
    if (!is.null(CorrectAbsorption)){
      if (CorrectAbsorption=='round'){
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']=as.numeric(
          as.character(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']))
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']=unique(round(
          Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef'],2))
        cat('Non unique absorption coefficients detected in EI file, changed to round(coeff,2)','\n')
      }else if(CorrectAbsorption=='frequency'){
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']=as.numeric(
          as.character(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']))
        lca=unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef'])
        nlca=table(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef'])
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']=as.numeric(names(nlca)[nlca==max(nlca)])
        cat('Non unique absorption coefficients detected in EI file, changed to most frequent coefficient','\n')
      }
    }else{
      stop('Non unique absorption coefficients detected in EI file','\n')
    }  
  }else{ 
    cat('Absorption coefficients OK','\n')
  }
   
  if (length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\pulseduration']))>1){
    #Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']=unique(round(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef'],2))
    stop('Non unique pulse durations detected in EI file, please check','\n')
  }
  if ((length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamGain']))>1)){
    if (!is.null(beamGainCor)){
      if (beamGainCor=='frequency'){
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamGain']=as.numeric(
          as.character(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamGain']))
        lca=unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamGain'])
        nlca=table(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamGain'])
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamGain']=as.numeric(names(nlca)[nlca==max(nlca)])
        cat('Non unique beam gain detected in EI file, changed to most frequent coefficient','\n')
      }else{
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamGain']=beamGainCor
        cat('Non unique beam gains detected in EI file, replaced by "beamGainCor"','\n')
      }
    }else{
      stop('Non unique beam gains detected in EI file, please check','\n')
    }
  }  
  if ((length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamSACorrection']))>1)|
      (T%in%is.na(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamSACorrection'])))){
    if (!is.null(beamSACor)){
      if (beamSACor=='frequency'){
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamSACorrection']=as.numeric(
          as.character(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamSACorrection']))
        lca=unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamSACorrection'])
        nlca=table(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamSACorrection'])
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamSACorrection']=as.numeric(names(nlca)[nlca==max(nlca)])
        cat('Non unique beam SAcorrection detected in EI file, changed to most frequent coefficient','\n')
      }else{
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamSACorrection']=beamSACor
        cat('Non unique or NA beamSACorrection detected in EI file, replaced by "beamSACor"','\n')
      }  
    }else{
      stop('Non unique or NA beamSACorrection detected in EI file, please check','\n')
    }
  }  
  if ((length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\transmissionPower']))>1)){
    if  (!is.null(powerCor)){
      Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\transmissionPower']=powerCor
      cat('Non unique transmission power detected in EI file, replaced by "powerCor"','\n')
    }else{
      stop('Non unique transmission power detected in EI file, please check','\n')        
    }
  }
  if (length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamEquTwoWayAngle']))>1){
    #Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef']=unique(round(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\absorptionCoef'],2))
    stop('Non unique beamEquTwoWayAngle detected in EI file, please check','\n')
  }
  if ((length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity']))>1)){
    if (!is.null(algAsensCor)){
      if (algAsensCor=='frequency'){
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity']=as.numeric(
          as.character(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity']))
        lca=unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity'])
        nlca=table(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity'])
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity']=as.numeric(names(nlca)[nlca==max(nlca)])
        cat('Non unique beam gain detected in EI file, changed to most frequent coefficient','\n')
      }else{
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity']=algAsensCor
        cat('Non unique beamAlongAngleSensitivity detected in EI file, replaced by "algAsensCor"','\n')
      }  
    }else{
      stop('Non unique beamAlongAngleSensitivity detected in EI file, please check','\n')
    }
  }
  if ((length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity']))>1)){
    if(!is.null(atwAsensCor)){
      if (atwAsensCor=='frequency'){
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity']=as.numeric(
          as.character(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity']))
        lca=unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity'])
        nlca=table(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity'])
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity']=as.numeric(names(nlca)[nlca==max(nlca)])
        cat('Non unique beam gain detected in EI file, changed to most frequent coefficient','\n')
      }else{
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity']=atwAsensCor
        cat('Non unique beamAthwartAngleSensitivity detected in EI file, replaced by "atwAsensCor"','\n')
      }  
    }else{
      stop('Non unique beamAlongAngleSensitivity detected in EI file, please check','\n')
    }
  }
  if ((length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\soundcelerity']))>1)){
    if(!is.null(CorrectSoundCelerity)){
      if (CorrectSoundCelerity=='frequency'){
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\soundcelerity']=as.numeric(
          as.character(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\soundcelerity']))
        lca=unique(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\soundcelerity'])
        nlca=table(Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\soundcelerity'])
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\soundcelerity']=as.numeric(names(nlca)[nlca==max(nlca)])
        cat('Non unique beam gain detected in EI file, changed to most frequent coefficient','\n')
      }else{
        Mcsv.bases.compi[,'MOVIES_EILayer\\sndset\\soundcelerity']=atwAsensCor
        cat('Non unique soundcelerity detected in EI file, replaced by "atwAsensCor"','\n')
      }  
    }else{
      stop('Non unique beamAlongAngleSensitivity detected in EI file, please check','\n')
    }
  }
  # if ((length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\cellset\\area']))>1)|
  #     (unique(Mcsv.bases.compi[,'MOVIES_EILayer\\cellset\\area'])==-1)){
  #   if (!is.null(areaCor)){
  #     Mcsv.bases.compi[,'MOVIES_EILayer\\cellset\\area']=areaCor
  #     cat('Cell areas in EI file replaced by "areaCor"','\n')
  #   }else{
  #     stop('Cell areas non unique or invalid in EI file, please check','\n')            
  #   }
  # }
  # if ((length(unique(Mcsv.bases.compi[,'MOVIES_EILayer\\cellset\\volume']))>1)|
  #     (unique(Mcsv.bases.compi[,'MOVIES_EILayer\\cellset\\volume'])==-1)){
  #   if (!is.null(volCor)){
  #     Mcsv.bases.compi[,'MOVIES_EILayer\\cellset\\volume']=volCor
  #     cat('Cell volumes in EI file replaced by "volumeCor"','\n')
  #   }else{
  #     stop('Cell volumes in EI file missing or invalid, please check','\n')
  #   }
  # }
  Mcsv.bases.compi
}

formatTime4Echobase=function(Mcsv.bases.comp,milli.correct=TRUE,fMovies='M3D'){
  if (milli.correct){
    if (fMovies=='Mplus2012'){
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],
                        "%Y-%d-%m %H:%M:%OS"),tz="GMT",
               "%Y-%m-%d %H:%M:%S")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        paste(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],".0000",sep='')
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],
                        "%Y-%d-%m %H:%M:%OS"),tz="GMT",
               "%Y-%m-%d %H:%M:%S")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        paste(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],".0000",sep='')
    }else if (fMovies=='M3D'){
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],
                        "%Y-%m-%d %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%S")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        paste(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],".0000",sep='')
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],
                        "%Y-%m-%d %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%S")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        paste(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],".0000",sep='')
    }else if (fMovies=='Mplus2014'){
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],
                        "%Y-%d-%m %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%S")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        paste(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],".0000",sep='')
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],
                        "%Y-%m-%d %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%S")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        paste(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],".0000",sep='')
    }
  }else{
    if (fMovies=='Mplus2012'){
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],
                        "%Y-%d-%m %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%OS4")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        format(strptime(as.character(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']),
                        "%Y-%d-%m %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%OS4")
    }else if (fMovies=='M3D'){
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],
                        "%Y-%m-%d %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%OS4")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        format(strptime(as.character(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']),
                        "%Y-%m-%d %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%OS4")
    }else if (fMovies=='Mplus2014'){
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\datestart'],
                        "%Y-%d-%m %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%OS4")
      Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']=
        format(strptime(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],
                        "%Y-%m-%d %H:%M:%OS",tz="GMT"),
               "%Y-%m-%d %H:%M:%OS4")
    }
  }
  cat('Date/times formatted in EI file','\n')
  if (T%in%'NA.0000'%in%(unique(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend']))){
    stop('NA in dateend')
  }
  Mcsv.bases.comp
}

convert2EBlay=function(Mcsv.base,validChannels,neg.nasc.remove,
                       CorrectChannelIdent,CorrectAbsorption,beamGainCor,
                       beamSACor,powerCor,algAsensCor,atwAsensCor,
                       CorrectSoundCelerity,areaCor,volCor,
                       milli.correct,fMovies='M3D',Transectfs,mantot){
  
  # Load M3D column names 
  data("csvM3DVsMplus")
  Mcsv.names=csvM3DVsMplus
  head(Mcsv.names)
  unique(Mcsv.names$layNamesM3D)
  Mcsv.names[Mcsv.names$layNamesM3D%in%
               c('MOVIES_EILayer\\sndset\\bandWidth',
                 'MOVIES_EILayer\\sndset\\acousticFrequency'),
             'EchobaseInput']='(Float) //O'
  if (names(Mcsv.base)[1]=='type'){
    nnames=as.character(Mcsv.names$Mplus2012)
    nnames[Mcsv.names$Mplus2012EchobaseCor!='']=as.character(Mcsv.names[Mcsv.names$Mplus2012EchobaseCor!='','Mplus2012EchobaseCor'])
    nnames2=nnames[nnames!='']
    names(Mcsv.base)[2:(length(nnames2)-1)]=as.character(nnames2[-c(length(nnames2)-1,length(nnames2))])
    Mcsv.base=Mcsv.base[,1:(length(nnames2)-1)]
    head(Mcsv.base)
  }
  names(Mcsv.base)=gsub('.','\\',as.character(names(Mcsv.base)),fixed=TRUE)  
  Mcsv.base=Mcsv.base[,names(Mcsv.base)!='X']
  # correct numeric columns formats
  #names(Mcsv.base)
  Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']=as.numeric(
    as.character(Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']))
  Mcsv.base[,'MOVIES_EILayer\\cellset\\long']=as.numeric(
    as.character(Mcsv.base[,'MOVIES_EILayer\\cellset\\long']))
  Mcsv.base[,c('MOVIES_EILayer\\cellset\\long')]=as.numeric(as.character(
    Mcsv.base[,c('MOVIES_EILayer\\cellset\\long')]))
  Mcsv.base[,c('MOVIES_EILayer\\cellset\\lat')]=as.numeric(as.character(
    Mcsv.base[,c('MOVIES_EILayer\\cellset\\lat')]))
  Mcsv.base[,c('MOVIES_EILayer\\cellset\\volume')]=as.numeric(as.character(
    Mcsv.base[,c('MOVIES_EILayer\\cellset\\volume')]))
  Mcsv.base[,c('MOVIES_EILayer\\cellset\\surface')]=as.numeric(as.character(
    Mcsv.base[,c('MOVIES_EILayer\\cellset\\surface')]))
  Mcsv.base[,c('MOVIES_EILayer\\cellset\\sa')]=as.numeric(as.character(
    Mcsv.base[,c('MOVIES_EILayer\\eilayer\\sa')]))
  Mcsv.base[,c('MOVIES_EILayer\\eilayer\\sv')]=as.numeric(as.character(
    Mcsv.base[,c('MOVIES_EILayer\\eilayer\\sv')]))
  Mcsv.base[,c('MOVIES_EILayer\\eilayer\\ni')]=as.numeric(as.character(
    Mcsv.base[,c('MOVIES_EILayer\\eilayer\\ni')]))
  Mcsv.base[,c('MOVIES_EILayer\\eilayer\\nt')]=as.numeric(as.character(
    Mcsv.base[,c('MOVIES_EILayer\\eilayer\\nt')]))
  if ('MOVIES_EILayer\\boterr\\sa'%in%names(Mcsv.base)){
    Mcsv.base[,c('MOVIES_EILayer\\boterr\\sa')]=as.numeric(as.character(
      Mcsv.base[,c('MOVIES_EILayer\\boterr\\sa')]))  
  }else{
    Mcsv.base[,c('MOVIES_EILayer\\boterr\\sa')]=0
  }
  if ('MOVIES_EILayer\\boterr\\ni'%in%names(Mcsv.base)){
    Mcsv.base[,c('MOVIES_EILayer\\boterr\\ni')]=as.numeric(as.character(
      Mcsv.base[,c('MOVIES_EILayer\\boterr\\ni')]))
  }else{
    Mcsv.base[,c('MOVIES_EILayer\\boterr\\ni')]=0
  }

  # adds data quality ----------
  # import lay files in csv format -----
  Mcsv.base$dataQuality=1
  
  #Check and correct export file ----------
  # ************************
  # Select valid channels 
  # ************************
  head(Mcsv.base)
  dim(Mcsv.base)
  Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\sndset\\softChannelId']==1,]
  table(Mcsv.base[,'MOVIES_EILayer\\sndset\\softChannelId'],
        Mcsv.base[,'MOVIES_EILayer\\cellset\\cellnum'])
  unique(Mcsv.base[,'MOVIES_EILayer\\sndset\\softChannelId'])
  Mcsv.base=Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\sndset\\softChannelId']%in%
                        validChannels$id,]
  Mcsv.base2=Mcsv.base[!Mcsv.base[,'MOVIES_EILayer\\sndset\\softChannelId']%in%
                         validChannels$id,]
  if (dim(Mcsv.base2)[1]>0) stop('Invalid channel IDs found in ESDUs',
                                 unique(Mcsv.base2[,'MOVIES_EILayer\\cellset\\dateend']))
  unique(Mcsv.base2[,'MOVIES_EILayer\\sndset\\softChannelId'])
  table(Mcsv.base[,'MOVIES_EILayer\\sndset\\softChannelId'],
        Mcsv.base[,'MOVIES_EILayer\\cellset\\cellnum'])
  dim(Mcsv.base)
  
  #Replace or remove negative and NA NASC (under the bottom) by NA 
  # -9999999.9 imported either as NA or -1e+07 
  # ************************
  dim(Mcsv.base)
  if (neg.nasc.remove){
    # Mcsv.base=rbind(Mcsv.base[!is.na(Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa'])&
    #                             Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']>=0,],
    #                 Mcsv.base[is.na(Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']),])
    Mcsv.base=Mcsv.base[!is.na(Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa'])&
                          Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']>=0,]
    #Mcsv.base.na=Mcsv.base[is.na(Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']),]
    # Mcsv.base.neg=Mcsv.base[!is.na(Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa'])&
    #                             Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']<=0,]
    dim(Mcsv.base)
  }else{
    #Mcsv.base.pb=Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']<0,]
    Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']<0,'dataQuality']=5
    Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\eilayer\\sa']<0,'MOVIES_EILayer\\eilayer\\sa']=NA
    cat('Negative NASC of (under the seabed) cells replaced by NA','\n')
  }
  
  #correct column names 
  # ************************
  names(Mcsv.base)[names(Mcsv.base)=='MOVIES_EILayer\\cellset\\surface']=
    'MOVIES_EILayer\\cellset\\area'
  names(Mcsv.base)[names(Mcsv.base)=='MOVIES_EILayer\\sndset\\svgain']=
    'MOVIES_EILayer\\sndset\\beamSACorrection'
  
  #correct celltype==4 positions 
  # ************************    
  # plot(Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\cellset\\cellnum']==1,
  #                c('MOVIES_EILayer\\cellset\\long')],
  #      Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\cellset\\cellnum']==1,
  #      c('MOVIES_EILayer\\cellset\\lat')])
  if (sum(c(-200,-100)%in%Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\cellset\\celltype']==4,
                                    c('MOVIES_EILayer\\cellset\\long')])>0){
    dim(Mcsv.base)
    Mcsv.base=xy.ESDU.correct(Mcsv.base) # in scrutinising_functions.r
    dim(Mcsv.base)
    names(Mcsv.base)
    Mcsv.base[is.na(Mcsv.base[,"MOVIES_EILayer\\cellset\\depthend"]),]
  }
  # plot(Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\cellset\\celltype']==4,
  #                c('MOVIES_EILayer\\cellset\\long')],
  #      Mcsv.base[Mcsv.base[,'MOVIES_EILayer\\cellset\\celltype']==4,
  #                c('MOVIES_EILayer\\cellset\\lat')])
  
  # correct column names according to Movies file format 
  # ************************
  #Adds M3D extra columns
  # ************************
  #Load M3D column names
  data("csvM3DVsMplus")
  Mcsv.names=csvM3DVsMplus
  Mcsv.names$type=substr(as.character(Mcsv.names$Echobase),16,21)
  Mcsv.names[Mcsv.names$layNamesM3D%in%
               c('MOVIES_EILayer\\sndset\\bandWidth',
                 'MOVIES_EILayer\\sndset\\acousticFrequency'),
             'EchobaseInput']='(Float) //O'
  names(Mcsv.base)
  head(Mcsv.names)
  #columns needed for Echobase
  ecn=Mcsv.names$Echobase[substr(Mcsv.names$EchobaseInput,1,14)!='(colonne ignor']
  ecn[!ecn%in%names(Mcsv.base)]
  if (dim(Mcsv.base)[2]%in%c(61)){
    fMovies='Mplus2012'
    Mcsv.basef=Mcsv.base
    Mcsv.basef[,'MOVIES_EILayer\\sndset\\beamSACorrection']=-0.59
    #correct required column names
    cn=Mcsv.names[Mcsv.names$Mplus2012EchobaseCor!='',]
    names(Mcsv.basef)[names(Mcsv.basef)%in%cn$Mplus2012]=as.character(cn$Mplus2012EchobaseCor)
  }else if (dim(Mcsv.base)[2]==55){
    fMovies='MplusOld'
  }else if (dim(Mcsv.base)[2]==60){
    fMovies='Mplus2014'
    Mcsv.basef=Mcsv.base
    Mcsv.basef[,'MOVIES_EILayer\\sndset\\beamSACorrection']=-0.59
    #correct required column names
    cn=Mcsv.names[Mcsv.names$Mplus2012EchobaseCor!='',]
    names(Mcsv.basef)[names(Mcsv.basef)%in%cn$Mplus2012]=as.character(cn$Mplus2012EchobaseCor)
  }else{
    Mcsv.basef=Mcsv.base
    fMovies='M3D'
  }
  rm('Mcsv.base')
  Mcsv.basefs=Mcsv.basef[,names(Mcsv.basef)%in%Mcsv.names$Echobase]
  rm('Mcsv.basef')
  if (dim(Mcsv.basefs)[2]<length(Mcsv.names$Echobase)){
    dim(Mcsv.basefs)
    #adds missing columns padded with NAs
    names.notinMplus=Mcsv.names$Echobase[!Mcsv.names$Echobase%in%names(Mcsv.basefs)]
    dfb=matrix(rep(NA,length(Mcsv.names$Echobase)*dim(Mcsv.basefs)[1]),
               ncol=length(Mcsv.names$Echobase))
    dfb=data.frame(dfb)
    names(dfb)=Mcsv.names$Echobase
    dfbs=dfb[,names(dfb)%in%names.notinMplus]
    dim(dfbs)
    head(dfbs)
    
    Mcsv.bases.comp=cbind(Mcsv.basefs,dfbs)
    dim(Mcsv.basefs)
    dim(Mcsv.bases.comp)
    length(Mcsv.names$Echobase)
    Mcsv.bases.comp=Mcsv.bases.comp[,as.character(Mcsv.names$Echobase)]
    rm('dfb')
  }else{
    Mcsv.bases.comp=Mcsv.basefs
  }
  rm('Mcsv.basefs')
  #rm('Mcsv.base.pb')
  gc()
  names(Mcsv.bases.comp)
  names.notinMplus=Mcsv.names$Echobase[!Mcsv.names$Echobase%in%names(Mcsv.bases.comp)]
  if (length(names.notinMplus)>0) stop ('Missing columns in EI export file')
  
  #Check and correct Echobase fields 
  # ************************
  # if M3D files or multiple EIlay in M+ files, EIlay data from all channels are imported, 
  # just check
  if ((length(unique(Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId']))>1)){
    Nc=length(unique(Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId']))
    # check if parameters used in data processing are unique
    lparn=as.character(Mcsv.names[Mcsv.names$EchobaseInput!='(colonne ignorée)'&Mcsv.names$type=='sndset','Echobase'])
    for (i in 2:length(lparn)){
      npari=aggregate(Mcsv.bases.comp[,lparn[i]],
                      list(Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId'],Mcsv.bases.comp[,lparn[i]]),length)
      names(npari)=c('MOVIES_EILayer\\sndset\\softChannelId',lparn[i],'N')
      lpari=unique(Mcsv.bases.comp[,c('MOVIES_EILayer\\sndset\\softChannelId',lparn[i])])
      lnpari=merge(lpari,npari)
      lnpari=lnpari[order(lnpari[,1]),]
      lnpari$param=lparn[i]
      names(lnpari)[2]='val'
      lnpari$val=as.numeric(as.character(lnpari$val))
      if (i==2){
        lnpar=lnpari
      }else{
        lnpar=rbind(lnpar,lnpari)
      }
    }
    lnpar.mult=aggregate(lnpar$param,list(channel=lnpar[,'MOVIES_EILayer\\sndset\\softChannelId'],param=lnpar$param),length)
    names(lnpar.mult)[3]='Nval'
    lnpar.mults=lnpar.mult[lnpar.mult$channel<60&lnpar.mult$Nval>1,]
    if (dim(lnpar.mults)[1]>0){
      # correct monobeam channels parameters
      # ************************
      lcha=validChannels[validChannels$type=='mono','id']
      for (i in 1:length(lcha)){
        dfi=Mcsv.bases.comp[Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId']==lcha[i],]
        if (dim(dfi)[1]>0){
          dfi=SounderParametersCorrection(Mcsv.bases.compi=dfi,CorrectChannelIdent=CorrectChannelIdent,
                                          CorrectAbsorption=CorrectAbsorption,beamGainCor=beamGainCor,
                                          beamSACor=beamSACor,powerCor=powerCor,algAsensCor=algAsensCor,
                                          atwAsensCor=atwAsensCor,CorrectSoundCelerity=CorrectSoundCelerity,
                                          areaCor=areaCor,volCor=volCor)
          if (i==1){
            dfidb=dfi
          }else{
            dfidb=rbind(dfidb,dfi)
          }
        }
      }
      dim(dfidb)
      #unique(dfi[,'MOVIES_EILayer\\sndset\\absorptionCoef'])
      #unique(dfi[,'MOVIES_EILayer\\sndset\\soundcelerity'])
      Mcsv.bases.comp[,c('MOVIES_EILayer\\sndset\\absorptionCoef','MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity',
                         'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity','MOVIES_EILayer\\sndset\\beamGain',
                         'MOVIES_EILayer\\sndset\\beamSACorrection','MOVIES_EILayer\\sndset\\soundcelerity')]=apply(
                           Mcsv.bases.comp[,c('MOVIES_EILayer\\sndset\\absorptionCoef','MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity',
                                              'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity','MOVIES_EILayer\\sndset\\beamGain',
                                              'MOVIES_EILayer\\sndset\\beamSACorrection','MOVIES_EILayer\\sndset\\soundcelerity')],
                           2,function(x){as.numeric(as.character(x))})
      Mcsv.bases.comp=rbind(Mcsv.bases.comp[!Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId']%in%lcha,],
                            dfidb)
      # unique(Mcsv.bases.comp[,c('MOVIES_EILayer\\sndset\\absorptionCoef','MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity',
      #                                  'MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity','MOVIES_EILayer\\sndset\\beamGain',
      #                                  'MOVIES_EILayer\\sndset\\beamSACorrection')])
      #stop('Non unique values detected in EI parameters:',paste(lnpar.mults$channel,lnpar.mults$param,'\n')) 
    }
    # else if M+ files, EIlay data of 1 channel is imported, and checked  
  }else if ((length(unique(Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId']))==1)){
    Mcsv.bases.comp=SounderParametersCorrection(Mcsv.bases.compi=Mcsv.bases.comp,
                                                CorrectChannelIdent=CorrectChannelIdent,
                                                CorrectAbsorption=CorrectAbsorption,beamGainCor=beamGainCor,
                                                beamSACor=beamSACor,powerCor=powerCor,algAsensCor=algAsensCor,
                                                atwAsensCor=atwAsensCor,CorrectSoundCelerity=CorrectSoundCelerity,
                                                areaCor=areaCor,volCor=volCor)
  }  
  
  # Format date/times --------------
  # ************************
  #unique(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'])
  Mcsv.bases.comp=formatTime4Echobase(Mcsv.bases.comp=Mcsv.bases.comp,
                                      milli.correct=milli.correct,
                                      fMovies=fMovies)
  names(Mcsv.bases.comp)
  #Mcsv.bases.comp[,"MOVIES_EILayer\\cellset\\datestart"]
  # Check and correct cell volume and area
  # ************************
  # Mcsv.bases.comps2[Mcsv.bases.comps2[,c('MOVIES_EILayer\\cellset\\volume')]<0,'MOVIES_EILayer\\cellset\\volume']=0
  # Mcsv.bases.comps2[Mcsv.bases.comps2[,c('MOVIES_EILayer\\cellset\\area')]<0,'MOVIES_EILayer\\cellset\\area']=0
  Mcsv.bases.comp[Mcsv.bases.comp[,"MOVIES_EILayer\\cellset\\cellnum"]%in%
                    c(15,16),
                  "MOVIES_EILayer\\cellset\\area"]=-1
  Mcsv.bases.comp[Mcsv.bases.comp[,"MOVIES_EILayer\\cellset\\cellnum"]%in%
                    c(15,16),
                  "MOVIES_EILayer\\cellset\\volume"]=-1

  # Look for NA in Echobase required columns 
  # ************************
  ecns=as.character(ecn[!ecn%in%c('MOVIES_EILayer\\cellset\\volume','MOVIES_EILayer\\eilayer\\sa')])
  na.ecn=names(Mcsv.bases.comp[,ecns])[apply(Mcsv.bases.comp[,ecns],2,function(x) T%in%is.na(x))]
  if (length(na.ecn)>0) stop ('NA found in EI data field required by Echobase, please check.')
  
  # Select 38kHz frequency for checks 
  # ************************
  if (length(unique(Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId']))>1){
    if (length(validChannels$id)==1){
      cchannel=validChannels$id
      Mcsv.bases.comp38=Mcsv.bases.comp[Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel,]
    }else{
      cchannel=validChannels[validChannels$sel,'id']
      Mcsv.bases.comp38=Mcsv.bases.comp[Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel,]
    }
  }else{
    Mcsv.bases.comp38=Mcsv.bases.comp
    cchannel=validChannels[validChannels$sel,'id']
    unique(Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId'])
  }
  
  # # Plot all esdus 
  # # ************************
  # Mcsv.base38.4=Mcsv.bases.comp38[Mcsv.bases.comp38[,'MOVIES_EILayer\\cellset\\celltype']==4,]
  # plot(Mcsv.base38.4[,'MOVIES_EILayer\\cellset\\long'],
  #      Mcsv.base38.4[,'MOVIES_EILayer\\cellset\\lat'],
  #      cex=log(Mcsv.base38.4[,'MOVIES_EILayer\\eilayer\\sa']+1)/10,pch=16)
  # coast()
  # dim(Mcsv.base38.4)
  # names(Mcsv.base38.4)
  # length(sort(unique(Mcsv.base38.4[,'MOVIES_EILayer\\cellset\\dateend'])))
  
  # Check that all esdus/channel/cellnum combinations are unique 
  # ************************
  #!!!!!!!!!!!!!!!!ESDUs IDs are "dateend" in Echobase!!!!!!!!!!!!!!!!!!!!!!!!
  t0=table(Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId'],
           Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\cellnum'])
  
  Mcsv.bases.comp$id=paste(Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],
                           Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\cellnum'],
                           Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId'])
  length(Mcsv.bases.comp$id);length(unique(Mcsv.bases.comp$id))
  Mcsv.bases.comp$id2=paste(Mcsv.bases.comp[,'MOVIES_EILayer'],
                            Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\dateend'],
                            Mcsv.bases.comp[,'MOVIES_EILayer\\cellset\\cellnum'],
                            Mcsv.bases.comp[,'MOVIES_EILayer\\sndset\\softChannelId'])
  length(Mcsv.bases.comp$id2);length(unique(Mcsv.bases.comp$id2))
  
  idnd=Mcsv.bases.comp$id[!duplicated(Mcsv.bases.comp$id)]
  length(idnd)
  Mcsv.bases.comp.nd=Mcsv.bases.comp[!duplicated(Mcsv.bases.comp$id),]
  Mcsv.bases.comp.d=Mcsv.bases.comp[duplicated(Mcsv.bases.comp$id),]
  t1=table(Mcsv.bases.comp.nd[,'MOVIES_EILayer\\sndset\\softChannelId'],
           Mcsv.bases.comp.nd[,'MOVIES_EILayer\\cellset\\cellnum'])
  #     sum(Mcsv.bases.comp.d$id%in%Mcsv.bases.comp.nd$id)
  #     sum(unique(Mcsv.bases.comp.d[,'MOVIES_EILayer\\cellset\\dateend'])%in%
  #           unique(Mcsv.bases.comp.nd[,'MOVIES_EILayer\\cellset\\dateend']))
  #     
  #     Mcsv.bases.comps=Mcsv.bases.comp.nd
  #     #list of duplicated cells
  #     gMcsv.base=Mcsv.bases.comp[duplicated(Mcsv.bases.comp$id),
  #                                c('MOVIES_EILayer\\cellset\\dateend','MOVIES_EILayer','id','id2')]
  #     #Dataframe with duplicated cells
  #     dMcsv.bases=Mcsv.bases.comp[Mcsv.bases.comp$id%in%gMcsv.base[,'id'],]
  #     unique(dMcsv.bases[,c('MOVIES_EILayer','MOVIES_EILayer\\cellset\\dateend')])
  #     cat('Esdu duplicated:',
  #         as.character(unique(gMcsv.base[,'MOVIES_EILayer\\cellset\\dateend'])),
  #         '; latest EI selected','\n')  
  
  # lay file without duplicated ESDUs: 
  # ************************
  #     Mcsv.bases.comps=rbind(Mcsv.bases.comp[!Mcsv.bases.comp[,'MOVIES_EILayer']%in%
  #                                              dMcsv.base38.4s[,'MOVIES_EILayer'],],
  #                           Mcsv.bases.comp[Mcsv.bases.comp[,'MOVIES_EILayer']%in%
  #                                             gMcsv.base38.4[,'MOVIES_EILayer'],])
  Mcsv.bases.comps=Mcsv.bases.comp.nd
  dim(Mcsv.bases.comp)
  dim(Mcsv.bases.comps)
  id=paste(Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\dateend'],
           Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\cellnum'],
           Mcsv.bases.comps[,'MOVIES_EILayer\\sndset\\softChannelId'])
  idd=id[duplicated(id)]
  if (length(idd)>0) stop("Esdu duplicated",idd)
  
  #Remove ESDUs with null length --------------
  # ************************
  Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\datestart']=
    as.character(Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\datestart'])
  Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\dateend']=
    as.character(Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\dateend'])
  nlesdus=unique(Mcsv.bases.comps[Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\datestart']==
                                    Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\dateend'],
                                  'MOVIES_EILayer\\cellset\\dateend'])
  Mcsv.bases.compss=Mcsv.bases.comps[!Mcsv.bases.comps[,'MOVIES_EILayer\\cellset\\dateend']%in%
                                       nlesdus,]
  dim(Mcsv.bases.comps)
  dim(Mcsv.bases.compss)
  length(unique(Mcsv.bases.compss[,'MOVIES_EILayer\\cellset\\dateend']))
  rm('Mcsv.bases.comps')
  rm('Mcsv.bases.comp.nd')
  gc()
  
  #Check that all EI esdus are in transects  
  # ************************
  head(Transectfs)
  act=Transectfs[Transectfs$transectAbstract=='ACOU',]
  for (i in 1:dim(act)[1]){
    tsei=c(act$timeCoverageStart[i],act$timeCoverageEnd[i])
    if (i==1){
      tse=tsei
    }else{
      tse=c(tse,tsei)
    } 
  }
  head(act)
  act=act[order(act$timeCoverageStart),]
  tsef=strptime(tse,"%Y-%m-%d %H:%M:%OS",tz="GMT")
  tsc=strptime(act$timeCoverageStart,"%Y-%m-%d %H:%M:%OS",tz="GMT")
  tec=strptime(act$timeCoverageEnd,"%Y-%m-%d %H:%M:%OS",tz="GMT")
  act$cst=cut(tsc,breaks=tsef)
  act$cet=cut(tec,breaks=tsef)
  etsc=strptime(Mcsv.bases.compss[Mcsv.bases.compss[,'MOVIES_EILayer\\cellset\\celltype']==4,
                                  'MOVIES_EILayer\\cellset\\datestart'],"%Y-%m-%d %H:%M:%OS",
                tz="GMT")
  etec=strptime(Mcsv.bases.compss[Mcsv.bases.compss[,'MOVIES_EILayer\\cellset\\celltype']==4,
                                  'MOVIES_EILayer\\cellset\\dateend'],"%Y-%m-%d %H:%M:%OS",
                tz="GMT")
  ecst=cut(etsc,breaks=tsef)
  cstdf=data.frame(etsc,ecst,esun=Mcsv.bases.compss[Mcsv.bases.compss[,'MOVIES_EILayer\\cellset\\celltype']==4,
                                                    'MOVIES_EILayer\\cellset\\datestart'])
  enit=cstdf[is.na(cstdf$ecst),]
  if (dim(enit)[1]>0){
    stop('ESDUs without transects: ',paste(as.character(enit$esun),collapse='\n'))
  }else{
    cat('All ESDUs have transects','\n')
  }
  
  # Compare sum of surface layers Sa to esdu total Sa
  # ************************
  Mcsv.base0=Mcsv.bases.compss[Mcsv.bases.compss[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel&
                                 Mcsv.bases.compss[,'MOVIES_EILayer\\cellset\\celltype']==0,]
  Mcsv.base4=Mcsv.bases.compss[Mcsv.bases.compss[,'MOVIES_EILayer\\sndset\\softChannelId']==cchannel&
                                 Mcsv.bases.compss[,'MOVIES_EILayer\\cellset\\celltype']==4,]
  length(unique(Mcsv.base4[,'MOVIES_EILayer\\cellset\\dateend']))
  
  Mcsv.base0a=aggregate(Mcsv.base0[,'MOVIES_EILayer\\eilayer\\sa'],
                        list(Mcsv.base0[,'MOVIES_EILayer\\cellset\\datestart']),sum)
  names(Mcsv.base0a)=c('st','sa')
  summary(Mcsv.base0a$sa)
  summary(Mcsv.base4[,'MOVIES_EILayer\\eilayer\\sa'])
  Mcsv.base0a=merge(Mcsv.base0a,Mcsv.base4,
                    by.x='st',
                    by.y='MOVIES_EILayer\\cellset\\datestart')
  dim(Mcsv.base0a);dim(Mcsv.base4)
  summary(Mcsv.base0a$sa-Mcsv.base0a[,'MOVIES_EILayer\\eilayer\\sa'])
  
  # Extract usefull columns 
  # ************************
  Mcsv.bases.comps2=Mcsv.bases.compss[,as.character(ecn)]
  head(Mcsv.bases.comps2)
  
  # Pad "MOVIES_EILayer\\cellset\\depthend" with NA to allow for EB internal data check --------
  # ************************
  names(Mcsv.bases.comps2)
  Mcsv.bases.comps2[Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\celltype"]==4,
                    "MOVIES_EILayer\\cellset\\depthend"]=NA
  #Mcsv.bases.comps2[is.na(Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\depthend"]),]
  
  # Check and correct max depth 
  # ************************
  Mcsv.bases.comps2[,c('MOVIES_EILayer\\shipnav\\depth')]=as.numeric(as.character(
    Mcsv.bases.comps2[,c('MOVIES_EILayer\\shipnav\\depth')]))
  udepths=unique(Mcsv.bases.comps2[Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\cellnum"]%in%c(0,16)&
                                     Mcsv.bases.comps2[,"MOVIES_EILayer\\sndset\\softChannelId"]%in%46:51,
                                   c('MOVIES_EILayer\\cellset\\dateend','MOVIES_EILayer\\shipnav\\depth',
                                     'MOVIES_EILayer\\sndset\\softChannelId')])
  udepthsa=aggregate(udepths[,c('MOVIES_EILayer\\shipnav\\depth')],
                     list(udepths[,c('MOVIES_EILayer\\cellset\\dateend')],
                          udepths[,c('MOVIES_EILayer\\sndset\\softChannelId')]),length)
  names(udepthsa)=c('esdu','f','x')
  udepthsas=udepthsa[udepthsa$x>1,]
  udepthsam=aggregate(udepths[,c('MOVIES_EILayer\\shipnav\\depth')],
                      list(udepths[,c('MOVIES_EILayer\\cellset\\dateend')],
                           udepths[,c('MOVIES_EILayer\\sndset\\softChannelId')]),max)
  names(udepthsam)=c('esdu','f','mx')
  udepthsams=udepthsam[udepthsa$x>1,]
  if (length(udepthsas$x)>0){
    for (i in 1:length(udepthsas$x)){
      Mcsv.bases.comps2[Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\dateend"]==udepthsas[i,'esdu']&
                          Mcsv.bases.comps2[,"MOVIES_EILayer\\sndset\\softChannelId"]==udepthsas[i,'f'],
                        c('MOVIES_EILayer\\shipnav\\depth')]=udepthsams[i,'mx']
    }
  }
  
  # Put the same date/time in "MOVIES_EILayer" for consistency ---------
  # ************************
  names(Mcsv.bases.comps2)
  Mcsv.bases.comps2[,"MOVIES_EILayer"]=Mcsv.bases.comps2[1,"MOVIES_EILayer"]
  #Mcsv.bases.comps2[is.na(Mcsv.bases.comps2[,"MOVIES_EILayer\\cellset\\depthend"]),]
  
  #Adds label column for transect ID ---------
  # ************************
  head(mantot)
  # correct mantot freq
  mantotc=mantot
  if (length(unique(mantotc$softChannelId))>1){
    tfm=table(mantotc$softChannelId)
    mantotc$softChannelId=as.numeric(names(tfm)[tfm==max(tfm)])
    cat('mantot frequency changed to most frequent one','\n')
  }
  if (length(unique(mantotc$channelName))>1){
    mantotc$channelName=as.numeric(names(table(mantotc$channelName)))
    cat('NA in channel name changed to most frequent one in mantot','\n')
  }
  mantotc$diststart=as.numeric(as.character(mantotc$diststart))
  mantotc$dateendss=paste(mantotc$dateend,'0000',sep='.')
  head(mantotc)
  Mcsv.bases.comps2=merge(Mcsv.bases.comps2,
                          unique(mantotc[,c('dateendss','RAD')]),
                          by.x='MOVIES_EILayer\\cellset\\dateend',
                          by.y='dateendss',all.x=TRUE)
  names(Mcsv.bases.comps2)[names(Mcsv.bases.comps2)=='RAD']='label'
  Mcsv.bases.comps2$label=as.character(Mcsv.bases.comps2$label)
  Mcsv.bases.comps2[is.na(Mcsv.bases.comps2$label),'label']=0
  Mcsv.bases.comps2$label=gsub('R0','',Mcsv.bases.comps2$label)
  Mcsv.bases.comps2$label=gsub('R','',Mcsv.bases.comps2$label)
  Mcsv.bases.comps2=Mcsv.bases.comps2[order(Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\dateend'],
                                            Mcsv.bases.comps2[,'MOVIES_EILayer\\cellset\\cellnum']),]
  head(Mcsv.bases.comps2)
  
  # convert numeric columns 
  #********************
  names(Mcsv.bases.comps2)
  Mcsv.bases.comps2[,!names(Mcsv.bases.comps2)%in%c(
    "MOVIES_EILayer\\cellset\\dateend",
                       "MOVIES_EILayer",
                       "MOVIES_EILayer\\cellset\\datestart")]=
    apply(Mcsv.bases.comps2[,!names(Mcsv.bases.comps2)%in%c(
      "MOVIES_EILayer\\cellset\\dateend",
      "MOVIES_EILayer",
      "MOVIES_EILayer\\cellset\\datestart")],2,as.character)
  Mcsv.bases.comps2[,!names(Mcsv.bases.comps2)%in%c(
    "MOVIES_EILayer\\cellset\\dateend",
    "MOVIES_EILayer",
    "MOVIES_EILayer\\cellset\\datestart")]=
    apply(Mcsv.bases.comps2[,!names(Mcsv.bases.comps2)%in%c(
      "MOVIES_EILayer\\cellset\\dateend",
      "MOVIES_EILayer",
      "MOVIES_EILayer\\cellset\\datestart")],2,as.numeric)
  head(Mcsv.bases.comps2)
  unique(Mcsv.bases.comps2$label)
  Mcsv.bases.comps2
}  



ICESxml.import=function(path.Biotic.xml=NULL,path.Acoustic.xml=NULL){
  
  library(xml2)
  library(plyr)
  
  if (!is.null(path.Biotic.xml)){
    # 1. import biotic ------
    # *************************
    # 1.1. biotic metadata ----
    biotic.xml <- read_xml(path.Biotic.xml)
    biotic.list=as_list(biotic.xml)
    biotic.list=biotic.list[[1]]
    names(biotic.list)
    length(biotic.list)
    biotic.list.cruise=biotic.list$Cruise
    names(biotic.list.cruise)
    biotic.cruise.metadata = data.frame(matrix(vector(), 0, 7),stringsAsFactors=F)
    names(biotic.cruise.metadata)=names(biotic.list.cruise)[1:7]
    for (k in 1:7){
      if (!is.null(attr(biotic.list.cruise[[k]],"IDREF"))){
        biotic.cruise.metadata[1,k]=attr(biotic.list.cruise[[k]],"IDREF")
      }else if (!is.null(attr(biotic.list.cruise[[k]]$Code,"IDREF"))){
        biotic.cruise.metadata[1,k]=attr(biotic.list.cruise[[k]]$Code,"IDREF")
      }else{
        if (length(biotic.list.cruise[[k]])>0){
          biotic.cruise.metadata[1,k]=biotic.list.cruise[[k]]
        }else{
          biotic.cruise.metadata[1,k]=NA
        }  
      }
    }
    # 2.1.4. Get biotic vocabulary ----
    biotic.vocabulary=biotic.list$Vocabulary
    names(biotic.vocabulary)
    biotic.vocabulary.df = data.frame(matrix(vector(), 0, 4),stringsAsFactors=F)
    names(biotic.vocabulary.df)=c("vartype","Code","ID","URL")
    for (i in 1:length(biotic.vocabulary)){
      if (class(biotic.vocabulary[[i]])=='list'&length(biotic.vocabulary[[i]])==0){
        biotic.vocabulary.df[i,1]=names(biotic.vocabulary)[i]
        biotic.vocabulary.df[i,2]=NA
        biotic.vocabulary.df[i,3]=NA
        biotic.vocabulary.df[i,4]=NA
      }else{
        avi=biotic.vocabulary[[i]]$Code
        biotic.vocabulary.df[i,1]=names(biotic.vocabulary)[i]
        biotic.vocabulary.df[i,2]=avi[[1]]
        biotic.vocabulary.df[i,3]=attr(avi,"ID")
        biotic.vocabulary.df[i,4]=attr(avi,"CodeType")
      }
    }
    # 1.2. import and reshape biotic data ----
    Ncstart=8
    biology.db = data.frame(matrix(vector(), 0, 13),stringsAsFactors=F)
    names(biology.db)=c("FishID","LengthCode","LengthClass","WeightUnit",
                         "IndividualWeight","IndividualSex","IndividualMaturity",
                         "MaturityScale","IndividualAge","AgeSource",
                         "GeneticSamplingFlag","StomachSamplingFlag","ParasiteSamplingFlag")
    
    for (i in Ncstart:length(names(biotic.list.cruise))){
      # loop on haul lines
      hauli=biotic.list.cruise[[i]]
      # level 1: haul metadata
      lhi=names(hauli)[names(hauli)!='Catch']
      iscatch='Catch'%in%names(hauli)
      hauli.df = data.frame(matrix(vector(), 0, length(lhi)),stringsAsFactors=F)
      names(hauli.df)=names(hauli)[1:length(lhi)]
      for (k in c(1:length(lhi))){
        if (!is.null(attr(hauli[[k]],"IDREF"))){
          hauli.df[1,k]=attr(hauli[[k]],"IDREF")
        }else if (!is.null(attr(hauli[[k]],"nil"))){
          hauli.df[1,k]=attr(hauli[[k]],"nil")
        }else if (class(hauli[[k]])=='list'&length(hauli[[k]])==0){
          hauli.df[1,k]=NA
        }else{
          hauli.df[1,k]=hauli[[k]]
        }  
      }
      if (i==Ncstart){
        haul.db=hauli.df
        biotic.samples.db=samples.dbi
        # if (isbiologyh){
        #   biology.db=biology.dbj
        # }else{
        #   biology.db = data.frame(matrix(vector(), 0, 13),stringsAsFactors=F)
        #   names(biology.db)=c("FishID","LengthCode","LengthClass","WeightUnit",
        #                        "IndividualWeight","IndividualSex","IndividualMaturity",
        #                        "MaturityScale","IndividualAge","AgeSource",
        #                        "GeneticSamplingFlag","StomachSamplingFlag","ParasiteSamplingFlag")
        # }
      }else{
        haul.db=rbind(haul.db,hauli.df)
        biotic.samples.db=plyr::rbind.fill(biotic.samples.db,samples.dbi)
        # if (isbiologyh){
        #   biology.db=plyr::rbind.fill(biology.db,biology.dbj)
        # }
        #biology.dbj=biology.dbj[FALSE,]
      }
      # level 2: total catch and subsamples 
      length(hauli)
      names(hauli)
      if (iscatch){
        for (j in (length(lhi)+1):length(hauli)){
          # loop on catch lines
          hhi2=hauli[[j]]
          names(hhi2)
          length(hhi2)
          lhhi=names(hhi2)[names(hhi2)!='Biology']
          isbiology='Biology'%in%names(hhi2)
          samples.dfi = data.frame(matrix(vector(), 0, length(lhhi)),stringsAsFactors=F)
          names(samples.dfi)=names(hhi2)[1:length(lhhi)]
          for (k in 1:length(lhhi)){
            if (!is.null(attr(hhi2[[k]],"IDREF"))){
              samples.dfi[1,k]=attr(hhi2[[k]],"IDREF")
            }else{
              if (length(hhi2[[k]])>0){
                samples.dfi[1,k]=hhi2[[k]]
              }else{
                samples.dfi[1,k]=NA
              }  
            }
          }
          # level 3: biometry
          if (isbiology){
            # if sample has biology lines, extract them
            for (l in (length(lhhi)+1):length(hhi2)){
              # loop on biology lines
              biologyi=hhi2[[l]]
              names(biologyi)
              biology.dfi = data.frame(matrix(vector(), 0, length(biologyi)),stringsAsFactors=F)
              names(biology.dfi)=names(biologyi)
              for (m in 1:length(biologyi)){
                if (!is.null(attr(biologyi[[m]],"IDREF"))){
                  biology.dfi[1,m]=attr(biologyi[[m]],"IDREF")
                }else{
                  if (length(biologyi[[m]])>0){
                    biology.dfi[1,m]=biologyi[[m]]
                  }else{
                    biology.dfi[1,m]=NA
                  }  
                }
              }
              biology.dfi$StationName=hauli.df$StationName
              biology.dfi$SpeciesCode=attr(hhi2[[2]],"IDREF")
              biology.dfi$SpeciesCategory=hhi2[[4]]
              biology.dfi$type='Biology'
              if (l==(length(lhhi)+1)){
                biology.dbi=biology.dfi
              }else{
                biology.dbi=plyr::rbind.fill(biology.dbi,biology.dfi)
              }
              head(biology.dbi)
            }
          }
          if (j==(length(lhi)+1)){
            samples.dbi=samples.dfi
            if (isbiology){
              biology.dbj=biology.dbi
              isbiologyh=TRUE
            }else{
              
              isbiologyh=FALSE
            }
          }else{
            samples.dbi=plyr::rbind.fill(samples.dbi,samples.dfi)
          }
          if (isbiology){
            biology.db=plyr::rbind.fill(biology.db,biology.dbi)
            isbiologyh=TRUE
          }
        }
        samples.dbi$StationName=hauli.df$StationName
        samples.dbi$type='Catch'
      }else{
        samples.dbi = data.frame(matrix(vector(), 0, 17),stringsAsFactors=F)
        names(samples.dbi)=c(names(hhi2)[1:16],'StationName')
        cat('Station without samples:',hauli.df$StationName,'\n')
      }
    }
    biology.db=data.frame(apply(biology.db,2,unlist))
    biology.db$FishID=as.numeric(as.character(biology.db$FishID))
    biology.db=biology.db[order(biology.db$FishID),]
    cat('Biotic xml file successfully imported','\n')
  }else{
    cat('No path to biotic xml provided','\n')
    biotic.cruise.metadata=NULL
    haul.db=NULL
    biotic.samples.db=NULL
    biology.db=NULL
  }
  
  if (!is.null(path.Acoustic.xml)){
    # 2. import acoustic (if any) ----------
    # *************************
    # 2.1. Import acoustic metadata ----
    acoustic.xml <- read_xml(path.Acoustic.xml)
    acoustic.list=as_list(acoustic.xml)
    names(acoustic.list)
    length(acoustic.list)
    acoustic.list=acoustic.list[[1]]
    names(acoustic.list)
    # 2.1.1. Get acoustic instrument metadata ----
    if ('Instrument'%in%names(acoustic.list)){
      acoustic.Instrument=acoustic.list$Instrument
      names(acoustic.Instrument)
      length(acoustic.Instrument)
      acoustic.Instrument.metadata = data.frame(matrix(vector(), 0, length(acoustic.Instrument)),stringsAsFactors=F)
      names(acoustic.Instrument.metadata)=names(acoustic.Instrument)[1:length(acoustic.Instrument)]
      for (k in 1:length(acoustic.Instrument)){
        if (!is.null(attr(acoustic.Instrument[[k]],"IDREF"))){
          acoustic.Instrument.metadata[1,k]=attr(acoustic.Instrument[[k]],"IDREF")
        }else{
          if (length(acoustic.Instrument[[k]])>0){
            acoustic.Instrument.metadata[1,k]=acoustic.Instrument[[k]]
          }else{
            acoustic.Instrument.metadata[1,k]=NA
          }  
        }
      }
      acoustic.Instrument.metadata$ID=attr(acoustic.list[[1]],'ID')
    }else{
      acoustic.Instrument.metadata=NULL
      cat('Acoustic instrument header missing','\n')
    }
    # 2.1.2. Get acoustic instrument calibration metadata ----
    if ('Calibration'%in%names(acoustic.list)){
      Calibration=acoustic.list$Calibration
      names(Calibration)
      length(Calibration)
      Calibration.metadata = data.frame(matrix(vector(), 0, length(Calibration)),stringsAsFactors=F)
      names(Calibration.metadata)=names(Calibration)[1:length(Calibration)]
      for (k in 1:length(Calibration)){
        if (!is.null(attr(Calibration[[k]],"IDREF"))){
          Calibration.metadata[1,k]=attr(Calibration[[k]],"IDREF")
        }else{
          if (length(Calibration[[k]])>0){
            Calibration.metadata[1,k]=Calibration[[k]]
          }else{
            Calibration.metadata[1,k]=NA
          }  
        }
      }
      Calibration.metadata$ID=attr(acoustic.list[[2]],'ID')
    }else{
      Calibration.metadata=NULL
      cat('Acoustic calibration header missing','\n')
    }
    # 2.1.3. Get acoustic data acquisition metadata ----
    if ('DataAcquisition'%in%names(acoustic.list)){
      acquisition=acoustic.list$DataAcquisition
      names(acquisition)
      length(acquisition)
      acquisition.metadata = data.frame(matrix(vector(), 0, length(acquisition)),stringsAsFactors=F)
      names(acquisition.metadata)=names(acquisition)[1:length(acquisition)]
      for (k in 1:length(acquisition)){
        if (!is.null(attr(acquisition[[k]],"IDREF"))){
          acquisition.metadata[1,k]=attr(acquisition[[k]],"IDREF")
        }else{
          if (length(acquisition[[k]])>0){
            acquisition.metadata[1,k]=acquisition[[k]]
          }else{
            acquisition.metadata[1,k]=NA
          }  
        }
      }
      acquisition.metadata$ID=attr(acoustic.list[[3]],'ID')
    }else{
      acquisition.metadata=NULL
      cat('Acoustic data acquisition header missing','\n')
    }
    # 2.1.4. Get acoustic data processing metadata ----
    if ('DataProcessing'%in%names(acoustic.list)){
      processing=acoustic.list$DataProcessing
      names(processing)
      length(processing)
      processing.metadata = data.frame(matrix(vector(), 0, length(processing)),stringsAsFactors=F)
      names(processing.metadata)=names(processing)[1:length(processing)]
      for (k in 1:length(processing)){
        if (!is.null(attr(processing[[k]],"IDREF"))){
          processing.metadata[1,k]=attr(processing[[k]],"IDREF")
        }else{
          if (length(processing[[k]])>0){
            processing.metadata[1,k]=processing[[k]]
          }else{
            processing.metadata[1,k]=NA
          }  
        }
      }
      processing.metadata$ID=attr(acoustic.list$DataProcessing,'ID')
    }else{
      processing.metadata=NULL
      cat('Acoustic data processing header missing','\n')
    }
  
    # 2.1.4. Get acoustic vocabulary ----
    if ('Vocabulary'%in%names(acoustic.list)){
      acoustic.vocabulary=acoustic.list$Vocabulary
      names(acoustic.vocabulary)
      acoustic.vocabulary.df = data.frame(matrix(vector(), 0, 4),stringsAsFactors=F)
      names(acoustic.vocabulary.df)=c("vartype","Code","ID","URL")
      for (i in 1:length(acoustic.vocabulary)){
        if (!is.null(names(acoustic.vocabulary[[i]]))){
          avi=acoustic.vocabulary[[i]]$Code
        }else{
          avi=acoustic.vocabulary[[i]]
        }
        acoustic.vocabulary.df[i,1]=names(acoustic.vocabulary)[i]
        if (length(avi)==0){
          acoustic.vocabulary.df[i,2]=NA
          acoustic.vocabulary.df[i,3]=NA
          acoustic.vocabulary.df[i,4]=NA
        }else{
          acoustic.vocabulary.df[i,2]=avi[[1]]
          acoustic.vocabulary.df[i,3]=attr(avi,"ID")
          acoustic.vocabulary.df[i,4]=attr(avi,"CodeType")
        }
      }
    }else{
      acoustic.vocabulary.df=NULL
      cat('Acoustic vocabulary header missing','\n')
    }
    
    # 2.1.5. Get echotypes ----
    if ('EchoType'%in%names(acoustic.list)){
      ne=0
      for (i in 1:length(names(acoustic.list))){
        if (names(acoustic.list)[i]=='EchoType'){
          ne=ne+1
          ei=acoustic.list[[i]]
          names(ei);length(ei)
          echotypei = data.frame(matrix(vector(), 0, 4),stringsAsFactors=F)
          names(echotypei)=c("echotype","depthStratum","species","sizeCategory")
          if (length(ei)==1){
            cat('!!!!!!!!!!!!!!!!!!!!! Echotype ',attr(ei,"ID"),' has no species !!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            echotypei[1,1]=attr(ei,"ID")
            echotypei[1,2]=attr(ei[[1]],"IDREF")
            echotypei[1,3]=NA
            echotypei[1,4]=NA
          }else{
            for (j in 2:length(ei)){
              echotypei[j-1,1]=attr(ei,"ID")
              echotypei[j-1,2]=attr(ei[[1]],"IDREF")
              echotypei[j-1,3]=attr(ei[[j]]$SpeciesCode,"IDREF")
              echotypei[j-1,4]=ei[[j]]$SpeciesCategory
            } 
          }
          if (ne==1){
            echotypes=echotypei
          }else{
            echotypes=rbind(echotypes,echotypei)
          }  
        }
      }
    }else{
      echotypes=NULL
      cat('Echotypes header missing','\n')
    }
   
    # 2.2. Import acoustic data ----
    #************************
    acoustic.list.cruise=acoustic.list$Cruise
    names(acoustic.list.cruise)
    # 2.2.1. Get acoustic cruise metadata ----
    lacmi=names(acoustic.list.cruise)[names(acoustic.list.cruise)!='Log']
    acoustic.cruise.metadata = data.frame(matrix(vector(), 0, length(lacmi)),stringsAsFactors=F)
    names(acoustic.cruise.metadata)=names(acoustic.list.cruise)[1:length(lacmi)]
    for (k in 1:length(lacmi)){
      if (!is.null(attr(acoustic.list.cruise[[k]],"IDREF"))){
        acoustic.cruise.metadata[1,k]=attr(acoustic.list.cruise[[k]],"IDREF")
      }else if (!is.null(attr(acoustic.list.cruise[[k]]$Code,"IDREF"))){
        acoustic.cruise.metadata[1,k]=attr(acoustic.list.cruise[[k]]$Code,"IDREF")
      }else{
        if (length(acoustic.list.cruise[[k]])>0){
          acoustic.cruise.metadata[1,k]=acoustic.list.cruise[[k]]
        }else{
          acoustic.cruise.metadata[1,k]=NA
        }  
      }
    }
    # initialise dataframes
    log.db = data.frame(matrix(vector(), 0, 6),stringsAsFactors=F)
    names(log.db)=c("Distance","Time","Latitude","Longitude","Origin","Validity")
    acoustic.samples.db = data.frame(matrix(vector(), 0, 11),stringsAsFactors=F)
    names(acoustic.samples.db)=c("ChannelDepthUpper","ChannelDepthLower","PingAxisInterval",
                   "PingAxisIntervalType","PingAxisIntervalUnit","SvThreshold","Instrument",
                   "Calibration","DataAcquisition","DataProcessing","PingAxisIntervalOrigin")
    data.db=data.frame(matrix(vector(), 0, 4),stringsAsFactors=F)
    names(data.db)=c("SaCategory","Type","Unit","Value")
    # 2.2.2. Get acoustic data -----   
    for (i in 8:length(acoustic.list.cruise)){
      logi=acoustic.list.cruise[[i]]
      names(logi)
      lnlogi=names(logi)[names(logi)!='Sample']
      issample="Sample"%in%names(logi)
      logi.df = data.frame(matrix(vector(), 0, length(lnlogi)),stringsAsFactors=F)
      names(logi.df)=lnlogi
      # level 1: EDSU metadata
      for (k in 1:length(lnlogi)){
        if (!is.null(attr(logi[[k]],"IDREF"))){
          logi.df[1,k]=attr(logi[[k]],"IDREF")
        }else{
          if (length(logi[[k]])>0){
            logi.df[1,k]=logi[[k]]
          }else{
            logi.df[1,k]=NA
          }  
        }
      }
      # level 2: samples
      if (issample){
        sample.dbi=data.frame(matrix(vector(), 0, 11),stringsAsFactors=F)
        names(sample.dbi)=c("ChannelDepthUpper","ChannelDepthLower","PingAxisInterval",
          "PingAxisIntervalType","PingAxisIntervalUnit","SvThreshold","Instrument",
          "Calibration","DataAcquisition","DataProcessing","PingAxisIntervalOrigin")
        for (j in (length(lnlogi)+1):length(logi)){
          si=logi[[j]]
          names(si)
          lnsi=names(si)[names(si)!='Data']
          isdata="Data"%in%names(si)
          si.df = data.frame(matrix(vector(), 0, length(lnsi)),stringsAsFactors=F)
          names(si.df)=names(si)[1:length(lnsi)]
          for (k in 1:length(lnsi)){
            if (!is.null(attr(si[[k]],"IDREF"))){
              si.df[1,k]=attr(si[[k]],"IDREF")
            }else{
              if (length(si[[k]])>0){
                si.df[1,k]=si[[k]]
              }else{
                si.df[1,k]=NA
              }  
            }
          }
          si.df$Time=logi.df$Time
          # level 3: data
          if (isdata){
            data.dbi = data.frame(matrix(vector(), 0, 7),stringsAsFactors=F)
            names(data.dbi)=c("SaCategory","Type","Unit","Value","Time","ChannelDepthUpper",
                              "ChannelDepthLower")
            di.dbi=data.dbi
            for (m in (length(lnsi)+1):length(si)){
              di=si[[m]]
              names(di)
              di.df = data.frame(matrix(vector(), 0, 4),stringsAsFactors=F)
              names(di.df)=names(di)
              for (l in 1:4){
                if (!is.null(attr(di[[l]],"IDREF"))){
                  di.df[1,l]=attr(di[[l]],"IDREF")
                }else{
                  if (length(di[[l]])>0){
                    di.df[1,l]=di[[l]]
                  }else{
                    di.df[1,l]=NA
                  }  
                }
              }
              if (dim(di.df)[1]>0){
                di.dbi=rbind(di.dbi,di.df)
              }
              # if (m==(length(lnsi)+1)){
              #   di.dbi=di.df
              # }else{
              #   di.dbi=rbind(di.dbi,di.df)
              # }
            }
            di.dbi$Time=logi.df$Time
            di.dbi$ChannelDepthUpper=si.df$ChannelDepthUpper
            di.dbi$ChannelDepthLower=si.df$ChannelDepthLower
            if (dim(di.dbi)[1]>0){
              data.db=rbind(data.db,di.dbi)
            }  
          }else{
            cat("Sample without data:",paste(si.df$Time,' ',si.df$ChannelDepthUpper,'m-',si.df$ChannelDepthLower,'m',sep=''),'\n')
            di.df = data.frame(matrix(vector(), 0, 7),stringsAsFactors=F)
            names(di.df)=c("SaCategory","Type","Unit","Value","Time","ChannelDepthUpper","ChannelDepthLower")
          }
          if (dim(si.df)[1]>0){
            acoustic.samples.db=rbind(acoustic.samples.db,si.df)
          }
          # if (j==(length(lnlogi)+1)){
          #   sample.dbi=si.df
          #   data.dbi=di.dbi
          # }else{
          #   sample.dbi=rbind(sample.dbi,si.df)
          #   data.dbi=rbind(data.dbi,di.dbi)
          # }
        }
      }
      if (dim(logi.df)[1]>0){
        log.db=rbind(log.db,logi.df)
      }
      # if (i==8){
      #   log.db=logi.df
      #   acoustic.samples.db=sample.dbi
      #   data.db=data.dbi
      # }else{
      #   log.db=rbind(log.db,logi.df)
      #   acoustic.samples.db=rbind(acoustic.samples.db,sample.dbi)
      #   data.db=rbind(data.db,data.dbi)
      # }
    }
    cat('Acoustic xml file successfully imported','\n')
  }else{
    cat('No path to acoustic xml provided','\n')
    acoustic.cruise.metadata=NULL
    log.db=NULL
    acoustic.samples.db=NULL
    data.db=NULL
  }
  list(biotic.cruise.metadata=biotic.cruise.metadata,haul.db=haul.db,biotic.samples.db=biotic.samples.db,
       biology.db=biology.db,biotic.vocabulary=biotic.vocabulary,
       acoustic.cruise.metadata=acoustic.cruise.metadata,
       acoustic.log.db=log.db,acoustic.samples.db=acoustic.samples.db,
       acoustic.data.db=data.db,
       acoustic.Instrument.metadata=acoustic.Instrument.metadata,
       acoustic.Instrument.Calibration.metadata=Calibration.metadata,
       acoustic.Instrument.Acquisition.metadata=acquisition.metadata,
       acoustic.Instrument.Processing.metadata=processing.metadata,
       acoustic.vocabulary=acoustic.vocabulary) 
}

dfnames.converter=function(x,names.df=
                             data.frame(a=c("GENR_ESP","NOSTA","SIGNEP","PT","NT",
                                            "LM","PM","MOULE","PTRI","quality","a","b",
                                            "SEXE","Lcm",'NBIND',
                                            'POIDSTAILLE','INC','POCHE','POIDSECHANT',
                                            "NECHANT","UNITE","VESSEL","CodEsp2",
                                            'discard',"CAMPAGNE"),
                                        b=c("baracoudaCode","operationId","sizeCategory",
                                            "sampleWeight","numberSampled","meanLength",
                                            "meanWeight","noPerKg","sortedWeight",
                                            "quality","a","b","sexCategory",
                                            "lengthClass","numberAtLength",
                                            "weightAtLength","round","subHaul",
                                            "sampleWeight","numberSampled","units",
                                            "vessel","CodEsp2",'discard','voyage')),
                           template=NULL){
  names.df.mens=data.frame(a=c("GENR_ESP","NOSTA","CATEG",
                               "LM","PM","MOULE","PTRI","quality","a","b",
                               "SEXE","Lcm",'NBIND',
                               'POIDSTAILLE','INC','POCHE','POIDSECHANT',
                               "NECHANT","UNITE","VESSEL","CodEsp2",
                               'discard',"CAMPAGNE","NOCHAL"),
                           b=c("baracoudaCode","operationId","sizeCategory",
                               "meanLength","meanWeight","noPerKg","sortedWeight",
                               "quality","a","b","sexCategory",
                               "lengthClass","numberAtLength",
                               "weightAtLength","round","subHaul",
                               "sampleWeight","numberSampled","units",
                               "vessel","CodEsp2",'discard','voyage',"operationId2"))
  names.df.mensEB=data.frame(a=c("baracoudacode","operationID","sizeCategory",
                               "meanLength","meanWeight","noPerKg","sortedWeight",
                               "quality","a","b","sexCategory",
                               "lengthClass","numberAtLength",
                               "weightAtLength","round","subHaul",
                               "sampleWeight","numberSampled","units",
                               "vessel","CodEsp2",'discard','voyage'),
                           b=c("baracoudaCode","operationId","sizeCategory",
                               "meanLength","meanWeight","noPerKg","sortedWeight",
                               "quality","a","b","sexCategory",
                               "lengthClass","numberAtLength",
                               "weightAtLength","round","subHaul",
                               "sampleWeight","numberSampled","units",
                               "vessel","CodEsp2",'discard','voyage'))
  names.df.mensEB2=data.frame(a=c("baracoudacode","operationId","sizeCategory",
                                 "meanLength","meanWeight","noPerKg","sortedWeight",
                                 "quality","a","b","sexCategory",
                                 "lengthClass","numberAtLength",
                                 "weightAtLength","round","subHaul",
                                 "sampleWeight","numberSampled","units",
                                 "vessel","CodEsp2",'discard','voyage'),
                             b=c("baracoudaCode","operationId","sizeCategory",
                                 "meanLength","meanWeight","noPerKg","sortedWeight",
                                 "quality","a","b","sexCategory",
                                 "lengthClass","numberAtLength",
                                 "weightAtLength","round","subHaul",
                                 "sampleWeight","numberSampled","units",
                                 "vessel","CodEsp2",'discard','voyage'))
  names.df.mens2=data.frame(a=c("GENR_ESP","NOSTA","CATEG",
                               "LM","PM","MOULE","PTRI","quality","a","b",
                               "SEXE","Lcm",'NBIND',
                               'POIDSTAILLE','INC','POCHE','POIDSECHANT',
                               "NECHANT","UNITE","VESSEL","CodEsp2",
                               'discard',"CAMPAGNE","NOCHAL"),
                           b=c("baracoudacode","operationID","sizeCategory",
                               "meanLength","meanWeight","noPerKg","sortedWeight",
                               "quality","a","b","sexCategory",
                               "lengthClass","numberAtLength",
                               "weightAtLength","round","subHaul",
                               "sampleWeight","numberSampled","units",
                               "vessel","CodEsp2",'discard','voyage','operationID'))
  names.df.peche=data.frame(a=c("GENR_ESP","NOSTA","SIGNEP",
                                "LM","PM","MOULE","PTRI","quality","a","b",
                                "SEXE","Lcm",'NBIND',
                                'POIDSTAILLE','INC','POCHE','PT',
                                "NT","UNITE","VESSEL","CodEsp2",
                                'discard',"CAMPAGNE",'CodEsp'),
                            b=c("baracoudaCode","operationId","sizeCategory",
                                "meanLength","meanWeight","noPerKg","sortedWeight",
                                "quality","a","b","sexCategory",
                                "lengthClass","numberAtLength",
                                "weightAtLength","round","subHaul",
                                "sampleWeight","numberSampled","units",
                                "vessel","CodEsp2",'discard','voyage','speciesCode'))
  names.df.peche.low=data.frame(a=c("GENR_ESP","NOSTA","SIGNEP",
                                "LM","PM","MOULE","PTRI","quality","a","b",
                                "SEXE","Lcm",'NBIND',
                                'POIDSTAILLE','INC','POCHE','PT',
                                "NT","UNITE","VESSEL","CodEsp2",
                                'discard',"CAMPAGNE",'CodEsp'),
                            b=tolower(c("baracoudaCode","operationId","sizeCategory",
                                "meanLength","meanWeight","noPerKg","sortedWeight",
                                "quality","a","b","sexCategory",
                                "lengthClass","numberAtLength",
                                "weightAtLength","round","subHaul",
                                "sampleWeight","numberSampled","units",
                                "vessel","CodEsp2",'discard','voyage','speciesCode')))
  names.df.peche2=data.frame(a=c("GENR_ESP","NOSTA","SIGNEP",
                                "LM","PM","MOULE","PTRI","quality","a","b",
                                "SEXE","Lcm",'NBIND',
                                'POIDSTAILLE','INC','POCHE','PT',
                                "NT","UNITE","VESSEL","CodEsp2",
                                'discard',"CAMPAGNE",'CodEsp'),
                            b=c("baracoudacode","operationID","sizeCategory",
                                "meanLength","meanWeight","noPerKg","sortedWeight",
                                "quality","a","b","sexCategory",
                                "lengthClass","numberAtLength",
                                "weightAtLength","round","subHaul",
                                "sampleWeight","numberSampled","units",
                                "vessel","CodEsp2",'discard','voyage','speciesCode'))
  names.df.pecheEB=data.frame(a=c("baracoudacode","operationID","sizeCategory",
                                "meanLength","meanWeight","noPerKg","sortedWeight",
                                "quality","a","b","sexCategory",
                                "lengthClass","numberAtLength",
                                "weightAtLength","round","subHaul",
                                "sampleWeight","numberSampled","units",
                                "vessel","CodEsp2",'discard','voyage','speciesCode'),
                            b=c("baracoudaCode","operationId","sizeCategory",
                                "meanLength","meanWeight","noPerKg","sortedWeight",
                                "quality","a","b","sexCategory",
                                "lengthClass","numberAtLength",
                                "weightAtLength","round","subHaul",
                                "sampleWeight","numberSampled","units",
                                "vessel","CodEsp2",'discard','voyage','speciesCode'))
  if (!is.null(template)){
    if (template=='mens'){
      names.df=names.df.mens
    }else if  (template=='peche'){
      names.df=names.df.peche
    }else if  (template=='peche2'){
      names.df=names.df.peche2
    }else if  (template=='pecheEB'){
      names.df=names.df.pecheEB
    }else if  (template=='pecheEB2'){
      names.df=names.df.pecheEB2
    }else if  (template=='mens2'){
      names.df=names.df.mens2
    }else if  (template=='mensEB'){
      names.df=names.df.mensEB
    }else if  (template=='mensEB2'){
      names.df=names.df.mensEB2
    }else if  (template=='pechelow'){
      names.df=names.df.peche.low
    }
  }
  if (sum(duplicated(names.df$a))>0){
    stop('Duplicated names found in a, please check','\n')
  }
  if (sum(duplicated(names.df$b))>0){
    stop('Duplicated names found in b, please check','\n')
  }
  ni=data.frame(ni=names(x))
  if (sum(names(x)%in%names.df$b)==length(names(x))){
    ni=merge(ni,names.df,by.x='ni',by.y='b',sort=FALSE)
    names(x)=ni$a
    cat('Data frame names converted from names b to names a','\n')
  }else if (sum(names(x)%in%names.df$a)==length(names(x))){
    ni=merge(ni,names.df,by.x='ni',by.y='a',sort=FALSE)
    names(x)=ni$b
    cat('Data frame names converted from names a to names b','\n')
  }else{
    nnia=names(x)[!names(x)%in%names.df$a]
    nnib=names(x)[!names(x)%in%names.df$b]
    if (length(nnia)<length(nnib)){
      nnis=nnia
    }else{
      nnis=nnib
    }  
    stop('Data frame names not found in names.df: ',
         paste(nnis,collapse = ', '))
  }
x  
}

subsampleConsistencyCheck=function(mens4Echobase,tnames='EB'){
  if (tnames=='EB'){
    mens4Echobasea2=aggregate(mens4Echobase[,c('numberAtLength','weightAtLength')],
                              list(operationId=mens4Echobase$operationId,
                                   baracoudaCode=mens4Echobase$baracoudaCode,
                                   sizeCategory=mens4Echobase$sizeCategory,
                                   sexCategory=mens4Echobase$sexCategory),
                              sum,na.rm=TRUE)
    names(mens4Echobasea2)[5:6]=c("numberSampled2","sampleWeight2")
    dim(mens4Echobase)
    mens4Echobase=merge(mens4Echobase,mens4Echobasea2)
    dim(mens4Echobase)
    plot(mens4Echobase$sampleWeight,mens4Echobase$sampleWeight2,xlab='Number sampled',
         ylab='Corrected number sampled')
    abline(coef = c(0,1))
    #x11()
    plot(mens4Echobase$numberSampled,mens4Echobase$numberSampled2,xlab='Total sample',
         ylab='Corrected total sample')
    abline(coef = c(0,1))
    mens4Echobase$sampleWeight=mens4Echobase$sampleWeight2
    mens4Echobase$numberSampled=mens4Echobase$numberSampled2
    mens4Echobase=mens4Echobase[,names(mens4Echobase)[
      !names(mens4Echobase)%in%c('sampleWeight2','numberSampled2')]]
    cat('-> Subsamples internal consistency checked','\n')
  }else if (tnames=='RAPTRI'){
    mens4Echobasea2=aggregate(mens4Echobase[,c('NECHANT','POIDSECHANT')],
                              list(NOSTA=mens4Echobase$NOSTA,
                                   GENR_ESP=mens4Echobase$GENR_ESP,
                                   CATEG=mens4Echobase$CATEG,
                                   SEXE=mens4Echobase$SEXE),
                              sum,na.rm=TRUE)
    names(mens4Echobasea2)[5:6]=c("NECHANT2","POIDSECHANT2")
    dim(mens4Echobase)
    mens4Echobase=merge(mens4Echobase,mens4Echobasea2,
                        by.x=c('NOSTA','GENR_ESP','CATEG','SEXE'),
                        by.y=c('NOSTA','GENR_ESP','CATEG','SEXE'))
    dim(mens4Echobase)
    mens4Echobase$POIDSECHANT=mens4Echobase$POIDSECHANT2
    mens4Echobase$NECHANT=mens4Echobase$NECHANT2
    mens4Echobase=mens4Echobase[,names(mens4Echobase)[
      !names(mens4Echobase)%in%c('POIDSECHANT2','NECHANT2')]]
    cat('-> Subsamples internal consistency checked','\n')
  }
  list(mens4Echobase=mens4Echobase,mens4Echobasea2=mens4Echobasea2)
}


checkSubTotalSamplesOperationsCoherence=function(mens4Echobase,Pechei4Echobase,
                                                 Operations=NULL,
                                                 OperationMetadataValue=NULL,
                                                 GearMetadataValue=NULL,
                                                 exceptGear=c('MIK','MEPEL','CARRE','MINOF'),
                                                 TotCatchOpeCor=TRUE,
                                                 path.export=NULL){
  #Check nos. of operations
  length(sort(unique(mens4Echobase$operationId)))
  length(sort(unique(Pechei4Echobase$operationId)))
  if (!is.null(Operations)){
    length(sort(unique(Operations$operationId)))
  }
  ops.check=unique(mens4Echobase$operationId)[!unique(mens4Echobase$operationId)%in%unique(Pechei4Echobase$operationId)]
  
  if (length(ops.check)>0){
    cat('Operations ',as.character(ops.check),' have subsamples but no total samples.','\n')
    if (TotCatchOpeCor){
      Pechei4Echobase$id=paste(Pechei4Echobase$operationId,Pechei4Echobase$baracoudaCode,Pechei4Echobase$sizeCategory)
      mens4Echobase$id=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,mens4Echobase$sizeCategory)
      msp=unique(mens4Echobase[!mens4Echobase$id%in%unique(Pechei4Echobase$id),])
      mens4Echobase=mens4Echobase[,names(mens4Echobase)!='id']
      head(msp)
      lnop=unique(msp$operationId)
      lgnop=unique(Operations[Operations$operationId%in%lnop,'gearCode'])
      if (sum(!lgnop%in%exceptGear)>0) cat('SubSamples without totalSamples for non-exception gear, please check: ',
                                           as.character(lgnop[!lgnop%in%exceptGear]),'\n')
      head(Operations)
      mspa=aggregate(msp[,c('numberAtLength','weightAtLength')],
                     list(operationId=msp$operationId,baracoudaCode=msp$baracoudaCode,
                          sizeCategory=msp$sizeCategory),
                     sum,na.rm=TRUE)
      names(mspa)[4:5]=c('Ntot','Wtot')
      msp=merge(msp,mspa)
      head(msp)
      dim(msp);dim(mspa)
      # calculate mean length weighted by abundances
      msp$Li=msp$lengthClass*msp$numberAtLength/(msp$Ntot)
      mspa2=aggregate(msp[,c('Li')],
                      list(operationId=msp$operationId,baracoudaCode=msp$baracoudaCode,
                           sizeCategory=msp$sizeCategory),
                      sum,na.rm=TRUE)
      names(mspa2)[4]='meanLength'
      ntcr=unique(msp[,c('operationId','baracoudaCode','sizeCategory','sampleWeight',
                         'numberSampled')])
      ntcr=merge(ntcr,mspa2)
      ntcr$meanWeight=1000*ntcr$sampleWeight/ntcr$numberSampled
      ntcr$noPerKg=1000/ntcr$meanWeight
      ntcr$sortedWeight=ntcr$sampleWeight
      ntcr=ntcr[,names(ntcr)!='sexCategory']
      
      # aggregate newly created catches
      ntcr$CodEsp=paste(ntcr$baracoudaCode,ntcr$sizeCategory,sep='-')
      head(ntcr)
      rac=aggregate.catches(Pecheic=ntcr, spname = "CodEsp", stname = "operationId", 
                            svnames=c("sampleWeight", "numberSampled"),
                            mvnames=c("meanLength", "meanWeight", "noPerKg"),
                            path.export = NULL,wmethod='biomass')
      names(rac)
      ntcra=rac$Pechei
      dim(ntcr);dim(ntcra)
      # update species codes
      names(Pechei4Echobase);names(ntcra)
      Pechei4Echobase=Pechei4Echobase[,names(Pechei4Echobase)!='id']
      Pechei4Echobase=rbind(Pechei4Echobase,ntcra[,names(Pechei4Echobase)])
      cat('Operations ',as.character(ops.check),' new totalSamples added.','\n')
      # check that total samples biomass and abundance are larger than subsamples ones and correct eventually
      mspatot=aggregate(mens4Echobase[,c('numberAtLength','weightAtLength')],
                        list(operationId=mens4Echobase$operationId,
                             baracoudaCode=mens4Echobase$baracoudaCode,
                             sizeCategory=mens4Echobase$sizeCategory),
                        sum,na.rm=TRUE)
      names(mspatot)[4:5]=c('Ntot','Wtot')
      pecheNames=names(Pechei4Echobase)
      Pechei4Echobase=merge(Pechei4Echobase,mspatot,by.x=c("operationId","baracoudaCode","sizeCategory"),
                            by.y=c("operationId","baracoudaCode","sizeCategory"),all.x=TRUE)
      Pechei4Echobase$dW=Pechei4Echobase$sampleWeight-Pechei4Echobase$Wtot
      idWtotCor=Pechei4Echobase[!is.na(Pechei4Echobase$dW)&Pechei4Echobase$dW<0,
                                c("operationId","baracoudaCode","sizeCategory")]
      Pechei4Echobase[!is.na(Pechei4Echobase$dW)&Pechei4Echobase$dW<0,'sampleWeight']=
        Pechei4Echobase[!is.na(Pechei4Echobase$dW)&Pechei4Echobase$dW<0,'Wtot']
      Pechei4Echobase$dN=Pechei4Echobase$numberSampled-Pechei4Echobase$Ntot
      idNtotCor=Pechei4Echobase[!is.na(Pechei4Echobase$dN)&Pechei4Echobase$dN<0,
                                c("operationId","baracoudaCode","sizeCategory")]
      Pechei4Echobase[!is.na(Pechei4Echobase$dN)&Pechei4Echobase$dN<0,'numberSampled']=
        Pechei4Echobase[!is.na(Pechei4Echobase$dN)&Pechei4Echobase$dN<0,'Ntot']
      #Pechei4Echobase=Pechei4Echobase[,!names(Pechei4Echobase)%in%c('Ntot','Wtot','dW','dN')]
      Pechei4Echobase=Pechei4Echobase[,pecheNames]
      cat('Total samples total biomass changed to match subsamples in:',
          paste(idWtotCor$operationId,idWtotCor$baracoudaCode,idWtotCor$sizeCategory),'\n')
      cat('Total samples total abundance changed to match subsamples in:',
          paste(idNtotCor$operationId,idNtotCor$baracoudaCode,idNtotCor$sizeCategory),'\n')
      # re-Check that file is complete
      idts=paste(Pechei4Echobase$operationId,Pechei4Echobase$baracoudaCode,Pechei4Echobase$sizeCategory)
      idss=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,mens4Echobase$sizeCategory)
      idmsp=unique(idss)[!unique(idss)%in%unique(idts)]
      if (length(idmsp)>0){
        stop('subsamples without totalsamples found after correction')
      }
      #Export catches
      write.table(Pechei4Echobase,paste(path.export,cruise,'_totalSamples4Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)
      cat('-> Total samples data exported','\n')
      unique(Pechei4Echobase$operationId)
    }else{
      mens4Echobase=mens4Echobase[mens4Echobase$operationId%in%
                                    unique(Pechei4Echobase$operationId),]
      cat('Operations ',as.character(ops.check),' subsamples with no total samples removed.','\n')
      mens.discardi=mens4Echobase[!mens4Echobase$operationId%in%
                                    unique(Pechei4Echobase$operationId),]
      if  (dim(mens.discardi)[1]>0&!exists("mens.discard")){
        mens.discard=mens.discardi
      }else if (dim(mens.discard)[1]>0){
        mens.discard=plyr::rbind.fill(mens.discard,mens.discardi)
      }
    }
  }
  
  #5.2.2.9. Check duplicated total samples 
  #********************
  idts=paste(Pechei4Echobase$operationId,Pechei4Echobase$baracoudaCode,
             Pechei4Echobase$sizeCategory)
  if (sum(duplicated(idts))>0){
    stop('Duplicated total samples in: ',paste(idts[duplicated(idts)],collapse = ' '))
  }
  
  #5.2.2.10. Check operations / total samples links 
  # and remove operations without total catches if any
  #************************************
  if (!is.null(Operations)){
    uope=as.character(unique(Operations$operationId))
    uopets=as.character(unique(Pechei4Echobase$operationId))
    lop.missing2=uope[!uope%in%uopets]
    
    if (length(lop.missing2)>0){
      sort(lop.missing2)  
      #stop('Total samples without operation: ',paste(lop.missing2,collapse=' '),'\n')
      Operations=Operations[!Operations$operationId%in%as.character(lop.missing2),]
      OperationMetadataValue=OperationMetadataValue[!OperationMetadataValue$operationId%in%
                                                      as.character(lop.missing2),]
      GearMetadataValue=GearMetadataValue[!GearMetadataValue$operationId%in%
                                            as.character(lop.missing2),]
      # also remove associated subsamples if any
      mens4Echobase=mens4Echobase[!mens4Echobase$operationId%in%
                                    as.character(lop.missing2),]
      #Export Operation data in Echobase format 
      #********************
      if (!is.null(path.export)){
        write.table(Operations,paste(path.export,cruise,'_Operations4Echobase.csv',sep=''),
                    row.names=FALSE,sep=';')
        cat('-> Corrected Operation data exported','\n')
        write.table(OperationMetadataValue,
                    paste(path.export,cruise,'_OperationMetadataValue4Echobase.csv',sep=''),
                    row.names=FALSE,sep=';')
        cat('-> Corrected Operation metadata exported','\n')
        write.table(GearMetadataValue,paste(path.export,cruise,'_GearMetadataValue4Echobase.csv',sep=''),
                    row.names=FALSE,sep=';')
        cat('-> Corrected Gear metadata exported','\n')
      }
      cat(paste('Operations without Total samples removed: ',lop.missing2,'\n'),'\n')
    }
  }
  # Check total /sub samples links 
  # and add missing total samples if any
  #************************************
  ussid=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,
              mens4Echobase$sizeCategory)
  utsid=paste(Pechei4Echobase$operationId,Pechei4Echobase$baracoudaCode,
              Pechei4Echobase$sizeCategory)
  # Missing total samples
  #************************************
  mtsid=ussid[!ussid%in%utsid]
  if (length(mtsid)>0){
    head(mens4Echobase)
    head(Pechei4Echobase)
    ssi=mens4Echobase[ussid%in%mtsid,]
    ssi$Li=ssi$lengthClass*ssi$numberAtLength
    Pechei4Echobase.msadd=aggregate(
      ssi[,c('numberAtLength','weightAtLength','Li')],
      list(operationId=ssi$operationId,baracoudaCode=ssi$baracoudaCode,
           sizeCategory=ssi$sizeCategory),sum,na.rm=TRUE)
    names(Pechei4Echobase.msadd)[4:5]=c('numberSampled','sampleWeight')
    Pechei4Echobase.msadd$meanLength=Pechei4Echobase.msadd$Li/
      Pechei4Echobase.msadd$numberSampled
    Pechei4Echobase.msadd$meanWeight=1000*Pechei4Echobase.msadd$sampleWeight/
      Pechei4Echobase.msadd$numberSampled
    Pechei4Echobase.msadd$noPerKg=1000/
      Pechei4Echobase.msadd$meanWeight
    Pechei4Echobase.msadd$sortedWeight=Pechei4Echobase.msadd$sampleWeight
    Pechei4Echobase=rbind(Pechei4Echobase,
                          Pechei4Echobase.msadd[,names(Pechei4Echobase)])
    if (!is.null(path.export)){
      write.table(Pechei4Echobase,paste(path.export,cruise,'_totalSamples4Echobase.csv',sep=''),
                  row.names=FALSE,sep=';')
    }
    cat('-> Missing totalsamples without subsamples exported','\n')
  }
  list(mens4Echobase,Pechei4Echobase,Operations,OperationMetadataValue,
       GearMetadataValue)
}

Biometry.consistency.check=function(Biometry,Pechei4Echobase,mens4Echobase,
                                    path.export=NULL,cruise,
                                    LW.check=FALSE,removeBadCatch=FALSE,
                                    ssnames=c("operationId","baracoudaCode",
                                              "sizeCategory","sexCategory",
                                              "sampleWeight","numberSampled",
                                              "lengthClass","numberAtLength",
                                              "weightAtLength","round")){
  # remove null stations
  Biometry=Biometry[Biometry$operationId!='',]
  
  # 8.1. Check all biometry hauls are in operation hauls 
  moi=unique(Biometry$operationId[!Biometry$operationId%in%Pechei4Echobase$operationId])
  if (length(moi)>0){
    stop('Operations ',paste(moi,collapse=' '),' found in biometry and not in catches, please check')
  }
  unique(Biometry$operationId)
  
  # add sizeCategory if none
  Biometry$sizeCategory=0
  unique(Biometry[Biometry$name=='Sex','dataValue'])
  
  # 8.2. Check that all biometries have subsamples 
  #************************
  Biometryl=Biometry[Biometry$name=='LTmm1',]
  idBiometry=paste(Biometryl$operationId,Biometryl$baracoudaCode,
                   Biometryl$dataValue,Biometryl$sizeCategory)
  idss=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,
             mens4Echobase$lengthClass*10,mens4Echobase$sizeCategory)
  missbiomid=unique(idBiometry[!idBiometry%in%idss])
  missbiom=Biometryl[idBiometry%in%missbiomid,]
  Biometryw=Biometry[Biometry$name=='Weightg'&
                       Biometry$numFish%in%missbiom$numFish,]
  names(missbiom)[4]='LTmm1'
  names(Biometryw)[4]='Weightg'
  missbiomwl=merge(missbiom,Biometryw,
                   by.x=c('operationId','baracoudaCode','numFish','sizeCategory'),
                   by.y=c('operationId','baracoudaCode','numFish','sizeCategory'))
  missbiomwl$Weightg=as.numeric(missbiomwl$Weightg)
  missbiomaNaL=aggregate(missbiomwl$numFish,list(operationId=missbiomwl$operationId,
                                                 baracoudaCode=missbiomwl$baracoudaCode,
                                                 lengthClass=missbiomwl$LTmm1,
                                                 sizeCategory=missbiomwl$sizeCategory),
                         length)
  names(missbiomaNaL)[5]='numberAtLength'
  missbiomaNaW=aggregate(missbiomwl$Weightg,list(operationId=missbiomwl$operationId,
                                                 baracoudaCode=missbiomwl$baracoudaCode,
                                                 lengthClass=missbiomwl$LTmm1,
                                                 sizeCategory=missbiomwl$sizeCategory),
                         sum)
  names(missbiomaNaW)[5]='weightAtLength'
  missbiomaNaWL=merge(missbiomaNaW,missbiomaNaL)
  missbiomaNaWL$idss=paste(missbiomaNaWL$operationId,missbiomaNaWL$baracoudaCode,
                           missbiomaNaWL$lengthClass)
  if (length(missbiomid)>0){
    # 8.2.1. eventually, add missing length class to subsamples
    head(mens4Echobase)
    head(Biometry)
    mens4EchobaseAdd=mens4Echobase[FALSE,]
    mssa=FALSE
    for (i in 1:length(missbiomid)){
      idshort=strsplit(missbiomid[i],split=' ')
      missbiomaNaWLi=missbiomaNaWL[missbiomaNaWL$id==paste(idshort[[1]][1:3],collapse=' '),]
      missbiomaNaWLi$lengthClass=as.numeric(missbiomaNaWLi$lengthClass)
      ssi=mens4Echobase[mens4Echobase$operationId==as.character(missbiomaNaWLi$operationId)&
                          mens4Echobase$baracoudaCode==as.character(missbiomaNaWLi$baracoudaCode)&
                          mens4Echobase$sizeCategory==as.character(missbiomaNaWLi$sizeCategory),]
      if (dim(ssi)[1]==0&!removeBadCatch){
        stop('Missing subsample for biometry: ',missbiomid[i])
      }else if (dim(ssi)[1]==0&removeBadCatch){
        cat('Missing subsample for biometry skipped: ',missbiomid[i],'\n')
        next()
      }else{
        mens4EchobaseAdd[i,'operationId']=as.character(missbiomaNaWLi$operationId)
        mens4EchobaseAdd[i,'baracoudaCode']=as.character(missbiomaNaWLi$baracoudaCode)
        mens4EchobaseAdd[i,'sizeCategory']=as.character(missbiomaNaWLi$sizeCategory)
        mens4EchobaseAdd[i,'sexCategory']="N"
        mens4EchobaseAdd[i,'round']=5
        mens4EchobaseAdd[i,'lengthClass']=missbiomaNaWLi$lengthClass/10
        mens4EchobaseAdd[i,'numberAtLength']=missbiomaNaWLi$numberAtLength
        mens4EchobaseAdd[i,'weightAtLength']=missbiomaNaWLi$weightAtLength/1000
        mens4EchobaseAdd[i,'sampleWeight']=unique(ssi$sampleWeight)
        mens4EchobaseAdd[i,'numberSampled']=unique(ssi$numberSampled)
        mssa=TRUE
        cat('Missing subsample added: ',missbiomid[i],'\n')
      }
    }
    mens4Echobase=rbind(mens4Echobase,
                        mens4EchobaseAdd[complete.cases(mens4EchobaseAdd),
                                         names(mens4Echobase)])
    head(mens4Echobase)
    if (!is.null(path.export)&mssa){
      # Export corrected subsamples 
      mens4Echobase=mens4Echobase[,ssnames]
      mens4Echobase.id=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,
                             mens4Echobase$sizeCategory,mens4Echobase$lengthClass)
      if (length(mens4Echobase.id[duplicated(mens4Echobase.id)])>0){
        stop('Duplicated subsamples after biometry-subsample consistency check')
      }
      write.table(mens4Echobase,
                  paste(path.export,cruise,'_subSamples4Echobase.csv',sep=''),sep=';',
                  row.names=FALSE)
      cat('-> Corrected Sub-samples data exported','\n')
    }
    if (LW.check){
      mens4Echobase$meanWeight=mens4Echobase$weightAtLength/mens4Echobase$numberAtLength
      EBsubSamples.LW=LW.compute(catch=mens4Echobase,lspcodes.mens=NULL,
                                 codespname='baracoudaCode',Lname='lengthClass',
                                 Wname='meanWeight',wname='numberAtLength',
                                 plotit=TRUE,compute.LW=TRUE,Wscaling=1000,ux11=FALSE)
      mtext('Corrected subsamples (after biometry) export')
    }
    mens4Echobase=mens4Echobase[,ssnames]
  }
  list(Biometry=Biometry,mens4Echobase=mens4Echobase)
}

