
#***************************************************************
#Check fishing data
#***************************************************************
  duplicated.speciesIncatches.check=function(Pechei,stname='NOCHAL'){
    ID=paste(Pechei$CodEsp,Pechei[,stname])
    lnochal=Pechei[duplicated(ID),stname]
    if (length(lnochal)[1]>0) cat('Duplicated species in catches','\n')
    dPechei=Pechei[paste(Pechei$CodEsp,Pechei[,stname])%in%ID[duplicated(ID)],]
    dPechei=dPechei[order(dPechei[,stname],dPechei$GENR_ESP),]
    dPechei
  }
 
  captures.range.check=function(captures,EspDev=NULL,path.checkit=NULL,ux11=FALSE,plotit=TRUE,
                                lsp=c('ENGR-ENC','SARD-PIL','TRAC-TRU','MICR-POU','SCOM-SCO','SPRA-SPR')){

		#***************************************************************
		#Check measurement ranges 
		#***************************************************************
		#All species
		#***************************************************************
		#Check mean length ranges in catches 
		#***************************************************************
		captures.noNA=captures[!(is.na(captures$LM)|captures$LM==0|
			is.na(captures$MOULE)|captures$MOULE==0),]
		cat('Largest fishes caught','\n')
		print(head(captures.noNA[rev(order(captures.noNA$LM)),]))
		cat('Smallest fishes caught','\n')
		print(head(captures.noNA[order(captures.noNA$LM),]))
		cat('Highest numbers of fish per kg','\n')
		print(head(captures.noNA[rev(order(captures.noNA$MOULE)),]))
		cat('Smallest numbers of fish per kg','\n')
		print(head(captures.noNA[order(captures.noNA$MOULE),]))

		#Assessed species
		#***************************************************************
		captures2=captures.noNA[captures.noNA$GENR_ESP%in%lsp,]
		captures2$GENR_ESP=as.character(captures2$GENR_ESP)
    if (plotit){
      if (ux11) x11()
      par(mfrow=c(2,2),mar=c(5,6,1,1))
      boxplot(LM~GENR_ESP,captures2,horizontal=TRUE,las=1,
              xlab='Mean length in cm')
      par(mar=c(5,1,1,1),yaxt='n')
      boxplot(PM~GENR_ESP,captures2,horizontal=TRUE,las=1,
              xlab='Mean weight in g')
      par(mar=c(5,6,1,1))  
      boxplot(MOULE~GENR_ESP,captures2,horizontal=TRUE,las=1,
              xlab='No. of fish per kg')
      par(mar=c(5,1,1,1),yaxt='n')  
      boxplot(1000/PM~GENR_ESP,captures2,horizontal=TRUE,las=1,
              xlab='No. of fish per kg 2')
    }

		cat('Smallest assessed fishes caught','\n')
		print(head(captures2[order(captures2$LM),]))
		cat('Largest assessed fishes caught','\n')
		print(head(captures2[rev(order(captures2$LM)),]))
		cat('Assessed fishes larger than .99 quantile','\n')
		print(captures2[captures2$LM>quantile(captures2$LM,.99),])

		ampm=aggregate(1/captures$PM,list(captures$GENR_ESP),mean)
		names(ampm)=c('sp','ampm')
		am=aggregate(captures$MOULE,list(captures$GENR_ESP),mean)
		names(am)=c('sp','am')
		amoule.check=merge(ampm,am)
		amoule.check$dm=amoule.check$am-amoule.check$ampm
		moule.check=(1000/captures$PM)-captures$MOULE

		cat('summary(MOULE-1/PM):',"\n")
		print(summary(moule.check))

		if (!is.null(EspDev)){
			#***************************************************************
			#Captures vs. EspDev
			#***************************************************************
			#species found only in EspDev
			lusp.spdev=unique(EspDev$CodEsp)[!unique(EspDev$CodEsp)%in%unique(captures$CodEsp)]
			#species found only in catches
			lusp.captures=unique(captures$CodEsp)[!unique(captures$CodEsp)%in%unique(EspDev$CodEsp)]
			lgen.eval=c('MICR','ENGR','SARD','TRAC','SCOM','SPRA')
			#assessed 'genus' found only in catches
			pmiss=lusp.captures[substr(lusp.captures,1,4)%in%lgen.eval]
			cat('Assessed genus found in catches and not in echotype description',"\n")
			print(pmiss)
			codesp.cor=captures[captures$CodEsp%in%pmiss,]
			#export assessed 'genus' found only in catches
			if (!is.null(path.checkit)&(dim(codesp.cor)[1]>0)){
				cat('Assessed genus found only in catches exported in:',
					"\n",'codesp-cor.csv',"\n")
				write.csv(codesp.cor,paste(path.checkit,'codesp-cor.csv',sep=''),
					row.names=FALSE)
			}
		}

		#Size categories check
		#***************************************************************
		table(captures$CodEsp)
		captures1=captures[captures$GENR_ESP%in%
			c('ENGR-ENC','SARD-PIL','TRAC-TRU','MICR-POU',
			'SCOM-SCO','SCOM-JAP'),]
		captures1$GENR_ESP=as.character(captures1$GENR_ESP)
		tsc=table(captures1$SIGNEP,captures1$GENR_ESP)
		cat('No. of fish per size category and main species exported in:',"\n",
			paste(path.checkit,'size_category_check.csv',sep=''),"\n")
		print(tsc)
		if (!is.null(path.checkit)){
		write.csv(tsc,
			paste(path.checkit,'size_category_check.csv',sep=''),
			row.names=FALSE)
		}
	}	

mens.range.check=function(mens,path.pb=NULL,onepage=TRUE,
                          ux11=FALSE,verbosit=TRUE,plotit=TRUE,
                          lsp=c('ENGR-ENC','SARD-PIL','TRAC-TRU','MICR-POU','SCOM-SCO','SPRA-SPR','TRAC-MED'),
                          nsp='GENR_ESP'){
  
  #***************************************************************
  #Check measurement ranges 
  #***************************************************************
  #All species
  #***************************************************************
  
  #table(mens$NBIND==0)
  mens.pb=mens[mens$NBIND==0|is.na(mens$NBIND),]
  if (!is.null(path.pb)){
    if (dim(mens.pb)[1]>0){ 
      cat(dim(mens.pb)[1],'0 or NA length measurements exported in:',"\n",
          paste(path.pb,'Lf_errors.csv',sep=''),"\n")
      write.csv(mens.pb,paste(path.pb,'Lf_errors.csv',sep=''),
                row.names=FALSE)
    }else{
      cat('No zero or NA in length measurements',"\n")
    }  
  }    
  #Check mean length ranges in catches 
  #***************************************************************
  mens.noNA=mens[!is.na(mens$Lcm),]
  if (verbosit){
    cat('Largest fishes caught','\n')
    print(head(mens.noNA[rev(order(mens.noNA$Lcm)),]))
    cat('Smallest fishes caught','\n')
    print(head(mens.noNA[order(mens.noNA$Lcm),]))
  }
  
  #Assessed species
  #***************************************************************
  mens2=mens.noNA[mens.noNA[,nsp]%in%lsp,]
  mens2[,nsp]=as.character(mens2[,nsp])
  if (verbosit){
    cat('Largest target fish caught','\n')
    mens2=mens2[rev(order(mens2$Lcm)),]
    print(mens2[1:20,])
    cat('Smallest target fishes caught','\n')
    mens2=mens2[order(mens2$Lcm),]
    print(mens2[1:20,])
  }
  #Length frequencies per species plots
  #***************************************************************
  mens2a=aggregate(mens2$NBIND,list(mens2$CAMPAGNE,mens2[,nsp],
                                    mens2$Lcm),sum)
  names(mens2a)=c('CAMPAGNE','GENR_ESP','Lcm','N')
  lyears=as.character(sort(unique(mens2a$CAMPAGNE)))
  lsp=as.character(unique(mens2a$GENR_ESP))
  if (plotit){
    if (!onepage){
      for (j in 1:length(lsp)){
        for (i in 1:length(lyears)){
          if ((i%in%c(1,7,13,19,25,31,37))&ux11){
            x11()
            par(mfrow=c(2,3))
          } 
          mens2aj=mens2a[mens2a$CAMPAGNE==lyears[i]&
                           mens2a$GENR_ESP==lsp[j],]
          if (i==1){
            plot(mens2aj$Lcm,mens2aj$N,type='l',
                 xlab='Length (cm)',ylab='No. of fish',
                 main=lsp[j],lwd=2,
                 ylim=range(mens2a[mens2a$GENR_ESP==lsp[j],
                                   'N']),xlim=range(
                                     mens2a[mens2a$GENR_ESP==lsp[j],'Lcm']))
          }else{
            lines(mens2aj$Lcm,mens2aj$N,type='l',
                  xlab='Length (cm)',ylab='No. of fish',
                  col=i,lty=i,lwd=2)
          }
          legend('topright',legend=lyears,
                 col=seq(length(lyears)),lty=seq(length(lyears)),
                 bg='white',lwd=2)
        }
      }
    }else{
      for (i in 1:length(lyears)){
        par(mar=c(3,3,3,1),mfrow=c(2,3))
        lsp=sort(lsp)
        for (j in 1:length(lsp)){
          if ((j%in%c(1,7,13,19,25,31,37))&ux11){
            x11()
            par(mfrow=c(2,3))
          } 
          mens2aj=mens2a[mens2a$CAMPAGNE==lyears[i]&
                           mens2a$GENR_ESP==lsp[j],]
          plot(mens2aj$Lcm,mens2aj$N,type='l',
               xlab='Length (cm)',ylab='No. of fish',
               main=paste(lyears[i],lsp[j]),lwd=2,
               ylim=range(mens2a[mens2a$GENR_ESP==lsp[j],
                                 'N']),xlim=range(
                                   mens2a[mens2a$GENR_ESP==lsp[j],'Lcm']))
        }
      }
    }
  }  
  
  #Size categories check
  #***************************************************************
  head(mens2)
  tsc=table(mens2$CATEG,mens2[,nsp])
  cat('No. of fish per size category and main species exported:',
      "\n")
  print(tsc)
}	



#***************************************************************
#function to remove or correct catches with null 'MOULE'n, mean length or total weight
#returns corrected dataframe
#***************************************************************

#Args:
#***************************************************************
# path.checkit: path to file with errors found in catch file.

#Outputs:
#***************************************************************
#If correctit=FALSE, 
#the file "CAPTURES_BAD1.csv' comprising erroneous rows is created in path.checkit
#If correctit=TRUE, 
#the files  "CAPTURES_BAD1.csv' is created in path.checkit, as well as the 
#"CAPTURES_BAD2.csv' file comprising erroneous rows after correction 
#***************************************************************

	check.catches=function(captures.comp,path.checkit=NULL,
		correctit=FALSE,removeit=FALSE,LW=NULL,spname='GENR_ESP',LW.tsa=NULL){

    #STRATE test
		#*******************
    unique(captures.comp$STRATE)

		#1st pass -----
		#******************
		N=length(captures.comp[,1])
		lin.ok=rep(T,N)
    lin.ok=!(is.na(captures.comp$LM)| is.na(captures.comp$MOULE)|
  		  is.na(captures.comp$PT) | is.na(captures.comp$NT)|
        is.na(captures.comp$PM)|
        is.infinite(captures.comp$LM)|is.infinite(captures.comp$MOULE)|
        is.infinite(captures.comp$PT)|is.infinite(captures.comp$NT)|
        is.infinite(captures.comp$PM)|
			  captures.comp$LM==0 |captures.comp$MOULE==0 | 
			  captures.comp$PT==0|captures.comp$NT==0|captures.comp$PM==0)
    #row numbers with LM or MOULE or PT<=0 or NA of infinite 
		(Npb=seq(N)[lin.ok==F])
		captures.bad=captures.comp[!lin.ok,]
    dim(captures.bad)
    captures.comp$quality=0

		if (!is.null(path.checkit)&(dim(captures.bad)[1]>0)){
      cat('1st pass: erroneous rows saved in',paste(path.checkit,'CAPTURES_BAD1.csv',
                                          sep=''),'\n')
			write.csv(captures.bad,paste(path.checkit,
				'CAPTURES_BAD1.csv',sep=''),row.names=FALSE)
		}
  
		if (correctit){
      cat('Erroneous rows re-computed if ancillary data available','\n')
			#MOULE correction
			#*******************
      summary(captures.comp[!is.infinite(captures.comp$MOULE),'MOULE'])
      selbadMOULE=captures.comp$MOULE==0|
  			is.na(captures.comp$MOULE)|is.infinite(captures.comp$MOULE)
			captures.badMOULE=captures.comp[selbadMOULE,]
			dim(captures.badMOULE)
			captures.comp[selbadMOULE,'MOULE']=captures.comp[selbadMOULE,
				'NT']/captures.comp[selbadMOULE,'PT']
      captures.comp[selbadMOULE,'quality']=5
			captures.badMOULE=captures.comp[selbadMOULE,]
			dim(captures.badMOULE)
      summary(captures.comp[!is.infinite(captures.comp$MOULE),'MOULE'])
      captures.comp[captures.comp$MOULE==1000,]
      
			#NT correction
			#********************
      summary(captures.comp[!is.infinite(captures.comp$NT),'NT'])
      selbadNT=captures.comp$NT==0|is.na(captures.comp$NT)|
        is.infinite(captures.comp$NT)
      captures.badNT=captures.comp[selbadNT,]
			dim(captures.badNT)
			captures.comp[selbadNT,'NT']=captures.comp[selbadNT,'MOULE']*
        captures.comp[selbadNT,'PT']
      captures.comp[selbadNT,'quality']=5
			captures.badNT=captures.comp[selbadNT,]
			dim(captures.badNT)
      summary(captures.comp[!is.infinite(captures.comp$NT),'NT'])
      
			#PT correction
			#*******************
      selbadPT=captures.comp$PT==0|is.na(captures.comp$PT)|
        is.infinite(captures.comp$PT)
			captures.badPT=captures.comp[selbadPT,]
			dim(captures.badPT)
			captures.comp[selbadPT,'PT']=captures.comp[selbadPT,'NT']/
        captures.comp[selbadPT,'MOULE']
      captures.comp[selbadPT,'quality']=5      
			captures.badPT=captures.comp[selbadPT,]
			dim(captures.badPT)

      #PM correction
			#************************
      summary(captures.comp[!is.infinite(captures.comp$NT),'PM'])
      selbadPM=captures.comp$PM==0|is.na(captures.comp$PM)|
        is.infinite(captures.comp$PM)
			captures.badPM=captures.comp[selbadPM,]
			dim(captures.badPM)
      #correction using MOULE
			captures.comp[selbadPM,'PM']=1000/captures.comp[selbadPM,'MOULE']
      selbadPM=captures.comp$PM==0|is.na(captures.comp$PM)|
        is.infinite(captures.comp$PM)
      #correction using PT and NT      
  		captures.comp[selbadPM,'PM']=captures.comp[selbadPM,'PT']/
        captures.comp[selbadPM,'NT']
      captures.comp[selbadPM,'quality']=5
      #Remaining errors
      selbadPM=captures.comp$PM==0|is.na(captures.comp$PM)|
        is.infinite(captures.comp$PM)
			captures.badPM=captures.comp[selbadPM,]
			dim(captures.badPM)
      summary(captures.comp[!is.infinite(captures.comp$PM),'PM'])
      captures.comp[captures.comp$PM==0.05,]

			#2nd pass -----------
			#***************************
			N=length(captures.comp[,1])
			lin.ok=rep(T,N)
      lin.ok=!(is.na(captures.comp$LM)| is.na(captures.comp$MOULE)|
			  is.na(captures.comp$PT) | is.na(captures.comp$NT)|
        is.na(captures.comp$PM)|
        is.infinite(captures.comp$LM)|is.infinite(captures.comp$MOULE)|
        is.infinite(captures.comp$PT)|is.infinite(captures.comp$NT)|
        is.infinite(captures.comp$PM)|
			  captures.comp$LM==0 |captures.comp$MOULE==0 | 
			  captures.comp$PT==0|captures.comp$NT==0|captures.comp$PM==0)
			#row numbers with LM or MOULE or PT<=0 or NA 
			(Npb=seq(N)[lin.ok==F])
			(captures.bad=captures.comp[!lin.ok,])
      dim(captures.bad)

  		if (!is.null(path.checkit)&(dim(captures.bad)[1]>0)){
        cat('2nd pass: remaining erroneous rows saved in',paste(path.checkit,'CAPTURES_BAD2.csv',
                        sep=''),'\n')
				write.csv(captures.bad,paste(path.checkit,
					'CAPTURES_BAD2.csv',sep=''),row.names=FALSE)
			}
      if ((dim(captures.bad)[1]>0)&(!is.null(LW))){      
      cat('Erroneous mean weights re-computed based on length-weight relationships provided',
          '\n')
        # Eventually, correct missing parameters with length-weight keys ----
        #add LW parameters to catches
    	  #*****************************
        if (!is.null(LW.tsa)){
          # if global LWK provided, build mixed LWK 
          # by adding LW.tsa species not found in LW
          # sizeCategory NOT considered here, as LWK are for all size range
          #names(LW.tsa)[5]='GENR_ESP'
          LW0=LW[,names(LW)!='CodEsp2']
          sid0=paste(captures.comp$NOSTA,captures.comp$GENR_ESP,sep='-')
          sid1=paste(captures.comp$GENR_ESP,sep='-')
          sid2=paste(LW$GENR_ESP,sep='-')
          sid3=paste(LW.tsa$GENR_ESP,sep='-')
          sp.miss=unique(sid1[!sid1%in%sid2])
          sp.add=sp.miss[sp.miss%in%sid3]
          LW=rbind(LW0,LW.tsa[sid3%in%sp.add,names(LW0)])
        }
        
        captures.comp=merge(captures.comp,unique(LW[,c(spname,'a','b')]),
          by.x='GENR_ESP',by.y='GENR_ESP',all.x=TRUE)
          
        #PM correction
  		  #**********************
        #Correction using weight-length relationship and mean length
        summary(captures.comp[!is.infinite(captures.comp$PM),'PM'])
        sel.badPM=captures.comp$PM==0|
  			  is.na(captures.comp$PM)|is.infinite(captures.comp$PM)
			  captures.badPM=captures.comp[sel.badPM,]
  		  dim(captures.badPM)
        captures.comp[sel.badPM,'PM']=captures.comp[sel.badPM,'a']*
          captures.comp[sel.badPM,'LM']^captures.comp[sel.badPM,'b']
        captures.comp[sel.badPM,'quality']=5
        sel.badPM=captures.comp$PM==0|is.na(captures.comp$PM)|
          is.infinite(captures.comp$PM)  
			  captures.badPM=captures.comp[sel.badPM,]
			  dim(captures.badPM)
        summary(captures.comp[!is.infinite(captures.comp$PM),'PM'])
 
        #NT correction
    	  #*************************
        summary(captures.comp[!is.infinite(captures.comp$NT),'NT'])
        sel.badNT=captures.comp$NT==0|
  			  is.na(captures.comp$NT)|is.infinite(captures.comp$NT)
			  captures.badNT=captures.comp[sel.badNT,]
  		  dim(captures.badNT)
        captures.comp[sel.badNT,'NT']=captures.comp[sel.badNT,'PT']/
          (captures.comp[sel.badNT,'PM']/1000)
        captures.comp[sel.badNT,'quality']=5
        sel.badNT=captures.comp$NT==0|is.na(captures.comp$NT)|
          is.infinite(captures.comp$NT)  
			  captures.badNT=captures.comp[sel.badNT,]
			  dim(captures.badNT)
        summary(captures.comp[!is.infinite(captures.comp$NT),'NT'])
        
        #MOULE correction
        #*********************
        summary(captures.comp[!is.infinite(captures.comp$MOULE),'MOULE'])
        sel.badMOULE=captures.comp$MOULE==0|
  			  is.na(captures.comp$MOULE)|is.infinite(captures.comp$MOULE)
			  captures.badMOULE=captures.comp[sel.badMOULE,]
  		  dim(captures.badMOULE)
        captures.comp[sel.badMOULE,'MOULE']=1000/captures.comp[sel.badMOULE,
          'PM']
        captures.comp[sel.badMOULE,'quality']=5
        sel.badMOULE=captures.comp$MOULE==0|
    		  is.na(captures.comp$MOULE)|is.infinite(captures.comp$MOULE)
			  captures.badMOULE=captures.comp[sel.badMOULE,]
  		  dim(captures.badMOULE)
        summary(captures.comp[!is.infinite(captures.comp$MOULE),'MOULE'])
        
        #LM correction
    	  #*********************
        #Correction using weight-length relationship and mean length
        summary(captures.comp[!is.infinite(captures.comp$LM),'LM'])
        sel.badLM=captures.comp$LM==0|
  			  is.na(captures.comp$LM)|is.infinite(captures.comp$LM)
			  captures.badLM=captures.comp[sel.badLM,]
  		  dim(captures.badLM)
        captures.comp[sel.badLM&(!is.na(captures.comp$a)),'LM']=
          (captures.comp[sel.badLM&(!is.na(captures.comp$a)),'PM']/
          captures.comp[sel.badLM&(!is.na(captures.comp$a)),'a'])^(
            1/captures.comp[sel.badLM&(!is.na(captures.comp$a)),'b'])
        captures.comp[sel.badLM&(!is.na(captures.comp$a)),'quality']=5
        sel.badLM=captures.comp$LM==0|is.na(captures.comp$LM)|
          is.infinite(captures.comp$LM)  
			  captures.badLM=captures.comp[sel.badLM,]
			  dim(captures.badLM)
        summary(captures.comp[!is.infinite(captures.comp$LM),'LM'])
        
  			if (!is.null(path.checkit)&(dim(captures.bad)[1]>0)){
          cat('3rd pass: remaining erroneous rows saved in',paste(path.checkit,'CAPTURES_BAD_afterLWcor.csv.csv',
                        sep=''),'\n')          
     			write.csv(captures.bad,paste(path.checkit,
			  		'CAPTURES_BAD_afterLWcor.csv',sep=''),row.names=FALSE)
			  }
  		}  
			dim(captures.comp)
			dim(unique(captures.comp))
      lin.ok=rep(T,N)
      lin.ok=!(is.na(captures.comp$LM)| is.na(captures.comp$MOULE)|
  		  is.na(captures.comp$PT) | is.na(captures.comp$NT)|
        is.na(captures.comp$PM)|
        is.infinite(captures.comp$LM)|is.infinite(captures.comp$MOULE)|
        is.infinite(captures.comp$PT)|is.infinite(captures.comp$NT)|
        is.infinite(captures.comp$PM)|
			  captures.comp$LM==0 |captures.comp$MOULE==0 | 
			  captures.comp$PT==0|captures.comp$NT==0|captures.comp$PM==0)
		}
    if (removeit){
		  # Remove lines with errors if removit -----------
		  #*********************
      cat('Erroneous rows remaining after correction(s) removed','\n')
		  captures.comps=captures.comp[lin.ok,]
		  dim(captures.comps)
		  dim(unique(captures.comps))
    }else{
      captures.comps=captures.comp
    }  
	list(captures.OK=captures.comps,captures.PB=captures.comp[!lin.ok,])
	}

#**********************************
#Haul geographic positions check
#   Function to check that:
#       1. All hauls have geographic positions
#       2. All hauls have distinct geographic positions. 
#        If not and correctit==TRUE, hauls with same positions are jittered 
#        by a small constant to allow for geostatistical analysis
#**********************************

	haul.positions.check=function(captures,correctit=TRUE,
		path.checkit=NULL){

		#hauls with NA positions
		#***************************************************************
		posch.na=is.na(captures$LATF)|is.na(captures$LONF)|
			captures$LATF==0|captures$LONF==0

		if (T%in%posch.na){
			cat('Hauls without geographic positions:',"\n",
				as.character(captures$NOSTA[posch.na]),"\n")
		}else{
  		cat('All hauls have geographic positions',"\n")
		}
    
		#Haul positions check: hauls with same position?
		#*************************************************************** 
		lcruise=unique(captures$CAMPAGNE)
		hpos.check=lapply(X=seq(length(lcruise)),FUN=id.loc,df=captures,
			stname='NOCHAL')
		hpos.check.df=captures[NULL,c('CAMPAGNE','NOCHAL','LONF','LATF')]
		for (i in 1:length(hpos.check)){	
			hpos.checki=hpos.check[[i]]
			if (!is.na(hpos.checki)){
				hpos.check.df=rbind(hpos.check.df,
					hpos.checki$same.hauls[,c('CAMPAGNE','NOCHAL',
						'LONF','LATF')])
			}
		}

		#Correct positions for hauls with similar positions
		#***************************************************************
		if (correctit){
			pecore=jitter(captures,hpos.check.df)
			captures=pecore$captures.pcor
			lcruise=unique(captures$CAMPAGNE)
			hpos.check=lapply(X=seq(length(lcruise)),FUN=id.loc,
				df=captures,stname='NOCHAL')
			hpos.check.df=captures[NULL,c('CAMPAGNE','NOCHAL','LONF','LATF')]
			for (i in 1:length(hpos.check)){	
				hpos.checki=hpos.check[[i]]
				if (!is.na(hpos.checki)){
					hpos.check.df=rbind(hpos.check.df,
						hpos.checki$same.hauls[,c('CAMPAGNE','NOCHAL',
							'LONF','LATF')])
				}
			}
		}	

		if (dim(hpos.check.df)[1]>0){
			cat('Trawl hauls with identical geographic positions exported in:',
			 "\n",'same_haul_positions.csv',"\n")
		}else{
      cat('No trawl haul with identical geographic positions',"\n")
		}  
		if (!is.null(path.checkit)){

			#export haul description
				write.csv(hpos.check.df,
					paste(path.checkit,'same_haul_positions.csv',sep=''),
					row.names=FALSE)
		}
	captures
	}

	virtual.hauls.positions=function(captures,correctit=TRUE){
		if (correctit){ 
			#... or adds mean positions of true hauls to virtual hauls 
			#(for visualisation purposes)?
			#***************************************************************-
			#"Correction" of missing haul positions
			vPeche=captures[captures$ETAT=='FICTIF'&
				(is.na(captures$LATF)|is.na(captures$LONF)),]
			vcruise=sort(unique(vPeche$CAMPAGNE))
			for (i in 1:length(vcruise)){
				vPechei=vPeche[vPeche$CAMPAGNE==vcruise[i],]
				lstratesi=unique(vPechei$STRATE)
				for (j in 1:length(lstratesi)){
					vPechei[vPechei$STRATE==lstratesi[j],c('LATF')]=
						mean(captures[captures$CAMPAGNE==vcruise[i]&
						captures$STRATE==lstratesi[j],'LATF'],
						na.rm=TRUE)
					vPechei[vPechei$STRATE==lstratesi[j],c('LONF')]=
						mean(captures[captures$CAMPAGNE==vcruise[i]&
						captures$STRATE==lstratesi[j],'LONF'],
						na.rm=TRUE)
				}
			}
			dim(captures)
			captures=rbind(captures[!(captures$ETAT=='FICTIF'&
				(is.na(captures$LATF)|is.na(captures$LONF))),],
				vPeche)
			cat('Corrected with average haul position',"\n")
		}else {
			#remove 'virtual' hauls (without positions)? ...
			#***************************************************************
			captures=captures[captures$ETAT!='FICTIF',]
			cat('Hauls discarded',"\n")
		}
	captures
	}

#***************************************************************---
#Check acoustic data
#   Function to check that:
#      1. there is no esdus with negative TOTAL sA
#      2. there is no esdus with negative sA in echotypes
#***************************************************************

	check.esdudev=function(ESDUDEV,Ndev){
			
		#Energy to Sa conversion for PEL00 and PEL01
		#***************************************************************
		ESDUDEV[ESDUDEV$CAMPAGNE%in%c('PEL00','PEL01'),
			paste('D',Ndev,sep='')]=ESDUDEV[ESDUDEV$CAMPAGNE%in%
			c('PEL00','PEL01'),paste('D',Ndev,sep='')]*0.02327 

		#Esdus with null energy 
		D0=ESDUDEV[ESDUDEV$TOTAL==0,]
		#number of esdus with null sA
		dim(D0)[1]
		Dneg=ESDUDEV[ESDUDEV$TOTAL<0,]
		#cat('esdus with negative sA:',Dneg$TC,'\n')
		#check flag
		if (length(unique(ESDUDEV$FLAG))>1) cat('Unflagged esdus in EsduDev','\n')

		#Acoustic data
		#*************************************************************** 
		#Acoustic densities null or negative?
		#***************************************************************-
		dim(ESDUDEV)
		ds=ESDUDEV[,paste('D',Ndev,sep='')]
    Dineg=ESDUDEV[,paste('D',Ndev,sep='')]<0
		Dineg=ds<0
    if (is.null(dim(Dineg))) {lin.neg=sum(Dineg)
                              }else {lin.neg=rowSums(Dineg)}
    length(lin.neg)
    table(lin.neg)  
	  if (dim(Dneg)[1]>0){
      cat('esdus with negative TOTAL sA:',Dneg$TC,'\n')
	  }else{  
      cat('No esdus with negative TOTAL sA','\n')
	  }
    if (dim(ESDUDEV[lin.neg,])[1]>0){
      cat('Esdus with negative sA in echotypes:','\n')
		  print(ESDUDEV[as.logical(lin.neg),])
    }else{
      cat('No esdus with negative sA in echotypes','\n')
    }  
    
		#Comparison between total energy and energy in Dev
		#***************************************************************-------------------------*
		if (is.null(dim(ds))){
		  Devtot2=ds
		}else{ 
		  Devtot2=apply(ESDUDEV[,paste('D',Ndev,sep='')],1,sum)
		}  
		dSa.scrut2=ESDUDEV$TOTAL-Devtot2

    cat('Difference btw. TOTAL sA and sum of sA in echotypes is null?','\n')
		summary(ESDUDEV$TOTAL)
		summary(Devtot2)
		summary(dSa.scrut2)
		table(dSa.scrut2>=0)
	}



#***************************************************************
#Remove or correct biological samples of species present in 'captures'
#and not in 'mensurations'
#***************************************************************

	discard.catches=function(captures.comps){

	if (TS.fL){
		mens.extra=captures.comps[paste(captures.comps$NOSTA,
			captures.comps$CodEsp)%in%df1$lnomens,
			c('CAMPAGNE','STRATE','NOSTA','GENR_ESP','SIGNEP','LM',
			'NT')]
		names(mens.extra)[names(mens.extra)=='LM']='Lcm'
		names(mens.extra)[names(mens.extra)=='NT']='NBIND'
		mens.extra$POIDSTAILL=NA	
		mens.extra$CodEsp=paste(mens.extra$GENR_ESP,mens.extra$STRATE,
			mens.extra$SIGNEP,sep='-')	
		if (checkit){
			cat('Species with no length measurements exported in sp_Lmens_check.csv',"\n")
			write.csv(mens.extra,
				paste(path.checkit,'sp_Lmens_check.csv',sep=''),
				row.names=FALSE)
		}	

		if (correctit){
			mens2=rbind(mens,mens.extra)
		}else{
			mens2=mens
		}
	}

	dim(mens.extra)

	#number of hauls without length measurements
	#***************************************************************
	uhauls=sort(unique(as.character(mens$NOSTA)))
	table(substr(uhauls,1,1))
	uhauls.extra=sort(unique(as.character(mens.extra$NOSTA)))
	table(substr(uhauls.extra,1,1))
	cap.hc=unique(captures.comps[,c('CAMPAGNE','NOSTA')])	
	mens.hc=unique(mens[,c('CAMPAGNE','NOSTA')])	
	mens.hc.extra=unique(mens.extra[,c('CAMPAGNE','NOSTA')])
	#Total number of hauls
	table(as.character(cap.hc$CAMPAGNE))
	#Number of hauls with length measurements
	table(as.character(mens.hc$CAMPAGNE))
	#Number of hauls without length measurements	
	table(as.character(mens.hc.extra$CAMPAGNE))

	}

#***************************************************************
#Low-level functions for correcting haul positions
#***************************************************************

	id.loc=function(k,df,stname='Nosta',xname='LONF',yname='LATF',
		iyear='CAMPAGNE'){
		lcruise=sort(unique(df[,iyear]))
		dfi=unique(df[df[,iyear]==lcruise[k],c(iyear,stname,
			xname,yname)])
		mcos=cos(mean(dfi[,yname]*pi/180))
		dfi$x=dfi[,xname]*mcos*60
		dfi$y=dfi[,yname]*60
		dfi=dfi[order(dfi[,stname]),]
		d<-sqrt(outer(dfi$x,dfi$x,"-")*outer(dfi$x,dfi$x,"-")+
		outer(dfi$y,dfi$y,"-")*outer(dfi$y,dfi$y,"-"))
		dimnames(d)[[1]]=dfi[,stname]
		dimnames(d)[[2]]=dfi[,stname]

		dlt=d[lower.tri(d)]
		isid=TRUE%in%(dlt==0)

		mnames2=outer(dfi[,stname],dfi[,stname],paste,sep=',')
		mnames2=mnames2[lower.tri(mnames2)]
		lids=mnames2[dlt==0]
		if(length(lids)!=0){
			ids=data.frame(t(unlist(strsplit(lids,split=','))))
			cids=c(as.character(unlist(ids[,1:2])))
			dfiloc=dfi[dfi[,stname]%in%cids,]
		}else dfiloc=NA
	list(same.hauls=dfiloc)
	}

	jitter=function(captures,hpos.check.df,eps=0.0001){
		lc=unique(hpos.check.df$CAMPAGNE)
		if (length(lc)>0){

		chid=paste(hpos.check.df$CAMPAGNE,hpos.check.df$NOCHAL)
		selcor=paste(captures$CAMPAGNE,captures$NOCHAL)%in%chid
		captures.ncor=captures[!selcor,]
		captures.acor=captures[selcor,]
		for (i in (1:length(lc))){
			hpos.check.dfi=hpos.check.df[hpos.check.df$CAMPAGNE==lc[i],]
			capturesi=captures.acor[captures.acor$CAMPAGNE==lc[i],]
			lch=unique(hpos.check.dfi$NOCHAL)
			hpos.check.dfi$LONFcor=hpos.check.dfi$LONF
			hpos.check.dfi$LATFcor=hpos.check.dfi$LATF
			for (j in seq(1,length(lch),2)){
				hpos.check.dfi[hpos.check.dfi$NOCHAL==lch[j],
					'LONFcor']=
					hpos.check.dfi[hpos.check.dfi$NOCHAL==lch[j+1],
					'LONF']+eps
				capturesi[capturesi$NOCHAL==lch[j],'LONF']=
					hpos.check.dfi[hpos.check.dfi$NOCHAL==lch[j+1],
					'LONF']+eps	
				hpos.check.dfi[hpos.check.dfi$NOCHAL==lch[j],
					'LATFcor']=
					hpos.check.dfi[hpos.check.dfi$NOCHAL==lch[j+1],
					'LATF']+eps				
				capturesi[capturesi$NOCHAL==lch[j],'LATF']=
					hpos.check.dfi[hpos.check.dfi$NOCHAL==lch[j+1],
					'LATF']+eps	
				if (j==1){
					hpos.check.dfi.cor=hpos.check.dfi
				}else{
					hpos.check.dfi.cor=rbind(hpos.check.dfi.cor,
						hpos.check.dfi)
				}
			}
			if (i==1){
					hpos.check.df.cor	=hpos.check.dfi.cor
					captures.cor=capturesi
				}else{
					hpos.check.df.cor=rbind(hpos.check.df.cor,
						hpos.check.dfi.cor)
					captures.cor=rbind(captures.cor,capturesi)
			}
		}
		dim(captures.cor)
		dim(captures[selcor,])
		captures.pcor=rbind(captures.ncor,captures.cor)
		dim(captures);dim(captures.pcor)
		}else{
			hpos.check.df.cor=hpos.check.df
			captures.pcor=captures
		}
	list(hpos.check.df.cor=hpos.check.df.cor,captures.pcor=captures.pcor)
	}

  mensi.check=function(mensi){
    mensiL=aggregate(mensi$NBIND,list(mensi$NOSTA,mensi$CodEsp2,mensi$Lcm),sum)
    names(mensiL)=c('NOSTA','CodEsp2','Lcm','NBIND')
    mensiW=aggregate(mensi$POIDSTAILLE,list(mensi$NOSTA,mensi$CodEsp2,mensi$Lcm),unique)
    boxplot(mensi$PM)
    summary(mensi$PM)
    mensi.light=mensi[mensi$PM<0.012,]
    plot(mensi.light$Lcm,1000*mensi.light$PM,main=lspcodes.mens[i],
      xlab='Length (cm)',ylab='Weight (g)')
    names(mensiW)=c('NOSTA','CodEsp2','Lcm','POIDSTAILLE')
    mensia=merge(mensiL,mensiW)
  }

  raptri.check=function(mens){
        #How does raptri works?
        unique(mens$CATEG)
        mens$pi=mens$NBIND/mens$NECHANT
        mens$Wi=mens$POIDSTAILLE*mens$NBIND/mens$NECHANT
        mensa.Wcheck=aggregate(mens[,c('Wi','pi','NBIND')],
          list(mens$GENR_ESP,mens$NOSTA),sum)
        names(mensa.Wcheck)=c('GENR_ESP','NOSTA','PTOT','ptot','NTOT')
        mensa.Wcheck=merge(mensa.Wcheck,unique(mens[,c('GENR_ESP','NOSTA',
                                                       'POIDSECHANT','NECHANT')]))
        mensa.Wcheck$dPTOT=mensa.Wcheck$PTOT-mensa.Wcheck$POIDSECHANT
        mensa.Wcheck$dNTOT=mensa.Wcheck$NTOT-mensa.Wcheck$NECHANT
        summary(mensa.Wcheck$PTOT-mensa.Wcheck$POIDSECHANT)
        summary(mensa.Wcheck$NTOT-mensa.Wcheck$NECHANT)
        summary(mensa.Wcheck$ptot)
        mens.stations.pb=mensa.Wcheck[mensa.Wcheck$ptot!=1,'NOSTA']
        table(mensa.Wcheck$ptot==1)
        mens.pb=mens[mens$NOSTA%in%mens.stations.pb,]
        plot(mensa.Wcheck$NTOT,mensa.Wcheck$NECHANT)
        mensa.Wcheck
        unique(mens$POIDSECHANT)
  }

#***************************************************************
# Result check  
#***************************************************************
  #Check numbers at lengths 
	#------
  NLclass.check=function(NW.esdu.sp.size.longs){
  
	  NW.esdu.sp.longa=aggregate(NW.esdu.sp.size.longs[,c('W','N')],
		  list(NW.esdu.sp.size.longs$Esdu,NW.esdu.sp.size.longs$sp),sum)	
	  names(NW.esdu.sp.longa)[seq(2)]=c('Esdu','sp')
	  dim(NW.esdu.sp.longa)
	  head(NW.esdu.sp.size.longs)

    if (T%in%('CodEsp2'%in%names(NW.esdu.sp.size.longs))){
	    NW.esdu.csp.longaNtot=aggregate(NW.esdu.sp.size.longs[,c('Ntot')],
		    list(NW.esdu.sp.size.longs$Esdu,NW.esdu.sp.size.longs$sp,
		    NW.esdu.sp.size.longs$CodEsp2),unique)	
	    names(NW.esdu.csp.longaNtot)=c('Esdu','sp','codsp','Ntot')
    }
    
	  NW.esdu.sp.longaNtot=aggregate(NW.esdu.csp.longaNtot[,c('Ntot')],
		  list(NW.esdu.csp.longaNtot$Esdu,NW.esdu.csp.longaNtot$sp),sum)	
	  names(NW.esdu.sp.longaNtot)=c('Esdu','sp','Ntot')

	  NW.esdu.sp.longa=merge(NW.esdu.sp.longa,NW.esdu.sp.longaNtot,
		  by.x=c('Esdu','sp'),by.y=c('Esdu','sp'))
  
    #Total abundance -sum(abundances at lengths)
	  NW.esdu.sp.longa$Nsp.check=NW.esdu.sp.longa$N-NW.esdu.sp.longa$Ntot
	  print(summary(NW.esdu.sp.longa$Nsp.check))
	  boxplot(NW.esdu.sp.longa$Nsp.check)
    NW.esdu.sp.longa[NW.esdu.sp.longa$Nsp.check>1,]

    #Check total abundance/biomass/species
    NWtot1=aggregate(NW.esdu.sp.longa[,c('W','N')],list(NW.esdu.sp.longa$sp),sum)
    names(B.dev.sp.esdu.df)
    NWtot2=aggregate(B.dev.sp.esdu.df[,c('BB','BN')],list(B.dev.sp.esdu.df$GENR_ESP),
      sum)
    NWtot.check=merge(NWtot1,NWtot2)
    NWtot.check$dW=NWtot.check$W/1000-NWtot.check$BB
    NWtot.check$dN=NWtot.check$N-NWtot.check$BN                            
    NWtot.check$mWg1=(NWtot.check$W*1000)/NWtot.check$N
    NWtot.check$mWg2=NWtot.check$BB*1e6/NWtot.check$BN
    names(NWtot.check)[1]='sp'
  }  

   check.ops=function(Operations,Pechei2Echobase,exceptGear=NULL){
    #Check no. of operations vs. no. of operations in catches
    cat('No. of operations',length(Operations$operationId),'\n')
    cat('No. of operations in catches',
        length(unique(Pechei2Echobase$operationId)),'\n')
    opnotincatch=Operations$operationId[!Operations$operationId%in%unique(Pechei2Echobase$operationId)]
    Pechei2Echobase[Pechei2Echobase$NOSTA%in%opnotincatch,]
    catchnotinop=unique(Pechei2Echobase$operationId)[!unique(Pechei2Echobase$operationId)%in%Operations$operationId]
    if (!is.null(exceptGear)){
      catchnotinop=catchnotinop[catchnotinop%in%Operations[!Operations$gearCode%in%exceptGear,c("operationId")]]
    }
    if (length(catchnotinop)>0) stop('Operations found in catches and not in operations: ',
                                     paste(catchnotinop,collapse='\n'),'\n')
    # operations in operations and not in catches= hauls without catches
    Operations[Operations$operationId%in%opnotincatch,]
    Pechei2Echobase=merge(Pechei2Echobase,Operations[,
            c('operationId','vesselName')],by.x='operationId',by.y='operationId')
    list(opnotincatch=opnotincatch,catchnotinop=catchnotinop)
  }

# check abundance at length per species

check.abundance.length.species=function(list.sp.Nsize,type='N',eps=1){
  if (type=='N'){
  for (i in 1:length(list.sp.Nsize)){
    li=list.sp.Nsize[[i]]
    unique(li$Esdu)
    print(unique(li$CodEsp2))
    head(li)
    Ncheck=apply(li[,!names(li)%in%c('Esdu','CodEsp2','N')],1,sum)-li$N
    summary(Ncheck)
    #boxplot(Ncheck)
    li.pb=li[abs(Ncheck)>=eps,]
    cat('No. of errors','\n') 
    print(dim(li.pb)[1])
    #print(li.pb)
    if (i==1){
      li.pb.db=cbind(li.pb[,1:3],N2=apply(li.pb[,!names(li.pb)%in%c('Esdu','CodEsp2','N')],1,sum))
      lsp.biomL=unique(li$CodEsp2)
    }else{
      li.pb.db=rbind(li.pb.db,cbind(li.pb[,1:3],N2=apply(li.pb[,!names(li.pb)%in%c('Esdu','CodEsp2','N')],1,sum)))
      lsp.biomL=c(lsp.biomL,unique(li$CodEsp2))
    }
  }
  }
  else if (type=='NW'){
    for (i in 1:length(list.sp.Nsize)){
      li=list.sp.Nsize[[i]]
      unique(li$Esdu)
      print(unique(li$CodEsp2))
      head(li)
      Ncheck=apply(li[,substr(names(li),1,2)=='NL'],1,sum)-li$N
      summary(Ncheck)
      #boxplot(Ncheck)
      li.pb=li[abs(Ncheck)>=eps,]
      cat('No. of errors','\n') 
      print(dim(li.pb)[1])
    }
    li.pb.db=li.pb
  }
  li.pb.db
}  


size.category.check=function(Pechei,dflm=NULL,
                             spsel=c("ENGR-ENC","SARD-PIL","SCOM-JAP","MICR-POU",
                                     "SCOM-SCO","SPRA-SPR","TRAC-TRU"),
                             method='breaks',breaks='means',mensi=NULL,
                             LMAB.recompute=FALSE){

  if (!is.null(dflm)){
    spsel=dflm$sp
  }
  Pecheis=Pechei[Pechei$GENR_ESP%in%spsel,]
  Pecheins=Pechei[!Pechei$GENR_ESP%in%spsel,]
  # check NA
  PecheisPb=Pecheis[!complete.cases(Pecheis),]
  if (dim(PecheisPb)[1]>1){
    stop('Catches with NA, please correct: ',
         paste(PecheisPb$NOSTA,PecheisPb$CodEsp,collapse=' '),'\n')
  }
  cat('Original unique size categories:','\n')
  print(table(as.character(Pechei$SIGNEP)))
  cat('Original unique size categories for main species:','\n')
  print(table(as.character(Pechei[Pechei$GENR_ESP%in%spsel,'GENR_ESP']),
        as.character(Pechei[Pechei$GENR_ESP%in%spsel,'SIGNEP'])))
  if (method=='breaks'&!is.null(dflm)){
    Pecheis$SIGNEP=as.character(Pecheis$SIGNEP)
    Pecheis$scat='0'
    Pecheis$scat=as.character(Pecheis$scat)
    if (breaks=='means'){
      nb='mMeans'
    }else if (breaks=='quantiles'){
      nb='mQuant'
    }else if (breaks=='manual'){
      nb='mBreaks'
    }
    dflmsp=unique(dflm$sp)
    for (i in 1:length(dflmsp)){
      dflmi=dflm[dflm$sp==dflmsp[i],]
      if (sum(is.na(dflmi[,nb]))==0){
        if (dim(dflmi)[1]==1){
          Pecheis[
            Pecheis$GENR_ESP==as.character(dflmsp[i])&
              Pecheis$LM>dflm[i,nb],'scat']='G'
          # Pecheis[
          #   Pecheis$GENR_ESP==as.character(dflmsp[i])&
          #     Pecheis$LM>dflm[i,nb],]
          print(
            Pecheis[Pecheis$GENR_ESP==as.character(dflm$sp[i])&
                      Pecheis$scat=='G',c('GENR_ESP','scat','SIGNEP')])
        }else if (dim(dflmi)[1]==2){
          Pecheis[
            Pecheis$GENR_ESP==as.character(dflmsp[i])&
              Pecheis$LM<dflmi[1,nb],'scat']='P'
          Pecheis[
            Pecheis$GENR_ESP==as.character(dflmsp[i])&
              Pecheis$LM>dflmi[2,nb],'scat']='G'
          print(
            Pecheis[Pecheis$GENR_ESP==as.character(dflmsp[i])&
                      Pecheis$scat=='P',c('GENR_ESP','scat','SIGNEP')])
          print(
            Pecheis[Pecheis$GENR_ESP==as.character(dflmsp[i])&
                      Pecheis$scat=='G',c('GENR_ESP','scat','SIGNEP')])
        }else{
          stop('No. of size categories larger than 3 for',dflmsp[i])
        }
      }else{
        Pecheis[Pecheis$GENR_ESP==as.character(dflmsp[i]),'scat']='0'
      }
    }
    Pecheis$SIGNEP=Pecheis$scat
  }
  # Aggregate rows
  duplicated.speciesIncatches.check(Pecheis)
  Pecheis$CodEsp=paste(Pecheis$GENR_ESP,Pecheis$STRATE,
                       Pecheis$SIGNEP,sep='-')
  rac=aggregate.catches(Pecheic=Pecheis,spname='CodEsp',stname='NOSTA')
  names(rac)
  rac$dID
  Pecheis=rac$Pechei
  dim(Pecheis)
  Pecheis=Pecheis[order(Pecheis$NOSTA,Pecheis$GENR_ESP),]
  Pecheis$CodEsp=paste(Pecheis$GENR_ESP,Pecheis$STRATE,Pecheis$SIGNEP,sep='-')
  duplicated.speciesIncatches.check(Pecheis)
  # Eventually, recompute LM, biomass and abundance based on abundances-at-length
  if (LMAB.recompute){
    if (!is.null(mensi)){
      head(mensi)
      mensis=mensi[mensi$GENR_ESP%in%spsel,]
      mensisaNlcsp=aggregate(mensis$NBIND,list(NOSTA=mensis$NOSTA,GENR_ESP=mensis$GENR_ESP,CATEG=mensis$CATEG),sum)
      names(mensisaNlcsp)[4]='Ntotcsp'
      mensisaNlsp=aggregate(mensis$NBIND,list(NOSTA=mensis$NOSTA,GENR_ESP=mensis$GENR_ESP),sum)
      names(mensisaNlsp)[3]='Ntotsp'
      mensisaNlcsp2=merge(mensisaNlcsp,mensisaNlsp)
      # Proportion of size category for each species and haul, based on abundance-at-length
      mensisaNlcsp2$pcsp=mensisaNlcsp2$Ntotcsp/mensisaNlcsp2$Ntotsp
      mensis=merge(mensis,mensisaNlcsp2,by.x=c('NOSTA','GENR_ESP','CATEG'),by.y=c('NOSTA','GENR_ESP','CATEG'))
      mensis$wi=mensis$Lcm*mensis$NBIND/mensis$Ntotcsp
      # Recalculate mean length per species code and haul, based on abundance-at-length
      mLcsp.mens=aggregate(mensis$wi,list(NOSTA=mensis$NOSTA,GENR_ESP=mensis$GENR_ESP,CATEG=mensis$CATEG),sum)
      names(mLcsp.mens)[4]='LM2'
      dim(Pecheis)
      mLcsp.mens=mLcsp.mens[order(mLcsp.mens$NOSTA,mLcsp.mens$GENR_ESP),]
      Pecheis=merge(Pecheis,mLcsp.mens,by.x=c('NOSTA','GENR_ESP','SIGNEP'),
                    by.y=c('NOSTA','GENR_ESP','CATEG'),
                    all.x=TRUE,all.y=TRUE)
      duplicated.speciesIncatches.check(Pecheis)
      x11()
      par(mfrow=c(2,2))
      plot(Pecheis$LM,Pecheis$LM2)
      Pecheis$dLM=Pecheis$LM-Pecheis$LM2
      cat('Summary statistics of differences between original and corrected mean lengths:','\n')
      print(summary(Pechei$dLM))
      dlms=Pecheis[!is.na(Pecheis$LM2)&abs(Pecheis$dLM)>1,]
      # check No. of species codes per haul in sub and total samples
      # dataframe with total samples and no subsamples
      dftsnoss=Pecheis[is.na(Pecheis$LM2),]
      # dataframe with subsamples and no total samples 
      dfssnots=Pecheis[is.na(Pecheis$LM),]
      # add species codes in subsamples and not in total samples
      for (i in 1:dim(dfssnots)[1]){
        dfssnotsi=dfssnots[i,]
        dfssnotsib=Pecheis[!is.na(Pecheis$LM)&Pecheis$NOSTA==dfssnotsi$NOSTA&
                             Pecheis$GENR_ESP==dfssnotsi$GENR_ESP,]
        if (dim(dfssnotsib)[1]>0){
          dfssnotsic=dfssnotsib
          dfssnotsic$SIGNEP=dfssnotsi$SIGNEP
          dfssnotsic$LM2=dfssnotsi$LM2
          dfssnotsic$PT=NA
          dfssnotsic$NT=NA
          Pecheis[is.na(Pecheis$LM)&Pecheis$NOSTA==dfssnotsi$NOSTA&
                    Pecheis$GENR_ESP==dfssnotsi$GENR_ESP,]=dfssnotsic
          cat('New total sample line added:',
              paste(dfssnotsic$NOSTA,dfssnotsic$GENR_ESP,dfssnotsic$SIGNEP),'\n')
        }else{
          #Pecheis=Pecheis[Pecheis$NOSTA!=dfssnotsi$NOSTA&
          #                  Pecheis$GENR_ESP!=dfssnotsi$GENR_ESP,]
          cat('Subsamples without totalsamples removed from total samples:',
              paste(dfssnotsi$NOSTA,dfssnotsi$GENR_ESP),'\n')
        }
      }
      # remove remaining lines with NA in LM
      Pecheis=Pecheis[!is.na(Pecheis$LM),]
      Pecheis$CodEsp=paste(Pecheis$GENR_ESP,Pecheis$STRATE,Pecheis$SIGNEP,sep='-')
      duplicated.speciesIncatches.check(Pecheis)
      #Pecheis[is.na(Pecheis$PT),]
      # Recalculate total weight and abundance per species code and haul, based on abundance-at-length
      Pecheisa=aggregate(Pecheis[,c('PT','NT')],list(NOSTA=Pecheis$NOSTA,GENR_ESP=Pecheis$GENR_ESP),sum,
                         na.rm=TRUE)
      names(Pecheisa)[3:4]=c('PTOT','NTOT')
      Pecheisa=merge(Pecheisa,mensiaNlcsp2,by.x=c('NOSTA','GENR_ESP'),by.y=c('NOSTA','GENR_ESP'))
      Pecheisa$PT2=Pecheisa$PTOT*Pecheisa$pcsp
      Pecheisa$NT2=Pecheisa$NTOT*Pecheisa$pcsp
      Pecheis=merge(Pecheis,Pecheisa,by.x=c('NOSTA','GENR_ESP','SIGNEP'),by.y=c('NOSTA','GENR_ESP','CATEG'),all.x=TRUE)
      plot(Pecheis$PT,Pecheis$PT2)
      plot(Pecheis$NT,Pecheis$NT2)
      par(mfrow=c(1,1))
      Pecheis$dPT=Pecheis$PT-Pecheis$PT2
      Pecheis$dNT=Pecheis$NT-Pecheis$NT2
      cat('Summary statistics of differences between original and corrected total biomass in catch:','\n')
      print(summary(Pecheis$dPT))
      cat('Summary statistics of differences between original and corrected total abundance in catch:','\n')
      print(summary(Pecheis$dNT))
      dlms2=Pecheis[!is.na(Pecheis$LM2)&abs(Pecheis$dPT)>1,]
      # replace by corrected values
      Pecheis$LM=Pecheis$LM2
      Pecheis$NT=Pecheis$NT2
      Pecheis$PT=Pecheis$PT2
      Pecheis$PM=Pecheis$PT/Pecheis$NT
      Pecheis$MOULE=Pecheis$PT/Pecheis$NT
    }else{
      stop('Please provide abundances-at-lengths') 
    }
  }
  Pechei=rbind(Pecheis[,names(Pecheins)],Pecheins)
  #update species codes
  Pechei$CodEsp=paste(Pechei$GENR_ESP,Pechei$STRATE,Pechei$SIGNEP,sep='-')
  Pechei=Pechei[order(Pechei$NOSTA,Pechei$GENR_ESP),]
  duplicated.speciesIncatches.check(Pechei)
  Pechei
}

biomres.size.check=function(Biomres.sp.size){
  head(Biomres.sp.size)
  Biomres.sp.sizeas=aggregate(Biomres.sp.size$N,list(TC=Biomres.sp.size$TC,sp=Biomres.sp.size$sp,x=Biomres.sp.size$LONG,
                                                     y=Biomres.sp.size$LAT),sum)
  names(Biomres.sp.sizeas)[5]='Ntot2'
  Biomres.sp.sizeau=aggregate(Biomres.sp.size[,c('Ntot','LONG','LAT')],list(TC=Biomres.sp.size$TC,sp=Biomres.sp.size$sp),unique)
  names(Biomres.sp.sizeau)[3:5]=c('Ntot','x','y')
  Biomres.sp.sizea=merge(Biomres.sp.sizeas,Biomres.sp.sizeau)
  dim(Biomres.sp.sizeas);dim(Biomres.sp.sizeau);dim(Biomres.sp.sizea)
  head(Biomres.sp.sizea)
  head(B.sp.esdu.df)
  aggregate(B.sp.esdu.df$BN,list(B.sp.esdu.df$GENR_ESP),sum)
  aggregate(Biomres.sp.size$N,list(Biomres.sp.size$sp),sum)
  
  Biomres.sp.sizea2=merge(Biomres.sp.sizeas,biomres.sp.esdu)
  
  Biomres.sp.sizea$dN=Biomres.sp.sizea$Ntot-Biomres.sp.sizea$Ntot2
  plot(ESDUDEVs$LONG,ESDUDEVs$LAT)
  points(Biomres.sp.sizea$x,Biomres.sp.sizea$y,pch=16)
  plot(Biomres.sp.sizeau$x,Biomres.sp.sizeau$y)
  points(Biomres.sp.sizea[abs(Biomres.sp.sizea$dN)>=1,'x'],Biomres.sp.sizea[abs(Biomres.sp.sizea$dN)>=1,'y'],pch=16,col=2)
  points(Biomres.sp.sizea[Biomres.sp.sizea$sp=='SARD-PIL'&abs(Biomres.sp.sizea$dN)>=1,'x'],
         Biomres.sp.sizea[Biomres.sp.sizea$sp=='SARD-PIL'&abs(Biomres.sp.sizea$dN)>=1,'y'],pch=16,col=4)
  points(Biomres.sp.sizea[Biomres.sp.sizea$sp=='SCOM-SCO'&abs(Biomres.sp.sizea$dN)>=1,'x'],
         Biomres.sp.sizea[Biomres.sp.sizea$sp=='SCOM-SCO'&abs(Biomres.sp.sizea$dN)>=1,'y'],pch=16,col=2)
  
  Biomres.sp.sizea[abs(Biomres.sp.sizea$dN)>=1,]
  
  Biomres.sp.sizea1=Biomres.sp.sizea[Biomres.sp.sizea$sp=='SARD-PIL'&abs(Biomres.sp.sizea$dN)>=1,]
  Biomres.sp.size[Biomres.sp.size$TC%in%Biomres.sp.sizea1$TC&Biomres.sp.size$sp=='SARD-PIL',]
  head(chamens.wide)
  chamens.wide[chamens.wide$TC%in%Biomres.sp.sizea1$TC,]
  Pechef[Pechef$NOCHAL==926,]
  
  sum(Biomres.sp.size$N)-sum(biomres.sp.esdu[biomres.sp.esdu$sp%in%spsel,'BN'])
}