#*************************
#Functions for acoustic biomass assessment
#*************************
#BIOMASS PER ESDU
#*************************

#Methods
#*************************
# CALCUL DES BIOMASSES ACOUSTIQUES PAR ESPECE 
# calcul du facteur d'écho-intégration par espece pour chaque chalutage
#   d'apres Diner 1) note Evaluation des stocks par echo-integration, DITI/GO/TP,
#                    12/11/98 revue 2/12/98, p.3,11
#                 2) fonction étalonnage, manuel de MOVIES+
#                 3) note Equivalence énergie->sA, TMSI/TP 29/09/2001, p.6 annexe1
# 
# attention: energie en mv2m2 (an <2002)
#   Xei=kei/sum.sur.e(kei/Fei) i=chalut; e=espece
#   TSk=20*log(lmoy)-b+10*log(moule)
#   Fe=(Fr/x)*10**(-(35+TSk)/10); Fr/x = 0.00585
#   Xei=kei/sum.sur.e(kei/Fei) i=chalut; e=espece
# energie en sA (>=2002)
#   1/Fe = sigma = 4*pi*10**(TSk/10)
#                = 4*pi*(lmoy**2)*(moule*1000)*10**(-b/10) 
#   TSk=20*log(lmoy)-b+10*log(moule)
#
# unites sA : Dev en m2.NM-2 ; sigma en m2.tonne-1 ; Xe en tonne.m-2 ; Dev*Xe en tonne.NM-2
# unites Energ: Dev en m2mV2  ; Fr/x permet conversion unites ; 
#          Fe en tonne.NM-2.(m2mV2)-1 ref. x=1NM et TSk=-35dB.kg-1 ; Dev*Xe en tonne.NM-2 
#
# utilisation de la correspondance experte entre type de deviations et groupes d'especes
# evaluation par type de deviation
# association de la deviation.k de l'esdu.i au chalutage.j le plus proche
#      pour chaque Dev.k : 
#           calculer le ke.j du groupe d'espece de Dev.k dans chalut.j
#           calculer le Xe.kj
#      pour chaque Esdu.i et devation.k : 
#           identifier le chalutage.kj le plus proche 
#           calculer Xe.kj*Dev.ki
#      pour chaque Esdu.i :
#           sommer les biomasses par especes estimees dans les deviations.k
#
###
#*************************
#Front-end functions
#*************************
#*************************
	#Biomass per ESDU -------------
  #*************************

	#Biomass per ESDU for lyears cruise series
  #*************************

	multi.acoubiom.esdu=function(ESDUDEV,EspDev,Pechef,
		list.Assoc.year,list.XeB,list.XeN,verbosit=FALSE){

		meta=get.metadata(EspDev)
		lyears=meta$lyears
		lNdev=meta$lNdev

		for (i in 1:length(lyears)){
			cat(as.character(lyears[i]),'\n')
			EsduDevi=ESDUDEV[ESDUDEV$CAMPAGNE==as.character(lyears[i]),]
			# Check duplicated ESUs
			if (length(EsduDevi$TC)!=length(unique(EsduDevi$TC))){
			  stop('Duplicated rows in scrutinising results')
			}
			#!!! order ESDUDEVI by esdu number !!!
			EsduDevi=EsduDevi[order(EsduDevi$esdu.id),]
			Ndevi=lNdev[[i]]
			EspDevi=EspDev[EspDev$CAMPAGNE==as.character(lyears[i]),]
			Pechei=Pechef[Pechef$CAMPAGNE==as.character(lyears[i]),]
			list.Associ=list.Assoc.year[[i]]
			list.XeBi=list.XeB[[i]]
			list.XeNi=list.XeN[[i]]
			cat('Compute biomass','\n') 
			biomB.resi=acoubiom.proc(EsduDevi=EsduDevi,EspDevi=EspDevi,
				list.Xei=list.XeBi,list.Associ=list.Associ,Ndevi=Ndevi,
				verbosit=verbosit)
      cat('Compute abundance','\n')
      biomN.resi=acoubiom.proc(EsduDevi=EsduDevi,EspDevi=EspDevi,
				list.Xei=list.XeNi,list.Associ=list.Associ,Ndevi=Ndevi,verbosit=verbosit)
      names(biomB.resi)
      BIOM.assoc=biomB.resi$BIOM.assoc
			biomB.esdu.dev.spi=biomB.resi$Biom.DevEsp
			biomN.esdu.dev.spi=biomN.resi$Biom.DevEsp
#       dfs=data.frame(biomB.esdu.dev.spi[[4]])
#       head(dfs)
# 			dfs[116,]
#       dim(dfs)
                  
		#Data frame with biomass per species, echotype and esdu
		#*************************
			for (k in seq(length(biomB.esdu.dev.spi))){
				deviB=data.frame(biomB.esdu.dev.spi[[k]])
				deviB$DEV=paste('D',Ndevi[k],sep='')
				deviB$CAMPAGNE=lyears[i]
				deviN=data.frame(biomN.esdu.dev.spi[[k]])
				deviN$DEV=paste('D',Ndevi[k],sep='')
				deviN$CAMPAGNE=lyears[i]
        print(dim(deviB))
				if (k==1){		
					BB.dev.esdu.dfi=deviB
					BN.dev.esdu.dfi=deviN
				}else{
					BB.dev.esdu.dfi=rbind(BB.dev.esdu.dfi,deviB)
					BN.dev.esdu.dfi=rbind(BN.dev.esdu.dfi,deviN)
				}				
			}
      head(BB.dev.esdu.dfi)
      names(BB.dev.esdu.dfi)
      table(BB.dev.esdu.dfi$Esdu)
      desus=table(BB.dev.esdu.dfi$Esdu)[table(BB.dev.esdu.dfi$Esdu)>length(unique(EspDevi$DEV))]
#       names(desus)
#       BB.dev.esdu.dfi[BB.dev.esdu.dfi$Esdu==614,]
#       unique(BB.dev.esdu.dfi$DEV)
			#Reshape to long format and merge biomass and abundance:
      #*************************
      #Reshape biomass
#       lcsp=names(BB.dev.esdu.dfi)[!names(BB.dev.esdu.dfi)%in%
#           c("Esdu",'DEV','CAMPAGNE')]
#       length(lcsp);length(unique(lcsp))
#       BB.dev.esdu.dfi[BB.dev.esdu.dfi$Esdu==1001,]
      length(BB.dev.esdu.dfi$Esdu)
      length(unique(BB.dev.esdu.dfi$Esdu))
      
			BB.dev.esdu.dfi.long=reshape(BB.dev.esdu.dfi,
				idvar=c("Esdu",'DEV','CAMPAGNE'),
				varying=list(names(BB.dev.esdu.dfi)[
					!names(BB.dev.esdu.dfi)%in%
					c("Esdu",'DEV','CAMPAGNE')]),
				timevar='CodEsp',direction='long',v.names='BB',
				times=names(BB.dev.esdu.dfi)[
					!names(BB.dev.esdu.dfi)%in%
					c("Esdu",'DEV','CAMPAGNE')])
  		head(BB.dev.esdu.dfi.long)
  		unique(BB.dev.esdu.dfi.long$CodEsp)
      #table(BB.dev.esdu.dfi.long$DEV,BB.dev.esdu.dfi.long$CodEsp)
      #summary(BB.dev.esdu.dfi.long[BB.dev.esdu.dfi.long$CodEsp=='ENGR.ENC.CLAS.0','BB'])   
      #Valid CodEsp/strata pairs
      BB.1sp.esdu.dfa=aggregate(BB.dev.esdu.dfi.long$BB,
        list(BB.dev.esdu.dfi.long$DEV,BB.dev.esdu.dfi.long$CodEsp),sum)
      names(BB.1sp.esdu.dfa)=c('DEV','CodEsp','Btot')
      lspdev=unique(BB.1sp.esdu.dfa[BB.1sp.esdu.dfa$Btot>0,c('DEV','CodEsp')])
      #Select valid CodEsp/strata pairs
      dim(BB.dev.esdu.dfi.long)
      BB.dev.esdu.dfi.longs=BB.dev.esdu.dfi.long[paste(BB.dev.esdu.dfi.long$DEV,
        BB.dev.esdu.dfi.long$CodEsp)%in%paste(lspdev$DEV,lspdev$CodEsp),]
      dim(BB.dev.esdu.dfi.longs)  
      unique(BB.dev.esdu.dfi.longs[,c('DEV','CodEsp')])  
      #summary(BB.dev.esdu.dfi.longs[BB.dev.esdu.dfi.longs$CodEsp=='ENGR.ENC.CLAS.0','BB'])   
 
      #Reshape abundance   
			BN.dev.esdu.dfi.long=reshape(BN.dev.esdu.dfi,
				idvar=c("Esdu",'DEV','CAMPAGNE'),
				varying=list(names(BN.dev.esdu.dfi)[
					!names(BN.dev.esdu.dfi)%in%
					c("Esdu",'DEV','CAMPAGNE')]),
				timevar='CodEsp',direction='long',v.names='BN',
				times=names(BN.dev.esdu.dfi)[
					!names(BN.dev.esdu.dfi)%in%
					c("Esdu",'DEV','CAMPAGNE')])
			head(BN.dev.esdu.dfi.long)
			dim(BN.dev.esdu.dfi.long)
      #Select valid CodEsp/strata pairs
      dim(BN.dev.esdu.dfi.long)
      BN.dev.esdu.dfi.longs=BN.dev.esdu.dfi.long[paste(BN.dev.esdu.dfi.long$DEV,
        BN.dev.esdu.dfi.long$CodEsp)%in%paste(lspdev$DEV,lspdev$CodEsp),]
      dim(BN.dev.esdu.dfi.longs)  

      #Merge biomass and abundance   
			B.dev.esdu.dfi.long=merge(BB.dev.esdu.dfi.longs,
				BN.dev.esdu.dfi.longs,by.x=c('Esdu','DEV','CAMPAGNE',
				'CodEsp'),by.y=c('Esdu','DEV','CAMPAGNE','CodEsp'))
			dim(B.dev.esdu.dfi.long)
			head(B.dev.esdu.dfi.long)
			unique(B.dev.esdu.dfi.long$CodEsp)
     #summary(B.dev.esdu.dfi.long[B.dev.esdu.dfi.long$CodEsp=='ENGR.ENC.CLAS.0','BB'])   
 
			#adds reference haul and metadata to df in wide and long formats
			dim(EsduDevi)
  		head(EsduDevi)
      length(EsduDevi[,c('esdu.id')])
      length(unique(EsduDevi[,c('esdu.id')]))
			# lEsduDevi=reshape(EsduDevi[,c('esdu.id',paste('D',
			# 	Ndevi,'.CREF',sep=''))],idvar="esdu.id", 
			# 	varying=list(paste('D',Ndevi,'.CREF',sep='')),
			# 	v.names="charef",timevar="DEV",direction='long',
			# 	times=paste('D',Ndevi,sep=''))
			head(BIOM.assoc)
			lEsduDevi=reshape(BIOM.assoc[,c('ESDU',paste('CHA',
			                                              seq(length(Ndevi)),sep=''))],idvar="ESDU", 
			                  varying=list(paste('CHA',seq(length(Ndevi)),sep='')),
			                  v.names="charef",timevar="DEV",direction='long',
			                  times=paste('D',Ndevi,sep=''))
			head(lEsduDevi)
			names(lEsduDevi)[1]='esdu.id'
			dim(BB.dev.esdu.dfi)
      names(BB.dev.esdu.dfi)   
			BB.dev.esdu.dfi=merge(BB.dev.esdu.dfi,lEsduDevi,
				by.x=c('Esdu','DEV'),by.y=c('esdu.id','DEV'))
			dim(BB.dev.esdu.dfi)
			BB.dev.esdu.dfi=merge(BB.dev.esdu.dfi,
				EsduDevi[,c('esdu.id','TC','LAT','LONG')],
				by.x=c('Esdu'),by.y=c('esdu.id'))
			BN.dev.esdu.dfi=merge(BN.dev.esdu.dfi,lEsduDevi,
				by.x=c('Esdu','DEV'),by.y=c('esdu.id','DEV'))
			BN.dev.esdu.dfi=merge(BN.dev.esdu.dfi,
				EsduDevi[,c('esdu.id','TC','LAT','LONG')],
				by.x=c('Esdu'),by.y=c('esdu.id'))
      head(BN.dev.esdu.dfi)
			dim(B.dev.esdu.dfi.long)
			B.dev.esdu.dfi.long=merge(B.dev.esdu.dfi.long,
				lEsduDevi,by.x=c('Esdu','DEV'),by.y=c('esdu.id',
				'DEV'))
			head(B.dev.esdu.dfi.long)
			#summary(B.dev.esdu.dfi.long[B.dev.esdu.dfi.long$CodEsp=='ENGR.ENC.CLAS.0','BB'])   
			B.dev.esdu.dfi.long=merge(unique(EsduDevi[,c('esdu.id','TC',
				'LAT','LONG')]),B.dev.esdu.dfi.long,
				by.y=c('Esdu'),by.x=c('esdu.id'))
			head(B.dev.esdu.dfi.long)
			dim(B.dev.esdu.dfi.long)
			unique(B.dev.esdu.dfi.long$CodEsp)
     #summary(B.dev.esdu.dfi.long[B.dev.esdu.dfi.long$CodEsp=='ENGR.ENC.CLAS.0','BB'])   
    
			#Creates output files
			if (i==1){
				#list of lists format: 
				#BB.dev.sp.esdu.list=list(biomB.esdu.dev.spi)
				#BN.dev.sp.esdu.list=list(biomN.esdu.dev.spi)

				#list of dataframes format: 
				BB.dev.sp.esdu.df=list(BB.dev.esdu.dfi)
				BN.dev.sp.esdu.df=list(BN.dev.esdu.dfi)

				#A single dataframe in long format:
				B.dev.sp.esdu.df.long=B.dev.esdu.dfi.long
			}else{
				#list of lists format : 
				#BB.dev.sp.esdu.list=c(BB.dev.sp.esdu.list,
				#	list(biomB.esdu.dev.spi))
				#BN.dev.sp.esdu.list=c(BN.dev.sp.esdu.list,
				#	list(biomN.esdu.dev.spi))
				#list of dataframes format : 
				BB.dev.sp.esdu.df=c(BB.dev.sp.esdu.df,
					list(BB.dev.esdu.dfi))
				BN.dev.sp.esdu.df=c(BN.dev.sp.esdu.df,
					list(BN.dev.esdu.dfi))
				#A single dataframe in long format:
				B.dev.sp.esdu.df.long=rbind(B.dev.sp.esdu.df.long,
					B.dev.esdu.dfi.long)
  		}
		}
		B.dev.sp.esdu.df.long$CodEsp=gsub('\\.', '-',
			B.dev.sp.esdu.df.long$CodEsp)
		B.dev.sp.esdu.df.long[,'GENR_ESP']=substr(
			B.dev.sp.esdu.df.long$CodEsp,1,8)
		BB.dev.sp.esdu.list=NULL
		BN.dev.sp.esdu.list=NULL
    names(BB.dev.esdu.dfi)                                

		#summary(B.dev.sp.esdu.df.long[B.dev.sp.esdu.df.long$CodEsp=='ENGR-ENC-CLAS-0','BB'])   
	    
	list(BB.dev.sp.esdu.list=BB.dev.sp.esdu.list,BN.dev.sp.esdu.list=
		BN.dev.sp.esdu.list,BB.dev.sp.esdu.df=BB.dev.sp.esdu.df,
		BN.dev.sp.esdu.df=BN.dev.sp.esdu.df,B.dev.sp.esdu.df.long=
		B.dev.sp.esdu.df.long)
	}


	acoubiom.proc=function(EsduDevi,EspDevi,list.Xei,list.Associ,Ndevi,
	                       verbosit=FALSE){
	#Outputs:
	#*************************
	#BIOM: biomass per species:size class, depth layer and ESDU (t)
	#mBIOM: mean biomass per species:size class, depth layer over all ESDUs (t)
	#BIOMs: biomass per species:size class, depth layer and ESDU (t) 
		#for species with non-null biomass in at least one ESDU
	#BIOM.SURF: biomass per species:size class, depth layer and ESDU in surface layer (t)
	#BIOM.BOTT: biomass per species:size class, depth layer and ESDU in bottom layer (t)
	#BIOM.spL: biomass per species:size class and ESDU over all depth layers (t)
	#BIOM.sp: biomass per species and ESDU over all depth layers (t)
	#BIOM.assoc: Hauls allocated to each deviation in each esdu

		#Biomass in Esdu.i per species and Deviation.k
	  #*************************
  
    list.Biom = estim.k(nk = sort(Ndevi), EsduDevi=EsduDevi, list.Xei=list.Xei, 
                        EspDevi=EspDevi, list.Associ=list.Associ, 
                        method = "M", verbosit = verbosit)
# 	  list.Bioms = data.frame(list.Biom[[4]])
# 	  dim(list.Bioms)
# 	  head(list.Bioms)	 
#     list.Bioms[list.Bioms$Esdu==116,]
	  #Biomass in Esdu.i per species
    #*************************    
		BIOM=data.frame(t(sapply(X=seq(along=EsduDevi[,1]),
			FUN=biom.esp.i,list.Biom,EsduDevi)))
		dimnames(BIOM)[[2]]=c('ESDU','LAT','LONG',
			as.character(dimnames(list.Biom[[1]])[[2]][-1]))
    head(BIOM) 
  
	  BIOM = data.frame(t(sapply(X = seq(along = EsduDevi[, 1]), 
	                           FUN = biom.esp.i, list.Biom, EsduDevi)))
	  dimnames(BIOM)[[2]] = c("ESDU", "LAT", "LONG", as.character(dimnames(list.Biom[[1]])[[2]][-1]))
	  dimnames(BIOM)[[2]]=gsub("biom.","",dimnames(BIOM)[[2]])
	  head(BIOM)
  
		#Mean biomass per species in whole area
	  #*************************
  	Nsp = length(unique(EspDevi$CodEsp))
	  mBIOM = apply(BIOM[, 4:(Nsp + 3)], 2, mean)
	  apply(BIOM[, 4:(Nsp + 3)], 2, sum)
  
		#Select/aggregate CodEsp's
	  #*************************
	  uEspDev = unique(EspDevi[, c("STRATE", "CodEsp", "GENR_ESP","SIGNEP")])
	  uEspDev$spL = paste(uEspDev[, "GENR_ESP"], uEspDev[, "SIGNEP"],sep = "-")
  
		#CodEsp with non-null biomass
	  names(BIOM)[c(rep(TRUE, 3), c(unlist(mBIOM > 0)))]
	  BIOMs = BIOM[, c(rep(TRUE, 3), c(unlist(mBIOM > 0)))]
  
		#Fish biomass in surface layer
		if ('SURF'%in%unique(uEspDev$STRATE)){
			BIOM.SURF=BIOM[,c('ESDU','LAT','LONG',
				as.character(uEspDev[uEspDev$STRATE=='SURF','CodEsp']))]
			N=dim(BIOM.SURF)[2]
			#Non null fish biomass in surface layer
			BIOM.SURFs=BIOM.SURF[,c(rep(TRUE,3),
				c(apply(BIOM.SURF[,4:N],2,mean)>0))]
			BIOM.SURFs[1:10,]
			names(BIOM.SURFs)
			#apply(BIOM.SURFs[,4:7],2,sum)	
		}else BIOM.SURF=NULL

		#Fish biomass in bottom layers
		if ('CLAS'%in%unique(uEspDev$STRATE)){
			BIOM.BOTT=BIOM[,c('ESDU','LAT','LONG',
				as.character(uEspDev[uEspDev$STRATE=='CLAS','CodEsp']))]
			N=dim(BIOM.BOTT)[2]
			names(BIOM.BOTT)
			#apply(BIOM.BOTT[,4:dim(BIOM.BOTT)[2]],2,sum)	
	
			#Non null fish biomass in bottom layers
			BIOM.BOTTs=BIOM.BOTT[,c(rep(TRUE,3),c(apply(BIOM.BOTT[,4:N],2,mean)>0))]
			names(BIOM.BOTTs)
			#apply(BIOM.BOTT[,4:dim(BIOM.BOTTs)[2]],2,sum)	
		}else BIOM.BOTT=NULL

		#Aggregate per species and size class (whatever the depth)
		lnames=dimnames(t(BIOM[,-seq(3)]))[[1]]
		tBIOML=data.frame(t(BIOM[,-seq(3)]),spL=paste(substr(lnames,1,9),
			substr(lnames,15,15),sep=''))
		tBIOML[1:10,]
		#BIOM.spL[1:10,]
		N=dim(tBIOML)[2]
		BIOM.spL=t(aggregate(tBIOML[,1:(N-1)],by=list(tBIOML$spL),sum))
		lnames=BIOM.spL[1,]
		BIOM.spL=apply(BIOM.spL[-1,],2,as.numeric)
		BIOM.spL=data.frame(BIOM[,seq(3)],BIOM.spL)
		names(BIOM.spL)=c("ESDU","LAT","LONG",lnames)
		#apply(BIOM.spL[,4:dim(BIOM.spL)[2]],2,sum)		

		#Aggregate per species (whatever the size and depth)
		lnames=dimnames(t(BIOM[,-seq(3)]))[[1]]
		tBIOM=data.frame(t(BIOM[,-seq(3)]),GENR_ESP=substr(lnames,1,8))
		N=dim(tBIOM)[2]
		BIOM.sp=t(aggregate(tBIOM[,1:(N-1)],by=list(tBIOM$GENR_ESP),sum))
		lnames=BIOM.sp[1,]
		BIOM.sp=apply(BIOM.sp[-1,],2,as.numeric)
		BIOM.sp=data.frame(BIOM[,seq(3)],BIOM.sp)
		names(BIOM.sp)=c("ESDU","LAT","LONG",lnames)
	
		#apply(BIOM.sp[,4:dim(BIOM.sp)[2]],2,sum)

		#Hauls allocated to each deviation in each esdu
		#*************************
		for (i in 1:length(Ndevi)){
			if (i==1){
				BIOM.assoc=data.frame(list.Associ[[i]])
				dimnames(BIOM.assoc)[[2]][1:3]=c('ESDU',
					paste('DEV',Ndevi[i],sep=''),paste('CHA',i,
					sep=''))
			}else{
				BIOM.assoc=data.frame(BIOM.assoc,list.Associ[[i]][,2:3])
				N=dim(BIOM.assoc)[2]
				dimnames(BIOM.assoc)[[2]][c(N-3,N)]=c(paste('DEV',
					Ndevi[i],sep=''),paste('CHA',i,sep=''))
			}
		}
	list(BIOM=BIOM,mBIOM=mBIOM,BIOMs=BIOMs,BIOM.SURF=BIOM.SURF,
		BIOM.BOTT=BIOM.BOTT,BIOM.spL=BIOM.spL,BIOM.sp=BIOM.sp,
		BIOM.assoc=BIOM.assoc,Biom.DevEsp=list.Biom)
	}

biomass.esdu.check=function(B.dev.sp.esdu.df,saveIt=TRUE,plotIt=TRUE,path.results=NULL,
                            Pechef){
  
  #check all species spatial distribution per echotype
  if (plotIt){
    par(mfrow=c(1,1))
    simple.1sp.dev.esdu.plot(B.dev.sp.esdu.df)
  }
  #NA or null abundance with non-null biomass?
  # if colSums!=c(0,0) -> pb...
  colSums(B.dev.sp.esdu.df[B.dev.sp.esdu.df$BN==0,c('BB','BN')])
  colSums(B.dev.sp.esdu.df[B.dev.sp.esdu.df$BB==0,c('BB','BN')])
  
  # Total biomass and abundance per species code and echotype
  Btot.sp.dev.df=aggregate(B.dev.sp.esdu.df[,c('BB','BN')],
                           list(B.dev.sp.esdu.df$CAMPAGNE,B.dev.sp.esdu.df$DEV,
                                B.dev.sp.esdu.df$CodEsp,B.dev.sp.esdu.df$GENR_ESP),sum,na.rm=TRUE)
  names(Btot.sp.dev.df)=c('CAMPAGNE','DEV','CodEsp','sp','BB','BN')
  cat('Total biomass and abundance per species code and echotype','\n')
  print(Btot.sp.dev.df)
  
  #NA in abundance in case of null biomass?
  B.dev.sp.esdu.df.BNna=B.dev.sp.esdu.df[is.na(B.dev.sp.esdu.df$BN),]
  B.dev.sp.esdu.df[is.na(B.dev.sp.esdu.df$BN),'BN']=0
  
  # Total biomass and abundance per species
  Btot.sp.df=aggregate(B.dev.sp.esdu.df[,c('BB','BN')],
                       list(B.dev.sp.esdu.df$CAMPAGNE,B.dev.sp.esdu.df$GENR_ESP),sum,na.rm=TRUE)
  names(Btot.sp.df)=c('CAMPAGNE','sp','BB','BN')
  cat('Total biomass and abundance per species','\n')
  print(Btot.sp.df)
  
  # Mean biomass and abundance per species code and echotype
  mB.csp.dev.df=aggregate(B.dev.sp.esdu.df[,c('BB','BN')],
                          list(B.dev.sp.esdu.df$CAMPAGNE,B.dev.sp.esdu.df$DEV,
                               B.dev.sp.esdu.df$CodEsp,B.dev.sp.esdu.df$GENR_ESP),mean,na.rm=TRUE)
  names(mB.csp.dev.df)=c('CAMPAGNE','DEV','CodEsp','sp','mBB','mBN')
  cat('Mean biomass and abundance per species code and echotype','\n')
  print(mB.csp.dev.df)
  
  # Total mean biomass and abundance per species code
  mB.csp.df=aggregate(mB.csp.dev.df[,c('mBB','mBN')],
                      list(mB.csp.dev.df$CAMPAGNE,mB.csp.dev.df$CodEsp,mB.csp.dev.df$sp),
                      sum,na.rm=TRUE)
  names(mB.csp.df)=c('CAMPAGNE','CodEsp','sp','mBB','mBN')
  cat('Total mean biomass and abundance per species code','\n')
  print(mB.csp.df)
  
  # Total mean biomass and abundance per species
  mB.sp.df=aggregate(mB.csp.df[,c('mBB','mBN')],
                     list(mB.csp.df$CAMPAGNE,mB.csp.df$sp),sum,na.rm=TRUE)
  names(mB.sp.df)=c('CAMPAGNE','sp','mBB','mBN')
  
  #Check mean fish weights (BB in tons) from biomass computations
  Btot.sp.df$mw.g=Btot.sp.df$BB*1e6/Btot.sp.df$BN
  
  #Mean weights from catches
  mw.peche=aggregate(Pechef$PM,list(Pechef$GENR_ESP),mean,na.rm=TRUE)
  names(mw.peche)=c('sp','mw')
  mw.peche$mw.g.catch=mw.peche$mw
  
  #Merge all descriptive stats
  Btot.sp.df=merge(Btot.sp.df,mw.peche[,c('sp','mw.g.catch')],by.x='sp',
                   by.y='sp')
  Bsum.sp.esdu=merge(Btot.sp.df,mB.sp.df)
  
  cat('Descriptive stats summary','\n')  
  print(Bsum.sp.esdu)
  
  if (saveIt&!is.null(path.results)){
    write.table(Bsum.sp.esdu,paste(path.results,'/Bsum.sp.esdu.csv',sep=''),
                sep=';',row.names=FALSE)
  }
  Bsum.sp.esdu
}

biomass.esdu.sp=function(B.dev.sp.esdu.df){
  
  # Biomass per esdu and codesp
  B.codesp.esdu.df=aggregate(B.dev.sp.esdu.df[,c('BB','BN')],
                             list(TC=B.dev.sp.esdu.df$TC,CAMPAGNE=B.dev.sp.esdu.df$CAMPAGNE,
                                  CodEsp=B.dev.sp.esdu.df$CodEsp),sum)
  names(B.codesp.esdu.df)
  B.codesp.esdu.df$spsize=paste(substr(B.codesp.esdu.df$CodEsp,1,8),
                                substr(B.codesp.esdu.df$CodEsp,15,16),sep='-')
  # Biomass per esdu and species/size categories
  B.spsize.esdu.df=aggregate(B.codesp.esdu.df[,c('BB','BN')],
                             list(TC=B.codesp.esdu.df$TC,
                                  CAMPAGNE=B.codesp.esdu.df$CAMPAGNE,
                                  spsize=B.codesp.esdu.df$spsize),sum)
  names(B.spsize.esdu.df)
  B.spsize.esdu.df=merge(B.spsize.esdu.df,unique(B.dev.sp.esdu.df[,c('TC','LAT','LONG')]),
                         by.x='TC',by.y='TC')
  #adds DEV columns for sp.plots
  B.spsize.esdu.df$DEV='ALL'
  # Biomass per esdu and species
  B.sp.esdu.df=aggregate(B.dev.sp.esdu.df[,c('BB','BN')],
                         list(B.dev.sp.esdu.df$TC,B.dev.sp.esdu.df$CAMPAGNE,
                              B.dev.sp.esdu.df$GENR_ESP),sum)
  names(B.sp.esdu.df)=c('TC','CAMPAGNE','GENR_ESP','BB','BN')
  table(B.sp.esdu.df$GENR_ESP)
  B.sp.esdu.df$DEV='ALL'
  
  B.sp.esdu.df=merge(B.sp.esdu.df,unique(B.dev.sp.esdu.df[,c('TC','LAT','LONG')]),
                     by.x='TC',by.y='TC')
  dim(B.sp.esdu.df)
  table(B.sp.esdu.df$GENR_ESP)
  length(B.sp.esdu.df$TC)
  length(unique(B.sp.esdu.df$TC))
  names(B.sp.esdu.df)
  
  cat('Species biomass summary','\n')
  print(tapply(B.sp.esdu.df[,c('BB')],list(B.sp.esdu.df$GENR_ESP),summary))
  
  # Mean biomass per species
  mB.sp.esdu.df=aggregate(B.sp.esdu.df[,c('BB','BN')],
                          list(B.sp.esdu.df$GENR_ESP),mean)
  names(mB.sp.esdu.df)=c('sp','mBB','mBN')
  
  # Mean biomass per species/size categories
  mB.spsize.df=aggregate(B.spsize.esdu.df[,c('BB','BN')],
                         list(CAMPAGNE=B.spsize.esdu.df$CAMPAGNE,
                              spsize=B.spsize.esdu.df$spsize),mean)
  list(B.sp.esdu.df=B.sp.esdu.df,mB.sp.esdu.df=mB.sp.esdu.df,mB.spsize.df=mB.spsize.df,
       B.spsize.esdu.df=B.spsize.esdu.df)
}


#*************************
# Biomass per ESDU and size -------
#*************************


biomass.esdu.length=function(ESDUDEVs,B.dev.sp.esdu.df,Pechef,EspDev,
                             mens,LW,biomres.sp.esdu=NULL,list.assoc=NULL,
                             spsel=NULL,showDist=FALSE,minN=NULL,Pechels=NULL,
                             legpos='topleft',export.plot=NULL,xlim=NULL,
                             ylim=NULL,ux11=FALSE,logit=TRUE,nid='NOCHAL',
                             verbosit=FALSE,compute.chamens=TRUE,eps=1,
                             forceFind=FALSE,spname='GENR_ESP',
                             useHaulSum=FALSE){
  
  if (compute.chamens){
    #Option1: chamens per echotype and species category: bulletproof ------
    #*************************
    spcat.rem=FALSE # deprecated
    lspmens=unique(mens$GENR_ESP)
    lspBdev=unique(B.dev.sp.esdu.df$GENR_ESP)
    if (!is.null(spsel)){
      if (sum(!spsel%in%lspmens)>0) stop('Species ',spsel[!spsel%in%lspmens],'in spsel and not in mens')
    }
    charefmens=charef2chamens(ESDUDEVs2=ESDUDEVs,
                              B.dev.sp.esdu.df2=B.dev.sp.esdu.df,
                              Pechef=Pechef,Pechels=Pechels,mens=mens,
                              EspDev=EspDev,list.assoc=list.assoc,
                              spsel=spsel,showDist=showDist,minN=minN, 
                              legpos=legpos,export.plot=export.plot,
                              xlim=xlim,ylim=ylim,ux11=TRUE,
                              logit=logit,nid=nid,forceFind=forceFind,
                              useHaulSum=useHaulSum)
    names(charefmens)
    echo.rem=charefmens$echo.rem
    codesp.mean=charefmens$codesp.mean
    codesp.sum=charefmens$codesp.sum
      
    # if (!is.null(echo.rem)){
    #   cat('*******************************************************','\n')
    #   cat('-----> Echotypes with too few fish removed:',
    #       paste(charefmens$echo.rem,collapse=', '),'\n')
    #   cat('*******************************************************','\n')
    # }
    # if (!is.null(codesp.mean)){
    #   cat('*******************************************************','\n')
    #   cat('-----> Mean hauls/lengths used for echotypes with too few fish:',
    #       paste(codesp.mean,collapse=', '),'\n')
    #   cat('*******************************************************','\n')
    # }
    # if (!is.null(codesp.sum)){
    #   cat('*******************************************************','\n')
    #   cat('-----> sum of hauls/lengths used for echotypes with too few fish:',
    #       paste(codesp.sum,collapse=', '),'\n')
    #   cat('*******************************************************','\n')
    # }
    chamens.wide=charefmens$chamens.wide
    chamens.long=charefmens$chamens.long
    lBN.dev.esdu.db2=charefmens$lBN.dev.esdu.db2
    newMens=charefmens$newMens
    head(newMens)
    mens2=rbind(mens,newMens)
   }else{
    #Option2: chamens = charef in long format (old way, not bulletproof) ------
    #*************************
    names(chamens.wide)
    names(ESDUDEVs) 
    chamens.wide=ESDUDEVs
    chamens.wides=chamens.wide[,c(6,15:19)]
    names(chamens.wide)
    names(chamens.wides)  
    chamens.long=reshape(chamens.wides,varying=list(2:6),direction='long',
                         times=substr(names(chamens.wides)[2:6],1,2),
                         timevar='DEV',idvar='TC',v.names='NOCHAL.mens')
    dim(chamens.long)
    # Add NOSTA to chamens.long
    chamens.long=merge(chamens.long,unique(Pechef[,c('NOSTA','NOCHAL')]),
                       by.x='NOCHAL.mens',by.y='NOCHAL')
    names(chamens.long)=c("NOCHAL.mens","TC","DEV","NOSTA.mens")
    #Add chamens to abundance per ESDU
    dim(B.dev.sp.esdu.df)
    lBN.dev.esdu.db2=B.dev.sp.esdu.df[
      ,c('TC','charef','esdu.id','DEV','CodEsp','BN')]
    names(lBN.dev.esdu.db2)=c('TC','charef','Esdu','DEV','CodEsp','N')
    lBN.dev.esdu.db2$sp=paste(substr(lBN.dev.esdu.db2$CodEsp,1,4),
                              substr(lBN.dev.esdu.db2$CodEsp,6,8),sep='-')
    lBN.dev.esdu.db2$cat=substr(lBN.dev.esdu.db2$CodEsp,15,15)
    lBN.dev.esdu.db2$CodEsp2=paste(lBN.dev.esdu.db2$sp,lBN.dev.esdu.db2$cat,sep='-')
    names(lBN.dev.esdu.db2)
    dim(lBN.dev.esdu.db2)
    mens2=mens
  }
  if (is.null(echo.rem)){
    # Look for missing ESU:dev in charef2chamens outputs ----
    head(lBN.dev.esdu.db2)
    head(B.dev.sp.esdu.df)
    lBN.dev.esdu.db2a.sp=aggregate(lBN.dev.esdu.db2$N,
                                   list(sp=lBN.dev.esdu.db2$sp),sum)
    names(lBN.dev.esdu.db2a.sp)[2]='Ntot2'
    lBN.dev.esdu.dba.sp=aggregate(B.dev.sp.esdu.df$BN,
                                  list(sp=B.dev.sp.esdu.df$GENR_ESP),sum)
    names(lBN.dev.esdu.dba.sp)[2]='Ntot1'
    lBN.dev.esdu.dba.sp2=merge(lBN.dev.esdu.db2a.sp,lBN.dev.esdu.dba.sp,
                               by.x='sp',by.y='sp')
    lBN.dev.esdu.db2a.csp=aggregate(lBN.dev.esdu.db2$N,
                                    list(csp=lBN.dev.esdu.db2$CodEsp),sum)
    names(lBN.dev.esdu.db2a.csp)[2]='Ntot2'
    lBN.dev.esdu.dba.csp=aggregate(B.dev.sp.esdu.df$BN,
                                   list(csp=B.dev.sp.esdu.df$CodEsp),sum)
    names(lBN.dev.esdu.dba.csp)[2]='Ntot1'
    lBN.dev.esdu.dba.cspc=merge(lBN.dev.esdu.db2a.csp,lBN.dev.esdu.dba.csp,
                                by.x='csp',by.y='csp')
    lBN.dev.esdu.dba.cspc$dN=lBN.dev.esdu.dba.cspc$Ntot1-
      lBN.dev.esdu.dba.cspc$Ntot2
    lBN.dev.esdu.dba.cspc[lBN.dev.esdu.dba.cspc$dN>0,]
    lBN.dev.esdu.db2a.csp2=aggregate(lBN.dev.esdu.db2$N,
                                     list(csp2=paste(
                                       unlist(
                                         strsplit(lBN.dev.esdu.db2$DEV,'[.]'))[
                                           seq(1,4*length(lBN.dev.esdu.db2$DEV),4)],
                                       lBN.dev.esdu.db2$CodEsp)),sum)
    names(lBN.dev.esdu.db2a.csp2)[2]='Ntot2'
    lBN.dev.esdu.dba.csp2=aggregate(B.dev.sp.esdu.df$BN,
                                    list(
                                      csp2=paste(
                                        B.dev.sp.esdu.df$DEV,
                                        B.dev.sp.esdu.df$CodEsp)),sum)
    names(lBN.dev.esdu.dba.csp2)[2]='Ntot1'
    lBN.dev.esdu.dba.cspc2=merge(lBN.dev.esdu.db2a.csp2,lBN.dev.esdu.dba.csp2,
                                 by.x='csp2',by.y='csp2',all.y=TRUE)
    lBN.dev.esdu.dba.cspc2$dN=lBN.dev.esdu.dba.cspc2$Ntot1-
      lBN.dev.esdu.dba.cspc2$Ntot2
    lBN.dev.esdu.dba.cspc2.pb=lBN.dev.esdu.dba.cspc2[lBN.dev.esdu.dba.cspc2$dN>0|
                                                       is.na(lBN.dev.esdu.dba.cspc2$dN),]
    if (dim(lBN.dev.esdu.dba.cspc2.pb)[1]>0){
      stop('Missing ESU:DEV in charef2chamens outputs')
    }
  }
  #  list(chamens.long=charefmens$chamens.long,lBN.dev.esdu.db2=charefmens$lBN.dev.esdu.db2,ESDUDEVs2=charefmens$ESDUDEVs2)
  #}
  
  # 21.2. Calculate abundance (in no. of fish) per size class, ESDU, and species ---- 
  #*************************
  cat('Abundance per size class, ESDU, and species calculation','\n')
  #Select species
  table(lBN.dev.esdu.db2$CodEsp2)
  if (!is.null(spsel)){
    lBN.dev.esdu.dbs=lBN.dev.esdu.db2[lBN.dev.esdu.db2$sp%in%spsel,
                                      names(lBN.dev.esdu.db2)!='TC']
    lspcodes.mens=unique(lBN.dev.esdu.dbs[lBN.dev.esdu.dbs$sp%in%spsel,
                                          'CodEsp2'])
  }else{
    lBN.dev.esdu.dbs=lBN.dev.esdu.db2[,names(lBN.dev.esdu.db2)!='TC']
    lspcodes.mens=unique(lBN.dev.esdu.dbs$sp)
  }
  #lBN.dev.esdu.dbs[lBN.dev.esdu.dbs$CodEsp2=='TRAC-TRU-G','CodEsp2']='TRAC-TRU-0'
  head(lBN.dev.esdu.dbs)
  unique(lBN.dev.esdu.dbs$charef)
  unique(lBN.dev.esdu.dbs$sp)
  unique(lBN.dev.esdu.dbs$CodEsp)
  dfa1=aggregate(lBN.dev.esdu.dbs$N,list(lBN.dev.esdu.dbs$CodEsp),sum)
  #same no. of fish...
  dfa2=aggregate(lBN.dev.esdu.dbs$N,list(substr(lBN.dev.esdu.dbs$CodEsp,1,8)),
                 sum)
  sum(dfa1$x)-sum(dfa2$x)
  cat('Calculating abundance at length per species code','\n')
  
  # 21.2.1. Calculate abundance at length per species code -----
  list.sp.Nsize=lapply(X=seq(length(lspcodes.mens)),
                       FUN=size.ventilator.sp,lBN.dev.esdu.db=lBN.dev.esdu.dbs,
                       mens=mens2,Pechef,spname='CodEsp2',
                       Lname='Lcm',Nname='NBIND',verbosit=verbosit)
  names(list.sp.Nsize)=lspcodes.mens

  # Check abundances consistency
  Ntot0=sum(lBN.dev.esdu.dbs$N)
  lBN.dev.esdu.dbsa.Ntotsp=aggregate(lBN.dev.esdu.dbs$N,
                                     list(lBN.dev.esdu.dbs$sp),sum)
  lBN.dev.esdu.dbsa.NtotCodesp2=aggregate(lBN.dev.esdu.dbs$N,
                                          list(CodEsp2=lBN.dev.esdu.dbs$CodEsp2),sum)
  unique(lBN.dev.esdu.dbs$CodEsp2)
  Ntot1=NULL
  Ntot1.sp=NULL
  for (i in 1:length(lspcodes.mens)){
    dfi=list.sp.Nsize[[i]]
    NL.Ntotspi=aggregate(dfi$N,list(CodEsp2=dfi$CodEsp2),sum)
    Ntot1.sp=rbind(Ntot1.sp,NL.Ntotspi)
    Ntot1=cbind(Ntot1,sum(dfi$N))
  }
  Ntot0-sum(Ntot1)
  Ntot12.sp=merge(Ntot1.sp,lBN.dev.esdu.dbsa.NtotCodesp2,by.x='CodEsp2',
                  by.y='CodEsp2')
  Ntot12.sp$dN=Ntot12.sp$x.y-Ntot12.sp$x.x
  Ntot12.tot=apply(Ntot12.sp[,2:3],2,sum)
  if (sum(Ntot12.sp$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per CodEsp2')
  }
  
  # Calculate abundance per esdu, dev and species (sum over species codes)
  head(lBN.dev.esdu.dbs)
  lBN.dev.esdu.dbs$GENR_ESP=substr(lBN.dev.esdu.dbs$CodEsp,1,8)
  lBN.dev.esdu.dbs2=aggregate(lBN.dev.esdu.dbs[,'N'],
                              list(charef=lBN.dev.esdu.dbs$charef,
                                   Esdu=lBN.dev.esdu.dbs$Esdu,
                                   DEV=lBN.dev.esdu.dbs$DEV,
                                   sp=lBN.dev.esdu.dbs$sp),sum)
  head(lBN.dev.esdu.dbs2)
  names(lBN.dev.esdu.dbs2)[5]='N'
  lBN.dev.esdu.dbs2$cat='0'
  lBN.dev.esdu.dbs2$CodEsp2=paste(lBN.dev.esdu.dbs2$sp,
                                  lBN.dev.esdu.dbs2$cat,sep='-')

  # 21.2.2. Calculate abundance-at-size per echotype and species with size categories -----
  cat('Abundance-at-size per echotype and species with size categories calculation','\n')
  if (!is.null(spsel)){
    lechospcodes.mens2=unique(lBN.dev.esdu.dbs[lBN.dev.esdu.dbs$sp%in%spsel,
                                          'DEV'])
  }else{
    lechospcodes.mens2=unique(lBN.dev.esdu.dbs$DEV)
  }
  
  list.echotype.sp.Nsize=lapply(X=seq(length(lechospcodes.mens2)),
                                FUN=size.ventilator.sp,
                                lBN.dev.esdu.db=lBN.dev.esdu.dbs,
                                mens=mens2,Pechef=Pechef,Lname='Lcm',
                                Nname='NBIND',verbosit=verbosit,
                                spname='DEV')
  names(list.echotype.sp.Nsize)=lechospcodes.mens2  
  
  # Check abundances consistency
  Ntot0=sum(lBN.dev.esdu.dbs$N)
  lBN.dev.esdu.dbsa.Ntotsp=aggregate(lBN.dev.esdu.dbs$N,
                                     list(lBN.dev.esdu.dbs$sp),sum)
  lBN.dev.esdu.dbsa.NtotDEV=aggregate(lBN.dev.esdu.dbs$N,
                                          list(DEV=lBN.dev.esdu.dbs$DEV),sum)
  unique(lBN.dev.esdu.dbs$DEV)
  Ntot2=NULL
  Ntot2.sp=NULL
  for (i in 1:length(lechospcodes.mens2)){
    dfi=list.echotype.sp.Nsize[[i]]
    NL.Ntotdevi=aggregate(dfi$N,list(DEV=dfi$CodEsp2),sum)
    Ntot2.sp=rbind(Ntot2.sp,NL.Ntotdevi)
    Ntot2=cbind(Ntot2,sum(dfi$N))
  }
  Ntot0-sum(Ntot2)
  Ntot22.sp=merge(Ntot2.sp,lBN.dev.esdu.dbsa.NtotDEV,by.x='DEV',
                  by.y='DEV')
  Ntot22.sp$dN=Ntot22.sp$x.y-Ntot22.sp$x.x
  Ntot22.tot=apply(Ntot22.sp[,2:3],2,sum)
  Ntot22.tot[1]-Ntot22.tot[2]
  if (sum(Ntot22.sp$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per echotype and code species')
  }
  
  # 21.2.3. Calculate abundance-at-size without size category ----
  cat('Abundance-at-size without size category calculation','\n')
  # Remove size categories in total samples
  Pechef2=Pechef
  Pechef2$SIGNEP='0'
  Pechef2$CodEsp=paste(Pechef2$GENR_ESP,Pechef2$STRATE,Pechef2$SIGNEP,sep='-')
  rPechef2=aggregate.catches(Pechef2,spname='CodEsp',stname='NOSTA')
  names(rPechef2)
  dim(rPechef2$Pechei)
  Pechef2=rPechef2$Pechei
  # Remove size categories in subsamples
  mens3=mens2
  head(mens3)
  mens3$CATEG='0'
  mens3$CodEsp2=paste(mens3$GENR_ESP,mens3$CATEG,sep='-')
  mens3a=aggregate(mens3$NBIND,
                   list(NOSTA=mens3$NOSTA,CodEsp2=mens3$CodEsp2,
                        GENR_ESP=mens3$GENR_ESP,CATEG=mens3$CATEG,
                        Lcm=mens3$Lcm),sum)
  names(mens3a)[6]='NBIND'
  head(mens3)
  
  if (!is.null(spsel)){
    lspcodes.mens3=unique(lBN.dev.esdu.dbs2[lBN.dev.esdu.dbs2$sp%in%spsel,
                                               'CodEsp2'])
  }else{
    lspcodes.mens3=unique(lBN.dev.esdu.dbs2$CodEsp2)
  }

  list.sp.Nsize2=lapply(X=seq(length(lspcodes.mens3)),
                        FUN=size.ventilator.sp,
                        lBN.dev.esdu.db=lBN.dev.esdu.dbs2,
                        mens=mens3a,Pechef=Pechef2,Lname='Lcm',Nname='NBIND',
                        verbosit=verbosit,spname='CodEsp2')
  names(list.sp.Nsize2)=lspcodes.mens3

  # Check abundances consistency
  Ntot0=sum(lBN.dev.esdu.dbs$N)
  lBN.dev.esdu.dbsa.Ntotsp=aggregate(lBN.dev.esdu.dbs$N,
                                     list(lBN.dev.esdu.dbs$sp),sum)
  lBN.dev.esdu.dbsa.NtotSpCat=aggregate(lBN.dev.esdu.dbs2$N,
                                      list(CodEsp2=lBN.dev.esdu.dbs2$CodEsp2),
                                      sum)
  unique(lBN.dev.esdu.dbs2$CodEsp2)
  Ntot3=NULL
  Ntot3.spCat=NULL
  for (i in 1:length(lspcodes.mens3)){
    dfi=list.sp.Nsize2[[i]]
    NL.NtotspCat=aggregate(dfi$N,list(CodEsp2=dfi$CodEsp2),sum)
    Ntot3.spCat=rbind(Ntot3.spCat,NL.NtotspCat)
    Ntot3=cbind(Ntot3,sum(dfi$N))
  }
  Ntot0-sum(Ntot3)
  Ntot33.sp=merge(Ntot3.spCat,lBN.dev.esdu.dbsa.NtotSpCat,by.x='CodEsp2',
                  by.y='CodEsp2')
  Ntot33.sp$dN=Ntot33.sp$x.y-Ntot33.sp$x.x
  Ntot33.tot=apply(Ntot33.sp[,2:3],2,sum)
  Ntot33.tot[1]-Ntot33.tot[2]
  if (sum(Ntot33.sp$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per echotype and code species')
  }
  
    # 21.3. Check: N=sum(NLi)? ----
  #*************************
  li.pbN=check.abundance.length.species(list.sp.Nsize=list.sp.Nsize,type='N',
                                        eps=eps)

  #write.table(li.pb.db,paste(path.results,'/biomass.size.esdu/Pb-chamens.csv',sep=''),
  #            sep=';',row.names=FALSE)
  
  # 21.5. Add weight to number at-lengths ----
  #*************************
  #-> List of N dataframes (one per species) 
  #with abundances in no. of fish (N) and kg (W, based on LW equations) 
  #per size class (L) and ESDUs
  # size distributions per species code
  #undebug(numbers2weight.sizeclass)
  cat('Add weight to number at-lengths','\n')
  list.sp.NW.size=lapply(seq(length(list.sp.Nsize)),
                         FUN=numbers2weight.sizeclass,
                         list.sp.Nsize0=list.sp.Nsize,LW,
                         spname=spname)
  names(list.sp.NW.size)=names(list.sp.Nsize)
  names(list.sp.NW.size)
  
  # Check abundances consistency
  lspcodes.mens=names(list.sp.Nsize)
  Ntot0=sum(lBN.dev.esdu.dbs$N)
  lBN.dev.esdu.dbsa.Ntotsp=aggregate(lBN.dev.esdu.dbs$N,
                                     list(lBN.dev.esdu.dbs$sp),sum)
  lBN.dev.esdu.dbsa.NtotCodesp2=aggregate(lBN.dev.esdu.dbs$N,
                                          list(CodEsp2=lBN.dev.esdu.dbs$CodEsp2),sum)
  unique(lBN.dev.esdu.dbs$CodEsp2)
  Ntot1W=NULL
  Ntot1W.sp=NULL
  for (i in 1:length(lspcodes.mens)){
    dfi=list.sp.NW.size[[i]]
    NWL.Ntotspi=aggregate(dfi$N,list(CodEsp2=dfi$CodEsp2),sum)
    Ntot1W.sp=rbind(Ntot1W.sp,NWL.Ntotspi)
    Ntot1W=cbind(Ntot1W,sum(dfi$N))
  }
  Ntot0-sum(Ntot1W)
  Ntot1W.sp=merge(Ntot1W.sp,lBN.dev.esdu.dbsa.NtotCodesp2,by.x='CodEsp2',
                  by.y='CodEsp2')
  Ntot1W.sp$dN=Ntot1W.sp$x.y-Ntot1W.sp$x.x
  Ntot1W.tot=apply(Ntot1W.sp[,2:3],2,sum)
  if (sum(Ntot1W.sp$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per CodEsp2')
  }
  
  # size distributions per species
  list.sp.NW.size2=lapply(seq(length(list.sp.Nsize2)),
                           FUN=numbers2weight.sizeclass,
                          list.sp.Nsize2,LW,spname=spname)
  names(list.sp.NW.size2)=names(list.sp.Nsize2)
  
  # Check abundances consistency
  lspcodes.mens3=names(list.sp.Nsize2)
  Ntot0=sum(lBN.dev.esdu.dbs$N)
  lBN.dev.esdu.dbsa.Ntotsp=aggregate(lBN.dev.esdu.dbs$N,
                                     list(lBN.dev.esdu.dbs$sp),sum)
  lBN.dev.esdu.dbsa.NtotSpCat=aggregate(lBN.dev.esdu.dbs2$N,
                                        list(CodEsp2=lBN.dev.esdu.dbs2$CodEsp2),
                                        sum)
  unique(lBN.dev.esdu.dbs2$CodEsp2)
  Ntot3W=NULL
  Ntot3W.spCat=NULL
  for (i in 1:length(lspcodes.mens3)){
    dfi=list.sp.NW.size2[[i]]
    NL.NtotspCat=aggregate(dfi$N,list(CodEsp2=dfi$CodEsp2),sum)
    Ntot3W.spCat=rbind(Ntot3W.spCat,NL.NtotspCat)
    Ntot3W=cbind(Ntot3W,sum(dfi$N))
  }
  Ntot0-sum(Ntot3W)
  Ntot33W.sp=merge(Ntot3W.spCat,lBN.dev.esdu.dbsa.NtotSpCat,by.x='CodEsp2',
                  by.y='CodEsp2')
  Ntot33W.sp$dN=Ntot33W.sp$x.y-Ntot33W.sp$x.x
  Ntot33W.tot=apply(Ntot33W.sp[,2:3],2,sum)
  Ntot33W.tot[1]-Ntot33W.tot[2]
  if (sum(Ntot33W.sp$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per echotype and code species')
  }
  
  #check: N=sum(NLi)? -------
  #*************************
  li.pbNW=check.abundance.length.species(list.sp.Nsize=list.sp.NW.size,
                                         type='NW',eps=eps)      
  
  # Add weights to number-at-length per echotype and species categories
  cat('Add weights to number-at-length per echotype and species categories','\n')
  names(list.echotype.sp.Nsize)
  list.echotype.sp.NW.size=lapply(seq(length(list.echotype.sp.Nsize)),
                         FUN=numbers2weight.sizeclass,
                         list.sp.Nsize0=list.echotype.sp.Nsize,LW,
                         spname=spname)
  
  # Check abundances consistency
  Ntot0=sum(lBN.dev.esdu.dbs$N)
  lBN.dev.esdu.dbsa.Ntotsp=aggregate(lBN.dev.esdu.dbs$N,
                                     list(lBN.dev.esdu.dbs$sp),sum)
  lBN.dev.esdu.dbsa.NtotDEV=aggregate(lBN.dev.esdu.dbs$N,
                                      list(DEV=lBN.dev.esdu.dbs$DEV),sum)
  unique(lBN.dev.esdu.dbs$DEV)
  Ntot2W=NULL
  Ntot2W.sp=NULL
  for (i in 1:length(lechospcodes.mens2)){
    dfi=list.echotype.sp.Nsize[[i]]
    NL.Ntotdevi=aggregate(dfi$N,list(DEV=dfi$CodEsp2),sum)
    Ntot2W.sp=rbind(Ntot2W.sp,NL.Ntotdevi)
    Ntot2W=cbind(Ntot2W,sum(dfi$N))
  }
  Ntot0-sum(Ntot2W)
  Ntot22W.sp=merge(Ntot2W.sp,lBN.dev.esdu.dbsa.NtotDEV,by.x='DEV',
                  by.y='DEV')
  Ntot22W.sp$dN=Ntot22W.sp$x.y-Ntot22W.sp$x.x
  Ntot22W.tot=apply(Ntot22W.sp[,2:3],2,sum)
  Ntot22W.tot[1]-Ntot22W.tot[2]
  if (sum(Ntot22W.sp$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per echotype and code species')
  }
  
  # 21.6. One dataframe with abundances in no. of fish (N) and kg (W) --- 
  #per species code, size class (L) and ESDUs
  #*************************
  NW.esdu.codesp.size.long=NW.esdu.sp.size.long.summary(list.sp.NW.size)
  head(NW.esdu.codesp.size.long)  
  unique(NW.esdu.codesp.size.long$sp)
  
  lNtot.esus=unique(NW.esdu.codesp.size.long[,c('Esdu','CodEsp2','Ntot')])
  dim(lNtot.esus)
  lNtot.esus$sp=substr(lNtot.esus$CodEsp2,1,8)
  head(lNtot.esus)
  
  # Check abundance consistency
  Ntot1.df=sum(NW.esdu.codesp.size.long$N)
  Ntot1.sp.df=aggregate(NW.esdu.codesp.size.long$N,
                        list(CodEsp2=NW.esdu.codesp.size.long$CodEsp2),sum)
  Ntot0-Ntot1.df
  Ntot01.sp.df=merge(Ntot1.sp.df,lBN.dev.esdu.dbsa.NtotCodesp2,by.x='CodEsp2',
                   by.y='CodEsp2')
  Ntot01.sp.df$dN=Ntot01.sp.df$x.y-Ntot01.sp.df$x.x
  Ntot01.sp.df.tot=apply(Ntot01.sp.df[,2:3],2,sum)
  Ntot01.sp.df.tot[1]-Ntot01.sp.df.tot[2]
  if (sum(Ntot01.sp.df$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per echotype and code species')
  }
  
  # 21.6. One dataframe with abundances in no. of fish (N) and kg (W) -----
  #per echotype,species code, size category, size class (L) and ESDUs
  #*************************
  NW.esdu.echotype.codesp.size.long=NW.esdu.sp.size.long.summary(
    list.sp.NW.size0=list.echotype.sp.NW.size)
  head(NW.esdu.echotype.codesp.size.long)  
  unique(NW.esdu.echotype.codesp.size.long$sp)
  
  lNtot.esus.esp=unique(
    NW.esdu.echotype.codesp.size.long[,c('Esdu','CodEsp2','sp','Ntot')])
  dim(lNtot.esus.esp)
  head(lNtot.esus.esp)
  
  # Check abundance consistency
  Ntot2.df=sum(NW.esdu.echotype.codesp.size.long$N)
  Ntot2.sp.df=aggregate(
    NW.esdu.echotype.codesp.size.long$N,
    list(CodEsp2=NW.esdu.echotype.codesp.size.long$CodEsp2),sum)
  Ntot0-Ntot2.df
  Ntot02.sp.df=merge(Ntot2.sp.df,lBN.dev.esdu.dbsa.NtotDEV,by.x='CodEsp2',
                     by.y='DEV')
  Ntot02.sp.df$dN=Ntot02.sp.df$x.y-Ntot02.sp.df$x.x
  Ntot02.sp.df.tot=apply(Ntot02.sp.df[,2:3],2,sum)
  Ntot02.sp.df.tot[1]-Ntot02.sp.df.tot[2]
  if (sum(Ntot02.sp.df$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per echotype and code species')
  }
  
  # 21.7. One dataframe with abundances in no. of fish (N) and kg (W) ----
  #per species, size class (L) and ESDUs
  #*************************
  NW.esdu.sp.size.long=aggregate(
    NW.esdu.codesp.size.long[,c('W','N')],
    list(NW.esdu.codesp.size.long$Esdu,NW.esdu.codesp.size.long$L,
         NW.esdu.codesp.size.long$sp),sum)
  names(NW.esdu.sp.size.long)[seq(3)]=c('Esdu','L','sp')
  dim(NW.esdu.sp.size.long)
  head(NW.esdu.sp.size.long)
  
  # Check abundance consistency
  Ntot3.df=sum(NW.esdu.sp.size.long$N)
  Ntot3.sp.df=aggregate(
    NW.esdu.sp.size.long$N,list(CodEsp2=NW.esdu.sp.size.long$sp),sum)
  Ntot0-Ntot3.df
  names(lBN.dev.esdu.dbsa.Ntotsp)[1]='sp'
  Ntot03.sp.df=merge(Ntot3.sp.df,lBN.dev.esdu.dbsa.Ntotsp,by.x='CodEsp2',
                     by.y='sp')
  Ntot03.sp.df$dN=Ntot03.sp.df$x.y-Ntot03.sp.df$x.x
  Ntot03.sp.df.tot=apply(Ntot03.sp.df[,2:3],2,sum)
  Ntot03.sp.df.tot[1]-Ntot03.sp.df.tot[2]
  if (sum(Ntot03.sp.df$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per echotype and code species')
  }
  
  #Add total number of fish per esdu/species
  lNtot.esusa=aggregate(lNtot.esus[,c('Ntot')],list(Esdu=lNtot.esus$Esdu,
                                                    sp=lNtot.esus$sp),sum)
  names(lNtot.esusa)[3]='Ntot'
  head(lNtot.esusa)
  # Check abundance consistency
  NW.esdu.sp.size.longa.esuSp=aggregate(
    NW.esdu.sp.size.long$N,list(Esdu=NW.esdu.sp.size.long$Esdu,
                                sp=NW.esdu.sp.size.long$sp),sum)
  Ntot03.sp.df.Ntotcheck=merge(NW.esdu.sp.size.longa.esuSp,
                               lNtot.esusa,by.x=c('Esdu','sp'),
                               by.y=c('Esdu','sp'))
  head(Ntot03.sp.df.Ntotcheck)
  Ntot03.sp.df.Ntotcheck$dN=Ntot03.sp.df.Ntotcheck$Ntot-
    Ntot03.sp.df.Ntotcheck$x
  Ntot03.sp.df.Ntotcheck.tot=apply(Ntot03.sp.df.Ntotcheck[,3:4],2,sum)
  Ntot03.sp.df.Ntotcheck.tot[1]-Ntot03.sp.df.Ntotcheck.tot[2]
  if (sum(Ntot03.sp.df.Ntotcheck$dN)>1){
    stop('Difference between sum of total abundances and sum of abundances at length per echotype and code species')
  }
  
  dim(NW.esdu.sp.size.long)
  NW.esdu.sp.size.long=merge(NW.esdu.sp.size.long,lNtot.esusa,
                             by.x=c('Esdu','sp'),by.y=c('Esdu','sp'))
  NW.esdu.sp.size.long$PM=NW.esdu.sp.size.long$W/NW.esdu.sp.size.long$N
  dim(NW.esdu.sp.size.long)
  head(NW.esdu.sp.size.long)
  # check abundances consistency in dataframe 
  #*************************
  Biomres.sp.sizeas=aggregate(NW.esdu.codesp.size.long$N,
                              list(esdu=NW.esdu.codesp.size.long$Esdu,
                                   sp=NW.esdu.codesp.size.long$CodEsp2),sum)
  names(Biomres.sp.sizeas)[3]='Ntot2'
  Biomres.sp.sizeau=aggregate(NW.esdu.codesp.size.long$Ntot,
                              list(esdu=NW.esdu.codesp.size.long$Esdu,
                                   sp=NW.esdu.codesp.size.long$CodEsp2),unique)
  names(Biomres.sp.sizeau)[3]='Ntot'
  Biomres.sp.sizea=merge(Biomres.sp.sizeas,Biomres.sp.sizeau)
  dim(Biomres.sp.sizeas);dim(Biomres.sp.sizeau);dim(Biomres.sp.sizea)
  head(Biomres.sp.sizea)
  Biomres.sp.sizea$dN=Biomres.sp.sizea$Ntot-Biomres.sp.sizea$Ntot2
  Biomres.sp.sizea[abs(Biomres.sp.sizea$dN)>=eps,]
  
  # 21.7. Select rows with N>0 -------------
  #*************************
  NW.esdu.sp.size.longs=NW.esdu.sp.size.long[NW.esdu.sp.size.long$N>0,]
  dim(NW.esdu.sp.size.long);dim(NW.esdu.sp.size.longs)
  head(NW.esdu.sp.size.long)
  
  NW.esdu.echotype.codesp.size.longs=NW.esdu.echotype.codesp.size.long[NW.esdu.echotype.codesp.size.long$N>0,]
  dim(NW.esdu.echotype.codesp.size.long);dim(NW.esdu.echotype.codesp.size.longs)
  head(NW.esdu.echotype.codesp.size.long)
  
  # check mean biomass per esdu and species
  # with zeroes
  besp=aggregate(NW.esdu.sp.size.long$W,list(TC=NW.esdu.sp.size.long$Esdu,sp=NW.esdu.sp.size.long$sp),sum)
  table(besp$sp)
  mbsp=aggregate(besp$x,list(sp=besp$sp),mean)
  names(mbsp)=c('sp','mBkg')
  mbsp$mBt=mbsp$mBkg/1000
  # without zeroes
  besps=aggregate(NW.esdu.sp.size.longs$W,list(TC=NW.esdu.sp.size.longs$Esdu,
                                               sp=NW.esdu.sp.size.longs$sp),sum)
  table(besps$sp)
  mbsps=aggregate(besps$x,list(sp=besps$sp),mean)
  names(mbsps)=c('sp','mBkg')
  mbsps$mBt=mbsps$mBkg/1000
  
  # Adds esdus metadata
  head(ESDUDEVs)
  dim(NW.esdu.sp.size.longs)
  head(NW.esdu.sp.size.longs)
  Biomres.sp.size=merge(unique(ESDUDEVs[,c('esdu.id','TC','LONG','LAT')]),
                        NW.esdu.sp.size.longs,by.x='esdu.id',by.y='Esdu')
  Biomres.sp.size2=merge(unique(ESDUDEVs[,c('esdu.id','TC','LONG','LAT')]),
                        NW.esdu.sp.size.long,by.x='esdu.id',by.y='Esdu')
  head(Biomres.sp.size)
  dim(Biomres.sp.size)
  dim(Biomres.sp.size2)
  plot(ESDUDEVs$LONG,ESDUDEVs$LAT)
  points(Biomres.sp.size$LONG,Biomres.sp.size$LAT,pch=16)
  points(Biomres.sp.size2$LONG,Biomres.sp.size2$LAT,pch=16)
  
  Biomres.echotype.codesp.size=merge(
    unique(ESDUDEVs[,c('esdu.id','TC','LONG','LAT')]),
    NW.esdu.echotype.codesp.size.long,by.x='esdu.id',by.y='Esdu')
  Biomres.echotype.codesp.size$DEV=c(
    unlist(
      strsplit(Biomres.echotype.codesp.size$CodEsp2,
               split='[.]')))[
                 seq(1,4*length(Biomres.echotype.codesp.size$CodEsp2),4)]
  Biomres.echotype.codesp.size$sizeCat=c(
    unlist(
      strsplit(Biomres.echotype.codesp.size$CodEsp2,
               split='[.]')))[
                 seq(4,4*length(Biomres.echotype.codesp.size$CodEsp2),4)]
  head(Biomres.echotype.codesp.size)
  dim(Biomres.echotype.codesp.size)
  
  # 21.8. Checks -----------
  #*************************
  #Check unicity of results per species
  dim(Biomres.sp.size)
  dim(unique(Biomres.sp.size))
  
  # Check that sum(abundance per length class)=total abundance for selected species
  #*************************
  nwspa=aggregate(NW.esdu.sp.size.longs$N,list(sp=NW.esdu.sp.size.longs$sp),sum)
  head(biomres.sp.esdu)
  if (!is.null(spcat.rem)){
    head(B.dev.sp.esdu.df)
    devsp.id=paste(B.dev.sp.esdu.df$DEV,
                   gsub('-','.',substr(B.dev.sp.esdu.df$CodEsp,1,8)),
                   gsub('-','.',substr(B.dev.sp.esdu.df$CodEsp,15,15)),sep='.')
    B.dev.sp.esdu.dfs=B.dev.sp.esdu.df[!devsp.id%in%echo.rem,]
    dim(B.dev.sp.esdu.df);dim(B.dev.sp.esdu.dfs)
    biomres.sp.esdus=aggregate(B.dev.sp.esdu.dfs[,c('BB','BN')],
                               list(sp=B.dev.sp.esdu.dfs$GENR_ESP),sum)
    nwspa=merge(nwspa,biomres.sp.esdus[,c('sp','BN')],by.x='sp',by.y='sp')
    unique(biomres.sp.esdus$sp)[!unique(biomres.sp.esdus$sp)%in%
                                  unique(NW.esdu.sp.size.longs$sp)]
    length(unique(LW$GENR_ESP))
    length(spsel)
    nwspa$dN=nwspa[,2]-nwspa[,3]
  }else{
    nwspa=merge(nwspa,biomres.sp.esdu[,c('sp','BN')],by.x='sp',by.y='sp')
    unique(biomres.sp.esdu$sp)[!unique(biomres.sp.esdu$sp)%in%unique(NW.esdu.sp.size.longs$sp)]
    length(unique(LW$GENR_ESP))
    length(spsel)
    nwspa$dN=nwspa[,2]-nwspa[,3]
  }
  if (!is.null(spsel)){
  cat('Difference btw. total abundance by length class and total abundance (should be zero):',
      sum(NW.esdu.sp.size.longs$N)-sum(biomres.sp.esdu[biomres.sp.esdu$sp%in%spsel,'BN']),'\n')
  }else{
  cat('Difference btw. total abundance by length class and total abundance (should be zero):',
      sum(NW.esdu.sp.size.longs$N)-sum(biomres.sp.esdu[,'BN']),'\n')  
  }
  # Check that sum(abundance per length class)=total abundance for all esdus 
  # (with zeroes)
  #*************************
  #head(NW.esdu.codesp.size.long)
  #NLclass.check(NW.esdu.codesp.size.long)
  head(NW.esdu.codesp.size.long)
  nwcspa=aggregate(NW.esdu.codesp.size.long$N,list(sp=NW.esdu.codesp.size.long$sp),
                   sum)
  names(nwcspa)[2]='NtotCodSp'
  nwcspa2=aggregate(NW.esdu.echotype.codesp.size.long$N,
                    list(sp=NW.esdu.echotype.codesp.size.long$sp),sum)
  names(nwcspa2)[2]='NtotEchoCodSp'
  if (!is.null(echo.rem)){ # Deprecated
    head(B.dev.sp.esdu.df)
    devsp.id=paste(B.dev.sp.esdu.df$DEV,
                   gsub('-','.',substr(B.dev.sp.esdu.df$CodEsp,1,8)),
                   gsub('-','.',substr(B.dev.sp.esdu.df$CodEsp,15,15)),sep='.')
    B.dev.sp.esdu.dfs=B.dev.sp.esdu.df[!devsp.id%in%echo.rem,]
    dim(B.dev.sp.esdu.df);dim(B.dev.sp.esdu.dfs)
    biomres.sp.esdus=aggregate(B.dev.sp.esdu.dfs[,c('BB','BN')],
                               list(sp=B.dev.sp.esdu.dfs$GENR_ESP),sum)
    nwcspa=merge(nwcspa,biomres.sp.esdus[,c('sp','BN')],by.x='sp',by.y='sp')
    nwcspa=merge(nwcspa,nwcspa2,by.x='sp',by.y='sp')
    nwcspa$dN1=nwcspa$NtotCodSp-nwcspa$BN
    nwcspa$dN2=nwcspa$NtotEchoCodSp-nwcspa$BN
    if (!is.null(spsel)){
      cat('Difference btw. total abundance by length/weight class and total abundance (should be zero):',
          sum(NW.esdu.codesp.size.long$N)-
            sum(biomres.sp.esdus[biomres.sp.esdus$sp%in%spsel,'BN']),'\n')
      cat('Difference btw. total abundance by length/weight class and echotype and total abundance (should be zero):',
          sum(NW.esdu.echotype.codesp.size.long$N)-
            sum(biomres.sp.esdus[biomres.sp.esdus$sp%in%spsel,'BN']),'\n')
    }else{
      cat('Difference btw. total abundance by length class and total abundance (should be zero):',
          sum(NW.esdu.sp.size.longs$N)-sum(biomres.sp.esdus[,'BN']),'\n')  
      cat('Difference btw. total abundance by length class and total abundance (should be zero):',
          sum(NW.esdu.echotype.codesp.size.longs$N)-sum(biomres.sp.esdus[,'BN']),'\n')  
    }
  }else{
    nwcspa=merge(nwcspa,biomres.sp.esdu[,c('sp','BN')],by.x='sp',by.y='sp')
    nwcspa=merge(nwcspa,nwcspa2,by.x='sp',by.y='sp')
    nwcspa$dN1=nwcspa$NtotCodSp-nwcspa$BN
    nwcspa$dN2=nwcspa$NtotEchoCodSp-nwcspa$BN
    if (!is.null(spsel)){
      cat('Difference btw. total abundance by length/weight class and total abundance (should be zero):',
          sum(NW.esdu.codesp.size.long$N)-
            sum(biomres.sp.esdu[biomres.sp.esdu$sp%in%spsel,'BN']),'\n')
      cat('Difference btw. total abundance by length/weight class and echotype and total abundance (should be zero):',
          sum(NW.esdu.echotype.codesp.size.long$N)-
            sum(biomres.sp.esdu[biomres.sp.esdu$sp%in%spsel,'BN']),'\n')
    }else{
      cat('Difference btw. total abundance by length class and total abundance (should be zero):',
          sum(NW.esdu.sp.size.longs$N)-sum(biomres.sp.esdu[,'BN']),'\n')  
      cat('Difference btw. total abundance by length class and total abundance (should be zero):',
          sum(NW.esdu.echotype.codesp.size.longs$N)-sum(biomres.sp.esdu[,'BN']),'\n')  
    }
  }  

  # Fish total abundance per species and size class
  #*************************
  head(NW.esdu.sp.size.longs)
  Ntot.sp.size=aggregate(NW.esdu.sp.size.longs[,c('N','W')],list(sp=NW.esdu.sp.size.longs$sp,L=NW.esdu.sp.size.longs$L),sum)
  names(Ntot.sp.size)[3:4]=c('Nsum','Wsum')
  Ntot.sp.size=Ntot.sp.size[order(Ntot.sp.size$sp,Ntot.sp.size$L),]
  Ntot.sp.size$mW=Ntot.sp.size$Wsum/Ntot.sp.size$Nsum
  
  #Check size distribution
  #*************************
  #-> check that species plots are unimodal
  plot.biomres.DistSize(BLsp=NW.esdu.sp.size.longs,ux11=ux11)
  mtext('Abundance per ESDUs size distribution')
  #compare with biological measurement size distributions
  mens.range.check(mens,ux11=ux11,verbosit=FALSE)
  mtext('biological measurement size distributions',cex=0.7)
  
  #-> check that all CodEsp plots are unimodal
  plot.biomres.DistSize(BLsp=NW.esdu.codesp.size.long,ux11=ux11,spname='CodEsp2')
  mtext('check that all CodEsp plots are unimodal')
  
  list(Biomres.sp.size=Biomres.sp.size2,list.sp.NW.size2=list.sp.NW.size2,
       mbsp=mbsp,chamens.wide=chamens.wide,chamens.long=chamens.long,
       nspdif=nwspa,li.pbN=li.pbN,li.pbNW=li.pbNW,
       Biomres.echotype.codesp.size=Biomres.echotype.codesp.size,
       newMens=newMens,
       chamens.hauls=list(codesp.mean=codesp.mean,codesp.sum=codesp.sum))  
}  


	check.biomass.size=function(NW.esdu.sp.size.long,B.esdu.db,BN.esdu.db,Pechef){

		#check
	  #*************************
		NW.esdu.siza=aggregate(NW.esdu.sp.size.long[,c('W','N')],
			list(NW.esdu.sp.size.long$sp),sum)	

		#comparison with initial total no. of fish
		head(B.esdu.db)
		Wtot.AN=sum(B.esdu.db[B.esdu.db$GENR_ESP=='ENGR-ENC','BB'])
		Ntot.AN=sum(B.esdu.db[B.esdu.db$GENR_ESP=='ENGR-ENC','BN'])
		Wtot.SA=sum(B.esdu.db[B.esdu.db$GENR_ESP=='SARD-PIL','BB'])
		Ntot.SA=sum(B.esdu.db[B.esdu.db$GENR_ESP=='SARD-PIL','BN'])
	
		cat(paste('Total anchovy biomass, per esdu (kg):',floor(Wtot.AN)),"\n")
		cat(paste('Total anchovy biomass, per esdu and size (kg):',
			floor(NW.esdu.siza[NW.esdu.siza[,1]=='ENGR-ENC','W']/1000)),
			"\n")
		cat(paste('Total anchovy number, per esdu :',floor(Ntot.AN)),"\n")
		cat(paste('Total anchovy number, per esdu and size:',
			floor(NW.esdu.siza[NW.esdu.siza[,1]=='ENGR-ENC','N'])),
			"\n")

		cat(paste('Total sardine biomass (kg):',floor(Wtot.SA)),"\n")
		cat(paste('Total sardine biomass, per esdu and size (kg):',
			floor(NW.esdu.siza[NW.esdu.siza[,1]=='SARD-PIL','W']/1000)),
			"\n")
		cat(paste('Total sardine number :',floor(Ntot.SA)),"\n")
		cat(paste('Total sardine number, per esdu and size:',
			floor(NW.esdu.siza[NW.esdu.siza[,1]=='SARD-PIL','N'])),
			"\n")

		#comparison with initial no. of fish per esdu
		NW.esdu.sp.size.longa=aggregate(NW.esdu.sp.size.long[,c('W','N')],
			list(NW.esdu.sp.size.long$esdu.id,NW.esdu.sp.size.long$sp),sum)	
		names(NW.esdu.sp.size.longa)=c('Esdu','sp','W','N')

# 		NW.esdu.csp.longaNtot=aggregate(NW.esdu.codesp.size.long[,c('Ntot')],
# 			list(NW.esdu.codesp.size.long$esdu.id,NW.esdu.codesp.size.long$sp,
# 			NW.esdu.codesp.size.long$CodEsp2),unique)	
# 		names(NW.esdu.csp.longaNtot)=c('Esdu','sp','codsp','Ntot')
# 
		NW.esdu.sp.longaNtot=aggregate(NW.esdu.sp.size.long[,c('Ntot')],
			list(NW.esdu.sp.size.long$esdu,NW.esdu.sp.size.long$sp),
			unique)	
		names(NW.esdu.sp.longaNtot)=c('Esdu','sp','Ntot')
		
		NW.esdu.sp.size.longa=merge(NW.esdu.sp.size.longa,
			NW.esdu.sp.longaNtot,by.x=c('Esdu','sp'),
			by.y=c('Esdu','sp'))

		dim(NW.esdu.sp.size.long)
		dim(unique(NW.esdu.sp.size.long))

		Nsp.check=NW.esdu.sp.size.longa$N-NW.esdu.sp.size.longa$Ntot
		cat('Initial number of nb. of fish per esdu minus sum of numbers-at-lengths per esdu',"\n")
		print(summary(Nsp.check))
		boxplot(Nsp.check,
			main='Initial number of nb. of fish per esdu 
				minus sum of numbers-at-lengths per esdu')
		
		# !!! mean(mean weights/esdu)!=total weight/total number!!
		NW.esdu.siza$mw=NW.esdu.siza$W/NW.esdu.siza$N
		cat('Anchovy mean weight(g) per ESDU summary:','\n')
    print(NW.esdu.siza[NW.esdu.siza$Group.1=='ENGR-ENC','mw']*1000)
		cat('Anchovy mean weight(g) in catches summary:','\n')
		print(summary(Pechef[Pechef$GENR_ESP=='ENGR-ENC','PM']))
		cat('Sardine mean weight(g) per ESDU summary:','\n')
		print(NW.esdu.siza[NW.esdu.siza$Group.1=='SARD-PIL','mw']*1000)
		cat('Sardine mean weight(g) in catches summary:','\n')
		print(summary(Pechef[Pechef$GENR_ESP=='SARD-PIL','PM']))
    
    NW.esdu.sp.size.longa
	}

#*************************
#Low-level functions -----
#*************************
	#*************************
	#Biomass per species, deviation and esdu ----
	#*************************
# i=1378
# k=1
# nk=Ndevi
#undebug(estim.ik)
#debug(estim.ik)  
k=1
i=1
	
	estim.k=function(nk,EsduDevi,list.Xei,EspDevi,list.Associ,method='M',
                   verbosit=TRUE) {
		biom=vector("list",length(nk))
		for (k in 1:length(nk)){
			cat(paste('D',nk[k],sep=''),"\n")
		  # Spot NA's in NASC
		  if (sum(is.na(EsduDevi[,paste('D',nk[k],sep='')]))>0){
		    stop('NAs found in ',paste('D',nk[k],sep=''))
		  }
      lesdus=EsduDevi$esdu.id
      length(lesdus);length(unique(lesdus))
      names(list.Associ)
      names(list.Xei)
      list.Xei.numnames=as.numeric(gsub('D','',names(list.Xei)))
      list.Xei=list.Xei[names(list.Xei)[order(list.Xei.numnames)]]
      list.Associ.numnames=as.numeric(gsub('D','',names(list.Associ)))
      list.Associ=list.Associ[names(list.Associ)[order(list.Associ.numnames)]]
			biom[[k]]=t(sapply(X=seq(along=EsduDevi[,1]),
				FUN=estim.ik,k,EspDevi,list.Xei,list.Associ,Ndev=nk,method=method,
                         verbosit=verbosit,lesdus=lesdus))
			#biom[[k]]=unique(biom[[k]])
		}
	biom
	}
  

# class(bioms)
# bioms=biom[[1]]
# head(bioms)
#
#estim.ik(i=116,k=4,EspDevi=EspDevi,list.Xei,list.Associ,Ndev=nk,method='M')
#estim.ik(i=100,k=1,EspDevi,list.Xei,list.Associ,Ndev=nk,method='pierre')


estim.k.check=function(nk,EsduDevi,list.Xei,EspDevi,list.Associ,method='M',
                 verbosit=FALSE) {
  biom=vector("list",length(nk))
  for (k in 1:length(nk)){
    cat(paste('D',nk[k],sep=''),"\n")
    lesdus=EsduDevi$esdu.id
    biom[[k]]=t(sapply(X=seq(along=EsduDevi[,1]),
                       FUN=estim.ik.check,k,EspDevi,list.Xei,list.Associ,
                       Ndev=nk,verbosit=FALSE,lesdus=lesdus))
    #biom[[k]]=unique(biom[[k]])
  }
  biom
}

  #bioms=data.frame(biom[[1]])
  #head(bioms)
  #bioms2=bioms[bioms$Esdu==1,]

#check differences btw 2 methods:
#dpm=estim.k.check(nk,EsduDevi,list.Xei,EspDevi,list.Associ,method='M')

# for (i in 1:length(Ndevi)){
#   dpmi=data.frame(dpm[[i]])
#   dpmi[,1]=Ndevi[i]
#   names(dpmi)[1]='DEV'
#   if (i==1){
#     dpmf=dpmi
#   }else{
#     dpmf=rbind(dpmf,dpmi)
#   }
# }
# 
# colMeans(dpmf)
# aggregate(dpmf[,-1],list(dpmf$DEV),summary)
  
estim.ik=function(i,k,EspDevi,list.Xei,list.Associ,Ndevi,method='M',verbosit=TRUE,lesdus){
  Di=paste('D',Ndevi[k],sep='')  
  biom.ik=rep(0,length(unique(EspDevi$CodEsp)))

  if (is.na(list.Associ[[k]]$Dev[i])) { 
    # NA que pour les esp.concernees
    biom.ik[EspDevi[,k+3]==1]=NA                     
  }else {
    #cat(i,"\n")
    
    #order list.Associ[[k]] in increasing esdu order
    list.Associ[[k]]=list.Associ[[k]][order(list.Associ[[k]]$Esdu),]
    lk=list.Associ[[k]]
    
    if (verbosit){
      print(paste('Echotype',Di))
      print(paste('i =',i))
      print(paste('Esdu',list.Associ[[k]]$Esdu[i]))
    }
    
    #to which haul:species is associated esdu i?  
    sel.ik=is.element(list.Xei[[k]]$NOCHAL,
                      list.Associ[[k]]$NOCHAL[i])
    
    #to which haul:species is associated esdu i?
    #select haul:species Xe
    lk=list.Associ[[k]]
    lk[i,]
    lks=lk[lk$Esdu==lesdus[i],]
    NOCHALs=lks[,'NOCHAL']
    xeik=list.Xei[[k]]
    xeiks=xeik[xeik$NOCHAL%in%NOCHALs,]
    #xeiks=list.Xei[[k]][sel.ik,]
    
    #which species will be taken into account?
    #(those in EspDevi)  
    EspDevis=EspDevi[EspDevi$CodEsp%in%list.Xei[[k]]$CodEsp[sel.ik],]
    sel.esp=is.element(unique(EspDevi$CodEsp),
                       list.Xei[[k]]$CodEsp[sel.ik])
    namesXe=as.character(unique(list.Xei[[k]]$CodEsp))
    #acoustic energy in deviation k partioned between species
    #found in haul associated with esdu i (via Xe) 
#     if (length(biom.ik[sel.esp])!=length(list.Associ[[k]]$Dev[i]*
#                                            list.Xei[[k]]$Xe[sel.ik])) {
#       browser()
#     }  
    #Pierre's method
#     if (list.Associ[[k]]$Dev[i]!=0 ) {
#       biom.ik[sel.esp]=list.Associ[[k]]$Dev[i]*list.Xei[[k]]$Xe[sel.ik]
#     }  
    #M's method
    Sakref=list.Associ[[k]]
    Sakrefs=Sakref[Sakref$Esdu==lesdus[i],]
    biom.ik2=merge(xeiks,Sakrefs,by.x='NOCHAL',by.y='NOCHAL')
    if (dim(biom.ik2)[1]==0&list.Associ[[k]]$Dev[i]>0){
      stop (paste('No reference haul for esdu',i,
                  'in echotype',k))
    } 
    biom.ik2$biom=biom.ik2$Dev*biom.ik2$Xe
    #list.Xei[[k]][sel.ik,]
    #list.Associ[[k]]$Dev[i]
  }
  if (list.Associ[[k]]$Dev[i]==0){
    biom.ik.res0.long=data.frame(Esdu=list.Associ[[k]]$Esdu[i],CodEsp=unique(EspDevi[,'CodEsp']),
                            biom=0)
    biom.ik.res0.long=biom.ik.res0.long[order(biom.ik.res0.long$CodEsp),]
    biom.ik.res0=reshape(biom.ik.res0.long[,c('Esdu','CodEsp','biom')],idvar=c('Esdu'),
                       timevar='CodEsp',direction='wide')
    names(biom.ik.res0)[-1]=gsub('biom.','',names(biom.ik.res0)[-1])
    biom.ik.res0=biom.ik.res0[,c('Esdu',sort(names(biom.ik.res0)[-1]))]
    if (method=='M')  {biom.ik.res=unlist(biom.ik.res0)
    }else {biom.ik.res=estim.ik.res}
  }else{
    #Pierre's method
    estim.ik.res=c(Esdu=list.Associ[[k]]$Esdu[i],biom.ik)
    names(estim.ik.res)=c('Esdu',as.character(unique(EspDevi$CodEsp)))
    #M's method  
    biom.ik2e=merge(biom.ik2,unique(EspDevi[,c('CodEsp','GENR_ESP')]),
                    by.x='CodEsp',by.y='CodEsp',all.y=TRUE)
    biom.ik2e[is.na(biom.ik2e$biom),'biom']=0
    biom.ik2e[is.na(biom.ik2e$Esdu),'Esdu']=unique(biom.ik2e[!is.na(biom.ik2e$Esdu),
                                                             'Esdu'])
    biom.ik2e=biom.ik2e[order(biom.ik2e$CodEsp),]
    biom.ik2ew=reshape(biom.ik2e[,c('Esdu','CodEsp','biom')],idvar=c('Esdu'),
                       timevar='CodEsp',direction='wide')
    names(biom.ik2ew)[-1]=gsub('biom.','',names(biom.ik2ew)[-1])
    dMP=biom.ik2ew-estim.ik.res[names(biom.ik2ew)]
    biom.ik2ew=biom.ik2ew[,c('Esdu',sort(names(biom.ik2ew)[-1]))]
    #estim.ik.res
    #biom.ik2ew=unique(biom.ik2ew)
    #biom.ik2ew
    if (method=='M')  {biom.ik.res=unlist(biom.ik2ew)
    }else {biom.ik.res=estim.ik.res}
  }
  biom.ik.res
}

k=2
i=20

estim.ik.check=function(i,k,EspDevi,list.Xei,list.Associ,Ndevi,method='M',verbosit=TRUE,lesdus){
  Di=paste('D',Ndevi[k],sep='')  
  biom.ik=rep(0,length(unique(EspDevi$CodEsp)))
  #lesdus=ESDUDEVs$esdu.id
  if (is.na(list.Associ[[k]]$Dev[i])) { 
    # NA que pour les esp.concernees
    biom.ik[EspDevi[,k+3]==1]=NA                     
  }else {
      #cat(i,"\n")
      
      #order list.Associ[[k]] in increasing esdu order
      list.Associ[[k]]=list.Associ[[k]][order(list.Associ[[k]]$Esdu),]
    
      #to which haul:species is associated esdu i?  
      sel.ik=is.element(list.Xei[[k]]$NOCHAL,
       					list.Associ[[k]]$NOCHAL[i])
      
      #to which haul:species is associated esdu i?
      #select haul:species Xe
      lk=list.Associ[[k]]
      lks=lk[lk$Esdu==lesdus[i],]
      NOCHALs=lks[,'NOCHAL']
      xeik=list.Xei[[k]]
      xeiks=xeik[xeik$NOCHAL%in%NOCHALs,]
      #xeiks=list.Xei[[k]][sel.ik,]
      
      #which species will be taken into account?
      #(those in EspDevi)  
      EspDevis=EspDevi[EspDevi$CodEsp%in%list.Xei[[k]]$CodEsp[sel.ik],]
      sel.esp=is.element(unique(EspDevi$CodEsp),
                         list.Xei[[k]]$CodEsp[sel.ik])
      namesXe=as.character(unique(list.Xei[[k]]$CodEsp))
      #acoustic energy in deviation k partioned between species
      #found in haul associated with esdu i (via Xe) 
      if (length(biom.ik[sel.esp])!=length(list.Associ[[k]]$Dev[i]*
                                             list.Xei[[k]]$Xe[sel.ik])) {
        browser()
      }  
      #Pierre's method
      if (list.Associ[[k]]$Dev[i]!=0 ) {
        biom.ik[sel.esp]=list.Associ[[k]]$Dev[i]*list.Xei[[k]]$Xe[sel.ik]
      }  
      #M's method
      Sakref=list.Associ[[k]]
      Sakrefs=Sakref[Sakref$Esdu==lesdus[i],]
      biom.ik2=merge(xeiks,Sakrefs,by.x='NOCHAL',by.y='NOCHAL')
      if (dim(biom.ik2)[1]==0) stop (paste('No reference haul for esdu',i,
                                           'in echotype',k))
      biom.ik2$biom=biom.ik2$Dev*biom.ik2$Xe
      #list.Xei[[k]][sel.ik,]
      #list.Associ[[k]]$Dev[i]
  }
  #Pierre's method
  estim.ik.res=c(Esdu=list.Associ[[k]]$Esdu[i],biom.ik)
  names(estim.ik.res)=c('Esdu',as.character(unique(EspDevi$CodEsp)))
  #M's method  
  biom.ik2e=merge(biom.ik2,unique(EspDevi[,c('CodEsp','GENR_ESP')]),
                                       by.x='CodEsp',by.y='CodEsp',all.y=TRUE)
  biom.ik2e[is.na(biom.ik2e$biom),'biom']=0
  biom.ik2e[is.na(biom.ik2e$Esdu),'Esdu']=unique(biom.ik2e[!is.na(biom.ik2e$Esdu),
                                                           'Esdu'])
  biom.ik2e=biom.ik2e[order(biom.ik2e$CodEsp),]
  biom.ik2ew=reshape(biom.ik2e[,c('Esdu','CodEsp','biom')],idvar='Esdu',
                    timevar='CodEsp',direction='wide')
  names(biom.ik2ew)[-1]=substr(names(biom.ik2ew)[-1],6,20)
  dMP=biom.ik2ew-estim.ik.res[names(biom.ik2ew)]
  #estim.ik.res
  #biom.ik2ew=unique(biom.ik2ew)
  #biom.ik2ew
  if (method=='M')  {biom.ik.res=unlist(biom.ik2ew)
  }else {biom.ik.res=estim.ik.res}
  if (verbosit){
    print(paste('Echotype',Di))
    print(paste('i =',i))
    print(paste('Esdu',biom.ik.res[1]))
  }
  dmp=unlist(dMP)
  dmp
}


  #*************************
	#Biomass per species and Esdu -----
  #*************************

	biom.esp.i=function(i,list.Biom,EsduDevi) { 
	#  cat('Esdu ',i,"\n")
	# somme sur les k Dev
		Ndev=length(list.Biom)
		nesp=length(list.Biom[[1]][1,])-1
		biom.esp=matrix(0,nrow=nesp,ncol=length(list.Biom))
		for (k in 1:Ndev) {
			biom.esp[,k]=list.Biom[[k]][i,2:(nesp+1)]
		}
		biom=apply(biom.esp,1,sum,na.rm=T) 
		nna=apply(t(apply(biom.esp,1,is.na)),1,sum) 
		biom[nna==Ndev]=NA
		# somme des categ de trac-tru 
		#esp=c('ENGR-ENC','SPRA-SPR','SARD-PIL','SCOM-SCO','SCOM-JAP',
		#	'TRAC-TRU-NA','TRAC-TRU-P','TRAC-TRU-G','TRAC-MED','MICR-POU')
		#sel.tt=esp=="TRAC-TRU-NA" | esp=="TRAC-TRU-P" | esp=="TRAC-TRU-G"
		#biom[esp=="TRAC-TRU-NA"]=sum(biom[sel.tt])
		#sel.tt=esp=="TRAC-TRU-P" | esp=="TRAC-TRU-G"
		#biom=biom[-seq(esp)[sel.tt]]
#  biom[6]=sum(biom[6:8])
#  biom=biom[-c(7,8)]
	biom.esp.res=c(EsduDevi$esdu.id[i],EsduDevi$LAT[i],EsduDevi$LONG[i],
		biom)
	biom.esp.res
	}

#*************************
# Biomass (number and weight) per Esdu, species and size ------------
#*************************
#Create esdu~haul length distribution link, based on esdu~haul link ---- 
#************************* 
# spsel=c('ENGR-ENC','SARD-PIL')
# spsel=c('ENGR-ENC')
# showDist=TRUE

charef2chamens=function(ESDUDEVs2,B.dev.sp.esdu.df2,Pechef,EspDev,Pechels=NULL,
                        mens=NULL,biomres.sp.esdu=NULL,list.assoc=NULL,
                        spsel=NULL,showDist=FALSE,minN=NULL,legpos='topleft',
                        export.plot=NULL,xlim=NULL,ylim=NULL,ux11=TRUE,
                        logit=TRUE,nid='NOCHAL',forceFind=FALSE,
                        useHaulSum=FALSE){
  
  csp.assess=unique(B.dev.sp.esdu.df$CodEsp)
  # If ESDUs/hauls links are provided, copy them into ESDUDEVs
  if (!is.null(list.assoc)){
    ESDUDEVs2=nhInEsdudev(ESDUDEVs=ESDUDEVs2,list.assoc=list.assoc[[1]])
  }
  head(ESDUDEVs2)
  
  # new echotypes definition: sp:size cat ----
  #*************************
  EspDev2=EspDev[EspDev$GENR_ESP!='COMP-LEM',]
  if (!is.null(spsel)){
    EspDev2=EspDev2[EspDev2$GENR_ESP%in%spsel,]
  }
  EspDev2$DEV=paste(EspDev2$DEV,EspDev2$GENR_ESP,EspDev2$SIGNEP,sep='.')
  EspDev2$DEV=gsub('-','.',EspDev2$DEV)
  head(EspDev2)
  devs=sort(unique(B.dev.sp.esdu.df$DEV))
  B.dev.sp.esdu.df2$DEV0=B.dev.sp.esdu.df2$DEV
  B.dev.sp.esdu.df2$DEV=paste(
    B.dev.sp.esdu.df2$DEV0,
    B.dev.sp.esdu.df2$GENR_ESP,substr(B.dev.sp.esdu.df2$CodEsp,15,15),sep='.')
  B.dev.sp.esdu.df2$DEV=gsub('-','.',B.dev.sp.esdu.df2$DEV)
  if (!is.null(spsel)){
    devs2=sort(
      unique(B.dev.sp.esdu.df2[(B.dev.sp.esdu.df2$GENR_ESP!='COMP-LEM')&
               B.dev.sp.esdu.df2$GENR_ESP%in%spsel,'DEV']))
  }else{
    devs2=sort(
      unique(B.dev.sp.esdu.df2[B.dev.sp.esdu.df2$GENR_ESP!='COMP-LEM','DEV']))
  }
  #create new chamens.wide
  ncref=data.frame(matrix(rep(NA,dim(ESDUDEVs2)[1]*length(devs2)),
                          ncol=length(devs2)))
  names(ncref)=paste(devs2,'.CREF',sep='')
  ESDUDEVs2nd=data.frame(ESDUDEVs2,ncref)
  names(ESDUDEVs2nd)
  dim(ESDUDEVs2nd)

  #check no. of fish in subsamples for each haul ----
  #*************************
  unique(mens$GENR_ESP)
  if (!is.null(spsel)) {
    menss=mens[mens$GENR_ESP%in%spsel,]
  }else {
    menss=mens
  }
  if (sum(!'CodEsp3'%in%names(menss))>0){
    stop('Missing CodEsp3 field in subsamples')
  }
  
  mensa=aggregate(menss$NBIND,list(menss[,nid],menss$CodEsp3),sum)
  names(mensa)=c('haul','sp','N')
  #print(mensa)
  #library(lattice)
  #histogram(~N|sp,mensa)
  aggregate(mensa$N,list(mensa$sp),summary)
  # New dataframe to store aggregated subsamples
  newMens=NULL
  echo.rem=NULL
  codesp.mean=NULL
  codesp.sum=NULL
  # associate NASCs per ESDUs with nearest hauls with sufficient abundance-at-lenght
  if (!is.null(minN)){
    if (minN>=min(mensa$N)){
      # select hauls with enough length measurements
      lhs=mensa[mensa$N>=minN,'haul']
      Pechefs=Pechef[Pechef[,nid]%in%lhs,]
      EspDevs=EspDev2[EspDev2$GENR_ESP%in%spsel,]
      length(EspDevs$DEV);length(unique(EspDevs$DEV))
      list.assocList=vector("list", length(devs))

      for (j in 1:length(devs2)){
        EspDevj=EspDevs[EspDevs$DEV==devs2[j],]
        lsedi=unique(EspDevj[EspDevj$GENR_ESP!='COMP-LEM','CodEsp'])
        head(B.dev.sp.esdu.df)
        B.dev.sp.esdu.dfi=B.dev.sp.esdu.df2[B.dev.sp.esdu.df2$DEV==devs2[j],]
        lspi=unique(B.dev.sp.esdu.dfi[,'CodEsp'])
        lspj=unique(paste(substr(lspi,1,8),substr(lspi,14,15),sep=''))
        if (length(lspi)>0){
          mensa1=mensa[mensa$sp%in%lspi,]
          # Check if there is subsample for species code lspi
          if (dim(mensa1)[1]>0){
            # Check if there is a haul with Nsubsample>minN for species code lspi
            lhsis=mensa1[mensa1$N>=minN,'haul']
            Pechefs=Pechef[Pechef[,nid]%in%lhsis,]
            if (length(lhsis)==0){
              if (useHaulSum&(sum(mensa1$N)>=minN)){
                # If no  haul with Nsubsample>minN for species code lspi and
                # useHaulSum, all subsamples from all hauls with species code lspi
                # are summed
                nochalai=paste(mensa1$haul,collapse = '-')
                nochalai.list=rep(nochalai,dim(ESDUDEVs2nd)[1])
                nochalai.list=rep(nochalai,dim(ESDUDEVs2nd)[1])
                ESDUDEVs2nd=ESDUDEVs2nd[order(ESDUDEVs2nd$esdu.id),]
                ESDUDEVs2nd[,paste(devs2[j],'.CREF',sep='')]=nochalai.list
                list.assocList[[j]]=nochalai.list
                names(list.assocList)[j]=devs2[j]
                names(ESDUDEVs2nd)
                dim(ESDUDEVs2nd)
                head(menss)
                menss2=menss[menss$CodEsp3==lspi,]
                menssa=aggregate(menss2[,c('NBIND','POIDSTAILLE','POIDSECHANT'
                                           ,'NECHANT')],
                                 list(GENR_ESP=menss2$GENR_ESP,CATEG=menss2$CATEG,
                                      SEXE=menss2$SEXE,CAMPAGNE=menss2$CAMPAGNE,
                                      POCHE=menss2$POCHE,TAILLE=menss2$TAILLE,
                                      UNITE=menss2$UNITE,INC=menss2$INC,
                                      Lcm=menss2$Lcm,VESSEL=menss2$VESSEL,
                                      CodEsp2=menss2$CodEsp2,
                                      CodEsp3=menss2$CodEsp3,
                                      discard=menss2$discard,
                                      STRATE=menss2$STRATE),
                                 sum)
                menssa$NOCHAL=paste(nochalai,'-sum',sep='')
                menssa$NOSTA=paste(nochalai,'-sum',sep='')
                menssa$PM=menssa$POIDSTAILLE/menssa$NBIND
                sort(names(menss))
                sort(names(menssa))
                menss=rbind(menss[menss$CodEsp3!=lspi,],menssa[,names(menss)])
                newMens=rbind(newMens,menssa[,names(menss)])
                cat('Sum of length distributions of hauls',nochalai,'used for',
                    devs2[j])
                codesp.sum=c(codesp.sum,devs2[j])
              }else{
                #if (!spcat.rem){
                  # If no  haul with Nsubsample>minN for species code lspi and
                  # enough subsamples when summing all hauls (useHaulSum)
                  # a mean haul is used
                  nochalai=paste(mensa1$haul,collapse = '-')
                  nochalai.list=rep(nochalai,dim(ESDUDEVs2nd)[1])
                  nochalai.list=rep(nochalai,dim(ESDUDEVs2nd)[1])
                  ESDUDEVs2nd=ESDUDEVs2nd[order(ESDUDEVs2nd$esdu.id),]
                  ESDUDEVs2nd[,paste(devs2[j],'.CREF',sep='')]=nochalai.list
                  list.assocList[[j]]=nochalai.list
                  names(list.assocList)[j]=devs2[j]
                  names(ESDUDEVs2nd)
                  dim(ESDUDEVs2nd)
                  head(menss)
                  menss2=menss[menss$CodEsp3==lspi,]
                  menssa=aggregate(menss2[,c('NBIND','POIDSTAILLE','POIDSECHANT'
                                             ,'NECHANT')],
                                   list(GENR_ESP=menss2$GENR_ESP,CATEG=menss2$CATEG,
                                        SEXE=menss2$SEXE,CAMPAGNE=menss2$CAMPAGNE,
                                        POCHE=menss2$POCHE,TAILLE=menss2$TAILLE,
                                        UNITE=menss2$UNITE,INC=menss2$INC,
                                        Lcm=menss2$Lcm,VESSEL=menss2$VESSEL,
                                        CodEsp2=menss2$CodEsp2,
                                        CodEsp3=menss2$CodEsp3,
                                        discard=menss2$discard,
                                        STRATE=menss2$STRATE),
                                   sum)
                  head(menssa)
                  menssam=unique(menssa[,c( "GENR_ESP","CATEG","SEXE",
                                            "CAMPAGNE",
                                            "UNITE","INC","VESSEL","CodEsp2",
                                            "CodEsp3","discard","STRATE")])
                  menssam$Lcm=sum(menssa$NBIND*menssa$Lcm/sum(menssa$NBIND))
                  menssam$TAILLE=menssam$Lcm
                  menssam$NBIND=mean(menssa$NBIND)
                  menssam$POIDSTAILLE=mean(menssa$POIDSTAILLE,na.rm=TRUE)
                  menssam$POIDSECHANT=mean(menssa$POIDSECHANT,na.rm=TRUE)
                  menssam$NECHANT=mean(menssa$NECHANT,na.rm=TRUE)
                  menssam$NOCHAL=paste(nochalai,'-mean',sep='')
                  menssam$NOSTA=paste(nochalai,'-mean',sep='')
                  menssam$PM=menssam$POIDSTAILLE/menssam$NBIND
                  menssam$POCHE=1
                  menssam=menssam[,names(menss)]
                  sort(names(menss))
                  sort(names(menssa))
                  sort(names(menssam))
                  menss=rbind(menss[menss$CodEsp3!=lspi,],
                              menssam[,names(menss)])
                  # Add new mean haul in subsample file
                  newMens=rbind(newMens,menssam[,names(menss)])
                  cat('Mean length of hauls',nochalai,'used for',
                      devs2[j])              
                  codesp.mean=c(codesp.mean,devs2[j])
                # }else{
                #   cat('No single or mean haul had not enough subsamples for echotype:species ',
                #        devs2[j],', N = ',sum(mensa1$N),
                #       '. Echotype removed from at-length calculations.','\n')
                #   echo.rem=c(echo.rem,devs2[j])
                # }
              }
            }else{
            # If hauls with Nsubsample>minN for species code lspi and
            # useHaulSum, search for nearest ones
              nhi=nearest.haul(Pechei=Pechefs,EsduDevi=ESDUDEVs2nd,
                               EspDevi=EspDev2,Ndevi=devs2[j],
                               corr=cos(mean(EsduDevi$LAT)*pi/180),
                               nid=nid,nsp='CodEsp2',forceFind=forceFind)
              list.associ=nhi$list.Assoc[[1]]
              if (sum(!is.na(list.associ$NOCHAL))==0){
                if (!spcat.rem){
                  stop('No adequate nearest haul found for echotype: ',devs2[j])
                }else{
                  cat('No adequate nearest haul found for echotype: ',devs2[j],
                      '. Echotype removed','\n')
                  ESDUDEVs2nd=ESDUDEVs2nd[
                    ,!names(ESDUDEVs2nd)%in%c(
                      devs2[j],paste(devs2[j],'.CREF',sep=''))]
                  names(list.assocList)[j]=devs2[j]
                  list.assocList=list.assocList[names(list.assocList)!=devs2[j]]
                  menss=menss[menss$GENR_ESP!=lspj,]
                  echo.rem=c(echo.rem,devs2[j])
                }  
              }else{
                list.associ=list.associ[order(list.associ$Esdu),]
                dim(list.associ);dim(ESDUDEVs2nd)
                unique(list.associ$NOCHAL)
                ESDUDEVs2nd=ESDUDEVs2nd[order(ESDUDEVs2nd$esdu.id),]
                ESDUDEVs2nd[,paste(devs2[j],'.CREF',sep='')]=list.associ$NOCHAL
                list.assocList[[j]]=list.associ
                names(list.assocList)[j]=devs2[j]
                names(ESDUDEVs2nd)
                dim(ESDUDEVs2nd)
                menss=rbind(menss[menss$GENR_ESP!=lspj,],
                            menss[menss$GENR_ESP==lspj&menss$NOCHAL%in%
                                    list.associ$NOCHAL,])
              }
            }
          }else{
            cat('No subsample for',devs2[j],'\n')
          }
          cat('NASC per ESDUs and echotype re-allocated to hauls with number of measured fish larger than minN for biomass at-length computations',"\n")
          cat('Display new ESDU~haul relationships',"\n")
          
          devs2.split=strsplit(devs2[j],'[.]')[[1]]
          spi=paste('PT',devs2.split[2],devs2.split[3],sep='.')
          devi=devs2.split[1]
          
          show.esdu.refhaul(Ndevi=devi,list.Associ=nhi$list.Assoc,
                            Pechelsi=Pechels[
                              ,c('NOCHAL','LONF','LATF',spi)],
                            Pechei=Pechefs[
                              paste(
                                Pechefs$GENR_ESP,Pechefs$SIGNEP,sep='-')==lspj,],
                            EsduDevi=ESDUDEVs2nd,labelit=TRUE,
                            radSA=0.05,pradius=0.2,scale.SA=2,legpos=legpos,
                            export.plot=export.plot,xlim=NULL,ylim=NULL,
                            ux11=ux11,logit=logit,nid=nid,nfile=devs2[j])
        }
      }
    }
    if (showDist) {
      print('Numbers-at-lengths distributions for all hauls')
      show.mensDist(mens2=menss,ux11=ux11,export.plot=export.plot,spsel=spsel)
    }
  chamens.wide=ESDUDEVs2nd  
  }else{
    chamens.wide=ESDUDEVs
    print('NASC per ESDUs and echotype~hauls associations unchanged for biomass at-length computations')
    list.assocList
    if (showDist) {
      print('Numbers-at-lengths distributions for all hauls')
      show.mensDist(mens=menss,ux11=ux11,export.plot=export.plot,spsel=spsel)
    }
  }
  names(chamens.wide)
  # All chamens.wide DEV in original B.dev.sp.esdu.df2 file?
  names(chamens.wide)[!names(chamens.wide)%in%paste(devs2,'.CREF',sep='')]
  devs2[!paste(devs2,'.CREF',sep='')%in%names(chamens.wide)]
  
  chamens.wides=chamens.wide[,c('esdu.id','TC',paste(devs2,'.CREF',sep=''))]
  names(chamens.wides)
  
  chamens.wide[,c(3:(length(devs2)+2))]
  
  chamens.long=reshape(
    chamens.wides[,c('esdu.id','TC',paste(devs2,'.CREF',sep=''))],
                       varying=c(paste(devs2,'.CREF',sep='')),
                       direction='long',timevar='DEV',idvar='TC',
                       v.names='NOCHAL.mens',times=devs2)
  head(chamens.long)
  unique(chamens.long$DEV)
  unique(chamens.long$NOCHAL.mens)
  
  #Add chamens to abundance per ESDU 
  lBN.dev.esdu.db2=B.dev.sp.esdu.df2[
    ,c('TC','charef','esdu.id','DEV','CodEsp','BN')]
  head(lBN.dev.esdu.db2)
  unique(B.dev.sp.esdu.df2$charef)
  names(lBN.dev.esdu.db2)=c('TC','charef','Esdu','DEV','CodEsp','N')
  dim(B.dev.sp.esdu.df2);dim(lBN.dev.esdu.db2)
  dim(chamens.long)
  head(chamens.long)
  head(lBN.dev.esdu.db2)
  sum(!unique(lBN.dev.esdu.db2$TC)%in%unique(chamens.long$TC))
  unique(lBN.dev.esdu.db2$DEV)[
    !unique(lBN.dev.esdu.db2$DEV)%in%unique(chamens.long$DEV)]
  if ((T%in%is.na(lBN.dev.esdu.db2$charef))|(!is.null(minN))){
    # put NOCHAL.mens into charef column
    lBN.dev.esdu.db3=merge(lBN.dev.esdu.db2,
                           chamens.long[,c('TC','DEV','NOCHAL.mens')],
                           by.x=c('TC','DEV'),by.y=c('TC','DEV'))
    lBN.dev.esdu.db3$charef=lBN.dev.esdu.db3$NOCHAL.mens
    lBN.dev.esdu.db3=lBN.dev.esdu.db3[,names(lBN.dev.esdu.db3)!='NOCHAL.mens']
  }
  if (dim(lBN.dev.esdu.db3)[1]<dim(B.dev.sp.esdu.df2)[1]){
    stop('Missing ESU:DEV in lBN.dev.esdu.db2 after addition of NOCHAL.mens')
  }
  id1=paste(B.dev.sp.esdu.df2$TC,B.dev.sp.esdu.df2$DEV)
  id2=paste(lBN.dev.esdu.db3$TC,lBN.dev.esdu.db3$DEV)
  B.dev.sp.esdu.df2s=B.dev.sp.esdu.df2[!id1%in%id2,]
  B.dev.sp.esdu.df2s[B.dev.sp.esdu.df2s$BN>0,]
  
  lBN.dev.esdu.db2$sp=paste(substr(lBN.dev.esdu.db2$CodEsp,1,4),
                            substr(lBN.dev.esdu.db2$CodEsp,6,8),sep='-')
  lBN.dev.esdu.db2$cat=substr(lBN.dev.esdu.db2$CodEsp,15,15)
  lBN.dev.esdu.db2$CodEsp2=paste(lBN.dev.esdu.db2$sp,lBN.dev.esdu.db2$cat,sep='-')
  head(lBN.dev.esdu.db2)
  if (!is.null(spsel)){
    lBN.dev.esdu.db2=lBN.dev.esdu.db2[lBN.dev.esdu.db2$sp%in%spsel,]
  }
  
  #check chamens
  if (!is.null(biomres.sp.esdu)){
    check1=aggregate(lBN.dev.esdu.db2$N,list(lBN.dev.esdu.db2$sp),sum)
    names(check1)=c('sp','BNtot')
    check1=merge(check1,biomres.sp.esdu,by.x='sp',by.y='sp')
    check1$dB=check1$BNtot-check1$biomN.esdu
    check1$rmw=check1$mw.g/check1$mw.g.catch
    check1$rmwp=check1$rmw
    check1$rmwp[is.infinite(check1$rmw)|is.na(check1$rmw)]=0  
    check1=check1[order(check1$BB),]
    par(mfrow=c(1,2),mar=c(4,6,3,3))
    barplot(check1$BB,names.arg = check1$sp,horiz=TRUE,las=2,main='Total abundance')
    #barplot(check1$rmwp,names.arg = check1$sp,horiz=TRUE,las=2,
    #        main='Assessed/sample 
    #        mean weight ratio')
    par(mar=c(6,4,1,1))
    plot(check1$rmwp,xaxt = "n",ylab='Assessed/sample mean weight ratio',
         xlab='',ylim=c(0,2))
    axis(1,at=seq(length(check1$rmwp)),labels=check1$sp,las=2)
    abline(h=1,lty=2)
  }
  
  if (!is.null(echo.rem)){
    lBN.dev.esdu.db2=lBN.dev.esdu.db2[
      !lBN.dev.esdu.db2$DEV%in%echo.rem,]
    cat('-----> Echotypes with too few fish removed:',
        paste(echo.rem,collapse=', '),'\n')
    dim(chamens.long)
    chamens.long=chamens.long[!chamens.long$DEV%in%echo.rem,]
    dim(chamens.long)
    names(chamens.wide)
    chamens.wide=chamens.wide[,!substr(names(chamens.wide),1,13)%in%echo.rem]
    head(chamens.wide)
  }
  if (!is.null(codesp.mean)){
    cat('-----> Mean haul/length used for echotypes with too few fish:',
        paste(codesp.mean,collapse=', '),'\n')
  }
  if (!is.null(codesp.sum)){
    cat('-----> Sum of hauls/lengths used for echotypes with too few fish:',
        paste(codesp.mean,collapse=', '),'\n')
  }

  list(chamens.long=chamens.long,lBN.dev.esdu.db2=lBN.dev.esdu.db2,
       chamens.wide=chamens.wide,newMens=newMens,echo.rem=echo.rem,
       codesp.sum=codesp.sum,codesp.mean=codesp.mean)
}

      
#       for (i in 1:dim(ESDUDEVs2)[1]){
#         for (j in 1:length(devs)){
#           EspDevj=EspDevs[EspDevs$DEV==devs[j],]
#           lsedi=unique(EspDevj[EspDevj$GENR_ESP!='COMP-LEM','CodEsp'])
#           lspj=paste(substr(lspj,1,8),substr(lspj,14,15),sep='')
#           head(B.dev.sp.esdu.df)
#           B.dev.sp.esdu.dfi=B.dev.sp.esdu.df2[B.dev.sp.esdu.df2$DEV==devs2[j]&B.dev.sp.esdu.df2$TC==ESDUDEVs2[i,'TC'],]
#           lspi=unique(B.dev.sp.esdu.dfi[B.dev.sp.esdu.dfi$BB>0,'CodEsp'])
#           lspj=paste(substr(lspi,1,8),substr(lspi,14,15),sep='')
#           #lspj=lspj[lspj!='COMP-LEM-0']
#           if (length(lspj)>0){
#             #mensa1=mensa[mensa$sp%in%lspj,]
#               if (is.null(ESDUDEVsList[[j]])&is.null(list.assocList[[j]])){
#                 esdudevk=ESDUDEVs2nd
#                 list.assock=list.assoc
#               }else{
#                 esdudevk=ESDUDEVsList[[j]]
#                 list.assock=list.assocList[[j]]
#               }
#               mensa1=mensa[mensa$sp%in%lspj,]
#               if (dim(mensa1)[1]>0){
#                 pasteU=function(x){
#                   y=paste(sort(unique(x)),collapse=',')
#                   y
#                 }
#                 spch=aggregate(mensa1$sp,list(haul=mensa1$haul),pasteU)
#                 #select hauls where all species with non null biomass in esdu are present
#                 lhsi=spch[spch$x==paste(sort(unique(lspj)),collapse=','),'haul']
#                 #select hauls where all species with non null biomass have more than minN subsamples
#                 #mensa1$sp=as.character(mensa1$sp)
# #                 mensa.wide=reshape(mensa1,idvar='haul',timevar='sp',direction='wide')
# #                 isSup=function(x,thr){
# #                   y=x>=thr
# #                   y
# #                 }
#                 #if (length(lspj[k])==1){
#                 #hsel=mensa1$N>=minN
# #                 }else{
# #                   hsel=rowSums(apply(mensa.wide[,paste('N',lspj,sep='.')],2,isSup,minN),na.rm=TRUE)==length(lspj)
# #                 }
#                 lhsis=mensa1[mensa1$N>=minN,'haul']
#                 if (length(lhsis)==0){
#                   stop('No haul has not enough subsamples for echotype',devs[j],'in esdu',ESDUDEVs2[i,'esdu.id'])
#                 }
#                 nhi=nearest.haul(Pechei=Pechefs,EsduDevi=esdudevk,EspDevi=EspDev2,
#                       Ndevi=devs2[j],corr=cos(mean(EsduDevi$LAT)*pi/180),nid=nid)
#                 list.associ=list(nhi$list.Assoc)
#                 names(list.associ[[1]])=devs2[j]
#                 
#                 ESDUDEVsi=nhInEsdudev(ESDUDEVs=ESDUDEVsi,list.assoc=list.associ[[1]])
#                 ESDUDEVsic=rbind(ESDUDEVsi,ESDUDEVsni)
#                 names(list.associ)=devs[i]
#                 
#                 
#                 if (!esdudevk[i,paste(devs[j],'.CREF',sep='')]%in%lhsi){
#                   nhi.res=nearest.haul(Pechei=Pechefs[Pechef$NOCHAL%in%lhsis,],EsduDevi=esdudevk[i,],
#                                        EspDevi=EspDevj,
#                                        Ndevi=Ndevi[j],corr=cos(mean(EsduDevi$LAT)*pi/180),nid=nid)
#                   nhi=unlist(nhi.res$list.Assoc)
#                   esdudevk[i,paste(devs[j],'.CREF',sep='')]=nhi[names(nhi)=='NOCHAL']
#                   list.assock[[1]][[j]][i,'NOCHAL']=nhi[names(nhi)=='NOCHAL']
#                   cat(devs[j],'Reference haul with not enough subsamples for species',lspj[k],'in esdu',esdudevk[i,'esdu.id'],', 
#                   replaced by ref. haul no.',nhi[names(nhi)=='NOCHAL'],'\n')
#                 }
#                 ESDUDEVsList[[j]][[k]]=esdudevk
#                 list.assocList[[j]][[k]]=list.assock
#               }
#             
#           }
#         }
#         chamens.wide=ESDUDEVsList
#       }
# 
# #       for (i in 1:length(devs)){
# #         # ESDUs for which reference hauls of echotype devs[i] is NA 
# #         ESDUDEVsi=ESDUDEVs[is.na(ESDUDEVs[,paste(devs[i],'.CREF',sep='')]),
# #                          !names(ESDUDEVs)%in%c(devs[-i],paste(devs[-i],'.CREF',sep=''))]
# #         # ESDUs for which reference hauls of echotype devs[i] is not NA 
# #         ESDUDEVsni=ESDUDEVs[!is.na(ESDUDEVs[,paste(devs[i],'.CREF',sep='')]),
# #                          !names(ESDUDEVs)%in%c(devs[-i],paste(devs[-i],'.CREF',sep=''))]
# #         if (dim(ESDUDEVsi)[1]>0){
# #           # if ESDUs with no length measurements hauls, correct
# #           nhi=nearest.haul(Pechei=Pechefs,EsduDevi=ESDUDEVsi,EspDevi=EspDev,
# #                            Ndevi=Ndevi[i],corr=cos(mean(EsduDevi$LAT)*pi/180),nid=nid)
# #           list.associ=list(nhi$list.Assoc)
# #           names(list.associ[[1]])=devs[i]
# #           ESDUDEVsi=nhInEsdudev(ESDUDEVs=ESDUDEVsi,list.assoc=list.associ[[1]])
# #           ESDUDEVsic=rbind(ESDUDEVsi,ESDUDEVsni)
# #           names(list.associ)=devs[i]
# #         }else{
# #           # if not, do nothing 
# #           ESDUDEVsic=ESDUDEVsni
# #           list.associ=list.assoc[i]
# #           names(list.associ)=devs[i]
# #         }               
# #         if (i==1){
# #           ESDUDEVs2=ESDUDEVsic
# #           list.Assoc.db=list(list.Associ)
# #         }else{
# #           ESDUDEVs2=merge(ESDUDEVs2,ESDUDEVsic[,c('TC',devs[i],paste(devs[i],'.CREF',sep=''))],by.x='TC',by.y='TC')
# #           list.Assoc.db=c(list.Assoc.db,list(list.Associ))
# #         }  
# #       }
#       dim(ESDUDEVs);dim(ESDUDEVs2)
#       cat('NASC per ESDUs and echotype re-allocated to hauls with number of measured fish larger than minN for biomass at-length computations',"\n")
#       # select hauls with valid length measurements 
# #       menss2=menss[menss[,nid]%in%lhs,]
# #       if (!is.null(spsel)){
# #         menss2=menss2[menss2$GENR_ESP%in%spsel,]
# #       }
#       cat('Display new ESDU~haul relationships',"\n")
#       
#       show.esdu.refhaul(Ndevi=as.numeric(substr(devs,2,4)),list.Associ=list.Assoc.db[[1]],
#                       Pechelsi=Pechels,Pechei=Pechefs,EsduDevi=ESDUDEVs2,labelit=TRUE,
#                       radSA=0.05,pradius=0.5,scale.SA=2,legpos=legpos,
#                       export.plot=export.plot,xlim=xlim,ylim=ylim,ux11=ux11,logit=logit,nid=nid)
#       if (showDist){
#         #display numbers-at-lengths distributions for selected hauls
#         cat('Numbers-at-lengths distributions for selected hauls','\n')
#         show.mensDist(mens=menss2,ux11=ux11,export.plot=export.plot)
#       }
#     }
#   }else{
#     chamens.wide=ESDUDEVs
#     print('NASC per ESDUs and echotype~hauls associations unchanged for biomass at-length computations')
#     list.assocList
#     if (showDist) {
#       print('Numbers-at-lengths distributions for all hauls')
#       show.mensDist(mens=menss,ux11=ux11,export.plot=export.plot)
#     }
#   }
# 
#   names(chamens.wide)
#   chamens.wides=chamens.wide[,c('esdu.id','TC',paste('D',lNdev[[1]],'.CREF',sep=''))]
#   head(chamens.wides)
#   
#   chamens.long=reshape(chamens.wides,varying=list(3:(length(lNdev[[1]])+2)),direction='long',
#                        times=substr(names(chamens.wides)[3:(length(lNdev[[1]])+2)],1,2),
#                        timevar='DEV',idvar='TC',v.names='NOCHAL.mens')
#   head(chamens.long)
#   chamens.long=merge(chamens.long,unique(Pechef[,c('NOSTA','NOCHAL')]),
#                      by.x='NOCHAL.mens',by.y='NOCHAL')
#   names(chamens.long)=c("NOCHAL.mens","esdu.id","TC","DEV","NOSTA.mens")
#   head(chamens.long)
#   #Add chamens to abundance per ESDU 
#   lBN.dev.esdu.db2=B.dev.sp.esdu.df[,c('TC','charef','esdu.id','DEV','CodEsp','BN')]
#   head(lBN.dev.esdu.db2)
#   names(lBN.dev.esdu.db2)=c('TC','charef','Esdu','DEV','CodEsp','N')
#   if ((T%in%is.na(lBN.dev.esdu.db2$charef))|(!is.null(minN))){
#     lBN.dev.esdu.db2=merge(lBN.dev.esdu.db2,chamens.long[,c('TC','DEV','NOCHAL.mens')],by.x=c('TC','DEV'),by.y=c('TC','DEV'))
#     lBN.dev.esdu.db2$charef=lBN.dev.esdu.db2$NOCHAL.mens
#     lBN.dev.esdu.db2=lBN.dev.esdu.db2[,names(lBN.dev.esdu.db2)!='NOCHAL.mens']
#   }
#   lBN.dev.esdu.db2$sp=paste(substr(lBN.dev.esdu.db2$CodEsp,1,4),
#                             substr(lBN.dev.esdu.db2$CodEsp,6,8),sep='-')
#   lBN.dev.esdu.db2$cat=substr(lBN.dev.esdu.db2$CodEsp,15,15)
#   lBN.dev.esdu.db2$CodEsp2=paste(lBN.dev.esdu.db2$sp,lBN.dev.esdu.db2$cat,sep='-')
#   head(lBN.dev.esdu.db2)
#   if (!is.null(spsel)){
#     lBN.dev.esdu.db2=lBN.dev.esdu.db2[lBN.dev.esdu.db2$sp%in%spsel,]
#   }
#   
#   #check chamens
#   if (!is.null(biomres.sp.esdu)){
#     check1=aggregate(lBN.dev.esdu.db2$N,list(lBN.dev.esdu.db2$sp),sum)
#     names(check1)=c('sp','BNtot')
#     check1=merge(check1,biomres.sp.esdu,by.x='sp',by.y='sp')
#     check1$dB=check1$BNtot-check1$biomN.esdu
#     check1$rmw=check1$mw.g/check1$mw.g.catch
#     check1$rmwp=check1$rmw
#     check1$rmwp[is.infinite(check1$rmw)|is.na(check1$rmw)]=0  
#     check1=check1[order(check1$BB),]
#     par(mfrow=c(1,2),mar=c(4,6,3,3))
#     barplot(check1$BB,names.arg = check1$sp,horiz=TRUE,las=2,main='Total abundance')
#     #barplot(check1$rmwp,names.arg = check1$sp,horiz=TRUE,las=2,
#     #        main='Assessed/sample 
#     #        mean weight ratio')
#     par(mar=c(6,4,1,1))
#     plot(check1$rmwp,xaxt = "n",ylab='Assessed/sample mean weight ratio',
#          xlab='',ylim=c(0,2))
#     axis(1,at=seq(length(check1$rmwp)),labels=check1$sp,las=2)
#     abline(h=1,lty=2)
#  }
#   
#   list(chamens.long=chamens.long,lBN.dev.esdu.db2=lBN.dev.esdu.db2,
#        chamens.wide=chamens.wide)
# }

NAmens=function(x,lhs){
  x[!x%in%lhs]=NA
  x
}

nhInEsdudev=function(ESDUDEVs,list.assoc){
  for (i in 1:length(list.assoc)){
    list.associ=list.assoc[[i]]
    Devi=names(list.assoc)[i]
    head(ESDUDEVs);head(list.associ)
    ESDUDEVs=merge(ESDUDEVs,list.associ[,c('Esdu','NOCHAL')],by.x='esdu.id',by.y='Esdu')
    ESDUDEVs[,paste(Devi,'CREF',sep='.')]=ESDUDEVs$NOCHAL
    ESDUDEVs=ESDUDEVs[,names(ESDUDEVs)!='NOCHAL']
  }
  ESDUDEVs
}  

#display the length distributions of all hauls:species pairs

show.mensDist=function(mens2,ux11=FALSE,export.plot=NULL,spsel=NULL){
  lh=unique(mens2$NOSTA)
  for (i in 1:length(lh)){
    mensi=mens2[mens2$NOSTA==lh[i],]
    lspj=unique(as.character(mensi$GENR_ESP))
    if (!is.null(spsel)){
      lspj=lspj[lspj%in%spsel]
    }
    if (length(lspj)!=1){
      par(mfrow=c(2,3))
    }else{
      par(mfrow=c(1,1))
    } 
    for (j in 1:length(lspj)){
      mensj=mensi[mensi$GENR_ESP==lspj[j],]
      mensj=mensj[order(mensj$Lcm),]
      if (!is.null(export.plot)){
        png(file=paste(export.plot,'haul-',lh[i],'-',lspj[j],'.png',sep=''),
            width=800,height=800)
      }else if (ux11) x11()

      plot(mensj$Lcm,mensj$NBIND,type='l',lwd=2,main=paste('Haul',lh[i],lspj[j],'N =',sum(mensj$NBIND)),
           xlab='Fish length (cm)',ylab='No. of fish')
      if (!is.null(export.plot)){
        dev.off()
      }		
    }
  }
  par(mfrow=c(1,1))
  if (!is.null(export.plot)){
    cat('Length distribution plot saved to:',export.plot,'\n')
  }		
}

	#Abundance (in no. of fish) per ESDU, deviation and species, ----
	#with charef and chamens 
  #*************************

	B.esdu.dev.sp.charef.chamens=function(BN.dev.esdu.dbs,mensEsdu){

		#Biomass per echotype and esdu in long format ----
	  #*************************
		dim(BN.dev.esdu.dbs)
		dim(unique(BN.dev.esdu.dbs))
		head(BN.dev.esdu.dbs)
		lBN.dev.esdu.db=reshape(BN.dev.esdu.dbs,
			idvar=c("TC","Esdu","DEV","charef"),
			varying=list(names(BN.dev.esdu.dbs)[!names(BN.dev.esdu.dbs)%in%
			c('TC','Esdu','DEV','charef')]),
			v.names="N",timevar="CodEsp",direction='long',
			times=names(BN.dev.esdu.dbs)[!names(BN.dev.esdu.dbs)%in%
			c('TC','Esdu','DEV','charef')])
		head(lBN.dev.esdu.db)
		dim(unique(lBN.dev.esdu.db))

		lBN.dev.esdu.db$sp=paste(substr(lBN.dev.esdu.db$CodEsp,1,4),
			substr(lBN.dev.esdu.db$CodEsp,6,8),sep='-')
		lBN.dev.esdu.db$cat=substr(lBN.dev.esdu.db$CodEsp,15,15)
		lBN.dev.esdu.db$CodEsp2=paste(lBN.dev.esdu.db$sp,
			lBN.dev.esdu.db$cat,sep='-')
    head(lBN.dev.esdu.db)  
		dim(lBN.dev.esdu.db)
		dim(unique(lBN.dev.esdu.db))

		#Merge with chamens -----
		#*************************
		mensEsdu$CodEsp2=paste(mensEsdu$GENRE_ESP,mensEsdu$CATEG,sep='-')
		dim(lBN.dev.esdu.db)
		lBN.dev.esdu.db=merge(lBN.dev.esdu.db,
			unique(mensEsdu[,c('CodEsp2','NOCHA','NOSTA.mens')]),
			by.x=c('CodEsp2','charef'),by.y=c('CodEsp2','NOCHA'))
		dim(lBN.dev.esdu.db)
		dim(unique(lBN.dev.esdu.db))
		lBN.dev.esdu.db=merge(lBN.dev.esdu.db,
			unique(Peche[,c('NOSTA','NOCHAL')]),by.x='NOSTA.mens',
			by.y='NOSTA')
		dim(lBN.dev.esdu.db)
		dim(unique(lBN.dev.esdu.db))	
		names(lBN.dev.esdu.db)[names(lBN.dev.esdu.db)=='NOCHAL']='NOCHAL.mens'
		#lBN.dev.esdu.dbs=lBN.dev.esdu.db[lBN.dev.esdu.db$N!=0,]
    lBN.dev.esdu.db$N=as.numeric(lBN.dev.esdu.db$N)
		lBN.dev.esdu.db
	}

#   i=9
#   dim(lBNi)
#   lBNi[lBNi$Esdu==834,]
#   match(2068,lBNi$Esdu)
  
  size.ventilator.sp=function(i,lBN.dev.esdu.db,mens,Pechef,
                              Lname='Lcm',Nname='NBIND',LMname='LM',
                              verbosit=TRUE,spname='CodEsp2'){

    lsp.mens=unique(lBN.dev.esdu.db[,spname])
		cat(lsp.mens[i],"\n")
		lBNi=lBN.dev.esdu.db[lBN.dev.esdu.db[,spname]==lsp.mens[i],]
		if (!spname%in%names(mens)){
		  lsp.df=data.frame(spname=lsp.mens)
		  lsp.df$CodEsp2=paste(unlist(strsplit(lsp.df$spname,'[.]'))[
		    seq(2,4*length(lsp.df$spname),4)],
		    unlist(strsplit(lsp.df$spname,'[.]'))[seq(3,4*length(lsp.df$spname),4)],
		    unlist(strsplit(lsp.df$spname,'[.]'))[seq(4,4*length(lsp.df$spname),4)],
		    sep='-')
		  mensi=mens[mens$CodEsp2==unique(lsp.df[
		    lsp.df$spname==lsp.mens[i],'CodEsp2']),]
		  Pechefi=Pechef[paste(Pechef$GENR_ESP,Pechef$SIGNEP,sep='-')==
		                   unique(lsp.df[
		                     lsp.df$spname==lsp.mens[i],'CodEsp2']),]
		}else{
		  mensi=mens[mens$CodEsp2==lsp.mens[i],]
		  Pechefi=Pechef[paste(Pechef$GENR_ESP,Pechef$SIGNEP,sep='-')==lsp.mens[i],]
		}
		head(lBNi)
		head(mensi)
		size.classi=sort(unique(c(mensi[,Lname],round(Pechefi[,LMname]))))
		head(lBNi)
		length(unique(lBNi$Esdu))
		msizei=t(sapply(X=seq(dim(lBNi)[1]),FUN=size.ventilator.j,
			size.classi,mensi,lBNi,Pechefi,Lname=Lname,Nname=Nname,LMname=LMname,
                    verbosit=verbosit,spname=spname))
    head(msizei)
		dimnames(msizei)[[2]]=paste('NL',size.classi,sep='')
		msizei.df=data.frame(lBNi,msizei)
    head(msizei.df)
    msizei.df$dN=msizei.df$N-apply(msizei,1,sum)
    msizei.df.pb=msizei.df[abs(msizei.df$dN)>1,]
    if (dim(msizei.df.pb)[1]>0) cat('Missing abundance for species',lsp.mens[i],'in esdus',unique(msizei.df.pb$Esdu),'\n')
    #mens[mens$NOCHAL==930&mens$GENR_ESP=='ENGR-ENC',]
		msizeis=msizei.df[
		  ,!names(msizei.df)%in%
		    c('NOSTA.mens','charef','DEV','Esdu','CodEsp2','CodEsp','sp','cat',
		                            'NOCHAL.mens')]
    head(msizeis)
		amsizei=aggregate(msizei.df[,!names(msizei.df)%in%c('NOSTA.mens',
			'charef','DEV','Esdu','GENR_ESP','CodEsp2','CodEsp','sp','cat',
			'NOCHAL.mens')],
			by=list(msizei.df$Esdu,msizei.df[,spname]),sum)
		names(amsizei)[1:2]=c('Esdu','CodEsp2')
		# Check abundances consistency
		amsizei.checksum=sum(msizei.df$N)-sum(lBNi$N)
		if (abs(amsizei.checksum)>1){
		  stop('Difference between sum of total abundances and sum of abundance-at-length for species:',
		       lsp.mens[i])
		}
		amsizei
	}  
  
#   li=amsizei
#   head(li)
#   Ncheck=apply(li[,!names(li)%in%c('Esdu','CodEsp2','N')],1,sum)-li$N
# 	summary(Ncheck)
#   li.pb=li[abs(Ncheck)>1,]
#   unique(li.pb$CodEsp2)
#   chamens.pb=lBN.dev.esdu.dbs[(lBN.dev.esdu.dbs$CodEsp2%in%unique(li.pb$CodEsp2))&
#     (lBN.dev.esdu.dbs$Esdu%in%unique(li.pb$Esdu)),]
#   chamens.pbs=chamens.pb[chamens.pb$N!=0,]
#   chpb=unique(chamens.pbs$NOSTA)
#   mens[mens$CodEsp2=='SCOM-SCO-0'&mens$NOSTA%in%chpb,]
#   chpb.peche=Pechef[Pechef$NOCHAL%in%chpb,]
#   chpb.peche.maq=chpb.peche[chpb.peche$GENR_ESP=='SCOM-SCO',]
#   unique(chpb.peche.maq$NOSTA)
#   j=3087
#   lBNi[j,]
#   Pechef[Pechef$NOCHAL==41,]
  
  #Function for ventilating abundance into size classes
  #Charef size frequencies are used if no size frequencies in chamens
  #If no size frequencies in charef, all the abundance is allocated to the 
  #size class corresponding to the mean length found in Pechef 
  
	size.ventilator.j=function(j,size.classi,mensi,lBNi,Pechefi,Lname='Lcm',
                             Nname='NBIND',LMname='LM',verbosit=TRUE,
	                           spname='CodEsp2'){
    if (verbosit) cat(unique(lBNi[,spname]),j,'\n')
		vsizej=rep(0,length(size.classi))
    length(vsizej)
		mensij=mensi[as.character(mensi$NOSTA)==as.character(lBNi[j,'NOSTA.mens']),]
    Ltype='Lf'
    if (dim(mensij)[1]==0){
      mensij=mensi[as.character(mensi$NOCHAL)==as.character(lBNi[j,'charef']),]
    }
    if (dim(mensij)[1]==0){
      mensij=Pechefi[as.character(Pechefi$NOCHAL)==as.character(lBNi[j,'charef']),]
      Ltype='LM'
    }
    if (lBNi[j,'N']>0){
      if (Ltype=='Lf'){
        mensija=aggregate(mensij[,Nname],list(mensij[,Lname]),sum)
        names(mensija)=c(Lname,Nname)  
		    selvij=size.classi%in%sort(mensija[,Lname])
        table(selvij)
		    keij=mensija[,Nname]/sum(mensija[,Nname])
		    sum(keij)
		    vsizej[selvij]=lBNi$N[j]*keij
	    }else if (Ltype=='LM'){
   	    selvij=size.classi%in%round(mensij[,LMname],0)
        table(selvij)
		    vsizej[selvij]=lBNi$N[j]
	    }
    }  
  sum(vsizej)-lBNi$N[j]      
	vsizej 
	}
i=9
#names(list.sp.Nsize0)

	numbers2weight.sizeclass=function(i,list.sp.Nsize0,LW,spname='CodEsp2'){
	  #get abundance at lengths
    lssi=list.sp.Nsize0[[i]]
    cspi=unique(lssi[,2])
    if (nchar(cspi)>10){
      spi0=unlist(strsplit(cspi,'[.]'))
      spi=paste(spi0[2],spi0[3],sep='-')
    }else{
      spi=substr(cspi,1,8) 
    }
		#get length-weight equation
    LWi=LW[LW[,spname]==spi,]
		dim(lssi)
    #remove string columns
		lssis=as.matrix(lssi[,!names(lssi)%in%c('Esdu','CodEsp2','N','dN')])
		head(lssis)
    #get length class breaks
		sl=as.numeric(as.character(substr(dimnames(lssis)[[2]],3,6)))
    #compute weight from length
    if (dim(LWi)[1]!=0){
      wl=LWi$a*sl^LWi$b
    }else{
      cat (paste('Length-weight relationship missing for species',cspi,'no weight will be computed'),'\n')
      wl=rep(NA,length(sl))
    }
    #add weights as first row in abundance matrix
		lssis2=rbind(wl,lssis)
		head(lssis2)
    #multiply abundances by weight
		w=apply(lssis2,2,colmult)/1000
		dimnames(w)[[2]]=paste('WL',substr(dimnames(w)[[2]],3,6),sep='')
		lssif=data.frame(lssi,w)
		head(lssif)
		lssif
	}	


	NW.esdu.sp.size.long.summary=function(list.sp.NW.size0,spname='CodEsp2'){
		for (i in 1:length(list.sp.NW.size0)){
			sp.NW.size.dfi=list.sp.NW.size0[[i]]
			head(sp.NW.size.dfi)
			colt=substr(names(sp.NW.size.dfi),1,2)
			sp.NW.size.dfiN=sp.NW.size.dfi[,c('Esdu',spname,
				names(sp.NW.size.dfi)[colt=='NL'])]
			sp.NW.size.dfiW=sp.NW.size.dfi[,c('Esdu',spname,
				names(sp.NW.size.dfi)[colt=='WL'])]
			Ls=as.numeric(substr(names(sp.NW.size.dfiN)[-c(1:2)],3,6))
			sumN1=sum(colSums(sp.NW.size.dfiN[,!names(sp.NW.size.dfiN)%in%
				c('Esdu','CodEsp2')]))
			sumW1=sum(colSums(sp.NW.size.dfiW[,!names(sp.NW.size.dfiW)%in%
				c('Esdu','CodEsp2')]))
			sp.NW.size.dfliN=reshape(sp.NW.size.dfiN,
				idvar=c("Esdu","CodEsp2"),
				varying=list(names(sp.NW.size.dfiN)[-seq(2)]),
				v.names="N",timevar="L",direction='long',times=Ls)
			sumN2=sum(sp.NW.size.dfliN$N)
			sumN1-sumN2
			sp.NW.size.dfliW=reshape(sp.NW.size.dfiW,
				idvar=c("Esdu","CodEsp2"),
				varying=list(names(sp.NW.size.dfiW)[-seq(2)]),
				v.names="W",timevar="L",direction='long',times=Ls)
			sumW2=sum(sp.NW.size.dfliW$W)
			sumW1-sumW2
			sp.NW.size.dfliNW=data.frame(sp.NW.size.dfliN,
				W=sp.NW.size.dfliW[,-seq(3)])
			if (nchar(unique(sp.NW.size.dfliNW$CodEsp2))>10){
			  sp0=unlist(strsplit(sp.NW.size.dfliNW$CodEsp2,'[.]'))
			  sp.NW.size.dfliNW$sp=paste(sp0[2],sp0[3],sep='-')
			}else{
			  sp.NW.size.dfliNW$sp=substr(sp.NW.size.dfliNW$CodEsp2,1,8)			  
			}
			sp.NW.size.dfliNW$Ntot=sp.NW.size.dfi$N
			head(sp.NW.size.dfliNW)
			cat(unique(sp.NW.size.dfliNW$CodEsp2),"\n")
			if (i==1){
				sp.NW.size.dfl=sp.NW.size.dfliNW
			}else{
				sp.NW.size.dfl=rbind(sp.NW.size.dfl,
					sp.NW.size.dfliNW)
			}
		}		
	sp.NW.size.dfl
	}

	#*************************
	# Biomass (number and weight) per Esdu, species and age -----
	#*************************
age.at.length.import.2sp=function(path1,path2,sp=c('ENGR-ENC','SARD-PIL'),
                              cruise,plotit=TRUE){
  if (class(path1)=='data.frame'){
    LA.AN=path1
  }else{
    #LA.AN=read.table(path1,sep=';',header=TRUE)
    LA.AN=read.csv2(path1)
  }
  names(LA.AN)=c("ref","date","NOCHAL","NOSTA","Lmm","poids","sexe",
                 "maturite","age")
  LA.AN$sp=sp[1]
  LA.AN$CAMPAGNE=cruise
  if (class(path2)=='data.frame'){
    LA.SA=path2
  }else{
    #LA.AN=read.table(path1,sep=';',header=TRUE)
    LA.SA=read.csv2(path2)
  }
  names(LA.SA)=c("ref","date","NOCHAL","NOSTA","Lmm","poids","sexe",
                 "maturite","age")
  LA.SA$sp=sp[2]
  LA.SA$CAMPAGNE=cruise
  head(LA.AN)
  head(LA.SA)
  LA.ANSA=rbind(LA.AN,LA.SA)
  dim(LA.ANSA)
  # Remove age0 fish and ageless fish
  dim(LA.ANSA)
  LA.ANSAs=LA.ANSA[LA.ANSA$age!=-1&LA.ANSA$age!='',]
  dim(LA.ANSAs)
  
  table(LA.AN$age)
  table(LA.SA$age)
  
  LA.ANs=LA.ANSAs[LA.ANSAs$sp=='ENGR-ENC',]
  LA.SAs=LA.ANSAs[LA.ANSAs$sp=='SARD-PIL',]
#   x11()
#   plot(LA.ANs$Lmm,LA.ANs$age)
#   
  Af.AN=table(LA.ANs$L,LA.ANs$age)/apply(table(LA.ANs$L,LA.ANs$age),1,sum)
  Af.SA=table(LA.SAs$L,LA.SAs$age)/apply(table(LA.SAs$L,LA.SAs$age),1,sum)
  
  Af.AN.df=data.frame(CAMPAGNE=cruise,GENRE_ESP=sp[1],
                      TAILLE=as.numeric(rep(dimnames(Af.AN)[[1]],dim(Af.AN)[2])),
                      AGE=as.numeric(rep(dimnames(Af.AN)[[2]],each=dim(Af.AN)[1])),
                      POURCENTAGE=c(Af.AN))
  Af.SA.df=data.frame(CAMPAGNE=cruise,GENRE_ESP=sp[2],
                      TAILLE=as.numeric(rep(dimnames(Af.SA)[[1]],dim(Af.SA)[2])),
                      AGE=as.numeric(rep(dimnames(Af.SA)[[2]],each=dim(Af.SA)[1])),
                      POURCENTAGE=c(Af.SA))
  if (plotit){
  #Ages at Lengths  plot
  par(mfrow=c(1,1))  
  plot(Af.AN.df[,'TAILLE'],Af.AN.df[,'AGE'],type='n',
       xlab='Length(cm)',ylab='Age(year)',main='Anchovy',pch=16,col=1)
  points(Af.AN.df[Af.AN.df$POURCENTAGE>0,'TAILLE'],
         Af.AN.df[Af.AN.df$POURCENTAGE>0,'AGE'],cex=1+Af.AN.df[Af.AN.df$POURCENTAGE>0,
                                                               'POURCENTAGE'],pch=16,col=1)
  legend('topleft',legend='% at age',pch=16) 
  plot(Af.SA.df[,'TAILLE'],Af.SA.df[,'AGE'],type='n',
       xlab='Length(cm)',ylab='Age(year)',main='Sardine',pch=16,col=1)
  points(Af.SA.df[Af.SA.df$POURCENTAGE>0,'TAILLE'],
         Af.SA.df[Af.SA.df$POURCENTAGE>0,'AGE'],cex=1+Af.SA.df[Af.SA.df$POURCENTAGE>0,
                                                               'POURCENTAGE'],pch=16,col=1)
  legend('topleft',legend='% at age',pch=16)
  }
  
  #Check and correct length classes 
  Af.AN.df$TAILLE=as.numeric(as.character(Af.AN.df$TAILLE))
  usize1=unique(Af.AN.df$TAILLE)
  inc1=median(usize1[2:length(usize1)]-usize1[1:(length(usize1)-1)])
  Lage.AN=seq(min(Af.AN.df$TAILLE),max(Af.AN.df$TAILLE),inc1)
  Lage.AN.check=Lage.AN%in%unique(Af.AN.df$TAILLE)
  if (FALSE%in%Lage.AN.check){
    cat('Missing length class in anchovy age at length',
        Lage.AN[!Lage.AN.check],'\n')
  }else{
    cat('No missing length class in anchovy age at length','\n')
  }
  Af.SA.df$TAILLE=as.numeric(as.character(Af.SA.df$TAILLE))
  usize2=unique(Af.SA.df$TAILLE)
  inc2=median(usize2[2:length(usize2)]-usize2[1:(length(usize2)-1)])
  Lage.SA=seq(min(Af.SA.df$TAILLE),max(Af.SA.df$TAILLE),inc2)
  Lage.SA.check=Lage.SA%in%unique(Af.SA.df$TAILLE)
  if (FALSE%in%Lage.SA.check) {
    cat('Missing length class in sardine age at length',
        Lage.SA[!Lage.SA.check],'\n')
  }else{
    cat('No missing length class in sardine age at length','\n')
  }    
  #Add missing age class if needed
  if (FALSE%in%Lage.AN.check){
    i=1
    while(sum(Lage.AN.check==FALSE)>0){
      posANcheck=match(FALSE,Lage.AN.check)
      Af.ANaround=Af.AN.df[Af.AN.df$TAILLE%in%c(Lage.AN[posANcheck]-inc1,
                                                Lage.AN[posANcheck]+inc1),]
      Af.ANarounda=aggregate(Af.ANaround$POURCENTAGE,
                             list(CAMPAGNE=Af.ANaround$CAMPAGNE,
                                  GENRE_ESP=Af.ANaround$GENRE_ESP,AGE=Af.ANaround$AGE),mean)
      names(Af.ANarounda)[4]='POURCENTAGE'
      Af.ANarounda$TAILLE=Lage.AN[posANcheck]
      ANa=Af.ANarounda[,names(Af.AN.df)]
      if (i==1){
        Af.AN.df.cor=rbind(Af.AN.df,ANa)
      }else{
        Af.AN.df.cor=rbind(Af.AN.df.cor,ANa)
      }
      Lage.AN.check[posANcheck]=TRUE
      i=i+1
    }
    cat('Missing length class in anchovy age at length added','\n')
  }else{
    Af.AN.df.cor=Af.AN.df
  }  
  if (FALSE%in%Lage.SA.check){
    i=1
    while(sum(Lage.SA.check==FALSE)>0){
      posSAcheck=match(FALSE,Lage.SA.check)
      Af.SAaround=Af.SA.df[Af.SA.df$TAILLE%in%c(Lage.SA[posSAcheck]-inc2,
                                                Lage.SA[posSAcheck]+inc2),]
      Af.SAarounda=aggregate(Af.SAaround$POURCENTAGE,
                             list(CAMPAGNE=Af.SAaround$CAMPAGNE,
                                  GENRE_ESP=Af.SAaround$GENRE_ESP,AGE=Af.SAaround$AGE),mean)
      names(Af.SAarounda)[4]='POURCENTAGE'
      Af.SAarounda$TAILLE=Lage.SA[posSAcheck]
      SAa=Af.SAarounda[,names(Af.SA.df)]
      if (i==1){
        Af.SA.df.cor=rbind(Af.SA.df,SAa)
      }else{
        Af.SA.df.cor=rbind(Af.SA.df.cor,SAa)
      }
      Lage.SA.check[posSAcheck]=TRUE
      i=i+1
    }
    cat('Missing length class in sardine age at length added','\n')
  }else{
    Af.SA.df.cor=Af.SA.df
  }   
  LA=rbind(Af.AN.df.cor,Af.SA.df.cor)
  head(LA)
    #LA$TAILLE=LA$TAILLE/10
  LAa=aggregate(LA$POURCENTAGE,list(LA$GENRE_ESP,LA$AGE),sum)
  names(LAa)=c('GENRE_ESP','AGE','Pi')
  LA=merge(LA,LAa,by.x=c('GENRE_ESP','AGE'),by.y=c('GENRE_ESP','AGE'))                                       
  LA$Ti=LA$TAILLE*LA$POURCENTAGE/LA$Pi
  wmLA=aggregate(LA$Ti,list(LA$GENRE_ESP,LA$AGE),sum)                                         
  names(wmLA)=c('GENRE_ESP','AGE','wmL')
  
  #check length-age keys -----
  #*************************
  aLA=aggregate(LA$POURCENTAGE,list(LA$GENRE_ESP,LA$TAILLE),sum)
  aLAs=aLA[(aLA[,1]=='ENGR-ENC'&!aLA[,2]%in%Lage.AN[!Lage.AN.check])&
           (aLA[,1]=='SARD-PIL'&!aLA[,2]%in%Lage.SA[!Lage.SA.check]),]
  aLAb=aLA[aLA[,3]==0,]
#   aLAs=aLA[!(aLA$Group.1=='ENGR-ENC'&aLA$Group.2%in%Lage.AN[!Lage.AN.check])|
#              (aLA$Group.1=='SARD-PIL'&aLA$Group.2%in%Lage.SA[!Lage.SA.check]),]
#   if (dim(aLAb)[1]>1){
#     if ((aLA[,1]=='ENGR-ENC')&
#   }
  if (sum(aLA[,3])!=dim(aLA)[1]){
    stop('Percentage at age do not sum to unity')
  }
  list(LA.ANSAs=LA.ANSAs,Af.AN.df=Af.AN.df,Af.SA.df=Af.SA.df,Af.AN.df.cor=Af.AN.df.cor,
       Af.SA.df.cor=Af.SA.df.cor,LA=LA)
}
	

	age.at.length.import.1sp=function(path1,sp=NULL,cruise,plotit=TRUE,spname="sp",
	                                  stname="num_station",
	                                  cnames=c("ref","Lmm","weight","age"),
	                                  Lname='Lcm',Pname='Poids',Aname='Age',
	                                  sep=';',path.export=NULL,
	                                  roundit.p5Inf=FALSE){
	  
	  if (class(path1)=='data.frame'){
	    LA.sp=path1
	  }else{
	    #LA.AN=read.table(path1,sep=';',header=TRUE)
	    path.split=strsplit(path1,split="[.]")
	    if (path.split[[1]][2]=='csv'){
	      LA.sp=read.table(path1,sep=sep,header=TRUE,
	                       fill = TRUE)
	    }else if (path.split[[1]][2]%in%c('xls','xlsx')){
	      library("readxl")
	      LA.sp=data.frame(read_excel(path1,sheet = "data"))
	    }else{
	      stop('Biometry file format not supported. Please provide a .csv or .xls file')
	    }
	  }
	  head(LA.sp)
	  if (!spname%in%names(LA.sp)){
	    LA.sp$sp=sp 
	  }
	  LA.sps=LA.sp[,c(stname,spname,cnames)]
	  #names(LA.sps)=c('station','sp','ref','L','poids','age')
	  LA.sps=LA.sps[LA.sps[,spname]==sp,]
	  names(LA.sps)[names(LA.sps)==Lname]='L'
	  names(LA.sps)[names(LA.sps)==Pname]='poids'
	  names(LA.sps)[names(LA.sps)==Aname]='age'
	  head(LA.sps)
	  LA.sps$CAMPAGNE=cruise
	  LA.sps$L
	  head(LA.sps)
	  sort(unique(as.numeric(LA.sps$L)))
	  
	  if (roundit.p5Inf){
	    LA.sp[,Lname] = as.numeric(as.character(LA.sp[,Lname])) 
	    LA.sp[,Lname]=floor(LA.sp[,Lname]*2) / 2
	    LA.sps$L = as.numeric(as.character(LA.sps$L))
	    LA.sps$L = floor(LA.sps$L*2) / 2
	    # Af.sp.df$TAILLE= floor(Af.sp.df$TAILLE*2) / 2
	    # Af.sp.df.cor$TAILLE= floor(Af.sp.df.cor$TAILLE*2) / 2
	  }
	  sort(unique(as.numeric(LA.sps$L)))
	  dim(LA.sps[LA.sps$L==9,])
	  
	  # Remove age0 fish and ageless fish
	  dim(LA.sps)
	  LA.sps$L=as.numeric(gsub(',','.',LA.sps$L))
	  LA.sps$poids=as.numeric(gsub(',','.',LA.sps$poids))
	  LA.sps$age=as.numeric(as.character(LA.sps$age))
	  LA.sps$poids=as.numeric(as.character(LA.sps$poids))
	  LA.spss=LA.sps[!is.na(LA.sps$age)&LA.sps$age!=-1,]
	  sort(unique(as.numeric(LA.spss$L)))
	  LA.spss$sp=LA.spss[,spname]
	  dim(LA.spss)
	  table(LA.spss$age)
	  Af.sp=table(LA.spss$L,LA.spss$age)/
	    apply(table(LA.spss$L,LA.spss$age),1,sum)
	  apply(Af.sp,1,sum)
	  
	  Af.sp.df=data.frame(CAMPAGNE=cruise,GENRE_ESP=sp,
	                      TAILLE=as.numeric(rep(dimnames(Af.sp)[[1]],dim(Af.sp)[2])),
	                      AGE=as.numeric(rep(dimnames(Af.sp)[[2]],each=dim(Af.sp)[1])),
	                      POURCENTAGE=c(Af.sp))
	  Af.sp.df=Af.sp.df[order(Af.sp.df$TAILLE),]
	  #Af.sp.df[Af.sp.df$TAILLE==17,]
	  
	  if (plotit){
	    #Ages at Lengths  plot
	    x11()
	    par(mfrow=c(1,1))  
	    plot(Af.sp.df[,'TAILLE'],Af.sp.df[,'AGE'],type='n',
	         xlab='Length(cm)',ylab='Age(year)',main=sp,pch=16,col=1)
	    points(Af.sp.df[Af.sp.df$POURCENTAGE>0,'TAILLE'],
	           Af.sp.df[Af.sp.df$POURCENTAGE>0,'AGE'],
	           cex=1+Af.sp.df[Af.sp.df$POURCENTAGE>0,'POURCENTAGE'],pch=16,col=1)
	    legend('topleft',legend='% at age',pch=16)
	    if (!is.null(path.export)){
	      dev.print(device=png,
	                filename=paste(path.export,cruise,"_",sp,"_LA.png",sep=''),
	                width=800,height=800)
	    }
	  }
	  
	  #Check and correct length classes 
	  Af.sp.df$TAILLE=as.numeric(as.character(Af.sp.df$TAILLE))
	  usize1=unique(Af.sp.df$TAILLE)
	  inc1=median(usize1[2:length(usize1)]-usize1[1:(length(usize1)-1)])
	  Lage.sp=seq(min(Af.sp.df$TAILLE),max(Af.sp.df$TAILLE),inc1)
	  Lage.sp.check=Lage.sp%in%unique(Af.sp.df$TAILLE)
	  ager=seq(min(Af.sp.df$AGE),max(Af.sp.df$AGE),1)
	  age.sp.check=ager%in%unique(Af.sp.df$AGE)
	  if (FALSE%in%Lage.sp.check){
	    cat('Missing length class in age at length for',sp,
	        Lage.sp[!Lage.sp.check],'\n')
	  }else{
	    cat('No missing length class in age at length for ',sp,'\n')
	  }
	  missingLengths=Lage.sp[!Lage.sp.check]
	  if (FALSE%in%age.sp.check){
	    cat('Missing age class in age at length for',sp,
	        ager[!age.sp.check],'\n')
	  }else{
	    cat('No missing age class in age at length for ',sp,'\n')
	  }
	  missingAges=ager[!age.sp.check]
	  aLA=aggregate(Af.sp.df$POURCENTAGE,
	                list(sp=Af.sp.df$GENRE_ESP,L=Af.sp.df$TAILLE),sum)
	  if (sum(aLA[,3])!=dim(aLA)[1]){
	    if (FALSE%in%Lage.sp.check){
	      #Add missing age class if needed
	      for (i in 1:length(missingLengths)){
	        # Add new length class to age class only if length class exist in dataset
	        if (missingLengths[i]%in%aLA$L){
	          dfi=data.frame(CAMPAGNE=cruise,GENRE_ESP=sp,
	                         TAILLE=missingLengths[i],
	                         AGE=unique(Af.sp.df$AGE),POURCENTAGE=0)
	          Af.sp.df=rbind(Af.sp.df,dfi)
	        }
	      }
	      i=1
	      while(sum(Lage.sp.check==FALSE)>0){
	        posANcheck=match(FALSE,Lage.sp.check)
	        Af.ANaround=Af.sp.df[Af.sp.df$TAILLE%in%c(Lage.sp[posANcheck]-inc1,
	                                                  Lage.sp[posANcheck]+inc1),]
	        Af.ANarounda=aggregate(Af.ANaround$POURCENTAGE,
	                               list(CAMPAGNE=Af.ANaround$CAMPAGNE,
	                                    GENRE_ESP=Af.ANaround$GENRE_ESP,AGE=Af.ANaround$AGE),mean)
	        names(Af.ANarounda)[4]='POURCENTAGE'
	        Af.ANarounda$TAILLE=Lage.sp[posANcheck]
	        ANa=Af.ANarounda[,names(Af.sp.df)]
	        if (i==1){
	          Af.sp.df.cor=rbind(Af.sp.df,ANa)
	        }else{
	          Af.sp.df.cor=rbind(Af.sp.df.cor,ANa)
	        }
	        Lage.sp.check[posANcheck]=TRUE
	        i=i+1
	      }
	      cat('Missing length class in age at length added for',sp,'\n')
	    }
	  }else{
	    Af.sp.df.cor=Af.sp.df
	  }  
	  head(Af.sp.df)
	  #LA$TAILLE=LA$TAILLE/10
	  LAa=aggregate(Af.sp.df$POURCENTAGE,list(Af.sp.df$GENRE_ESP,Af.sp.df$AGE),sum)
	  names(LAa)=c('GENRE_ESP','AGE','Pi')
	  Af.sp.df=merge(Af.sp.df,LAa,by.x=c('GENRE_ESP','AGE'),by.y=c('GENRE_ESP','AGE'))                                       
	  Af.sp.df$Ti=Af.sp.df$TAILLE*Af.sp.df$POURCENTAGE/Af.sp.df$Pi
	  wmLA=aggregate(Af.sp.df$Ti,list(Af.sp.df$GENRE_ESP,Af.sp.df$AGE),sum)                                         
	  names(wmLA)=c('GENRE_ESP','AGE','wmL')
	  
	  #check length-age keys -----
	  #*************************
	  aLA=aggregate(Af.sp.df$POURCENTAGE,list(Af.sp.df$GENRE_ESP,Af.sp.df$TAILLE),sum)
	  aLAs=aLA[(!aLA[,2]%in%Lage.sp[!Lage.sp.check]),]
	  aLAb=aLA[aLA[,3]==0,]
	  #   aLAs=aLA[!(aLA$Group.1=='ENGR-ENC'&aLA$Group.2%in%Lage.AN[!Lage.AN.check])|
	  #              (aLA$Group.1=='SARD-PIL'&aLA$Group.2%in%Lage.SA[!Lage.SA.check]),]
	  #   if (dim(aLAb)[1]>1){
	  #     if ((aLA[,1]=='ENGR-ENC')&
	  #   }
	  if (sum(aLA[,3])!=dim(aLA)[1]){
	    stop('Percentage at age do not sum to unity')
	  }

	  list(LA.sp=LA.sp,LA.sps=LA.spss,Af.sp.df=Af.sp.df,
	       Af.sp.df.cor=Af.sp.df.cor)
	}

	
	LAK.check=function(BLspi,LAi,mult=1,maini=''){

	    BLspia=aggregate(BLspi$N,list(BLspi$L),sum)
	    names(BLspia)=c('L','N')
	    x11()
	    plot(BLspia$L,BLspia$N,type='l',xlab='Length (cm)',ylab='No. of fish',
	         main=paste(maini,'estimated length frequencies'))
	    Ls=unique(BLspi$L)
	    lspi=unique(BLspi$sp)
	    cat(lspi,"\n")
	    aLAi=aggregate(LAi$POURCENTAGE,list(LAi$GENRE_ESP,LAi$TAILLE),sum)
	    age.classi=sort(unique(LAi$AGE))
	    aLAis=aLAi[aLAi$x>0,]
	    #check percent at age
	    if (sum(aLAis[,3])!=dim(aLAis)[1]){
	      stop('Percentage at age do not sum to unity')
	    }
	    #check size ranges in at length and biometry data
	    if (min(BLspia$L)<min(LAi$TAILLE/mult)){
	      Linf=unique(BLspia$L)[unique(BLspia$L)<min(LAi$TAILLE/mult)]
	      LAi=rbind(data.frame(GENRE_ESP=lsp[i],AGE=1,CAMPAGNE=unique(LAi$CAMPAGNE),
	                           TAILLE=Linf*mult,POURCENTAGE=1,Pi=NA,Ti=NA),LAi)
	      cat(lspi,'Missing lowest size class',Linf,'added in length age key','\n')
	    }else if  (min(BLspia$L)>=min(LAi$TAILLE)){
	      cat(lspi,'Catches size and length age key size ranges matched','\n')
	    }
	    if (max(BLspia$L)>max(LAi$TAILLE/mult)){
	      Lsup=unique(BLspia$L)[unique(BLspia$L)>max(LAi$TAILLE/mult)]
	      LAi=rbind(data.frame(GENRE_ESP=lsp[i],AGE=1,CAMPAGNE=unique(LAi$CAMPAGNE),
	                           TAILLE=Lsup*mult,POURCENTAGE=1,Pi=NA,Ti=NA),LAi)
	      cat(lspi,'Missing largest size class',Lsup,'added in length age key','\n')
	    }else if  (max(BLspia$L)<=max(LAi$TAILLE)){
	      cat(lspi,'Catches size and length age key size ranges matched','\n')
	    }
	    uBLspia=sort(unique(BLspia$L))
	    uLAi=sort(unique(LAi$TAILLE/mult))
	    missing.LAi=uBLspia[!uBLspia%in%uLAi]
	    if (length(missing.LAi)>0){
	      names(LAi)
	      # LAi=rbind(LAi,data.frame(
	      #   GENRE_ESP=lsp[i],AGE=rep(unique(LAi$AGE),length(missing.LAi)),
	      #   CAMPAGNE=unique(LAi$CAMPAGNE),
	      #   TAILLE=rep(missing.LAi,each=length(unique(LAi$AGE))),
	      #   POURCENTAGE=0,Pi=NA,Ti=NA))
	      cat(lspi,'Missing size class: ',paste(missing.LAi,collapse=','),
	           ' in length age key','\n')
	    }
    LAi
	}

i=1
	age.ventilator.sp=function(i,BLsp,LA,lsp=c('ENGR-ENC','SARD-PIL'),
	                           mult=10){
		BLspi=BLsp[BLsp$sp==lsp[i],]
   	LAi=LA[LA$GENRE_ESP==lsp[i],]
   	age.classi=sort(unique(LAi$AGE))
   	
   	# Check LAK internal coherence
   	LAi=LAK.check(BLspi,LAi,mult=mult)
   
		#BLspi1721=BLspi[BLspi$L>=17&BLspi$L<=21,]
		#BLspi1721[BLspi1721$N>0,]
    #dim(BLspi1721)
    #LAi[LAi$AGE==2,]
    
   	# Duplicated abundance at lengths?
   	idLN=paste(BLspi$esdu.id,BLspi$sp,BLspi$L)
   	length(idLN);length(unique(idLN))
   	
   	# Calculate abundance-at-age
		age.size.i=t(sapply(X=seq(dim(BLspi)[1]),FUN=age.ventilator.j,
			age.classi=age.classi,LAi=LAi,lBNi=BLspi,mult=mult))
  	dimnames(age.size.i)[[2]]=paste('N.Age',age.classi,sep='')
		age.size.i=data.frame(BLspi,age.size.i)
		head(age.size.i)
		dim(age.size.i)
		
		# Duplicated abundance at age and lengths?
		idLAN=paste(age.size.i$esdu.id,age.size.i$sp,age.size.i$L)
		length(idLAN);length(unique(idLAN))
		
		age.size.tot2=data.frame(esdu.id=age.size.i$esdu.id,Ntot.rows=apply(
		  age.size.i[
		    ,!names(
		      age.size.i)%in%
		      c('esdu.id','TC','LONG','LAT','sp','L','CodEsp2','Ntot','PM','PM2',
		        'a','b','N','W','Com1','Com2','Com3','CAMPAGNE','CATEG','Not')],
		                       1,sum))
		head(age.size.tot2)
		age.size.tot2=merge(age.size.tot2,unique(age.size.i[,c('esdu.id','Ntot')]),
		                    by.x='esdu.id',by.y='esdu.id')
		age.size.tot2$dN=age.size.tot2$Ntot.rows-age.size.tot2$Ntot
		summary(age.size.tot2$dN)
    
		#sum all N per by species / esu over length classes
		age.size.tot=aggregate(age.size.i[,!names(age.size.i)%in%
			c('esdu.id','TC','LONG','LAT','sp','L','CodEsp2','Ntot','PM','PM2','a','b','N','W','Com1','Com2','Com3','CAMPAGNE','CATEG')],
			by=list(age.size.i$esdu.id,age.size.i$sp),sum)
		names(age.size.tot)[1:2]=c('Esdu','sp')
		#sum Ntot per esu / species code (if present) over length classes
    if ('CodEsp2'%in%names(age.size.i)){
		  df.uNtot=aggregate(age.size.i[,c('Ntot')],
			  list(age.size.i$esdu.id,age.size.i$sp,age.size.i$CodEsp2),
			  unique)	
		  names(df.uNtot)=c('Esdu','sp','codsp','Ntot')
    }else{
      df.uNtot=aggregate(age.size.i[,c('Ntot')],
                         list(age.size.i$esdu.id,age.size.i$sp),
                         unique)	
      names(df.uNtot)=c('Esdu','sp','Ntot')
    }
		df.Ntot=aggregate(df.uNtot[,c('Ntot')],
			list(df.uNtot$Esdu,df.uNtot$sp),sum)	
		names(df.Ntot)=c('Esdu','sp','Ntot')
		dim(age.size.tot)
		age.size.tot=merge(age.size.tot,df.Ntot,
			by.x=c('Esdu','sp'),by.y=c('Esdu','sp'))
		dim(age.size.tot)
		names(age.size.tot)
		Ncheck=(apply(age.size.tot[,3:(dim(age.size.tot)[2]-1)],1,sum)-
		          age.size.tot$Ntot)
		
		age.size.tot
	}
j=11329
	j=1


	age.ventilator.j=function(j,age.classi,LAi,lBNi,mult=10){
    
    #creates vector of n ages for size L
    vagej=rep(0,length(age.classi))

    #select proportions of ages at length L in length-age key
    if (lBNi[j,'L']*mult<min(LAi$TAILLE)){
      stop(as.character(unique(LAi$GENRE_ESP)),
          ' length lesser than age-length minimum length, row',j)
      LAij=LAi[LAi$TAILLE==min(LAi$TAILLE),]
      LAij$TAILLE=lBNi[j,'L']*mult
    }else{
      LAij=LAi[as.character(LAi$TAILLE)==as.character(lBNi[j,'L']*mult),]
    }
    LAij=LAij[order(LAij$AGE),]
    #LAij=LAi[LAi$TAILLE==lBNi[j,'L'],]
  	keij=LAij$POURCENTAGE
    if (round(sum(keij))!=1){
      stop("Sum proportions at age do not sum to unity in length class: ",
           unique(LAij$TAILLE))
    }
    #select ages of interest    
		selvij=sort(age.classi)%in%sort(LAij$AGE)			
   
    #multiply proportions at age by number at length
		vagej[selvij]=lBNi$N[j]*keij
    Ncheckj=sum(vagej)-lBNi$N[j]
    if (round(Ncheckj)!=0){
      stop('Sum of numbers at age differ from number at length, row',j)
    }
  vagej  
	}

	numbers2weight.ageclass=function(i,list.sp.Nage,LA){
		lssi=list.sp.Nage[[i]]
		LAi=LA[LW$CodEsp2==lspcodes.mens[i],]
		names(lssi)
		lssis=as.matrix(lssi[,!names(lssi)%in%c('Esdu','CodEsp2','N')])
		sl=as.numeric(as.character(substr(dimnames(lssis)[[2]],3,6)))
		wl=LWi$Coeff_A_RTP*sl^LWi$Exposant_N_RTP
		lssis2=rbind(sl,lssis)
		w=apply(lssis2,2,colmult)/1000
		dimnames(w)[[2]]=paste('WL',substr(dimnames(w)[[2]],3,6),sep='')
		lssif=data.frame(lssi,w)
		lssif
	}	

	N.esdu.sp.age.long.summary=function(list.N.esdu.ANSA.age,wmLA=NULL,LW=NULL,mult=1){
		for (i in 1:length(list.N.esdu.ANSA.age)){
      sp.NW.size.dfiN=list.N.esdu.ANSA.age[[i]]
      head(sp.NW.size.dfiN)
			#colt=substr(names(sp.NW.size.dfiN),1,1)
			Ls=as.numeric(substr(names(sp.NW.size.dfiN)[!names(sp.NW.size.dfiN)%in%
        c('Esdu','sp','Ntot')],6,7))
			sp.NW.size.dfliN=reshape(sp.NW.size.dfiN,
				idvar=c("Esdu","sp",'Ntot'),
				varying=list(names(sp.NW.size.dfiN)[!names(sp.NW.size.dfiN)%in%
        c('Esdu','sp','Ntot')]),
				v.names="N",timevar="Age",direction='long',times=Ls)
			cat(unique(sp.NW.size.dfliN$sp),"\n")
      head(sp.NW.size.dfliN)
      head(wmLA)
      head(LW)
      if (!is.null(wmLA)&!is.null(LW)){
        sp.NW.size.dfliN=merge(sp.NW.size.dfliN,wmLA,by.x=c('sp','Age'),
            by.y=c('GENRE_ESP','AGE'))
        sp.NW.size.dfliN=merge(sp.NW.size.dfliN,LW[,c('GENR_ESP','a','b')],
            by.x=c('sp'),by.y=c('GENR_ESP'))
        head(sp.NW.size.dfliN)
        #compute mean length per age class in cm
        sp.NW.size.dfliN$wmL=sp.NW.size.dfliN$wmL/mult
        #compute mean weight per age class in grams
        sp.NW.size.dfliN$wmW=sp.NW.size.dfliN$a*(sp.NW.size.dfliN$wmL)^
          sp.NW.size.dfliN$b
        #compute weight per age class in tons
        sp.NW.size.dfliN$BA=sp.NW.size.dfliN$wmW*sp.NW.size.dfliN$N*1e-6
      }else{
        stop('Please provide length weight and mean length at age data')
      }       
			if (i==1){
				sp.NW.age.dfl=sp.NW.size.dfliN
			}else{
				sp.NW.age.dfl=rbind(sp.NW.age.dfl,sp.NW.size.dfliN)
			}
		}		
	sp.NW.age.dfl
	}

#**********************************************************  
#**********************************************************
#Plots ----
#**********************************************************
	#**********************************************************
	#**********************************************************  
	#Size distributions per species ----
	#**********************************************************

	plot.biomres.DistSize=function(BLsp=NW.esdu.sp.size.longs,ux11=FALSE,
	                               spname='sp',path.export=NULL,
	                               mtitle='Abundance at length per ESDU,',
	                               xlims=NULL){
	  lsp=unique(BLsp[,spname])
    
	  if (length(lsp)>1){
	    for (i in 1:length(lsp)){
	      if ((i%in%c(1,7,13,19,25,31,37))){
	        if (ux11){
	          x11(bg='white') 
	        }
	        par(mfrow=c(2,3))
	      }
	      BLspi=BLsp[BLsp[,spname]==lsp[i],]
	      BLspia=aggregate(BLspi$N,list(BLspi$L),sum)
	      names(BLspia)=c('L','N')
	      if (!is.null(xlims)){
	        plot(BLspia$L,BLspia$N,type='l',xlab='Length (cm)',
	             ylab='No. of fish',main=paste(mtitle,lsp[i]),xlim=xlims)
	      }else{
	        plot(BLspia$L,BLspia$N,type='l',xlab='Length (cm)',
	             ylab='No. of fish',main=paste(mtitle,lsp[i]))	        
	      }
	    }
	  }else{
      if (ux11){
	        x11(bg='white') 
      }
	    BLspi=BLsp[BLsp[,spname]==lsp,]
	    BLspia=aggregate(BLspi$N,list(BLspi$L),sum)
	    names(BLspia)=c('L','N')
	    if (!is.null(xlims)){
	      plot(BLspia$L,BLspia$N,type='l',xlab='Length (cm)',ylab='No. of fish',
	           main=paste(mtitle,lsp[i]),xlim=xlims)
	    }else{
	      plot(BLspia$L,BLspia$N,type='l',xlab='Length (cm)',ylab='No. of fish',
	           main=paste(mtitle,lsp[i]))	        
	    }
	  }
	  if (!is.null(path.export)){
	    for (i in 1:length(lsp)){
	      nfile=paste(cruise,'_',lsp[i],'_LengthDist.png',sep='')
	      png(file=paste(path.export,nfile,sep=''),width=600,height=600)
	      BLspi=BLsp[BLsp[,spname]==lsp[i],]
	      BLspia=aggregate(BLspi$N,list(BLspi$L),sum)
	      names(BLspia)=c('L','N')
	      plot(BLspia$L,BLspia$N,type='l',xlab='Length (cm)',ylab='No. of fish',
	           main=paste('Abundance at length per ESDU,',lsp[i]))
	      dev.off()
	    }
	  }
	}

	#**********************************************************  
	#Biomass and abundance per echotype ESDU and species ------
	#**********************************************************

	Bdev.sp.plot=function(k,biom.esdu.dev.sp,Ndev,save.plot=FALSE,
		path.res.charef=NULL,sp.sel=FALSE,logit=FALSE,smax=1,plotit=TRUE,
                        legpos='bottomleft',ux11=FALSE,pradius=1,
		spcol.db=data.frame(sp=c("ENGR.ENC","MICR.POU","SARD.PIL",
		                         "SCOM.SCO","SCOM.JAP","SPRA.SPR","TRAC.TRU","TRAC.MED",
		                         "CAPR.APE"),lcol=c(3,8,4,2,5,1,7,6,0))){

		devi=paste('D',Ndev[k],sep='')

		#Extract biomass per deviation and species from R outputs	
		biom.D1=biom.esdu.dev.sp[biom.esdu.dev.sp$DEV==
			paste('D',Ndev[k],sep=''),]
		head(biom.D1)
		biom.Dn=biom.D1[,!names(biom.D1)%in%c('Esdu','DEV','CAMPAGNE',
			'charef','TC','LAT','LONG')]
		apply(biom.Dn,2,sum)

		#aggregate per species/depth strata (= sum over size class)
		lspz=substr(names(biom.Dn),1,13)
		luspz=unique(lspz)
		#Sum per species over size categories

		for (i in 1:length(luspz)){
			bioms=biom.Dn[,lspz==luspz[i]]
			if (!is.null(dim(bioms))){
				slspz=data.frame(rowSums(bioms))
			}else{
				slspz=data.frame(bioms)
			}				
			names(slspz)=luspz[i]
			if (i==1){
				biom.D1.1z=data.frame(Esdu=biom.D1$Esdu,slspz)
			}else{
				biom.D1.1z=data.frame(biom.D1.1z,slspz)
			}
		}
		names(biom.D1.1z)
		head(biom.D1.1z)

		#selection of species with non null biomass
		Btot.D1=apply(biom.D1.1z[,-1],2,sum)
		Btot.D1s=Btot.D1[Btot.D1>0]
		Btot.D1l=data.frame(CodEsp=names(Btot.D1s),Btot=Btot.D1s)
		Btot.D1l$CodEsp=gsub('\\.', '-',Btot.D1l$CodEsp)
		Btot.D1l[,'GENR_ESP']=substr(Btot.D1l$CodEsp,1,8)
		Btot.D1l$DEV=devi
		Btot.D1l$CAMPAGNE=unique(biom.D1$CAMPAGNE)		

		#Adds metadata
		biom.D1s=data.frame(biom.D1.1z[,c(TRUE,Btot.D1>0)])
		names(biom.D1s)
		dim(biom.D1s)
		biom.D1s=merge(biom.D1[,c('TC','Esdu','LAT','LONG')],
			biom.D1s,by.x='Esdu',by.y='Esdu')
		dim(biom.D1s)
		head(biom.D1s)

		#Species selection
		lsp2=names(biom.D1s)[c(5:dim(biom.D1s)[2])]
		if (sp.sel){
			biom.D1s=biom.D1s[,c(rep(TRUE,4),
				!substr(lsp2,1,4)%in%'SCOM')]
			lsp2=lsp2[!substr(lsp2,1,4)%in%'SCOM']
		}

		#Color definition
		cols=data.frame(sp=lsp2,sp2=substr(lsp2,1,8))
		spcol=merge(cols,spcol.db,by.x='sp2',by.y='sp')
		spcol=spcol[match(lsp2,spcol$sp),]
		pcols=spcol$lcol

		if (plotit&save.plot&!is.null(path.res.charef)){

			if (sp.sel){
				nfile=paste(devi,'_biomassMap-woMACK~expert.png',sep='')
			}else{	
				nfile=paste(devi,'_biomassMap~expert.png',sep='')
			}
			png(file=paste(path.res.charef,nfile,sep=''),width=600,height=600)
			if (logit){
				z=log(as.matrix(biom.D1s[,lsp2])+1)
				mtitle=paste(unique(biom.D1$CAMPAGNE),devi,
					'log(biomass+1) per species')
			}else{
				z=as.matrix(biom.D1s[,lsp2])
				mtitle=paste(unique(biom.D1$CAMPAGNE),devi,
					'biomass per species')
			}
			if (plotit){
				pie.xy(x=biom.D1s$LONG,y=biom.D1s$LAT,
					z=z,pcol=pcols,mtitle=mtitle,smax=smax,
					bar=FALSE,pradius=pradius,ux11=ux11)
			legend(legpos,legend=lsp2,fill=pcols,bg='white')
			dev.off()
			}
		}
		if ((plotit&k>1)&ux11){x11()}
			if (logit){
				z=log(as.matrix(biom.D1s[,lsp2])+1)
				mtitle=paste(unique(biom.D1$CAMPAGNE),devi,
				'log(biomass+1) per species')
			}else{
				z=as.matrix(biom.D1s[,lsp2])
				mtitle=paste(unique(biom.D1$CAMPAGNE),devi,
					'biomass per species')
			}
			if (plotit){
				pie.xy(x=biom.D1s$LONG,y=biom.D1s$LAT,
					z=z,pcol=pcols,mtitle=mtitle,smax=smax,
					bar=FALSE,pradius=pradius,ux11=ux11)

				legend(legpos,legend=lsp2,fill=pcols,bg='white')
			}
		Btot.D1l
	}

	#**********************************************************  
# Biomass and abundance per ESDU, species and size class ----
	#**********************************************************  

	B.esdu.size.sp.plot=function(k,list.sp.NW.size,lspcodes.mens,EsduDev,
		save.plot=FALSE,path.res.charef=NULL,clims=vector("list", 10),
		logit=FALSE,v2plot='W',smax=1,barplotit=TRUE,spname='CodEsp',
    plotit=list(Lclass1=TRUE,LclassALL=TRUE),ux11=FALSE,cruise='',
		spcols=data.frame(CodEsp=c("MICR-POU-0","ENGR-ENC-0","SARD-PIL-0",
		                           "SCOM-SCO-0","SPRA-SPR-0","TRAC-TRU-0","CAPR-APE-0",
		                           "TRAC-MED-0","SCOM-COL-0"),
		                  GENR_ESP=c("MICR-POU","ENGR-ENC","SARD-PIL",
		                             "SCOM-SCO","SPRA-SPR","TRAC-TRU","CAPR-APE",
		                             "TRAC-MED","SCOM-COL"),
		                  spcol=c(8,3,4,2,1,7,5,6,9))){

		spi=lspcodes.mens[k]
    cat(spi,'\n')
    spcol=spcols[spcols[,spname]==spi,'spcol']
    
		#Extract biomass per species from R outputs	
		
		biom.D1.1z=data.frame(list.sp.NW.size[[k]])
    head(biom.D1.1z)
		#selection of species with non null biomass
		Btot.D1=apply(biom.D1.1z[,-seq(3)],2,sum)
		Btot.D1s=Btot.D1[Btot.D1>0]
		if (sum(grepl(v2plot,names(Btot.D1s)))>0){
		  biom.D1s=data.frame(biom.D1.1z[,c(TRUE,TRUE,FALSE,Btot.D1>0)])
		  head(biom.D1s)
		  dim(biom.D1s)
		  biom.D1s=merge(EsduDev[,c('TC','esdu.id','LAT','LONG')],
		                 biom.D1s,by.y='Esdu',by.x='esdu.id')
		  dim(biom.D1s)
		  head(biom.D1s)
		  
		  colt=substr(names(biom.D1s)[-seq(5)],1,1)
		  biom.D1s2=biom.D1s[,c(rep(TRUE,5),colt==v2plot)]
		  #head(biom.D1s2)
		  colt2=substr(names(biom.D1s2),1,1)
		  Ls=as.numeric(substr((names(biom.D1s2)[colt2==v2plot]),3,6))
		  #Define and aggregate abundances per size class based on clim
		  clim=clims[[k]]
		  if (is.null(clim)){
		    clim=seq(0,100,10)
		  }  
		  cLs=cut(Ls,clim)
		  length(cLs)
		  bsel=biom.D1s2[,colt2==v2plot]
		  dim(bsel)
		  twl=t(bsel)
		  dim(twl)
		  biom.D1s2a=aggregate(twl,by=list(cLs),FUN=sum)
		  #biom.D1s2a[1,3]
		  dim(biom.D1s2a)
		  #head(biom.D1s2a)
		  z=t(biom.D1s2a[,-1])
		  cLsi=biom.D1s2a[,1]
		  dimnames(z)[[2]]=cLsi
		  tbiom.D1s2a=data.frame(z)
		  tbiom.D1s2a$x=biom.D1s$LONG
		  tbiom.D1s2a$y=biom.D1s$LAT
		  #head(tbiom.D1s2a)
		  
		  if (length(cLsi)==1){
		    pcol=c('yellow1')  
		  }else if (length(cLsi)==2){
		    pcol=c('yellow1','red')
		  }else if (length(cLsi)==3){
		    pcol=c('yellow1','orange3','red')
		  }else if (length(cLsi)==4){
		    pcol=c('yellow1','orange2','indianred','red')
		  }
		  
		  if (save.plot&!is.null(path.res.charef)){
		    if (logit){
		      zp=log(z+1)
		      mtitle=paste(spi,', log(biomass+1) per size class',
		                   sep='')
		    }else{
		      zp=z
		      mtitle=paste(spi,', biomass per size class',sep='')
		    }
		    if (plotit$LclassALL){
		      nfile=paste(cruise,'_',spi,'_biomassMapLength.png',sep='')
		      png(file=paste(path.res.charef,nfile,sep=''),width=600,
		          height=600)
		      pie.xy(x=biom.D1s$LONG,y=biom.D1s$LAT,
		             z=zp,pcol=pcol,mtitle=mtitle,smax=smax,
		             bar=barplotit)
		      legend('bottomleft',legend=cLsi,fill=pcol,bg='white',
		             title='Length class (cm)')
		      dev.off()
		    }  
		    if (plotit$Lclass1){
		      
		      # 			  if (length(cLsi)>1){
		      # 			    par(mfrow=c(ceiling(length(cLsi)/2),ceiling(length(cLsi)/2)))
		      # 			  }  
		      for (i in 1:length(cLsi)){
		        if (ux11) x11()
		        par(bg='white')
		        plot(tbiom.D1s2a$x,tbiom.D1s2a$y, asp=1/cos(46*pi/180),
		             cex=0.001,xlab='',ylab='',
		             main=paste(spi,cLsi[i],'cm, biomass per EDSU (tons)'),pch=16,
		             col='grey70')
		        points(tbiom.D1s2a$x,tbiom.D1s2a$y, asp=1/cos(46*pi/180),
		               cex=log(tbiom.D1s2a[,i]+1)/10,xlab='',ylab='',
		               main=paste(spi,cLsi[i],'cm, biomass per EDSU (tons)'),pch=16,
		               col=spcol)
		        coast()
		        nfile=paste(cruise,'_',spi,'_biomassMapLengthClass',i,'.png',sep='')
		        dev.print(device=png,filename=paste(path.res.charef,nfile,sep=''),width=800,height=800)
		        
		      }
		    }		
		  }else{
		    if (logit){
		      zp=log(z+1)
		      mtitle=paste(spi,', log(biomass+1) per size class',
		                   sep='')
		    }else{
		      zp=z
		      mtitle=paste(spi,', biomass per size class',sep='')
		    }
		    if (plotit$LclassALL){
		      if (ux11) x11()
		      pie.xy(x=biom.D1s$LONG,y=biom.D1s$LAT,
		             z=zp,pcol=pcol,mtitle=mtitle,smax=smax,
		             bar=barplotit)
		      legend('bottomleft',legend=cLsi,fill=pcol,bg='white',
		             title='Length class (cm)')
		    }  
		    if (plotit$Lclass1){
		      #par(mfrow=c(ceiling(length(cLsi)/2),ceiling(length(cLsi)/2)))
		      #if (length(cLsi)>1){
		      #  par(mfrow=c(2,Ncol))
		      #}  
		      for (i in 1:length(cLsi)){
		        if (ux11) x11()
		        plot(tbiom.D1s2a$x,tbiom.D1s2a$y, asp=1/cos(46*pi/180),
		             cex=0.001,xlab='',ylab='',
		             main=paste(spi,cLsi[i],'cm, biomass per EDSU (tons)'),pch=16,
		             col='grey70')
		        points(tbiom.D1s2a$x,tbiom.D1s2a$y, asp=1/cos(46*pi/180),
		               cex=log(tbiom.D1s2a[,i]+1)/10,xlab='',ylab='',
		               main=paste(spi,cLsi[i],'cm, biomass per EDSU (tons)'),pch=16,
		               col=spcol)
		        coast()
		      }
		    }
		  }
		}else{
		  cat('NA found in numbers or weights-at-length, species skipped','\n')
		}
  }

	#**********************************************************  
	# Biomass and abundance per ESDU, species and age -------
	#**********************************************************  
  
	B.esdu.age.ANSA.plot=function(path.res.charef=NULL,Biomres.AN.age,
	                             Biomres.SA.age){
    if (!is.null(path.res.charef)){
 
      #par(mfrow=c(2,2))
      aAN=Biomres.AN.age[,!names(Biomres.AN.age)%in%c('esdu.id',
                                                      'TC','LONG','LAT','sp','Ntot')]
      names(aAN)
      Na=dim(aAN)[2]                   
      for (i in 1:Na){
        #x11()
        nfile=paste('ENGR-ENC_biomassMapAge',i,'.png',sep='')
        png(file=paste(path.res.charef,nfile,sep=''),width=600,
            height=600)
        plot(Biomres.AN.age$LONG,Biomres.AN.age$LAT, asp=1/cos(46*pi/180),
             cex=0.001,xlab='',ylab='',
             main=paste('ENGR-ENC',names(aAN)[i],
                        'abundance per EDSU (No. of fish)'),pch=16,col='grey70')
        points(Biomres.AN.age$LONG,Biomres.AN.age$LAT, asp=1/cos(46*pi/180),
               cex=log(aAN[,i]+1)/10,xlab='',ylab='',
               ,pch=16,col=3)
        coast()
        dev.off()
      }
      Na=dim(Biomres.SA.age[,!names(Biomres.SA.age)%in%c('esdu.id',
                                                         'TC','LONG','LAT','sp','Ntot')])[2]     
      for (i in 1:Na){
        nfile=paste('SARD-PIL_biomassMapAge',i,'.png',sep='')
        png(file=paste(path.res.charef,nfile,sep=''),width=600,
            height=600)
#         if (i%in%c(1,5,9,13,17)){
#           x11()
#           par(mfrow=c(2,2))
#         }  
        plot(Biomres.SA.age$LONG,Biomres.SA.age$LAT, asp=1/cos(46*pi/180),
             cex=0.001,xlab='',ylab='',
             main=paste('SARD-PIL',substr(names(Biomres.SA.age)[5+i],3,7),
                        'abundance per EDSU (No. of fish)'),pch=16,col='grey70')
        points(Biomres.SA.age$LONG,Biomres.SA.age$LAT, asp=1/cos(46*pi/180),
               cex=log(Biomres.SA.age[,5+i]+1)/10,xlab='',ylab='',
               ,pch=16,col=4)
        coast()
        dev.off()
      }
    }  
	  #Anchovy
	  #**********************************************************
    aAN=Biomres.AN.age[,!names(Biomres.AN.age)%in%c('esdu.id',
                                                'TC','LONG','LAT','sp','Ntot')]
    names(aAN)
    Na=dim(aAN)[2]                   
	  for (i in 1:Na){
	    if (i%in%c(1,5,9,13,17)){
	      x11()
	      par(mfrow=c(2,2))
	    } 
	    plot(Biomres.AN.age$LONG,Biomres.AN.age$LAT, asp=1/cos(46*pi/180),
	         cex=0.001,xlab='',ylab='',
	         main=paste('ENGR-ENC',names(aAN)[i],
	                    'abundance per EDSU (No. of fish)'),pch=16,col='grey70')
	    points(Biomres.AN.age$LONG,Biomres.AN.age$LAT, asp=1/cos(46*pi/180),
	           cex=log(aAN[,i]+1)/10,xlab='',ylab='',
	           ,pch=16,col=3)
	    coast()
	  }
	
	  #Sardine
    #**********************************************************
    Na=dim(Biomres.SA.age[,!names(Biomres.SA.age)%in%c('esdu.id',
      'TC','LONG','LAT','sp','Ntot')])[2]	
	  for (i in 1:Na){
	    if (i%in%c(1,5,9,13,17)){
	      x11()
	      par(mfrow=c(2,2))
	    }  
	    plot(Biomres.SA.age$LONG,Biomres.SA.age$LAT, asp=1/cos(46*pi/180),
	       cex=0.001,xlab='',ylab='',
	       main=paste('SARD-PIL',substr(names(Biomres.SA.age)[5+i],3,7),
	                  'abundance per EDSU (No. of fish)'),pch=16,col='grey70')
	    points(Biomres.SA.age$LONG,Biomres.SA.age$LAT, asp=1/cos(46*pi/180),
	         cex=log(Biomres.SA.age[,5+i]+1)/10,xlab='',ylab='',
	         ,pch=16,col=4)
	    coast()
	   }
   }    
  
  
	#**********************************************************  
	# Biomass and abundance per ESDU, species -------
	#**********************************************************
  
 sp.plot=function(B.sp.esdu.df1,sp='ENGR-ENC',logit=TRUE,polygoni=NULL,plotit=TRUE,
  projectit=TRUE,spcol=3,bathy=TRUE,ylat=NULL,cruise,display=FALSE,spname='GENR_ESP',fwidth=12,fheight=12,res=300,
                  filename="Rplot%03d.png",zmult=0.01,lpos="bottomleft",pt.cex=0.1){
  #Select species
   #**********************************************************
  B.1sp.esdu.df=B.sp.esdu.df1[B.sp.esdu.df1[,spname]==sp,]

  if (!is.null(polygoni)){
    library(splancs)
    if (projectit){
      if (is.null(ylat)) ylat=mean(B.1sp.esdu.df$LAT)
      mcos=cos(ylat*pi/180)
      B.1sp.esdu.df$xnm=B.1sp.esdu.df$LONG*mcos*60
      B.1sp.esdu.df$ynm=B.1sp.esdu.df$LAT*60
      B.1sp.esdu.df.sp=as.points(list(x=B.1sp.esdu.df$xnm,
                                      y=B.1sp.esdu.df$ynm))
      polygonil=as.matrix(polygoni[,c('xcornm','ynm')])
      #Esdus in polygon
      B.1sp.esdu.df.in=B.1sp.esdu.df[
        inout(B.1sp.esdu.df.sp,polygonil),]
      #check
#       plot(B.1sp.esdu.df$xnm,B.1sp.esdu.df$ynm)
#       lines(polygoni$xcornm,polygoni$ynm)
#       plot(B.1sp.esdu.df.sp)
#       plot(B.1sp.esdu.df.in$xnm,B.1sp.esdu.df.in$ynm)
#       lines(polygoni$xcornm,polygoni$ynm)
    }else{
      B.1sp.esdu.df.in=B.1sp.esdu.df[
        inout(B.1sp.esdu.df[,c('LONG','LAT')],polygoni[,c('LONG','LAT')]),]
    }
  }else{
     B.1sp.esdu.df.in=B.1sp.esdu.df
  }
  B.1sp.esdu.dfa=aggregate(B.1sp.esdu.df$BB,list(B.1sp.esdu.df$DEV),sum)
  names(B.1sp.esdu.dfa)=c('DEV','Btot')
  B.1sp.esdu.dfa=B.1sp.esdu.dfa[order(B.1sp.esdu.dfa$DEV),]
  ldev=unique(B.1sp.esdu.df$DEV)
  ldevs=ldev[B.1sp.esdu.dfa$Btot>0]
  if(!logit){
    for (i in 1:length(ldevs)){
      B.1sp.esdu.dfi=B.1sp.esdu.df[B.1sp.esdu.df$DEV==ldevs[i],]
      B.1sp.esdu.df.ini=B.1sp.esdu.df.in[B.1sp.esdu.df.in$DEV==ldevs[i],]
      if (plotit){
        if (display=='x11') x11()
        else if (display=='png'){
          png(filename=paste(filename,i,'.png',sep=''),width=fwidth,height=fheight, units="in",res=res)
        } 
        par(bg='white')
        if (bathy){
          BiscayBathyPlot(icol='grey70',xlab='',ylab='',
                          main=paste(cruise,sp,ldevs[i],'biomass per EDSU (tons)'))
        }else{
          plot(B.1sp.esdu.dfi$LONG,B.1sp.esdu.dfi$LAT, asp=1/cos(46*pi/180),
               cex=pt.cex+B.1sp.esdu.dfi$BB*zmult,xlab='',ylab='',col='grey50',
               main=paste(cruise,sp,ldevs[i],'biomass per EDSU (tons)'))
          coast()
        }
        points(B.1sp.esdu.dfi$LONG,B.1sp.esdu.dfi$LAT, 
               cex=pt.cex+B.1sp.esdu.dfi$BB*zmult,pch=16,col='grey50')
        points(B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB==0,'LONG'],
               B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB==0,'LAT'],
               cex=pt.cex,col=2,pch=1)    
        
        if (!is.null(polygoni)){
          lines(polygoni$LONF,polygoni$LATF,col=2)
        }  
        points(B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB>0,'LONG'],
               B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB>0,'LAT'],
               cex=pt.cex+B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB>0,'BB']*zmult,col=spcol,
               pch=16)
        legend.bubbleplot(z=B.1sp.esdu.df.ini$BB,zmult=zmult,pch=16,col=spcol,
                          lpos=lpos,bg='white',bty='o',pt.cex=pt.cex)
        if (display=='png') dev.off()
      }   
    }    
  }else{
     for (i in 1:length(ldevs)){
      B.1sp.esdu.dfi=B.1sp.esdu.df[B.1sp.esdu.df$DEV==ldevs[i],]
      B.1sp.esdu.df.ini=B.1sp.esdu.df.in[B.1sp.esdu.df.in$DEV==ldevs[i],]
      if (plotit){
        if (display=='x11') x11()
        else if (display=='png'){
          png(filename=paste(filename,i,'.png',sep=''),width=fwidth,height=fheight, units="in",res=res)
        }
        par(bg='white')
        if (bathy){
          BiscayBathyPlot(icol='grey70',xlab='',ylab='',
                          main=paste(cruise,sp,ldevs[i],'log(biomass+1) per EDSU'))
        }else{
          plot(B.1sp.esdu.dfi$LONG,B.1sp.esdu.dfi$LAT, asp=1/cos(46*pi/180),
               cex=pt.cex+log(B.1sp.esdu.dfi$BB+1)*zmult,xlab='',ylab='',col='grey50',
               main=paste(cruise,sp,ldevs[i],'log(biomass+1) per EDSU (tons)'))
          coast()
        }
        points(B.1sp.esdu.dfi$LONG,B.1sp.esdu.dfi$LAT, 
               cex=pt.cex+log(B.1sp.esdu.dfi$BB+1)*zmult,col='grey50')
        points(B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB==0,'LONG'],
               B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB==0,'LAT'], asp=1/cos(46*pi/180),
               cex=pt.cex,col=2,pch=16)    
        if (!is.null(polygoni)){
          lines(polygoni$LONF,polygoni$LATF,col=2)
        }
        points(B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB>0,'LONG'],
               B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB>0,'LAT'], asp=1/cos(46*pi/180),
               cex=pt.cex+log(B.1sp.esdu.df.ini[B.1sp.esdu.df.ini$BB>0,'BB']+1)*zmult,
               col=spcol,pch=16)
        legend.bubbleplot(z=log(B.1sp.esdu.df.ini$BB+1),zmult=zmult,pch=16,col=spcol,
                          lpos=lpos,bg='white',bty='o',pt.cex=pt.cex)
        if (display=='png') dev.off()
      }  
     }  
    }
  list(B.1sp.esdu.df=B.1sp.esdu.df,B.1sp.esdu.df.in=B.1sp.esdu.df.in)
}

simple.1sp.dev.esdu.plot=function(B.dev.sp.esdu.df){
  lsp=unique(B.dev.sp.esdu.df$GENR_ESP)
  for (i in 1:length(lsp)){
    BdevSP=B.dev.sp.esdu.df[B.dev.sp.esdu.df$GENR_ESP==lsp[i],]
    ldev=unique(BdevSP$DEV)
    for (k in 1:length(ldev)){
      BdevSPk=BdevSP[BdevSP$DEV==ldev[k],]
      BdevSPk0=BdevSPk[BdevSPk$BB==0,]
      plot(BdevSPk$LONG,BdevSPk$LAT,cex=0.1+BdevSPk$BN/5e6,
           main=paste(lsp[i],'Echotype',ldev[k]),xlab='',ylab='')
      points(BdevSPk0$LONG,BdevSPk0$LAT,cex=1,col=2,pch=4)
      legend('bottomright',legend=c('Non-null abundance','Null abundance'),pch=c(1,4),
             col=seq(2))
      coast()
    }
  }  
}

calculate.globalMeanWeightAtAge=function(Biomres.sp.size2,LA,LW,
                                         LALunit='mm'){
  
  # Mean length and weight-at-age based on at-length per ESU data -----
  # **********************
  # Sum numbers and weights at length per ESU in each species and length class 
  head(Biomres.sp.size2)
  Biomres.sp.size2a=aggregate(Biomres.sp.size2[,c('N','W')],
                              list(sp=Biomres.sp.size2$sp,
                                   L=Biomres.sp.size2$L),
                              sum)
  Biomres.sp.size2a$pN=Biomres.sp.size2a$N/sum(Biomres.sp.size2a$N)
  Biomres.sp.size2a$mw=1000*Biomres.sp.size2a$W/Biomres.sp.size2a$N
  # Numbers-at-age variance
  Biomres.sp.size2avar=aggregate(Biomres.sp.size2[,c('N','W')],
                              list(sp=Biomres.sp.size2$sp,
                                   L=Biomres.sp.size2$L),
                              var)
  head(Biomres.sp.size2avar)
  # Merge with length-age keys
  head(Biomres.sp.size2a)
  if (LALunit=='mm'){
    LA$TAILLE=LA$TAILLE/10
  }
  Biomres.sp.size2a=merge(Biomres.sp.size2a,LA,by.x=c('sp','L'),
                          by.y=c('GENRE_ESP','TAILLE'))
  # Calculate numbers at ages in each length class, from number at length and age length key, p
  Biomres.sp.size2a$Nage=Biomres.sp.size2a$N*Biomres.sp.size2a$POURCENTAGE
  # Calculate total numbers at age, by summing numbers at ages by length class 
  Biomres.sp.size2aAtot=aggregate(Biomres.sp.size2a$Nage,
                                  list(sp=Biomres.sp.size2a$sp,age=Biomres.sp.size2a$AGE),sum)
  names(Biomres.sp.size2aAtot)[3]="NtotAge"
  aggregate(Biomres.sp.size2aAtot$NtotAge,list(Biomres.sp.size2aAtot$sp),sum)
  aggregate(Biomres.sp.size2a$Nage,list(Biomres.sp.size2a$sp),sum)
  # Calculate mean lengths and weights at age, weighted by numbers at age  
  Biomres.sp.size2a=merge(Biomres.sp.size2a,Biomres.sp.size2aAtot,by.x=c('sp','AGE'),
                          by.y=c('sp','age'))
  head(Biomres.sp.size2a)
  Biomres.sp.size2a$Li=Biomres.sp.size2a$Nage*Biomres.sp.size2a$L/Biomres.sp.size2a$NtotAge
  Biomres.sp.size2a$Wi=Biomres.sp.size2a$Nage*Biomres.sp.size2a$mw/Biomres.sp.size2a$NtotAge
  Biomres.sp.size2a$w=Biomres.sp.size2a$Nage/Biomres.sp.size2a$NtotAge
  Biomres.sp.size2a$w2=Biomres.sp.size2a$w^2
  
  w.check=aggregate(Biomres.sp.size2a$w,
                     list(sp=Biomres.sp.size2a$sp,age=Biomres.sp.size2a$AGE),sum)
  
  Biomres.sp.size2a2=aggregate(Biomres.sp.size2a[,c('Li','Wi','w','w2')],
                               list(sp=Biomres.sp.size2a$sp,age=Biomres.sp.size2a$AGE),
                               sum,na.rm=TRUE)
  names(Biomres.sp.size2a2)[3:6]=c('wmL.size.esdu','wmW.size.esdu','sum.w','sum.w2')
  head(Biomres.sp.size2a2)
  # Calculate length-at-age and weight-at-age variances, weighted by numbers at age
  # weighted variance formula:
  # sum.w <- sum(w)
  # sum.w2 <- sum(w^2)
  # mean.w <- sum(x * w) / sum(w)
  # (sum.w / (sum.w^2 - sum.w2)) * sum(w * (x - mean.w)^2, na.rm =
  #                                      na.rm)
  Biomres.sp.size2a=merge(Biomres.sp.size2a,Biomres.sp.size2a2,by.x=c('sp','AGE'),
                          by.y=c('sp','age'))
  #Biomres.sp.size2a$wvari1=Biomres.sp.size2a$sum.w/
  #                               (Biomres.sp.size2a$sum.w^2-Biomres.sp.size2a$sum.w2)
  Biomres.sp.size2a$wvari2L=Biomres.sp.size2a$w*(Biomres.sp.size2a$L-Biomres.sp.size2a$wmL.size.esdu)^2
  Biomres.sp.size2a$wvari2W=Biomres.sp.size2a$w*(Biomres.sp.size2a$mw-Biomres.sp.size2a$wmW.size.esdu)^2
  Biomres.sp.size2a3=aggregate(Biomres.sp.size2a[,c('Li','Wi','w','w2','wvari2L','wvari2W')],
                               list(sp=Biomres.sp.size2a$sp,age=Biomres.sp.size2a$AGE),
                               sum,na.rm=TRUE)
  names(Biomres.sp.size2a3)[3:8]=c('wmL.size.esdu','wmW.size.esdu','sum.w',
                                   'sum.w2','sum2L','sum2W')
  Biomres.sp.size2a3$wvarL.size.esdu=(Biomres.sp.size2a3$sum.w/
    (Biomres.sp.size2a3$sum.w^2-Biomres.sp.size2a3$sum.w2))*Biomres.sp.size2a3$sum2L
  Biomres.sp.size2a3$wvarW.size.esdu=(Biomres.sp.size2a3$sum.w/
                                        (Biomres.sp.size2a3$sum.w^2-
                                           Biomres.sp.size2a3$sum.w2))*Biomres.sp.size2a3$sum2W
  # OR:
  # lsp=unique(Biomres.sp.size2a$sp)
  # #library(Weighted.Desc.Stat)
  # for (i in 1:length(lsp)){
  #   dfi=Biomres.sp.size2a[Biomres.sp.size2a$sp==lsp[i],]
  #   lagei=unique(dfi$AGE)
  #   for (j in 1:length(lagei)){
  #     dfj=dfi[dfi$AGE==lagei[j],]
  #     #wvar1=w.var(dfj$L,dfj$w)
  #     wvar2L=weighted.var(dfj$L,dfj$w)
  #     wvar2W=weighted.var(dfj$mw,dfj$w)
  #     #wvari=data.frame(sp=lsp[i],age=lagei[j],wvar1=wvar1,wvar2=wvar2)
  #     wvari=data.frame(sp=lsp[i],age=lagei[j],wvar2L=wvar2L,wvar2W=wvar2W)
  #     if (i==1&j==1){
  #       wvar.df=wvari
  #     }else{
  #       wvar.df=rbind(wvar.df,wvari)
  #     }
  #   }
  # }
  # dfm=aggregate(Biomres.sp.size2a[,c('L','mw')],
  #           list(Biomres.sp.size2a$sp,Biomres.sp.size2a$AGE),mean)
  # names(dfm)[3:4]=c('mL','mW')
  # dfv=aggregate(Biomres.sp.size2a[,c('L','mw')],
  #           list(Biomres.sp.size2a$sp,Biomres.sp.size2a$AGE),var)
  # names(dfv)[3:4]=c('vL','vW')
  # dfmv=merge(dfm,dfv)
  # dfmv$LCV=sqrt(dfmv$vL)/dfmv$mL
  # dfmv$WCV=sqrt(dfmv$vW)/dfmv$mW
  
  Biomres.sp.size2a2=merge(Biomres.sp.size2a2,
                           Biomres.sp.size2a3[,
                                              c('sp','age','wvarL.size.esdu','wvarW.size.esdu')],
                           by.x=c('sp','age'),
                           by.y=c('sp','age'))
  Biomres.sp.size2a3=Biomres.sp.size2a3[order(Biomres.sp.size2a3$sp),]
  head(Biomres.sp.size2a3)
  Biomres.sp.size2a3$wL.CV=sqrt(Biomres.sp.size2a3$wvarL.size.esdu)/
    Biomres.sp.size2a3$wmL.size.esdu
  Biomres.sp.size2a3$wW.CV=sqrt(Biomres.sp.size2a3$wvarW.size.esdu)/
    Biomres.sp.size2a3$wmW.size.esdu
  Biomres.sp.size2a3=merge(Biomres.sp.size2a3,LW[,
                                                 c('CAMPAGNE','CATEG','a',
                                                   'b','GENR_ESP')],
                           by.x=c('sp'),by.y=c('GENR_ESP'))
  
  list(Biomres.sp.size2a=Biomres.sp.size2a,Biomres.sp.size2aAtot=Biomres.sp.size2aAtot,
       Biomres.sp.size2a2=Biomres.sp.size2a3)
}

