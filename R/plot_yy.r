plot.yy<-function(x,yright,yleft, yleftlim=NULL, yrightlim =
	NULL, xlab = '' ,yylab=c("",""),lpch=c(1,2),lcol=c(1,2),
	linky=FALSE,xlabels=NULL, smooth=0, lwds=1, length=10, 
	format="%d-%H:%M",
	mar=c(5,4,2,2),
	oma=c(0,0,0,3),line.y1=1,line.y2=2.5,pts=NULL,cex.axis=1,las=1,cex=1,...)

	{

	par(mar=mar,oma=oma)
	plot(x, yright, ylim=yrightlim, axes=FALSE,ylab="", xlab=xlab,
		pch=lpch[1],col=lcol[1],cex.axis=cex.axis,...)

	#right axis
	#-------------
	axis(4,pretty(range(yright,na.rm=TRUE),10),col=lcol[1],
		cex.axis=cex.axis,las=las)

	if (linky) lines(x,yright,col=lcol[1])

	if (smooth!=0) lines(supsmu(x,yright,span=smooth),col=lcol[1],
		lwd=lwds, ...)

		#labels
		#-------------

	if(yylab[1]==""){
	mtext(deparse(substitute(yright)),side=4,outer=TRUE,line=line.y1,
	col=lcol[1], cex=cex)
		}else{
	mtext(yylab[1],side=4,outer=TRUE,line=line.y1, col=lcol[1])}

	par(new=TRUE)
	plot(x,yleft, ylim=yleftlim, ylab="", axes=FALSE ,xlab=xlab,
		pch=lpch[2],col=lcol[2])
	if (linky) lines(x,yleft,col=lcol[2])
	box()

	#left axis
	#-------------
	if (is.null(yleftlim)){
		axis(2,pretty(range(yleft,na.rm=TRUE),10),col=lcol[2], 
			col.axis=lcol[2],cex.axis=cex.axis,las=las)
	}else{
		axis(2,pretty(yleftlim,10),col=lcol[2], 
			col.axis=lcol[2],cex.axis=cex.axis,las=las)
	}		

	if (!is.null(pts)){
	points(pts[[1]],pts[[2]],pch=pts[[3]],cex=pts[[4]])}	

		#labels
		#-------------

	if(yylab[2]=="")
		mtext(deparse(substitute(yleft)),side=2,line=line.y2, col=lcol[2], ...)
	else
		mtext(yylab[2],side=2,line=line.y2, col=lcol[2], ...)

	#x axis labs
	#----------------
	if (!is.null(xlabels)) {
		axis(1,pretty(range(x,na.rm=TRUE),length(xlabels)),labels=xlabels,
			cex.axis=cex.axis)
	}else{
		axis(1,pretty(range(x,na.rm=TRUE),10),
			cex.axis=cex.axis)
	}



	if (linky) lines(x,yleft,col=lcol[2], lty=2, ...)
	if (smooth!=0) lines(supsmu(x,yleft,span=smooth),col=lcol[2],
		lty=2, lwd=lwds, ...)

} 
