"maf" <-
function(formulas,data)
{
#===============================================================================
# Min/Max Autocorrelation Factors (MAF) in time (or 1D)
#
# EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez, J. Rivoirard, Mines-ParisTech
# Last update : 01 march 2008
#
# Description:
# The MAF are a multivariate statistical method, that allows the set of
# initial variables to be decomposed into factors. The autocorrelation of
# which decreases, or the variogram increases, when going from the first
# factors to the last ones. Hence the very first factors extract the part of
# the variables which is the most continuous in time.
#
# Details:
# The present routine makes twice use of PCA. Here 'prcomp' is used for PCA
# instead of 'princomp', as it allows less samples (e.g. years) than variables.
# After original variables are turned into normalized PC, MAF are obtained as
# the PC of their increments; their order is then reversed to begin with the
# smallest variograms (i.e. the half variances of increments).
#
# Note:
# The number of MAF cannot exceed the number of variables, nor the number of
# year increments (number of sampled years - 1). If the number of variables
# tends to be larger than the number of sampled years, the MAF no.i (i=1, ...)
# tends to have period (nb years - 1)*2/i. In particular there will be
# evidence of a high continuity with period (nb years -1)*2 for MAF1,
# (nb years -1) for MAF2, etc, whatever the data, which may not be significant
# out of this series e.g. for additional years. The number of variables should
# then be better reduced well below the number of sampled years.
#
# Inputs:        
#   formula   A formula with no response variable, referring only 
#             to numeric variables.
#
#   data      A data frame containing the variables in the formula.
#
#===============================================================================

    if(length(data[,1])==1) stop("Cannot be run with only one sample.")

# PCA of original variables
    bidpr<-prcomp(formulas,data,scale=TRUE)
    bidn<-sum(bidpr$sdev>1e-10)
    if(bidn<2) stop("stop: strictly less than two PCs")
    bidpr$sdev<-bidpr$sdev[1:bidn]
    bidpr$rotation<-bidpr$rotation[,1:bidn]
    bidpr$x<-bidpr$x[,1:bidn]

# normalization of PCs
    bidim<-dim(bidpr$x)
    bidprsdev<-matrix(rep(bidpr$sdev,each=bidim[1]),ncol=bidim[2])
    bidprx<-bidpr$x/bidprsdev
    bidim<-dim(bidpr$rotation)
    bidprsdev<-matrix(rep(bidpr$sdev,each=bidim[1]),ncol=bidim[2])
    bidpr$rotation<-bidpr$rotation/bidprsdev

# PCA of increments of normalized PCs
    biddtab<- diff(bidprx)
    biddpr<-prcomp(biddtab,center=F,scale=F)
    biddnmaf<-sum(biddpr$sdev>1e-10)
    if(biddnmaf<2) stop("stop: strictly less than two MAFs")
    biddpr$sdev<-biddpr$sdev[1:biddnmaf]
    biddpr$rotation<-biddpr$rotation[,1:biddnmaf]
    biddpr$x<-biddpr$x[,1:biddnmaf]
    biddx<-biddpr$x

# store complete transformation as a PCA output from original data
    maf<-bidpr
    maf$sdev <- biddpr$sdev
    biddim<-dim(biddpr$x)
    maf$rotation<-bidpr$rotation%*%biddpr$rotation
    maf$x<-bidprx%*%biddpr$rotation

# reverse the order of MAFs
# in order to begin with lowest eigenvalues i.e. lowest variograms

    maf$sdev<-.5*(maf$sdev^2)*(biddim[1]-1)/biddim[1]
    names(maf$sdev)<-rev(names(maf$sdev))
    maf$sdev<-rev(maf$sdev)
    lng<-length(dimnames(maf$rotation)[[2]])
    maf$rotation <- maf$rotation[,seq(lng,1)]
    dimnames(maf$rotation)[[2]]<-paste("MAF",1:lng,sep="")
    lng<-length(dimnames(maf$x)[[2]])
    maf$x<-maf$x[,seq(lng,1)]
    dimnames(maf$x)[[2]]<-paste("MAF",1:lng,sep="")
    maf
}

"vg1d.reg" <-
function(x)
{
#===============================================================================
# Regular 1d variogram
#
# EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez, J. Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
#===============================================================================

lng<-length(x)
res<-numeric(0)
    for(h in 1:(lng-1))
    res<-c(res,0.5*mean(diff(x,lag=h)^2,na.rm=T))
res<-cbind(1:(lng-1),res)
res
}

"vg1d.irreg" <-
function(t,x)
{
#===============================================================================
# Irregular 1d variogram
#
# EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : P.Petitgas (Ifremer), M.Woillez, J. Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
#===============================================================================

d<-abs(outer(t,t,"-"))
h<-sort(unique(d[lower.tri(d)]))
res<-numeric(0)
for (k in 1:length(h)) {
    d2x<-outer(x,x,"-")^2
    res<-c(res,0.5*mean(d2x[d==h[k]],na.rm=T)) }
res<-cbind(h,res)
res
}

"ploc" <-
function(x,y=NA,plot=T,add=F,...)
{
#===============================================================================
# Bivariate plot and linear regression
#
# EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez, J. Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
#===============================================================================

    if(length(y) == 1 && is.na(y)){
        y<-x
        x<-seq(1,length(y),1)
    }
    lab<-paste("R2= Null   F value= Null   Pr(>F)=Null",sep="")
    if(sum(x*y,na.rm=T)!=0){
        reg<-lm(y~x)
        bid<-summary(reg)
        bidaov<-summary.aov(reg)
        if(bid$r.squared!=0 & !is.na(bid$r.squared))
            lab<-paste("R?=",round(bid$r.squared,digit=3),"   F value=",
                round(bidaov[[1]]$"F value"[1],3),"   Pr(>F)=",
                round(bidaov[[1]]$"Pr(>F)"[1],3),sep="")
        }
    if(plot==T){
    par("cex.main"=1)
    par("mar"=c(4.5,4.5,2,1))
    if(add==T)
        points(x,y,main=lab,...)
    else
        plot(x,y,main=lab,...)
    if(sum(x*y,na.rm=T)!=0 & bid$r.squared!=0 & !is.na(bid$r.squared))
        abline(bid$coefficients[,1],col=2)
    }
    reg
}

"crosscor" <-
function (x, y, type="1", lag.max=NA,plotit=FALSE,cmethod=c("pearson", "kendall", "spearman"))
{
#===============================================================================
# Cross correlation between two variables x and y
#
# EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez, J. Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
#===============================================================================

    if(length(x)!=length(y))
        stop("x and y must have the same length")
    res<-numeric(0)
    bidn<-length(x)-1
    if(bidn > lag.max & !is.na(lag.max))
        bidn<-lag.max
    for(i in -bidn:bidn){
        if(i<0){
            xx<-c(x,rep(NA,abs(i)))
            yy<-c(rep(NA,abs(i)),y)
        }
        if(i>=0){
            xx<-c(rep(NA,abs(i)),x)
            yy<-c(y,rep(NA,abs(i)))
        }
        bid<-na.omit(data.frame(xx,yy))
        xx<-bid[,1]
        yy<-bid[,2]
        res=c(res,cor(xx,yy,method=cmethod))
        # res<-c(res,(mean((xx-mean(xx,na.rm=T))*(yy-mean(yy,na.rm=T)),na.rm=T))/
        #     (sqrt((sum(xx^2)/length(xx)) - ((sum(xx)^2)/length(xx)^2))
        #     *sqrt((sum(yy^2)/length(yy)) - ((sum(yy)^2)/length(yy)^2))))
    }
    if (plotit){
      plot(seq(-bidn,bidn,1),res,type="h",xlab="Lag",ylab="Correlation",ylim=c(-1,1))
      abline(h=c(-1,0,1),lty=c(2,1,2))
    }
    res
}

"nugget" <-
function(h)
{
#===============================================================================
# Variogram form of the nugget model
#
# Routine from Geostatistics for Estimating Fish Abundance (GEFA)
# & EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez (Mines-ParisTech), N.Bez (IRD) 
#           and J.Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
#===============================================================================

	null <- (h == 0)
	g <- h
	g[null] <- 0
	g[!null] <- 1
	g
}

"spheric" <-
function(h)
{
#===============================================================================
# Variogram form of the spherical model
#	with sill 1 and range 1
#
# Routine from Geostatistics for Estimating Fish Abundance (GEFA)
# & EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez (Mines-ParisTech), N.Bez (IRD) 
#           and J.Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
#===============================================================================

	h <- abs(h)
	superieur <- (h > 1)
	g <- h
	g[superieur] <- 1
	inferieur <- (!superieur) & (!is.na(h))
	g[inferieur] <- 0.5 * h[inferieur] * (3 - h[inferieur]^2)
	g
}

"cubic" <-
function(h)
{
#===============================================================================
# Variogram form of the cubic model
#	with sill 1 and range 1
#
# Routine from Geostatistics for Estimating Fish Abundance (GEFA)
# & EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez (Mines-ParisTech), N.Bez (IRD) 
#           and J.Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
#===============================================================================

	h <- abs(h)
	superieur <- (h > 1)
	g <- h
	g[superieur] <- 1
	inferieur <- (!superieur) & (!is.na(h))
	g[inferieur] <- h[inferieur]^2 * (7 - (35/4) * h[inferieur] + 3.5 * h[
		inferieur]^3 - 0.75 * h[inferieur]^5)
	g
}

"powervg" <-
function(h, alpha)
{
#===============================================================================
# Variogram form of the power model
#
# Routine from Geostatistics for Estimating Fish Abundance (GEFA)
# & EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez (Mines-ParisTech), N.Bez (IRD) 
#           and J.Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
#===============================================================================

        h <- abs(h)
        g <- h
        sel <- !is.na(h)
        g[sel] <- h[sel]^alpha
        g
}

"okun1d" <- function(z,x,xk,model)
{
#===============================================================================
# One dimensional ordinary kriging with unique neighborhood 
#
# Routine from Geostatistics for Estimating Fish Abundance (GEFA)
# & EU program Fisboat, DG-Fish, STREP n? 502572
# Authors : M.Woillez (Mines-ParisTech), N.Bez (IRD) 
#           and J.Rivoirard (Mines-ParisTech)
# Last update : 01 march 2008
#
# Inputs:
#   z         Data values. NA values are NOT ALLOWED.
#   x         A vector of x-coordinates of data points.  
#   xk        A vector of x-coordinates of target points. 
#   model     An appropriate structural model (unevaluated expression).
#
# Outputs:
#   zkrig     A vector of the kriged values at target points.
#   vark      A vector of the kriging variance at target points.
#
#===============================================================================

    nx <- length(x)
    nxk <- length(xk)

# Built and invert left member of kriging system
    hx <- outer(x, rep(1, nx)) - outer(rep(1, nx), x)
    Kij <- eval({h<-hx; model})
    Kij <- cbind(Kij, rep(1, nx), deparse.level = 0)
    Kij <- rbind(Kij, c(rep(1, nx), rep(0, 1)), deparse.level = 0)

# Built right member of kriging system
    hx <- outer(x, rep(1, nxk)) - outer(rep(1, nx), xk)
    Kix <- eval({h<-hx; model})
    Kix <- rbind(Kix, rep(1, nxk), deparse.level = 0)

# Process the kriging system 
    solution <- solve(Kij)%*%Kix
    lambda <- solution[1:nx,]

# Evaluation on target points
    zkrig <- t(lambda) %*% z
    vark <- as.vector(t(solution * Kix) %*% rep(1,length(solution[,1])))
    res <- data.frame(zkrig,vark)
    res
}

multiSelectMAF=function(zhsel,g1h,t,minNI=NULL,maxNI=NULL,nreal=1000,
                        seed=310882,path.export=NULL,tnoise=1,
                        nbmafs=2,nc=2,contiCalc='article',name=''){
  
  if (is.null(minNI)){
    minNI=round(length(t)/2)
  }
  if (is.null(maxNI)){
    if (dim(zhsel)[2]>dim(zhsel)[1]){
      maxNI=dim(zhsel)[1]
    }else{
      maxNI=dim(zhsel)[2]
    }
  }  
  for (jInd in c(minNI:maxNI)){
    # Selection of the indices to be included in the MAF
    # Either select as many clumps as possible with a total number of indices 
    # below the number of studied years, or retain all indices with variogram 
    # at lag 1 below 1
    #length(t)
    #jInd <- 13
    #jInd <- length(g1h[g1h<1])
    Ind <- names(g1h)
    Ind <- Ind[1:jInd]
    
    # Set MAF formula 
    zInd<-zhsel[,sort(match(Ind,names(zhsel)))]
    zInd<-na.omit(data.frame(zInd,row.names=t))
    fo<-as.formula(paste("~",sapply(list(names(zInd)), paste, collapse="+"),sep=""))
    # Test on indices
    # function 'prcomp' fails if its variance is null (i.e. values are equal)
    # In that case, analysis is done without this index
    for(i in 1:jInd){ 
      if(is.numeric(zInd[,i])){
        va<-var(zInd[,i])
        if(!is.na(va)&va==0){
          zInd<-zInd[names(zInd)!=names(zInd[i])]
          fo<-as.formula(paste("~",sapply(list(names(zInd)), 
                                          paste, collapse="+"),sep=""))
        }
      }
    }
    
    # define outputs to store MAFs realisations 
    # where a normal random noise is added to indices   
    res.noise <- vector("list",jInd)
    res <- vector("list",2)
    
    for(i in 1:jInd) res.noise[[i]] <- res
    
    # set the random seed
    set.seed(seed)
    
    # loop: nreal realizations is performed               
    for(i in 1:nreal){
      
      # work on centred and normed indices
      # add of a normal random noise with mean 0 and 
      # variance 0.1*(nb.indices/(nb.years-1))
      scaled.zInd <- scale(zInd)
      nij <- dim(scaled.zInd)
      noise1=matrix(rnorm(prod(nij),0,sqrt(0.1*(nij[2]/(nij[1]-1)))),
                    nij[1],nij[2])
      if (tnoise==1){
        noise=noise1
      }else if (tnoise==2){
        noise=noise1+noise1^2
      }else if (tnoise==0){
        noise=0
      }  
      #noise <- matrix(rnorm(prod(nij),0,sqrt(0.1*(nij[2]/(nij[1]-1)))),nij[1],nij[2])
      new.zInd <- as.data.frame(scaled.zInd + noise)
      
      # Compute MAFs
      resmaf<-maf(fo,new.zInd)
      
      # MAFs are ordered according to a reference
      # the reference is the first realisation
      lng <- length(resmaf$x[1,])
      if(i==1)
        test <- resmaf
      for(j in 1:lng){
        
        # the reference is ordered so as to have 50% of the MAFs values as positive
        if(sum(test$x[,j]>=0)/length(test$x[,j])<0.5){
          test$x[,j] <- test$x[,j] * -1
          test$rotation[,j] <- test$rotation[,j] * -1
        }
        
        # the following realisations will be order so as to have 50% of the MAFs values 
        # with the same sign than the reference 
        if(sum((test$x[,j]>=0)==(resmaf$x[,j]>=0),na.rm=T)/length(test$x[,j])<0.5){
          resmaf$x[,j] <- resmaf$x[,j] * -1
          resmaf$rotation[,j] <- resmaf$rotation[,j] * -1
        }
      }
      
      # save results in outputs
      t.l <- seq(t[1],t[length(t)],1)                        
      for(j in 1:lng){
        res.noise[[j]][[1]] <- rbind(res.noise[[j]][[1]],as.numeric(resmaf$x[,j][match(t.l,t)]))
        res.noise[[j]][[2]] <- rbind(res.noise[[j]][[2]],as.numeric(resmaf$rotation[,j]))
      }
    }
    
    # Number of MAFs retained
    #nbmafs <- 2
    
    # Compute and save
    # - median loadings of the nreal previous realisations   
    # - median MAF = median loadings * centred and normed indices     
    for(i in 1:lng){
      med.rot <- apply(res.noise[[i]][[2]],2,median)
      resmaf$rotation[,i] <- med.rot
      rot <- matrix(rep(med.rot,nij[1]),nij[1],byrow=T)
      x <- apply(rot * scaled.zInd,1,sum)
      resmaf$rotation[,i] <- resmaf$rotation[,i]/sd(x)
      resmaf$x[,i] <- x/sd(x)
    }
    
    # Loop on the retained MAFs
    # Plots produced for each MAF: 
    # - its time series
    # - its variogram
    # - the loadings of each variable in the MAF (the highest is highlighted)
    
    res.maf.xi=data.frame(year=row.names(resmaf$x),resmaf$x)
    res.maf.xi=reshape(data.frame(resmaf$x),idvar = "year",ids = row.names(resmaf$x),
                            varying = list(seq(dim(resmaf$x)[2])),times=dimnames(resmaf$x)[[2]],
                            timevar="MAF",v.names = "val", direction = "long")
    res.maf.xi$Nind=jInd
    
    maf.g1h <- numeric(0)
    maf.vg <- vector("list",nbmafs)
    
    for(i in 1:nbmafs){
      x11(width=18,heigh=6,bg='white')
      par(mfrow = c(1,3))
      par("mar"=c(3.5,2.5,2.5,1))
      maf.x<-as.numeric(resmaf$x[,i][match(t.l,t)])
      barplot(aperm(matrix(maf.x)),names.arg=matrix(t.l),beside=F,space=0,col=8,
              cex.names=1,main=paste("MAF ",i," (",name,")",sep=""),
              ylim=c(-3.5,3.5))
      
      maf.vg[[i]]<-vg1d.reg(maf.x)
      maf.g1h <- c(maf.g1h,maf.vg[[i]][1,2])
      plot(maf.vg[[i]],type="o",main="",ylim=c(0,3))
      abline(h=1,lty=2)
      title("Variogram")
      
      maf.rot<-resmaf$rotation[,i]
      par("mar"=c(5,2.5,2.5,1))
      x=barplot(sort(maf.rot),horiz=FALSE,names.arg=names(sort(maf.rot)),
                las=2,main=paste(paste('MAF',i,sep='')),xaxt="n",space=1)
      end_point = 0.5 + length(maf.rot) + length(maf.rot)-1 #this is the line which does the trick (together with barplot "space = 1" parameter)
      text(seq(1.5,end_point,by=2), par("usr")[3]-0.005, 
           srt = 60, adj= 1, xpd = TRUE,
           labels = gsub('val.','',names(sort(maf.rot))), cex=0.65)
      # plot(0,0,ylim=c(-max(ceiling(abs(maf.rot))),max(ceiling(abs(maf.rot)))),
      #      xlim=c(0,jInd),type="n",xlab="",ylab="",xaxt="n",main="Loadings")
      # segments(x0=seq(0.5,jInd-.5,1),y0=rep(0,jInd),x1=seq(0.5,jInd-.5,1),y1=maf.rot)
      # abline(h=0)
      # Col<-rep(1,jInd)
      # Col[seq(1,jInd)[abs(maf.rot)==max(abs(maf.rot))]]<-3
      # text(seq(0.5,jInd-.5,1),maf.rot, names(z[,sort(match(Ind,names(z)))]),cex=1.25,col=Col)
      if (!is.null(path.export)){
        dev.print(png, file=paste(path.export,name,
                                  ".MAF",i,'.',jInd,"var.png",sep=""), 
                  units='cm',width=30, height=10,res=300)
        #dev.copy2eps(file=paste(path.export,"MAF.",jInd,"var.",name,"fig",i,'.',jInd,"var.eps",sep=""))
      }
    }
    res.maf.vario.at.lag.1 <- data.frame(MAFs=paste("MAF",1:nbmafs,sep=""),
                                         Vario.at.lag.1=maf.g1h)
    res.maf.rotation <- resmaf$rotation[,1:nbmafs]

    # Continuity of Indices
    # with the first nc MAFs
    #nc <- 2
    res.ts.vario.at.lag.1h=data.frame(Indices=names(g1h),Vario.at.lag.1=g1h)
    if (contiCalc=='script'){
      vario.at.lag.1h=res.ts.vario.at.lag.1h
    }else if (contiCalc=='article'){
      vario.at.lag.1h=res.maf.vario.at.lag.1
    }
    if(nc>nbmafs) {cat("nc must be < nbmafs","\n")}
    conti<-apply(sweep(res.maf.rotation[,1:nc]^2,2,
                       (1-vario.at.lag.1h[1:nc,2]),"*"),1,sum)
    conti[order(conti,decreasing=T)]
    res.continuity <- data.frame(continuity=conti[order(conti,decreasing=T)])
    
    # Plots
    x11(bg='white'); plot(1:length(conti),conti[order(conti,decreasing=T)],
                          type="b",xlab='rank of index',ylab='continuity',
                          pch="+", 
                          ylim=c(0,max(conti[order(conti,decreasing=T)])+.1),
                          xlim=c(1,length(conti)+3))
    text(1:length(conti),conti[order(conti,decreasing=T)],
         labels=labels(conti[order(conti,decreasing=T)]),srt=45,pos=4)
    if (!is.null(path.export)){
      dev.print(png, file=paste(path.export,name,"_continuityIndex",
                                nc,'MAFs.',jInd,"var.png",sep=""), 
                units='cm',width=30, height=30,res=300)
      #dev.copy2eps(file=paste(path.export,"MAF.",name,".fig",nbmafs+1,'.',jInd,"var.eps",sep=""))
    }
    # plot time series of selected indic
    # j<-match(names(conti[order(conti,decreasing=T)]),gsub('-','.',dimnames(z)[[2]]))
    # x11(bg='white')
    # par(mfrow=c(3,3))
    # for (i in 1:jInd){
    #   plot(t,z[,j[i]],type="o",xlab="",ylab=dimnames(z)[[2]][j[i]],main=dimnames(z)[[2]][j[i]]) 
    # }
    # dev.print(png, file=paste(path.export.hydro.Isel,"MAF.",name,".fig",nbmafs+2+i,'.',jInd,"var.png",sep=""), width=800, height=800)
    # dev.copy2eps(file=paste(path.export.hydro.Isel,"MAF.",name,".fig",nbmafs+2+i,'.',jInd,"var.eps",sep=""))
    if (!is.null(path.export)){
      graphics.off()
    }  
    resmaf=list(resmaf)
    names(resmaf)=jInd
    if (jInd==minNI){
      res.continuity.df=data.frame(NI=jInd,res.continuity,Ind=row.names(res.continuity))
      resmaf.list=resmaf
      resmaf.x=res.maf.xi
    }else{
      res.continuity.df=rbind(res.continuity.df,data.frame(NI=jInd,res.continuity,Ind=row.names(res.continuity)))
      resmaf.list=c(resmaf.list,resmaf)
      resmaf.x=rbind(resmaf.x,res.maf.xi)
    }
  }
  # MAF1,2 periods
  length(resmaf.list)
  names(resmaf.list)
  #plot MAFs standard devs
  for (i in 1:length(resmaf.list)){
    sdi=data.frame(sd=resmaf.list[[i]]$sdev)
    sdi$nind=names(resmaf.list)[[i]]
    sdi$MAF=seq(dim(sdi)[1])
    if (i==1){
      sds=sdi
    }else{
      sds=rbind(sds,sdi)
    }
  }
  x11(bg='white')
  par(mfrow=c(1,2))
  plot(sds[sds$MAF==1,'nind'],sds[sds$MAF==1,'sd'],type='b',
       xlab='No. of indices',ylab='lambda(h)',main='MAF1 variogram at lag1')
  plot(sds[sds$MAF==2,'nind'],sds[sds$MAF==2,'sd'],type='b',
       xlab='No. of indices',ylab='lambda(h)',main='MAF2 variogram at lag1')
  if (!is.null(path.export)){
    dev.print(png, file=paste(path.export,"MAF1&2.periods-Ninds",
                              minNI,'-',maxNI,".png",sep=""), 
              units='cm',width=30, height=20,res=300)
    #dev.copy2eps(file=paste(path.export,"MAF1&2.periods-Ninds",minNI,'-',maxNI,".eps",sep=""))
  }
  
  list(res.ts.vario.at.lag.1h=res.ts.vario.at.lag.1h,
       res.maf.vario.at.lag.1=res.maf.vario.at.lag.1,
       res.maf.rotation=res.maf.rotation,res.continuity.df=res.continuity.df,
       resmaf.list=resmaf.list,MAF12periodNind=sds,resmaf.x=resmaf.x)
}


normalize = function(y){
  sk = c()
  for(t in seq(0.1,1,0.1)){
    sk = c(sk,skewness(y^t))
  }
  return(seq(0.1,1,0.1)[which.min(abs(sk))])
}

maf.1D.rgeostats=function(zsela.BoB,ta.BoB,x0=2000,dx=1,xplag=0.1){
# 1D MAF calculation using RGeostats ----
  dim(zsela.BoB)
  clup.BoB.db <- db.create(zsela.BoB,x1=ta.BoB,nx=length(ta.BoB),x0=x0,dx=dx,
                           nvar=dim(zsela.BoB)[2],ndim=1)
  
  for (i in 1:dim(zsela.BoB)[2]){
    clup.BoB.db <- db.locate(clup.BoB.db,i+2,"z",i)
  }

  # Calculate 1D MAFs
  clup.BoB.db.maf <- maf.calc(clup.BoB.db,h0=0,dh=1,
                              dirvect = 0,tolang=0)

  # Get eigenvalues
  clup.BoB.db.maf.eigenval=clup.BoB.db.maf$eigen[clup.BoB.db.maf$eigen>0]
  clup.BoB.db.maf.eigenval=rev(clup.BoB.db.maf.eigenval)
  names(clup.BoB.db.maf.eigenval)=paste(
    'MAF',seq(length(clup.BoB.db.maf.eigenval)),sep='')
  # Vvariance explained per MAF
  clup.BoB.db.maf.varex=clup.BoB.db.maf.eigenval^2 / 
    sum(clup.BoB.db.maf.eigenval^2)
  clup.BoB.db.maf.varex.df=data.frame(
    MAF=paste('MAF',seq(length(clup.BoB.db.maf.varex)),sep=''),
    varex=clup.BoB.db.maf.varex)
  clup.BoB.db.maf.varex.df$cumVarex=cumsum(clup.BoB.db.maf.varex.df$varex)
  
  x11()
  barplot(clup.BoB.db.maf.varex,
          names.arg=paste('MAF',seq(length(clup.BoB.db.maf.varex)),sep=''),
          main='Variance explained')
  
  # eigen vectors
  clup.BoB.db.maf.eigenvec=clup.BoB.db.maf$pcaz2f
  # ????
  clup.BoB.db.maf$pcaf2z
  
  # Add MAFs to raw data db
  clup.BoB.db.maf.db <- pca.z2f(clup.BoB.db,clup.BoB.db.maf,radix="MAF")
  clup.BoB.db.maf.df = db.extract(clup.BoB.db.maf.db,"MAF*")

  # Individuals loadings in MAF space
  x11();par(bg="white")
  plot(clup.BoB.db.maf.df$MAF.1,clup.BoB.db.maf.df$MAF.2,
       type='n',xlab='MAF1',ylab='MAF2')
  lines(clup.BoB.db.maf.df$MAF.1,clup.BoB.db.maf.df$MAF.2)
  text(clup.BoB.db.maf.df$MAF.1,clup.BoB.db.maf.df$MAF.2,
       (x0-1)+as.numeric(row.names(clup.BoB.db.maf.df)))
  abline(v=0);abline(h=0)

  # Calculate variables coordinates in MAF space
  U = clup.BoB.db.maf$pcaz2f
  Y.eigv=rev(clup.BoB.db.maf$eigen[clup.BoB.db.maf$eigen>0])
  U2 = U %*% diag(Y.eigv^(0.5))
  clup.BoB.db.varinmaf.mat=U2
  dimnames(clup.BoB.db.varinmaf.mat)[[2]]=paste(
    'MAF',seq(dim(clup.BoB.db.varinmaf.mat)[2]),sep='')
  dimnames(clup.BoB.db.varinmaf.mat)[[1]]=names(zsela.BoB)

  x11()
  plot(clup.BoB.db.varinmaf.mat[,1],clup.BoB.db.varinmaf.mat[,2],type='n',
       xlim=c(min(clup.BoB.db.varinmaf.mat[,1])-xplag,
              max(clup.BoB.db.varinmaf.mat[,1])+xplag),
       ylim=c(min(clup.BoB.db.varinmaf.mat[,2])-xplag,
              max(clup.BoB.db.varinmaf.mat[,2])+xplag),
       xlab='MAF1',ylab='MAF2')
  text(clup.BoB.db.varinmaf.mat[,1],clup.BoB.db.varinmaf.mat[,2],
       dimnames(clup.BoB.db.varinmaf.mat)[[1]],cex=0.8)
  arrows(x0=rep(0,length(clup.BoB.db.varinmaf.mat[,1])),
         y0=rep(0,length(clup.BoB.db.varinmaf.mat[,1])),
         x1=clup.BoB.db.varinmaf.mat[,1],
         y1=clup.BoB.db.varinmaf.mat[,2],col='grey50',lengt=.1)
  abline(v=0,h=0)
  
list(res.maf=clup.BoB.db.maf,maf.eigenval=clup.BoB.db.maf.eigenval,
     maf.varex=clup.BoB.db.maf.varex,data.maf.db=clup.BoB.db.maf.db,
     ind.maf=clup.BoB.db.maf.df,var.maf=clup.BoB.db.varinmaf.mat)
}
