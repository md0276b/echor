#examples of multiplying factors for track=F
	#mult.list=power2(x=2,res.max=res.max)
	#mult.list=c(1,mult.list)

#function to change the horizontal resolution of a dataframe gmsa 
#by a factor mult


res.change=function(gmsa,mult=2,res=1,xname='xc',yname='y',
		zname='z',tname='tgmt.js',track=T,FUN=mean){

	if(track){
		#computes start, end and max. survey extent based on time in julian format
		#----------------------------
		xs=gmsa[gmsa[,tname]==min(gmsa[,tname]),c(xname,yname)]
		xe=gmsa[gmsa[,tname]==max(gmsa[,tname]),c(xname,yname)]
		xmD=rbind(xs,xe)
		LtransD=dist(xmD)

		#computes linear distances of each ESU and new distance bins
		#----------------------------
		dil=di(x=gmsa[,xname],y=gmsa[,yname],t=gmsa[,tname])*60
		gmsa$dx0i=cumsum(dil)
		gmsa$dx0=d0(x=gmsa[,xname],y=gmsa[,yname],
			x0=xs[,xname],y0=xs[,yname])*60
		d2=seq(0,max(gmsa$dx0i+res),by=res)

		#labels for distance bins
		#----------------------------
		d2lab=d2[-length(d2)]+res/2
		xnew=paste('x',res,sep='');ynew=paste('y',res,sep='')
		dnew=paste('d',res,sep='')
		gmsa[,dnew]=cut(gmsa$dx0i,breaks=d2,labels=d2lab,
			include.lowest=T)

		#aggregate layers within new distance bins with FUN function
		#----------------------------
		gmsa2=aggregate(gmsa[,c(xname,yname,zname,'hour',tname)],
			list(gmsa[,dnew]),FUN)
		names(gmsa2)[1:3]=c('dnew',xnew,ynew)

		#adds diel period flag
		#----------------------------
		gmsa2=add.DN(gmsa2)

	}else{
		x.dist=data.frame(p1=gmsa[,xname],
			p2=c(0,gmsa[1:(length(gmsa[,xname])-1),xname]))	
		x.dist$d=abs(x.dist$p2-x.dist$p1)
		resx=median(x.dist$d)	
		y.dist=data.frame(p1=gmsa[,yname],
			p2=c(0,gmsa[1:(length(gmsa[,yname])-1),yname]))
		y.dist$d=abs(y.dist$p2-y.dist$p1)
		resy=median(y.dist$d)	
		x2=seq(min(gmsa[,xname])-2*mult*resx,max(gmsa[,xname])+2*mult*resx,
			by=resx*mult)
		x2lab=x2[-1]+resx
		x2l=length(x2)
		y2=seq(min(gmsa[,yname])-2*mult*resy,max(gmsa[,yname])+2*mult*resy,
			by=resy*mult)
		y2lab=y2[-1]+resy
		y2l=length(y2)
		cl=min(c(x2l,y2l))
		res=min(c(dist(data.frame(x2[1:cl],y2[1:cl]))))*60

		xnew=paste('x',mult,sep='');ynew=paste('y',mult,sep='')
		gmsa[,xnew]=cut(gmsa[,xname],breaks=x2,labels=x2lab)
		gmsa[is.na(gmsa[,xnew]),]
		gmsa[,ynew]=cut(gmsa[,yname],breaks=y2,labels=y2lab)
		gmsa[is.na(gmsa[,ynew]),]
		gmsa$cell2=paste(gmsa[,xnew],gmsa[,ynew])
		gmsa2=aggregate(gmsa[,c('mSa','hour',tname)],list(gmsa$cell2),
			mean)
		names(gmsa2)[1]='cell2'
		gmsa2=add.DN(gmsa2)
		dim(gmsa2)
		gmsa2=merge(gmsa2,unique(gmsa[,c(xnew,ynew,'cell2')]),
			by.x='cell2',by.y='cell2')
	}
		gmsa2[,xnew]=as.numeric(as.character(gmsa2[,xnew]))
		gmsa2[,ynew]=as.numeric(as.character(gmsa2[,ynew]))
list(gmsa=gmsa,gmsa2=gmsa2,res=res)}



d0=function(x,y,x0,y0){
	d0=sqrt((x-x0)^2+(y-y0)^2)
d0}

di=function(x,y,t){
	x2=x[order(t)];y2=y[order(t)]
	x1=c(0,x2[-length(x2)]);y1=c(0,y2[-length(x2)])
	di=sqrt((x2-x1)^2+(y2-y1)^2)
	di[1]=0
di}






