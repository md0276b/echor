
resEOF=function(df,path.export.results=NULL,nEOF=1,addinverse.eof=NULL,
                nsp,signif.thr=0.5,xname='x',yname='y',zname='z',
                tname='year',plot.all=FALSE,ptitles=TRUE,
                mpalette=rev(brewer.pal(n = 10, name = "RdBu")),
                locvar.diag=FALSE){
  
  library(ggplot2)
  library(ggpattern)
  library(cowplot)
  library(viridis)
  library(RColorBrewer)
  
  nsp=gsub(']','',nsp)
  nsp=gsub('[(]','',nsp)
  nsp=gsub(')','',nsp)
  nsp=gsub(' ','',nsp)
  nsp=gsub(',','-',nsp)

  df$cell=paste(df[,xname],df[,yname],sep='-')
  # Calculate residuals
  dfa=aggregate(df[,zname],list(cell=df$cell),mean,na.rm=TRUE)
  names(dfa)[2]='mZ'
  df=merge(df,dfa,by.x='cell',by.y='cell')
  df$resid=df[,zname]-df$mZ

  df.wide.z=reshape(df[,c('cell',zname,tname)],direction='wide',
                  idvar = tname, v.names = zname, timevar= 'cell')
  df.wide=reshape(df[,c('cell','resid',tname)],direction='wide',
                  idvar = tname, v.names = 'resid', timevar= 'cell')
  df.wide.na=apply(df.wide,2,function(x){sum(is.na(x))})
  na.check=sum(df.wide.na)
  if (na.check>0) stop ('NA found in z, please check')
    
  # Mean map
  df.eof.mx=apply(df.wide.z[,names(df.wide.z)!=tname],2,mean)

  # EOF on residuals maps
  z.mat=as.matrix(df.wide[,names(df.wide)!=tname])
  dimnames(z.mat)[[1]]=df.wide[,tname]
  
  df.eof=eof2.f(x=z.mat)
  names(df.eof)

  # lda are the q non-null eigenvalues associated with the EOFs
  df.eof.lda=df.eof$lda
  # Em (s) the eigenvectors or EOFs (principal spatial modes) scaled to unity
  df.eof.Em=df.eof$em
  dimnames(df.eof.Em)[[1]]=names(df.wide)[names(df.wide)!=tname]
  # Um(t) the EOF amplitudes (principal components) scaled to lda
  df.eof.Um=df.eof$um
  dimnames(df.eof.Um)[[1]]=df.wide[,tname]
  # nu the “local” explained variance at location s associated with EOF of order m, hm(s), is the proportion of
  # variance across time that Um(t) and Em(s) explain at that location
  df.eof.nu=df.eof$nu
  dimnames(df.eof.nu)[[1]]=
    dimnames(df.eof.Em)[[1]]

  # standard deviation, % of explained variance and
  # confidence intervals.
  df.eof.r2=df.eof.lda/sum(df.eof.lda)
  df.eof.r2.df=data.frame(varex=df.eof.r2)
  df.eof.r2.df$EOF=paste('EOF',seq(dim(df.eof.r2.df)[1]),sep='')
  df.eof.r2.df$sp=nsp
  
  # Average map
  # **********************
  df.meanmap=data.frame(df.eof.mx)
  names(df.meanmap)[1]='mZ'
  # df.meanmap$cell=gsub(
  #   "logZvalue.","",row.names(df.meanmap))
  df.meanmap$cell=gsub(
    paste(zname,'.',sep=''),"",row.names(df.meanmap))
  df.meanmap=merge(
    df.meanmap,
    unique(df[,c('cell',xname,yname)]),by.x='cell',
    by.y='cell')
  head(df.meanmap)

  # EOF maps and time series
  # **********************
  # EOF maps
  #*********
  df.eof.map=data.frame(df.eof.Em)
  names(df.eof.map)=gsub('X','EOF',names(df.eof.map))
  df.eof.map$cell=gsub(
    paste('resid','.',sep=''),"",row.names(df.eof.map))
  df.eof.map=merge(
    df.eof.map,
    unique(df[,c('cell',xname,yname)]),by.x='cell',
    by.y='cell')
  head(df.eof.map)
  
  # EOF time series ----------
  #*********
  df.eof.PC.years=data.frame(df.eof.Um)
  names(df.eof.PC.years)=gsub('X','EOF',names(df.eof.PC.years))  
  df.eof.PC.years$Year=as.numeric(row.names(
    df.eof.PC.years))
  df.eof.PC.years=df.eof.PC.years[
    order(df.eof.PC.years$Year),]
  head(df.eof.PC.years)
  
  if (nEOF>dim(df.eof.Em)[2]){
    cat('No. of EOFs to plot larger than total no. of EOFs. 
        No. of EOFs to plot set to total no. of EOFs.','\n')
    nEOF=dim(df.eof.Em)[2]
  }
  
  # EOF local explained variance
  #**********
  # Local explained variance (EOFi~biomass(xy:year))
  df.eof.map.locvar=data.frame(df.eof.nu)
  names(df.eof.map.locvar)=gsub('X','locvar',names(df.eof.map.locvar))
  df.eof.map.locvar$cell=gsub(
    paste('resid','.',sep=''),"",row.names(df.eof.map.locvar))
  df.eof.map.locvar=merge(
    df.eof.map,df.eof.map.locvar,
    by.x='cell',by.y='cell')
  head(df.eof.map.locvar)
  
  plist=NULL
  
  for (i in 1:nEOF){
    
    EOFin=paste('EOF',i,sep='')
    
    limit <- max(abs(df.eof.map.locvar[,EOFin])) * c(-1, 1)
    
    #EOFi map
    print(paste('EOF',i,' summary: ',sep=''))
    print(summary(df.eof.map.locvar[,paste('EOF',i,sep='')]))
    
    if (!is.null(addinverse.eof)){
      if (length(addinverse.eof)!=nEOF) stop ('length of addinverse.eof must be nEOF (or null)')
      if (addinverse.eof[i]){
        # change ¨PCi sign
        df.eof.map.locvar[,paste('EOF',i,sep='')]=
          -df.eof.map.locvar[,paste('EOF',i,sep='')]
        summary(df.eof.map.locvar[,paste('EOF',i,sep='')])
        print(paste('EOF',i,' summary after sign change: ',sep=''))
        print(summary(df.eof.map.locvar[,paste('EOF',i,sep='')]))
      }
    }

    #EOFi amplitudes
    summary(df.eof.PC.years[,paste('EOF',i,sep='')])
    print(paste('EOF',i,' amplitude summary: ',sep=''))
    print(summary(df.eof.PC.years[,paste('EOF',i,sep='')]))
    
    if (!is.null(addinverse.eof)){
      if (length(addinverse.eof)!=nEOF) stop ('length of addinverse.eof must be nEOF (or null)')
      if (addinverse.eof[i]){
        df.eof.PC.years[,paste('EOF',i,sep='')]=
          -df.eof.PC.years[,paste('EOF',i,sep='')]
        print(paste('EOF',i,' amplitude summary after sign change: ',sep=''))
        print(summary(df.eof.PC.years[,paste('EOF',i,sep='')]))
      }
    }

    # Local explained variance (EOFi~biomass(xy:year))
    df.eof.map.locvar$signif=abs(
      df.eof.map.locvar[,paste('locvar',i,sep='')])>signif.thr
    df.eof.map.locvar$signif[
      is.na(df.eof.map.locvar$signif)]=FALSE
    names(df.eof.map.locvar)[names(df.eof.map.locvar)=='signif']=
      paste("signif",i,sep='')
    locvarin=paste('locvar',i,sep='')
    signifin=paste('signif',i,sep='')
    
    head(df.eof.map.locvar)
 
    if (length(names(table(df.eof.map.locvar[,paste('signif',i,sep='')])))==1){
      if (unique(names(table(df.eof.map.locvar[,paste('signif',i,sep='')])))==FALSE){
        cat(EOFin,': no local explained variance higher than threshold','\n')
      }
    }

    # Local explained variance diagnostic ----
    if (locvar.diag){
      EOF.locvar.analysis.res=EOF.locvar.analysis(
        df.eof.map.locvar.diag=df.eof.map.locvar[,paste('locvar',i,sep='')])
    }
    
    df.eof.map.locvar.mean=merge(
      df.eof.map.locvar,df.meanmap[,c('cell','mZ')],by.x='cell',by.y='cell')
    df.eof.map.locvar.mean$sp=nsp
    df.eof.PC.years$sp=nsp
    
    # EOF plots
    plot.EOF(df.eof.r2=df.eof.r2.df,df.eof.maps=df.eof.map.locvar.mean,
             df.eof.ts=df.eof.PC.years,EOFno=i,
             xname=xname,yname=yname,zname=zname,tname=tname,aname='mZ',
             plot.all=plot.all,path.export.results=path.export.results,
             nsp=nsp,signif.thr=signif.thr,ptitles=ptitles)
  }
  
  df.eof.map=merge(
    df.eof.map.locvar,df.meanmap[,c('cell','mZ')],by.x='cell',by.y='cell')
  df.eof.map$sp=nsp
  df.eof.PC.years$sp=nsp

  list(df.eof=df.eof,df.eof.r2=df.eof.r2.df,df.eof.lda=df.eof.lda,
       df.eof.Em=df.eof.Em,df.eof.Um=df.eof.Um,df.eof.nu=df.eof.nu,
       df.eof.mx=df.eof.mx,df.eof.map=df.eof.map,df.eof.ts=df.eof.PC.years)
}

plot.EOF=function(df.eof.r2=NULL,df.eof.maps,df.eof.ts,
                  EOFno=1,xname='Xgd',yname='Ygd',
                  zname='Zvalue',tname='Year',aname='mZ',plot.all=FALSE,
                       path.export.results=NULL,nsp='',signif.thr=0.5,
                  ptitles=TRUE,addinverse.eof=NULL,
                  mpalette=rev(brewer.pal(n = 10, name = "RdBu"))){
  
  library(ggplot2)
  library(ggpattern)
  library(cowplot)
  library(viridis)
  library(RColorBrewer)
  
  df.eof.maps.names=names(df.eof.maps)
  df.eof.ts.names=names(df.eof.ts)

  if (!is.null(df.eof.r2)){
    #Varex barplot ----
    if (ptitles){
      maini=paste("EOF",nsp,zname)
    }else{
      maini=NULL
    }
    x11()
    par(bg='white')
    barplot(df.eof.r2$varex,
            names.arg=paste('EOF',
                            seq(length(df.eof.r2$varex)),sep=''),
            ylab="Explained variance",main=maini)
    if (!is.null(path.export.results)){
      # Save figure
      dev.print(png, file=paste(path.export.results,
                                paste("EOFvarex",nsp,".png",sep=""),sep=''), 
                units='cm',width=20, height=15,res=300)
    }
  }
 
  # Average map --------
  if (ptitles){
    maini=paste('Average map \n',nsp,' ',zname,sep='')
  }else{
    maini=NULL
  }
  
  df.eof.maps$x=df.eof.maps[,xname]
  df.eof.maps$y=df.eof.maps[,yname]
  df.eof.maps$mZ=df.eof.maps[,aname]
  
  p1=ggplot() +
      geom_raster(data = df.eof.maps, 
                  aes(x = x, y = y,fill=mZ))+
      labs(title = maini,size=paste('Mean',zname)) +
      xlab('')+ylab('')+
      annotation_map(map_data("world"))+ #Add the map as a base layer before the points
      coord_quickmap()+    scale_fill_gradientn(colours = viridis(10))

  if (plot.all){
    x11()
    print(p1)
    if (!is.null(path.export.results)){
      # Save figure
      dev.print(png, file=paste(path.export.results,
                                paste(zname,"_meanMap_",nsp,".png",sep=""),
                                sep=''), 
                units='cm',width=20, height=15,res=300)
    }
  }
  if (plot.all){
    x11()
    plot(df.eof.maps$Xgd,df.eof.maps$Ygd,cex=0.1+df.eof.maps$mZ,main='mean map')
  }

  # EOF map ------  
  EOFin=paste('EOF',EOFno,sep='')

    if (!is.null(addinverse.eof)){
    if (addinverse.eof){
      # change ¨PCi sign
      df.eof.maps[,EOFin]=-df.eof.maps[,EOFin]
      print(paste(EOFin,' sign changed',sep=''))
    }
  }
  df.eof.maps$EOFin=df.eof.maps[,EOFin]
  
  limit <- max(abs(df.eof.maps[,EOFin])) * c(-1, 1)
  
  p2=ggplot() +
    geom_raster(data = df.eof.maps, 
                aes(x = x, y = y,fill=EOFin))+
    labs(title = paste(nsp,EOFin),size=EOFin) +
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+scale_fill_gradientn(colours = mpalette,limits=limit)
  if (plot.all){
    x11()
    print(p2)
    if (!is.null(path.export.results)){
      # Save figure
      dev.print(png, file=paste(path.export.results,
                                paste(zname,"_EOF",EOFno,"map_",nsp,".png",sep=""),
                                sep=''), 
                units='cm',width=20, height=15,res=300)
    }
  }
  
  locvarin=paste('locvar',EOFno,sep='')
  df.eof.maps$locvarin=df.eof.maps[,locvarin]
  signifin=paste('signif',EOFno,sep='')
  df.eof.maps$signifin=df.eof.maps[,signifin]
  
  # Local explained variance map ------
  p3=ggplot() +
    geom_raster(data = df.eof.maps, 
                aes(x = x, y = y, fill=abs(locvarin)))+
    labs(title = paste(nsp,EOFin),size=paste('EOF',EOFno,' loc var',sep='')) +
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+scale_fill_gradientn(colours = viridis(10))
  
  if (plot.all){
    x11()
    print(p3)
    if (!is.null(path.export.results)){
      # Save figure
      dev.print(png, file=paste(path.export.results,
                                paste(zname,"_EOF",EOFno,"locVar_",nsp,".png",sep=""),
                                sep=''), 
                units='cm',width=20, height=15,res=300)
    }
  }
  
  # EOF amplitudes time series ------
  if (!is.null(addinverse.eof)){
    if (addinverse.eof){
      df.eof.ts[,EOFin]=-df.eof.ts[,EOFin]
      print(paste('EOF',EOFno,'Sign changed.',sep=''))
    }
  }
  
  if (plot.all){
    x11()
    par(bg='white')
    plot(df.eof.ts[,tname],
         df.eof.ts[,paste('EOF',EOFno,sep='')],type='b',
         main=paste(nsp,EOFin),xlab='',ylab=paste('EOF',EOFno,sep=''),pch=16)
    if (!is.null(path.export.results)){
      # Save figure
      dev.print(png, file=paste(path.export.results,
                                paste("EOF",EOFno,"timeSeries",nsp,".png",
                                      sep=""),sep=''), 
                units='cm',width=20, height=15,res=300)
    }
  }
  
  df.eof.ts$tint=df.eof.ts[,tname]
  df.eof.ts$EOFin=df.eof.ts[,EOFin]
  
  p5=ggplot(data=df.eof.ts,aes(x=tint,y = EOFin,group=1))+
    geom_point()+geom_line()+
    theme(axis.text.x = element_text(angle = 45, hjust = 1))+xlab('')+
    ylab(EOFin)
  
  # EOF map with local explained variance -------------
  if (ptitles){
    maini=paste('EOF',EOFno,'\n',nsp,' ',zname,sep='')
  }else{
    maini=NULL
  }
  
  p4=ggplot(data = df.eof.maps, 
            aes(x = x, y = y, 
                fill = EOFin,pattern=signifin))+
    geom_raster()+theme(axis.title.x = element_blank(),
                        axis.title.y = element_blank())+
    labs(title = maini,
         fill=paste('EOF',EOFno,sep=''),size='Local expl. \n variance',
         pattern=paste('Local expl. \n variance>',signif.thr,sep='')) + 
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    geom_tile_pattern(pattern_colour='grey50',pattern_density=0.1)+
    #scale_pattern_type_manual(values = c(FALSE = "stripe", TRUE = "none"))+
    scale_fill_gradientn(colours = mpalette,limits=limit)+
    guides(color = "colorbar", size="legend",shape="legend")
  #scale_pattern_density_discrete(range = c(0.01, 0.3)) + 
  #scale_pattern_spacing_discrete(range = c(0.01, 0.03))
  #scale_size_area("Local correlation", breaks = c(0,0.25,0.5,0.75,1))
  if (plot.all){
    x11()
    par(bg='white')
    print(p4)
    if (!is.null(path.export.results)){
      # Save figure
      dev.print(png, file=paste(path.export.results,
                                paste("EOF",EOFno,"map",nsp,".png",sep=""),sep=""), 
                units='cm',width=15, height=20,res=300)
    }
  }  
  if (length(names(table(df.eof.maps[,paste('signif',EOFno,sep='')])))==1){
    if (unique(names(table(df.eof.maps[,paste('signif',EOFno,sep='')])))==FALSE){
      cat(EOFin,': no local explained variance higher than threshold','\n')
    }
  }
  
  x11(width=14, height=6,bg='white')
  pdiag=plot_grid(p1,p4,p5,nrow=1,scale=c(1,1.05,0.9),align='h')
  print(pdiag)
  if (!is.null(path.export.results)){
    # Save figure
    dev.print(png, file=paste(path.export.results,
                              paste("EOF",EOFno,"diagPlots",nsp,"-",zname,".png",
                                    sep=""),sep=""), 
              units='cm',width=30, height=15,res=300)
  }
  df.eof.maps2=df.eof.maps[,df.eof.maps.names]
  df.eof.ts2=df.eof.ts[,df.eof.ts.names]
  
  list(df.eof.maps2=df.eof.maps2,df.eof.ts2=df.eof.ts2)
}

EOF.locvar.analysis=function(df.eof.map.locvar.diag,lquant=seq(0,1,0.1),
                             plotit=TRUE){
  # Local explained variance distribution
  summary(df.eof.map.locvar.diag)
  lquant.df=data.frame(qmax=lquant,clquant=cut(lquant,lquant))
  df.eof.map.locvar.diag.quantiles=data.frame(
    probs=lquant,quantile=quantile(df.eof.map.locvar.diag,lquant,na.rm=TRUE))
  
  # Area vs. local explained variance
  df.eof.map.locvar.diag.area=data.frame(
    locvar=sort(df.eof.map.locvar.diag))
  df.eof.map.locvar.diag.area$c.locvar1=cut(
    x=df.eof.map.locvar.diag.area$locvar,breaks=lquant)
  df.eof.map.locvar.diag.area.areaLocvar=aggregate(
    df.eof.map.locvar.diag.area$c.locvar,
    list(c.locvar1=df.eof.map.locvar.diag.area$c.locvar1),
    length)
  names(df.eof.map.locvar.diag.area.areaLocvar)=c('c.locvarex','Ncell')
  print(dim(df.eof.map.locvar.diag.area.areaLocvar))
  print(length(lquant[-1]))
  df.eof.map.locvar.diag.area.areaLocvar=merge(
    df.eof.map.locvar.diag.area.areaLocvar,lquant.df,by.x='c.locvarex',
    by.y='clquant')
  df.eof.map.locvar.diag.area.areaLocvar$csNcell=cumsum(
    df.eof.map.locvar.diag.area.areaLocvar$Ncell)
  df.eof.map.locvar.diag.area.areaLocvar$cumpArea=
    df.eof.map.locvar.diag.area.areaLocvar$csNcell/
    sum(df.eof.map.locvar.diag.area.areaLocvar$Ncell)
  df.eof.map.locvar.diag.area.areaLocvar$pArea=
    df.eof.map.locvar.diag.area.areaLocvar$Ncell/
    sum(df.eof.map.locvar.diag.area.areaLocvar$Ncell)
  
  df.eof.map.locvar.diag.final=merge(df.eof.map.locvar.diag.area.areaLocvar,
                                     df.eof.map.locvar.diag.quantiles,
                                     by.x='qmax',by.y='probs',all.y=TRUE)
  
  if (plotit){
    x11()
    par(bg='white',mfrow=c(2,1))
    plot(lquant,quantile(df.eof.map.locvar.diag,lquant,na.rm=TRUE),type='b',
         xlab='Data percentage',
         ylab='Local explained variance quantile',main=paste('EOF',i,sep=''))
    abline(h=signif.thr,lty=2)
    plot(df.eof.map.locvar.diag.area.areaLocvar$qmax,
         1-df.eof.map.locvar.diag.area.areaLocvar$pArea,type='b',
         xlab='Local explained variance',
         ylab='Proportion of total area',main=paste('EOF',i,sep=''))
    abline(v=signif.thr,lty=2)
    if (!is.null(path.export.results)){
      dev.print(png, file=paste(path.export.results,
                                paste("EOF",i,"locvarDiag",nsp,".png",sep=""),sep=""), 
                units='cm',width=15, height=20,res=300)
    }
  }
  df.eof.map.locvar.diag.final
}

eof2.f=function(x) {
  # author: pierre.petitgas@ifremer.fr
  # x : 1 carte par ligne (k colonnes); temps : n lignes
  # returns a list: lambda, em, um, stvm, maps of leading modes, maps of explained local variance
  # performs eof or eofa depending on size of array
  n<-dim(x)[1]; k<-dim(x)[2]
  if (n>=k) {res<-eof.f(x)} else {res<-eofa.f(x)}
  return(res)
}

eof.f=function(x) {
  # author: pierre.petitgas@ifremer.fr
  # x : 1 carte par ligne (k colonnes); temps : n lignes
  # returns a list: lambda, em, um, stvm, maps of leading modes
  # |em|=1 ; |um|=lda
  n<-dim(x)[1]; k<-dim(x)[2]
  s<-t(x)%*%x /n
  res<-eigen(s); q<-sum(res$values>1e-10)
  l<-res$values[1:q]; vec<-res$vectors[,1:q]
  em<-vec; um<-(x/sqrt(n))%*%em ; um[abs(um)<1e-10]<-0
  stvm<-l/sum(l) 
  xm<-vector("list",q); for (i in 1:q) {xm[[i]]<-um[,i]%*%t(em[,i])
  xm[[i]][abs(xm[[i]])<1e-10]<-0} 
  nu<-matrix(0,nrow=k,ncol=q); for (i in 1:q) {nu[,i]<- l[i]*apply(xm[[i]],2,var)}
  nu<-sweep(nu,1,apply(nu,1,sum),"/")
  return( list(lda=l,em=em,um=um,stvm=stvm,xm=xm,nu=nu) )
} 

eofa.f = function(x) {
  # author: pierre.petitgas@ifremer.fr
  # x : cartes en colonnes (n lignes); temps en ligne (k colonnes)
  # alternative decomposition and transition formulae to eof 
  # returns a list: lambda, em, um, stvm, maps of leading modes
  # |em|=1 ; |um|=lda
  n<-dim(x)[1]; k<-dim(x)[2]
  sa<-x%*%t(x) /k
  res<-eigen(sa); q<-sum(res$values>1e-10)
  la<-res$values[1:q]; vec<-res$vectors[,1:q]
  em<-t(x)%*%vec; c<-apply(em^2,2,sum);  em<-sweep(em,2,sqrt(c),"/")   # |em|=1 :eof
  l<-la*k/n; um<-sweep(vec,2,sqrt(l),"*") ;um[abs(um)<1e-10]<-0             # |um|=lda :amplitude 
  stvm<-l/sum(l) 
  xm<-vector("list",q); for (i in 1:q) {xm[[i]]<-um[,i]%*%t(em[,i])
  xm[[i]][abs(xm[[i]])<1e-10]<-0} 
  nu<-matrix(0,nrow=k,ncol=q); for (i in 1:q) {nu[,i]<- l[i]*apply(xm[[i]],2,var)}
  nu<-sweep(nu,1,apply(nu,1,sum),"/")
  return( list(lda=l,em=em,um=um,stvm=stvm,xm=xm,nu=nu) )
}

# spacetime EOF function:

eof.spacetime=function (x, how = c("spatial", "temporal"), returnEOFs = TRUE, 
          ...) 
{
  stopifnot(is(x, "STFDF"))
  sp = x@sp
  index = index(x@time)
  x = as(x, "xts")
  switch(how[1], spatial = {
    pr = prcomp(~., data.frame(x), ...)
    if (!returnEOFs) return(pr)
    x = addAttrToGeom(sp, data.frame(pr$rotation), FALSE)
  }, temporal = {
    pr = prcomp(~., data.frame(t(x)), ...)
    if (!returnEOFs) return(pr)
    x = xts(data.frame(pr$rotation), index)
  }, stop("unknown mode: use spatial or temporal"))
  names(x) = paste("EOF", 1:ncol(x), sep = "")
  x
}