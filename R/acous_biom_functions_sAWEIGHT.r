#Compute haul h weights for each echotype e, as the mean sA of echotype e 
#within a radius of 'radius' around h
#-----------------------------------------------------

  multi.mSa.radius.dev=function(ESDUDEV,Pechef,EspDev,plotit=0,
		hradius=10,corr=cos(46*pi/180),mfrowc=c(3,3),nid='NOCHAL',ux11=FALSE){
    
		lcruises=sort(unique(as.character(ESDUDEV$CAMPAGNE)))

		for (i in 1:length(lcruises)){
			ESDUDEVi=ESDUDEV[ESDUDEV$CAMPAGNE==lcruises[i],]
			Pechei=Pechef[Pechef$CAMPAGNE==lcruises[i],]
			Pecheis=unique(Pechei[,c(nid,'LATF','LONF')])
			EspDevi=EspDev[EspDev$CAMPAGNE==lcruises[i],]
			Ndevi=sort(unique(gsub('D','',EspDevi$DEV)))
			xh=Pecheis$LONF*corr
			yh=Pecheis$LATF
			x=ESDUDEVi$LONG*corr
			y=ESDUDEVi$LAT
			list.mSa.radiusi=lapply(X=seq(length(Ndevi)),FUN=mSa.radius.dev,
				Peche2=Pechei,EsduDev2=ESDUDEVi,xh=xh,yh=yh,x=x,
					y=y,hradius=hradius,Ndev=Ndevi,nid=nid)
			if (plotit==1){
				if (ux11) x11()
				par(mfrow=mfrowc,mar=c(1,1,3,1))
				for (k in 1:length(Ndevi)){		
					plot(ESDUDEVi$LONG,ESDUDEVi$LAT,cex=0.1,
						main=paste(lcruises[i],', D',Ndevi[k],sep=''),xlab='',ylab='')
					points(Pecheis$LONF,Pecheis$LATF,pch=1,col=2,
					  cex=log(list.mSa.radiusi[[k]]$mSa+1))
          points(Pecheis[list.mSa.radiusi[[k]]$mSa==0,'LONF'],
            Pecheis[list.mSa.radiusi[[k]]$mSa==0,'LATF'],pch=3,col=2,
					  cex=1)  
					coast()
					if (k==1){
						legend('bottomleft',
							legend=c('ESDU','Trawl haul (size prop. to weight)',
              'Trawl haul with null weight'),
							pch=c(1,16,3),col=seq(2),bg='white')
					} 
				}
			}else if (plotit==2){
        par(mar=c(1,1,3,1))
				for (k in 1:length(Ndevi)){
          if (ux11) x11()
					plot(ESDUDEVi$LONG,ESDUDEVi$LAT,cex=0.1,
						main=paste(lcruises[i],', D',Ndevi[k],sep=''),xlab='',ylab='')
					points(Pecheis$LONF,Pecheis$LATF,pch=1,col=2,
					  cex=0.1+log(list.mSa.radiusi[[k]]$mSa+1))
          points(Pecheis[list.mSa.radiusi[[k]]$mSa==0,'LONF'],
            Pecheis[list.mSa.radiusi[[k]]$mSa==0,'LATF'],pch=3,col=2,
  				  cex=1)
					coast()
					legend('bottomleft',
							legend=c('ESDU','Trawl haul (size prop. to weight)',
              'Trawl haul with null weight'),
							pch=c(1,1,3),col=seq(2),bg='white')
				}
			}  
			if (i==1){
				list.mSa.radius.db=list(list.mSa.radiusi)
			}else{
				list.mSa.radius.db=c(list.mSa.radius.db,
					list(list.mSa.radiusi))
			}
		}
	list.mSa.radius.db
	}

	mSa.radius.dev=function(X,Ndev,Peche2,EsduDev2,xh,yh,x,y,hradius=10,nid='NOCHAL'){
                z=EsduDev2[,paste('D',Ndev[X],sep='')]
                mSa.radius2=data.frame(NOCHAL=unique(Peche2[,nid]),
                        mSa=sapply(seq(length(unique(Peche2[,nid]))),
				                mSa.radius,xh,yh,x,y,z,id=unique(Peche2[,nid]),hradius))
                mSa.radius2[is.na(mSa.radius2$mSa),'mSa']=0
  mSa.radius2
  }

	mSa.radius=function(k,xh,yh,x,y,z,id,hradius=10){
                d<-(sqrt(outer(x,xh[k],"-")*outer(x,xh[k],"-")+
                        outer(y,yh[k],"-")*outer(y,yh[k],"-")))*60
                mzs=mean(z[d<hradius])
                Ns=length(z[d<hradius])
                rres=data.frame(NOCHAL=id[k],mSa=mzs,Nesu=Ns)
                mzs     
      }


