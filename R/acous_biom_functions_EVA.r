#Function for computing estimation variances and CV
#-------------------------------------------------
# mean biomass: mB = msA * mXE
# msA: mean acoustic density in area (sA)
# mXE: mean scaling coefficients, derived from catches and TS equations
# sigmaE2 = var(mB)= var(msA * mXE) = msA^2*var(mXE)+mXE^2*var(msA)
# CVE = sqrt(sigmaE2)/mB
# the formula is applied for each echotype:region combination
# the overall estimation variance is computed by summing all 
# echotype:region variances 
#Inputs:
#----------
# Xek.df: Xe per species/region
# ESDUDEV0: sA per esdu/echotype/species/region
# deszones: regions areas

EVA=function(Xek.df,ESDUDEV0,deszones){
  
  ldevs=unique(Xek.df$DEV)

  #Prepare equation terms: sA and Xe variance/mean per echotype and region
  #------------------------------------------------
  for (i in 1:length(ldevs)){
#   
    #Import XE per species/echotype/region/haul          
    Xei=Xek.df[Xek.df$DEV==ldevs[i],]
    head(Xei)
    #Type of depth stratum
    tregion=unique(deszones[deszones$ID%in%unique(Xei$region),'TYPE'])
    #check weights
    aggregate(Xei$wE,list(Xei$region,Xei$CodEsp),sum)
    dim(Xei)
    nregion='region'
    #Xe variances per species cat and region
    varXei=aggregate(Xei$Xe,list(Xei$CodEsp,Xei[,nregion]),var,na.rm=FALSE)
    names(varXei)=c('CodEsp','region','varXe')
    varXei$DEV=ldevs[i]
    #sA variances per species cat and region    
    varsAi=aggregate(ESDUDEV0[,ldevs[i]],
      list(ESDUDEV0[,paste('zones',tregion,sep='')]),var,na.rm=FALSE)
    names(varsAi)=c('region','varsA')
    varsAi$DEV=ldevs[i]
    #Merge Xe and sA variances per species cat and region
    varsi=merge(varXei,varsAi,by.x=c('region','DEV'),by.y=c('region','DEV'))
    #Xe arithmetic average
    mXei=aggregate(Xei$Xe,list(Xei$CodEsp,Xei[,nregion]),mean,na.rm=FALSE)
    names(mXei)=c('CodEsp','region','mXe')
    mXei$DEV=ldevs[i]
    #Xe weighted average
    mwXei=aggregate(Xei$Xe*Xei$wE,list(Xei$CodEsp,Xei[,nregion]),
                    sum,na.rm=FALSE)
    names(mwXei)=c('CodEsp','region','mwXe')
    mwXei$DEV=ldevs[i]
    #Sa arithmetic average
    msAi=aggregate(ESDUDEV0[,as.character(ldevs[i])],
      list(ESDUDEV0[,paste('zones',tregion,sep='')]),mean,na.rm=FALSE)
    names(msAi)=c('region','msA')
    msAi$DEV=ldevs[i]    
    #No of esdus per region
    wsA=aggregate(ESDUDEV0[,'TC'],
      list(ESDUDEV0[,paste('zones',tregion,sep='')]),length)
    names(wsA)=c('region','NsA')
    wsA$wsA=1/wsA$NsA
    #Merge all
    msi=merge(mXei,msAi,by.x=c('region','DEV'),by.y=c('region','DEV'))
    msi=merge(msi,mwXei,by.x=c('region','DEV','CodEsp'),
              by.y=c('region','DEV','CodEsp'))
    msi=merge(msi,wsA,by.x=c('region'),by.y=c('region'))
    
    #compute sum of squared weights
    wi=aggregate(Xei$wE^2,list(Xei$CodEsp,Xei[,nregion]),sum,na.rm=FALSE)
    names(wi)=c('CodEsp','region','wss')
    #Check weights                                            
    aggregate(Xei$wE,list(Xei$CodEsp,Xei[,nregion]),sum)
    #arithmetic average weights for comparison
    Ni=aggregate(Xei$NOCHAL,list(Xei$CodEsp,Xei[,nregion]),length)
    names(Ni)=c('CodEsp','region','Nhauls')
    Ni$wXes=1/Ni$Nhauls
    wi=merge(wi,Ni)
    wi$DEV=ldevs[i]
    CV.dfi=merge(varsi,msi,by.x=c('CodEsp','region','DEV'),
      by.y=c('CodEsp','region','DEV'))
    CV.dfi=merge(CV.dfi,wi,by.x=c('CodEsp','region','DEV'),
      by.y=c('CodEsp','region','DEV'))
    
    if (i==1){CV.df=CV.dfi
    }else{CV.df=rbind(CV.df,CV.dfi)}
  }
  names(CV.df)
  
  #adds region areas
  #---------------------------
  CV.df=merge(CV.df,deszones[,c('ID','AREA','CAMPAGNE','TYPE')],
    by.x=c('region'),by.y=c('ID'))

  #Estimation variances and CVs
  #--------------------------
  #Surface of the estimation zone
  Asup=sum(unique(deszones$AREA))/2
  Asups=aggregate(deszones$AREA,list(deszones$TYPE),sum)
  names(Asups)=c('TYPE','AREAtot')
  CV.df=merge(CV.df,Asups,by.x='TYPE',by.y='TYPE')
  #Check sum of sum of squared X_E weights vs. arithmetic weights
  plot(CV.df$wss,CV.df$wXes,xlab='sum of squared X_E weights',
       ylab='1/No. of hauls')
  abline(a=0,b=1)
  #Estimation variance and CV per species, echotype and region
  #---------------------------
  #Biomass per ESDU derived from Xe average weighted by sA in neighborhood
  CV.df$mwB=CV.df$msA*CV.df$mwXe
  # XE Identification variance of the biomass per ESDU weighted average
  CV.df$ewi=CV.df$msA^2*CV.df$varXe*CV.df$wss           
  # NASC Spatial estimation variance of the biomass per ESDU weighted average
  CV.df$ews=CV.df$mwXe^2*CV.df$varsA*CV.df$wsA
  # 2nd order product of XE and NASC variances of the biomass per ESDU weighted average
  CV.df$ew2=CV.df$varXe*CV.df$wss*CV.df$varsA*CV.df$wsA 
  # Total estimation variance of the biomass per ESDU weighted average
  CV.df$ew=CV.df$msA^2*CV.df$varXe*CV.df$wss+
    CV.df$mwXe^2*CV.df$varsA*CV.df$wsA+
    CV.df$varXe*CV.df$wss*CV.df$varsA*CV.df$wsA
  # Total estimation error per species, echotype and region, Xe weighted average
  CV.df$wCV=sqrt(CV.df$ew)/CV.df$mwB
  # Identification estimation error per species, echotype and region, Xe weighted average
  CV.df$wCVi=sqrt(CV.df$ewi)/CV.df$mwB
  # NASC spatial estimation error per species, echotype and region, Xe weighted average
  CV.df$wCVs=sqrt(CV.df$ews)/CV.df$mwB
  # residual estimation error per species, echotype and region, Xe weighted average
  CV.df$wCV2=sqrt(CV.df$ew2)/CV.df$mwB
  
  #Biomass per ESDU derived from Xe arithmetic average
  CV.df$mB=CV.df$msA*CV.df$mXe
  # XE Identification variance of the biomass per ESDU arithmetic average
  CV.df$eai=CV.df$msA^2*CV.df$varXe*CV.df$wXes
  # NASC Spatial variance of the biomass per ESDU arithmetic average
  CV.df$eas=CV.df$mXe^2*CV.df$varsA*CV.df$wsA
  # 2nd order product of XE and NASC variances of the biomass per ESDU arithmetic average
  CV.df$ea2=CV.df$varXe*CV.df$wXes*CV.df$varsA*CV.df$wsA
  # Total estimation variance of the biomass per ESDU arithmetic average
  CV.df$ea=CV.df$msA^2*CV.df$varXe*CV.df$wXes+
    CV.df$mXe^2*CV.df$varsA*CV.df$wsA+
    CV.df$varXe*CV.df$wXes*CV.df$varsA*CV.df$wsA
  # total estimation error per species, echotype and region, Xe arithmetic average
  CV.df$CV=sqrt(CV.df$ea)/CV.df$mB
  # identification error per species, echotype and region, Xe arithmetic average
  CV.df$CVi=sqrt(CV.df$eai)/CV.df$mB
  # spatial error per species, echotype and region, Xe arithmetic average
  CV.df$CVs=sqrt(CV.df$eas)/CV.df$mB
  # 2nd order error per species, echotype and region, Xe arithmetic average
  CV.df$CV2=sqrt(CV.df$ea2)/CV.df$mB
  # Percentages of total variance per species, echotype and regions
  #---------------------------
  #CV.df=merge(CV.df,CV.csp,by.x='CodEsp',by.y='CodEsp')

  CV.df$sp=substr(CV.df$CodEsp,1,8)
  CV.df$DEVregion=paste(CV.df$DEV,CV.df$region,sep='R')
  #Weights for weighted averaged over regions
  CV.df$wA=CV.df$AREA^2/CV.df$AREAtot^2
  #Weighted estimation variances over regions
  CV.df$wew=CV.df$ew*CV.df$wA
  CV.df$wea=CV.df$ea*CV.df$wA
  CV.df$wewi=CV.df$ewi*CV.df$wA
  CV.df$wews=CV.df$ews*CV.df$wA
  CV.df$wew2=CV.df$ew2*CV.df$wA
  CV.df$weai=CV.df$eai*CV.df$wA
  CV.df$weas=CV.df$eas*CV.df$wA
  CV.df$wea2=CV.df$ea2*CV.df$wA
  CV.df$mwB1=CV.df$mwB*CV.df$wA
  CV.df$mB1=CV.df$mB*CV.df$wA
  
  sigmaE2tot=aggregate(CV.df$wea,list(CV.df$sp),sum,na.rm=TRUE)
  names(sigmaE2tot)=c('sp','sigmaE2tot')
  wsigmaE2tot=aggregate(CV.df$wew,list(CV.df$sp),sum,na.rm=TRUE)
  names(wsigmaE2tot)=c('sp','wsigmaE2tot')
  sigmaE2tots=merge(sigmaE2tot,wsigmaE2tot)
  
  CV.df=merge(CV.df,sigmaE2tots,by.x='sp',by.y='sp')
  #Proportions of total estimation variance per sp and region
  CV.df$prop.vara=CV.df$wea/CV.df$sigmaE2tot
  CV.df$prop.varw=CV.df$wew/CV.df$wsigmaE2tot
  #identification variance over total estimation variance per sp and region
  CV.df$prop.varai=CV.df$weai/CV.df$sigmaE2tot
  CV.df$prop.varwi=CV.df$wewi/CV.df$wsigmaE2tot
  #spatial variance over total estimation variance per sp and region
  CV.df$prop.varas=CV.df$weas/CV.df$sigmaE2tot
  CV.df$prop.varws=CV.df$wews/CV.df$wsigmaE2tot
  #2nd order variance over total estimation variance per sp and region
  CV.df$prop.vara2=CV.df$wea2/CV.df$sigmaE2tot
  CV.df$prop.varw2=CV.df$wew2/CV.df$wsigmaE2tot

  CV.df
}  
  
#Function for plotting estimation CVs or 
#proportions of total estimation variance
#--------------------------------------------------

  EVA.barplot=function(df,lsps=c('SCOM-SCO','TRAC-TRU',
        'ENGR-ENC','SARD-PIL'),anames=NULL,vnames=c('prop.vara','prop.varw'),
        v2names=NULL,tlegend=c('Weighted mean Xe','Arithmetic mean Xe'),
        xlabs=c('',''),mar=c(5, 4, 4, 2) + 0.1,...){
    CV.dfs=df[df$sp%in%lsps,]
    lsp=unique(CV.dfs$sp)
    if(length(lsps)==1){
      par(mfrow=c(1,2),mar=mar)
      barplot(t(as.matrix(CV.dfs[,vnames])),names.arg=CV.dfs[,anames],
          horiz=TRUE,las=2,main=lsp[1],beside=TRUE,legend=tlegend,
          xlab=xlabs[1],...)
      barplot(t(as.matrix(CV.dfs[,v2names])),names.arg=CV.dfs[,anames],
          horiz=TRUE,las=2,main=lsp[1],beside=TRUE,
          xlab=xlabs[2])
    }else{
      par(mfrow=c(2,2),mar=mar)
      for (i in 1:length(lsp)){
        CV.dfi=CV.dfs[CV.dfs$sp==lsp[i],]
        if (i==1){
          barplot(t(as.matrix(CV.dfi[,vnames])),names.arg=CV.dfi[,anames],
            horiz=TRUE,las=2,main=lsp[i],beside=TRUE,
            legend=tlegend,xlab=xlabs[1],...)
        }else{
          barplot(t(as.matrix(CV.dfi[,vnames])),names.arg=CV.dfi[,anames],
            horiz=TRUE,las=2,main=lsp[i],beside=TRUE,xlab='')
        }
      }
    }  
    CV.dfs
  }
  
#Function for plotting estimation CVs
#--------------------------------------------------

  eCV.plot=function(df,lsps=c('SCOM-SCO','TRAC-TRU',
        'ENGR-ENC','SARD-PIL'),anames=NULL,vnames=c('prop.vara','prop.varw'),
        tlegend=c('CV, weighted mean Xe','CV, arithmetic mean Xe'),...){
    CV.dfs=df[df$sp%in%lsps,]
    lsp=unique(CV.dfs$sp)
    par(mfrow=c(2,2))
    for (i in 1:length(lsp)){
      CV.dfi=CV.dfs[CV.dfs$sp==lsp[i],]
      if (i==1){
        barplot(t(as.matrix(CV.dfi[,vnames])),names.arg=CV.dfi[,anames],
            horiz=TRUE,las=2,main=lsp[i],beside=TRUE,xlab='',
            legend=tlegend,...)
      }else{
        barplot(t(as.matrix(CV.dfi[,vnames])),names.arg=CV.dfi[,anames],
            horiz=TRUE,las=2,main=lsp[i],beside=TRUE,xlab='')
      }
    }
    CV.dfs
  }  
 