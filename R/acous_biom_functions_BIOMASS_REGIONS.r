#*********************
#Functions for acoustic biomass assessment
#*********************
#BIOMASS PER REGION
#***********************************

#Methods
#***********************************
# CALCUL DES BIOMASSES ACOUSTIQUES PAR ESPECE 
# calcul du facteur d'écho-intégration par espece pour chaque chalutage
#   d'apres Diner 1) note Evaluation des stocks par echo-integration, DITI/GO/TP,
#                    12/11/98 revue 2/12/98, p.3,11
#                 2) fonction étalonnage, manuel de MOVIES+
#                 3) note Equivalence énergie->sA, TMSI/TP 29/09/2001, p.6 annexe1
# 
# attention: energie en mv2m2 (an <2002)
#   Xei=kei/sum.sur.e(kei/Fei) i=chalut; e=espece
#   TSk=20*log(lmoy)-b+10*log(moule)
#   Fe=(Fr/x)*10**(-(35+TSk)/10); Fr/x = 0.00585
#   Xei=kei/sum.sur.e(kei/Fei) i=chalut; e=espece
# energie en sA (>=2002)
#   1/Fe = sigma = 4*pi*10**(TSk/10)
#                = 4*pi*(lmoy**2)*(moule*1000)*10**(-b/10) 
#   TSk=20*log(lmoy)-b+10*log(moule)
#
#	
#
# unites sA : Dev en m2.NM-2 ; sigma en m2.tonne-1 ; Xe en tonne.m-2 ; Dev*Xe en tonne.NM-2
# unites Energ: Dev en m2mV2  ; Fr/x permet conversion unites ; 
#          Fe en tonne.NM-2.(m2mV2)-1 ref. x=1NM et TSk=-35dB.kg-1 ; Dev*Xe en tonne.NM-2 
#
# utilisation de la correspondance experte entre type de deviations et groupes d'especes
# evaluation par type de deviation
# association de la deviation.k de l'esdu.i au chalutage.j le plus proche
#      pour chaque Dev.k : 
#           calculer le ke.j du groupe d'espece de Dev.k dans chalut.j
#           calculer le Xe.kj
#      pour chaque Esdu.i et devation.k : 
#           identifier le chalutage.kj le plus proche 
#           calculer Xe.kj*Dev.ki
#      pour chaque Esdu.i :
#           sommer les biomasses par especes estimees dans les deviations.k
#
###
#***************************************************
#Front-end functions
#***************************************************


#****************************************************************
#Low-level functions
#****************************************************************
  #***************************************************
  #Import region boundaries in shape files
	#***************************************************

  regionshp.import=function(path.regions,ESDUDEV=NULL,Pechef=NULL,
                            keep.projection=FALSE){
    #list .shp files in folder path
    lf.shp=sort(list.files(path.regions,pattern='*.shp'))
    if(length(lf.shp)>0){
      ftype='shp'
      if (grepl('_',lf.shp[1])){
        lfs.shp=data.frame(t(data.frame(strsplit(lf.shp,split='_'))))
        if (dim(lfs.shp)[2]==2){
          lfs.shp$zcode=lfs.shp$X1
          lfs.shp$STRATE=substr(lfs.shp$X2,1,4)
          lfs.shp$TYPE=substr(lfs.shp$X2,1,1)
          lfs.shp$ID=substr(lfs.shp$X1,7,7)
          lfs.shp$AREA.sp=0
          lfs.shp$AREA.pbs=0
          lfs.shp$ID2=paste(lfs.shp$STRATE,lfs.shp$ID,sep='.')
        }else if (dim(lfs.shp)[2]==3){
          lfs.shp$cruise=lfs.shp$X1
          lfs.shp$zcode=lfs.shp$X2
          lfs.shp$STRATE=substr(lfs.shp$X3,1,4)
          lfs.shp$TYPE=substr(lfs.shp$X3,1,1)
          lfs.shp$ID=as.numeric(gsub(".*?([0-9]+).*", "\\1", lfs.shp$X2)) 
          lfs.shp$AREA.sp=0
          lfs.shp$AREA.pbs=0
          lfs.shp$ID2=paste(lfs.shp$STRATE,lfs.shp$ID,sep='.')
        }  
      }else{
        N=length(lf.shp)
        lfs.shp=data.frame(CAMPAGNE=substr(lf.shp,1,3),zcode=substr(lf.shp,4,7),
                           TYPE=substr(lf.shp,5,5))
        lfs.shp$zcode=gsub('\\.','',lfs.shp$zcode)  
        lfs.shp$ID=gsub('ZC','',lfs.shp$zcode)
        lfs.shp$ID=gsub('ZS','',lfs.shp$ID)
        lfs.shp$STRATE='CLAS'
        lfs.shp[lfs.shp$TYPE=='S','STRATE']='SURF'
        lfs.shp$AREA=0
        lfs.shp$ID2=paste(lfs.shp$STRATE,lfs.shp$ID,sep='.')
      }	 
    }
    
    lf.geo=sort(list.files(path.regions,pattern='*.gpkg'))
    if(length(lf.geo)>0){
      ftype='geo'
      if (grepl('_',lf.geo[1])){
        lfs.shp=data.frame(t(data.frame(strsplit(lf.geo,split='_'))))
        if (dim(lfs.shp)[2]==2){
          lfs.shp$zcode=lfs.shp$X1
          lfs.shp$STRATE=substr(lfs.shp$X2,1,4)
          lfs.shp$TYPE=substr(lfs.shp$X2,1,1)
          lfs.shp$ID=rep(1,2)
          lfs.shp$AREA.sp=0
          lfs.shp$AREA.pbs=0
          lfs.shp$ID2=paste(lfs.shp$STRATE,lfs.shp$ID,sep='.')
        }else if (dim(lfs.shp)[2]==4){
          lfs.shp$cruise=lfs.shp$X1
          lfs.shp$zcode=lfs.shp$X3
          lfs.shp$STRATE=lfs.shp$X3
          lfs.shp$TYPE=lfs.shp$X2
          lfs.shp$ID=substr(lfs.shp$X4,1,1) 
          lfs.shp$AREA.sp=0
          lfs.shp$AREA.pbs=0
          lfs.shp$ID2=paste(lfs.shp$STRATE,lfs.shp$ID,sep='.')
        }  
      }else{
        N=length(lf.geo)
        lfs.shp=data.frame(CAMPAGNE=substr(lf.geo,1,3),zcode=substr(lf.geo,4,7),
                           TYPE=substr(lf.geo,5,5))
        lfs.shp$zcode=gsub('\\.','',lfs.shp$zcode)  
        lfs.shp$ID=gsub('ZC','',lfs.shp$zcode)
        lfs.shp$ID=gsub('ZS','',lfs.shp$ID)
        lfs.shp$STRATE='CLAS'
        lfs.shp[lfs.shp$TYPE=='S','STRATE']='SURF'
        lfs.shp$AREA=0
        lfs.shp$ID2=paste(lfs.shp$STRATE,lfs.shp$ID,sep='.')
      }	 
    }
    #lfs.shp=lfs.shp[order(lfs.shp$ID),]
    #aggregate(lfs.shp$AREA,list(lfs.shp$STRATE),sum)
    #area11 
    #import .shp files in folder path
    # library(maptools) retired
    #library(PBSmapping)
    
    if(length(lf.geo)==0&length(lf.shp)==0){
      stop('No region file provided')
    }
    
    xyhauls=unique(Pechef[,c('NOCHAL','LONF','LATF','STRATE')])
    #Define sp objects based on esdus and trawl hauls positions
    esdu.sp=SpatialPoints(cbind(ESDUDEV$LONG,ESDUDEV$LAT))
    peche.sp=SpatialPoints(cbind(xyhauls$LONF,xyhauls$LATF))
    #plot(esdu.sp);plot(peche.sp,add=TRUE,col=2)
    #esdu.sp<- spTransform(esdu.sp, CRS=CRS("+proj=longlat +datum=WGS84"))
    proj4string(esdu.sp)=CRS("+proj=longlat +datum=WGS84")
    #peche.sp<- spTransform(peche.sp, CRS=CRS("+proj=longlat +datum=WGS84"))
    proj4string(peche.sp)=CRS("+proj=longlat +datum=WGS84")
    
    #readShapeLines(paste(path.regions,lf.shp[i],sep=''))
    #library(shapefiles)
    #nfile=paste(path.regions,lf.shp[i],sep='')
    #nfile="~/.gvfs/donnees2 sur nantes/Campagnes/PELGAS2012/Evaluation_stock/donnees/FishRview/shp/strate 1"
    #polyi=read.shapefile(nfile)
    #Apply region projection to esdus and trawl hauls positions
    #***************************************************-
    if (ftype=='shp'){
      library(sf)
      polyi = read_sf(paste(path.regions,'/',lf.shp[1],sep=''))
      #polyi=as_Spatial(polyi)
      #projpolyi <- proj4string(polyi)
      lf.regions=lf.shp
    }else if(ftype=='geo'){
      library(sf)
      polyi = read_sf(paste(path.regions,'/',lf.geo[1],sep=''))
      lf.regions=lf.geo
    }
    
    if (!keep.projection){
      st_transform(polyi, 4326)
      polyi=as_Spatial(polyi)
      projpolyi <- proj4string(polyi)
      cat('Polygon projection set to longlat','\n')
    }else{
      cat('Polygon projection:',
          projpolyi,'\n')
      cat('Esdu and catches projected into polygon projection','\n')
    }
    
    #esdu and catch data in polygon projection
    esdu.sp.shp <- spTransform(esdu.sp, CRS=CRS(projpolyi))
    peche.sp.shp <- spTransform(peche.sp, CRS=CRS(projpolyi))
    
    #Import regions sequentially and define which esdus and hauls are inside
    #each region
    N=length(lf.regions)
    lfs.shp$AREA.sp=NA
    lfs.shp$AREA.pbs=NA
    Npoly=1
    lfs.shp2=NULL
    
    for (i in 1:N){
      if (ftype=='shp'){
        polyi = read_sf(paste(path.regions,'/',lf.shp[i],sep=''))
        polyi=as_Spatial(polyi)
        projpolyi <- proj4string(polyi)
      }else if (ftype=='geo'){
        polyi = read_sf(paste(path.regions,'/',lf.geo[i],sep=''))
        polyi=as_Spatial(polyi)
        projpolyi <- proj4string(polyi)
      }
      lfs.shpi=lfs.shp[i,]
      Npolsi=dim(polyi)[1]
      if (is.null(Npolsi)){
        Npolsi=length(polyi)
      }
      # Case 1: several polygons in file
      if (Npolsi>1){
        lfs.shp2i=NULL
        for (j in 1:length(polyi@polygons)){
          polyij=polyi[j,]
          lfs.shpij=lfs.shpi
          # set polygon IDs as increasing numbers
          polyij@data[1]=as.character(Npoly)
          lfs.shpij$ID=c(polyij@data)
          lfs.shpij$ID2=paste(lfs.shpij$STRATE,lfs.shpij$ID,sep='.')
          esdu.hauls.regions.res=get.esdu.haul.regions.belongings(
            polyi=polyij,esdu.sp,peche.sp,lfs.shpij,xyhauls,
            keep.projection=keep.projection,ESDUDEV = ESDUDEV)
          names(esdu.hauls.regions.res)
          lfs.shp2i=esdu.hauls.regions.res$lfs.shpij
          esdu.inpolyi=esdu.hauls.regions.res$esdu.inpolyi
          peche.inpolyi=esdu.hauls.regions.res$peche.inpolyi
          if (i==1&j==1){
            esdu.inpoly=esdu.inpolyi
            peche.inpoly=peche.inpolyi
            polys=list(polyij)
            lfs.shp2=lfs.shp2i
          }else{
            esdu.inpoly=cbind(esdu.inpoly,sel=esdu.inpolyi$sel)
            peche.inpoly=cbind(peche.inpoly,sel=peche.inpolyi$sel)
            polys=c(polys,polyij)
            lfs.shp2=rbind(lfs.shp2,lfs.shp2i)
          }
          names(esdu.inpoly)[names(esdu.inpoly)=='sel']=
            paste(lfs.shpij[,'STRATE'],as.character(lfs.shpij[,'ID']),
                  sep='.')
          names(peche.inpoly)[names(peche.inpoly)=='sel']=
            paste(lfs.shpij[,'STRATE'],as.character(lfs.shpij[,'ID']),
                  sep='.')
          Npoly=Npoly+1
        }
      # Case 2: 1 polygon in file  
      }else{
        lfs.shpi$ID=1
        lfs.shpi$ID2=paste(lfs.shpi$STRATE,lfs.shpi$ID,sep='.')
        lfs.shp[i,'ID']=1
        lfs.shp[i,'ID2']=paste(lfs.shp[i,'STRATE'],lfs.shp[i,'ID'],sep='.')
        esdu.hauls.regions.res=get.esdu.haul.regions.belongings(
          polyi=polyi,esdu.sp,peche.sp,lfs.shpij=lfs.shpi,xyhauls,
          keep.projection=keep.projection,ESDUDEV=ESDUDEV)
        lfs.shpi=esdu.hauls.regions.res$lfs.shpij
        esdu.inpolyi=esdu.hauls.regions.res$esdu.inpolyi
        peche.inpolyi=esdu.hauls.regions.res$peche.inpolyi
        if (i==1){
          esdu.inpoly=esdu.inpolyi
          peche.inpoly=peche.inpolyi
          polys=list(polyi)
          lfs.shp2=lfs.shpi
        }else{
          esdu.inpoly=cbind(esdu.inpoly,sel=esdu.inpolyi$sel)
          peche.inpoly=cbind(peche.inpoly,sel=peche.inpolyi$sel)
          polys=c(polys,polyi)
          lfs.shp2=rbind(lfs.shp2,lfs.shpi)
        }
        names(esdu.inpoly)[names(esdu.inpoly)=='sel']=
          paste(lfs.shp[i,'STRATE'],as.character(lfs.shp[i,'ID']),sep='.')
        names(peche.inpoly)[names(peche.inpoly)=='sel']=
          paste(lfs.shp[i,'STRATE'],as.character(lfs.shp[i,'ID']),sep='.')
      }
    }
    lfs.shp=lfs.shp2

    #Adds esdus belonging to regions
    #*****************************
    dim(esdu.inpoly)
    names(esdu.inpoly)
    #esdu.inpoly=data.frame(esdu.inpoly)
    polyCLAS=paste(lfs.shp[lfs.shp$STRATE=='CLAS','STRATE'],
                   lfs.shp[lfs.shp$STRATE=='CLAS','ID'],sep='.')
    polySURF=paste(lfs.shp[lfs.shp$STRATE=='SURF','STRATE'],
                   lfs.shp[lfs.shp$STRATE=='SURF','ID'],sep='.')
    #esdus belongings to regions
    in.poly.CLAS=data.frame(esdu.inpoly[,names(esdu.inpoly)%in%polyCLAS])
    names(in.poly.CLAS)=polyCLAS
    in.poly.SURF=data.frame(esdu.inpoly[,names(esdu.inpoly)%in%polySURF])
    names(in.poly.SURF)=polySURF
    dim(esdu.inpoly)
    dim(in.poly.CLAS)
    esdu.inpoly$Region.CLAS1=unlist(apply(in.poly.CLAS,1,sel.name,
      n=names(in.poly.CLAS)))
    esdu.inpoly$Region.SURF1=unlist(apply(in.poly.SURF,1,sel.name,
      n=names(in.poly.SURF)))
    #trawl hauls belongings to regions  
    in.poly.CLAS=data.frame(peche.inpoly[,names(peche.inpoly)%in%polyCLAS])
    names(in.poly.CLAS)=polyCLAS
    in.poly.SURF=data.frame(peche.inpoly[,names(peche.inpoly)%in%polySURF])
    names(in.poly.SURF)=polySURF
    peche.inpoly$Region.CLAS=unlist(apply(in.poly.CLAS,1,sel.name,
      n=names(in.poly.CLAS)))
    peche.inpoly$Region.SURF=unlist(apply(in.poly.SURF,1,sel.name,
      n=names(in.poly.SURF)))
    names(esdu.inpoly)
    peche.inpoly=merge(peche.inpoly,xyhauls,by.x='NOCHAL',by.y='NOCHAL')
    
    list(lfs.shp=lfs.shp,esdu.sp=esdu.sp,esdu.sp.shp=esdu.sp.shp,polys=polys,
         esdu.inpoly=esdu.inpoly,peche.sp=peche.sp,peche.sp.shp=peche.sp.shp,
         peche.inpoly=peche.inpoly)
  }  
  
  
  get.esdu.haul.regions.belongings=function(polyi,esdu.sp,
                                            peche.sp,
                                            lfs.shpij,xyhauls,
                                            keep.projection=FALSE,
                                            ESDUDEV){
    library(sf)
      if (!keep.projection){
        polyi <- spTransform(polyi, CRS=CRS("+proj=longlat +datum=WGS84"))
        cat('Polygon projection set to longlat','\n')
        # select esdus in polygon
        esdu.ini=unlist(over(polygons(polyi),esdu.sp,returnList = TRUE))
        # select hauls in polygon
        peche.ini=unlist(over(polygons(polyi),peche.sp,returnList = TRUE))
        # extract polygon area
        lfs.shpij$AREA.sp=polyi@polygons[[1]]@Polygons[[1]]@area*3600
        api=polyi@polygons[[1]]@Polygons[[1]]@area
        #area in squared nautical miles 
        api*3600
        #area in squared km
        api*(1.852*60)^2
        #with sf
        polyi_sf=st_as_sf(polyi)
        # convert to UTM to get same results as PBSmapping
        polyi_sf_xy=data.frame(st_coordinates(polyi_sf))
        utmzonei=utmzone(mean(polyi_sf_xy$X,na.rm=TRUE),
                         mean(polyi_sf_xy$Y,na.rm=TRUE))
        polyi_sf_utm=st_transform(
          polyi_sf,crs = paste("+proj=utm +zone=",utmzonei,sep=''))
        #area in km2
        api2=as.numeric(st_area(polyi_sf))
        api2=as.numeric(st_area(polyi_sf_utm))
        # with PBSmapping: deprecated since rgdal retirement
        #polyi2=SpatialPolygons2PolySet(polyi)
        #attr(polyi2, "projection") <- "LL"
        #area in km2
        #api2 = calcArea(polyi2,rollup=1)
        #area in mn2
        #api2$area/(1.852^2)
        #lfs.shpij$AREA.pbs=api2$area/(1.852^2)
        #area in mn2
        akm2=as.numeric(api2/(1000^2))
        lfs.shpij$AREA.pbs=akm2/(1.852^2)
      }else{
        cat('Polygon projection:',projpolyi,'\n')
        cat('Esdu and catches projected into polygon projection','\n')
        projpolyi <- proj4string(polyi)
        esdu.sp.shp <- spTransform(esdu.sp, CRS=CRS(projpolyi))
        peche.sp.shp <- spTransform(peche.sp, CRS=CRS(projpolyi))
        esdu.ini =unlist(over(polygons(polyi),esdu.sp.shp,returnList = TRUE))
        peche.ini =unlist(over(polygons(polyi),peche.sp.shp,returnList = TRUE))                
        api=polyi@polygons[[1]]@Polygons[[1]]@area
        # unprojected area in (false) squared nautical miles 
        api*3600
        # unprojected area in (false) in squared km
        api*(1.852*60)^2
        #with PBSmapping, projection in UTM before computations
        polyi2=SpatialPolygons2PolySet(polyi)
        attr(polyi2, "projection") <- "LL"
        # exact area in km2
        api2 = calcArea(polyi2,rollup=1)
        # exact area in mn2
        api2$area/(1.852^2)
        lfs.shp$AREA.pbs=api2$area/(1.852^2)
      }
    # esdus in polygon
    esdu.inpoly1=data.frame(NESU=ESDUDEV$esdu.id[esdu.ini],
                            TC=ESDUDEV$TC[esdu.ini],
                            sel=!is.na(esdu.ini))
    # esdus outside polygon
    esdu.inpoly2=data.frame(NESU=ESDUDEV[!ESDUDEV$esdu.id%in%
                                           esdu.inpoly1$NESU,'esdu.id'],
                            TC=ESDUDEV[!ESDUDEV$TC%in%esdu.inpoly1$TC,'TC'],
                            sel=FALSE)
    # all esdus, with polygon belongings
    esdu.inpolyi=rbind(esdu.inpoly1,esdu.inpoly2)
    esdu.inpolyi=esdu.inpolyi[order(esdu.inpolyi$NESU),]
    # hauls in polygon      
    peche.inpoly1=data.frame(NOCHAL=xyhauls$NOCHAL[peche.ini],
                             sel=!is.na(peche.ini))
    # hauls outside polygon
    if (length(xyhauls[!xyhauls$NOCHAL%in%
                       peche.inpoly1$NOCHAL,'NOCHAL'])>0){
      peche.inpoly2=data.frame(NOCHAL=xyhauls[!xyhauls$NOCHAL%in%
                                                peche.inpoly1$NOCHAL,'NOCHAL'],
                               sel=FALSE)
    }else{
      peche.inpoly2=data.frame(NOCHAL=0,sel=FALSE)
      peche.inpoly2=peche.inpoly2[FALSE,]
    }
    # all hauls
    peche.inpolyi=rbind(peche.inpoly1,peche.inpoly2)
    peche.inpolyi=peche.inpolyi[order(peche.inpolyi$NOCHAL),]
    
    list(esdu.inpolyi=esdu.inpolyi,peche.inpolyi=peche.inpolyi,
         lfs.shpij=lfs.shpij)
  }
  
  #Function to create CHvalid
	#***********************************-
  CHvalidit=function(B.dev.sp.esdu.df,Pechef,ESDUDEV,tlist=NULL,cCLAS=c('CLAS','ALLS','ALLF'),
                     cSURF=c('SURF','ALLS','ALLF'),haul2Region='geo',peche.inpoly=NULL){
    head(B.dev.sp.esdu.df)
    dim(B.dev.sp.esdu.df)
    length(unique(B.dev.sp.esdu.df$TC))*length(unique(B.dev.sp.esdu.df$DEV))
    CHvalid=unique(B.dev.sp.esdu.df[,c('TC','DEV','CAMPAGNE','charef')])
    names(CHvalid)
    CHvalid$ID_EI='EIT_11_38'
    CHvalid$NOTE=NA
    CHvalid$keep=1
    if (!is.null(tlist)){
      CHvalid[!CHvalid$charef%in%tlist,'keep']=0
    }
    CHvalid=merge(CHvalid,unique(Pechef[,c('NOCHAL','NOSTA','STRATE')]),
      by.x='charef',by.y='NOCHAL')
    head(CHvalid)
    names(CHvalid)=c("NOCHAL","DATEHEURE","DEV","CAMPAGNE","ID_EI","NOTE","keep",
      "NOSTA","DLAY")
    # Adds region belongings
    #***************************
    if (haul2Region=='geo'&!is.null(peche.inpoly)){
      dim(CHvalid)
      chlist.CLAS=peche.inpoly[peche.inpoly$STRATE=='CLAS',c('NOCHAL','STRATE','Region.CLAS')]
      names(chlist.CLAS)=c('NOCHAL','STRATE','Region')
      chlist.SURF=peche.inpoly[peche.inpoly$STRATE=='SURF',c('NOCHAL','STRATE','Region.SURF')]
      names(chlist.SURF)=c('NOCHAL','STRATE','Region')
      chlist=rbind(chlist.CLAS,chlist.SURF)
      CHvalid=merge(CHvalid,chlist,by.x='NOCHAL',by.y='NOCHAL')
      dim(CHvalid)
      cat('Hauls belongings to regions defined based on haul positions','\n')
    }else if (haul2Region=='ref'){
      CHvalid.CLAS=CHvalid[CHvalid$DLAY%in%cCLAS,]
      CHvalid.CLAS=merge(CHvalid.CLAS,unique(ESDUDEV[,c('TC','zonesCLAS')]),
                         by.x='DATEHEURE',by.y='TC')
      names(CHvalid.CLAS)[names(CHvalid.CLAS)=='zonesCLAS']='Region'  
      CHvalid.SURF=CHvalid[CHvalid$DLAY%in%cSURF,]
      CHvalid.SURF=merge(CHvalid.SURF,unique(ESDUDEV[,c('TC','zonesSURF')]),
                         by.x='DATEHEURE',by.y='TC')
      names(CHvalid.SURF)[names(CHvalid.SURF)=='zonesSURF']='Region'
      CHvalid=rbind(CHvalid.SURF,CHvalid.CLAS)
      cat('Hauls belongings to regions defined based on haul:esdu associations','\n')
    }
    names(CHvalid)[names(CHvalid)=='DLAY']='Region'
    CHvalid
  }
 
  #***************************************************
	#Computes mean Sa per deviation and region
	#***************************************************

	meanSa.dev.region=function(EspDevi,EsduDevi,deszonesi=NULL){	
		#Whatever post-stratification... 
		Nz=unique(EspDevi$STRATE)		
		uzclas=unique(EsduDevi$zonesCLAS)
		uzsurf=unique(EsduDevi$zonesSURF)
		if (!TRUE%in%is.na(uzclas)){
      zclas=TRUE
    }else{
      zclas=FALSE
      cat('NA in zones CLAS in echotype definitions','\n')
    }    
		if (!TRUE%in%is.na(uzsurf)){
      zsurf=TRUE
    }else{
      zsurf=FALSE
      cat('NA in zones SURF in echotype definitions','\n')
    }  
		if (zclas){
			lDevCLAS=as.character(unique(EspDevi[EspDevi$STRATE==
				'CLAS','DEV']))
			mEsduDev.CLAS=aggregate(EsduDevi[,lDevCLAS],
				by=list(EsduDevi$zonesCLAS),mean)
			names(mEsduDev.CLAS)=c('Region',lDevCLAS)
		}
		if (zsurf){
			lDevSURF=as.character(unique(EspDevi[EspDevi$STRATE=='SURF','DEV']))
			mEsduDev.SURF=aggregate(EsduDevi[,lDevSURF],
				by=list(EsduDevi$zonesSURF),mean)
			names(mEsduDev.SURF)=c('Region',lDevSURF)
		}
		zonesdev=matrix(rep(0,dim(deszonesi)[1]*
			length(unique(EspDevi$DEV))),ncol=length(unique(
			EspDevi$DEV)))
		dimnames(zonesdev)=list(sort(deszonesi$ID),sort(unique(
			EspDevi$DEV)))
		mEsduDev=data.frame(Region=sort(deszonesi$ID),zonesdev)
		if (zclas){
			mEsduDev[mEsduDev$Region%in%mEsduDev.CLAS$Region,
				c(FALSE,names(mEsduDev)[-1]%in%
				names(mEsduDev.CLAS)[-1])]=
			mEsduDev.CLAS[mEsduDev.CLAS$Region%in%mEsduDev$Region,
				c(FALSE,names(mEsduDev.CLAS)[-1]%in%
				names(mEsduDev)[-1])]
		}
		if (zsurf){
			mEsduDev[mEsduDev$Region%in%mEsduDev.SURF$Region,
				c(FALSE,names(mEsduDev)[-1]%in%
				names(mEsduDev.SURF)[-1])]=
			mEsduDev.SURF[mEsduDev.SURF$Region%in%mEsduDev$Region,
				c(FALSE,names(mEsduDev.SURF)[-1]%in%
				names(mEsduDev)[-1])]
		}
		mEsduDev=merge(mEsduDev,deszonesi[,c('ID','AREA')],
				by.x='Region',by.y='ID')
	mEsduDev
	}

	#******************************************************************************
	#Merge EsduDev and regions ids from Fishview (if !is.null(zones)) (DEFAULT)
	# or baracouda (if !is.null(CHvalid))
	#*************************************

	region2esduDev=function(EsduDev,zones,CHvalid=NULL){

		if (!is.null(zones)){	
			EsduDev=merge(EsduDev,zones[,c('DATEHEURE','zonesCLAS','zonesSURF')],
				by.y='DATEHEURE',by.x='TC',all.x=TRUE)
			dim(EsduDev)
		}else if (!is.null(CHvalid)){	

			#OR regions from Baracouda's 'POST_STRATE' (=post.baras):
			zonesSURF=unique(CHvalid[CHvalid$DEV%in%EspDev[EspDev$STRATE=='SURF','DEV'],
				c('DATEHEURE','Region')])
			names(zonesSURF)[2]='zonesSURF'
			dim(zonesSURF)	
			zonesCLAS=unique(CHvalid[CHvalid$DEV%in%EspDev[EspDev$STRATE=='CLAS','DEV'],
				c('DATEHEURE','Region')])
			names(zonesCLAS)[2]='zonesCLAS'
			dim(zonesCLAS)	
			dim(EsduDev)
			if (dim(zonesCLAS)[1]!=0){
				EsduDev=merge(EsduDev,zonesCLAS,by.x='TC',by.y='DATEHEURE',
				all.x=TRUE)
				dim(EsduDev)
			}else{
				EsduDev$zonesCLAS=0
			}
			if (dim(zonesSURF)[1]!=0){
				EsduDev=merge(EsduDev,zonesSURF,by.x='TC',by.y='DATEHEURE',
				all.x=TRUE)
				dim(EsduDev)
			}else{
			EsduDev$zonesSURF=0
			}
		}
	EsduDev
	}

	#******************************************************************************
	#Xe per codEsp/echotype/region
	#******************************************************************************
k=2
	Xe.k.region=function(k,list.Xei,list.Associ,EspDevi,
		mSa.radiusi,Ndevi,CH.validi=NULL,peche.inpoly=NULL,
		mixHauls=c('ALLS','ALLF'),
		haul2Region='geo') {

		cat('Dev.',Ndevi[k],"\n")
  
		#selection of species in deviation k
		#*****************************--
    #spcodes in echotype k
    EspDev.k=EspDevi[EspDevi$DEV==paste('D',Ndevi[k],sep=''),]
    # set mixHauls depth stratum to echotype depth stratum
    if ('CLAS'%in%unique(EspDev.k$STRATE)){
      EspDev.k[EspDev.k$STRATE%in%mixHauls,'STRATE']='CLAS'
      EspDev.k$CodEsp=paste(EspDev.k$GENR_ESP,EspDev.k$STRATE,EspDev.k$SIGNEP,sep='-')
      EspDev.k=unique(EspDev.k)
    }else if ('SURF'%in%unique(EspDev.k$STRATE)){
      EspDev.k[EspDev.k$STRATE%in%mixHauls,'STRATE']='SURF'
      EspDev.k$CodEsp=paste(EspDev.k$GENR_ESP,EspDev.k$STRATE,EspDev.k$SIGNEP,sep='-')
      EspDev.k=unique(EspDev.k)
    }
    #depth stratum		
    zclass=as.character(unique(EspDev.k$STRATE))
    Regionk=paste('Region',zclass,sep='.')
    #Esdu/echotype/haul/region link
    Assock=list.Associ[[k]]
		names(Assock)[names(Assock)=='Nosta']='NOCHAL'
    Assock$ID=paste(Assock$Esdu,Assock$NOCHAL,Assock[,Regionk])
    if (length(Assock$ID)!=length(unique(Assock$ID))) cat(paste('Dev.',
      Ndevi[k],'Duplicated Esdus/haul/regions'),'\n')  
    #Xe per spcode/haul
    Xek=list.Xei[[k]]
	  names(Xek)[names(Xek)=='Nosta']='NOCHAL'
	  # set mixHauls depth stratum to echotype depth stratum
	  if ('CLAS'%in%unique(EspDev.k$STRATE)){
	    Xek$CodEsp=as.character(Xek$CodEsp)
	    Xek$CodEsp=gsub(mixHauls[1],'CLAS',Xek$CodEsp)
	    Xek$CodEsp=gsub(mixHauls[2],'CLAS',Xek$CodEsp)
	    Xek$CodEsp=gsub('SURF','CLAS',Xek$CodEsp)
	  }else if ('SURF'%in%unique(EspDev.k$STRATE)){
	    Xek$CodEsp=as.character(Xek$CodEsp)
	    Xek$CodEsp=gsub(mixHauls[1],'SURF',Xek$CodEsp)
	    Xek$CodEsp=gsub(mixHauls[2],'SURF',Xek$CodEsp)
	    Xek$CodEsp=gsub('CLAS','SURF',Xek$CodEsp)
	  }
    Xek$ID=paste(Xek$NOCHAL,Xek$CodEsp)
    if (length(Xek$ID)!=length(unique(Xek$ID))) cat(paste('Dev.',Ndevi[k],
      'Duplicated hauls:CodEsp pairs in Xe file'),'\n')
	
		if (!(TRUE%in%(Xek$Xe>0))){
			Xe.res=NULL
		}else{

			#selection of haul selection for deviation k
			#*****************************--
			if (!is.null(CH.validi)){
				CHvalidk=CH.validi[CH.validi$DEV==paste('D',Ndevi[k],
					sep=''),]
				head(CHvalidk)
				lCHk=unique(CHvalidk[,c('NOCHAL','keep','Region')])
				unique(CHvalidk$NOCHAL)
				unique(Xek$NOCHAL)
			}else{
				#in progress...
			}		
			#Check Xe sum
			#aggregate(Xek$ke,list(Xek$NOCHAL),sum)	

      #merge all files
			#************************************* 
      if (!T%in%(Xek$NOCHAL%in%Assock$NOCHAL)){
          stop (paste('No reference haul associated with echotype:',k))
      }
      if (!is.null(peche.inpoly)&haul2Region=='geo'){
        #Add region to Xes per haul/echotype
        # based on haul geographical positions
        length(Xek$ID);length(unique(Xek$ID))
  		  Xek=merge(Xek,peche.inpoly[,c('NOCHAL',Regionk)],by.x='NOCHAL',
            by.y='NOCHAL')
  		  #selection of weights (mean Sa per deviation around hauls)
  		  #*********************************************
  		  w=mSa.radiusi[[k]]
  		  names(w)[names(w)=='Nosta']='NOCHAL'
  		  head(w)
  		  if(length(w$NOCHAL)!=length(unique(w$NOCHAL))){
  		    cat(paste('Dev.',Ndevi[k],'Duplicated hauls in sA weights file'))
  		  } 
  		  w=merge(w,unique(peche.inpoly[,c('NOCHAL',Regionk)]),
  		          by.x='NOCHAL',by.y='NOCHAL')
  		  head(w)
  		  cat('Belonging of hauls to regions defined based on haul position','\n')
      }else{    
        #Add region to Xes per haul/echotype, based on esdu:haul associations
        length(Xek$ID);length(unique(Xek$ID))
			  Xek=merge(Xek,unique(Assock[,c('NOCHAL',Regionk)]),
				  by.x='NOCHAL',by.y='NOCHAL')
        length(Xek$ID);length(unique(Xek$ID))
        #selection of weights (mean Sa per deviation around hauls)
        #*********************************************
        w=mSa.radiusi[[k]]
        names(w)[names(w)=='Nosta']='NOCHAL'
        if(length(w$NOCHAL)!=length(unique(w$NOCHAL))){
          cat(paste('Dev.',Ndevi[k],'Duplicated hauls in sA weights file'))
        } 
        w=merge(w,unique(Assock[,c('NOCHAL',Regionk)]),
                by.x='NOCHAL',by.y='NOCHAL')
        cat('Belonging of hauls to regions defined based on haul:esdu association','\n')
      }  
      #Add sA weights to Xes per haul/echotype
			unique(Xek[,c('NOCHAL',Regionk)])
			unique(w[,c('NOCHAL',Regionk)])
      Xek=merge(Xek,w,by.x=c('NOCHAL',Regionk),
			  by.y=c('NOCHAL',Regionk))
      length(Xek$ID);length(unique(Xek$ID))  
			#adds selected hauls
# 			Xek=merge(Xek,lCHk,by.x=c('NOCHAL',Regionk),
# 			  by.y=c('NOCHAL','Region'))
#       length(Xek$ID);length(unique(Xek$ID))
      #!!!!!!!!!!!!!!!!!!!!!!! For now, all hauls are selected by default, CHvalidit function needs some polishing before using CHvalid !!!!!!!
      Xek$keep=1
      #dcsp.inregion=aggregate(Xek$ID,list(Xek[,Regionk]),duplicated)
  		#names(dcsp.inregion)=c(Regionk,'dcsp')
      
			#compute total mean Sa per region around hauls
			#************************************* 
			#mSatot1=aggregate(Xek$mSa,list(Xek[,Regionk],Xek$CodEsp),unique)
  		#names(mSatot1)=c(Regionk,'NOCHAL','uSa')
      mSatot=aggregate(Xek[,c('mSa')],list(Xek[,Regionk],
                                                     Xek$CodEsp),sum)
			names(mSatot)=c(Regionk,'CodEsp','mSatot')
			#mean weights
			#************************************* 
			Xek=merge(Xek,mSatot,by.x=c(Regionk,'CodEsp'),by.y=c(Regionk,'CodEsp'))
			Xek$wE=Xek$mSa/Xek$mSatot
			Xek$Xei=Xek$Xe*Xek$wE
      Xek$mwi=Xek$mweight*Xek$wE
      head(Xek)
      #check that weights sum to unity
      aggregate(Xek$wE,list(Xek[,Regionk],Xek$CodEsp),sum)
			#discards unselected hauls		
			#************************************* 
			Xeks=Xek[Xek$keep==1,]
      unique(Xek$Region.CLAS)
      unique(Xeks$Region.CLAS)
      unique(Xek$NOCHAL)
      unique(Xeks$NOCHAL)
			#Average of Xe's per codEsp and region, weighted by mean SA
			#around hauls
  		#*************************************
      #NA removed to discard regions with hauls surrounded by no ESDUs with
      #echotype k SA (i.e. for which Satot=0 and Xei=NaN)
      #*************************************
      #weighted average of Xe per region/sp code based on all hauls
			wmXek=aggregate(Xek[,c('Xei','mwi')],
				by=list(Xek[,Regionk],Xek$CodEsp),sum,na.rm=TRUE)
			names(wmXek)=c(Regionk,'CodEsp','wmXe','wmweight')
      #weighted average of Xe per region/sp code after hauls selection
      wmXeks=aggregate(Xeks[,c('Xei','mwi')],
				by=list(Xeks[,Regionk],Xeks$CodEsp),sum,na.rm=TRUE)
			names(wmXeks)=c(Regionk,'CodEsp','wmXes','wmweights')
      #Merge Xes  per region/sp code    
			wmXek=merge(wmXek,wmXeks,by.x=c(Regionk,'CodEsp'),
          by.y=c(Regionk,'CodEsp'))

      #Check for No. and unicity of averaged hauls
      #no. and list of hauls used in Xe per spcode/region computation (unique/duplicated?)      
      is.duplicated=function(x){
        #dupli=paste(x[duplicated(x)],collapse=',')
        dupli=T%in%duplicated(x)
        dupli
      }
      mXek.dhauls=aggregate(Xek$NOCHAL,by=list(Xek[,Regionk],
        Xek$CodEsp),is.duplicated)
      names(mXek.dhauls)=c(Regionk,'CodEsp','duplihauls')  
      mXek.Nhauls=aggregate(Xek$NOCHAL,by=list(Xek[,Regionk],Xek$CodEsp),
        length)
      names(mXek.Nhauls)=c(Regionk,'CodEsp','Nhauls')
      mXek.haul.check=merge(mXek.Nhauls,mXek.dhauls)

      #Simple Average of Xe's per codEsp and region
			#*************************************
      #haul/spcode pairs unique? -> no because one haul can be associated 
      #with several regions
      length(Xek$ID);length(unique(Xek$ID))
      Xek$ID2=paste(Xek$NOCHAL,Xek$CodEsp,Xek[,Regionk])  
      length(Xek$ID2);length(unique(Xek$ID2))
      #simple average of Xe per region/sp code based on all hauls
      mXek=aggregate(Xek[,c('Xe','mweight')],by=list(Xek[,Regionk],
                                                     Xek$CodEsp),mean)
			names(mXek)=c(Regionk,'CodEsp','mXe','mweight')
      #simple average of Xe per region/sp code after hauls selection
      mXeks=aggregate(Xeks[,c('Xe','mweight')],by=list(Xeks[,Regionk],
                                                       Xeks$CodEsp),mean)
			names(mXeks)=c(Regionk,'CodEsp','mXes','mweights')
      #Merge simple averages of Xe per region/sp code
			mXek=merge(mXek,mXeks,by.x=c(Regionk,'CodEsp'),
          by.y=c(Regionk,'CodEsp'))
      
      #Merge simple and weighted averages of Xe per region/sp code
  		#*************************************
      Xe.res=merge(wmXek,mXek,by.x=c(Regionk,'CodEsp'),
        by.y=c(Regionk,'CodEsp'))
      Xe.res=merge(Xe.res,mXek.haul.check,by.x=c(Regionk,'CodEsp'),
        by.y=c(Regionk,'CodEsp'))  
	}		
	list(Xe.res=Xe.res,Xek=Xek)
	}

	#******************************************************************************
	#Biomass per codEsp/deviation/region
	#******************************************************************************

	Biom.Dev.region=function(k,list.Xe.regionsi,mEsduDevi,Ndevi,EspDevi,mixHauls=c('ALLS','ALLF')) {

		cat('Dev.',Ndevi[k],"\n")
	  
		#selection of species in deviation k
		Dk=paste('D',Ndevi[k],sep='')
		EspDev.k=EspDevi[EspDevi$DEV==Dk,]
		# set mixHauls depth stratum to echotype depth stratum
		if ('CLAS'%in%unique(EspDev.k$STRATE)){
		  EspDev.k[EspDev.k$STRATE%in%mixHauls,'STRATE']='CLAS'
		  EspDev.k$CodEsp=paste(EspDev.k$GENR_ESP,EspDev.k$STRATE,EspDev.k$SIGNEP,sep='-')
		  EspDev.k=unique(EspDev.k)
		}else if ('SURF'%in%unique(EspDev.k$STRATE)){
		  EspDev.k[EspDev.k$STRATE%in%mixHauls,'STRATE']='SURF'
		  EspDev.k$CodEsp=paste(EspDev.k$GENR_ESP,EspDev.k$STRATE,EspDev.k$SIGNEP,sep='-')
		  EspDev.k=unique(EspDev.k)
		}
		zclass=as.character(unique(EspDev.k$STRATE))
		Xer.k=list.Xe.regionsi[[k]]$Xe.res
		Regionk=paste('Region',zclass,sep='.')	
    
    if (!is.null(Xer.k)){
		  Biomk=merge(mEsduDevi[,c('Region',Dk,'AREA')],Xer.k,
			  by.x='Region',by.y=Regionk)
	  	Biomk$wbiom=Biomk[,Dk]*Biomk$wmXe*Biomk$AREA
	  	Biomk$biom=Biomk[,Dk]*Biomk$mXe*Biomk$AREA
	  	Biomk$wbioms=Biomk[,Dk]*Biomk$wmXes*Biomk$AREA
	  	Biomk$bioms=Biomk[,Dk]*Biomk$mXes*Biomk$AREA
		  Biomk$wabun=Biomk$wbiom/Biomk$wmweight
		  Biomk$abun=Biomk$biom/Biomk$mweight
		  Biomk$wabuns=Biomk$wbioms/Biomk$wmweights
		  Biomk$abuns=Biomk$bioms/Biomk$mweights
    }else{
      Biomk=mEsduDevi[,c('Region',Dk,'AREA')]
      Biomk$CodEsp='COMP-LEM-SURF-0'
      Biomk$wmXe=Biomk$wmXes=Biomk$mXe=Biomk$mXes=NA
      Biomk$wbiom=Biomk$biom=Biomk$wbioms=Biomk$bioms=0
      Biomk$wabun=Biomk$abun=Biomk$wabuns=Biomk$abuns=0
    }  
	Biomk
	}

	#******************************************************************************
	#Biomass per codEsp/region
	#******************************************************************************
i=4
	Biom.region=function(BiomDev.regionsi,Ndevi,EspDevi,Deszonesi,mixHauls=c('ALLS','ALLF')){
		N=length(Ndevi)
		for (i in 1:N){
		  Dk=paste('D',Ndevi[i],sep='')
      cat(paste('D',Ndevi[i],sep=''),'\n')
			Devi=BiomDev.regionsi[[i]]
			EspDev.k=EspDevi[EspDevi$DEV==Dk,]
			# set mixHauls depth stratum to echotype depth stratum
			if ('CLAS'%in%unique(EspDev.k$STRATE)){
			  EspDev.k[EspDev.k$STRATE%in%mixHauls,'STRATE']='CLAS'
			  EspDev.k$CodEsp=paste(EspDev.k$GENR_ESP,EspDev.k$STRATE,EspDev.k$SIGNEP,sep='-')
			  EspDev.k=unique(EspDev.k)
			}else if ('SURF'%in%unique(EspDev.k$STRATE)){
			  EspDev.k[EspDev.k$STRATE%in%mixHauls,'STRATE']='SURF'
			  EspDev.k$CodEsp=paste(EspDev.k$GENR_ESP,EspDev.k$STRATE,EspDev.k$SIGNEP,sep='-')
			  EspDev.k=unique(EspDev.k)
			}
			Devi$LAYER=unique(EspDev.k[,'STRATE'])
			Devi$NDEV=paste('D',Ndevi[i],sep='')
			names(Devi)[2]='Dev'
			if (i==1) {
				Biom.region.dbi=Devi

			}else{
				Biom.region.dbi=rbind(Biom.region.dbi,Devi)
			}
		}
		names(Biom.region.dbi)
		Biom.region.dbi=merge(Biom.region.dbi,unique(EspDevi[,
			c('GENR_ESP','CodEsp')]),by.x='CodEsp',by.y='CodEsp')
		names(Biom.region.dbi)	
		Biom.sp.regioni=aggregate(Biom.region.dbi[,c('biom','wbiom',
      'bioms','wbioms','abun','wabun','abuns','wabuns')],
			list(Biom.region.dbi$Region,Biom.region.dbi[,'GENR_ESP']),sum,na.rm=TRUE)
		names(Biom.sp.regioni)[1:2]=c('Region','sp')
		Biom.spi=aggregate(Biom.sp.regioni[,c('biom','wbiom','bioms','wbioms',
		                                      'abun','wabun','abuns','wabuns')],
        list(Biom.sp.regioni$sp),sum,na.rm=TRUE)
		names(Biom.spi)[1]=c('sp')
		Biom.sp.regioni=merge(Biom.sp.regioni,Deszonesi[,c('ID','TYPE')],
			by.x='Region',by.y='ID')	
	
	list(Biom.region.db.ALL=Biom.region.dbi,
    Biom.sp.region.ALL=Biom.sp.regioni,Biom.sp.ALL=Biom.spi)
	}
  
  #***************************************************************
  #Plots
  #***************************************************************
  
   polygon.simple.bubbleplot=function(esdu.sp.UTM,lpolys,z,cexi=0.1,
                                      scalef=1,zlab,rlab,tmain,
                                      polylab=NULL,...){
    plot(esdu.sp.UTM,col='white')
    N=length(lpolys)
    for (i in 1:N){
        plot(lpolys[[i]],add=TRUE)
        centroids <- coordinates(lpolys[[i]])
        x <- centroids[,1]
        y <- centroids[,2]
        if (is.null(polylab)) polylab=seq(N)
        if (polylab[i]%in%z[,rlab]){
          points(x,y,cex=cexi+z[z[,rlab]==polylab[i],zlab]/scalef,pch=16)
          #,...)
        }
        if (!is.null(polylab)){
          text(x,y,polylab[i],col='grey50')
        }  
        mtext(tmain)
    }
  }
  
  #more powerful but doesn't work yet...
  
   polygon.bubbleplot=function(esdu.sp.UTM,polys,z,zlab,scalef,tmain,...){
    par(mfrow=c(1,2))
    #plot(esdu.sp.UTM,col='white',main='Anchovy CLAS biomass')
    for (i in 1:7){
        #plot(polys[[i]],add=TRUE)
        centroids <- coordinates(polys[[i]])
        dfi=data.frame(x=centroids[,1],y=centroids[,2],z=z[z$Region==i,
            c('Region',zlab)])
        if (i==1) df=dfi
        else df=rbind(df,dfi)
    }
    coordinates(df) <- c("x", "y") # promote to SpatialPointsDataFrame
    bubble(df, paste('z',zlab,sep='.'), maxsize = 2.5, main = tmain,
      key.entries = 2^(-1:4))
   #plot(esdu.sp.UTM,col='white',main='Anchovy CLAS biomass')
    for (i in 1:7){
        plot(polys[[i]],add=TRUE)
    }
  }

  esdu.regions.check=function(ESDUDEV){
    if (T%in%is.na(ESDUDEV$zonesCLAS)) {
      cat(as.character(ESDUDEV[is.na(ESDUDEV$zonesCLAS),'TC']),
                                         'CLAS ESDU without regions label')
    }
    if (T%in%is.na(ESDUDEV$zonesSURF)) {
      cat(as.character(ESDUDEV[(ESDUDEV$depth>50)&(is.na(ESDUDEV$zonesSURF)),'TC']),
                                         'SURF ESDU without regions label')
    }
    esdu.naregions=ESDUDEV[is.na(ESDUDEV$zonesCLAS)|(ESDUDEV$depth>50)&(is.na(ESDUDEV$zonesSURF)),]
    esdu.naregions
  }
  
  poly2wgs84LongLat=function(polys){
  #convert polygons coordinates to WGS84 unprojected coordinates:
  for (i in 1:length(polys)){
    polys.wgs84i=spTransform(polys[[i]],CRS("+proj=longlat +datum=WGS84"))
    poly.dfi=data.frame(region=i,polys.wgs84i@polygons[[1]]@Polygons[[1]]@coords)
    names(poly.dfi)=c('region','x','y')
    if (i==1){
      polys.wgs84=list(polys.wgs84i)
      poly.df=poly.dfi
    }else{
      polys.wgs84=c(polys.wgs84,list(polys.wgs84i))
      poly.df=rbind(poly.df,poly.dfi)
    }  
  }
  length(polys.wgs84)
  polys.wgs84
  }

  
survey.poly.proc=function(polys,IDs,simplify=FALSE,alpha=1){
  library(sf)

  for (i in 1:length(polys)){
    polyi=polys[[i]]
    poly.dfi=data.frame(IDs[i],polys[[i]]@polygons[[1]]@Polygons[[1]]@coords,polys[[i]]@polygons[[1]]@Polygons[[1]]@area)
    names(poly.dfi)=c('region','x','y','area')
    polyi=spChFIDs(polyi,paste('p',i))
    # compute true areas with sf    
    polyi_sf=st_as_sf(polyi)
    # convert to UTM to get same results as PBSmapping
    polyi_sf_xy=data.frame(st_coordinates(polyi_sf))
    utmzonei=utmzone(mean(polyi_sf_xy$X,na.rm=TRUE),
                     mean(polyi_sf_xy$Y,na.rm=TRUE))
    polyi_sf_utm=st_transform(
      polyi_sf,crs = paste("+proj=utm +zone=",utmzonei,sep=''))
    #area in km2
    api2=as.numeric(st_area(polyi_sf))/1000^2
    api2=as.numeric(st_area(polyi_sf_utm))/1000^2
    poly.dfi$AREA.pbs=api2/(1.852^2)
    # compute true areas with PBSmapping
    # polyi2=SpatialPolygons2PolySet(polyi)
    # attr(polyi2, "projection") <- "LL"
    # # exact area in km2
    # api2 = calcArea(polyi2,rollup=1)
    # # exact area in mn2
    # api2$area/(1.852^2)
    # poly.dfi$AREA.pbs=api2$area/(1.852^2)
    if (i==1){
      pall=polyi
      poly.dfa=poly.dfi
    }else{
      names(polyi)=names(pall)
      pall=rbind(pall,polyi)
      poly.dfa=rbind(poly.dfa,poly.dfi)
    }
  }
  # aggregates polygons with sf (instead of maptools)
  pols_sf <- st_as_sf(pall)
  survey.poly.sf=st_union(pols_sf)
  survey.poly=as(survey.poly.sf, "Spatial")
  #survey.poly=unionSpatialPolygons(pall,IDs=rep(1,length(polys)),avoidGEOS=FALSE)
  #length(pall@polygons[[1]]@Polygons)
  #length(survey.poly)
  #length(polys)
  par(mfrow=c(2,2))
  plot(pall,main='Original polygons set')
  plot(survey.poly,main='Merged polygon')
  #
  polyi_sf=st_as_sf(survey.poly)
  # convert to UTM to get same results as PBSmapping
  polyi_sf_xy=data.frame(st_coordinates(polyi_sf))
  utmzonei=utmzone(mean(polyi_sf_xy$X,na.rm=TRUE),
                   mean(polyi_sf_xy$Y,na.rm=TRUE))
  polyi_sf_utm=st_transform(
    polyi_sf,crs = paste("+proj=utm +zone=",utmzonei,sep=''))
  #area in km2
  api2=as.numeric(st_area(polyi_sf))/1000^2
  api2=as.numeric(st_area(polyi_sf_utm))/1000^2
  # with PBSmapping: deprecated since rgdal retirement
  #polyi2=SpatialPolygons2PolySet(polyi)
  #attr(polyi2, "projection") <- "LL"
  #area in km2
  #api2 = calcArea(polyi2,rollup=1)
  # "The function automatically converts
  # longitude-latitude coordinates to UTM before calculating the area."
  #area in mn2
  #api2$area/(1.852^2)
  #lfs.shpij$AREA.pbs=api2$area/(1.852^2)
  #area in mn2
  as.numeric(api2/(1.852^2))
  
  # select large polygons and remove holes
  # list of polygons in unioned polygon
  lps=survey.poly@polygons[[1]]@Polygons
  Nps=length(lps)
  if(Nps>1){
    ishole=rep(FALSE,Nps)
    for (i in 1:Nps){
      #extract non hole polygons
      #plot(survey.poly@polygons[[1]]@Polygons[[i]]@coords,main=paste('poly',i),type='l')
      if (survey.poly@polygons[[1]]@Polygons[[i]]@hole){
        ishole[i]=TRUE
        #cat('Poly',i,'is hole','\n')
      }
    }
  }
  

  # check that unionned polygon is unique
  if ((sum(!ishole)>1)|simplify){
    #extract all polygon vertices
    Np=seq(length(ishole))[!ishole]
    projpolyi <- proj4string(survey.poly)

    for (i in 1:length(Np)){
      xyi=survey.poly@polygons[[1]]@Polygons[[Np[i]]]@coords
      if (i==1){xys=xyi
      }else xys=rbind(xys,xyi)
    }
    #
    #plot(xys,main='Merged polygon outer vertices')
    library(alphahull)
    #remove duplicated data points
    xys=unique(xys)
    xx=xys[,1]
    yy=xys[,2]
    apoly=ahull(xx,yy,alpha=alpha)
    #
    plot(apoly,main='Merged polygon outer vertices and alpha hull')
    #names(apoly)
    #extract and plot alphahull contour vertices
    # The arcs defining the boundary of the alpha-convex hull are ordered,
    # so one must use the arcs ordering to extract the ordered vertices
    apolyContour=apoly$arcs[,7]
    apolyc=cbind(xx[apolyContour],yy[apolyContour])
    #plot(apolyc[,1],apolyc[,2])
    #lines(apolyc[,1],apolyc[,2])
    cat('Disjunct polygons: alpha hull used to approximate their outer limits','\n')
    (areaf=areaahull(apoly)*3600)
    #stop('More than 1 unionned polygon')
    Sr1 = Polygon(apolyc)
    Srs1 = Polygons(list(Sr1), "s1")
    # Convert to spatial polygon with polygon projection
    survey.poly = SpatialPolygons(list(Srs1),proj4string=CRS(projpolyi))
  } 
  # extract coords and area
  ps=lps[!ishole]
  poly.df0=data.frame(0,ps[[1]]@coords,ps[[1]]@area)
  names(poly.df0)=c('region','x','y','area')
  #poly.df0$AREA.pbs=api2$area/(1.852^2)
  poly.df0$AREA.pbs=api2/(1.852^2)
  poly.dfa$region=as.character(poly.dfa$region)
  poly.dfa=rbind(poly.dfa,poly.df0)
  plot(survey.poly,main='Survey polygon')
  par(mfrow=c(1,1))
  
  list(survey.poly=survey.poly,poly.dfa=poly.dfa)                                                                    
}

# regions.autodef ----

regions.autodef=function(
  pcatch.all,Ncol.fish,Ncol.xy,Nc2,balpha,
  SurveyPolygon,export.path=NULL,pID=NULL,
  fish.palette=c('brown2','green','grey','pink','blue','orange','red',
                 'black','white','yellow'),pie.radius=.1,poly.export=TRUE){

  data("PELGASpolygon")
  #library(terra)
  library(sf)
  library(scatterpie)
  library(ggnewscale)
  
  # VI.1.2. Spatial clustering of catches proportions (unWeighted) ----
  #*************************************************
  #library(const.clust)
  names(pcatch.all)
  
  # VI.1.2.1. CLAS hauls clustering ----------
  D0 <- dist(pcatch.all[,Ncol.fish])   # distance between catches proportions
  D1 <- dist(pcatch.all[,Ncol.xy])   # geographic distance between haul locations
  
  #Nc2=5
  x11(bg='white')
  cr <- choicealpha(D0, D1, range.alpha=seq(0, 1, 0.1), K=Nc2, 
                    graph=TRUE)
  if (!is.null(export.path)){
    dev.print(device=png,
              filename=paste(
                export.path,cruise,"_",pID,"_regionsAutodef_spatClustDiag.png",sep=''),
              width=800,height=800)
  }
  cr$Q # proportion of explained pseudo-inertia
  cr$Qnorm # normalized proportion of explained pseudo-inertia
  # choose best alpha
  #"The mixing parameter alpha sets the importance of the 
  # constraint in the clustering procedure."
  #balpha=0.8
  tree.clas <- hclustgeo(D0, D1, alpha=balpha)
  x11()
  plot(tree.clas,labels=FALSE)
  gcclust.clas <- cutree(tree.clas, Nc2)
  gcclust.clascut=rect.hclust(tree.clas,Nc2)
  pcatch.all$gcclust=gcclust.clas
  if (!is.null(export.path)){
    dev.print(device=png,
              filename=paste(
                export.path,cruise,"_",pID,"_regionsAutodef_tree.png",sep=''),
              width=800,height=800)
  }
  # VI.1.2.1.1. CLAS hauls clusters mean composition ----
  #head(pcatch.all)
  mhgcclustp=aggregate(pcatch.all[,Ncol.fish],
                       list(pcatch.all$gcclust),mean)
  mhgcclustp.cs=apply(mhgcclustp,2,sum)
  # Remove species with null catches
  mhgcclustp=mhgcclustp[,mhgcclustp.cs>0]
  tmhgcclustp=t(as.matrix(mhgcclustp[,-1]))
  dimnames(tmhgcclustp)[[2]]=row.names(mhgcclustp[,-1])
  x11()
  par(mar=c(5,4,5,9),bg='white')
  barplot(tmhgcclustp,legend.text=names(mhgcclustp[,-1]),
          names.arg=,col=fish.palette,
          args.legend=list(x='topleft',inset = c(1, 0)),
          xlab='Cluster',main=pID)
  if (!is.null(export.path)){
    dev.print(device=png,
              filename=paste(
                export.path,cruise,"_",pID,"_regionsAutodef_mcompoBarplot.png",sep=''),
              width=800,height=600)
  }
  # VI.1.2.1.2. CLAS hauls clusters spatial distribution ----
  # x11()
  # plot(pcatch.all$LONF,pcatch.all$LATF,col=pcatch.all$gcclust,pch=pcatch.all$gcclust,xlab='',
  #      ylab='',
  #      main='Hierarchical clustering with geographical constraints',
  #      cex=2)
  # legend('bottomleft',legend=unique(pcatch.all$gcclust),
  #        col=unique(pcatch.all$gcclust),pch=unique(pcatch.all$gcclust))
  # coast()
  
  if (poly.export){
    # VI.1.2.1.3. CLAS regions creation, based on haul clustering ----
    #****************************
    # Bouding box definition
    pp.bbox=c(min(SurveyPolygon$x),min(SurveyPolygon$y),
              max(SurveyPolygon$x),max(SurveyPolygon$y))
    pp.bbox.matrix=rbind(pp.bbox[1:2],c(pp.bbox[1],pp.bbox[4]),
                         c(pp.bbox[3],pp.bbox[2]),pp.bbox[3:4],pp.bbox[1:2])
    #plot(pp.bbox.matrix[,1],pp.bbox.matrix[,2])
    # pp.bbox=c(min(pcatch.all[,Ncol.xy[2]])-.1,min(pcatch.all[,Ncol.xy[1]])-.1,
    #           max(pcatch.all[,Ncol.xy[2]])+.1,max(pcatch.all[,Ncol.xy[1]])+.1)
    
    # Create Voronoi polygons around centroids from catches/spatial clustering
    #*********************
    # with rgeos: deprecated
    #pclas.voro <- voronoi(pcatch.all[,Ncol.xy],ext=pp.bbox)
    
    # with terra package: NOT used due to this bug: https://github.com/rspatial/terra/issues/164
    # pclas.voro.t <- terra::voronoi(pcatch.all[,Ncol.xy],bnd=pp.bbox)
    # crs(pclas.voro.t) <- "epsg:4326"
    # class(pclas.voro.t)
    #plot(pclas.voro.t)
    # with sf package
    
    pcatch.all.sf = st_as_sf(pcatch.all,coords=Ncol.xy)
    # compute Voronoi polygons:
    pclas.voro.sf.mpols = st_collection_extract(
      st_voronoi(do.call(c, st_geometry(pcatch.all.sf))))
    # match them to points:
    pcatch.all.sf$pols = pclas.voro.sf.mpols[
      unlist(st_intersects(pcatch.all.sf, pclas.voro.sf.mpols))]
    # plot(st_set_geometry(pcatch.all.sf, "pols")["gcclust"], 
    #      reset = FALSE)
    # plot(st_geometry(pcatch.all.sf), add = TRUE)
    pcatch.all.sf$geometry=pcatch.all.sf$pols
    st_crs(pcatch.all.sf) <- "epsg:4326" 
    #plot(pcatch.all.sf)
    #plot(pclas.voro.sf.pols)
    #points(pcatch.all[,Ncol.xy[1]],pcatch.all[,Ncol.xy[2]],pch=16)
    #coast()
    
    # Trick: apply buffer with dist=0 to make spatial objects valid
    #*********************
    # with rgeos: deprecated
    #pclas.voro  <- gBuffer(pclas.voro , byid=TRUE, width=0)
    #sum(gIsValid(pclas.voro, byid=TRUE)==FALSE)
    
    # with terra
    #pclas.voro.t  <- buffer(pclas.voro.t , width=0)
    
    # with sf
    pcatch.all.sf  <- st_buffer(pcatch.all.sf , dist=0)
    pcatch.all.sf=st_make_valid(pcatch.all.sf)
    st_is_valid(pcatch.all.sf)
    
    # Create polygon objects based on PELGAS polygon
    #*********************
    # With sp
    PELGASpoly.sp= SpatialPolygons(
      list(Polygons(list(Polygon(coords=SurveyPolygon)),
                    ID=c("c"))),
      proj4string=CRS("+proj=longlat +datum=WGS84"))
    
    # with terra
    # crdref <- "+proj=longlat +datum=WGS84"
    # lonlat <- cbind(id=1, part=1, SurveyPolygon$x, SurveyPolygon$y)
    # PELGASpoly.v <- vect(lonlat, type="polygons", crs=crdref)
    # #plot(PELGASpoly.v)
    # # buffer PELGAS polygon with dist=0 to make it valid
    # # cf. https://r-spatial.org/r/2017/03/19/invalid.html
    # PELGASpoly.v  <- buffer(PELGASpoly.v , width=0)
    
    # with sf
    PELGASpoly.sf=st_as_sf(PELGASpoly.sp)
    st_crs(PELGASpoly.sf) <- "epsg:4326" 
    #plot(pclas.voro.sf)
    st_is_valid(PELGASpoly.sf)
    # buffer PELGAS polygon with dist=0 to make it valid
    # cf. https://r-spatial.org/r/2017/03/19/invalid.html
    sf_use_s2(FALSE)
    PELGASpoly.sf  <- st_buffer(PELGASpoly.sf , dist=0)
    sf_use_s2(TRUE)
    st_is_valid(PELGASpoly.sf)
    
    # Intersect clusters voronoi polygons with survey polygon
    #*********************
    # with rgeos: deprecated
    # PELGASpoly.sp  <- gBuffer(PELGASpoly.sp , byid=TRUE, width=0)
    # sum(gIsValid(PELGASpoly.sp, byid=TRUE)==FALSE)
    # pclas.voro.clipped=gIntersection(pclas.voro, PELGASpoly.sp, 
    #                                  byid = T)
    
    # with terra: R crashes with unbuffered PELGAS polygon 
    # pclas.voro.clipped.t=crop(pclas.voro.v, PELGASpoly.v)
    # crs(pclas.voro.clipped.t) <- "epsg:4326"
    # plot(pclas.voro.clipped.t)
    #pclas.voro.clipped=cover(pclas.voro.v, PELGASpoly.v)
    
    # with sf
    #sf_use_s2(FALSE)
    pcatch.all.sf.clipped=st_intersection(pcatch.all.sf,PELGASpoly.sf)

    #plot(pcatch.all.sf.clipped$geometry)
    #plot(pclas.voro.sf.pols)
    #points(pcatch.all[,Ncol.xy[1]],pcatch.all[,Ncol.xy[2]],pch=16)
    
    # Union clipped clusters voronoi polygons
    #*********************
    # With maptools: deprecated
    # packages to install to use the "unionSpatialPolygons" function 
    #install.packages("rgeos")
    #install.packages("gpclib")
    # gpclibPermit() and rgeosStatus() should be TRUE
    # pclas.clustered=unionSpatialPolygons(pclas.voro.clipped,
    #                                      IDs=pcatch.all$gcclust)
    # pclas.clustered.spdf= SpatialPolygonsDataFrame(
    #   pclas.clustered,
    #   data=data.frame(poly.names=row.names(pclas.clustered),
    #                   row.names=row.names(pclas.clustered)))
    # proj4string(pclas.clustered.spdf) <- CRS("+proj=longlat +datum=WGS84")
    
    # with terra:
    # pclas.clustered.t=terra::aggregate(pclas.voro.clipped.t)
    # crs(pclas.clustered.t) <- "epsg:4326"
    
    # with sf:
    pclas.clustered.sf=aggregate(pcatch.all.sf.clipped[,c('geometry')],
                                 by=list(pcatch.all.sf.clipped$gcclust),unique)
    
    names(pclas.clustered.sf)[1]='ggclust'
    #plot(pclas.clustered.sf)
    st_crs(pclas.clustered.sf) <- "epsg:4326" 
    
    # Ncol.fish2=seq(
    #   length(names(pcatch.all.sf.clipped)))[
    #     substr(names(pcatch.all.sf.clipped),1,3)=='pPT']
    # names(pcatch.all.sf.clipped)
    # pclas.clustered.sf.mpPTsp=aggregate(pcatch.all.sf.clipped[,Ncol.fish2],
    #                              by=list(pcatch.all.sf.clipped$gcclust),mean)
    # names(pclas.clustered.sf.mpPTsp)[1]='ggclust'
    # #plot(pclas.clustered.sf)
    # st_crs(pclas.clustered.sf.mpPTsp) <- "epsg:4326"
    
    # Use terra objects
    # poly.clustered.spdf=pclas.clustered.t
    # points.voro.clipped=pclas.voro.clipped.t

    # Use sf objects
    poly.clustered.spdf=pclas.clustered.sf
    points.voro.clipped=pcatch.all.sf.clipped
    class(points.voro.clipped)
    
    # VI.1.2.1.4. Check CLAS regions vs. hauls composition ----
    
    #spcol=c('brown2','green','grey','pink','blue','orange','red','black','white','yellow')
    
    pcatch.all.long=reshape(data=pcatch.all[,c(Ncol.xy,Ncol.fish)],
                           idvar = c("LONF","LATF"),
                           varying = list(3:dim(pcatch.all[
                             ,c(Ncol.xy,Ncol.fish)])[2]),
                           v.names = "value", direction = "long",
                           timevar='sp', 
                           times=gsub('pPT.','',names(pcatch.all[,Ncol.fish])))
    x11(bg='white')
    #plot(poly.clustered.spdf)
    #plot(pcatch.all.long$LONF,y=pcatch.all.long$LATF,type='n')
    # draw.pie(xyz$x, xyz$y, xyz$z,
    #         radius=.5,col=fish.palette,scale=TRUE) -> does not work

    p=ggplot(poly.clustered.spdf) +geom_sf(aes(fill = factor(ggclust)))+
      scale_fill_discrete(name="Cluster")+
      new_scale_fill()+ annotation_map(map_data("world"))+
      geom_scatterpie(aes(x=LONF,y=LATF,r=pie.radius),
                      data=pcatch.all,cols=names(pcatch.all)[Ncol.fish],
                      long_format=FALSE)+
      #geom_scatterpie(aes(x=LONF,y=LATF,r=pie.radius),
      #                 data=pcatch.all.long,cols="sp",long_format=TRUE)+
      scale_fill_manual(values = fish.palette,name='Species')+
      coord_sf(default_crs = sf::st_crs(4326))
    print(p)
    if (!is.null(export.path)){
      dev.print(device=png,
                filename=paste(
                  export.path,cruise,"_",pID,"_regionsAutodef_map.png",sep=''),
                width=800,height=800)
    }
    
    # Deprecated: does not work with sf plots
    # pie.xy(x=pcatch.all[,Ncol.xy[1]],y=pcatch.all[,Ncol.xy[2]],
    #        z=pcatch.all[,Ncol.fish],pcol=fish.palette,
    #        mtitle='Trawl haul composition',
    #        smax=.1,pradius=2,bar=F,#aspi=1/cos(46 * pi/180),
    #        addit=TRUE,ux11=FALSE)
    # text(points.voro.clipped, "id", halo=TRUE)
    # legend('bottomleft',legend=gsub("pPT.","",names(pcatch.all[,Ncol.fish])),
    #        fill=fish.palette,bg='white')
    
    # VI.1.2.1.5. Export auto CLAS regions as shapefiles ----
    if (!is.null(export.path)){
      # with rgdal: deprecated
      # writeOGR(obj=pclas.clustered.spdf, dsn=export.path, 
      #          layer=paste(pID,'_autoRegions',sep=''),driver="ESRI Shapefile",
      #          overwrite_layer=TRUE)
      # with terra
      # writeVector(pclas.voro.clipped.t, 
      #             filename=paste(export.path,pID,'_autoRegions.gpkg',sep=''),
      #             overwrite=TRUE)
      # with sf
      write_sf(poly.clustered.spdf,
               dsn=paste(export.path,pID,'_autoRegions.gpkg',sep=''),
               delete_layer = TRUE)
      cat('Regions saved in:',paste(export.path,'/',pID,
                                    '_autoRegions','.gpkg',sep=''),'\n')
    }
  }else{
    poly.clustered.spdf=NULL
    points.voro.clipped=NULL
    pcatch.all=NULL
  }
 
  list(poly.clustered.spdf=poly.clustered.spdf,
       points.voro.clipped=points.voro.clipped,points.all.clust=pcatch.all,
       mhgcclustp=mhgcclustp)
}

# Polygons created with "regions.autodef" function can be easily edited in QGIS
# using the "reshape/remodeler" tool

ashape2poly=function(dat.ashape){
  
  a<- data.frame(dat.ashape$edges)[,c( 'x1', 'y1', 'x2', 'y2')]
  
  # create first line segment to initialize the object
  i=1
  line_obj <- sp::Line(cbind(  c(a$x1[i], a$x2[i]),c(a$y1[i], a$y2[i])  ))
  lines_obj <- sp::Lines(list(line_obj),ID=i)
  myLines <- sp::SpatialLines(list(lines_obj))
  
  for (i in 2:nrow(a)){
    line_obj <- sp::Line(cbind(  c(a$x1[i], a$x2[i]),c(a$y1[i], a$y2[i])  ))
    lines_obj <- sp::Lines(list(line_obj),ID=i)
    myLines <- rbind(myLines, sp::SpatialLines(list(lines_obj))) #bind the line to the rest
  }
  
  sfL<-as(myLines, "sf") #convert lines to sf
  alphapoly = st_collection_extract(st_polygonize(st_union(sfL))) # union the lines and convert to polygon
  apol<-as_Spatial(alphapoly) #if you want to convert back to sp
  list(alphapoly=alphapoly,apol=apol)
}
