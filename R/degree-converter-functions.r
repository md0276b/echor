dec2sex=function(pos,oformat='sex'){
		intx=trunc(pos$x)
		inty=trunc(pos$y)
		decx=pos$x-intx
		decy=pos$y-inty
		sexx=abs(decx*60)
		sexx=round(sexx,digits=5)
		sexy=abs(decy*60)
		sexy=round(sexy,digits=5)
		sexx=sapply(sexx,add0)
		sexy=sapply(sexy,add0)
		if (oformat=='export'){
			dsexx=paste(abs(intx),sexx,'W')
			dsexy=paste(abs(inty),sexy,'N')
		}else{
			dsexx=paste(intx,sexx,sep='°')	
			dsexy=paste(inty,sexy,sep='°')
		}	
	dsex=list(xs=dsexx,ys=dsexy)
dsex
}

add0=function(sexx){
	if (nchar(as.character(sexx))<8)
		sexx=paste(as.character(sexx),
			paste(rep(0,8-nchar(as.character(sexx))),sep='',collapse=''),
			sep='')
sexx}


conv2num=function(x){
	y=as.numeric(as.character(x))
y
}

coord2ddegree2=function(x){
	x=as.numeric(as.character(x))
	deg=floor(x/100)
	minut=(x/100-deg)*100
	minut=minut/60
	cx=deg+minut
	cx
}

coord2ddegree.frag=function(deg,min,sec){
	deg=as.numeric(as.character(deg))
	min=as.numeric(as.character(min))
	sec=as.numeric(as.character(sec))
	minut=min/60
	second=sec/60/100
	cx=deg+minut+second
	cx
}


coord2ddegree.rond=function(x,sep=list(deg=c(1,2),minut=c(4,5),sec=c(7,9))){
	deg=as.numeric(substr(x,sep$deg[1],sep$deg[2]))
	minut=as.numeric(substr(x,sep$minut[1],sep$minut[2]))
	sec=as.numeric(substr(x,sep$sec[1],sep$sec[2]))
	cx=deg+minut/60+sec/60/100
	cx
}
