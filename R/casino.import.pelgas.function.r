casino.import.pelgas=function(path.cas,stp.gears=c('76x70','57x52'),
                              stp.events=c('DFIL','DPLON'),
                       gear.events=c('HQ','DFIL','FFIL','DVIR','FVIR','CULAB'),
                       config.event='DCONFIG',echo.event='ECHO',
                       start.events=c('DEBURAD','DEBUTRA','REPRAD','REPTRA','DCONFIG'),
                       stop.events=c('STOPRAD','FINRAD','FINTRA','STOPTRA'),
                       seq.start=c('DEBURAD','DEBUTRA'),
                       seq.stop=c('REPRAD','REPTRA','DCONFIG'),
                       nsonde=c("CINNA.Sonde.vert...m.","Sonde.ref...m."),
                       cruiseCode='PG19',vesselName='THALASSA II',
                       sep='\t',runlag=0,dstart=NULL,lstrates=c('CLAS','SURF','NULL'),
                       check.strates=TRUE){
  
  #Import Casino events file -----------------------------
  #********************************************
  cas=read.csv(path.cas,header=TRUE,sep=sep)  
  #names(cas)
  names(cas)[14]='N..Station'
  cas=cas[,names(cas)!='X']
  names(cas)
  
  #Select and format Casino stations ------------------------------
  #********************************************
  cat('No of hauls: ',
      length(cas[cas$Code.Action=='DVIR','N..Station']),'\n')
  head(cas)
  evts=cas[,c('Date','Heure','Latitude','Longitude','Code.Appareil','Code.Action','N..Station','Strate',
              'Identificateur.Operation')]
  names(evts)=c('date','heure','LATF','LONF','GEAR','EVT','NOSTA','STRATEL','ID')
  head(evts)
  stp=evts[evts$GEAR%in%stp.gears,]    
  stp=stp[stp$EVT%in%stp.events,]
  if (dim(stp)[1]>0){
    stp$NOCHAL=NA
    stp$TC=paste(stp$date,stp$heure)
    stp$STRATE=substr(stp$STRATEL,1,4)
    stp$LATF=as.numeric(gsub(',','.',stp$LATF))
    stp$LONF=as.numeric(gsub(',','.',stp$LONF))  
    stp=stp[order(stp$NOSTA),]
    stp$NOCHAL=seq(dim(stp)[1])
    
    head(stp)  
    if (T%in%(nchar(as.character(stp$NOSTA))>5)){ 
      stop('Invalid station id in casino file')
    }
    dim(stp)
    
    #Remove rows with NA --------------------
    #********************************************
    stp=stp[!is.na(stp$date),]  
    dim(stp)  
    #Check "strate"
    #head(stp)
    strate.errors=stp[!substr(stp$STRATE,1,4)%in%lstrates,]
    if (check.strates){  
      if (dim(strate.errors)[1]>0){
        cat("Invalid 'strate' fields in hauls:",
            as.character(unique(strate.errors$NOSTA)),'\n')
        stop('Please correct errors before proceeding')
      }
      Nope0=dim(stp)[1]
    }
    
    stp$rad=NA  
    
    #check that no trawl action is missing ------------------------------------
    #********************************************
    stp2=evts[evts$GEAR%in%stp.gears,]    
    stp2=stp2[stp2$EVT%in%gear.events,]
    head(stp2)
    
    lt=unique(stp2$NOSTA)
    for (i in 1:length(lt)){
      stpi=stp2[stp2$NOSTA==lt[i],]
      if (sum(stpi$EVT%in%gear.events)!=length(gear.events)){
        cat('Action de chalutage manquante ou en trop dans :',as.character(lt[i]),'Merci de compl?ter','\n')
      }
    }
  }
  
  sonde=cas[,nsonde]
  names(sonde)=c('SONDE12','SONDEref')
  sonde$SONDE12=as.numeric(gsub(',','.',sonde$SONDE12))
  sonde$SONDEref=as.numeric(gsub(',','.',sonde$SONDEref))             
  sondes=sonde[(evts$GEAR%in%stp.gears)&(evts$EVT%in%stp.events),]
  if (dim(sondes)[1]>0){
    sondes$SONDE=NA
    for (i in 1:dim(sondes)[1]){
      if (sondes$SONDE12[i]%in%c(-1,0)&!sondes$SONDEref[i]%in%c(-1,0)){
        sondes$SONDE[i]=sondes$SONDEref[i]
      }else if (!sondes$SONDE12[i]%in%c(-1,0)&sondes$SONDEref[i]%in%c(-1,0)){
        sondes$SONDE[i]=sondes$SONDE12[i]
      }else if (!sondes$SONDE12[i]%in%c(-1,0)&!sondes$SONDEref[i]%in%c(-1,0)){
        sondes$SONDE[i]=sondes$SONDEref[i]
      }else{
        sondes$SONDE[i]=NA
      }
    }
    stp$SONDE=sondes$SONDE  
  }
 
    #check that no survey or leg  event is missing -----------------------------------
  #********************************************
  head(evts)
  stp3=evts[evts$EVT%in%c('DEBUCAMP','FINCAMP','DEBULEG','FINLEG'),]    
  head(stp3)
  lt=unique(stp3$ID)
  for (i in 1:length(lt)){
    stpi=stp3[stp3$ID==lt[i],]
    if (sum(stpi$EVT%in%gear.events)!=2){
      cat('Debut ou fin de leg/campagne manquante dans identificateur operation :',as.character(lt[i]),'Merci de compl?ter','\n')
    }
  }
  
  #jackpot: Select and format acoustic transects ------------------------------
  #*********************************************************
  #Vessel specific data: Transect
  #*********************************************************
  
  #select transects ----------------
  #********************************************
  Transecti=cas[!cas$Code.Appareil%in%c('CAMP','')&!cas$Code.Action%in%c('CUFIX','NEXT'),]
  #names(Transecti)
  #Correct acoustic stratum ----------------
  #********************************************
  unique(Transecti$Strate)
  Transecti$Strate=as.character(Transecti$Strate)
  Transecti$Strate=gsub('Rad ','R',Transecti$Strate,ignore.case = TRUE)
  Transecti$Strate=gsub('Rad','R',Transecti$Strate,ignore.case = TRUE)
  Transecti$stratum=substr(Transecti$Strate,1,3)
  Transecti$stratum=gsub(' ','',Transecti$stratum)
  Transecti[nchar(Transecti$stratum)==2,'Stratum']=paste(substr(Transecti[nchar(Transecti$stratum)==2,'stratum'],
                                                                1,1),'0',
                                                         substr(Transecti[nchar(Transecti$stratum)==2,'stratum'],
                                                                2,2),sep='')
  Transecti$ID=paste(Transecti$Identificateur.Operation,Transecti$stratum)
  Transecti$vesselName=vesselName
  Transecti$vesselName=as.character(Transecti$vesselName)
  Transecti$rad=substr(Transecti$Strate,1,3)
  Transecti$direction=substr(Transecti$Strate,4,8)
  Transecti$direction=gsub(' ','',Transecti$direction)
  Transecti$direction=gsub(' ','',Transecti$direction)
  Transecti$direction=gsub('->','',Transecti$direction)
  Transecti$rad=gsub(' ','',Transecti$rad)
  Transecti[is.na(Transecti$rad),'Strate']
  Transecti[Transecti$Code.Action=='ECHO','direction']=as.character(Transecti[Transecti$Code.Action=='ECHO','Observation'])
  Transecti[Transecti$Code.Action=='DEBURAD',c('Date','Code.Action','Strate','rad','direction')]
  names(Transecti)
  names(Transecti)[c(1,2,3,4,9,11,14,83,124,125)]
  #   jackpot=Transecti[Transecti$Code.Action%in%c('DEBURAD','STOPRAD','DEBUTRA','FINTRA','STOPTRA','REPTRA','HQ',
  #                                                'CULAB','REPRAD','FINRAD','ECHO','DCONFIG'),c(1,2,3,4,9,11,14,83,124,125)]
  jackpot=Transecti[Transecti$Code.Action%in%unique(c(gear.events,start.events,stop.events,echo.event,config.event)),
                    c("Date","Heure","Latitude","Longitude","Code.Appareil","Code.Action","N..Station",
                      nsonde[2],"rad","direction")]
  names(jackpot)=c('date','heure','lat','lon','appareil','action','NumStation','profondeur','rad','direction')
  jackpot[,c('lat','lon')]=apply(jackpot[,c('lat','lon')],2,FUN=gsub,pattern=',',replacement='.')
  jackpot[,c('lat','lon')]=apply(jackpot[,c('lat','lon')],2,FUN=as.numeric)
  tl=strptime(paste(as.character(jackpot$date),as.character(jackpot$heure)),"%d/%m/%Y %H:%M:%OS")
  tld=strptime(as.character(jackpot$date),"%d/%m/%Y")
  if (!is.null(dstart)){
    jackpot=jackpot[tl>strptime(dstart,"%d/%m/%Y %H:%M:%OS"),]
  }
  tld=strptime(as.character(jackpot$date),"%d/%m/%Y")
  jackpot$run=runlag+as.numeric((tld-min(tld))/(3600*24)+1)
  jackpot=jackpot[order(jackpot$run,jackpot$heure),]
  jackpot$Nesdu.indicatif=NA
  jackpot$prefix=NA
  head(jackpot)
  
  jackpot.naetc=jackpot[jackpot$action%in%c('DEBURAD')&(is.na(jackpot$rad)|is.null(jackpot$rad)|jackpot$rad==''|is.na(jackpot$direction)|
                                                          is.null(jackpot$direction)|jackpot$direction==''),]
  jackpot$radir=paste(jackpot$rad,jackpot$direction,sep='_')
  jackpot$t=strptime(paste(jackpot$date,jackpot$heure), "%d/%m/%Y %H:%M:%S",tz="GMT")
  jackpots=jackpot[jackpot$action!=echo.event&(jackpot$appareil=='ACOU'&substr(jackpot$rad,1,1)=='R'),]
  jackpots=jackpots[order(jackpots$t),]
  head(jackpots)
  uradir=unique(jackpots$radir)
  
  errors=NULL
  for (i in 1:(length(uradir))){
    jackpotsi=jackpots[jackpots$radir==uradir[i],]
    if (!(dim(jackpotsi)[1] %% 2) == 0){
      cat("Evenement(s) manquant(s) dans: ",uradir[i])
      errors=rbind(errors,jackpotsi)
    }
    jackpots[jackpots$radir==uradir[i]&jackpots$action%in%start.events,'prefix']=
      paste(jackpots[jackpots$radir==uradir[i]&jackpots$action%in%start.events,'radir'],
            '-',letters[1:length(jackpots$radir[
        jackpots$radir==uradir[i]&jackpots$action%in%start.events])],'_',cruiseCode,sep='')
  }
    
  # for (i in 1:(length(jackpots$date)-1)){
  #     if (jackpots$action[i]%in%start.events){
  #       if (jackpots$action[i+1]%in%stop.events){
  #         #print(paste(jackpots[i,'date'],jackpots[i,'heure'],jackpots[i,'action'],
  #         #            jackpots[i+1,'date'],jackpots[i+1,'heure'],jackpots[i+1,'action']))
  #         jackpots$Nesdu.indicatif[i]=ceiling(dist(data.frame(xcor=jackpots[i:(i+1),'lon']*cos(46*180/pi),
  #                                                             y=jackpots[i:(i+1),'lat']))*60)
  #         # Correct transect name
  #         if (is.na(jackpots$rad[i+1])|(jackpots$rad[i+1]!=jackpots$rad[i])) jackpots$rad[i+1]=jackpots$rad[i]
  #         # Correct transect direction
  #         if (is.na(jackpots$direction[i+1])|(jackpots$direction[i+1]!=jackpots$direction[i])) jackpots$direction[i+1]=jackpot$direction[i]
  #         # creates prefix
  #         if (jackpots$action[i]%in%seq.start) {
  #           rad=jackpots$rad[i]
  #           prefi=paste(jackpots$rad[i],'_',jackpots$direction[i],'-a','_',cruiseCode,sep='')
  #           prefi1=paste(jackpots$rad[i],'_',jackpots$direction[i],sep='')
  #         }else if (jackpots$action[i]%in%seq.stop){
  #           rad=c(rad,jackpots$rad[i])
  #           ll=c('a','b','c','d','e','f','g','h','i','j','k','l','m')
  #           prefi=paste(prefi1,'-',ll[length(rad)],'_',cruiseCode,sep='')
  #         }  
  #         jackpots$prefix[i]=prefi
  #         errorsi=jackpots[FALSE,]
  #       }else if (jackpots$action[i+1]==config.event){
  #         #            jackpots[i+1,'date'],jackpots[i+1,'heure'],jackpots[i+1,'action']))
  #         jackpots$Nesdu.indicatif[i]=ceiling(dist(data.frame(xcor=jackpots[i:(i+1),'lon']*cos(46*180/pi),
  #                                                             y=jackpots[i:(i+1),'lat']))*60)
  #         #correct rad and direction
  #         if (is.na(jackpots$rad[i+1])|(jackpots$rad[i+1]!=jackpots$rad[i])) jackpots$rad[i+1]=jackpots$rad[i]
  #         if (is.na(jackpots$direction[i+1])|(jackpots$direction[i+1]!=jackpots$direction[i])) jackpots$direction[i+1]=jackpot$direction[i]
  #         # creates prefix
  #         if (jackpots$action[i]%in%seq.start) {
  #           rad=jackpots$rad[i]
  #           prefi=paste(jackpots$rad[i],'_',jackpots$direction[i],'-a','_',cruiseCode,sep='')
  #           prefi1=paste(jackpots$rad[i],'_',jackpots$direction[i],sep='')
  #         }else if (jackpots$action[i]%in%seq.stop){
  #           rad=c(rad,jackpots$rad[i])
  #           ll=c('a','b','c','d','e','f','g','h','i','j','k','l','m')
  #           prefi=paste(prefi1,'-',ll[length(rad)],'_',cruiseCode,sep='')
  #         }
  #         jackpots$prefix[i]=prefi
  #         errorsi=jackpots[FALSE,]
  #       }else{
  #         stop(paste('Erreur detectee dans : ',jackpots[i,'date'],jackpots[i,'heure'],jackpots[i,'action'],
  #                    jackpots[i+1,'date'],jackpots[i+1,'heure'],jackpots[i+1,'action']))
  #         errorsi=jackpots[i,]
  #         #print(i)
  #       }
  #     }else if (jackpots$action[i]%in%c(config.event,echo.event)){
  #       errorsi=NULL
  #     } 
  #     if (i==1) errors=errorsi
  #     else errors=rbind(errors,errorsi)
  #   }
    #print(i)

  errors2=jackpots[(jackpots$action%in%seq.start)&(jackpots$direction==''|jackpots$rad==''),]
  
  errors=rbind(errors,errors2)
  
  if (dim(errors)[1]>0) cat('Il y a  des erreurs dans le jackpot, consultable dans "errors",merci de les corriger')
  
  jackpot=rbind(jackpots,jackpot[jackpot$action==echo.event,],jackpot[jackpot$appareil%in%stp.gears,])
  tl=strptime(paste(as.character(jackpot$date),as.character(jackpot$heure)),"%d/%m/%Y %H:%M:%OS")
  tlo=order(tl)
  jackpot=jackpot[tlo,]
  jackpot=jackpot[,c("date","run","heure","lat","lon","appareil","action","NumStation","profondeur","rad",
                     "direction","Nesdu.indicatif","prefix")]
  
  if (dim(jackpot.naetc)[1]>0&dim(errors)[1]>0) {errors=rbind(errors,jackpot.naetc)
  }else if (dim(jackpot.naetc)[1]>0&dim(errors)[1]==0) {errors=jackpot.naetc}
  
  #Ajoute colonnes a remplir
  jackpot$Nesu.reel=''
  jackpot$Offset.surf=10
  jackpot$Echotypeur=''
  jackpot$NotesEchotypage=''
  
  #if (dim(errors)[1]>0) cat('Errors in transect breaks:',paste(errors[,c('date')],errors[,c('heure')],' '),'see errors file','\n')
  head(jackpot)
  list(jackpot=jackpot,stp=stp,errors=errors,cas=cas)
}
#************************************************************************************************************

# changer la lettre du disque M: si besoin:
#
# lettre    = 'M:' # windows disk
# #letre    = '/run/user/1000/gvfs/smb-share:domain=thalassa,server=tl-nas,share=mission,user=pelgas' # windows share in Linux
# path.cas = file.path(lettre, 'PELGAS2024/Casino/pelgas2024_event.csv')
# 
# # Ci-dessous :
# # - ajuster le jour de debut de campagne dans "dstart"
# # - mettre le d?calage en nombre de run entre la date de d?but de mission et le jour de d?but de prospection dans 'runlag'
# 
# casimp=casino.import.pelgas(path.cas,cruiseCode='PELGAS2024',sep='\t',runlag=0,
#                             stp.gears=c('76x70','57x52','76x70_cam','57x52_cam'),
#                      start.events=c('DEBURAD','DEBUTRA','REPRAD','REPTRA','FCONFIG'),
#                      stop.events=c('STOPRAD','FINRAD','FINTRA','STOPTRA','DCONFIG'),
#                      config.event=NULL,echo.event='ECHO',
#                      seq.start=c('DEBURAD','DEBUTRA','REPRAD','DCONFIG'),
#                      seq.stop=c('STOPRAD','FINRAD','REPTRA','FCONFIG'),
#                      dstart="28/04/2024 00:00:00")
# names(casimp)
# jackpot=casimp$jackpot
# errors=casimp$errors
# 
# write.table(jackpot, file.path(lettre,'PELGAS2024/Casino/jackpototo2.csv'), sep=';',row.names=FALSE)


