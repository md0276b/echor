#function to change the resolution of a matrix mat 
#	the vertical resolutionis is divided by mult$v
#	the new horizontal resolution is res$h in NM
#	based on data.frame xyt with positions and time
#	time: [1] time in seconds since origin, [2] hour labels
#res$h not implemented


	resMod=function(mat,xyt=NULL,cres=list(v=T,h=T),mult=list(v=2,h=1),
		res=list(v=1,h=1),FUN=list(v=mean,h=mean),xnames=c('xc','x'),
		yname='y',tnames=c('tgmt.js','hour')){

	#change vertical resolution
	#--------------------------	
		if (cres$v){
			rv=dim(mat)[[1]]
			bnrv=seq(0,rv+mult$v,mult$v)
			nrv=cut(seq(rv),breaks=bnrv)
			Sadf=data.frame(mat)
			Sadf$nrv=nrv
			n=length(Sadf)
			Sandf=aggregate(Sadf[,1:(n-1)],by=list(Sadf$nrv),FUN=FUN$v)
			Sanmat=as.matrix(Sandf[,-1])
			dim(Sanmat)
		}
	#change horizontal resolution
	#--------------------------
		if (cres$h){
			#computes start, end and max. survey extent based on time in julian format
			#----------------------------
			xs=xyt[xyt[,tnames[1]]==min(xyt[,tnames[1]]),c(xnames[1],yname)]
			xe=xyt[xyt[,tnames[1]]==max(xyt[,tnames[1]]),c(xnames[1],yname)]
			xmD=rbind(xs,xe)
			LtransD=dist(xmD)

			#computes linear distances of each ESU and new distance bins
			#----------------------------
			dil=di(x=xyt[,xnames[1]],y=xyt[,yname],t=xyt[,tnames[1]])*60
			xyt$dx0i=cumsum(dil)
			xyt$dx0=d0(x=xyt[,xnames[1]],y=xyt[,yname],
				x0=xs[,xnames[1]],y0=xs[,yname])*60
			d2=seq(0,max(xyt$dx0i+res$h),by=res$h)

			#labels for distance bins
			#----------------------------
			d2lab=d2[-length(d2)]+res$h/2
			xnew=paste('x',res$h,sep='');ynew=paste('y',res$h,sep='')
			dnew=paste('d',res$h,sep='')
			xyt[,dnew]=cut(xyt$dx0i,breaks=d2,labels=d2lab,
				include.lowest=T)

			#aggregate ESUs within new distance bins with FUN function
			#----------------------------
			if (cres$v){nmat=t(Sanmat)
			}else{nmat=t(mat)}
			gmsa=data.frame(nmat)
			dim(gmsa)
			dim(xyt)
			length(dnew)
			gmsa2=aggregate(gmsa,list(xyt[,dnew]),FUN=FUN$h)
			nxyt=aggregate(xyt[,c(xnames,yname,tnames)],
				list(xyt[,dnew]),FUN=FUN$h)
			names(nxyt)[1]='log'
			Sanmat=t(as.matrix(gmsa2[,-1]))
		}else {nxyt=NULL}
	list(nmat=Sanmat,nxyt=nxyt)}