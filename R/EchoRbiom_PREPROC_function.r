#*******************************************************************************************************
# STEP 2: PRE-PROCESSING: COMPUTE AND CHECK XE SCALING FACTORS, DEFINE AND CHECK ESDU~HAULS ASSOCIATIONS
#*******************************************************************************************************


EchoRbiom.preproc=function(ESDUDEVs,Pechef,Pechels,EspDev,hradius=6,plotit=TRUE,nid='NOCHAL',path.results=NULL,
                           link=list(near=TRUE,ref=TRUE),showlink='near',add.charef=FALSE,xlim=NULL,ylim=NULL,
                           legpos='topleft',radSA=0.05,pradius=0.5,scale.SA=2,logit=TRUE,labelit=TRUE,ux11=TRUE,
                           cruise=cruise){

  #---------------------------------------------------------------
  #Extract metadata
  #---------------------------------------------------------------  
  meta=get.metadata(EspDev)
  lyears=cruise
  lNdev=meta$lNdev
  EspDev$CAMPAGNE=cruise
  Ndevi=lNdev[[1]]
  sde=apply(ESDUDEVs[,paste('D',Ndevi,sep='')],2,sum)
  #select only non-null echotypes
#   Ndevi=Ndevi[sde>0]
#   lNdev[[1]]=lNdev[[1]][sde>0]
#   cat('Echotypes with non-null density are:',paste('D',Ndevi,sep=''),'\n')
  
  #---------------------------------------------------------------
  #II. Computes average fish backscatter around each haul as weights...
  #---------------------------------------------------------------
  
  #8. Computes average fish backscatter per echotype around each haul 
  # (used only in the biomass per region method)
  #-------------------------------------------------------
 
  par(mfrow=c(1,1))
  list.mSa.radius.db=multi.mSa.radius.dev(ESDUDEV=ESDUDEVs,
                                          Pechef,EspDev,hradius=hradius,plotit=plotit,mfrowc=c(2,3),nid=nid)
  length(list.mSa.radius.db)
  if (!is.null(path.results)){
    dev.print(device=png,
              filename=paste(path.results,cruise,"_sAweightHaulsMap.png",sep=''),
              width=800,height=800)
  }
  
  #-------------------------------------------------------------------------
  # III. Compute ECHO-INTEGRATION FACTOR PER SPECIES: 
  # X[ie] : ie=haul.species
  #-------------------------------------------------------------------------
  
  # 9.1. check if species are only found in echotype list
  #--------------------------------------------------------------------
  cat('Check if species are only found in echotype list:','\n')
  print(unique(EspDev$CodEsp)[!unique(EspDev$CodEsp)%in%unique(Pechef$CodEsp)])
  
  # 9.2. Compute Xe's
  #--------------------------------------------------------------------
  
  list.Xe.BN=Xe.compute(captures.comps2=Pechef,
                        compute.it=list(XeB=TRUE,XeN=TRUE),
                        EspDev=EspDev,DEVit=TRUE,TS.fL=FALSE,Unit.dev='sA')
  names(list.Xe.BN)
  # Extract Xe's in kg
  #-------------
  list.XeB=list.Xe.BN$XeB
  # Extract Xe's in no. of fish
  #-------------
  list.XeN=list.Xe.BN$XeN
  
  # 9.3. Aggregate all Xe's in dataframe format
  #-------------
  XeB.df=Xe.dframe.it(list.Xe=list.XeB,lNdev=lNdev)
  XeN.df=Xe.dframe.it(list.Xe=list.XeN,lNdev=lNdev)
  names(XeB.df)
  unique(XeB.df$CodEsp)
  
  # 10. Xe's check
  #----------------  
  # 10.1. Check for erroneous Xe's (NA or infinite)
  #----------------
  Xe.pb=XeB.df[is.na(XeB.df$Xe)|is.infinite(XeB.df$Xe),]
  if (dim(Xe.pb)[1]>0) stop ('Erroneous Xes found')
  
  # 10.2. Xe visual check: Xe "profiles"
  #--------------------------------------------------------------------
  #All Xe's boxplots
  #-----------
  XeB.df$CodEsp=as.character(XeB.df$CodEsp)
  head(XeB.df)
  #Xe's per CodEsp, all cruises
  if (plotit){
    par(mfrow=c(1,1),mar=c(3,9,3,1))
    boxplot(Xe~CodEsp,XeB.df,las=2,horizontal=TRUE,main=paste(cruise,'Xes'))
    if (!is.null(path.results)){
      dev.print(device=png,
                filename=paste(path.results,cruise,"_XeCheck.png",sep=''),
                width=800,height=800)
    }
  }  

  #Xe's per echotype boxplots
  #-----------  
  par(mfrow=c(1,1))
  list.Xe=list.XeB
  if (plotit){
    for (k in 1:length(lyears)){
      list.Xei=list.Xe[[k]]
      Ndevi=unlist(lNdev[[k]])
      EspDevi=EspDev[EspDev$CAMPAGNE==as.character(lyears[k]),]
      
      for (i in 1:length(Ndevi)){
        xei=list.Xei[[i]]
        #xeis=xei[xei$CodEsp%in%EspDevi[EspDevi[,
        #  'GENR_ESP']%in%c('ENGR-ENC','SARD-PIL',
        #  'SCOM-SCO','MICR-POU','TRAC-TRU'),'CodEsp'],]
        xei$CodEsp=as.character(xei$CodEsp)
        #png(file=paste(path.res,paste('XeSp.D',Ndev[i],'.png',sep=''),sep=''),
        #	width=800,height=800)
        #if (i>1){x11()}
        if (i==1){
          par(mfrow=c(2,3),mar=c(3,9,3,1))
        }
        boxplot(Xe~CodEsp,xei,main=paste(lyears[k],' D',
                                         Ndevi[i],sep=''),las=2,horizontal=TRUE)
        #dev.off()
      }
    }
    if (!is.null(path.results)){
      dev.print(device=png,
                filename=paste(path.results,cruise,"_XeDEVCheck.png",sep=''),
                width=800,height=800)
    }
  }
  
  
  
  #-------------------------------------------------------------------------
  # IV. Hauls / ESDUs associations definition
  #-------------------------------------------------------------------------
  
  # 11. Echotypes in ESDUs associated to nearest haul comprising species-in-echotype
  #--------------------------------------------------------------------
  
  #EspDev2=EspDev[EspDev$DEV=='D1',]
  #Ndevi=1
  if (link$near){
    cat('Nearest haul-ESDU association','\n')
    for (k in 1:length(lyears)){
      EsduDevi=ESDUDEVs[ESDUDEVs$CAMPAGNE==as.character(lyears[k]),]
      Ndevi=c(unlist(lNdev[[k]]))
      EspDevi=EspDev[EspDev$CAMPAGNE==as.character(lyears[k]),]
      Pechei=Pechef[Pechef$CAMPAGNE==as.character(lyears[k]),]
      #if (!TRUE%in%is.na(Pechei$LONF)){
      near.Associ=nearest.haul(Pechei=Pechef,EsduDevi=ESDUDEVs,
                               EspDevi=EspDev,Ndevi,nid=nid)
      list.Associ=near.Associ$list.Assoc
      #}else list.Associ='NA in haul positions'
      names(list.Associ)=paste('D',Ndevi,sep='')
      if (k==1){
        list.Assoc.year=list(list.Associ)
      }else{
        list.Assoc.year=c(list.Assoc.year,list(list.Associ))
      }
    }
    
    list.Assoc.year.near=list.Assoc.year
    #eventually, add nearest hauls as reference hauls in ESDUDEVs
    if(add.charef){
      ESDUDEVs=nhInEsdudev(ESDUDEVs,list.assoc=list.Assoc.year.near[[1]])
      head(ESDUDEVs)
    }
    
  }else{
    list.Assoc.year.near=NULL
  }
  
  # 12. AND/OR echotypes in ESDUs manually associated to a trawl haul
  #--------------------------------------------------------------------
  if (link$ref){
    cat('Reference haul-ESDU association','\n')
    if (sum(apply(ESDUDEVs[,paste('D',Ndevi,'.CREF',sep='')],2,is.na))>0){
      stop('Some reference hauls are missing')
    }
    for (k in 1:length(lyears)){
      EsduDevi=ESDUDEVs[ESDUDEVs$CAMPAGNE==as.character(lyears[k]),]
      Ndevi=c(unlist(lNdev[[k]]))
      EspDevi=EspDev[EspDev$CAMPAGNE==as.character(lyears[k]),]
      Pechei=Pechef[Pechef$CAMPAGNE==as.character(lyears[k]),]
      
      cat(as.character(lyears[k]),"\n")
      list.Associ=lapply(X=seq(length(Ndevi)),FUN=assoc.k.gen,
                         Ndevi,EsduDevi)
      names(list.Associ)=paste('D',Ndevi,sep='')
      if (k==1){
        list.Assoc.year=list(list.Associ)
      }else{
        list.Assoc.year=c(list.Assoc.year,list(list.Associ))
      }
    }
    list.Assoc.year.ref=list.Assoc.year
  }else{
    list.Assoc.year.ref=NULL
  }
  
  # 13. Check esdu/haul association
  # NB: if you are using the reference haul method, use:
  #    list.Associ=list.Assoc.year.ref[[1]]
  # for the nearest haul method, use:
  #    list.Associ=list.Assoc.year.near[[1]]
  #--------------------------------------------------------------------
  
  if (plotit){
    if (showlink=='near'&link$near){
      list.Associ=list.Assoc.year.near[[1]]
      cat('Plot nearest haul-ESDU association','\n')
    }else if (showlink=='ref'&link$ref){
      list.Associ=list.Assoc.year.ref[[1]]
      cat('Plot reference haul-ESDU association','\n')
    }else{
      stop('link and showlink arguments mismatch')
    }  
    #par(op)
    show.esdu.refhaul(Ndevi=Ndevi,list.Associ=list.Associ,Pechelsi=Pechels,Pechei=Pechef,EsduDevi=ESDUDEVs,
                      legpos=legpos,export.plot=path.results,radSA=radSA,pradius=pradius,scale.SA=scale.SA,
                      xlim=xlim,ylim=ylim,logit=logit,labelit=labelit,nid=nid,ux11=ux11)
  }
  
  list(list.XeB=list.XeB,list.XeN=list.XeN,XeB.df=XeB.df,XeN.df=XeN.df,ESDUDEVs=ESDUDEVs,
       list.Assoc.year.near=list.Assoc.year.near,list.Assoc.year.ref=list.Assoc.year.ref,
       list.mSa.radius.db=list.mSa.radius.db)
}  
