classicPopIndices=function(B.ALL,Biomres.sp.size,p=c(0.05,0.25,0.75,0.95)){
  
  names(B.ALL)
  # convert abundance in 1000fish in no. of fish
  B.ALL$N=B.ALL$wabun*1000
  # convert biomass in tons in kg
  B.ALL$W=B.ALL$wbiom*1000
  # compute estimation variance in number 
  B.ALL$mwt=B.ALL$wbiom/B.ALL$wabun/1000
  B.ALL$Nvar=(unique(B.ALL$area.esdu)^2)*B.ALL$wsigmaE2/(B.ALL$mwt^2)
  # compute estimation variance of total biomass in kg 
  B.ALL$Wvar=(unique(B.ALL$area.esdu)^2)*B.ALL$wsigmaE2*(1000^2)
  
  # global indices
  Cindices1=data.frame(survey=B.ALL$cruise,species=B.ALL$sp,
                       N=B.ALL$N,N_stdev=sqrt(B.ALL$Nvar),logN=log(B.ALL$N),
                       W=B.ALL$W,W_stdev=sqrt(B.ALL$Wvar),logW=log(B.ALL$W),
                       Wbar=B.ALL$W/B.ALL$N)
  Cindices1=Cindices1[Cindices1$species!='COMP-LEM',]
  Cindices1$N_CV=Cindices1$N_stdev/Cindices1$N
  Cindices1$W_CV=Cindices1$W_stdev/Cindices1$W
  unique(B.ALL$area.esdu)*sqrt(B.ALL$wsigmaE2)/B.ALL$wbiom
  sizeI.sp.res=sizeI.sp(Biomres.sp.size0=Biomres.sp.size)
  Cindices=merge(Cindices1,sizeI.sp.res$wmLsp,by.x='species',by.y='sp')
  Cindices=merge(Cindices,sizeI.sp.res$wvLsp,by.x='species',by.y='sp')
  Cindices=merge(Cindices,sizeI.sp.res$qL,by.x='species',by.y='sp')
  Cindices
}

sizeI.sp=function(Biomres.sp.size0,p=c(0.05,0.25,0.75,0.95)){
  # size-related indices
  head(Biomres.sp.size0)
  bssa=aggregate(Biomres.sp.size0$N,list(Biomres.sp.size0$sp),sum)
  names(bssa)=c('sp','Ntot2')
  bss2=merge(Biomres.sp.size0,bssa,by.x='sp',by.y='sp')
  head(bss2)
  bss2$wL=bss2$L*bss2$N/bss2$Ntot2
  wmLsp=aggregate(bss2$wL,list(bss2$sp),sum)
  names(wmLsp)=c('sp','Lbar')
  bss2=merge(bss2,wmLsp,by.x='sp',by.y='sp')
  bss2$wvari=(bss2$N/bss2$Ntot2)*(bss2$L-bss2$Lbar)^2
  wvLsp=aggregate(bss2$wvari,list(bss2$sp),sum)
  names(wvLsp)=c('sp','Lvar')
  #aggregate(Pechef$LM,list(Pechef$GENR_ESP),var)
  nlL=aggregate(bss2$N,list(bss2$sp,bss2$L),sum)
  names(nlL)=c('sp','L','N')
  nlL=merge(nlL,bssa,by.x='sp',by.y='sp')
  nlL$p=nlL$N/nlL$Ntot2
  lsp=unique(nlL$sp)
  for (i in 1:length(lsp)){
    nlls=nlL[nlL$sp==lsp[i],]
    nlls=nlls[order(nlls$L),]
    nlls$pcumsum=cumsum(nlls$p)
    qi=rep(NA,length(p))
    qi=wtd.quantile(nlls$L, weights = nlls$N,probs=p)
    # replicates each length measurements according to abundance
    # rnlls=rep(nlls$L,nlls$N) (too much memory)
#     for (j in 1:length(p)){
#       lsj=nlls[round(nlls$pcumsum,2)<=p[j],'L']
#       if ((length(lsj))>0){
#         qi[j]=lsj[length(lsj)]        
#       }else{
#         qi[j]=NA
#         cat(p[j],'length quantile not found for',lsp[i],'\n')
#       }
#     }
#     resi=data.frame(lsp[i],t(qi))
    #qi=quantile(rnlls,probs=p)
    resi=data.frame(lsp[i],t(qi))
    names(resi)=c('sp',paste('Lo',p,sep='.'))
    if (i==1){
      qL=resi
    }else{
      qL=rbind(qL,resi)
    }
  }
  list(wmLsp=wmLsp,wvLsp=wvLsp,qL=qL)
}


# Ni1=Biom.sp.ALL
# 
# B.ALL=biom.CV.res

classicComIndices=function(B.ALL,B.size){
  # global indicators: abundance, biomass, mean weight
  lncom=names(B.ALL)[grep('Com',names(B.ALL))]
  Ncom=length(lncom)
  for (i in 1:Ncom){
    bsai=B.ALL[!is.na(B.ALL[,lncom[i]]),]
    names(bsai)
    lcomi=B.ALL[,lncom[i]];lcomi=lcomi[!is.na(lcomi)]
    uc=as.character(unique(lcomi))
    #Ni1i=Ni1[!is.na(Ni1[,lncom[i]])&Ni1[,lncom[i]]==uc,]
    Ni1i=B.ALL[!is.na(B.ALL[,lncom[i]])&B.ALL[,lncom[i]]==uc,]
    names(Ni1i)[names(Ni1i)=='wabun']='wabun1'
    bsai=merge(bsai,Ni1i[,c('sp','wabun1')],by.x='sp',by.y='sp',all.x=TRUE)
    bsai$wGtot=log(bsai$wabun+1)/log(bsai$wabun1+1)
    names(bsai)
    bspaais=aggregate(bsai[,c('biom','wbiom','bioms','wbioms','abun','wabun',
                              'abuns','wabuns','mBB','mBN','biomW.esdu',
                              'biomN.esdu','BB','BN','mB','mwB','wsigmaE2',
                              'wsigmaE2i','wsigmaE2s','sigmaE2','sigmaE2i',
                              'sigmaE2s')],
                      list(bsai[,lncom[i]]),sum)
    names(bspaais)[1]='Com'
    bspaaim=aggregate(bsai[,c('mw.g','mw.g.catch','wGtot')],list(bsai[,lncom[i]]),mean)
    bspaaim$Gtot=exp(bspaaim$wGtot)
    names(bspaaim)[1]='Com'
    library(vegan)
    bspaai=merge(bspaais,bspaaim)
    bspaai$Delta1=diversity(bsai$wabun, index = "shannon", MARGIN = 1, 
                            base = exp(1))
    if (i==1) bspaa=bspaai
    else bspaa=rbind(bspaa,bspaai)
  }
  # compute estimation variance in number 
  bspaa$mwt=bspaa$wbiom/bspaa$wabun/1000
  bspaa$NsigmaE2=bspaa$wsigmaE2/(bspaa$mwt^2)
  # convert abundance (in 1000fish) in numbers of fish
  bspaa$wabun=bspaa$wabun*1000
  # convert biomass (in tons) in kg
  bspaa$wbiom=bspaa$wbiom*1000
  Cindices2=bspaa[,c('Com','wabun','NsigmaE2','wbiom','wsigmaE2','Gtot','Delta1')]
  names(Cindices2)=c('Com','Ntot','varNtot','Btot','varBtot','Gtot','delta1')
  
  Cindices2$Wbcomm=Cindices2$Btot/Cindices2$Ntot
  #for now:
  Cindices2$varNtot=Cindices2$varBtot/(Cindices2$Wbcomm^2)
  sqrt(Cindices2$varNtot)/Cindices2$Ntot
  sqrt(Cindices2$varBtot)/Cindices2$Btot
  Cindices2$varWbcomm=Cindices2$varBtot/(Cindices2$Ntot^2)+Cindices2$varNtot/(Cindices2$Btot^2)
  # Length-based indicators: abundance, biomass, mean weight
  Cindices2
}


spatialIndices=function(df,poly1,spname='CodEsp',xname='LONG',yname='LAT',
                        tname='year',zname='BB',sname='CAMPAGNE',
                        save.path,ndisc=100,dlim=15,dlim2=50,A.li=10,h0=10,
                        plot.CGI=TRUE,inches=0.25,y.intersp=2,lpos="bottomleft",
                        xlim=NULL,ylim=NULL,bathy.plot=TRUE,zcol=2,
                        lon1=NULL,lon2=NULL,lat1=NULL,lat2=NULL,resolution=1,
                        newbathy=FALSE,add=TRUE,deep=-450,shallow=-50,bstep=400,
                        bcol='grey50',drawlabels=TRUE,display='x11',eps=NULL,...){
  
  #library(SpatialIndices)
  library(splancs)
  library(MASS)
  
  # compute spatial indices ---------------------------------------------------------------
  
  # perform the projection for data and polygon
  mlong=mean(df[,xname])
  mlat=mean(df[,yname])
  
  proj <- dg2nm(x=df[,xname], y=df[,yname], modproj="mean", mlong=mlong, 
                mlat=mlat)
  df$long.p <- proj$x
  df$lat.p <- proj$y
  df$surf=NA
  
  #poly=poly1
  #poly1=poly
  proj <- dg2nm(x=poly1$x, y=poly1$y, modproj="mean", mlong=mlong,mlat=mlat)
  poly1$x <- proj$x
  poly1$y <- proj$y
  
  # plot(df[,xname],df[,yname])
  # lines(poly1$x,poly1$y)
  # coast()
  # plot(df$long.p,df$lat.p)
  # lines(poly$x,poly$y)

  #Compute spatial indicators on per-esdu data
  #-------------------------------------------------------------------------
  lspcodes=sort(unique(as.character(df[,spname])))
  
  for (i in 1:length(lspcodes)){
    spci=as.character(lspcodes[i])
    lyears=unique(as.character(df[,tname]))
    lcruise=unique(as.character(df[,sname]))
    for (j in 1:length(lyears)){
      year=as.character(lyears[j])
      dfi=df[df[,spname]==spci&df[,tname]==year,]
      #select samples with coordinates and z values
      dfi=dfi[!is.na(dfi[,xname])&!is.na(dfi[,yname])&!is.na(dfi[,zname]),]
      cruise=as.character(lcruise[j])
      dim(dfi)
      if (dim(dfi)[1]>0){
        # compute areas of influence of survey samples
        dfi$surf <- infl(x=dfi$long.p, y=dfi$lat.p, z=dfi[,zname],pol=poly1,dlim=dlim, ndisc=ndisc, visu=TRUE) 
        if (mean(dfi[,zname])>0){
          cat('Processing',as.character(spci),as.character(year),'\n')
          if (!is.null(eps)){
            lqp=c(seq(0.01,1,0.01))
            lquant=quantile(dfi$Zvalue,lqp)
            plot(lqp,lquant)
            abline(h=quantile(dfis$Zvalue,eps))
            dfi[dfi[,zname]<quantile(dfi[,zname],eps),zname]=0
            plot(dfi$long.p,dfi$lat.p,cex=0.1+log(dfi$Zvalue+1)*10)
            points(dfi[dfi$Zvalue<quantile(dfi$Zvalue,eps),'long.p'],dfi[dfi$Zvalue<quantile(dfi$Zvalue,eps),'lat.p'],pch=4,
                   cex=1,col=2)
            lines(poly1$x,poly1$y)
            dev.print(png,filename=paste(save.path,spci,'-lowZset2Zeroes-',cruise,'.png',sep=''),width=12,height=12,res=300,units='in')
            dev.off()
            cat('Z values lesser than quantile',eps,'set to zero','\n')
          }
          # make a bubble plot
          # sp.bubbleplot(dfi,xname=xname,yname=yname,zname=zname,inches=inches,spci=spci,cruise=cruise,xlim=xlim,ylim=ylim,zcol=zcol,
          #               bathy.plot=TRUE,lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution,newbathy=newbathy,lpos=lpos,
          #               y.intersp=y.intersp,add=add,deep=deep,shallow=shallow,bstep=bstep,bcol=bcol,drawlabels=drawlabels,display=display,...)
          # lines(poly1,col=8)
          
          # compute and plot the center of gravity (CG), the inertia and the isotropy 
          # of the regionalized variable 'z'
          # sp.bubbleplot(dfi,xname=xname,yname=yname,zname=zname,inches=inches,spci=spci,
          #               cruise=cruise,xlim=xlim,ylim=ylim,zcol=zcol,bathy.plot=TRUE,
          #               lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution,
          #               newbathy=newbathy,lpos=lpos,y.intersp=y.intersp,
          #               add=add,deep=deep,shallow=shallow,bstep=bstep,bcol=bcol,
          #               drawlabels=drawlabels,display=display)
          #,...)
          #lines(poly1,col=8)
            
          res.cgi <- cgi(x=dfi[,xname], y=dfi[,yname], z=dfi[,zname], w=dfi$surf, modproj="mean", 
                           mlong=mean(dfi[,xname]), mlat=mean(dfi[,yname]), col=4, plot=plot.CGI)
          # compute and plot the center of gravity, the inertia and the isotropy of the samples
          res.cgi.s <- cgi(x=dfi[,xname], y=dfi[,yname], w=dfi$surf, modproj="mean", 
                             mlong=mean(dfi[,xname]), mlat=mean(dfi[,yname]), col=1, plot=plot.CGI)
          if (plot.CGI){
            legend('topleft',legend=c('Samples inertia axis','Biomass inertia axis'),pch=c(NA,NA),col=c(1,4),lty=c(1,1),bty='n')
          }    
          dev.print(png,filename=paste(save.path,spci,'-CG-Inertia-',cruise,'.png',sep=''),width=12,height=12,res=300,units='in')
          dev.off()
          # compute the number of spatial patches, i.e. the number of high density fish patch
          # if error, goes on thanks to "try"
          res.sppatch=try(spatialpatches(x=dfi[,xname], y=dfi[,yname], z=dfi[,zname], w=dfi$surf, Lim.D=dlim2, A.li=A.li, 
                                         modproj="mean", mlong=mean(dfi[,xname]), mlat=mean(dfi[,yname])),silent=FALSE)
          if (class(res.sppatch)=="try-error"){
            res.sppatch=list(nsp=NA)
            cat('Could not compute spatial patches','\n')
          }else{
            mtext(paste(cruise,spci,'spatial patches'),line=2,cex=1.5)
            res.sppatchi=data.frame(res.sppatch$mat)
            res.sppatchi=res.sppatchi[res.sppatchi$pabun>=A.li,]
            dev.print(png,filename=paste(save.path,spci,'-spatialPatches-',cruise,'.png',sep=''),width=800,height=800)
            dev.off()
          }  
          # compute positive area (PA), i.e. the area occupied by fish densities>0 
          PA.res=positivearea(z=dfi[,zname], w=dfi$surf)
          
          # compute spreading area (SA), i.e. a measure of the area occupied by the stock that takes into account the fish density
          SA.res=spreadingarea(z=dfi[,zname], w=dfi$surf, plot = T)  
          # compute equivalent area (EA) i.e. an individual-based measure of the area occupied by the stock. 
          # Changes in EA are likely to reveal changes in the contribution of high density values to the total abundance.
          EA.res=equivalentarea(z=dfi[,zname], w=dfi$surf)
          
          # compute microstructure index (MI) i.e. the relative importance of structural components that have a smaller scale than
          # the sample lag (including random noise): if MI close to 0, smooth, well structured density surface, if MI close to 1,
          #  highly irregular, poorly structured, density surface.
          micro.res=microstructure(x=dfi[,xname], y=dfi[,yname], z=dfi[,zname], w=dfi$surf, h0=h0, pol=poly1, dlim=dlim2, ndisc=ndisc, 
                                   modproj="mean", mlong=mean(dfi[,xname]), mlat=mean(dfi[,yname]))
          
          # store selected spatial indices results in a dataframe 
          SI.res.dfi=data.frame(year=year,cruise=cruise,sp=spci,xcg=res.cgi$xcg,
                                ycg=res.cgi$ycg,I=res.cgi$I,Imax=res.cgi$Imax,
                                Imin=res.cgi$Imin,Iso=res.cgi$Iso,
                                xaxe1.1=res.cgi$xaxe1[1],xaxe1.2=res.cgi$xaxe1[2],
                                yaxe1.1=res.cgi$yaxe1[1],yaxe1.2=res.cgi$yaxe1[2],
                                xaxe2.1=res.cgi$xaxe2[1],xaxe2.2=res.cgi$xaxe2[2],
                                yaxe2.1=res.cgi$yaxe2[1],yaxe2.2=res.cgi$yaxe2[2],
                                Npatch=res.sppatch$nsp,PA=PA.res,SA=SA.res,EA=EA.res,
                                microS=micro.res)
          
          # store all spatial indices results in a list
          SI.res.listi=list(sp=spci,cgi=res.cgi,cgis=res.cgi.s,sppatch=res.sppatch,
                            positiveA=PA.res,spreadingA=SA.res,
                            equivalentA=EA.res,microS=micro.res)
          if (i==1&j==1){
            dfsurf=dfi
            SI.res.df=SI.res.dfi
            SI.res.list=SI.res.listi
          }else{
            dfsurf=rbind(dfsurf,dfi)
            SI.res.df=rbind(SI.res.df,SI.res.dfi)
            SI.res.list=c(SI.res.list,SI.res.listi)
          }
        }else{
          if (i==1&j==1){
            dfsurf=dfi
            SI.res.df=data.frame(year=year,cruise=cruise,sp=spci,xcg=NA,ycg=NA,I=NA,Imax=NA,Imin=NA,
                                 Iso=NA,xaxe1.1=NA,xaxe1.2=NA,yaxe1.1=NA,yaxe1.2=NA,
                                 xaxe2.1=NA,xaxe2.2=NA,yaxe2.1=NA,yaxe2.2=NA,Npatch=NA,PA=NA,SA=NA,EA=NA,microS=NA)
            SI.res.list=NA
          }else{
            dfsurf=rbind(dfsurf,dfi)
            SI.res.dfi=data.frame(year=year,cruise=cruise,sp=spci,xcg=NA,ycg=NA,I=NA,Imax=NA,Imin=NA,
                                 Iso=NA,xaxe1.1=NA,xaxe1.2=NA,yaxe1.1=NA,yaxe1.2=NA,
                                 xaxe2.1=NA,xaxe2.2=NA,yaxe2.1=NA,yaxe2.2=NA,Npatch=NA,PA=NA,SA=NA,EA=NA,microS=NA)
            SI.res.df=rbind(SI.res.df,SI.res.dfi)
            SI.res.listi=NA
            SI.res.list=c(SI.res.list,SI.res.listi)
          } 
        }
      }else{
        if (i==1&j==1){
          dfsurf=dfi
          SI.res.df=data.frame(year=year,cruise=cruise,sp=spci,xcg=NA,ycg=NA,I=NA,Imax=NA,Imin=NA,
                               Iso=NA,xaxe1.1=NA,xaxe1.2=NA,yaxe1.1=NA,yaxe1.2=NA,
                               xaxe2.1=NA,xaxe2.2=NA,yaxe2.1=NA,yaxe2.2=NA,Npatch=NA,PA=NA,SA=NA,EA=NA,microS=NA)
          SI.res.list=NA
        }else{
          dfsurf=rbind(dfsurf,dfi)
          SI.res.dfi=data.frame(year=year,cruise=cruise,sp=spci,xcg=NA,ycg=NA,I=NA,Imax=NA,Imin=NA,
                               Iso=NA,xaxe1.1=NA,xaxe1.2=NA,yaxe1.1=NA,yaxe1.2=NA,
                               xaxe2.1=NA,xaxe2.2=NA,yaxe2.1=NA,yaxe2.2=NA,Npatch=NA,PA=NA,SA=NA,EA=NA,microS=NA)
          SI.res.df=rbind(SI.res.df,SI.res.dfi)
          SI.res.listi=NA
          SI.res.list=c(SI.res.list,SI.res.listi)
        } 
      }
      }
  }
  
  lcruises=unique(df[,sname])
  for (k in 1:length(lcruises)){
    # Compute Global Indices of Collocation (GIC) between species i.e. their spatial overlap. 
    # GIC = 0 if the populations are distributed in two different locations with no overlap
    # GIC = 1 if the populations are distributed in exactly the same manner
    cruise=lcruises[k]
    dfi=dfsurf[dfsurf[,sname]==cruise,]
    dim(dfi)
    lspcodes=unique(dfi[,spname])
    Ncsp=length(lspcodes)
    if (Ncsp>1){
      GICmat=matrix(rep(NA,Ncsp^2),nrow=Ncsp)
      dimnames(GICmat)[[1]]=dimnames(GICmat)[[2]]=as.character(lspcodes)
      
      for (i in 1:Ncsp){
        df1=dfi[dfi[,spname]==lspcodes[i],]
        for (j in 1:Ncsp){
          df2=dfi[dfi[,spname]==lspcodes[j],]
          GICmat[i,j]=gic(x1=df1[,xname], y1=df1[,yname], z1=df1[,zname], w1=df1$surf, x2=df2[,xname], y2=df2[,yname], z2=df2[,zname], 
                          w2=df2$surf, modproj="mean", mlong=mean(df1[,xname]), mlat=mean(df1[,yname]))
          
        }
      }
      write.matrix(GICmat,paste(save.path,cruise,'-',spname,'-GICmatrix.txt',sep=''))
    }else{
      cat('Only one species, GIC not computed')
      GICmat=NULL
    }
  }
  
  list(dfsurf=dfsurf,SI.res.df=SI.res.df,SI.res.list=SI.res.list,GICmat=GICmat)
}
