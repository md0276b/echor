
vario1D.plot=function(x,tx,x0=2000,dx=1,nugget.pvar=TRUE){
  x.db <- db.create(x,x1=tx,nx=length(tx),x0=min(tx),dx=dx,nvar=1,ndim=1)
  x.db <- db.locate(x.db,3,"z")
  #plot(x.db,name='z1',reset=TRUE)
  # Calculate MAF experimental variograms
  x.db.vario <- vario.grid(x.db)
  #plot(x.db.vario)
  x.db.vario.model = model.auto(x.db.vario,
                                struct=c("Nugget Effect",
                                         "Linear","Exponential",
                                         "Cosinus"),
                                title="Vario model autofit",
                                verbose=FALSE,draw = FALSE)
  plot(x.db.vario)
  plot(x.db.vario.model,col=2,add=TRUE)
  Nmods=length(x.db.vario.model@basics)
  x.db.vario.model.df=NULL
  for (i in 1:Nmods){
    modi=x.db.vario.model@basics[[i]]
    modi.df=data.frame(type=modi$vartype,Model=paste(modi$vartype,i),
                       sills=diag(modi$sill),range=modi$range)
    x.db.vario.model.df=rbind(x.db.vario.model.df,modi.df)
  }
  x.db.vario.model.df$varPerc=x.db.vario.model.df$sills/
    sum(x.db.vario.model.df$sills)
  if (nugget.pvar){
    cat('Unstructured (random Nugget effect) perc. of variance:',
        x.db.vario.model.df[x.db.vario.model.df$type=='Nugget Effect','varPerc'])
  }
  list(x.db.vario=x.db.vario,x.db.vario.model=x.db.vario.model,
       x.db.vario.model.df=x.db.vario.model.df)
}


vario2D.fitNplot=function(x1,x2,z,nugget.pvar=TRUE,
                          ndir=1,nlag=10,ang0=0,
                          lstruct=c("Nugget Effect",
                                    "Spherical","Spherical"),
                          projectit=TRUE,polygonit=NULL,
                          path.save.plot=NULL,
                          tit=NA){
  
  x.db <- db.create(x1=x1,x2=x2,z=z)
  x.db <- db.locate(x.db,4,"z")
  
  if (!is.null(polygonit)){
    poly.data=polygon.create(x=polygonit$x,
                             y=polygonit$y)
    x.db=db.polygon(x.db,poly.data)
  }
  
  if (projectit){
    #projec.define("mean",db=)
    projec.define(projection="mean",db=x.db)      # projec.toggle(mode=-1) switch on/off
  }
  
  #plot(x.db,name='z1',coast='world')
  
  #plot(x.db.vario)
  # Creating the Data DB #
  rx1=extendrange(x1); ext1=rx1[2]-rx1[1]
  rx2=extendrange(x2); ext2=rx2[2]-rx2[1]
  a=cos(mean(x2)*pi/360)
  diam=60*sqrt((a*ext1)^2+ext2^2)
  diam 
  
  # Calculate the experimental variogram
  # and automatic fit #
  dirvect <- (seq(1,ndir)-1) * 180 / ndir + ang0
  
  # nlag1=40; dlag1=2 # max(nlag1*dlag1)~diam/2 lag=dlag1,nlag=nlag1,
  
  x.db.vario <- vario.calc(x.db,dirvect=dirvect,
                           lag=diam/(2*nlag),nlag=nlag)
  #plot(x.db.vario)

  x.db.vario.model = model.auto(x.db.vario,
                                struct=lstruct,
                                title="Vario model autofit",
                                verbose=FALSE,draw = FALSE)
  
  if (!is.null(path.save.plot)){
    x11(bg='white')
  }
  plot(x.db.vario,main=tit)
  plot(x.db.vario.model,col=2,add=TRUE)
  
  if (!is.null(path.save.plot)){
    filei=paste(path.save.plot,tit,'.png',sep='')
    dev.print(device = png, file = filei, width = 800,
              height = 800,bg = "white")
    dev.off()
  }
  
  Nmods=length(x.db.vario.model@basics)
  x.db.vario.model.df=NULL
  for (i in 1:Nmods){
    modi=x.db.vario.model@basics[[i]]
    modi.df=data.frame(type=modi$vartype,Model=paste(modi$vartype,i),
                       sills=diag(modi$sill),range=modi$range)
    x.db.vario.model.df=rbind(x.db.vario.model.df,modi.df)
  }
  x.db.vario.model.df$varPerc=x.db.vario.model.df$sills/
    sum(x.db.vario.model.df$sills)
  if (nugget.pvar){
    cat('Unstructured (random Nugget effect) perc. of variance:',
        x.db.vario.model.df[x.db.vario.model.df$type=='Nugget Effect','varPerc'])
  }
  list(x.db.vario=x.db.vario,x.db.vario.model=x.db.vario.model,
       x.db.vario.model.df=x.db.vario.model.df)
}
