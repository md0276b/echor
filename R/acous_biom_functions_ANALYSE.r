# Function to provide indices for choosing the number of map cell clusters in a MFA factorial plane 
#*************************************************************

ecoMFAclustEval=function(cellId,clustId,cellsel,MFAres,MFAdat,
                         covariate=NULL,
                         plotit=list(raster=TRUE,base=TRUE),
                         pati=NULL,idi=''){
  # Add clusters ids to data
  CL=data.frame(cell=cellId,clust=clustId)
  CLres.fssd= merge(CL, cellsel[,c('cell','I','J','x','y')],
                    by.x='cell',by.y='cell')
  CLres.fssd$cell.id=paste(CLres.fssd$I,CLres.fssd$J,sep='-')
  head(CLres.fssd)
  head(MFAdat)
  MFAdat.ALL=merge(MFAdat,
                   CLres.fssd,by.x="row.names",by.y='cell.id')
  head(MFAdat.ALL)

  # Plot cluster belongings in factorial and geographical spaces
  #*************************************************************
  if (plotit$raster){
    # Cluster belonging in raster format in geographical space
    library(raster)
    library(rasterVis)
    library(sf)
    library(maps)
    library(sp)

    CLres.fssd.ALLxy=expand.grid(x=seq(min(unique(CLres.fssd$x)),max(unique(CLres.fssd$x)),0.25),
                                 y=seq(min(unique(CLres.fssd$y)),max(unique(CLres.fssd$y)),0.25))
    CLres.fssd.ALLxy=merge(CLres.fssd.ALLxy,CLres.fssd[,c('x','y','clust')],by.x=c('x','y'),by.y=c('x','y'),all.x=TRUE)
    head(CLres.fssd.ALLxy)
    CLres.fssd.ALLxy=CLres.fssd.ALLxy[order(CLres.fssd.ALLxy$y),]
    CLres.fssd.raster=raster(ncol=length(unique(CLres.fssd.ALLxy$x)), nrow=length(unique(CLres.fssd.ALLxy$y)),
                             xmn=min(unique(CLres.fssd.ALLxy$x)),xmx=max(unique(CLres.fssd.ALLxy$x)), 
                             ymn=min(unique(CLres.fssd.ALLxy$y)), ymx=max(unique(CLres.fssd.ALLxy$y)))
    values(CLres.fssd.raster) <- CLres.fssd.ALLxy[,'clust']
    CLres.fssd.raster=flip(CLres.fssd.raster, 2)
    CLres.fssd.raster=ratify(CLres.fssd.raster)
    rat <- levels(CLres.fssd.raster)[[1]]
    rat$clust=factor(paste('Cluster',rat$ID),ordered=TRUE,
                     levels=paste('Cluster',rat$ID)[order(rat$ID)])
    levels(CLres.fssd.raster) <- rat
    mt=map(database = "worldHires", plot = F,
           xlim=c(extent(CLres.fssd.raster)[1],extent(CLres.fssd.raster)[2]),
           ylim=c(extent(CLres.fssd.raster)[3],extent(CLres.fssd.raster)[4]),fill=TRUE)
    mt.sp <- map2SpatialPolygons(mt, IDs=mt$names,proj4string=CRS("+proj=longlat"))
    x11()
    par(bg='white')
    p=levelplot(CLres.fssd.raster,xlim=c(x1,x2),ylim=c(y1,y2),par.settings=viridisTheme)
    p=p + latticeExtra::layer(sp.polygons(mt.sp,fill=gray(0.8)))
    print(p)
    if (!is.null(pati)){
      dev.print(device = png, file = paste(pati,paste(idi,i,'ClustRasterMap',sep='_'),'.png',sep=''),
                width = 1000,height = 1000, bg = "white")
      dev.off()
    }
  }
  if (plotit$base){
    # Cluster belonging in MFA1-2 and geographical spaces
    x11()
    par(mfrow=c(1,2),bg='white')
    #barplot(apply(ddres1.1corPlus.res,1,sum),main='Most contributive variable to MFA1')
    #barplot(apply(ddres1.2corPlus.res,1,sum),main='Most contributive variable to MFA2')
    plot(MFAres$ind$coord[,1],MFAres$ind$coord[,2],type='n',main='Individuals  clustering in MFA1-2 space',
         xlab=paste('Dim 1 (',round(MFAres$eig[1,2]),'%)',sep=''),ylab=paste('Dim 2 (',round(MFAres$eig[2,2]),'%)',sep=''))
    text(MFAres$ind$coord[,1],MFAres$ind$coord[,2],clustId)

    plot(CLres.fssd$x,CLres.fssd$y,type='n',xlim=c(-5,-1),ylim=c(43,48.5),xlab='',ylab='',main='Individuals  clustering in geographical space')
    text(CLres.fssd$x,CLres.fssd$y,CLres.fssd$clust)
    coast()
    if (!is.null(pati)){
      dev.print(device = png, file = paste(pati,paste(idi,i,'clustersSumPlot',sep=''),'.png',sep=''),
                width = 1000,height = 1000, bg = "white")
      dev.off()
    }
    par(mfrow=c(1,1))
  }
  # ANOVA of cluster effet
  #*************************************************************
  library(vegan)
  Y=MFAdat.ALL[,!names(MFAdat.ALL)%in%
                 c('cell','clust','I','J','x','y','Row.names')]
  dimnames(Y)
  X=as.matrix(MFAdat.ALL[,c('clust')])
  head(Y)
  MFAres.ALL.rda=rda(Y~X)
  #summary(big.fssd.MFAs.ALL.rda)
  MFAres.ALL.rda.anova=anova(MFAres.ALL.rda)

  # Clustering performance indices
  #*************************************************************
  n=length(unique(CLres.fssd$cell.id))
  i=length(unique(CLres.fssd$clust))
  k=i-1
  varexi=data.frame(Nc=i,k=k,n=n,
                    varex=MFAres.ALL.rda.anova$Variance[1],
                    varnonex=MFAres.ALL.rda.anova$Variance[2],
                    pvarex=MFAres.ALL.rda.anova$Variance[1]/(MFAres.ALL.rda.anova$Variance[1]+MFAres.ALL.rda.anova$Variance[2]))
  # AIC = 2k + n*ln(RSS) for least square fit
  varexi$AIC=2*(i-1)+n*log(varexi$varnonex)
  # AICc (corection for finite sample size) AICc = AIC + (2*k*(k+1))/(n-k-1)
  varexi$AICc=varexi$AIC+(2*k*(k+1))/(n-k-1)
  # number of cells per cluster average and SD
  ncclust=table(CLres.fssd$clust)
  varexi$mncclust=mean(ncclust)
  varexi$sncclust=sd(ncclust)
  
  if (!is.null(covariate)){
    # Add clusters ids to data
    CL=data.frame(cell=cellId,clust=clustId)
    CLres.fssd= merge(CL, cellsel[,c('cell','I','J','x','y')],by.x='cell',by.y='cell')
    CLres.fssd$cell.id=paste(CLres.fssd$I,CLres.fssd$J,sep='-')
    head(CLres.fssd)
    #Adds cluster id to covariate df
    covariate$cell.id=paste(covariate$I,covariate$J,sep='-')
    covariatec=merge(covariate,CLres.fssd[,c('clust','x','y','cell.id')],
                     by.x="cell.id",by.y='cell.id')
    names(covariate);names(covariatec)
    # RDA with environmental data
    library(vegan)
    Y=as.matrix(covariatec[,5:89])
    Y=apply(Y,2,NA2null)
    Y=apply(Y,2,scale)
    
    X=as.matrix(covariatec[,90:96])
    X2=covariatec[,c(3)]
    X3=as.matrix(covariatec[,c(3,89:93)])
    
    fishHydro.rda=rda(Y,X)
    names(summary(fishHydro.rda))
    summary(fishHydro.rda)$constr.chi/summary(fishHydro.rda)$tot.chi
    anova(fishHydro.rda)
  }
  
  list(varexi=varexi,MFAres.ALL=MFAdat.ALL,
       CLres.fssd=CLres.fssd,CLres.fssd.raster=CLres.fssd.raster)
}

# Function to produce Table 4.5 in Pages (2014) for MFA results interpretation 
#*************************************************************
MakeTable4_5=function(resMFA,stnnc,MFAdata=NULL,evarThr=75){
  if (!is.null(MFAdata)){
    resPCA=PCA(MFAdata)
  }else{
    resPCA=resMFA$global.pca
  }
  gcoord=resMFA$group$coord
  Nrow=dim(gcoord)[1]*3+2
  lyears=row.names(gcoord)
  Nyears=length(lyears)
  index=factor(rep(lyears,times=stnnc))
  tab4_5=matrix(nrow=Nrow,ncol=10)
  row.names(tab4_5)=c(paste('PCA',dimnames(gcoord)[[1]]),"PCA All",
                      paste('Group',dimnames(gcoord)[[1]]),
                      "MFA",paste('Group',dimnames(gcoord)[[1]]))
  colnames(tab4_5)=c("Global inertia","F1","F2","F3","F1%","F2%","F3%","F1+F2%","F1+F2+F3%",'Ndim')
  # total inertia = no. of variable per analysis/group:
  tab4_5[1:(Nyears),1]=stnnc
  tab4_5[(Nyears+2):(2*Nyears+1),1]=stnnc
  tab4_5[match('PCA All',row.names(tab4_5)),1]=sum(tab4_5[1:(Nyears),1])
  # PCA eigenvalues (in resPCA$eig)
  tab4_5[match('PCA All',row.names(tab4_5)),2:7]=c(t(resPCA$eig[1:3,1]),t(resPCA$eig[1:3,2]))
  # Lignes 4 et 5, colonnes 4 et 5. Les contributions des variables sont dans 
  # resPCA$var$contrib ; il faut les additionner par groupe.
  tab4_5[(Nyears+2):(2*Nyears+1),5:7]=as.matrix(aggregate(resPCA$var$contrib[,1:3],
                                                          list(index),sum)[,2:4])
  # Lignes 4 et 5, colonnes 2 et 3. On retrouve l'inertie en multipliant la contribution (en %) 
  # par la valeur propre correspondante.
  tab4_5[(Nyears+2):(2*Nyears+1),2]=tab4_5[(Nyears+2):(2*Nyears+1),5]*resPCA$eig[1,1]/100
  tab4_5[(Nyears+2):(2*Nyears+1),3]=tab4_5[(Nyears+2):(2*Nyears+1),6]*resPCA$eig[2,1]/100
  tab4_5[(Nyears+2):(2*Nyears+1),4]=tab4_5[(Nyears+2):(2*Nyears+1),7]*resPCA$eig[2,1]/100
  # Separate PCAs per groups are taken from resAFM$separate.analyses, eigenvalues being in $eig
  for (i in 1:Nyears){
    tab4_5[i,2:7]=c(t(resMFA$separate.analyses[[i]]$eig[1:3,1]),
                    t(resMFA$separate.analyses[[i]]$eig[1:3,2]))
  }
  # Ligne 6. Les valeurs propres de l'AFM sont dans ResAFM$eig.
  tab4_5[(2*Nyears+2),2:7]=c(t(resMFA$eig[1:3,1]),t(resMFA$eig[1:3,2]))
  # Lignes 6, 7, 8, colonne 1. L'inertie globale d'un groupe dans l'AFM resulte
  #directement du nombre de variables (les variables sont reduites) et de la ponderation 
  # (par la premiere valeur propre).
  tab4_5[(2*Nyears+3):(3*Nyears+2),1]=tab4_5[1:Nyears,1]/tab4_5[1:Nyears,2]
  tab4_5[(2*Nyears+2),1]=sum(tab4_5[(2*Nyears+3):(3*Nyears+2),1])
  # Lignes 7 et 8. Les inerties des variables somm�es par groupe sont d
  #ans ResAFM$group ; les inertie brutes sont dans coord
  #(l'explication de ce terme appara�t au chapitre 8) 
  #et les pourcentages dans contrib.
  tab4_5[(2*Nyears+3):(3*Nyears+2),2:4]=c(t(resMFA$group$coord[,1]),t(resMFA$group$coord[,2]),
                                          t(resMFA$group$coord[,3]))
  tab4_5[(2*Nyears+3):(3*Nyears+2),5:7]=c(t(resMFA$group$contrib[,1]),t(resMFA$group$contrib [,2]),
                                          t(resMFA$group$contrib[,3]))
  # Pour l'�dition, on r�duit le nombre de chiffres d�cimaux.
  tab4_5[,2:4]=round(tab4_5[,2:4],3)
  tab4_5[,5:7]=round(tab4_5[,5:7],2)
  tab4_5[1:(2*Nyears+1),1]=round(tab4_5[1:(2*Nyears+1),1],0)
  tab4_5[(2*Nyears+2):(3*Nyears+2),1]=round(tab4_5[(2*Nyears+2):(3*Nyears+2),1],3)
  tab4_5[,8]=tab4_5[,5]+tab4_5[,6]
  tab4_5[,9]=tab4_5[,8]+tab4_5[,7]
  tab4_5[c(1:(Nyears+1),2*Nyears+2),10]=99
  tab4_5[(seq(length(dimnames(tab4_5)[[1]]))%in%c(1:(Nyears+1),2*Nyears+2))&
           tab4_5[,9]>=evarThr,10]=3
  tab4_5[(seq(length(dimnames(tab4_5)[[1]]))%in%c(1:(Nyears+1),2*Nyears+2))&
           tab4_5[,8]>=evarThr,10]=2
  tab4_5[(seq(length(dimnames(tab4_5)[[1]]))%in%c(1:(Nyears+1),2*Nyears+2))&
           tab4_5[,5]>=evarThr,10]=1
  tab4_5
}

MakeTable7_2=function(resMFA){
  gcoord=resMFA$group$coord
  Nrow=dim(gcoord)[1]+1
  lyears=row.names(gcoord)
  Nyears=length(lyears)
  #### Tableau 7.2 
  # Initialisation
  tab7_2=matrix(nrow=Nrow,ncol=9)
  # Noms des lignes et des colonnes
  row.names(tab7_2)=c(lyears,"NJ")
  colnames(tab7_2)=c(paste("Axe",1:5),"Plan(1,2)","Plan(2,3)","Plan(1,3)","Ssp(1,5)")
  # Qualite de representation des groupes
  tab7_2[1:Nyears,1:5]=resMFA$group$cos2[,1:5]
  # Inertie projetee de NJ
  tab7_2[Nyears+1,1:5]=apply(resMFA$group$coord[,1:5]^2,MARGIN=2,FUN=sum)
  # Qualite de representation de NJ
  # L'inertie totale des Wj est dans ResAFM$group$dist2
  tab7_2[Nyears+1,1:5]=tab7_2[Nyears+1,1:5]/sum(resMFA$group$dist2)
  # Deux marges colonnes
  tab7_2[,6]=apply(tab7_2[,1:2],MARGIN=1,FUN=sum)
  tab7_2[,7]=apply(tab7_2[,2:3],MARGIN=1,FUN=sum)
  tab7_2[,8]=apply(tab7_2[,c(1,3)],MARGIN=1,FUN=sum)
  tab7_2[,9]=apply(tab7_2[,1:5],MARGIN=1,FUN=sum)
  tab7_2=round(tab7_2,4)
  tab7_2
}

BoBclusters.rasterPlot=function(MFA.coord,bNc,cellsel,pipo,ptitle='',labelit=FALSE,path.export=NULL){
  if (class(MFA.coord)[1]=="MFA"){
    res.fssd.hcpc<-HCPC(res.fssd.MFA ,nb.clust=bNc,consol=FALSE,graph=FALSE)
    # cell position data
    CL=data.frame(cell=row.names(res.fssd.hcpc$data.clust),clust=res.fssd.hcpc$data.clust$clust)
    MFAres=MFA.coord$ind$coord
  }else if (class(MFA.coord)[1]=="data.frame"){
    res.fssd.hcpc<-hclust(dist(MFA.coord),method='ward.D2')
    x11()
    plot(res.fssd.hcpc)
    res.fssd.hcpc2i=rect.hclust(res.fssd.hcpc,bNc)
    dev.off()
    for (i in 1:length(res.fssd.hcpc2i)){
      ci=res.fssd.hcpc2i[[i]]
      dfi=data.frame(cell=names(ci),clust=i)
      if (i==1){
        hclust2i=dfi
      }else{
        hclust2i=rbind(hclust2i,dfi)
      }
    }
    # cell position data
    hclust2i$cell=as.character(hclust2i$cell)
    cellnames=unlist(strsplit(hclust2i$cell,split='[.]'))[seq(1,(2*length(hclust2i$cell)),2)]
    CL=data.frame(cell=cellnames,clust=hclust2i$clust)
  }
  head(CL)
  dim(CL)
  CLres.fssd= merge(CL, cellsel[,c('cell','I','J','x','y')],by.x='cell',by.y='cell')
  CLres.fssd$cell.id=paste(CLres.fssd$I,CLres.fssd$J,sep='-')
  head(CLres.fssd)
  dim(CLres.fssd)
  head(CLres.fssd)
  CLres.fssd.ALLxy=pipo[,c('Xgd','Ygd','I','J')]
  CLres.fssd.ALLxy=merge(CLres.fssd.ALLxy,CLres.fssd[,c('x','y','clust')],by.x=c('Xgd','Ygd'),by.y=c('x','y'),all.x=TRUE)
  head(CLres.fssd.ALLxy)
  names(CLres.fssd.ALLxy)[1:2]=c('x','y')
  CLres.fssd.ALLxy=CLres.fssd.ALLxy[order(CLres.fssd.ALLxy$y),]
  CLres.fssd.ALLxy$cell=paste(CLres.fssd.ALLxy$I,CLres.fssd.ALLxy$J,sep='-')
  # MFA results
  head(CLres.fssd.ALLxy)
  head(MFAres)
  MFAres.fssd.ALLxy=merge(CLres.fssd.ALLxy,MFAres,by.x=c('cell'),by.y='row.names',all.x=TRUE)
  head(MFAres.fssd.ALLxy)
  MFAres.fssd.ALLxy=MFAres.fssd.ALLxy[order(MFAres.fssd.ALLxy$y),]
    # Plot clusters in geographical space
    #*************************************************************
    CLres.fssd.raster=raster(ncol=length(unique(CLres.fssd.ALLxy$x)), nrow=length(unique(CLres.fssd.ALLxy$y)),
                             xmn=min(unique(CLres.fssd.ALLxy$x)),xmx=max(unique(CLres.fssd.ALLxy$x)), 
                             ymn=min(unique(CLres.fssd.ALLxy$y)), ymx=max(unique(CLres.fssd.ALLxy$y)),
                             crs=CRS("+proj=longlat +datum=WGS84"))
    values(CLres.fssd.raster) <- CLres.fssd.ALLxy[,'clust']
    CLres.fssd.raster=flip(CLres.fssd.raster, 2)
    CLres.fssd.raster=ratify(CLres.fssd.raster)
    rat <- levels(CLres.fssd.raster)[[1]]
    rat$clust=factor(paste('Cluster',rat$ID),ordered=TRUE,levels=paste('Cluster',rat$ID)[order(rat$ID)])
    levels(CLres.fssd.raster) <- rat
    mt=map(database = "worldHires", plot = F,xlim=c(extent(CLres.fssd.raster)[1],extent(CLres.fssd.raster)[2]),
           ylim=c(extent(CLres.fssd.raster)[3],extent(CLres.fssd.raster)[4]),fill=TRUE)
    mt.sp <- map2SpatialPolygons(mt, IDs=mt$names,proj4string=CRS("+proj=longlat +datum=WGS84"))
    #x11()
    par(bg='white')
    # Plot raster clusters
    # Prepare isobaths
    data("BoBisobaths")
    BoBisobath.mat=cbind(BoBisobaths$x,BoBisobaths$y)
    BoBisobath.mat=BoBisobath.mat[complete.cases(BoBisobath.mat),]
    BoBisobath=Line(BoBisobath.mat)
    xyr=xyFromCell(CLres.fssd.raster, 1:ncell(CLres.fssd.raster), spatial=TRUE)
    xyr.df=data.frame(coordinates(xyr),clust=values(CLres.fssd.raster))
    
    p=levelplot(CLres.fssd.raster,xlim=c(x1,x2),ylim=c(y1,y2),par.settings=viridisTheme,main=ptitle)
    p=p + latticeExtra::layer(sp.polygons(mt.sp,fill=gray(0.8)))
    if (labelit){
      p=p+latticeExtra::layer(panel.text(xyr.df[!is.na(xyr.df$clust),1],xyr.df[!is.na(xyr.df$clust),2],
                                         xyr.df[!is.na(xyr.df$clust),'clust'],cex=0.5,col='white'))
    }
    #p=p + latticeExtra::layer(sp.points(xyr))
    # add isobaths
    p=p + latticeExtra::layer(sp.lines(BoBisobath,col='grey70'))
    print(p)
}

MFA.rasterPlot=function(MFAres,nMFA=3,cellsel,xyCell=NULL,ptitle='',
                        path.export=NULL,
                            plotit=list(MFA123coord=TRUE,MFA123timevar=TRUE,
                                        MFA123contrib=TRUE,MFA123cos2=TRUE,
                                        MFA123coordMcos2=TRUE,
                                        MFA123coordMcontrib=TRUE,MFA123pind=FALSE),
                        funSel='mean',anomalit=FALSE,sanomalit=FALSE,
                        ux11=FALSE,lptheme=viridisTheme,marginit=FALSE,
                        Ndim=list(dim(MFAres$ind$coord)[2]),
                        thrMFAs=NULL,
                        crs=CRS("+proj=longlat +datum=WGS84"),
                        bathy.plot=TRUE,newbathy=TRUE,coastit=TRUE,...){
  if (class(MFAres)[1]=="MFA"){
    MFAcoord=MFAres$ind$coord
    MFAcos2=MFAres$ind$cos2
    MFAcontrib=MFAres$ind$contrib
    head(MFAcoord)
    head(MFAcos2)
    rnsplit=unlist(strsplit(row.names(MFAcoord),split='[.]'))
    if (length(rnsplit)>length(row.names(MFAcoord))){
      cellnames=unlist(strsplit(row.names(MFAcoord),split='[.]'))[seq(1,(2*length(row.names(MFAcoord))),2)]
      row.names(MFAcoord)=cellnames
    }
    MFApcoord=data.frame(MFAres$ind$coord.partiel)
    rnsplit2=unlist(strsplit(row.names(MFApcoord),split='[.]'))
    length(rnsplit2);head(MFApcoord)
    Nel=length(rnsplit2)/dim(MFApcoord)[1]
    lnames=c('cell','year','month')
    for (i in 1:Nel){
      MFApcoord[,lnames[i]]=unlist(strsplit(row.names(MFApcoord),split='[.]'))[
        seq(i,(Nel*length(row.names(MFApcoord))),Nel)]
    }
    names(MFApcoord)
    MFAeig=MFAres$eig
  }else if (class(MFAres)[1]=="data.frame"){
    MFAcoord=MFAres
    rnsplit=unlist(strsplit(row.names(MFAcoord),split='[.]'))
    if (length(rnsplit)>length(row.names(MFAcoord))){
      cellnames=unlist(strsplit(row.names(MFAcoord),split='[.]'))[seq(1,(2*length(row.names(MFAcoord))),2)]
      row.names(MFAcoord)=cellnames
    }
    MFAcos2=NULL
    MFAcontrib=NULL
    MFApcoord=NULL
    head(MFAcoord)
  }else if (class(MFAres)[1]=="matrix"){
    MFAcoord=MFAres
    rnsplit=unlist(strsplit(row.names(MFAcoord),split='[.]'))
    if (length(rnsplit)>length(row.names(MFAcoord))){
      cellnames=unlist(strsplit(row.names(MFAcoord),split='[.]'))[seq(1,(2*length(row.names(MFAcoord))),2)]
      row.names(MFAcoord)=cellnames
    }
    MFAcos2=NULL
    MFAcontrib=NULL
    MFApcoord=NULL
    head(MFAcoord)
  }  
  # Merge MFA results and cell positions
  CLres.fssd.ALLxy=data.frame(cellsel)
  names(CLres.fssd.ALLxy)[names(CLres.fssd.ALLxy)=='Xgd']='x'
  names(CLres.fssd.ALLxy)[names(CLres.fssd.ALLxy)=='Ygd']='y'
  CLres.fssd.ALLxy=CLres.fssd.ALLxy[order(CLres.fssd.ALLxy$y),]
  #CLres.fssd.ALLxy$cell=row.names(CLres.fssd.ALLxy)
  head(CLres.fssd.ALLxy)
  MFAcoord.ALLxy=merge(CLres.fssd.ALLxy,MFAcoord,by.x=c('cell'),
                       by.y='row.names',all.x=TRUE)
  head(MFAcoord.ALLxy)
  #plot(MFAcoord.ALLxy$x,MFAcoord.ALLxy$y,cex=MFAcoord.ALLxy$Dim.1/1000)
  #plot(MFAcoord.ALLxy$x,MFAcoord.ALLxy$y)
  #coast()
  if (!is.null(MFAcontrib)){
    MFAcontrib.ALLxy=merge(CLres.fssd.ALLxy,MFAcontrib,by.x=c('cell'),by.y='row.names',all.x=TRUE)
    MFAcontrib.ALLxy=MFAcontrib.ALLxy[order(MFAcontrib.ALLxy$y),]
  }
  if (!is.null(MFAcos2)){
    MFAcos2.ALLxy=merge(CLres.fssd.ALLxy,MFAcos2,by.x=c('cell'),by.y='row.names',all.x=TRUE)
    MFAcos2.ALLxy=MFAcos2.ALLxy[order(MFAcos2.ALLxy$y),]
  } 
  if (plotit$MFA123coord){
    # Plot individuals mean coordinates on MFA axis in geographical space -----
    #*************************************************************
    for (i in 1:nMFA){
      if (sanomalit){
        MFAcoord.ALLxy[,c(paste('Dim.',i,sep=''))]=MFAcoord.ALLxy[,c(paste('Dim.',i,sep=''))]/MFAeig[i,1]
      }
      MFAcoord.rasteri=rasterFromXYZ(MFAcoord.ALLxy[,c('x','y',paste('Dim.',i,sep=''))],
                                     crs=crs)
      if (i==1){
        MFAcoord.rasterStack=MFAcoord.rasteri
      }else{
        MFAcoord.rasterStack=stack(MFAcoord.rasterStack,MFAcoord.rasteri)
      }
    }
    raster.levelplot(r=MFAcoord.rasterStack,ux11=ux11,lptheme=lptheme,
                     path1=path.export,fid1='MFAcoordGeoSpace',
                     marginf = marginit,
                     nattri=paste(ptitle,'MFA coordinates'),
                     bathy.plot=bathy.plot,newbathy=newbathy,
                     coastit=coastit,...)
  }else{
    MFAcoord.rasterStack=NULL
  }
  if (plotit$MFA123timevar){
    # Inter-annual variability in cells ------
    #*************************************************************
    # distance between cell mean position (individuals) and annual cell positions (partial individuals)
    dim(MFAcoord.ALLxy);dim(MFApcoord)
    coord.dist<- merge(MFAcoord.ALLxy,MFApcoord, by.x= "cell", by.y= "cell")
    head(coord.dist)
    for (k in 1:length(Ndim)){
      for (i in Ndim[[k]]){
        di=(coord.dist[,paste('Dim.',i,'.x',sep='')]-coord.dist[,paste('Dim.',i,'.y',sep='')])^2
        if (i==Ndim[[k]][1]) {
          dt=di
        }else{
          dt=dt+di
        }
      }
      coord.dist$distance<- sqrt(dt)
      head(coord.dist)
      coord.dista=aggregate(coord.dist$distance,list(cell=coord.dist$cell),mean)
      names(coord.dista)[2]='meanD2bary'
      #coord.dista=merge(coord.dista,CLres.fssd.ALLxy[,c('cell','x','y','I','J')],all.y=TRUE)
      #plot(CLres.fssd.ALLxy$x,CLres.fssd.ALLxy$y)
      #plot(coord.dist$x,coord.dist$y)
      coord.dista=merge(coord.dista,CLres.fssd.ALLxy[,c('cell','x','y')],all.y=TRUE)
      head(coord.dista)
      dim(coord.dista)
      coord.dista=coord.dista[order(coord.dista$y,coord.dista$x),]
      CLres.fssdD.raster=rasterFromXYZ(coord.dista[,c('x','y','meanD2bary')],
                    crs=crs)
      #plot(CLres.fssdD.raster)
      # CLres.fssdD.raster=raster(ncol=length(unique(coord.dista$x)),
      #                           nrow=length(unique(coord.dista$y)),
      #                           xmn=min(unique(coord.dista$x)),xmx=max(unique(coord.dista$x)), 
      #                           ymn=min(unique(coord.dista$y)), ymx=max(unique(coord.dista$y)),
      #                           crs=crs)
      # values(CLres.fssdD.raster) <- coord.dista[,'meanD2bary']
      #CLres.fssdD.raster=flip(CLres.fssdD.raster,2)
      Dim1=min(Ndim[[k]]);DimN=max(Ndim[[k]])
      coord.dista$MFAs=paste(Dim1,'-',DimN,sep='')
      names(CLres.fssdD.raster)=paste('MFA',Dim1,'-',DimN,sep='')
      raster.levelplot(CLres.fssdD.raster,ux11=ux11,lptheme=lptheme,
                              path1=path.export,fid1=paste(Dim1,'-',DimN,'MFAvarGeoSpace',sep=''),
                              marginf = marginit,
                              nattri=paste(ptitle,' ',Dim1,'-',DimN,' MFA coordinates inertia',sep=''),
                              layout=c(1,1),bathy.plot=bathy.plot,
                       newbathy=newbathy,coastit=coastit)
      if (k==1){
        MFAinertia=list(coord.dista)
        MFAinertia.raster=CLres.fssdD.raster
      }else{
        MFAinertia=c(MFAinertia,list(coord.dista))
        MFAinertia.raster=stack(MFAinertia.raster,CLres.fssdD.raster)
      }
    }
  }else{
    MFAinertia=NA
    MFAinertia.raster=NA
  }
  if (plotit$MFA123contrib){
    # Plot individuals contribution to MFA axis in geographical space -----
    #*************************************************************
    for (i in 1:nMFA){
      MFAcontrib.rasteri=rasterFromXYZ(MFAcontrib.ALLxy[,c('x','y',paste('Dim.',i,sep=''))],
                                       crs=crs)
      maskMFAcontrib.rasteri=MFAcontrib.rasteri>cellStats(MFAcontrib.rasteri, funSel)
      maskMFAcontrib.rasteri=reclassify(maskMFAcontrib.rasteri, matrix(c(0,1,NA,1),ncol=2))
      if (i==1){
        MFAcontrib.rasterStack=MFAcontrib.rasteri
        maskMFAcontrib.rasterStack=maskMFAcontrib.rasteri
      }else{
        MFAcontrib.rasterStack=stack(MFAcontrib.rasterStack,MFAcontrib.rasteri)
        maskMFAcontrib.rasterStack=stack(maskMFAcontrib.rasterStack,maskMFAcontrib.rasteri)
      }
    }
    raster.levelplot(MFAcontrib.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcontribGeoSpace',
                            marginf = marginit,nattri=paste(ptitle,'Contribution to MFA axes'),
                     bathy.plot=bathy.plot,newbathy=newbathy,coastit=coastit,
                     ...)
  }else{
    MFAcontrib.rasterStack=NULL
    maskMFAcontrib.rasterStack=NULL
  }
  if (plotit$MFA123cos2){
    # Plot individuals quality of representation on MFA axis in geographical space ------
    #*************************************************************
    for (i in 1:nMFA){
      MFAcos2.rasteri=rasterFromXYZ(MFAcos2.ALLxy[,c('x','y',paste('Dim.',i,sep=''))],
                                    crs=crs)
      maskMFAcos2.rasteri=MFAcos2.rasteri>cellStats(MFAcos2.rasteri, funSel)
      maskMFAcos2.rasteri=reclassify(maskMFAcos2.rasteri, matrix(c(0,1,NA,1),ncol=2))
      if (i==1){
        MFAcos2.rasterStack=MFAcos2.rasteri
        maskMFAcos2.rasterStack=maskMFAcos2.rasteri
      }else{
        MFAcos2.rasterStack=stack(MFAcos2.rasterStack,MFAcos2.rasteri)
        maskMFAcos2.rasterStack=stack(maskMFAcos2.rasterStack,maskMFAcos2.rasteri)
      }
    }
    raster.levelplot(MFAcos2.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcos2GeoSpace',
                            marginf = marginit,
                            nattri=paste(ptitle,'Quality of representation on MFA axes'),
                            bathy.plot=bathy.plot,newbathy=newbathy,
                     coastit=coastit,...)
  }else{
    MFAcos2.rasterStack=NULL
    maskMFAcos2.rasterStack=NULL
  }
  if (plotit$MFA123coordMcos2){
    # Plot individuals quality of representation on MFA axis in geographical space -----
    #*************************************************************
    for (i in 1:nMFA){
      if (dim(MFAcoord.rasterStack)[3]>1){
        MFAcoord.rasteri=raster(MFAcoord.rasterStack,i)
        maskMFAcos2.rasteri=raster(maskMFAcos2.rasterStack,i)
      }else{
        MFAcoord.rasteri=MFAcoord.rasterStack
        maskMFAcos2.rasteri=maskMFAcos2.rasterStack
      }
      MFAcoord.rasteris=mask(MFAcoord.rasteri,maskMFAcos2.rasteri)
      if (i==1){
        MFAcoordMcos2.rasterStack=MFAcoord.rasteris
      }else{
        MFAcoordMcos2.rasterStack=stack(MFAcoordMcos2.rasterStack,MFAcoord.rasteris)
      }
    }
    raster.levelplot(MFAcoordMcos2.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcoordMaskCos2GeoSpace',
                            marginf = marginit,
                            nattri=paste(ptitle,'Coordinates on MFA axes of well represented cells'),
                            bathy.plot=bathy.plot,newbathy=newbathy,
                     coastit=coastit,...)
  }else{
    MFAcoordMcos2.rasterStack=NULL
  }
  if (plotit$MFA123coordMcontrib){
    # Plot individuals quality of representation on MFA axis in geographical space -----
    #*************************************************************
    for (i in 1:nMFA){
      if (dim(MFAcoord.rasterStack)[3]>1){
        MFAcoord.rasteri=raster(MFAcoord.rasterStack,i)
        maskMFAcontrib.rasteri=raster(maskMFAcontrib.rasterStack,i)
      }else{
        MFAcoord.rasteri=MFAcoord.rasterStack
        maskMFAcos2.rasteri=maskMFAcos2.rasterStack
      }
      MFAcoord.rasteris=mask(MFAcoord.rasteri,maskMFAcontrib.rasteri)
      if (i==1){
        MFAcoordMcontrib.rasterStack=MFAcoord.rasteris
      }else{
        MFAcoordMcontrib.rasterStack=stack(MFAcoordMcontrib.rasterStack,MFAcoord.rasteris)
      }
    }
    raster.levelplot(MFAcoordMcontrib.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcoordMaskContribGeoSpace',
                            marginf = marginit,
                            nattri=paste(ptitle,'Coordinates on MFA axes of cells contributing above average to MFA axes'),
                            bathy.plot=bathy.plot,newbathy=newbathy,
                     coastit=coastit,...)
  }else{
    MFAcoordMcontrib.rasterStack=NULL
  }
    # Plot partial individuals -----
  #*************************************************************
  if (!is.null(MFApcoord)&plotit$MFA123pind){
    head(MFApcoord)
    if ('month'%in%names(MFApcoord)){
      lyears=unique(MFApcoord$year)
      lmonths=unique(MFApcoord$month)
      names(MFApcoord)
      MFAmpcoord.year=aggregate(MFApcoord[,substr(names(MFApcoord),1,3)=='Dim'],
                                list(MFApcoord$cell,MFApcoord$year),mean)
      names(MFAmpcoord.year)[1:2]=c('cell','year')
      MFAmpcoord.month=aggregate(MFApcoord[,substr(names(MFApcoord),1,3)=='Dim'],
                                list(MFApcoord$cell,MFApcoord$month),mean)
      names(MFAmpcoord.month)[1:2]=c('cell','month')
      head(MFAmpcoord.year)
      MFAmpcoord.year=prep.MFApcoord(MFAmpcoord.year,MFAeig=MFAeig,
                                     thrMFAs=thrMFAs)
      MFAmpcoord.month=prep.MFApcoord(MFAmpcoord.month,MFAeig=MFAeig,
                                      thrMFAs=thrMFAs)
      # 1 pcoord plot per year
      for (i in 1:nMFA){
        for (j in 1:length(lyears)){
          dfi=MFAmpcoord.year[MFAmpcoord.year$year==lyears[j],]
          if (anomalit){
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('anDim.',i,sep='')]
          }
          if (sanomalit){
            #dfi[,paste('Dim.',i,sep='')]=dfi[,paste('sanDim.',i,sep='')]
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('esanDim.',i,sep='')]
          }
          if (!is.null(thrMFAs)){
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('asanDim.',i,sep='')]
          }
          dfi=merge(CLres.fssd.ALLxy,dfi,by.x=c('cell'),by.y='cell',all.x=TRUE)
          #plot(dfi$x,dfi$y,cex=dfi[,'Dim.1'])
          dfi=dfi[order(dfi$y),]
          range(dfi$x);range(dfi$y)
          MFApcoord.rasteri=rasterFromXYZ(dfi[,c('x','y',paste('Dim.',i,sep=''))],crs=crs)
          names(MFApcoord.rasteri)=paste(paste('Dim.',i,sep=''),' 20',lyears[j],sep='')
          #plot(MFApcoord.rasteri)
          if (j==1){
            MFApcoord.rasterStack=MFApcoord.rasteri
          }else{
            MFApcoord.rasterStack=stack(MFApcoord.rasterStack,MFApcoord.rasteri)
          }
        }
        if (anomalit){
          ptitlef=paste(ptitle,'Coordinates anomaly on MFA axis',i)
        }else if (sanomalit){
          ptitlef=paste(ptitle,'Coordinates anomaly on MFA axis',i,'scaled by eigenvalue')
        }else if (!is.null(thrMFAs)){
          ptitlef=paste(ptitle,'Coordinates standard anomaly on MFA axis',i,'provided thrMFAs')
        }else{  
          ptitlef=paste(ptitle,'Coordinates on MFA axis',i)
        }
        raster.levelplot(MFApcoord.rasterStack,ux11=ux11,lptheme=lptheme,
                         path1=path.export,
                         fid1=paste('MFA',i,'pcoordGeoSpace',sep=''),
                         marginf = marginit,nattri=ptitlef,
                         bathy.plot=bathy.plot,newbathy=newbathy,
                         coastit=coastit)
        if (i==1){
          MFApcoord.rasterStack.year.list=list(MFApcoord.rasterStack)
        }else{
          MFApcoord.rasterStack.year.list=c(MFApcoord.rasterStack.year.list,list(MFApcoord.rasterStack))
        }
      }
      # 1 pcoord plot per month
      for (i in 1:nMFA){
        for (j in 1:length(lmonths)){
          dfi=MFAmpcoord.month[MFAmpcoord.month$month==lmonths[j],]
          if (anomalit){
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('anDim.',i,sep='')]
          }
          if (sanomalit){
            #dfi[,paste('Dim.',i,sep='')]=dfi[,paste('sanDim.',i,sep='')]
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('esanDim.',i,sep='')]
          }
          if (!is.null(thrMFAs)){
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('asanDim.',i,sep='')]
          }
          dfi=merge(CLres.fssd.ALLxy,dfi,by.x=c('cell'),by.y='cell',all.x=TRUE)
          #plot(dfi$x,dfi$y,cex=dfi[,'Dim.1'])
          dfi=dfi[order(dfi$y),]
          range(dfi$x);range(dfi$y)
          MFApcoord.rasteri=rasterFromXYZ(dfi[,c('x','y',paste('Dim.',i,sep=''))],crs=crs)
          names(MFApcoord.rasteri)=paste(paste('Dim.',i,sep=''),'m',lmonths[j],sep='')
          #plot(MFApcoord.rasteri)
          if (j==1){
            MFApcoord.rasterStack=MFApcoord.rasteri
          }else{
            MFApcoord.rasterStack=stack(MFApcoord.rasterStack,MFApcoord.rasteri)
          }
        }
        if (anomalit){
          ptitlef=paste(ptitle,'Coordinates anomaly on MFA axis',i)
        }else if (sanomalit){
          ptitlef=paste(ptitle,'Coordinates anomaly on MFA axis',i,'scaled by eigenvalue')
        }else if (!is.null(thrMFAs)){
          ptitlef=paste(ptitle,'Coordinates standard anomaly on MFA axis',i,'provided thrMFAs')
        }else{  
          ptitlef=paste(ptitle,'Coordinates on MFA axis',i)
        }
        raster.levelplot(MFApcoord.rasterStack,ux11=ux11,lptheme=lptheme,
                         path1=path.export,
                         fid1=paste('MFA',i,'pcoordGeoSpace',sep=''),
                         marginf = marginit,nattri=ptitlef,
                         bathy.plot=bathy.plot,newbathy=newbathy,
                         coastit=coastit)
        if (i==1){
          MFApcoord.rasterStack.month.list=list(MFApcoord.rasterStack)
        }else{
          MFApcoord.rasterStack.month.list=c(MFApcoord.rasterStack.month.list,list(MFApcoord.rasterStack))
        }
      }
      MFApcoord.rasterStack.list=NULL
    }else{
      lyears=unique(MFApcoord$year)
      MFApcoordam=aggregate(MFApcoord[,c('Dim.1','Dim.2','Dim.3')],
                            list(MFApcoord$cell),mean)
      names(MFApcoordam)=c("cell","meanDim.1","meanDim.2","meanDim.3")
      MFApcoordas=aggregate(MFApcoord[,c('Dim.1','Dim.2','Dim.3')],
                            list(MFApcoord$cell),sd)
      names(MFApcoordas)=c("cell","sdDim.1","sdDim.2","sdDim.3")
      MFApcoordams=merge(MFApcoordam,MFApcoordas)
      MFApcoord=merge(MFApcoord,MFApcoordams,by.x='cell',
                      by.y='cell')
      names(MFApcoord)
      MFApcoord$anDim.1=MFApcoord$Dim.1-MFApcoord$meanDim.1
      MFApcoord$anDim.2=MFApcoord$Dim.2-MFApcoord$meanDim.2
      MFApcoord$anDim.3=MFApcoord$Dim.3-MFApcoord$meanDim.3
      MFApcoord$sanDim.1=MFApcoord$anDim.1/MFApcoord$sdDim.1
      MFApcoord$sanDim.2=MFApcoord$anDim.2/MFApcoord$sdDim.2
      MFApcoord$sanDim.3=MFApcoord$anDim.3/MFApcoord$sdDim.3
      MFApcoord$esanDim.1=MFApcoord$anDim.1/MFAeig[1,1]
      MFApcoord$esanDim.2=MFApcoord$anDim.2/MFAeig[2,1]
      MFApcoord$esanDim.3=MFApcoord$anDim.3/MFAeig[3,1]
      head(MFApcoord)
      if (!is.null(thrMFAs)){
        MFApcoord$asanDim.1=MFApcoord$anDim.1/thrMFAs[1]
        MFApcoord$asanDim.2=MFApcoord$anDim.2/thrMFAs[2]
        MFApcoord$asanDim.3=MFApcoord$anDim.3/thrMFAs[3]
      } 
      for (i in 1:nMFA){
        for (j in 1:length(lyears)){
          dfi=MFApcoord[MFApcoord$year==lyears[j],]
          if (anomalit){
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('anDim.',i,sep='')]
          }
          if (sanomalit){
            #dfi[,paste('Dim.',i,sep='')]=dfi[,paste('sanDim.',i,sep='')]
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('esanDim.',i,sep='')]
          }
          if (!is.null(thrMFAs)){
            dfi[,paste('Dim.',i,sep='')]=dfi[,paste('asanDim.',i,sep='')]
          }
          dfi=merge(CLres.fssd.ALLxy,dfi,by.x=c('cell'),by.y='cell',all.x=TRUE)
          #plot(dfi$x,dfi$y,cex=dfi[,'Dim.1'])
          dfi=dfi[order(dfi$y),]
          range(dfi$x);range(dfi$y)
          MFApcoord.rasteri=rasterFromXYZ(dfi[,c('x','y',paste('Dim.',i,sep=''))],crs=crs)
          names(MFApcoord.rasteri)=paste(paste('Dim.',i,sep=''),' 20',lyears[j],sep='')
          #plot(MFApcoord.rasteri)
          if (j==1){
            MFApcoord.rasterStack=MFApcoord.rasteri
          }else{
            MFApcoord.rasterStack=stack(MFApcoord.rasterStack,MFApcoord.rasteri)
          }
        }
        if (anomalit){
          ptitlef=paste(ptitle,'Coordinates anomaly on MFA axis',i)
        }else if (sanomalit){
          ptitlef=paste(ptitle,'Coordinates anomaly on MFA axis',i,'scaled by eigenvalue')
        }else if (!is.null(thrMFAs)){
          ptitlef=paste(ptitle,'Coordinates standard anomaly on MFA axis',i,'provided thrMFAs')
        }else{  
          ptitlef=paste(ptitle,'Coordinates on MFA axis',i)
        }
        raster.levelplot(MFApcoord.rasterStack,ux11=ux11,lptheme=lptheme,
                         path1=path.export,
                         fid1=paste('MFA',i,'pcoordGeoSpace',sep=''),
                         marginf = marginit,nattri=ptitlef,
                         bathy.plot=bathy.plot,newbathy=newbathy,
                         coastit=coastit)
        if (i==1){
          MFApcoord.rasterStack.list=list(MFApcoord.rasterStack)
        }else{
          MFApcoord.rasterStack.list=c(MFApcoord.rasterStack.list,list(MFApcoord.rasterStack))
        }
      }
      MFApcoord.rasterStack.year.list=NULL
      MFApcoord.rasterStack.month.list=NULL
    }
     length(MFApcoord.rasterStack.list)
  }else{
    MFApcoord.rasterStack.list=NULL
    MFApcoord.rasterStack.year.list=NULL
    MFApcoord.rasterStack.month.list=NULL
  }
  list(MFAcoord.rasterStack=MFAcoord.rasterStack,MFAcontrib.rasterStack=MFAcontrib.rasterStack,
       MFAcos2.rasterStack=MFAcos2.rasterStack,MFAcoordMcos2.rasterStack=MFAcoordMcos2.rasterStack,
       MFAcoordMcontrib.rasterStack=MFAcoordMcontrib.rasterStack,MFApcoord.rasterStack.list,
       MFAinertia=MFAinertia,MFAinertia.raster=MFAinertia.raster)
}


prep.MFApcoord=function(MFApcoord,MFAeig,thrMFAs=NULL){
  MFApcoordam=aggregate(MFApcoord[,c('Dim.1','Dim.2','Dim.3')],
                        list(MFApcoord$cell),mean)
  names(MFApcoordam)=c("cell","meanDim.1","meanDim.2","meanDim.3")
  MFApcoordas=aggregate(MFApcoord[,c('Dim.1','Dim.2','Dim.3')],
                        list(MFApcoord$cell),sd)
  names(MFApcoordas)=c("cell","sdDim.1","sdDim.2","sdDim.3")
  MFApcoordams=merge(MFApcoordam,MFApcoordas)
  MFApcoord=merge(MFApcoord,MFApcoordams,by.x='cell',
                  by.y='cell')
  names(MFApcoord)
  MFApcoord$anDim.1=MFApcoord$Dim.1-MFApcoord$meanDim.1
  MFApcoord$anDim.2=MFApcoord$Dim.2-MFApcoord$meanDim.2
  MFApcoord$anDim.3=MFApcoord$Dim.3-MFApcoord$meanDim.3
  MFApcoord$sanDim.1=MFApcoord$anDim.1/MFApcoord$sdDim.1
  MFApcoord$sanDim.2=MFApcoord$anDim.2/MFApcoord$sdDim.2
  MFApcoord$sanDim.3=MFApcoord$anDim.3/MFApcoord$sdDim.3
  MFApcoord$esanDim.1=MFApcoord$anDim.1/MFAeig[1,1]
  MFApcoord$esanDim.2=MFApcoord$anDim.2/MFAeig[2,1]
  MFApcoord$esanDim.3=MFApcoord$anDim.3/MFAeig[3,1]
  head(MFApcoord)
  if (!is.null(thrMFAs)){
    MFApcoord$asanDim.1=MFApcoord$anDim.1/thrMFAs[1]
    MFApcoord$asanDim.2=MFApcoord$anDim.2/thrMFAs[2]
    MFApcoord$asanDim.3=MFApcoord$anDim.3/thrMFAs[3]
  }
  MFApcoord
}


BoB.MFA.rasterPlot=function(MFAres,nMFA=3,cellsel,pipo,ptitle='',path.export=NULL,
                            plotit=list(MFA123coord=TRUE,MFA123timevar=TRUE,
                                        MFA123contrib=TRUE,MFA123cos2=TRUE,
                                        MFA123coordMcos2=TRUE,
                                        MFA123coordMcontrib=TRUE,MFA123pind=FALSE),
                            funSel='mean',anomalit=FALSE,sanomalit=FALSE,ux11=FALSE,
                            lptheme=viridisTheme,marginit=FALSE,
                            Ndim=list(dim(MFAres$ind$coord)[2]),thrMFAs=NULL,
                            crs=CRS("+proj=longlat +datum=WGS84"),...){
  if (class(MFAres)[1]=="MFA"){
    MFAcoord=MFAres$ind$coord
    MFAcos2=MFAres$ind$cos2
    MFAcontrib=MFAres$ind$contrib
    head(MFAcoord)
    head(MFAcos2)
    rnsplit=unlist(strsplit(row.names(MFAcoord),split='[.]'))
    if (length(rnsplit)>length(row.names(MFAcoord))){
      cellnames=unlist(strsplit(row.names(MFAcoord),split='[.]'))[seq(1,(2*length(row.names(MFAcoord))),2)]
      row.names(MFAcoord)=cellnames
    }
    MFApcoord=data.frame(MFAres$ind$coord.partiel)
    head(MFApcoord)
    MFApcoord$cell=unlist(strsplit(row.names(MFApcoord),split='[.]'))[seq(1,(2*length(row.names(MFApcoord))),2)]
    MFApcoord$year=unlist(strsplit(row.names(MFApcoord),split='[.]'))[seq(2,(2*length(row.names(MFApcoord))),2)]
    MFAeig=MFAres$eig
  }else if (class(MFAres)[1]=="data.frame"){
    MFAcoord=MFAres
    rnsplit=unlist(strsplit(row.names(MFAcoord),split='[.]'))
    if (length(rnsplit)>length(row.names(MFAcoord))){
      cellnames=unlist(strsplit(row.names(MFAcoord),split='[.]'))[seq(1,(2*length(row.names(MFAcoord))),2)]
      row.names(MFAcoord)=cellnames
    }
    MFAcos2=NULL
    MFAcontrib=NULL
    MFApcoord=NULL
    head(MFAcoord)
  }else if (class(MFAres)[1]=="matrix"){
    MFAcoord=MFAres
    rnsplit=unlist(strsplit(row.names(MFAcoord),split='[.]'))
    if (length(rnsplit)>length(row.names(MFAcoord))){
      cellnames=unlist(strsplit(row.names(MFAcoord),split='[.]'))[seq(1,(2*length(row.names(MFAcoord))),2)]
      row.names(MFAcoord)=cellnames
    }
    MFAcos2=NULL
    MFAcontrib=NULL
    MFApcoord=NULL
    head(MFAcoord)
  }  
  # Merge MFA results and cell positions
  CLres.fssd.ALLxy=pipo[,c('Xgd','Ygd','I','J')]
  head(CLres.fssd.ALLxy)
  names(CLres.fssd.ALLxy)[1:2]=c('x','y')
  CLres.fssd.ALLxy=CLres.fssd.ALLxy[order(CLres.fssd.ALLxy$y),]
  CLres.fssd.ALLxy$cell=paste(CLres.fssd.ALLxy$I,CLres.fssd.ALLxy$J,sep='-')
  MFAcoord.ALLxy=merge(CLres.fssd.ALLxy,MFAcoord,by.x=c('cell'),by.y='row.names',all.x=TRUE)
  head(MFAcoord.ALLxy)
  #plot(MFAcoord.ALLxy$x,MFAcoord.ALLxy$y,cex=MFAcoord.ALLxy$Dim.1+10/10)
  #coast()
  if (!is.null(MFAcontrib)){
    MFAcontrib.ALLxy=merge(CLres.fssd.ALLxy,MFAcontrib,by.x=c('cell'),by.y='row.names',all.x=TRUE)
    MFAcontrib.ALLxy=MFAcontrib.ALLxy[order(MFAcontrib.ALLxy$y),]
  }
  if (!is.null(MFAcos2)){
    MFAcos2.ALLxy=merge(CLres.fssd.ALLxy,MFAcos2,by.x=c('cell'),by.y='row.names',all.x=TRUE)
    MFAcos2.ALLxy=MFAcos2.ALLxy[order(MFAcos2.ALLxy$y),]
  } 
  if (plotit$MFA123coord){
    # Plot individuals mean coordinates on MFA axis in geographical space
    #*************************************************************
    for (i in 1:nMFA){
      if (sanomalit){
        MFAcoord.ALLxy[,c(paste('Dim.',i,sep=''))]=MFAcoord.ALLxy[,c(paste('Dim.',i,sep=''))]/MFAeig[i,1]
      }
      MFAcoord.rasteri=rasterFromXYZ(MFAcoord.ALLxy[,c('x','y',paste('Dim.',i,sep=''))],
                                     crs=crs)
      if (i==1){
        MFAcoord.rasterStack=MFAcoord.rasteri
      }else{
        MFAcoord.rasterStack=stack(MFAcoord.rasterStack,MFAcoord.rasteri)
      }
    }
    raster.levelplot.PELGAS(MFAcoord.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcoordGeoSpace',
                            margin = margin,nattri=paste(ptitle,'MFA coordinates'),...)
  }else{
    MFAcoord.rasterStack=NULL
  }
  if (plotit$MFA123timevar){
    # Inter-annual variability in cells
    #*************************************************************
    # distance between cell mean position (individuals) and annual cell positions (partial individuals)
    coord.dist<- merge(MFAcoord.ALLxy,MFApcoord, by.x= "cell", by.y= "cell")
    for (k in 1:length(Ndim)){
      for (i in Ndim[[k]]){
        di=(coord.dist[,paste('Dim.',i,'.x',sep='')]-coord.dist[,paste('Dim.',i,'.y',sep='')])^2
        if (i==Ndim[[k]][1]) {
          dt=di
        }else{
          dt=dt+di
        }
      }
      coord.dist$distance<- sqrt(dt)
      head(coord.dist)
      coord.dista=aggregate(coord.dist$distance,list(cell=coord.dist$cell),mean)
      names(coord.dista)[2]='meanD2bary'
      coord.dista=merge(coord.dista,CLres.fssd.ALLxy[,c('cell','x','y','I','J')],all.y=TRUE)
      head(coord.dista)
      coord.dista=coord.dista[order(coord.dista$y,coord.dista$x),]
      CLres.fssdD.raster=raster(ncol=length(unique(coord.dista$x)),
                                nrow=length(unique(coord.dista$y)),
                                xmn=min(unique(coord.dista$x)),xmx=max(unique(coord.dista$x)), 
                                ymn=min(unique(coord.dista$y)), ymx=max(unique(coord.dista$y)),
                                crs=crs)
      values(CLres.fssdD.raster) <- coord.dista[,'meanD2bary']
      CLres.fssdD.raster=flip(CLres.fssdD.raster,2)
      Dim1=min(Ndim[[k]]);DimN=max(Ndim[[k]])
      coord.dista$MFAs=paste(Dim1,'-',DimN,sep='')
      names(CLres.fssdD.raster)=paste('MFA',Dim1,'-',DimN,sep='')
      raster.levelplot.PELGAS(CLres.fssdD.raster,ux11=ux11,lptheme=lptheme,
                              path1=path.export,fid1=paste(Dim1,'-',DimN,'MFAvarGeoSpace',sep=''),
                              margin = margin,
                              nattri=paste(ptitle,' ',Dim1,'-',DimN,' MFA coordinates inertia',sep=''),
                              layout=c(1,1))
      if (k==1){
        MFAinertia=list(coord.dista)
        MFAinertia.raster=CLres.fssdD.raster
      }else{
        MFAinertia=c(MFAinertia,list(coord.dista))
        MFAinertia.raster=stack(MFAinertia.raster,CLres.fssdD.raster)
      }
    }
  }else{
    MFAinertia=NA
    MFAinertia.raster=NA
  }
  if (plotit$MFA123contrib){
    # Plot individuals contribution to MFA axis in geographical space
    #*************************************************************
    for (i in 1:nMFA){
      MFAcontrib.rasteri=rasterFromXYZ(MFAcontrib.ALLxy[,c('x','y',paste('Dim.',i,sep=''))],
                                       crs=crs)
      maskMFAcontrib.rasteri=MFAcontrib.rasteri>cellStats(MFAcontrib.rasteri, funSel)
      maskMFAcontrib.rasteri=reclassify(maskMFAcontrib.rasteri, matrix(c(0,1,NA,1),ncol=2))
      if (i==1){
        MFAcontrib.rasterStack=MFAcontrib.rasteri
        maskMFAcontrib.rasterStack=maskMFAcontrib.rasteri
      }else{
        MFAcontrib.rasterStack=stack(MFAcontrib.rasterStack,MFAcontrib.rasteri)
        maskMFAcontrib.rasterStack=stack(maskMFAcontrib.rasterStack,maskMFAcontrib.rasteri)
      }
    }
    raster.levelplot.PELGAS(MFAcontrib.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcontribGeoSpace',
                            margin = margin,nattri=paste(ptitle,'Contribution to MFA axes'),...)
   }else{
    MFAcontrib.rasterStack=NULL
    maskMFAcontrib.rasterStack=NULL
  }
  if (plotit$MFA123cos2){
    # Plot individuals quality of representation on MFA axis in geographical space
    #*************************************************************
    for (i in 1:nMFA){
      MFAcos2.rasteri=rasterFromXYZ(MFAcos2.ALLxy[,c('x','y',paste('Dim.',i,sep=''))],
                                    crs=crs)
      maskMFAcos2.rasteri=MFAcos2.rasteri>cellStats(MFAcos2.rasteri, funSel)
      maskMFAcos2.rasteri=reclassify(maskMFAcos2.rasteri, matrix(c(0,1,NA,1),ncol=2))
      if (i==1){
        MFAcos2.rasterStack=MFAcos2.rasteri
        maskMFAcos2.rasterStack=maskMFAcos2.rasteri
      }else{
        MFAcos2.rasterStack=stack(MFAcos2.rasterStack,MFAcos2.rasteri)
        maskMFAcos2.rasterStack=stack(maskMFAcos2.rasterStack,maskMFAcos2.rasteri)
      }
    }
    raster.levelplot.PELGAS(MFAcos2.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcos2GeoSpace',
                            margin = margin,
                            nattri=paste(ptitle,'Quality of representation on MFA axes'),...)
  }else{
    MFAcos2.rasterStack=NULL
    maskMFAcos2.rasterStack=NULL
  }
  if (plotit$MFA123coordMcos2){
    # Plot individuals quality of representation on MFA axis in geographical space
    #*************************************************************
    for (i in 1:nMFA){
      MFAcoord.rasteri=raster(MFAcoord.rasterStack,i)
      maskMFAcos2.rasteri=raster(maskMFAcos2.rasterStack,i)
      MFAcoord.rasteris=mask(MFAcoord.rasteri,maskMFAcos2.rasteri)
      if (i==1){
        MFAcoordMcos2.rasterStack=MFAcoord.rasteris
      }else{
        MFAcoordMcos2.rasterStack=stack(MFAcoordMcos2.rasterStack,MFAcoord.rasteris)
      }
    }
    raster.levelplot.PELGAS(MFAcoordMcos2.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcoordMaskCos2GeoSpace',
                            margin = margin,
                            nattri=paste(ptitle,'Coordinates on MFA axes of well represented cells'),...)
  }else{
    MFAcoordMcos2.rasterStack=NULL
  }
  if (plotit$MFA123coordMcontrib){
    # Plot individuals quality of representation on MFA axis in geographical space
    #*************************************************************
    for (i in 1:nMFA){
      MFAcoord.rasteri=raster(MFAcoord.rasterStack,i)
      maskMFAcontrib.rasteri=raster(maskMFAcontrib.rasterStack,i)
      MFAcoord.rasteris=mask(MFAcoord.rasteri,maskMFAcontrib.rasteri)
      if (i==1){
        MFAcoordMcontrib.rasterStack=MFAcoord.rasteris
      }else{
        MFAcoordMcontrib.rasterStack=stack(MFAcoordMcontrib.rasterStack,MFAcoord.rasteris)
      }
    }
    raster.levelplot.PELGAS(MFAcoordMcontrib.rasterStack,ux11=ux11,lptheme=lptheme,
                            path1=path.export,fid1='MFAcoordMaskContribGeoSpace',
                            margin = margin,
                            nattri=paste(ptitle,'Coordinates on MFA axes of cells contributing above average to MFA axes'),...)
  }else{
    MFAcoordMcontrib.rasterStack=NULL
  }
  if (!is.null(MFApcoord)&plotit$MFA123pind){
    head(MFApcoord)
    lyears=unique(MFApcoord$year)
    MFApcoordam=aggregate(MFApcoord[,c('Dim.1','Dim.2','Dim.3')],
                                list(MFApcoord$cell),mean)
    names(MFApcoordam)=c("cell","meanDim.1","meanDim.2","meanDim.3")
    MFApcoordas=aggregate(MFApcoord[,c('Dim.1','Dim.2','Dim.3')],
                                list(MFApcoord$cell),sd)
    names(MFApcoordas)=c("cell","sdDim.1","sdDim.2","sdDim.3")
    MFApcoordams=merge(MFApcoordam,MFApcoordas)
    
    MFApcoord=merge(MFApcoord,MFApcoordams,by.x='cell',
                          by.y='cell')
    names(MFApcoord)
    MFApcoord$anDim.1=MFApcoord$Dim.1-MFApcoord$meanDim.1
    MFApcoord$anDim.2=MFApcoord$Dim.2-MFApcoord$meanDim.2
    MFApcoord$anDim.3=MFApcoord$Dim.3-MFApcoord$meanDim.3
    MFApcoord$sanDim.1=MFApcoord$anDim.1/MFApcoord$sdDim.1
    MFApcoord$sanDim.2=MFApcoord$anDim.2/MFApcoord$sdDim.2
    MFApcoord$sanDim.3=MFApcoord$anDim.3/MFApcoord$sdDim.3
    MFApcoord$esanDim.1=MFApcoord$anDim.1/MFAeig[1,1]
    MFApcoord$esanDim.2=MFApcoord$anDim.2/MFAeig[2,1]
    MFApcoord$esanDim.3=MFApcoord$anDim.3/MFAeig[3,1]
    if (!is.null(thrMFAs)){
      MFApcoord$asanDim.1=MFApcoord$anDim.1/thrMFAs[1]
      MFApcoord$asanDim.2=MFApcoord$anDim.2/thrMFAs[2]
      MFApcoord$asanDim.3=MFApcoord$anDim.3/thrMFAs[3]
    } 
    for (i in 1:nMFA){
      for (j in 1:length(lyears)){
        dfi=MFApcoord[MFApcoord$year==lyears[j],]
        if (anomalit){
          dfi[,paste('Dim.',i,sep='')]=dfi[,paste('anDim.',i,sep='')]
        }
        if (sanomalit){
          #dfi[,paste('Dim.',i,sep='')]=dfi[,paste('sanDim.',i,sep='')]
          dfi[,paste('Dim.',i,sep='')]=dfi[,paste('esanDim.',i,sep='')]
        }
        if (!is.null(thrMFAs)){
          dfi[,paste('Dim.',i,sep='')]=dfi[,paste('asanDim.',i,sep='')]
        }
        dfi=merge(CLres.fssd.ALLxy,dfi,by.x=c('cell'),by.y='cell',all.x=TRUE)
        #plot(dfi$x,dfi$y,cex=dfi[,'Dim.1'])
        dfi=dfi[order(dfi$y),]
        range(dfi$x);range(dfi$y)
        MFApcoord.rasteri=rasterFromXYZ(dfi[,c('x','y',paste('Dim.',i,sep=''))],crs=crs)
        names(MFApcoord.rasteri)=paste(paste('Dim.',i,sep=''),' 20',lyears[j],sep='')
        #plot(MFApcoord.rasteri)
        if (j==1){
          MFApcoord.rasterStack=MFApcoord.rasteri
        }else{
          MFApcoord.rasterStack=stack(MFApcoord.rasterStack,MFApcoord.rasteri)
        }
      }
      if (anomalit){
        ptitlef=paste(ptitle,'Coordinates anomaly on MFA axis',i)
      }else if (sanomalit){
          ptitlef=paste(ptitle,'Coordinates anomaly on MFA axis',i,'scaled by eigenvalue')
      }else if (!is.null(thrMFAs)){
        ptitlef=paste(ptitle,'Coordinates standard anomaly on MFA axis',i,'provided thrMFAs')
      }else{  
        ptitlef=paste(ptitle,'Coordinates on MFA axis',i)
      }
      raster.levelplot.PELGAS(MFApcoord.rasterStack,ux11=ux11,lptheme=lptheme,
                              path1=path.export,fid1=paste('MFA',i,'pcoordGeoSpace',sep=''),
                              margin = margin,nattri=ptitlef,...)
      if (i==1){
        MFApcoord.rasterStack.list=list(MFApcoord.rasterStack)
      }else{
        MFApcoord.rasterStack.list=c(MFApcoord.rasterStack.list,list(MFApcoord.rasterStack))
      }
    }  
    length(MFApcoord.rasterStack.list)
  }else{
    MFApcoord.rasterStack.list=NULL
  }
  list(MFAcoord.rasterStack=MFAcoord.rasterStack,MFAcontrib.rasterStack=MFAcontrib.rasterStack,
       MFAcos2.rasterStack=MFAcos2.rasterStack,MFAcoordMcos2.rasterStack=MFAcoordMcos2.rasterStack,
       MFAcoordMcontrib.rasterStack=MFAcoordMcontrib.rasterStack,MFApcoord.rasterStack.list,
       MFAinertia=MFAinertia,MFAinertia.raster=MFAinertia.raster)
}

plotellipses.MFA=function(model,keepvar,axes,level=0.95,...){
  
  aux <- cbind.data.frame(model$call$X[,keepvar, drop = FALSE], model$ind$coord[, 1:max(axes)])
  res.pca <- PCA(aux, quali.sup = 1, scale.unit = FALSE,graph = FALSE)
  res.pca$eig[1:max(axes), ] = model$eig[1:max(axes), ]
  #dimnames(res.pca$eig)[[1]]=paste("comp",axes)
  coord.ell <- coord.ellipse(coord.simul=aux[,c(1,(axes+1))], bary = TRUE, level.conf = level)
  plot.PCA(res.pca, habillage = 1, ellipse = coord.ell, axes = axes,
           title = paste("Confidence ellipses around the categories of", 
                         colnames(model$call$X)[var]),...)
}

Fishcluster.arcomp=function(CLres.fssd.ALL.MFA,cname='clumps'){
  CLres.fssd.ALL.MFA.am=aggregate(CLres.fssd.ALL.MFA$Zvalue,list(clust=CLres.fssd.ALL.MFA[,cname],
                                                                   sp=CLres.fssd.ALL.MFA$sp),mean,na.rm=TRUE)
  names(CLres.fssd.ALL.MFA.am)[3]='mZ'
  CLres.fssd.ALL.MFA.as=aggregate(CLres.fssd.ALL.MFA$Zvalue,list(clust=CLres.fssd.ALL.MFA[,cname],
                                                                   sp=CLres.fssd.ALL.MFA$sp),sd,na.rm=TRUE)
  names(CLres.fssd.ALL.MFA.as)[3]='sdZ'
  CLres.fssd.ALL.MFAa=merge(CLres.fssd.ALL.MFA.am,CLres.fssd.ALL.MFA.as)
  CLres.fssd.ALL.MFAa=CLres.fssd.ALL.MFAa[!is.na(CLres.fssd.ALL.MFAa$mZ),]
  # Compute relative mean biomass
  CLres.fssd.ALL.MFAaClustTot=aggregate(CLres.fssd.ALL.MFAa$mZ,list(clust=CLres.fssd.ALL.MFAa$clust),sum,na.rm=TRUE)
  names(CLres.fssd.ALL.MFAaClustTot)[2]="mZtot"
  CLres.fssd.ALL.MFAa=merge(CLres.fssd.ALL.MFAa,CLres.fssd.ALL.MFAaClustTot)
  head(CLres.fssd.ALL.MFAa)
  CLres.fssd.ALL.MFAa$pmZ=CLres.fssd.ALL.MFAa$mZ/CLres.fssd.ALL.MFAa$mZtot
  aggregate(CLres.fssd.ALL.MFAa$pmZ,list(clust=CLres.fssd.ALL.MFAa$clust),sum)
  names(CLres.fssd.ALL.MFAa)[2]='spSD'
  spSDsplit=data.frame(strsplit(as.character(CLres.fssd.ALL.MFAa$spSD),split=' '))
  CLres.fssd.ALL.MFAa$sp=as.character(unlist(spSDsplit[1,]))
  CLres.fssd.ALL.MFAa$cz=factor(as.character(unlist(spSDsplit[2,])),ordered = TRUE,
                                 levels=c('Surface','Bottom'))
  CLres.fssd.ALL.MFAa$cs=factor(as.character(unlist(spSDsplit[3,])),ordered = TRUE,
                                 levels=c('(0,5]','(5,10]','(10,15]','(15,20]','(20,25]','(25,30]','(30,35]','(35,40]','(40,45]','(45,50]',
                                          '(50,55]','(55,60]'))
  CLres.fssd.ALL.MFAa
}

anova.maps=function(df,df.id='',varname='',path.export=NULL,
                    qplot=FALSE,quantiles=c(.25,.75)){
  dfs=df[,-seq(2)]
  # time average
  x11()
  par(mfrow=c(3,2),bg='white')
  if (qplot){
    ylimi=range(c(apply(dfs,2,quantile,quantiles[1]),
                  apply(dfs,2,quantile,quantiles[2])))
  }else{
    ylimi=NULL
  }
  plot(apply(dfs,2,mean),
       main=paste(varname,'maps average over time'),xlab='time',
       ylab=varname,type='b',ylim=ylimi)
  # segments(x0=seq(length(apply(dfs,2,mean))),
  #          y0=apply(dfs,2,mean)-2*apply(dfs,2,sd),
  #          x1=seq(length(apply(dfs,2,mean))),
  #          y1=apply(dfs,2,mean)+2*apply(dfs,2,sd))
  if (qplot){
    segments(x0=seq(length(apply(dfs,2,mean))),
             y0=apply(dfs,2,quantile,quantiles[1]),
             x1=seq(length(apply(dfs,2,mean))),y1=apply(dfs,2,
                                                        quantiles[2]))
  }
  plot(apply(dfs,2,var),
       main=paste(varname,'maps variance over time'),xlab='time',
       ylab=varname,type='b')
  plot(apply(dfs,2,mean),
       apply(dfs,2,var),
       main=paste(varname,'maps average vs. variance over time'),
       xlab='Average',ylab='Variance',type='p')
  # space average
  cex.pos=abs(min(apply(dfs,1,mean)))
  cex.scale=1+abs(ceiling(sd(apply(dfs,1,mean)+cex.pos)))
  plot(df$x,df$y,cex=(apply(dfs,1,mean)+cex.pos)/cex.scale,
       main=paste(varname,'mean map'),xlab='x',ylab='y')
  cex.pos=abs(min(apply(dfs,1,var)))
  cex.scale=1+abs(ceiling(sd(apply(dfs,1,var)+cex.pos)))
  plot(df$x,df$y,cex=apply(dfs,1,var)/cex.scale,
       main=paste(varname,'variance map'),xlab='x',ylab='y')
  
  anova.spacetime=data.frame(id=df.id)
  # Variance of mean maps over time (mean time space)
  anova.spacetime$time.var=var(apply(dfs,2,mean))
  # Variance of mean map (mean space space)
  anova.spacetime$space.var=var(apply(dfs,1,mean))
  # total variance
  anova.spacetime$tot.var=var(unlist(dfs))
  # time:space variance
  anova.spacetime$other.var=anova.spacetime$tot.var-
    (anova.spacetime$time.var+anova.spacetime$space.var)
  anova.spacetime$rtime.var=anova.spacetime$time.var/anova.spacetime$tot.var
  anova.spacetime$rspace.var=anova.spacetime$space.var/anova.spacetime$tot.var
  anova.spacetime$rother.var=anova.spacetime$other.var/
    anova.spacetime$tot.var
  barplot(as.matrix(
    anova.spacetime[,c('rtime.var','rspace.var','rother.var')]))
  par(mfrow=c(1,1))
  if (!is.null(path.export)){
    # Save figures
    dev.print(png, file=paste(path.export,varname,'_',df.id,'_',
                              "anovaMaps.png",sep=""), 
              units='cm',width=40, height=30,res=300)
  }
  anova.spacetime
}

