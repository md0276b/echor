names(world.map)
range(world.map$range)
world.map.x360=world.map
world.map.x360$x[!is.na(world.map.x360$x)&world.map.x360$x<0]=world.map.x360$x[!is.na(world.map.x360$x)&world.map.x360$x<0]+360
range(world.map.x360$x)

wpdf=data.frame(x=world.map.x360$x,y=world.map.x360$y)
wpdf[!is.na(wpdf$x)&(wpdf$x>359|wpdf$x<1),c('x','y')]=c(NA,NA)

world.map.x360$x=wpdf$x
world.map.x360$y=wpdf$y



coast=function(database = "world.map", xlim = c(-180, 180), ylim = c(-90, 90),
   add = T, plot = T, color = 1, lwd = 1,fill=F,...)
{
   database <- get(database)
   if(plot) {
       if(add){
           if (fill==F){lines(database, col = color, err = -1, lwd = lwd,...)
		}else{polygon(database$x,database$y,col=color,border=color)}
       }else {
		if (fill==F){plot(database, type = "l", xlim = xlim, ylim = ylim, col
                = color, lwd = lwd,...)
		}else{polygon(database$x,database$y,col=color,border=color)}
	}
       res <- NULL
   }else {
       x <- database$x
       y <- database$y
       sel <- (x >= xlim[1]) & (x <= xlim[2]) & (y >= ylim[1]) & (
           y <= ylim[2])
       x <- x[sel]
       y <- y[sel]
       res <- list(x = x, y = y)
   }
   res
}
