#Install packages (optionnal)-----------------------------
#install.packages('S:/PELGAS11/SUMATRA/R4Sumatra/RODBC_1.3-2.zip',repos=NULL)
#library(RODBC)
  
# FishRview: swiss knife/workhorse function for 
# fusionning acoustic, log and fishing data
#Arguments:
#  path.export.cas: path where to export pro casino-compliant rows
#  stp.events: haul start event(s)
#  WL.recompute=0: shows length-weight relationships but no fishing data 
#                 correction
#  WL.recompute=1: re-compute mean length and weight based on raw biological measurements
#  WL.recompute=2: re-compute mean length and weight based on length-weight equations
#  WL.recompute=3: re-compute commercial mean length and weight based on length-weight equations

FishRview=function(path.EspDev=NULL,path.cas,path.man=NULL,newman=TRUE,
  path.raptri,path.raptri.pro=NULL,path.mens=NULL,path.mens.pro=NULL,
  path.metapeche=NULL,path.export=NULL,cruise,datestart=NULL,
  dateend=NULL,path.discard=NULL,nid='NOCHAL',appendit=FALSE,
  path.TS=NULL,promoulit=TRUE,raptri.fusion=0,lstrates=c('CLAS','SURF','NULL'),
  WL.recompute=0,verbose=FALSE,stp.gears=c('76x70','57x52','942OBS','EROCENROL'),
  logit=TRUE,stp.events=c('DFIL','DPLON'),pradius=0.8,smax=0.5,stopIfError=TRUE,
  lsp=c('ENGR.ENC','MICR.POU','SARD.PIL','SCOM.SCO','SPRA.SPR','TRAC.TRU'),
  xlim=NULL,ylim=NULL,plot.species=TRUE,legpos='bottomleft',
  cas.names=c('Date','Heure','Latitude.deg.min.milli.','Longitude.deg.min.milli.',
              'Code Appareil','Code Action','No Station','Strate','CINNA Sonde vert (m)','Type'),
  casino.sep=',',SURFdepthLim=50,xname='long',yname='lat',repSTRATE='CLAS',
  dfc.med=data.frame(spmed=c('ENGR-ENG','SARD-PIG','SARD-PIP','SCOM-SCO','SPRA-SPR','TRAC-TRG',
          'DIVE-RS1','SCOM-JAP','TRAC-MEP','TRAG-MEG','TRAC-MEG','TRAC-TRP',
          'SARI-AUR','TRAC-PIC'),sp=c('ENGR-ENC','SARD-PIL','SARD-PIL','SCOM-SCO','SPRA-SPR','TRAC-TRU',
       'DIVE-RS1','SCOM-JAP','TRAC-MED','TRAC-MED','TRAC-MED',
       'TRAC-TRU','SARI-AUR','TRAC-PIC'),
       size=c('0','G','0','0','0','G','0','0','0','G','G','0','0','0')),
  numCol.route=c(1,2,3,4,9,11,14,15,30,40,43),udec='.',
  spcol=data.frame(sp=c("ENGR.ENC","MICR.POU","SARD.PIL","SCOM.SCO",
                        "SPRA.SPR","TRAC.TRU","CAPR.APE","SCOM.JAP","TRAC.MED",
                        "CLUP.HAR","COMP.LEM","MERL.MNG","MERL.MER"),
                   sp2=c("ENGR-ENC","MICR-POU","SARD-PIL","SCOM-SCO",
                         "SPRA-SPR","TRAC-TRU","CAPR-APE","SCOM-JAP","TRAC-MED",
                         "CLUP-HAR","COMP-LEM","MERL-MNG","MERL-MER"),
                   lcol=c(3,8,4,2,1,7,6,5,0,0,0,0,0)),correctHaulz=TRUE,
  getNOAAbathy=TRUE,TSref=FALSE){

#Import EspDevi------------------------------
  if (!is.null(path.EspDev)){
  #write.table(EspDevi,
   # 'F:/Projects/Acoustic biomass assessment/Data/PELGAS11/EspDevi.txt',
    #row.names=FALSE,sep=';')
    #path.EspDev='~/.gvfs/donnees2 sur nantes/Campagnes/PELGAS2012/Evaluation_stock/donnees/PELGAS2012_ESPDEV.csv'
    EspDevi= read.table(path.EspDev,header=TRUE,sep=';')
    #Select fish echotypes
    EspDevi=EspDevi[!EspDevi$GENR_ESP%in%c('PLNC-TON','FOND-BUL'),]
    Ndevi=as.numeric(gsub('\\D','',unique(EspDevi$DEV)))
    if (!is.null(path.export)){
      write.table(EspDevi,paste(path.export,'Echotypes.csv',sep=''),
                  row.names=FALSE,sep = ";")
      cat('Definitions des echotypes exportees vers:',paste(path.export,'Echotypes.csv',sep=''),'\n')
    }
  }else{
    cat('Pas de definition d echotype fournie','\n')
    EspDevi=NA
    Ndevi=NA
  }  

#Import Casino metadata------------------------------

  cas=read.csv(path.cas,header=TRUE,sep=casino.sep)
  cas=cas[,!names(cas)%in%c('X','X.1','X.2')]
  casn=read.csv(path.cas,header=FALSE,sep=casino.sep)
  casn2=as.matrix(casn[1,])
  nc2=c(as.character(casn2))
  nc2=nc2[!is.na(nc2)]
  nc2=nc2[nc2!='']
  
  # convert zname in casino name format to RcasinoName format
  RcasinoNames=data.frame(Rname=names(cas),casName=nc2)
  RcasinoNames=RcasinoNames[complete.cases(RcasinoNames),]
  RcasinoNames[,1]=as.character(RcasinoNames[,1])
  RcasinoNames[,2]=as.character(RcasinoNames[,2])
  RcasinoNames=RcasinoNames[RcasinoNames$Rname!='X',]
  
  #Import Casino stations
  #names(cas)
  if (substr(cruise,1,6)=='PELMED'){
    # evts=cas[cas$Type!='ACQAUTO',c('Date','Heure','Latitude.deg...min.milli.',
    #                                'Longitude.deg...min.milli.','Code.Appareil','Code.Action',
    #                                'N.Station','Strate','Sonde.m.')]
    evts = cas[cas$Type!="ACQAUTO",cas.names]
  }else if (substr(cruise,1,6)=='PELGAS'){
    if (sum(!cas.names%in%RcasinoNames$casName)>0){
      stop(as.character(RcasinoNames$Rname[RcasinoNames$Rname%in%cas.names]),
' Entetes de casino non trouvees dans le fichier du fait de changement de formats aleatoires, adressez vos reclamations a mpcorre@ifremer.fr')
    }else{
      evts=cas[cas$Type=='OPE',as.character(RcasinoNames[RcasinoNames$casName%in%cas.names,'Rname'])]      
    } 
  } 
  names(evts)=c('date','heure','LATF','LONF','Type','Nom.Appareil','GEAR',"Nom.Action",'EVT',
                "Identificateur.Operation","Observation",'NOSTA',
                'STRATEL','SONDE')
  evts$SONDE=as.numeric(gsub(',','.',evts$SONDE))
  evts$LATF=as.numeric(gsub(',','.',evts$LATF))
  evts$LONF=as.numeric(gsub(',','.',evts$LONF))  
  #enleve les espaces en trop dans date et heure
  stp=evts[evts$GEAR%in%stp.gears,]    
  stp$date=gsub(' ','',stp$date)
  stp$heure=gsub(' ','',stp$heure)
  stp=stp[stp$EVT%in%stp.events,]
  stp$NOCHAL=NA
  stp$TC=paste(stp$date,stp$heure)
  stp$STRATE=substr(stp$STRATEL,1,4)
  stp$SONDE=as.numeric(gsub(',','.',stp$SONDE))
  stp$LATF=as.numeric(gsub(',','.',stp$LATF))
  stp$LONF=as.numeric(gsub(',','.',stp$LONF))  
  stp=stp[order(stp$NOSTA),]
  if (dim(stp)[1]!=length(unique(stp[,'NOSTA']))){ 
  cat(as.character(stp[,'NOSTA'][duplicated(stp[,'NOSTA'])]),
      'station dupliquee dans la liste','\n')
  }
  stp$NOCHAL=seq(dim(stp)[1])
  head(stp)  
  if (T%in%(nchar(as.character(stp[,nid]))>5)){ 
    cat('Numéro de station invalide dans le fichier casino','\n')
    if (stopIfError) stop('Merci de corriger le numéro de station')
  }
  if (dim(stp)[1]!=length(unique(stp[,nid]))){ 
    cat(as.character(stp[,nid][duplicated(stp[,nid])]),
        'station dupliquee dans la liste','\n')
  if (stopIfError) stop(paste(as.character(stp[,nid][duplicated(stp[,nid])]),
                        'station dupliquee dans la liste'))
  }
  dim(stp)  
  #Remove rows with NA
  #Remove rows with NA in date
  stp=stp[!is.na(stp$date),]  
  dim(stp)  
  #Check "strate"
   #head(stp)
  if (!is.null(lstrates)){
    strate.errors=stp[!substr(stp$STRATE,1,4)%in%lstrates,]
    
    if (dim(strate.errors)[1]>0){
      cat("Champ 'strate' non valide dans le(s) chalut(s):",
          as.character(unique(strate.errors$NOSTA)),'\n')
      if (stopIfError) stop('Please correct station id')
    }
  }
    ## Haul depth correction ----
    if (sum(is.na(stp$SONDE)|stp$SONDE<=0)>0){
      stpNAz=stp[is.na(stp$SONDE)|stp$SONDE<=0,]
      for (i in 1:dim(stpNAz)[1]){
        casNAz=evts[evts$NOSTA==stpNAz[i,'NOSTA'],]
        stp[stp$NOSTA==stpNAz[i,'NOSTA'],'SONDE']=mean(
          casNAz$SONDE,na.rm=TRUE)
      }

      if ((sum(is.na(stp$SONDE)|stp$SONDE<=0)&correctHaulz)>0){
        # correct null seabed depth with GEBCO bathy
        library(marmap)
        if (getNOAAbathy){
          bathyBoB <- getNOAA.bathy(lon1 = min(stp$LONF)-1, lon2 = max(stp$LONF)+1,
                                    lat1 = min(stp$LATF)-1, lat2 = max(stp$LATF)+1, 
                                    resolution = 4)
          assign("bathyBoB", bathyBoB, envir = .GlobalEnv)
          zget=get.depth(bathyBoB,stp[stp$SONDE<=0|is.na(stp$SONDE),
                                      c('LONF','LATF')],locator = FALSE)
          stp[stp$SONDE<=0|is.na(stp$SONDE),'SONDE']=-zget$depth
          cat('Hauls with no seabed depth corrected using GEBCO data:',
              as.character(stpNAz$NOSTA),'\n')
        }else if (exists('bathyBoB')){
          zget=get.depth(bathyBoB,stp[stp$SONDE<=0|is.na(stp$SONDE),
                                      c('LONF','LATF')],locator = FALSE)
          stp[stp$SONDE<=0|is.na(stp$SONDE),'SONDE']=-zget$depth
          cat('Hauls with no seabed depth corrected using GEBCO data:',
              as.character(stpNAz$NOSTA),'\n')
        }else if (exists("metapeche")){
          if(sum(stp[stp$SONDE<=0|is.na(stp$SONDE),'NOSTA']%in%
                      metapeche$Code_Station)==length(stp[
                        stp$SONDE<=0|is.na(stp$SONDE),'NOSTA'])){
          stp[stp$SONDE<=0|is.na(stp$SONDE),'SONDE']=
            metapeche[metapeche$Code_Station%in%
                            stp[stp$SONDE<=0|is.na(stp$SONDE),'NOSTA'],'SONDE']
          cat('Hauls with no seabed depth corrected using Tutti metadata:',
              as.character(stpNAz$NOSTA),'\n')
          }
        }else{
          stop('Impossible to correct haul depths, please define metapeche of set getNOAAbathy to TRUE')
        }
      }  
      if (sum(is.na(stp$SONDE)|stp$SONDE<=0)>0){
        stop('Haul with NA depth after correction,: ',stp[is.na(stp$SONDE),'NOSTA'], ' please check','\n')        
      }
    }
  if (!is.null(lstrates)){
    lfsurf=stp[substr(stp$STRATE,1,4)=='SURF'&stp$SONDE<=SURFdepthLim,]
      if (dim(lfsurf)[1]>0){
        if (stopIfError) {stop (paste(as.character(lfsurf$NOSTA),
                                'Chalut(s) de surface a moins de ',SURFdepthLim,' m de profondeur'),'\n') 
        }else{
          cat(paste(as.character(lfsurf$NOSTA),
                   'Chalut(s) de surface a moins de',SURFdepthLim,' m de profondeur'),'\n')
          cat(paste(as.character(lfsurf$NOSTA),'Surface hauls stratum changed to ',repSTRATE,'\n'))
          stp[substr(stp$STRATE,1,4)=='SURF'&stp$SONDE<=SURFdepthLim,"STRATE"]=repSTRATE
          }
      }
  }  
  ft=strptime(stp$TC,format="%d/%m/%Y %H:%M:%S")  
  if (!is.null(datestart)){
    stp=stp[ft>=strptime(datestart,format="%d/%m/%Y %H:%M:%S"),]
    ft=strptime(stp$TC,format="%d/%m/%Y %H:%M:%S")  
  }
  if (!is.null(dateend)){  
    stp=stp[ft<=strptime(dateend,format="%d/%m/%Y %H:%M:%S"),]    
  }    
  Nope0=dim(stp)[1]
  cat("Metadonnees d'evenements Thalassa importees,",Nope0,"operations de peche identifiees",'\n')

  
  #Import ship track ------------------------------
  ltype=unique(cas$Type)
  if ('ACQAUTO'%in%ltype){
    route=cas[cas$Type=='ACQAUTO',numCol.route]
    names(route)=c('date','heure','LATF','LONF','GEAR','EVT','NOSTA',
                   'STRATEL','SONDE','Tsurf','Ssurf')
    route[,c(3,4,9:11)]=apply(route[,c(3,4,9:11)],2,as.character)
    route[,c(3,4,9:11)]=apply(route[,c(3,4,9:11)],2,gsub,pattern=',',
                              replacement='.')
    route[,c(3,4,9:11)]=apply(route[,c(3,4,9:11)],2,as.numeric)
    selcas=seq(1,dim(route)[1],10)
    route=route[selcas,]
    dim(route)
    #plot(route$LONF,route$LATF)
    #coast()
    cat('Route du navire importee','\n')
  }else route=evts[FALSE,]
  
# #Import EI data in matlab format
# ******************************
#   library(R.matlab)
# 
#   pathname=paste('R:/PELGAS11/HacCorPG11/',run,'/EIlaySel/',run,
#     '_EIlaySel.mat',sep='')
#   mat <- readMat(pathname)
#   names(mat)
#   
#   EsduDevi=data.frame(LONG=mat$Long.surfER60s[,1,2],
#     LAT=mat$Lat.surfER60s[,1,2],ts=c(mat$timeER60s),
#     sAsurf=mat$Sa.surfER60s[,1,2],sAbot=mat$Sa.botER60s[,1,2])
#   esdu.id=seq(dim(EsduDevi)[1])
#   EsduDevi=data.frame(esdu.id=esdu.id,EsduDevi)
#   X=rep(1,dim(EsduDevi)[1]*length(Ndevi))
#   X=data.frame(matrix(X,ncol=length(Ndevi)))
#   names(X)=paste('D',Ndevi,sep='')
#   EsduDevi=data.frame(EsduDevi,X)
#   head(EsduDevi)
#   EsduDevi$zonesCLAS=NA
#   EsduDevi$zonesSURF=NA
  
#Import .man data -----------------------------
if (!is.null(path.man)){
    
  #Import and format .man supervized echo-integration by layer data 
  #located in:
  #Used to create chaman file and set reference hauls  

  if (newman){
    #import NASC per fish echotypes found in EspDevi
    man.import=read.table(path.man,sep=';',header=TRUE)
    sadev=man.import[,c('NESU',as.character(unique(EspDevi$DEV)),
                      'TOT','ID','f','depth','dateend',xname,yname,'RAD','f')]
    names(sadev)=c('ESUid',as.character(unique(EspDevi$DEV)),'TOTAL','ID',
                    'f','depth','TC','x','y','RAD','f')
    sadev$TC=gsub('-','/',sadev$TC)
    sadev$R=substr(sadev$ID,2,3)
    ft=strptime(sadev$TC,format="%Y/%m/%d %H:%M:%S",tz='GMT')
    if (sum(is.na(ft))>0)     ft=strptime(sadev$TC,format="%Y/%d/%m %H:%M:%S")
    if (sum(is.na(ft))>0)     stop('Probleme de format date/heure dans le fichier d echotypage','\n')
    sadev=sadev[order(ft),]
    sadev$ESUid=seq(dim(sadev)[1])
    if (!is.null(datestart)){
      sadev=sadev[ft>=strptime(datestart,format="%d/%m/%Y %H:%M:%S",tz='GMT'),]
    }
    if (!is.null(dateend)){  
      if (strptime(dateend,format="%d/%m/%Y %H:%M:%S",tz='GMT')<=max(ft)){
        sadev=sadev[ft<=strptime(dateend,format="%d/%m/%Y %H:%M:%S",tz='GMT'),]        
      }
    }
  }else{  
  	man.import=import.man.folder(path.man,verbose=verbose,man.version='man')
	  names(man.import)
	  sadev=man.import$sa.db
    names(sadev)
  	DEVdef=man.import$DEVdef
    #select echotypes found in EspDevi
  	#DEVdef=DEVdef[DEVdef$DEV%in%unique(EspDevi$DEV),]
  	DEVdef.out=DEVdef$DEV[!DEVdef$DEV%in%unique(EspDevi$DEV)]
    if (length(DEVdef.out)>0)	sadev=sadev[,!names(sadev)%in%DEVdef.out]
  	sadev$TC=gsub('-','/',sadev$t)
  	sadev$R=substr(sadev$ID,2,3)
  	names(sadev)
  }  

#   write.table(sadev,'M:/PELGAS12/Mathieu/PELGAS11/Evaluation_stock/Resultats/sadevEx.csv',
#                 sep=';',row.names=FALSE)  
#     
  #check for duplicated ESDUs  
  if (length(sadev$TC)>length(unique(sadev$TC))){
    stop(paste('Esdu',sadev[duplicated(sadev$TC),'TC'],'duplicated','\n'))
    sadev[sadev$TC%in%sadev[duplicated(sadev$TC),'TC'],]
  }
  
  names(sadev)[1]="esdu.id"
  names(sadev)[names(sadev)%in%c('x','y')]=c('LONG','LAT')
  #if (T%in%(sadev$LONG>0)) sadev$LONG=-abs(sadev$LONG)
 
  lnames=c("esdu.id","LONG","LAT","depth","TC",paste("D",Ndevi,sep=''),"RAD","f")
  EsduDevi.man=sadev[,lnames]  
  EsduDevi.man$zonesCLAS=NA
  EsduDevi.man$zonesSURF=NA
  EsduDevi.man$CAMPAGNE=cruise
  EsduDevi=EsduDevi.man
  # Extract acoustic frequency
  Freq=unique(EsduDevi$f)
}else{
  cat('No acoustic data provided','\n')
  EsduDevi=NA
}    

#Import other fishing metadata (pros ...)------------------------------
  if (!is.null(path.metapeche)){
    if (class(path.metapeche)=="character"){
    #pro.fishing.data.import=function(path.metapro){
      metapeche=read.table(path.metapeche,header=TRUE,sep=casino.sep)
      if (dim(metapeche)[2]==26){
        # import metapeche data in old format
        head(metapeche)
        metapeche=correc.date(metapeche,ndatetime=NA,ndate=NA,ntime='Hfil')
        metapeche=correc.date(metapeche,ndatetime=NA,ndate=NA,ntime='Hvir')
        metapeche$TC=paste(metapeche$DATE,metapeche$Hfil)    
        metapeche.long=reshape(metapeche[,c(2,7,8,12,13,19:27,9,6)],varying=list(names(metapeche)[19:26]),
                         direction='long',times=names(metapeche)[19:26],timevar='GENR_ESP')
        metapeche.long$SIGNEP=0
        metapeche.long$id=cruise
        metapeche.long$GENR_ESP=gsub('_','-',metapeche.long$GENR_ESP)
        head(metapeche.long)
        names(metapeche.long)=c('NOSTA','GEAR','STRATE','LATF','LONF','TC','SONDE','Observation','GENR_ESP',
                          'PT','CAMPAGNE','SIGNEP')
        metapeche.long$CodEsp=paste(metapeche.long$GENR_ESP,metapeche.long$STRATE,metapeche.long$SIGNEP,
                              sep='-')
        metapeche.long$Ntot=NA
        #metapeche.long$NOSTA
        #adds NOCHAL to metapeche
        nostachal2=data.frame(NOSTA=sort(unique(metapeche.long$NOSTA)))
        nostachal2$NOCHAL=900+seq(dim(nostachal2)[1])
        # Add type
        metapeche.long$Type='OPE'
        metapeche.long=merge(metapeche.long,nostachal2,by.x='NOSTA',by.y='NOSTA')
        head(metapeche.long)
        #Filter out zero catches
        metapeche.long=metapeche.long[metapeche.long$PT!=0,]
    
      }else if (dim(metapeche)[2]==15){
        # import metapeche data in Tutti format
        head(metapeche)
        stp.metapeche=metapeche[,c('LatDeb','LongDeb','DateDeb','Code_Station',
                                   'STRATE','SONDE','Engin','Navire','LatFin',
                                   'LongFin','DateFin')]
        names(stp.metapeche)=c('LATF','LONF','TC','NOSTA','STRATE','SONDE',
                               'GEAR','Observation','LatFin','LongFin','DateFin')
        # ajoute NOCHAL
        nostachal2=data.frame(NOSTA=sort(unique(stp.metapeche$NOSTA)))
        nostachal2$NOCHAL=900+seq(dim(nostachal2)[1])
        stp.metapeche=merge(stp.metapeche,nostachal2,by.x='NOSTA',by.y='NOSTA')
        stp.metapeche$date=substr(stp.metapeche$TC,1,10)
        stp.metapeche$heure=substr(stp.metapeche$TC,12,19)
        stp.metapeche$datef=substr(stp.metapeche$DateFin,1,10)
        stp.metapeche$heuref=substr(stp.metapeche$DateFin,12,19)
        # Add type
        stp.metapeche$Type='OPE'
        if (length(stp.metapeche$NOSTA)!=length(unique(stp.metapeche$NOSTA))){
          stop ('Peche metadata',
                stp.metapeche$NOSTA[duplicated(stp.$NOSTA)],'dupliquees')
        } 
      }
    }else if (class(path.metapeche)=="dataframe"){
      stp.metapeche=metapeche[,c('LatDeb','LongDeb','DateDeb','Code_Station',
                                 'STRATE','SONDE','Engin','Navire','LatFin',
                                 'LongFin','DateFin')]
      names(stp.metapeche)=c('LATF','LONF','TC','NOSTA','STRATE','SONDE',
                             'GEAR','Observation','LatFin','LongFin','DateFin')
      # ajoute NOCHAL
      nostachal2=data.frame(NOSTA=sort(unique(stp.metapeche$NOSTA)))
      nostachal2$NOCHAL=900+seq(dim(nostachal2)[1])
      stp.metapeche=merge(stp.metapeche,nostachal2,by.x='NOSTA',by.y='NOSTA')
      stp.metapeche$date=substr(stp.metapeche$TC,1,10)
      stp.metapeche$heure=substr(stp.metapeche$TC,12,19)
      stp.metapeche$datef=substr(stp.metapeche$DateFin,1,10)
      stp.metapeche$heuref=substr(stp.metapeche$DateFin,12,19)
      stp.metapeche$rad=NA
      stp.metapeche$ID=stp.metapeche$Identificateur.Operation
    }

      stp.metapeche$EVT='DFIL'
      stp.metapeche$STRATEL=stp.metapeche$STRATE
      names(stp.metapeche)
      stp.metapeche$Nom.Appareil=stp.metapeche$GEAR
      # Correct pro gear code
      stp.metapeche[substr(stp.metapeche$NOSTA,2,2)==5,'GEAR']='942OBS'
      stp.metapeche$Identificateur.Operation=paste(stp.metapeche$GEAR,stp.metapeche$NOCHAL,sep='')
      stp.metapeche$Nom.Action=stp.metapeche$EVT
      stp.pro=stp.metapeche[substr(stp.metapeche$NOSTA,2,2)==5,]
      names(stp.metapeche);names(stp)
      names(stp)[!names(stp)%in%names(stp.metapeche)]
      stp$Observation=as.numeric(as.character(stp$SONDE))
      lnc=names(stp)[!names(stp)%in%names(stp.pro)]
      head(stp)
      stp.pros=stp.pro[,names(stp)]
      head(stp.pros)
      
      ## Correct pro hauls depth if needed -------------
      if (sum(is.na(stp.pros$SONDE)|stp.pros$SONDE<=0)>0){
        stp.prosNAz=stp.pros[is.na(stp.pros$SONDE)|stp.pros$SONDE<=0,]
        for (i in 1:dim(stp.prosNAz)[1]){
          casNAz=evts[evts$NOSTA==stp.prosNAz[i,'NOSTA'],]
          stp.pros[stp.pros$NOSTA==stp.prosNAz[i,'NOSTA'],'SONDE']=mean(
            casNAz$SONDE,na.rm=TRUE)
        }
        if ((sum(is.na(stp.pros$SONDE)|stp.pros$SONDE<=0)&correctHaulz)>0){
          # correct null seabed depth with GEBCO bathy
          library(marmap)
          if (getNOAAbathy){
            bathyBoB <- getNOAA.bathy(lon1 = min(stp.pros$LONF)-1, lon2 = max(stp.pros$LONF)+1,
                                      lat1 = min(stp.pros$LATF)-1, lat2 = max(stp.pros$LATF)+1, 
                                      resolution = 4)
            assign("bathyBoB", bathyBoB, envir = .GlobalEnv)
            zget=get.depth(bathyBoB,stp.pros[stp.pros$SONDE<=0|is.na(stp.pros$SONDE),
                                             c('LONF','LATF')],locator = FALSE)
            stp.pros[stp.pros$SONDE<=0|is.na(stp.pros$SONDE),'SONDE']=-zget$depth
            cat('Hauls with no seabed depth corrected using GEBCO data:',
                as.character(stp.prosNAz$NOSTA),'\n')
          }else if (exists('bathyBoB')){
            zget=get.depth(bathyBoB,stp.pros[stp.pros$SONDE<=0|is.na(stp.pros$SONDE),
                                             c('LONF','LATF')],locator = FALSE)
            stp.pros[stp.pros$SONDE<=0|is.na(stp.pros$SONDE),'SONDE']=-zget$depth
            cat('Hauls with no seabed depth corrected using GEBCO data:',
                as.character(stp.prosNAz$NOSTA),'\n')
          }else{
            stop('Impossible to correct haul depths, please define metapeche of set getNOAAbathy to TRUE')
          }
        }  
        if (sum(is.na(stp.pros$SONDE)|stp.pros$SONDE<=0)>0){
          stop('Haul with NA depth after correction,: ',stp.pros[is.na(stp.pros$SONDE),'NOSTA'], ' please check','\n')        
        }
      }  
      
      stp$LONF=as.numeric(as.character(stp$LONF))
      stp$LATF=as.numeric(as.character(stp$LATF))
      stp$SONDE=as.numeric(as.character(stp$SONDE))
      # Bind pro and Thalassa operations
      stp=rbind(stp,stp.pros)
      stp$STRATE=gsub(" ","",stp$STRATE)
      Nope0=dim(stp)[1]
    
      cat("Metadonnees d'evenements Thalassa et pros importees,",Nope0,"operations de peche identifiees",'\n')
    
      #Check "strate"
      head(stp)
      stp$NOCHAL
      strate.errors=stp[!substr(stp$STRATE,1,4)%in%c('CLAS','SURF','NULL'),]
	  
	    if (stopIfError&(dim(strate.errors)[1]>0)){
		    cat("Invalid 'strate' fields in hauls:",
			    as.character(unique(strate.errors$NOSTA)),'\n')
		    stop('Please correct errors before proceeding')
	    }
    
      lfsurf=stp[substr(stp$STRATE,1,4)=='SURF'&!is.na(stp$SONDE)&stp$SONDE<=SURFdepthLim,]
      if (dim(lfsurf)[1]>0){
        if (stopIfError) {stop (paste(as.character(lfsurf$NOSTA[!is.na(lfsurf$NOSTA)]),
                                      'Surface hauls at less than ',SURFdepthLim,' m depth')) 
        }else {
          cat(paste(as.character(lfsurf$NOSTA),
                         'Surface hauls at less than ',SURFdepthLim,' m depth','\n'))
          cat(paste(as.character(lfsurf$NOSTA),'Surface hauls stratum changed to ',repSTRATE,'\n'))
          stp[substr(stp$STRATE,1,4)=='SURF'&stp$SONDE<=SURFdepthLim,"STRATE"]=repSTRATE
        }
      }  
      # creates metadata rows in casino format for pro hauls ----
      ncodeaction=RcasinoNames[RcasinoNames$casName==cas.names[8],'Rname']
      basecas1=cas[FALSE,]
      if (dim(metapeche)[2]==26){
        basecas1=basecas1[seq(length(pro$NOSTA)),]
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[1],'Rname']]=pro$DATE
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[2],'Rname']]=pro$Hfil
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[6],'Rname']]=pro$ENGIN
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[13],'Rname']]=pro$BATHY
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[5],'Rname']]=pro$ENGIN
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[10],'Rname']]=pro$NAV   
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[11],'Rname']]=pro$NOSTA
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[12],'Rname']]=pro$STRATE
        basecas1$Type='OPE'
        basecas1$Type.De.Phase=''
        basecas1$Nom.Phase=''
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[3],'Rname']]=pro$LatDfil
        basecas1[,RcasinoNames[RcasinoNames$casName==cas.names[4],'Rname']]=pro$LongDfil
        basecas1$Identificateur.Operation=paste(pro$NAV,pro$ENGIN,
                                                substr(pro$NOSTA,4,6),sep='')
        basecas2=cas[cas[,ncodeaction]%in%c('DVIR'),]
        basecas2=basecas2[seq(length(pro$NOSTA)),]
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[1],'Rname']]=pro$DATE
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[2],'Rname']]=pro$Hfil
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[6],'Rname']]=pro$ENGIN
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[13],'Rname']]=pro$BATHY
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[5],'Rname']]=pro$ENGIN
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[10],'Rname']]=pro$NAV   
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[11],'Rname']]=pro$NOSTA
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[12],'Rname']]=pro$STRATE
        basecas2$Type='OPE'
        basecas2$Type.De.Phase=''
        basecas2$Nom.Phase=''
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[3],'Rname']]=pro$LatDfil
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[4],'Rname']]=pro$LongDfil
        basecas2$Identificateur.Operation=paste(pro$NAV,pro$ENGIN,
                                                substr(pro$NOSTA,4,6),sep='')
      }else if (dim(metapeche)[2]==15){
        # in tutti format
        basecas1=cas[FALSE,]
        basecas1=basecas1[,names(basecas1)!='X']
        basecas1=basecas1[seq(length(stp.pro$NOSTA)),]
        #basecas1=data.frame(lapply(basecas1, as.character), stringsAsFactors=FALSE)
        #basecas1=data.frame(apply(basecas1,2,as.character),stringsAsFactors = FALSE)
        basecas1[,RcasinoNames[RcasinoNames$casName=='Date','Rname']]=stp.pro$date
        basecas1[,RcasinoNames[RcasinoNames$casName=="Heure",'Rname']]=stp.pro$heure
        basecas1[,RcasinoNames[RcasinoNames$casName=="Code Appareil",'Rname']]=stp.pro$GEAR
        basecas1[,RcasinoNames[RcasinoNames$casName=="CINNA Sonde vert. (m)",'Rname']]=stp.pro$SONDE
        basecas1[,RcasinoNames[RcasinoNames$casName=="Nom Appareil",'Rname']]=stp.pro$Nom.Appareil
        basecas1[,RcasinoNames[RcasinoNames$casName=="Observation",'Rname']]=stp.pro$Observation   
        basecas1[,RcasinoNames[RcasinoNames$casName=="Num Station",'Rname']]=as.character(stp.pro$NOSTA)
        basecas1[,RcasinoNames[RcasinoNames$casName=="Strate",'Rname']]=as.character(stp.pro$STRATE)
        basecas1[,RcasinoNames[RcasinoNames$casName=="Type",'Rname']]=as.character(stp.pro$Type)
        basecas1$Type.De.Phase=''
        basecas1$Nom.Phase=''
        basecas1$Code.Action='DFIL'
        basecas1[,RcasinoNames[RcasinoNames$casName=="Latitude",'Rname']]=stp.pro$LATF
        basecas1[,RcasinoNames[RcasinoNames$casName=="Longitude",'Rname']]=stp.pro$LONF
        basecas1$Identificateur.Operation=paste(stp.pro$Observation,stp.pro$GEAR,
                                                substr(stp.pro$NOSTA,4,6),sep='')
        basecas2=basecas1[seq(length(stp.pro$NOSTA)),]
        basecas2[,RcasinoNames[RcasinoNames$casName=="Date",'Rname']]=stp.pro$datef
        basecas2[,RcasinoNames[RcasinoNames$casName=="Heure",'Rname']]=stp.pro$heuref
        basecas2[,RcasinoNames[RcasinoNames$casName=="Code Appareil",'Rname']]=stp.pro$GEAR
        basecas2[,RcasinoNames[RcasinoNames$casName=="CINNA Sonde vert. (m)",'Rname']]=stp.pro$SONDE
        basecas2[,RcasinoNames[RcasinoNames$casName=="Nom Appareil",'Rname']]=stp.pro$Nom.Appareil
        basecas2[,RcasinoNames[RcasinoNames$casName=="Observation",'Rname']]=stp.pro$Observation   
        basecas2[,RcasinoNames[RcasinoNames$casName=="Num Station",'Rname']]=stp.pro$NOSTA
        basecas2[,RcasinoNames[RcasinoNames$casName=="Strate",'Rname']]=stp.pro$STRATE
        basecas2[,RcasinoNames[RcasinoNames$casName=="Type",'Rname']]=as.character(stp.pro$Type)
        basecas2$Type.De.Phase=''
        basecas2$Nom.Phase=''
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[3],'Rname']]=stp.pro$LatFin
        basecas2[,RcasinoNames[RcasinoNames$casName==cas.names[4],'Rname']]=stp.pro$LongFin
        basecas2$Identificateur.Operation=paste(stp.pro$Observation,stp.pro$GEAR,
                                                substr(stp.pro$NOSTA,4,6),sep='')
        basecas2$Code.Action='DVIR'
      }else{
        stop("Incorrect fishing metadata format (metapeche), please check")
      }
      cas.pro=rbind(basecas1,basecas2)
      cas.pro$Observation=as.character(cas.pro$Observation)
      #Removes NA columns
      #names(cas.pro)
      cas.pro=cas.pro[,!names(cas.pro)%in%c('X','X.1','X.2')]
      #names(cas.pro)
      #Pads columns with no info with NA
      okcol=RcasinoNames[RcasinoNames$casName%in%cas.names,'Rname']
      setNA=function(x){x=rep(NA,length(x));x}
      cas.pro[,!names(cas.pro)%in%okcol]=apply(cas.pro[,
        !names(cas.pro)%in%okcol],2,setNA)
      #Set decimal delimiter to "," for casino compatibility
      cas.pro[,3:4]=apply(cas.pro[,3:4],2,as.character)
      cas.pro[,3]=gsub('\\.',',',cas.pro[,3])
      cas.pro[,4]=gsub('\\.',',',cas.pro[,4])
    
      # Generate complete metadata file
      cas.pro$`CINNA.Sonde.vert...m.`=as.character(cas.pro$`CINNA.Sonde.vert...m.`)
      cas$`CINNA.Sonde.vert...m.`=as.character(cas$`CINNA.Sonde.vert...m.`)
      cas.ALL=rbind(cas,cas.pro)
      names(cas);names(cas.pro)
      head(cas.ALL)

      #Export pro hauls metadata
      if (!is.null(path.export)){
        write.table(cas.pro,paste(path.export,'caspro.csv',sep=''),
                                row.names=FALSE,sep = ";")
        write.table(cas.ALL,paste(path.export,'casALL.csv',sep=''),
                    row.names=FALSE,sep = ";")
      } 
  }else{
    pro.long=NA
    cas.pro=NA
    cat('No pro metadata provided','\n')
  }  
  
#Import fishing data from raptri------------------------------
  #check path.raptri extension: 
    # if extension = .mdb: connect to access database
    # if extension = .txt: opens text file

  #import from access database
  if ((!is.null(path.raptri))&(strsplit(basename(path.raptri),split="\\.")[[1]][2]=='mdb')){
    #Direct import using library RODBC
    if (raptri.fusion==1){
      cat('Importing fishing data from multiple Raptri databases','\n')
      library(RODBC)
      ## open ODBC connection with raptri database
      channel <- odbcConnectAccess(path.raptri)
      #If pro and thalassa catches in one database, catacombes style
      Pecheb <- sqlFetch(channel = channel,## nom de la table ? importer 
        sqtable = 'Capture_par_esp?ce_fusion')
      Pechem <- sqlFetch(channel = channel,## nom de la table ? importer 
        sqtable = 'YL')
      ## close ODBC connection 
      odbcClose(channel)  
      head(Pecheb)
      head(Pechem)
      Peche=Pecheb[,c('Station','Rubin_code','Signe','Poids_captur?_Kg',
        'Nombre_captur?')]
      names(Peche)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT')  
      Pechem$PM=Pechem$Poids_?chantillon/Pechem$Nombre_Echantillon  
      Pechema=aggregate(Pechem$Nombre_Echantillon,list(Pechem$Station,
        Pechem$Rubin_code),sum)
      names(Pechema)=c('NOSTA','GENR_ESP','NTOT')  
      Pechem=merge(Pechem,Pechema,by.x=c('Station','Rubin_code'),
        by.y=c('NOSTA','GENR_ESP'))
      Pechem$L=Pechem$Longueur
      Pechem[Pechem$Unite_mesure==0,'L']=Pechem[Pechem$Unite_mesure==0,'L']/10
      Pechem$Li=Pechem$L*Pechem$Nombre_Echantillon/Pechem$NTOT
      #mean weight in gram
      Pechem$mwi=1000*Pechem$PM*Pechem$Nombre_Echantillon/Pechem$NTOT
      LwM.df=aggregate(Pechem[,c('Li','mwi')],list(Pechem$Station,
        Pechem$Rubin_code),sum)
      names(LwM.df)=c('NOSTA','GENR_ESP','LM','PM')
      LwM.df$MOULE=1000/LwM.df$PM
      Peche=merge(Peche,LwM.df[,c('NOSTA','GENR_ESP','LM','MOULE','PM')],
        by.x=c('NOSTA','GENR_ESP'),by.y=c('NOSTA','GENR_ESP'))
      Peche=Peche[,c('NOSTA','GENR_ESP','SIGNEP','PT','NT','MOULE','LM','PM')]
      #Biological data import
      #*************************
      names(Pechem)
      mens=Pechem[,c('Station','Rubin_code','Signe','L',
        'Nombre_?chantillon','PM')]
      mens=data.frame(CAMPAGNE=cruise,mens)  
      names(mens)=c('CAMPAGNE','NOSTA','GENR_ESP','CATEG','TAILLE','NBIND',
        'POIDSTAILLE')
      mens$CodEsp2=paste(mens$GENR_ESP,mens$CATEG,sep='-')  
      head(mens)
    }else if (raptri.fusion==2){
      cat('Importing fishing data from a single Raptri database','\n')
      ## open ODBC connection with raptri database
      channel <- odbcConnectAccess(path.raptri)      
      ## Thalassa catches
      Peche.thala <- sqlFetch(channel = channel,## nom de la table ? importer 
        sqtable = "Ytot_CodEsp_CH")
      ## Pro catches
      Peche.pro <- sqlFetch(channel = channel,## nom de la table ? importer 
        sqtable = "Ytot_CodEsp_CH_pro")
      ## close ODBC connection 
      odbcClose(channel)
      names(Peche.thala)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT','MOULE','LM')
      names(Peche.pro)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT','MOULE','LM')
      Peche=rbind(Peche.thala,Peche.pro)
    }
    
  #import from text file exported from raptri
  #*************************
  }else if  ((!is.null(path.raptri))&(strsplit(basename(path.raptri),split="\\.")[[1]][2]%in%c('txt','csv'))){
    if (raptri.fusion>0){
      cat('Importing fishing data from .txt exports from multiple Raptri databases','\n')
      Peche.thala=read.table(path.raptri,header=TRUE,sep=';',dec=udec)  
      names(Peche.thala)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT','MOULE','LM')
      Peche.pro=read.table(path.raptri.pro,header=TRUE,sep=';',dec=udec)  
      names(Peche.pro)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT','MOULE','LM')
      Peche=rbind(Peche.thala,Peche.pro)
      Peche=data.frame(CAMPAGNE=cruise,Peche)
      if (!is.null(path.mens)){
        mens.thala=read.table(path.mens,header=TRUE,sep=';',dec=udec)  
        names(mens.thala)=c('NOSTA','POCHE','GENR_ESP','CATEG','SEXE',
            'POIDSECHANT','NECHANT','TAILLE','NBIND','POIDSTAILLE','UNITE',
            'INC')
        #check that sum(POIDSTAILLE)=POIDSECHANT
        mensa.thala=aggregate(mens.thala$POIDSTAILLE,list(mens.thala$NOSTA,
          mens.thala$GENR_ESP,mens.thala$POIDSECHANT),sum)
        names(mensa.thala)=c('NOSTA','GENR_ESP','POIDSECHANT','PTOT')
        mensa.thala$dp=mensa.thala[,'POIDSECHANT']-mensa.thala[,'PTOT']
        #mensa.thala[mensa.thala$dp>0.1,]
        #Proc catches
        mens.pro=read.table(path.mens.pro,header=TRUE,sep=';',dec=udec)  
        names(mens.pro)=c('NOSTA','POCHE','GENR_ESP','CATEG','SEXE',
            'POIDSECHANT','NECHANT','TAILLE','NBIND','POIDSTAILLE','UNITE',
            'INC')
        #check that sum(POIDSTAILLE)=POIDSECHANT
        mensa.pro=aggregate(mens.pro$POIDSTAILLE,list(mens.pro$NOSTA,
          mens.pro$GENR_ESP,mens.pro$POIDSECHANT),sum)
        names(mensa.pro)=c('NOSTA','GENR_ESP','POIDSECHANT','PTOT')
        mensa.pro$dp=mensa.pro[,'POIDSECHANT']-mensa.pro[,'PTOT']
        #mensa.pro[mensa.pro$dp>0.1,]
        #binds thalassa and pro catches        
        mens=rbind(mens.thala,mens.pro)
        mens$Lcm=mens$TAILLE
        #Convert length in mm to cm
        mens[mens$UNITE==0,'Lcm']=mens[mens$UNITE==0,'Lcm']/10
        mens$PM=mens$POIDSTAILLE/mens$NBIND
        mens=data.frame(CAMPAGNE=cruise,mens)
        #check that sum(POIDSTAILLE)=POIDSECHANT
        mensa=aggregate(mens$POIDSTAILLE,list(mens$NOSTA,mens$GENR_ESP,
          mens$POIDSECHANT),sum)
      }  
    }else if (raptri.fusion==0){
      #Catches import from 'TotalSamples4Echobase.txt'
      #*************************
      cat('Importing fishing data from .txt exports from a single Raptri database','\n')
      Peche=read.table(path.raptri,header=TRUE,sep=';',dec=udec)
      head(Peche)
      if (dim(Peche)[2]==9){
        names(Peche)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT','LM','PM','MOULE','PTRI')
      }else if (dim(Peche)[2]==10){
        names(Peche)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT','LM','PM','MOULE','PTRI','VESSEL')
      }else if (dim(Peche)[2]==11){
        names(Peche)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT','LM','PM','MOULE','PTRI','VESSEL','PRO')
      }   
      Peche$LM=Peche$LM/10
      dimopPeche0=length(unique(Peche$NOSTA))
      (dimPeche0=dim(Peche)[1])
      length(unique(Peche$NOSTA))
      #Peche$PM=1000/Peche$MOULE
      #Peche[Peche$GENR_ESP=='TRAG-MEG',]
      Peche=data.frame(CAMPAGNE=cruise,Peche)
      unique(Peche$GENR_ESP)
      if (substr(cruise,1,6)=='PELMED'){
        Peche=merge(Peche,dfc.med,by.x='GENR_ESP',by.y='spmed')
        Peche$GENR_ESP=Peche$sp
        Peche$SIGNEP=Peche$size
        Peche=Peche[,names(Peche)!='sp']
      }
      dim(Peche)
      Peche$CodEsp=paste(Peche$GENR_ESP,Peche$SIGNEP,sep='-')
      #Check for duplicated species
      ID=paste(Peche$CodEsp,Peche$NOSTA)
      lnochal=Peche[duplicated(ID),'NOSTA']
      Peche[Peche$NOSTA%in%lnochal,]
      if (length(lnochal)>0) cat ('Duplicated species:hauls',
                                  ID[duplicated(ID)],'\n')
      dim(Peche)
      length(unique(Peche$NOSTA))
      #Biological measurements import from 'SubSample4Echobase.txt'
      #*************************    
      if (!is.null(path.mens)){
        mens=read.table(path.mens,header=TRUE,sep=';',dec=udec)
        if (dim(mens)[2]==12){
          names(mens)=c('NOSTA','POCHE','GENR_ESP','CATEG','SEXE',
            'POIDSECHANT','NECHANT','TAILLE','NBIND','POIDSTAILLE','UNITE',
            'INC')
        }else if (dim(mens)[2]==13){
          names(mens)=c('NOSTA','POCHE','GENR_ESP','CATEG','SEXE',
                        'POIDSECHANT','NECHANT','TAILLE','NBIND','POIDSTAILLE','UNITE',
                        'INC','VESSEL')
        }  
        mens$Lcm=mens$TAILLE
        length(unique(mens$NOSTA))
        DimMens0=dim(mens)
        dimopMens0=length(unique(mens$NOSTA))        
        #Convert length in mm to cm
        mens[!is.na(mens$UNITE)&mens$UNITE==0,'Lcm']=mens[!is.na(mens$UNITE)&mens$UNITE==0,'Lcm']/10
        mens$PM=mens$POIDSTAILLE/mens$NBIND
        mens=data.frame(CAMPAGNE=cruise,mens)
        if (substr(cruise,1,6)=='PELMED'){
          spmed=c('ENGR-ENG','SARD-PIG','SARD-PIP','SCOM-SCO','SPRA-SPR','TRAC-TRG',
                  'DIVE-RS1','SCOM-JAP','TRAC-MEP','TRAG-MEG','TRAC-MEG','TRAC-TRP',
                  'SARI-AUR','TRAC-PIC')
          sp=c('ENGR-ENC','SARD-PIL','SARD-PIL','SCOM-SCO','SPRA-SPR','TRAC-TRU',
               'DIVE-RS1','SCOM-JAP','TRAC-MED','TRAC-MED','TRAC-MED',
               'TRAC-TRU','SARI-AUR','TRAC-PIC')
          size=c('0','G','0','0','0','G','0','0','0','G','G','0','0','0')
          dfc=data.frame(spmed=spmed,sp=sp,size)
          mens=merge(mens,dfc,by.x='GENR_ESP',by.y='spmed')
          mens$GENR_ESP=mens$sp
          mens$CATEG=mens$size
          mens=mens[,names(mens)!='sp']
        }
        mens$CodEsp2=paste(mens$GENR_ESP,mens$CATEG,sep='-')
        # Remove species with no length
        mens=mens[!is.na(mens$Lcm),]
      }
    }

    if (WL.recompute==1){      
    #Re-compute mean length and weight based on raw biological measurements 
      cat('Correcting catches mean weight based on raw biological measurements','\n')
      Pechem=mens
      Pechem$CodEsp=paste(Pechem$GENR_ESP,Pechem$CATEG,sep='-')
      Pechem$PM=Pechem$POIDSECHANT/Pechem$NECHANT  
      Pechema=aggregate(Pechem$NECHANT,list(Pechem$NOSTA,Pechem$CodEsp),
        sum)
      names(Pechema)=c('NOSTA','CodEsp','NTOT')  
      Pechem=merge(Pechem,Pechema,by.x=c('NOSTA','CodEsp'),
        by.y=c('NOSTA','CodEsp'))
      Pechem$L=Pechem$TAILLE
      df0=Pechem[Pechem$UNITE==0,]
      df1=Pechem[Pechem$UNITE==1,]      
      aggregate(df0$PM,list(df0$GENR_ESP),mean,na.rm=TRUE)
      aggregate(df1$PM,list(df1$GENR_ESP),mean,na.rm=TRUE)
      Pechem[Pechem$UNITE==0,'L']=Pechem[Pechem$UNITE==0,'L']/10
      Pechem$Li=Pechem$L*Pechem$NECHANT/Pechem$NTOT
      #mean weight in gram
      Pechem$mwi=1000*Pechem$PM*Pechem$NECHANT/Pechem$NTOT
      LwM.df=aggregate(Pechem[,c('Li','mwi')],list(Pechem$NOSTA,
        Pechem$CodEsp),sum)
      names(LwM.df)=c('NOSTA','CodEsp','LM','PM')
      LwM.df$MOULE=1000/LwM.df$PM
      Pechecheck=merge(Peche,LwM.df[,c('NOSTA','CodEsp','LM','MOULE','PM')],
        by.x=c('NOSTA','CodEsp'),by.y=c('NOSTA','CodEsp'))
      #plot(Pechecheck$LM.x,Pechecheck$LM.y)
      #plot(Pechecheck$MOULE.x,Pechecheck$MOULE.y)
      summary(Pechecheck$MOULE.x)
      summary(Pechecheck$MOULE.y)
      Peche=merge(Peche[,!names(Peche)%in%c('LM','MOULE','PM')],LwM.df[,
        c('NOSTA','CodEsp','LM','MOULE','PM')],
        by.x=c('NOSTA','CodEsp'),by.y=c('NOSTA','CodEsp'))      
      Peche=Peche[,c('CAMPAGNE','NOSTA','GENR_ESP','CodEsp','SIGNEP','PT',
                     'NT','MOULE','LM','PM')]
    }else if (WL.recompute==2){
      cat('Correcting catches mean weight based on length-weight equations','\n')
      if (!exists("mens")) {
        stop('No biological measurements provided, unable to compute length-weight equations')
      }else{
        LW=LW.compute(catch=mens,codespname='CodEsp2',
              Lname='Lcm',Wname='PM',wname='NBIND',plotit=TRUE,Nmin=10)
        LW$GENR_ESP=substr(LW$CodEsp2,1,8)
        LWs=LW[!is.na(LW$a),]
        dim(Peche)
        Peches=merge(Peche,LWs[,c('CodEsp2','a','b')],by.x='CodEsp',
                     by.y='CodEsp2')
        dim(Peches)
        Peches$PM=Peches$a*Peches$LM^Peches$b
        Peches$MOULE=1000/Peches$PM
        Peche=rbind(Peche[!Peche$CodEsp%in%LWs$CodEsp,],
                    Peches[,names(Peche)])
        dim(Peche)
        #Check for duplicated species
        ID=paste(Peche$CodEsp,Peche$NOSTA)
        lnochal=Peche[duplicated(ID),'NOSTA']
        #if (length(lnochal)>0) cat ('Duplicated species in hauls','\n')
      }  
    }else if (WL.recompute==3){
      cat('Correcting pro catches mean weight based on length-weight equations','\n')
      if (!exists("mens")) {
        stop('No biological measurements provided, unable to compute length-weight equations')
      }else{
        cat('Checking subSamples length-weight equations','\n')
        LW=LW.compute(catch=mens,codespname='CodEsp2',
          Lname='Lcm',Wname='PM',wname='NBIND',plotit=TRUE,Nmin=10,path.export=path.export)
    
        LW$GENR_ESP=substr(LW$CodEsp2,1,8)
        LWs=LW[!is.na(LW$a),]
        dim(Peche)
        Peche.pro=Peche[substr(Peche$NOSTA,2,2)=='5',]
        dim(Peche.pro)
        Peche.tha=Peche[substr(Peche$NOSTA,2,2)!='5',]        
        dim(Peche.tha)
        dim(Peche.pro)        
        Peches.pro=merge(Peche.pro,LWs[,c('CodEsp2','a','b')],by.x='CodEsp',
                   by.y='CodEsp2')
        dim(Peches.pro)
        Peches.pro$PM=Peches.pro$a*Peches.pro$LM^Peches.pro$b
        Peches.pro$MOULE=1000/Peches.pro$PM
        Peche2=rbind(Peche.tha,Peches.pro[,names(Peche)],
                    Peche.pro[!paste(Peche.pro$NOSTA,Peche.pro$CodEsp)%in%
                      paste(Peches.pro$NOSTA,Peches.pro$CodEsp),names(Peche)])
        dim(Peche2)
        F%in%paste(Peche$NOSTA,Peche$CodEsp)%in%paste(Peche2$NOSTA,Peche2$CodEsp)
        Peche=Peche2
        dim(Peche)
        #Check for duplicated species
        ID=paste(Peche$CodEsp,Peche$NOSTA)
        lnochal=Peche[duplicated(ID),'NOSTA']
        dim(Peche)
        dimopPeche1=length(unique(Peche$NOSTA))
        #if (length(lnochal)>0) cat ('Duplicated species in hauls','\n')
      }
    }else if (WL.recompute==0){
      cat('No correction of catches mean weight','\n')
    }else{
      if (exists("mens")) {
        cat('Checking subSamples length-weight equations','\n')
        LW=LW.compute(catch=mens,codespname='CodEsp2',
                      Lname='Lcm',Wname='PM',wname='NBIND',plotit=TRUE,Nmin=10,path.export=path.export)
      }  
      cat('No correction of catches mean weight','\n')
    }
    
    #adds time and positions
    head(Peche)
    head(stp)
    Pechei=merge(unique(stp[,c('NOSTA','STRATE','LATF','LONF','TC','SONDE',
                               'NOCHAL','GEAR')]),Peche,by.x='NOSTA',by.y='NOSTA',
                 all.x=FALSE)
    head(Pechei)
    dimopPeche2=length(unique(Pechei$NOSTA))
    
    #length(stp$NOSTA);length(unique(stp$NOSTA))
    #stp[duplicated(stp$NOSTA),]
    if (sum(is.na(Pechei$GENR_ESP)|Pechei$GENR_ESP=='')>0){
      stop('Missing species name in: ',
           paste(Pechei[is.na(Pechei$GENR_ESP)|Pechei$GENR_ESP=='','NOSTA'],
                 collapse=' / '))
    }
    
    Pechei$CodEsp=paste(Pechei$GENR_ESP,Pechei$STRATE,Pechei$SIGNEP,sep='-')
    if (dim(Pechei)[1]!=dimPeche0){
      cat('Differences in no. of operations in log and fishing data','\n')
      cat(as.character(unique(stp$NOSTA)[!unique(stp$NOSTA)%in%
                                           unique(Pechei$NOSTA)]),'hauls in stations and not in catches','\n')
      cat(as.character(unique(Pechei$NOSTA)[!unique(Pechei$NOSTA)%in%
                                              unique(stp$NOSTA)]),'hauls in catches and not in stations','\n')
    }
    #Peche$NOSTA[!Peche$NOSTA%in%stp$NOSTA]
    #Check for duplicated species
    ID=paste(Pechei$CodEsp,Pechei[,nid])
    lnochal=Pechei[duplicated(paste(Pechei$NOSTA,Pechei$CodEsp)),'NOSTA']
    #Aggregate duplicated species
    dimPeche1=dim(Pechei)[1]
    acr=aggregate.catches(Pechei=Pechei,spname='CodEsp',stname=nid)
    Pechei=acr$Pechei
    Nag=dimPeche1-dim(Pechei)[1]
    ID=paste(Pechei$CodEsp,Pechei$NOCHAL)
    lnochal=Pechei[duplicated(paste(Pechei$NOSTA,Pechei$CodEsp)),'NOSTA']
    lnochal=Peche[duplicated(ID),'NOSTA']
    if (length(lnochal)>0) cat ('Duplicated species in hauls','\n')
    nostachal=data.frame(NOSTA=sort(unique(Pechei$NOSTA)))
    nostachal$NOCHAL=seq(dim(nostachal)[1])
    #adds NOCHAL
    #Pechei=merge(Pechei,nostachal,by.x='NOSTA',by.y='NOSTA')
    Pechei$NOCHAL
    Peche=Pechei[order(Pechei$NOSTA),]
    Peche$LATF=as.numeric(gsub(',','.',Pechei$LATF))
    Peche$LONF=as.numeric(gsub(',','.',Pechei$LONF))
    head(Peche)
    dimPeche0
    dim(Peche)
    dimopPeche3=length(unique(Peche$NOSTA))
    if (T%in%(nchar(as.character(Peche$NOSTA))>5)){ 
      stop('Invalid station id in casino file')
    }
    if (F%in%(unique(as.character(Peche$NOSTA))%in%unique(as.character(stp$NOSTA)))){
      stop('Station IDs differ in log and fishing data')
    }    
    
    if (raptri.fusion==0){
      if (!is.null(path.raptri.pro)){
        #Add MOULE to pro catches
        mmoule=aggregate(Pechei[,c('MOULE','LM')],list(Pechei$CodEsp),mean)
        names(mmoule)=c('CodEsp','MOULE','LM')
        pro.long$CodEsp=as.character(pro.long$CodEsp)
        Pechei$CodEsp=as.character(Pechei$CodEsp)
        if (promoulit){
          dim(pro.long)
          unique(pro.long$CodEsp)[!unique(pro.long$CodEsp)%in%unique(mmoule$CodEsp)]
          pro.long=merge(pro.long,mmoule,by.x='CodEsp',by.y='CodEsp',all.x=FALSE)
          dim(pro.long) 
          #pro.long[is.na(pro.long$MOULE),]
        }
        names(pro.long)
        pro.long$NT=pro.long$PT*pro.long$MOULE
        #Reorder columns
        pro.long=pro.long[,names(Pechei)]
        #bind Thalassa and pro catches
        #names(Peche)
        Peche=rbind(Pechei,pro.long)
      }else{Peche=Pechei}  
    }else{Peche=Pechei}
    
    #Add TS coefficients -----
    
    if (!is.null(path.TS)){
      TSk=read.table(path.TS,header=TRUE,sep=';')
      TSk$BAC0=TSk$BAC
      #head(TSk)
      #adds TS coefficients to Peche df
      if (TSref){
        TSk$BAC=TSk$BACref
      }
      Peche=merge(Peche,TSk[TSk$Freq==Freq,c('Genre_esp','CAC','BAC')],
                  by.x='GENR_ESP',by.y='Genre_esp',all.x=TRUE)
      dim(Peche)
      length(unique(Peche$NOSTA))
    }
    
    if (!is.null(path.discard)){
      lhrm0=read.table(path.discard)
      lhrm=as.character(lhrm0[,1])
      # liste des chaluts a annuler
      lhrm=c(lhrm,as.character(Peche[Peche$STRATE=='NULL','NOSTA']))
      Peche$discard=FALSE
      if (exists("mens")){
        mens$discard=FALSE
        mens[(mens$NOSTA%in%lhrm),'discard']=TRUE
      }
      Peche[(Peche$NOSTA%in%lhrm),'discard']=TRUE

      if (length(lhrm)>0){
        cat('Hauls',lhrm,'discarded','\n')
      }
      Nhd=length(lhrm)
    }else{
      lhrm=NULL
      Nhd=0
      Peche$discard=FALSE
      if (exists("mens")){
        mens$discard=FALSE
      }
    }  
    
    #Export PECHE
    if (!is.null(path.export)){
      if (!is.null(path.raptri)){
        write.table(Peche,paste(path.export,'FishingTotalSamples.csv',sep=''),
                    row.names=FALSE,sep = ";")
        cat('Fishing total samples exported to:',paste(path.export,'FishingTotalSamples.csv',sep=''),'\n')
      }
      if (!is.null(path.mens)){
      write.table(mens,paste(path.export,'FishingSubSamples.csv',sep=''),
                  row.names=FALSE,sep = ";")
      cat('Fishing sub-samples exported to:',paste(path.export,'FishingSubSamples.csv',sep=''),'\n')
      }
    }
    
    #Compute nearest haul per deviation------------------------------
    if (!is.null(path.man)){
            
      lnh=nearest.haul(Pechei=Peche[!Peche$discard,],EsduDevi=EsduDevi,
                       EspDevi=EspDevi[EspDevi$DEV%in%names(EsduDevi)[!names(EsduDevi)%in%
                                                                        c("esdu.id","LONG","LAT","TC","zonesCLAS","zonesSURF")],],
                       Ndevi=paste('D',Ndevi,sep=''),corr=cos(mean(EsduDevi$LAT)*pi/180),nid=nid)
      
      #plot(EsduDevi$LONG,EsduDevi$LAT)
      #points(Peche$LONF,Peche$LATF,pch=16)
      
      names(lnh) 
      
      for (i in 1:length(lnh[[1]])){
        near.charefi=lnh$list.Assoc[[i]]
        if (i==1){
          near.charef=data.frame(Esdu=EsduDevi$esdu.id,
                                 X=near.charefi$NOCHAL)
          names(near.charef)[i+1]=paste('D',Ndevi[i],'.CREF',sep='')
        }else{
          near.charef=data.frame(near.charef,X=near.charefi$NOCHAL)
          names(near.charef)[i+1]=paste('D',Ndevi[i],'.CREF',sep='')    }
      } 
      
      EsduDevi.charef=merge(EsduDevi,near.charef,by.x='esdu.id',by.y='Esdu')
      #head(EsduDevi.charef)
      
      #Export formatted file --------------------------
      #*************************
      if (!appendit){
        chaman=EsduDevi.charef
        names(chaman)=gsub('\\.','',names(chaman))
      }else if (appendit){
        #List of .man files in man folder
        #     lman.import=import.man.folder(path.man,import.man=FALSE)
        #     names(lman.import)
        #     lfs.man=man.import$lfs.man
        #Current chaman
        chaman2=EsduDevi.charef
        #Import previous chaman.csv file
        chaman1=read.csv(paste(path.export,'chaman.csv',sep=''),
                         header=TRUE,sep=';')
        #select new ESDUs 
        sel=!chaman2$TC%in%as.character(chaman1$TC)
        chaman2s=chaman2[sel,]
        #append ESDUs
        chaman=rbind(chaman1,chaman2s)
        dim(chaman1);dim(chaman)
        cat(dim(chaman2s)[1],'new ESDUs added in chaman file','\n')
        names(chaman)=gsub('\\.','_',names(chaman))
      }
      chaman$TOTAL=rowSums(chaman[,as.character(unique(EspDevi$DEV))])
      if (!is.null(path.export)){
        write.table(chaman,paste(path.export,'chaman.csv',sep=''),
                    row.names=FALSE,sep = ";")
        cat('Scrutinising results exported to:',paste(path.export,'chaman.csv',sep=''),'\n')
      }
    }else{
      cat('No esdu-haul association computed','\n')
      EsduDevi.charef=NA
    }  
    # Plots ------------------------------
    # 2. Catches in wide format for piecharts
    #*************************
    #Catches and sA positions
    #*************************
    Pechef=Peche[!Peche$discard,]
    # catches per species
    Pechels=widen.catch(Pechef,nid=nid,nsp='GENR_ESP',lsp=lsp)
    names(Pechels)
    lspr=gsub('-','.',lsp)
    lsps=lspr[paste('PT',lspr,sep='.')%in%names(Pechels)]
    Pechels$TOT=apply(Pechels[,paste('PT',lsps,sep='.')],1,sum)
  
    # catches per species code
    Pechels2=widen.catch(Pechef,nid=nid,nsp='CodEsp',lsp=lsp)
      
    if (plot.species){Pechelex=Pechels
    }else {Pechelex=Pechels2}
    
    names(Pechelex)[-seq(2)]=gsub('PT.','',names(Pechelex)[-seq(2)])
    names(Pechelex)=gsub('\\.','_',names(Pechelex))
    Pechelex$TOTAL=rowSums(Pechelex[,names(Pechelex)[!names(Pechelex)%in%c(
      'CAMPAGNE',nid,'LONF','LAT','STRATE','SONDE','GEAR')]])
    dim(Pechelex)
    Pechelex=merge(Pechelex,unique(Pechef[,c('NOSTA','NOCHAL')]),by.x=nid,by.y=nid)
    #head(Pechelex)
    
    #Pechelex in log for display
    lPechelex=Pechelex
    lPechelex[,names(lPechelex)[!names(lPechelex)%in%c(
      'CAMPAGNE','NOSTA','NOCHAL','LONF','LAT','STRATE','SONDE','GEAR')]]=log(lPechelex[,names(lPechelex)[!names(lPechelex)%in%c(
        'CAMPAGNE','NOSTA','NOCHAL','LONF','LAT','STRATE','SONDE','GEAR')]]+1)
    #head(lPechelex)
    
    #export tables for GIS ------------------------
    if (!is.null(path.export)){
      write.table(Pechelex,paste(path.export,'peche_wide.csv',sep=''),
                  row.names=FALSE,sep = ";")
      write.table(lPechelex,paste(path.export,'peche_wide_log.csv',sep=''),
                  row.names=FALSE,sep = ";")
    }
    if (!is.null(path.export)){
      write.table(route,paste(path.export,'route.csv',sep=''),
                  row.names=FALSE,sep = ";")
    }  
    #Select main species
    #*************************  
    sps=names(Pechels)%in%c('CAMPAGNE','NOCHAL','NOSTA','LONF','LATF','STRATE','SONDE','GEAR',
                            paste('PT.',lsp,sep=''))
    Pechels3=Pechels[,sps]
    lsps=names(Pechels3)[!names(Pechels3)%in%c('NOSTA','CAMPAGNE','NOCHAL',
                                               'LONF','LATF','STRATE','SONDE','GEAR')]
    lsps2=substr(lsps,4,10)
    #head(Pechels3)
    dim(Pechels3)
    #If detailled casino file provided, plots route and haul composition
    if (dim(route)[1]>0){
      library(maps)
      #palette definition
      lcol=spcol[,'lcol'][match(lsps2,substr(spcol$sp,1,7))]
      #eventually, geodata subset selection     
      xrb=c(route$LONF,Pechels$LONF)
      yrb=c(route$LATF,Pechels$LATF)
      if (is.null(xlim)){
        xlim=c(min(xrb)-abs(max(xrb)-min(xrb))/10,
               max(xrb)+abs(max(xrb)-min(xrb))/10)
      }
      routes=route[route$LONF>=xlim[1]&route$LONF<=xlim[2],]
      Pecheis=Pechels[Pechels$LONF>=xlim[1]&Pechels$LONF<=xlim[2],]
      if (is.null(ylim)){
        ylim=c(min(yrb)-abs(max(yrb)-min(yrb))/10,
               max(yrb)+abs(max(yrb)-min(yrb))/10)
      }    
      routes=route[route$LAT>=ylim[1]&route$LAT<=ylim[2],]
      Pecheis=Pecheis[Pecheis$LATF>=ylim[1]&Pecheis$LATF<=ylim[2],]
      #Plot route and trawl hauls
      x11()
      map("WorldHires", xlim = xlim, ylim =ylim,fill=TRUE,col='lightgreen',
          main=cruise)
      #map("france", xlim = range(routes$LONF), ylim = range(routes$LATF),
      #    fill=TRUE,col='green')
      #map.cities(country = "france", capitals = 2)
      #plot(routes$LONF,routes$LATF,pch=1,col='grey50',main=cruise,xlab='',
      #     ylab='',asp=1/cos(mean(routes$LATF)*pi/180),type='n')
      #coast(fill=T,color='lightblue')
      box()
      map.axes()
      map.scale(x=min(xlim)+0.1,y=max(ylim)-0.1,ratio=FALSE)
      lines(routes$LONF,routes$LATF,pch=1,col='grey50')
      if (logit) {z=as.matrix(log(Pecheis[,lsps]+1))
      }else{z=as.matrix(Pecheis[,lsps])}
      pie.xy(x=Pecheis$LONF,y=Pecheis$LATF,z=z,pcoast=FALSE,pcol=lcol,draw1=FALSE,
             pradius=pradius,smax=smax)
      legend(legpos,legend=substr(lsps,4,11),
             fill=lcol,bg='white')
      text(Pecheis$LONF,y=Pecheis$LATF,Pecheis[,nid],col='grey60')
      mtext(cruise,line=1,cex=1.5)
      MBathyPlot(lon1=xlim[1],lon2=xlim[2],lat1=ylim[1],lat2=ylim[2],
                 resolution =4,newbathy=TRUE,
                 add=TRUE,deep=-450,shallow=-50,bstep=450,bcol='grey50',
                 drawlabels = TRUE)
    }
    
    
    if (!is.null(path.man)){
      
      lnh=nearest.haul(Pechei=Peche[!Peche$discard,],EsduDevi=EsduDevi,
                       EspDevi=EspDevi[EspDevi$DEV%in%names(EsduDevi)[!names(EsduDevi)%in%
                                                                        c("esdu.id","LONG","LAT","TC","zonesCLAS","zonesSURF")],],
                       Ndevi=paste('D',Ndevi,sep=''),corr=cos(mean(EsduDevi$LAT)*pi/180),nid=nid)
      
      show.esdu.refhaul(Ndevi=paste('D',Ndevi,sep=''),list.Associ=lnh[[2]],
                        Pechelsi=Pechels,Pechei=Peche[!Peche$discard,],EsduDevi=EsduDevi,
                        legpos='bottomleft',export.plot=path.export,
                        radSA=0.05,pradius=pradius,scale.SA=2,logit=TRUE,labelit=TRUE,
                        xlim=xlim,ylim=ylim,add.layer=NULL,nid=nid)
      
      cat('Esdu-hauls assocition plots generated in: ',path.export,'\n')
    }
    #graphics.off()
    cat('Number of unique operations in input operation file',Nope0,'\n') 
    cat('Number of unique operations in input catch file',dimopPeche0,'\n')
    cat('Number of unique operations in output catch file',
        length(unique(Peche$NOSTA)),'\n')
    cat('Number of fishing operations discarded',Nhd,'\n')
    if (exists("mens")){
      cat('Number of unique operations in input at-lengths file',dimopMens0,'\n')
      cat('Number of unique operations in output at-lengths file',
          length(unique(mens$NOSTA)),'\n')
      ops.check=unique(mens$NOSTA)[!unique(mens$NOSTA)%in%unique(Peche$NOSTA)]
      if (length(ops.check)>0) cat('Operations ',as.character(ops.check),' have subsamples but no total samples.','\n')
    }  
    cat('Number of rows of input catch file',dimPeche0,'\n')
    cat('Number of rows of output catch file',dim(Peche)[1],'\n')
    cat('Number of aggregated rows in output catch file',Nag,'\n')  
    
  }else {
    Peche=NULL
    Pechels=NULL
  }
  if (!exists("mens")) mens=NULL

list(Peche=Peche,Peche.wide=Pechels,chaman=EsduDevi.charef,EspDev=EspDevi,
  mens=mens,cas.pro=cas.pro,cas.ALL=cas.ALL,route=route,stp=stp)
}

 na.null=function(x){
   x[is.na(x)]=0
   x}

remainization.checkplots=function(lays,mantot2){
  
  #Total NASC from EIlayer ----------------------------
  x11()
  plot(lays[lays$celltype==4,'dateend'],
       lays[lays$celltype==4,'sa'],main='Total NASC')
  for (i in 0:19){
    points(mantot2[mantot2$class=='TOTAL'&mantot2$cellnum==i,'dateend'],
           mantot2[mantot2$class=='TOTAL'&mantot2$cellnum==i,'sa'],col=i,pch=i)
  } 
  #legend('topleft',legend=paste('Layer',0:19,sep=''),pch=seq(20),col=seq(20))
  
  #unscrutinized NASC ---------------------
  x11()
  plot(lays[lays$celltype==4,'dateend'],
       lays[lays$celltype==4,'sa'],main='Total and unscrutinized NASC')
  for (i in 0:19){
    points(D11new[D11new$cellnum==i,'dateend'],
           D11new[D11new$cellnum==i,'sa'],col=i,pch=i)
  }
  
  #scrutinized NASC ---------------------
  x11()
  plot(lays[lays$celltype==4,'dateend'],
       lays[lays$celltype==4,'sa'],main='Total and scrutinized NASC')
  for (i in 0:19){
    points(mantot.scrut[mantot.scrut$cellnum==i,'dateend'],
           mantot.scrut[mantot.scrut$cellnum==i,'sa'],col=i,pch=i)
  }
  
  #scrutinized NASC per layer ---------------------
  mantot2s=mantot2[mantot2$class!='TOTAL',]
  mantot2sa=aggregate(mantot2s$sa,list(mantot2s$dateend,mantot2s$cellnum),
                      sum)
  names(mantot2sa)=c("dateend","cellnum","satot")
  x11()
  plot(lays[lays$celltype==4,'dateend'],
       lays[lays$celltype==4,'sa'],main='Total and scrutinized NASC')
  for (i in 0:19){
    points(mantot2sa[mantot2sa$cellnum==i,'dateend'],
           mantot2sa[mantot2sa$cellnum==i,'satot'],col=i,pch=i)
  }
  
  #total scrutinized NASC ---------------------
  mantot2s=mantot2[mantot2$class!='TOTAL',]
  mantot2satot=aggregate(mantot2s$sa,list(mantot2s$dateend),
                         sum)
  names(mantot2satot)=c("dateend","satot")
  x11()
  plot(lays[lays$celltype==4,'dateend'],
       lays[lays$celltype==4,'sa'],main='Total and total scrutinized NASC')
  points(mantot2satot$dateend,mantot2satot$satot,col=2,pch=2)
  
}

#Function to convert mantota.wide to chaman format

mantota.wide2chaman=function(mantota.wide1,mantot1,cruise='',suffix=NA,fishdev=NULL){

  if (!is.na(suffix)){
    bys=paste('dateend',suffix,sep='.')
  }else{
    bys='dateend'
  }
  chaman1=merge(mantota.wide1,unique(mantot1[,c('dateend','cell.long','cell.lat','depth')]),
                by.x=bys,by.y='dateend')
  
  #if suffix!=NA replace ".suffix" by nothing
  if (!is.na(suffix)){
    names(chaman1)=gsub(paste("\\.",suffix,sep=''),"",names(chaman1))
  }  
  ncs=names(chaman1)[names(chaman1)%in%fishdev]
  ec1=matrix(rep(NA,dim(chaman1)[1]*length(ncs)),ncol=length(ncs))
  ec1=data.frame(ec1)
  names(ec1)=paste(ncs,'.CREF',sep='')
  chaman1=data.frame(chaman1,ec1)
  chaman1$zonesCLAS=NA
  chaman1$zonesSURF=NA
  chaman1$CAMPAGNE=cruise    
  names(chaman1)[names(chaman1)=='dateend']='TC'
  names(chaman1)[names(chaman1)=='NESU']='esdu.id'
  names(chaman1)[names(chaman1)=='cell.long']='LONG'
  names(chaman1)[names(chaman1)=='cell.lat']='LAT'
  chaman1=chaman1[,c("esdu.id","LONG","LAT","depth","TC",fishdev,"zonesCLAS","zonesSURF",
                     "CAMPAGNE",names(ec1))]
  chaman1
}


Tutti2EchoR=function(path.catch,path.operations,
                     path.sampleCategory=NULL,path.sp=NULL,path.param=NULL,
                     path.export=NULL,spCor=NULL,
                     lsp=NULL,
                     nsonde='Valeur.855',operation.peche=NULL,
                     haulTypes=data.frame(Valeur.1434=c("CLAS - Chalutage classique","SURF - Chalutage de surface",
                                                        "SURFS - Chalutage d'extreme surface",
                                  "SURFM - Chalutage en subsurface","CLASF - Chalutage prÃ¨s du fond",
                                  "CLASD - Chalutage dÃ©collÃ© de 2m du fond"),
                                          STRATE=c('CLAS','SURF','SURFS','SURFM','CLASF','CLASD')),
                     skipc=NULL,check.strate=TRUE,path.Reference=NULL,
                     path.indiv=NULL,gear.sel=NULL,lparam.biometry=NULL){
  
  #  OBJET ........: Script permettant la transformation du format generique issu de TUTTI au format EchoR              #
  # ********************************************************************************************
  #
  #  INSTITUT........:  IFREMER Nantes/EMH
  #  DATE ..................:  18 avril 2014
  #  CREE PAR .........:  Cornou Anne-Sophie
  #  MODIFIE PAR ......:  Doray Mathieu le 27-28-29-30/04/2014, A-S 16/04/2015, M.D. le 04/05/2015      
  # ******************************************************************************************** 
  
  
  # ******************************************************************************************** 
  #                                                                                                                     #
  #                         ETAPE 1 : Chargement du format generique                                                    #
  #                                                                                                                     #
  # ********************************************************************************************  

  # chargement du format generique
  # ******************************************************************************************** 
  # 1) Import table CATCH --------
  #CATCH=read.csv("C:/Campagne/Format_generique_Tutti_Mod_EchoR/Pelgas_2014/Version_Corrigee_05_05_2014/exportCruise-Campagne PELGAS_2014/exportCruise-0/catch.csv",header=T,sep=";",dec=".")
  CATCH=read.csv(path.catch,header=T,sep=";",dec=".")
  
  #CATCH=CATCH[CATCH$Code_station=='S0339',]
  
  # if (!is.null(path.indiv)&!is.null(path.Reference)){
  #   # 1) Import table indiv 
  #   indiv=read.csv(path.indiv,header=T,sep=";",dec=".")  
  #   head(indiv)
  #   
  #   Fichier_Reference=read.csv2(path.Reference,header=T,dec=".")
  #   head(Fichier_Reference)
  #   
  #   # Correction du catch (poids de catégorie non renseignés)
  #   CATCH=Correction_poids_sexe(CATCH)
  #   
  #   # Integration des observations individuelles dans le CATCH
  #   CATCH=obs_indiv_dans_catch(CATCH, indiv, path.Reference)
  # }
  
  # Import des données individuelles ---------
  if (!is.null(path.indiv)){
    # 1) Import table indiv --------
    indiv=read.csv(path.indiv,header=T,sep=";",dec=".")
    head(indiv)
  }else{
    indiv=NULL
  }
  
  # Import de la table des espèces ---------
  if (!is.null(path.sp)){
    # 1) Import table indiv --------
    sp.df=read.csv(path.sp,header=T,sep=";",dec=".")
    head(sp.df)
  }else{
    sp.df=NULL
  }

  # verifier que le fichier a bien ete importe
  head(CATCH)
  #head(CATCH)
  dim(CATCH)
  summary(CATCH)
  # n lignes 40 colonnes avec deux sous-categories, Class_tri et Sexe
  
  # patch pour que ca marche
  #CATCH$Poids_Classe_Taille=CATCH$Poids_Reference

  dim(CATCH) # verifier qu'on obtient 40 colonnes  
  names(CATCH)
  nostaCatch=unique(CATCH$Code_station)
  if(length(nostaCatch)==0) nostaCatch=unique(CATCH$Code_Station)
  cat('Nombre d operations dans CATCH :',length(nostaCatch),'\n')
  prosCatch=substr(nostaCatch,2,2)==5
  table(prosCatch)
  
  # 2) Import table OPERATION --------------------
  # ******************************************************************************************** 
  #OPERATION=read.csv("C:/Campagne/Format_generique_Tutti_Mod_EchoR/Pelgas_2014/Version_Corrigee_05_05_2014/exportCruise-Campagne PELGAS_2014/exportCruise-0/operation.csv",header=T,sep=";",dec=".")
  OPERATION=read.csv(path.operations,header=T,sep=";",dec=".")
  
  # verifier que le fichier a bien ete importe
  #head(OPERATION)
  dim(OPERATION)
  summary(OPERATION)
  head(OPERATION)
  nostaOP=unique(OPERATION$Code_Station)
  cat('Nombre d operations dans OPERATION :',length(nostaOP),'\n')
  prosOP=substr(nostaOP,2,2)==5
  table(prosOP)
  
  # 3) Import des parametres ----------
  # ******************************************************************************************** 
  if (!is.null(path.param)){
    #param=read.table(path.param,sep=';',header=TRUE)
    #param=read.table(path.param,sep=';',header=FALSE,skip=1)
    param=read.csv2(path.param)
    if (dim(param)[1]==0){
      stop('Fichier de parametres vide. Utiliser path.param=NULL pour ignorer.')
    }
    names(param)=c('Annee','Serie','Serie_Partielle','Code_station','Id_Operation','Poche',
                   'Code_PMFM','Libelle_PMFM','Valeur','Type','Survey','SONDE')
    nostaParam=unique(param$Code_station)
    cat('Nombre d operations dans param :',length(nostaParam),'\n')
    prosParam=substr(param$Code_station,2,2)==5
    table(prosParam)
  }else{
    param=NULL
    prosParam=NULL
  }
  
  # 4) import total and sub samples and sample categories (nouveaute Tutti 2016) --------
  # ******************************************************************************************** 
  if (!is.null(path.sampleCategory)){
    SAMPLECATEGORY=read.csv(path.sampleCategory,header=T,sep=";",dec=".")
    res=Tutti2EchoR_Part1(CATCH,OPERATION,SAMPLECATEGORY)
    SubSamples4EchoR=res[[1]]
    TotSamples4EchoR=res[[2]]
  }else{
    res=Tutti2EchoR_Part12015(CATCH,OPERATION,path.sp)
    SubSamples4EchoR=res[[1]]
    TotSamples4EchoR=res[[2]]
  }

  # 5) Sauvegarde des fichier --------
  # ******************************************************************************************** 
  if (!is.null(path.export)){
    write.table(SubSamples4EchoR,paste(path.export,"SubSamples4EchoR.csv",sep=''),row.names=F,quote=F,sep=";")
    cat('SubSamples data written to:',paste(path.export,"SubSamples4EchoR.csv",sep=''),'\n')
    write.table(TotSamples4EchoR,paste(path.export,"TotSamples4EchoR.csv",sep=''),row.names=F,quote=F,sep=";")
    cat('TotSamples data written to:',paste(path.export,"TotSamples4EchoR.csv",sep=''),'\n')
    
  }
  
  nostaTS=unique(TotSamples4EchoR$operationID)
  cat('Nombre d operations dans total samples :',length(nostaTS),'\n')
  prosTS=substr(TotSamples4EchoR$operationID,2,2)==5
  table(prosTS)
  nostaSS=unique(SubSamples4EchoR$operationID)
  cat('Nombre d operations dans sub samples :',length(nostaSS),'\n')
  prosSS=substr(SubSamples4EchoR$operationID,2,2)==5
  table(prosSS)
  
  # 6) cree peche.wide pour SIG --------------------------------------
  # ********************************************************************************************
  if (!is.null(path.param)){
    param$Valeur=as.character(param$Valeur)
    param$Code_PMFM=as.character(param$Code_PMFM)
    param.wide=reshape(param[,c('Code_station','Code_PMFM','Valeur')],
                       v.names='Valeur',idvar = "Code_station",
                       timevar = "Code_PMFM", direction = "wide")
    dim(param.wide)
    pwNA=param.wide[is.na(param.wide$Valeur.1434),]
    length(unique(param$Code_station))
    if (dim(pwNA)[1]>0){
      cat(paste('Type missing in operations: ',pwNA$Code_station,'\n'))
    }
    unique(param.wide$Valeur.1434)
    param.wide$STRATE=substr(param.wide$Valeur.1434,1,5)
    # enleve espaces
    param.wide$STRATE=gsub(' ','',param.wide$STRATE)
    if (sum(is.na(param.wide$STRATE))>0){
      cat('Missing or unknown haul categories in param.wide','\n')
    }
    names(param.wide)[names(param.wide)==nsonde]='SONDE'
    dim(param.wide)
    param.wide=merge(param.wide,OPERATION[,c('Code_Station','Engin')],by.x='Code_station',by.y='Code_Station')
    dim(param.wide)
    #head(param.wide)
    names(param.wide)[names(param.wide)=='Engin']='GEAR'
    nostaParamW=unique(param.wide$Code_station)
    cat('Nombre d operations dans param.wide :',length(nostaParamW),'\n')
    prosParamW=substr(param.wide$Code_station,2,2)==5
    table(prosParamW)
    unique(param.wide$GEAR)
    dim(param.wide[param.wide$GEAR=='MIK 1600 Âµm, cylindre diamÃ¨tre 2 m',])
    if (check.strate){
      if (sum(!param.wide$STRATE%in%haulTypes$STRATE)){
        stop('Strate incorrecte dans :',paste(param.wide[!param.wide$STRATE%in%haulTypes$STRATE,
                                                         'Code_station'],collapse = ',')) 
      }
    }
  }else{
    param.wide=NULL
  }
  
  # 7) fusionne captures, biometries et operations --------------------
  #***************************************  
  t2f.res=Tutti2FishRview(TotalSamples=TotSamples4EchoR,operation=OPERATION,
                          param.wide=param.wide,lsp=lsp,
                          operation.peche=operation.peche)
  names(t2f.res)
  Peche.wide=t2f.res$Peche0.wide
  dim(Peche.wide)
  #head(Peche.wide)
  #TotSamples4EchoR[TotSamples4EchoR$operationID=='U0230',]
  #Peche.wide[Peche.wide$NOSTA=='U0230',]
  nostaPW=unique(Peche.wide$NOSTA)
  cat('Nombre d operations dans peche.wide :',length(nostaPW),'\n')
  prosPW=substr(Peche.wide$NOSTA,2,2)==5
  table(prosPW)
  
  lsps=unique(CATCH[,c('Code_Espece_Campagne','Nom_Scientifique')])
  if (!is.null(path.indiv)){
    head(indiv)
    if (!is.null(lparam.biometry)){
      indivs=indiv[indiv$Libelle_PMFM%in%lparam.biometry,]      
    }else{
      indivs=indiv
    }
    biometry=merge(indivs,lsps,by.x='Nom_Scientifique',by.y='Nom_Scientifique')
  }else{
    biometry=NULL
  }
  head(biometry)
  
  # 8) Cree fichier metadata complet avec operations et parameters ---------------
  #***************************************
  if (!is.null(path.param)){
    names(OPERATION)
    names(param.wide)
    metadata=merge(OPERATION[,c('Annee','Serie','Serie_Partielle','Code_Station','Engin','Navire','DateDeb','LatDeb',
                                'LongDeb','DateFin','LatFin','LongFin','Duree')],
                   param.wide[,c('Code_station','SONDE','STRATE')],by.x='Code_Station',
                   by.y='Code_station',all.x=TRUE)
    metadata$Serie_Partielle=as.character(metadata$Serie_Partielle)
    
    # 9) Check strate et sonde ------
    # ********************************************************************************************
    if (check.strate){
      if (!is.null(skipc)){
        metadatas=metadata[!metadata$Code_Station%in%skipc,]
      }else{
        metadatas=metadata
      }
      if (sum(is.na(metadatas$STRATE))>0){
        stop('Strate manquante dans :',
             paste(metadatas[is.na(metadatas$STRATE),'Code_Station'],collapse=','),'\n')
      }
      if (sum(is.na(metadatas$SONDE))>0){
        stop('Sonde manquante dans :',
             paste(metadatas[is.na(metadatas$STRATE),'Code_Station'],collapse=','),'\n')
      }
    }
  }else{
    metadata=NULL
  }
  # Sauvegarde du fichier
  dim(TotSamples4EchoR)
  names(TotSamples4EchoR)
  # x lignes 9 colonnes
  #head(TotSamples4EchoR)
  if (sum(is.na(TotSamples4EchoR$baracoudacode)|TotSamples4EchoR$baracoudacode=='')>0){
    ope.pb=unique(TotSamples4EchoR[
      is.na(TotSamples4EchoR$baracoudacode)|
        TotSamples4EchoR$baracoudacode=='','operationID'])
    if (!is.null(gear.sel)){
      if(sum(OPERATION$EnginM%in%gear.sel&
                   OPERATION$Code_Station%in%ope.pb)>0){
        stop('Missing species name in: ',paste(ope.pb,collapse =' / '))
      }
    }
  }
  
  if (!is.null(path.export)){
    write.table(TotSamples4EchoR,paste(path.export,"TotSamples4EchoR_B.csv",sep=''),row.names=F,quote=F,sep=";")
    cat('Total samples data written to:',paste(path.export,"TotSamples4EchoR_B.csv",sep=''),'\n')
  }
  
  list(SubSamples4EchoR=SubSamples4EchoR,TotalSamples4EchoR=TotSamples4EchoR,
       OPERATION=OPERATION,param=param,
       param.wide=param.wide,Peche.wide=Peche.wide,metadata=metadata,
       CATCH=CATCH,biometry=biometry,sp.df=sp.df)
}

Tutti2FishRview=function(TotalSamples,operation,param.wide=NULL,operation.peche=NULL,SubSamples=NULL,
                         path.export=NULL,lsp=NULL){
  Peche0=TotalSamples
  names(Peche0)=c('NOSTA','GENR_ESP','SIGNEP','PT','NT','LM','PM','MOULE','PTRI','NAVIRE')
  Peche0$CodEsp=paste(Peche0$GENR_ESP,Peche0$SIGNEP,sep='-')
  Peche0$CAMPAGNE=paste(substr(unique(operation$Serie),10,15),unique(operation$Annee),sep='')
  Peche0$CAMPAGNE=paste(Peche0$CAMPAGNE,Peche0$NAVIRE)
  # ajoute coordonnees de tutti ------
  # ********************************************************************************************
  dim(Peche0)
  Peche0=merge(Peche0,unique(operation[,c('Code_Station','LongDeb','LatDeb')]),by.x='NOSTA',by.y='Code_Station',
               all.x=TRUE)
  dim(Peche0)
  # ajoute strates de tutti
  #head(param.wide)
  if (!is.null(param.wide)){
    Peche0=merge(Peche0,unique(param.wide[,c('Code_station','STRATE','SONDE','GEAR')]),by.x='NOSTA',
                 by.y='Code_station',
                 all.x=TRUE)
  }else{
    Peche0$STRATE=NA
    Peche0$SONDE=NA
    Peche0$GEAR=NA
  }
  dim(Peche0)
  #head(Peche0)
  names(Peche0)[names(Peche0)%in%c('LongDeb','LatDeb')]=c('LONF','LATF')
  #head(Peche0)
  
  # si coordonnees manquantes, ajoute coordonnees de casino ------
  # ********************************************************************************************
  
  if (!is.null(operation.peche)&(sum(is.na(Peche0$LONF)>0)|sum(is.na(Peche0$LATF)>0)|sum(is.na(Peche0$STRATE)>0))){
    p1=Peche0[is.na(Peche0$LONF)|is.na(Peche0$STRATE),]
    p2=Peche0[!(is.na(Peche0$LONF)|is.na(Peche0$STRATE)),]
    dim(p1)
    p1=merge(p1[,!names(Peche0)%in%c('LONF','LATF','STRATE')],
             unique(operation.peche[,c('NumStation','lon','lat','STRATE')]),
             by.x='NOSTA',by.y='NumStation')
    names(p1)[names(p1)%in%c('lon','lat')]=c('LONF','LATF')
    dim(p1)
    dim(Peche0)
    Peche0=rbind(p1,p2)
    dim(Peche0)
  }
  
  if (sum(is.na(Peche0$LONF))>0) cat('Positions de chaluts manquantes : ',
                                     unique(as.character(Peche0[is.na(Peche0$LONF),'NOSTA'])))
  
  if (!is.null(SubSamples)){
    mensuration=SubSamples
    names(mensuration)=c('NOSTA','Poche','GENR_ESP','SIGNEP','SEXE','PTOT','NTOT','L','N','POIDSTAILLE','UNITEs','ARRONDI',
                         'NAVIRE')    
  }else{mensuration=NULL}
  # ajoute especes manquantes au fichier peche -----
  # ********************************************************************************************
  if (is.null(lsp)){
    lsp=unique(Peche0$GENR_ESP)
  }
  lsp1=gsub('[.]','-',lsp)
  lsp0=gsub('-','.',Peche0$GENR_ESP)
  lsp2=unique(lsp0)
  msp=lsp[!lsp%in%lsp2]
  if (length(msp)>0){
    for (i in 1:length(msp)){
      dfi=Peche0[1,]
      dfi$GENR_ESP=msp[i]
      dfi$PT=0
      if (i==1) dfa=dfi
      else dfa=rbind(dfa,dfi)
    }
    Peche02=rbind(Peche0,dfa)
    Peche02=Peche02[Peche02$GENR_ESP%in%lsp1,]
  }else{
    Peche02=Peche0[Peche0$GENR_ESP%in%lsp1,]
  }
  unique(Peche02$GENR_ESP)  
  
  # Fichier peche pour SIG -----
  # ********************************************************************************************
  Peche0.wide=widen.catch(Pechef=Peche02,nid='NOSTA',nsp='GENR_ESP',lsp=lsp)
  names(Peche0.wide)=gsub('[.]','_',names(Peche0.wide))
  Peche0.wide$TOT=apply(Peche0.wide[,names(Peche0.wide)[!names(Peche0.wide)%in%c('CAMPAGNE','NOSTA','LONF','LATF',
                                                                                 'STRATE','SONDE','GEAR')]],1,sum)
  #head(Peche0.wide)
  
  # Export control and GIS compliant files -----
  # ********************************************************************************************
  if (!is.null(path.export)){
    write.table(Peche0.wide,paste(path.export,"PecheSIG.csv",sep=''),row.names=F,quote=F,sep=",")
    write.table(Peche0,paste(path.export,"TRI.csv",sep=''),row.names=F,quote=F,sep=";")
    write.table(mensuration,paste(path.export,"mensuration.csv",sep=''),row.names=F,quote=F,sep=";")
  }  
  list(Peche0.wide=Peche0.wide,TRI=Peche0,mensuration=mensuration)
}

casino.import=function(path.cas,stp.gears=c('76x70','57x52'),stp.events=c('DFIL','DPLON'),
                       gear.events=c('HQ','DFIL','FFIL','DVIR','FVIR','CULAB'),
                       survey.events=c('DEBUCAMP','DEBULEG','FINCAMP','FINLEG'),
                       start.events=c('DEBURAD','DEBUTRA','REPRAD','REPTRA','DCONFIG'),
                       stop.events=c('STOPRAD','FINRAD','FINTRA','STOPTRA'),
                       seq.start=c('DEBURAD','DEBUTRA'),
                       seq.stop=c('REPRAD','REPTRA','DCONFIG'),
                       nsonde=c("CINNA.Sonde.vert...m.","Sonde.ref...m."),
                       cruiseCode='PG18',vesselName='THALASSA II',
                       sep='\t',runlag=0,dstart=NULL,lstrates=c('CLAS','SURF','NULL'),
                       check.strates=TRUE,get.jackpot=TRUE){
  
  #Import Casino events file -----------------------------
  #********************************************
  cas=read.csv(path.cas,header=TRUE,sep=sep)  
  #names(cas)
  names(cas)[14]='N..Station'
  cas=cas[,names(cas)!='X']
  names(cas)
  
  #Select and format Casino stations ------------------------------
  #********************************************
  cat('No of hauls: ',
      length(cas[cas$Code.Action=='DVIR','N..Station']),'\n')
  head(cas)
  evts=cas[,c('Date','Heure','Latitude','Longitude','Code.Appareil','Code.Action','N..Station','Strate',
              'Identificateur.Operation')]
  names(evts)=c('date','heure','LATF','LONF','GEAR','EVT','NOSTA','STRATEL','ID')
  head(evts)
  stp=evts[evts$GEAR%in%stp.gears,]    
  stp=stp[stp$EVT%in%stp.events,]
  stp$NOCHAL=NA
  stp$TC=paste(stp$date,stp$heure)
  stp$STRATE=substr(stp$STRATEL,1,4)
  stp$LATF=as.numeric(gsub(',','.',stp$LATF))
  stp$LONF=as.numeric(gsub(',','.',stp$LONF))  
  stp=stp[order(stp$NOSTA),]
  stp$NOCHAL=seq(dim(stp)[1])
  
  sonde=cas[,nsonde]
  names(sonde)=c('SONDE12','SONDEref')
  sonde$SONDE12=as.numeric(gsub(',','.',sonde$SONDE12))
  sonde$SONDEref=as.numeric(gsub(',','.',sonde$SONDEref))             
  sondes=sonde[(evts$GEAR%in%stp.gears)&(evts$EVT%in%stp.events),]
  sondes$SONDE=NA
  for (i in 1:dim(sondes)[1]){
    if (sondes$SONDE12[i]%in%c(-1,0)&!sondes$SONDEref[i]%in%c(-1,0)){
      sondes$SONDE[i]=sondes$SONDEref[i]
    }else if (!sondes$SONDE12[i]%in%c(-1,0)&sondes$SONDEref[i]%in%c(-1,0)){
      sondes$SONDE[i]=sondes$SONDE12[i]
    }else if (!sondes$SONDE12[i]%in%c(-1,0)&!sondes$SONDEref[i]%in%c(-1,0)){
      sondes$SONDE[i]=sondes$SONDEref[i]
    }else{
      sondes$SONDE[i]=NA
    }
  }
  stp$SONDE=sondes$SONDE  
  
  head(stp)  
  if (T%in%(nchar(as.character(stp$NOSTA))>5)){ 
    stop('Invalid station id in casino file')
  }
  dim(stp)
  
  #Remove rows with NA --------------------
  #********************************************
  stp=stp[!is.na(stp$date),]  
  dim(stp)  
  #Check "strate"
  #head(stp)
  strate.errors=stp[!substr(stp$STRATE,1,4)%in%lstrates,]
  if (check.strates){  
    if (dim(strate.errors)[1]>0){
      cat("Invalid 'strate' fields in hauls:",
          as.character(unique(strate.errors$NOSTA)),'\n')
      stop('Please correct errors before proceeding')
    }
    Nope0=dim(stp)[1]
  }
  
  stp$rad=NA  
  
  #check that no trawl action is missing ------------------------------------
  #********************************************
  stp2=evts[evts$GEAR%in%stp.gears,]    
  stp2=stp2[stp2$EVT%in%gear.events,]
  head(stp2)
  
  lt=unique(stp2$NOSTA)
  for (i in 1:length(lt)){
    stpi=stp2[stp2$NOSTA==lt[i],]
    if (sum(stpi$EVT%in%gear.events)!=length(gear.events)){
      cat('Action de chalutage manquante ou en trop dans :',as.character(lt[i]),'Merci de compl?ter','\n')
    }
  }
  
  #check that no survey or leg  event is missing -----------------------------------
  #********************************************
  head(evts)
  stp3=evts[evts$EVT%in%survey.events,]    
  head(stp3)
  
  lt=unique(stp3$ID)
  for (i in 1:length(lt)){
    stpi=stp3[stp3$ID==lt[i],]
    if (sum(stpi$EVT%in%survey.events)!=2){
      cat('Debut ou fin de leg/campagne manquante dans identificateur operation :',as.character(lt[i]),'Merci de compl?ter','\n')
    }
  }
  
  #jackpot: Select and format acoustic transects ------------------------------
  #*********************************************************
  #Vessel specific data: Transect
  #*********************************************************
  if (get.jackpot){
    #select transects ----------------
    #********************************************
    Transecti=cas[!cas$Code.Appareil%in%c('CAMP','')&!cas$Code.Action%in%c('CUFIX','NEXT'),]
    #names(Transecti)
    #Correct acoustic stratum ----------------
    #********************************************
    unique(Transecti$Strate)
    Transecti$Strate=as.character(Transecti$Strate)
    Transecti$Strate=gsub(' ','',Transecti$Strate)
    Transecti$Strate=gsub('Rad ','R',Transecti$Strate,ignore.case = TRUE)
    Transecti$Strate=gsub('Rad','R',Transecti$Strate,ignore.case = TRUE)
    Transecti$stratum=substr(Transecti$Strate,1,3)
    Transecti$stratum=gsub(' ','',Transecti$stratum)
    Transecti[nchar(Transecti$stratum)==2,'Stratum']=paste(substr(Transecti[nchar(Transecti$stratum)==2,'stratum'],
                                                                  1,1),'0',
                                                           substr(Transecti[nchar(Transecti$stratum)==2,'stratum'],
                                                                  2,2),sep='')
    Transecti$ID=paste(Transecti$Identificateur.Operation,Transecti$stratum)
    Transecti$vesselName=vesselName
    Transecti$vesselName=as.character(Transecti$vesselName)
    Transecti$rad=substr(Transecti$Strate,1,3)
    Transecti$direction=substr(Transecti$Strate,4,8)
    Transecti$direction=gsub(' ','',Transecti$direction)
    Transecti$direction=gsub('->','',Transecti$direction)
    Transecti$rad=gsub(' ','',Transecti$rad)
    
    Transecti[is.na(Transecti$rad),'Strate']
    Transecti[Transecti$Code.Action=='ECHO','direction']=as.character(Transecti[Transecti$Code.Action=='ECHO',
                                                                                'Observation'])
    Transectic=Transecti[as.character(Transecti$Code.Action)%in%c(start.events,stop.events),
                         c('Date','Code.Action','Strate','rad','direction')]
    pbDirection=!(Transectic$direction%in%c('MC','CM','LM','ML','CL','LC','MM'))
    if (sum(pbDirection)>0){
      Transectic[pbDirection,]
      stop('Strate incorrecte dans ',paste(Transectic[pbDirection,'Date'],
                                           Transectic[pbDirection,'Code.Action'],
                                           Transectic[pbDirection,'Strate']),collapse='\n')
    }
    names(Transecti)
    names(Transecti)[c(1,2,3,4,9,11,14,83,124,125)]
    #   jackpot=Transecti[Transecti$Code.Action%in%c('DEBURAD','STOPRAD','DEBUTRA','FINTRA','STOPTRA','REPTRA','HQ',
    #                                                'CULAB','REPRAD','FINRAD','ECHO','DCONFIG'),c(1,2,3,4,9,11,14,83,124,125)]
    jackpot=Transecti[Transecti$Code.Action%in%c('DEBURAD','STOPRAD','DEBUTRA','FINTRA','STOPTRA','REPTRA','HQ',
                                                 'CULAB','REPRAD','FINRAD','ECHO','DCONFIG'),
                      c("Date","Heure","Latitude","Longitude","Code.Appareil","Code.Action","N..Station",
                        nsonde[2],"rad","direction")]
    names(jackpot)=c('date','heure','lat','lon','appareil','action','NumStation','profondeur','rad','direction')
    jackpot[,c('lat','lon')]=apply(jackpot[,c('lat','lon')],2,FUN=gsub,pattern=',',replacement='.')
    jackpot[,c('lat','lon')]=apply(jackpot[,c('lat','lon')],2,FUN=as.numeric)
    tl=strptime(paste(as.character(jackpot$date),as.character(jackpot$heure)),"%d/%m/%Y %H:%M:%OS")
    tld=strptime(as.character(jackpot$date),"%d/%m/%Y")
    if (!is.null(dstart)){
      jackpot=jackpot[tl>strptime(dstart,"%d/%m/%Y %H:%M:%OS"),]
    }
    tld=strptime(as.character(jackpot$date),"%d/%m/%Y")
    jackpot$run=runlag+as.numeric((tld-min(tld))/(3600*24)+1)
    jackpot=jackpot[order(jackpot$run,jackpot$heure),]
    jackpot$Nesdu.indicatif=NA
    jackpot$prefix=NA
    head(jackpot)
    
    jackpot.naetc=jackpot[jackpot$action%in%c('DEBURAD')&(is.na(jackpot$rad)|is.null(jackpot$rad)|jackpot$rad==''|is.na(jackpot$direction)|
                                                            is.null(jackpot$direction)|jackpot$direction==''),]
    jackpots=jackpot[jackpot$action!='ECHO',]
    
    for (i in 1:(length(jackpots$date)-1)){
      #print(i)
      if (jackpots$action[i]%in%start.events){
        if (jackpots$action[i+1]%in%stop.events){
          #print(paste(jackpots[i,'date'],jackpots[i,'heure'],jackpots[i,'action'],
          #            jackpots[i+1,'date'],jackpots[i+1,'heure'],jackpots[i+1,'action']))
          jackpots$Nesdu.indicatif[i]=ceiling(dist(data.frame(xcor=jackpots[i:(i+1),'lon']*cos(46*180/pi),
                                                              y=jackpots[i:(i+1),'lat']))*60)
          if (is.na(jackpots$rad[i+1])|(jackpots$rad[i+1]!=jackpots$rad[i])) jackpots$rad[i+1]=jackpots$rad[i]
          if (is.na(jackpots$direction[i+1])|(jackpots$direction[i+1]!=jackpots$direction[i])) jackpots$direction[i+1]=jackpot$direction[i]
          # creates prefix
          if (jackpots$action[i]%in%seq.start) {
            rad=jackpots$rad[i]
            prefi=paste(jackpots$rad[i],'_',jackpots$direction[i],'-a','_',cruiseCode,sep='')
            prefi1=paste(jackpots$rad[i],'_',jackpots$direction[i],sep='')
          }else if (jackpots$action[i]%in%seq.stop){
            rad=c(rad,jackpots$rad[i])
            ll=c('a','b','c','d','e','f','g','h','i','j','k','l','m')
            prefi=paste(prefi1,'-',ll[length(rad)],'_',cruiseCode,sep='')
          }  
          jackpots$prefix[i]=prefi
          errorsi=jackpots[FALSE,]
        }else if (jackpots$action[i+1]=='DCONFIG'){
          #            jackpots[i+1,'date'],jackpots[i+1,'heure'],jackpots[i+1,'action']))
          jackpots$Nesdu.indicatif[i]=ceiling(dist(data.frame(xcor=jackpots[i:(i+1),'lon']*cos(46*180/pi),
                                                              y=jackpots[i:(i+1),'lat']))*60)
          #correct rad and direction
          if (is.na(jackpots$rad[i+1])|(jackpots$rad[i+1]!=jackpots$rad[i])) jackpots$rad[i+1]=jackpots$rad[i]
          if (is.na(jackpots$direction[i+1])|(jackpots$direction[i+1]!=jackpots$direction[i])) jackpots$direction[i+1]=jackpot$direction[i]
          # creates prefix
          if (jackpots$action[i]%in%seq.start) {
            rad=jackpots$rad[i]
            prefi=paste(jackpots$rad[i],'_',jackpots$direction[i],'-a','_',cruiseCode,sep='')
            prefi1=paste(jackpots$rad[i],'_',jackpots$direction[i],sep='')
          }else if (jackpots$action[i]%in%seq.stop){
            rad=c(rad,jackpots$rad[i])
            ll=c('a','b','c','d','e','f','g','h','i','j','k','l','m')
            prefi=paste(prefi1,'-',ll[length(rad)],'_',cruiseCode,sep='')
          }
          jackpots$prefix[i]=prefi
          errorsi=jackpots[FALSE,]
        }else{
          print(paste('Erreur detectee dans : ',jackpots[i,'date'],jackpots[i,'heure'],jackpots[i,'action'],
                      jackpots[i+1,'date'],jackpots[i+1,'heure'],jackpots[i+1,'action']))
          errorsi=jackpots[i,]
          #print(i)
        }
      } 
      if (i==1) errors=errorsi
      else errors=rbind(errors,errorsi)
    }
    
    errors2=jackpots[(jackpots$action%in%seq.start)&(jackpots$direction==''|jackpots$rad==''),]
    
    errors=rbind(errors,errors2)
    
    if (dim(errors)[1]>0) cat('Il y a  des erreurs dans le jackpot, consultable dans "errors",merci de les corriger')
    
    jackpot=rbind(jackpots,jackpot[jackpot$action=='ECHO',])
    tl=strptime(paste(as.character(jackpot$date),as.character(jackpot$heure)),"%d/%m/%Y %H:%M:%OS")
    tlo=order(tl)
    jackpot=jackpot[tlo,]
    jackpot=jackpot[,c("date","run","heure","lat","lon","appareil","action","NumStation","profondeur","rad",
                       "direction","Nesdu.indicatif","prefix")]
    
    if (dim(jackpot.naetc)[1]>0&dim(errors)[1]>0) {errors=rbind(errors,jackpot.naetc)
    }else if (dim(jackpot.naetc)[1]>0&dim(errors)[1]==0) {errors=jackpot.naetc}
    
    #if (dim(errors)[1]>0) cat('Errors in transect breaks:',paste(errors[,c('date')],errors[,c('heure')],' '),'see errors file','\n')
    head(jackpot)
  }else{
    jackpot=NA
    errors=NA
  }
  list(jackpot=jackpot,stp=stp,errors=errors,cas=cas)
}


convert2cm=function(peche.df,mens.df,convert=list(totalSamples=TRUE,subSamples=TRUE),
                    correctLM=FALSE){
  # add length units to totalsamples is needed
  if (sum(!c('units','round')%in%names(peche.df))==2){
    d0=dim(peche.df)[1]
    peche.df=merge(peche.df,
                   unique(mens.df[,c('operationId','baracoudaCode','sizeCategory','units',
                                     'round')]),
                   by.x=c('operationId','baracoudaCode','sizeCategory'),
                   by.y=c('operationId','baracoudaCode','sizeCategory'))
    d1=dim(peche.df)[1]
    if (d1>d0){
      uids=paste(peche.df$operationId,peche.df$baracoudaCode,peche.df$sizeCategory,
                 peche.df$units,peche.df$round)
      duids=uids[duplicated(uids)]
      stop('Several units/round values for samples: ',paste(duids, collapse=' '))
    }
  }
  if (convert$subSamples){
    # convert subsamples length to cm
    mens.df[mens.df$units==0,'lengthClass']=mens.df[mens.df$units==0,'lengthClass']/10
    mens.df[mens.df$units==0,'units']=1
    cat('Lengths in subsamples converted to cm','\n')
  }
  if (convert$totalSamples){
    # convert totalsamples mean length to cm
    peche.df[peche.df$units==0,'meanLength']=peche.df[peche.df$units==0,'meanLength']/100
    peche.df[peche.df$units==0,'units']=1
    cat('Mean lengths in totalsamples converted to cm','\n')
    if (correctLM){
      mens.df[mens.df$units==0,'lengthClass']=mens.df[mens.df$units==0,'lengthClass']/10
      mens.df[mens.df$units==0,'units']=1
      no=names(mens.df)
      cat('Lengths in subsamples converted to cm','\n')
      mens.dfa=aggregate(mens.df$numberAtLength,
                         list(operationId=mens.df$operationId,
                              baracoudaCode=mens.df$baracoudaCode,
                              sizeCategory=mens.df$sizeCategory),sum,na.rm=TRUE)
      names(mens.dfa)[4]='Ntot'
      mens.df=merge(mens.df,mens.dfa)
      mens.df$Li=mens.df$lengthClass*mens.df$numberAtLength/mens.df$Ntot
      mens.dfla=aggregate(mens.df$Li,
                          list(operationId=mens.df$operationId,
                               baracoudaCode=mens.df$baracoudaCode,
                               sizeCategory=mens.df$sizeCategory),sum,na.rm=TRUE)
      names(mens.dfla)[4]='meanLength2'
      head(mens.dfla)
      nts=names(peche.df)
      peche.df=merge(peche.df,mens.dfla)
      peche.df[,c('meanLength','meanLength2')]
      peche.df$meanLength=peche.df$meanLength2
      peche.df=peche.df[,nts]
      mens.df=mens.df[,no]
      cat('Mean lengths in totalsamples corrected based on subsamples','\n')
    }
  }
  list(peche.df=peche.df,mens.df=mens.df)
}

Tutti2EchoR_Part12015=function(CATCH,OPERATION,path.sp){
  #OPERATION=OPERATION[OPERATION$Code_Station=='S0339',]
  
  # Suppression des sous_categorie >2, ici conserve que les deux premieres
  if(dim(CATCH)[2]>=40){
    CATCH=CATCH[,c(1:29,(dim(CATCH)[2]-10):dim(CATCH)[2])]
  }
  
  # 3) Detection des cas de Benthos Saisie et des especes saisies sans Nom de campagne
  Resume_CATCH=CATCH[,c("Code_station","Id_Operation","Code_Taxon","Code_Espece_Campagne","Nom_scientifique","V_HV","Num_Ordre_V_HV_H2")]
  
  for (i in 1:dim(Resume_CATCH)[1]){
    if(Resume_CATCH[i,"Num_Ordre_V_HV_H2"]>=100){
      print("Attention, saisie de Benthos dans le trait")
      print(Resume_CATCH[i,"Code_station"])
      print("Espece")
      print(Resume_CATCH[i,"Nom_scientifique"])
    }
    if((Resume_CATCH[i,"Code_Espece_Campagne"]=="")&is.null(spCor)){
      print("Attention, une espece sans nom de campagne a ete detectee dans le trait : ")
      print(Resume_CATCH[i,"Code_station"])
      print("Espece")
      print(Resume_CATCH[i,"Nom_scientifique"])
    }
  }  
  
  # 4) Detection des cas de "Poids Inerte tries" et des "Poids vivants non detailles tries"
  
  for (i in 1:dim(OPERATION)[1]){
    if(OPERATION[i,"Poids_Total_Espece_Inerte_Trie"]!=0){
      print("Attention, saisie d'un poids inerte trie dans le trait")
      print(OPERATION[i,"Code_Station"])
    }
    if(OPERATION[i,"Poids_Total_Espece_Vivant_non_detaille_trie"]!=0){
      print("Attention, saisie d'un poids d'espece vivant non detaille trie dans le trait : ")
      print(OPERATION[i,"Code_Station"])
    }
  }
  
  
  
  # 5) Import de la liste d'especes
  if (!is.null(path.sp)){
    lsp=read.table(path.sp,sep=';',header=TRUE)    
  }

  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  #                                                                                                                     #
  #                         ETAPE 2 : Construction du fichier SubSamples4EchoR.csv                                      #
  #                                                                                                                     #
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  
  # 0) Suppression des lignes concernant le Benthos
  id_benthos=which(CATCH[,"Num_Ordre_V_HV_H2"]>=100)
  if(length(id_benthos)>0&id_benthos[1]>=1){
    CATCH=CATCH[-id_benthos,]
  }
  names(CATCH)
  
  # Corrige especes synonymes/manquantes
  if (!is.null(spCor)){
    dim(CATCH)
    ccor=CATCH[CATCH$Code_Espece_Campagne=='',]
    ccor=merge(ccor,spCor,by.y='spns',by.x='Nom_scientifique')
    ccor$Code_Espece_Campagne=ccor$spcode
    ccor=ccor[,names(ccor)!='spcode']
    CATCH=rbind(CATCH[CATCH$Code_Espece_Campagne!='',],ccor)
    dim(CATCH)
    if (dim(ccor)[1]>0){
      cat('Les codes especes synonymes ou manquantes suivantes ont ete corrigees selon spCor :',
          as.character(unique(ccor$Nom_scientifique)),'\n')
    }
  }  
  
  
  # 1) selection des variables dans la table CATCH
  SubSamples4EchoR=CATCH[,c(5,6,9,12,14,15,18,24,32,34,35,36,37)]
  
  # 2) correction de la variable "Code_Espece_Campagne" 3
  # Ajout - entre la 4 et la 5eme lettre
  SubSamples4EchoR$Code_Espece_Campagne=paste(substring(SubSamples4EchoR$Code_Espece_Campagne,1,4),substring(SubSamples4EchoR$Code_Espece_Campagne,5,8),sep="-")
  
  # 3) Correction de la variable "Class_Tri" nom original mais possible d'etre modifie variable 7
  SubSamples4EchoR$Class_Tri2=substring(SubSamples4EchoR[,7],1,1)
  SubSamples4EchoR$V_HV=as.character(SubSamples4EchoR$V_HV)
  SubSamples4EchoR[SubSamples4EchoR$V_HV=="Vrac","V_HV"]=""
  SubSamples4EchoR[SubSamples4EchoR$V_HV=="Hors Vrac","V_HV"]="H"
  SubSamples4EchoR[is.na(SubSamples4EchoR$Class_Tri2),"Class_Tri2"]=""
  SubSamples4EchoR$Class_Tri3=paste(SubSamples4EchoR$V_HV,SubSamples4EchoR$Class_Tri2,sep="")
  SubSamples4EchoR[SubSamples4EchoR$Class_Tri3=="","Class_Tri3"]="0"
  SubSamples4EchoR$Class_Tri3=as.factor(SubSamples4EchoR$Class_Tri3)
  SubSamples4EchoR$Class_Tri=SubSamples4EchoR$Class_Tri3
  
  #suppression des variables temporaires
  SubSamples4EchoR=SubSamples4EchoR[,!colnames(SubSamples4EchoR)%in%c("Class_Tri2","Class_Tri3")]
  
  # 4) Correction de la variable "Sexe" nom original mais possible d'etre modifie variable 8
  SubSamples4EchoR$Sexe=as.character(SubSamples4EchoR[,8])
  SubSamples4EchoR[is.na(SubSamples4EchoR$Sexe),"Sexe"]="N"
  SubSamples4EchoR[SubSamples4EchoR$Sexe=="UNK - Indetermine","Sexe"]="I"
  SubSamples4EchoR[SubSamples4EchoR$Sexe=="Femelle","Sexe"]="F"
  SubSamples4EchoR[SubSamples4EchoR$Sexe=="Male","Sexe"]="M"
  SubSamples4EchoR$Sexe=as.factor(SubSamples4EchoR$Sexe)
  
  
  # 5) Correction de la variable "Ech_V_HV" Ech_V_HV=poids echantillonne par sous-categorie d'espece
  # s'il n'y a pas de sous-categorie (Class_tri=0 et Sexe= N) le poids echantillonne renseigne est le bon, ne rien modifier
  temp=SubSamples4EchoR
  temp=data.frame(temp,Annee=CATCH$Annee,Serie=CATCH$Serie,Tot_Class_Tri=CATCH[,20],Ech_Class_Tri=CATCH[,21],Tot_Sexe=CATCH[,26],Ech_Sexe=CATCH[,27])
  #colnames(temp)[16:21]=c("Annee","Serie","Tot_Class_Tri","Ech_Class_Tri","Tot_Sexe","Ech_Sexe")
  temp$key=paste(temp$Annee,temp$Serie,temp$Code_station,temp$Id_Operation,temp$Code_Espece_Campagne,temp$V_HV,temp$Class_Tri,temp$Sexe,sep="_")
  
  #faire la somme des poids sous-taille par sous categorie
  id_pds_s_taille=which(!is.na(temp$Poids_Classe_Taille))
  #if (length(id_pds_s_taille)==0) stop('Poids des classes de taille manquant')
  if (length(id_pds_s_taille)>0){
    pds_s_taille=temp[id_pds_s_taille,]
    pds_s_taille$key=paste(pds_s_taille$Annee,pds_s_taille$Serie,pds_s_taille$Code_station,pds_s_taille$Id_Operation,
                           pds_s_taille$Code_Espece_Campagne,pds_s_taille$V_HV,pds_s_taille$Class_Tri,pds_s_taille$Sexe,
                           sep="_")
    temp2=aggregate(pds_s_taille$Poids_Classe_Taille,list(pds_s_taille$key),"sum")
    colnames(temp2)=c("key","Somme_poids_ech")
    
    #affecte la somme des poids si le plus bas niveau est renseigne
    temp=merge(temp,temp2,by="key",all.x=T)
  }else{
    temp$Somme_poids_ech=NA
  }
  
  # affecte les poids tries echantillonnes
  for (i in 1:dim(temp)[1]){
    
    # Cas VRAC
    #1er cas : Sous categorie tri mais pas de sexe
    if((temp[i,"Class_Tri"]!="0") & (temp[i,"Class_Tri"]!="H") & (temp[i,"Sexe"]=="N")){
      if(is.na(temp[i,"Poids_Classe_Taille"])){
        if(is.na(temp[i,"Ech_Class_Tri"])){
          temp[i,"Ech_V_HV"]=temp[i,"Tot_Class_Tri"]
        }else{
          temp[i,"Ech_V_HV"]=temp[i,"Ech_Class_Tri"]
        }
      }else{
        temp[i,"Ech_V_HV"]=temp[i,"Somme_poids_ech"]
      }
    } 
    
    #2eme cas : Pas de sous categorie tri ou Hors-Vrac mais Sous categorie sexe
    if(((temp[i,"Class_Tri"]=="0") | (temp[i,"Class_Tri"]=="H")) & (temp[i,"Sexe"]!="N")){
      if(is.na(temp[i,"Poids_Classe_Taille"])){
        if(is.na(temp[i,"Ech_Sexe"])){
          temp[i,"Ech_V_HV"]=temp[i,"Tot_Sexe"]
        }else{
          temp[i,"Ech_V_HV"]=temp[i,"Ech_Sexe"]
        }
      }else{
        temp[i,"Ech_V_HV"]=temp[i,"Somme_poids_ech"]
      }
    }   
    
    #3eme cas : Sous categorie tri et Sous categorie sexe
    if((temp[i,"Class_Tri"]!="0") & (temp[i,"Class_Tri"]!="H") & (temp[i,"Sexe"]!="N")){
      if(is.na(temp[i,"Poids_Classe_Taille"])){
        if(is.na(temp[i,"Ech_Sexe"])){
          if(is.na(temp[i,"Tot_Sexe"])){
            if(is.na(temp[i,"Ech_Class_Tri"])){
              temp[i,"Ech_V_HV"]=temp[i,"Tot_Class_Tri"]
            }else{
              temp[i,"Ech_V_HV"]=temp[i,"Ech_Class_Tri"]
            }
          }else{
            temp[i,"Ech_V_HV"]=temp[i,"Tot_Sexe"]
          }
        }else{
          temp[i,"Ech_V_HV"]=temp[i,"Ech_Sexe"]  
        }
      }else{
        temp[i,"Ech_V_HV"]=temp[i,"Somme_poids_ech"]  
      }   
    }
    
    #4eme cas : Pas de Sous categorie tri ou Hors-Vrac et pas de Sous categorie sexe
    if(((temp[i,"Class_Tri"]=="0") | (temp[i,"Class_Tri"]=="H")) & (temp[i,"Sexe"]=="N")){
      if(is.na(temp[i,"Ech_V_HV"])){
        temp[i,"Ech_V_HV"]=temp[i,"Tot_V_HV"] 
      }else{
        temp[i,"Ech_V_HV"]=temp[i,"Ech_V_HV"]  
      }   
    }
    
  }
  
  
  # suppresssion des variables temporaires
  temp=temp[,!colnames(temp)%in%c("key","V_HV","Annee","Serie","Tot_V_HV","Tot_Class_Tri","Ech_Class_Tri","Tot_Sexe","Ech_Sexe","Somme_poids_ech")]
  SubSamples4EchoR=temp
  
  
  # 6) Creation de la variable NumberSampled
  #creation d'une cle
  SubSamples4EchoR$key=paste(SubSamples4EchoR$Code_station, SubSamples4EchoR$Id_Operation, SubSamples4EchoR$Code_Espece_Campagne,SubSamples4EchoR$Class_Tri, SubSamples4EchoR$Sexe,sep="_")
  res=aggregate(SubSamples4EchoR$Nbr,list(SubSamples4EchoR$key),sum)
  colnames(res)=c("key","nb_ech")
  SubSamples4EchoR=merge(SubSamples4EchoR,res,by="key",all.x=T)
  
  
  # 7) Correction Poids_Classe_Taille
  SubSamples4EchoR[is.na(SubSamples4EchoR$Poids_Classe_Taille),"Poids_Classe_Taille"]=""
  
  
  # 8) Correction Unite_Taille
  SubSamples4EchoR$Unite_Taille=as.character(SubSamples4EchoR$Unite_Taille)
  SubSamples4EchoR[SubSamples4EchoR$Unite_Taille=="cm","Unite_Taille"]="1"
  SubSamples4EchoR[SubSamples4EchoR$Unite_Taille=="mm","Unite_Taille"]="0"
  SubSamples4EchoR$Unite_Taille=as.factor(SubSamples4EchoR$Unite_Taille)
  
  
  # 9) Correction Precision_Mesure
  SubSamples4EchoR[is.na(SubSamples4EchoR$Precision_Mesure),"Precision_Mesure"]=""
  SubSamples4EchoR[SubSamples4EchoR$Precision_Mesure==0.5,"Precision_Mesure"]=5
  SubSamples4EchoR[SubSamples4EchoR$Precision_Mesure==1.0,"Precision_Mesure"]=1
  SubSamples4EchoR$Precision_Mesure=as.factor(SubSamples4EchoR$Precision_Mesure)
  
  
  # 10) mise au propre du fichier et changement des noms de colonnes pour coller au format EchoR
  #suppression des variables inutiles
  SubSamples4EchoR=SubSamples4EchoR[,!colnames(SubSamples4EchoR)%in%c("key")]
  #mise en l'ordre des variables
  SubSamples4EchoR=SubSamples4EchoR[,c("Code_station","Id_Operation","Code_Espece_Campagne","Class_Tri","Sexe","Ech_V_HV","nb_ech","Taille","Nbr","Poids_Classe_Taille","Unite_Taille","Precision_Mesure")]
  #renommer les variables
  colnames(SubSamples4EchoR)=c("operationID","subHaul","baracoudacode","sizeCategory","sexCategory","sampleWeight","numberSampled","lengthClass","numberAtLength","weightAtLength","units","round")
  SubSamples4EchoR$baracoudacode=gsub('--','-',SubSamples4EchoR$baracoudacode)
  # ordonner par espece, trait et taille
  SubSamples4EchoR=SubSamples4EchoR[order(SubSamples4EchoR$operationID,SubSamples4EchoR$baracoudacode,
                                          SubSamples4EchoR$sizeCategory,SubSamples4EchoR$lengthClass),]
  SubSamples4EchoR$vessel=unique(OPERATION$Navire)
  
  # 11) Verification : le fichier final doit contenir
  dim(SubSamples4EchoR)
  # n lignes et 12 colonnes
  head(SubSamples4EchoR)
  summary(SubSamples4EchoR)

  # Code pour regarder un trait en particulier
  #   id=which(SubSamples4EchoR$operationID=="A001")
  #   SubSamples4EchoR[id,]  
  
  
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  #                                                                                                                     #
  #                         ETAPE 3 : Construction du fichier TotalSamples4EchoR.csv                                   #
  #                                                                                                                     #
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  
  
  # 1) Donnees dans CATCH
  temp2=CATCH[,c(5,9,10,12,18,14,20,26)] #Info : ici on repart de la table CATCH avec les lignes de Benthos supprimees
  colnames(temp2)=c("Code_station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Class_Tri","Tot_V_HV","Tot_Class_Tri","Tot_Sexe")
  
  temp2$Code_Espece_Campagne=paste(substring(temp2$Code_Espece_Campagne,1,4),substring(temp2$Code_Espece_Campagne,5,8),sep="-") 
  temp2$Class_Tri2=substring(temp2$Class_Tri,1,1)
  temp2$V_HV=as.character(temp2$V_HV)
  temp2[temp2$V_HV=="Vrac","V_HV"]=""
  temp2[temp2$V_HV=="Hors Vrac","V_HV"]="H"
  temp2[is.na(temp2$Class_Tri2),"Class_Tri2"]=""
  temp2$Class_Tri3=paste(temp2$V_HV,temp2$Class_Tri2,sep="")
  temp2[temp2$Class_Tri3=="","Class_Tri3"]="0"
  temp2$Class_Tri3=as.factor(temp2$Class_Tri3)
  temp2$Class_Tri=temp2$Class_Tri3
  
  # suppression des variables temporaires
  temp2=temp2[,!colnames(temp2)%in%c("Class_Tri2","Class_Tri3")]
  temp2=unique(temp2)
  
  # 2) remplir la colonne Tot_Class_Tri
  for (i in 1:dim(temp2)[1]){
    
    #1er cas : Sous categorie tri mais pas de sexe
    if(is.na(temp2[i,"Tot_Class_Tri"])) {
      if(is.na(temp2[i,"Tot_Sexe"])){
        temp2[i,"Tot_Class_Tri"]=temp2[i,"Tot_V_HV"]
      }else{
        temp2[i,"Tot_Class_Tri"]=temp2[i,"Tot_Sexe"]
      }  
    }
  }
  
  # 3) remplir la colonne Tot_V_HV
  for (i in 1:dim(temp2)[1]){
    
    #1er cas : Sous categorie tri mais pas de sexe
    if(is.na(temp2[i,"Tot_V_HV"])) {
      if(is.na(temp2[i,"Tot_Sexe"])){
        temp2[i,"Tot_V_HV"]=temp2[i,"Tot_Class_Tri"]
      }else{
        temp2[i,"Tot_V_HV"]=temp2[i,"Tot_Sexe"]
      }  
    }
  }
  
  
  sauv_somme_pds_trie=temp2
  
  # 3) Somme des poids tries par cle unique
  temp2$key=paste(temp2$Code_station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,temp2$Class_Tri,sep="_")
  temp2$key2=paste(temp2$Code_station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,sep="_")
  
  #cas d'un sous-?chantillon de poids tri? (?l?ver le poids)
  for (i in unique(temp2$key2)){
    
    if( sum(temp2[temp2$key2==i,"Tot_Class_Tri"]) < unique(temp2[temp2$key2==i,"Tot_V_HV"]) ) {
      temp2[temp2$key2==i,"Tot_Class_Tri"]=temp2[temp2$key2==i,"Tot_Class_Tri"]*unique(temp2[temp2$key2==i,"Tot_V_HV"])/sum(temp2[temp2$key2==i,"Tot_Class_Tri"])
    }
  }
  
  Somme_pds_trie=aggregate(temp2$Tot_Class_Tri,list(temp2$key),sum)
  colnames(Somme_pds_trie)=c("key","SommePds")
  temp2=merge(temp2,Somme_pds_trie,by="key",all.x=T)
  
  
  # 4) suppression des variables temporaires
  temp2=temp2[,!colnames(temp2)%in%c("key","Tot_V_HV","Tot_Class_Tri","Tot_Sexe")]
  colnames(temp2)[5]="Tot_Class_Tri"
  temp2=unique(temp2)
  
  # 5) Verifie la somme HV = au nombre dans la table Operation idem pour le vrac
  OP=OPERATION[,c("Code_Station","Poids_Total","Poids_Total_Vrac","Poids_Total_Espece_Vrac_Trie","Poids_Total_HorsVrac","Poids_Total_Espece_Vrac","Poids_Total_Benthos_Vrac","Poids_Total_Espece_HorsVrac","Poids_Total_Benthos_HorsVrac","Poids_Total_Espece_Inerte_Trie","Poids_Total_Espece_Vivant_non_detaille_trie")]
  #cas des valeurs manquantes notees a  -9, a  remplacer par 0
  OP[OP$Poids_Total==-9,"Poids_Total"]=0
  OP[OP$Poids_Total_Vrac==-9,"Poids_Total_Vrac"]=0
  OP[OP$Poids_Total_Espece_Vrac_Trie==-9,"Poids_Total_Espece_Vrac_Trie"]=0
  OP[OP$Poids_Total_HorsVrac==-9,"Poids_Total_HorsVrac"]=0
  OP[OP$Poids_Total_Espece_Vrac==-9,"Poids_Total_Espece_Vrac"]=0
  OP[OP$Poids_Total_Benthos_Vrac==-9,"Poids_Total_Benthos_Vrac"]=0
  OP[OP$Poids_Total_Espece_HorsVrac==-9,"Poids_Total_Espece_HorsVrac"]=0
  OP[OP$Poids_Total_Benthos_HorsVrac==-9,"Poids_Total_Benthos_HorsVrac"]=0
  OP[OP$Poids_Total_Espece_Inerte_Trie==-9,"Poids_Total_Espece_Inerte_Trie"]=0
  OP[OP$Poids_Total_Espece_Vivant_non_detaille_trie==-9,"Poids_Total_Espece_Vivant_non_detaille_trie"]=0
  OP$Poids_Total=round(OP$Poids_Total,3)
  OP$Poids_Total_Vrac=round(OP$Poids_Total_Vrac,3)
  OP$Poids_Total_Espece_Vrac_Trie=round(OP$Poids_Total_Espece_Vrac_Trie,3)
  OP$Poids_Total_HorsVrac=round(OP$Poids_Total_HorsVrac,3)
  OP$Poids_Total_Espece_Vrac=round(OP$Poids_Total_Espece_Vrac,3)
  OP$Poids_Total_Benthos_Vrac=round(OP$Poids_Total_Benthos_Vrac,3)
  OP$Poids_Total_Espece_HorsVrac=round(OP$Poids_Total_Espece_HorsVrac,3)
  OP$Poids_Total_Benthos_HorsVrac=round(OP$Poids_Total_Benthos_HorsVrac,3)
  OP$Poids_Total_Espece_Inerte_Trie=round(OP$Poids_Total_Espece_Inerte_Trie,3)
  OP$Poids_Total_Espece_Vivant_non_detaille_trie=round(OP$Poids_Total_Espece_Vivant_non_detaille_trie,3)
  
  temp2[temp2$V_HV=="","V_HV"]="V"
  temp3=aggregate(temp2$SommePds,list(temp2$Code_station,temp2$V_HV),sum)
  colnames(temp3)=c("Code_Station","V_HV","Somme_poids_trie")
  temp3$Somme_poids_trie=round(temp3$Somme_poids_trie,3)
  #indicateur Somme_poids_trie correspond au poids total esp?ce echantillonne ne prend pas en compte les poids inerte trie et vivant non detaille trie 
  temp3_OP=merge(temp3,OP,by="Code_Station",all.x=T)
  temp3_OP[is.na(temp3_OP$Somme_poids_trie),"Somme_poids_trie"]=0
  temp3_OP[,"Total_vrac"]=temp3_OP[,"Poids_Total_Espece_Vrac"]-temp3_OP[,"Poids_Total_Espece_Inerte_Trie"]-temp3_OP[,"Poids_Total_Espece_Vivant_non_detaille_trie"]
  temp3_OP$Total_vrac=round(temp3_OP$Total_vrac,3)
  temp3_OP[,"Poids_Total_Espece_Vrac_Trie"]=temp3_OP[,"Poids_Total_Espece_Vrac_Trie"]-temp3_OP[,"Poids_Total_Espece_Inerte_Trie"]-temp3_OP[,"Poids_Total_Espece_Vivant_non_detaille_trie"]
  temp3_OP$Poids_Total_Espece_Vrac_Trie=round(temp3_OP$Poids_Total_Espece_Vrac_Trie,3)
  
  for (i in 1:nrow(temp3_OP)){
    if(temp3_OP[i,"V_HV"]=="H"){
      if(temp3_OP[i,"Somme_poids_trie"]==temp3_OP[i,"Poids_Total_Espece_HorsVrac"]){ # ne pas comparer au Pds total Hors-Vrac ? causes des cas de Benthos,  Pds tot HV = Pds tot esp HV + Pds tot benthos HV
        temp3_OP[i,"Valide_HorsVrac"]="TRUE"
      }else{
        temp3_OP[i,"Valide_HorsVrac"]="FALSE"
      }
    }else{
      if(temp3_OP[i,"Somme_poids_trie"]==temp3_OP[i,"Poids_Total_Espece_Vrac_Trie"]){ # Pds tot V = Pds tot esp V + Pds tot benthos V, Pds tot esp V = Pds esp V + Pds inerte trie + Pds vivant non detaille trie 
        temp3_OP[i,"Valide_Vrac"]="TRUE"
      }else{
        temp3_OP[i,"Valide_Vrac"]="FALSE"
      }
    }
  }
  
  
  # visualise les lignes ? FALSE pour le VRAC
  temp3_OP[which(temp3_OP$Valide_Vrac=="FALSE"),]
  
  # visualise les lignes ? FALSE pour le HORS - VRAC
  temp3_OP[which(temp3_OP$Valide_HorsVrac=="FALSE"),]
  
  
  
  #----------------------------------------
  # ELEVATIONS
  #----------------------------------------  
  
  # 6) Si pas d'erreur "FALSE", on peut continuer et elever les poids.
  # variable Poids eleve
  temp2=merge(temp2,OP,by.x="Code_station",by.y="Code_Station",all.x=T)
  
  for (i in 1:nrow(temp2)){
    if(temp2[i,"V_HV"]=="H"){
      temp2[i,"Poids_eleve"]=temp2[i,"SommePds"]*( (temp2[i,"Poids_Total"]-(temp2[i,"Poids_Total"]-temp2[i,"Poids_Total_HorsVrac"]))/((temp2[i,"Poids_Total_Vrac"]+temp2[i,"Poids_Total_HorsVrac"])-temp2[i,"Poids_Total_Vrac"])) #Poids total echantillonne HV * ( (Pds total - (total -Pds total Vrac)/(Pds total trie - Pds total trie Vrac) )
    }else{
      temp2[i,"Poids_eleve"]=temp2[i,"SommePds"]*(temp2[i,"Poids_Total_Vrac"]/ temp2[i,"Poids_Total_Espece_Vrac_Trie"]) *( (temp2[i,"Poids_Total"]-temp2[i,"Poids_Total_HorsVrac"]) / temp2[i,"Poids_Total_Vrac"])  #Poids total echantillonne V * ( (Pds total - Pds total HVrac)/Pds total trie HVrac) )
    }
  }
  
  
  # 7) Elevation du nombre d'individus
  # 7.1.1) constitution d'une table correspondant a  la vu de tutti avec elevation des poids pour faciliter les calculs et la comprehension
  
  Vue_Tutti=CATCH[,c(5,9,10,12,14,15,18,20,21,24,26,27,34,37)]
  colnames(Vue_Tutti)=c("Code_station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Tot_V_HV","Ech_V_HV","Class_Tri","Tot_Class_Tri","Ech_Class_Tri","Sexe","Tot_Sexe","Ech_Sexe","Poids_Classe_Taille","Nbr")
  Vue_Tutti$Code_Espece_Campagne=paste(substring(Vue_Tutti$Code_Espece_Campagne,1,4),substring(Vue_Tutti$Code_Espece_Campagne,5,8),sep="-") 
  Vue_Tutti$Class_Tri2=substring(Vue_Tutti$Class_Tri,1,1)
  Vue_Tutti$V_HV=as.character(Vue_Tutti$V_HV)
  Vue_Tutti[Vue_Tutti$V_HV=="Vrac","V_HV"]=""
  Vue_Tutti[Vue_Tutti$V_HV=="Hors Vrac","V_HV"]="H"
  Vue_Tutti[is.na(Vue_Tutti$Class_Tri2),"Class_Tri2"]=""
  Vue_Tutti$Class_Tri3=paste(Vue_Tutti$V_HV,Vue_Tutti$Class_Tri2,sep="")
  Vue_Tutti[Vue_Tutti$Class_Tri3=="","Class_Tri3"]="0"
  Vue_Tutti$Class_Tri3=as.factor(Vue_Tutti$Class_Tri3)
  Vue_Tutti$Class_Tri=Vue_Tutti$Class_Tri3
  
  Vue_Tutti$Sexe=as.character(Vue_Tutti$Sexe)
  Vue_Tutti[is.na(Vue_Tutti$Sexe),"Sexe"]="N"
  Vue_Tutti[Vue_Tutti$Sexe=="UNK - Indetermine","Sexe"]="I"
  Vue_Tutti[Vue_Tutti$Sexe=="Femelle","Sexe"]="F"
  Vue_Tutti[Vue_Tutti$Sexe=="Male","Sexe"]="M"
  Vue_Tutti$Sexe=as.factor(Vue_Tutti$Sexe)
  
  
  # 7.1.2) suppression des variables temporaires
  Vue_Tutti=Vue_Tutti[,!colnames(Vue_Tutti)%in%c("Class_Tri2","Class_Tri3")]
  
  # 7.1.3) Faire l'elevation des nombres
  Vue_Tutti$key=paste(Vue_Tutti$Code_station, Vue_Tutti$Code_Espece_Campagne,Vue_Tutti$Nom_Scientifique, Vue_Tutti$Class_Tri, Vue_Tutti$Sexe,sep="_")
  res=aggregate(Vue_Tutti$Nbr,list(Vue_Tutti$key),sum)
  colnames(res)=c("key","nb_ech")
  Vue_Tutti=merge(Vue_Tutti,res,by="key",all.x=T)
  
  # 7.1.4) Faire l'elevation des poids echantillonne
  #faire la somme des poids sous-taille par sous categorie
  id_pds_s_taille=which(!is.na(Vue_Tutti$Poids_Classe_Taille))
  if (length(id_pds_s_taille)>0){
    pds_s_taille=Vue_Tutti[id_pds_s_taille,]
    pds_s_taille$key=paste(pds_s_taille$Code_station,pds_s_taille$Code_Espece_Campagne,pds_s_taille$Nom_Scientifique,pds_s_taille$Class_Tri,pds_s_taille$Sexe,sep="_")
    tempo=aggregate(pds_s_taille$Poids_Classe_Taille,list(pds_s_taille$key),"sum")
    colnames(tempo)=c("key","Somme_poids_ech")
    #affecte la somme des poids si le plus bas niveau est renseigne
    Vue_Tutti=merge(Vue_Tutti,tempo,by="key",all.x=T)
  }else{
    Vue_Tutti$Somme_poids_ech=NA
  }
  
  #supprime la variable nbr
  Vue_Tutti=Vue_Tutti[,!colnames(Vue_Tutti)%in%c("Nbr","Poids_Classe_Taille")]
  Vue_Tutti=unique(Vue_Tutti)
  
  # 7.1.5) Creation de la colonne total Poids Vrac/Hors Vrac echantillonne
  #reprendre le fichier temp2 precedent et calculer le poids total V/HV
  temp2$key2=paste(temp2$Code_station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,temp2$V_HV,sep="_")
  Pds_tot_V_HV=aggregate(temp2$SommePds,list(temp2$key2),sum)
  colnames(Pds_tot_V_HV)=c("key2","Pds_tot_ech_V_HV")
  
  Vue_Tutti[Vue_Tutti$V_HV=="","V_HV"]="V"
  Vue_Tutti$key2=paste(Vue_Tutti$Code_station,Vue_Tutti$Code_Espece_Campagne,Vue_Tutti$Nom_Scientifique,Vue_Tutti$V_HV,sep="_")
  Vue_Tutti=merge(Vue_Tutti,Pds_tot_V_HV,by="key2",all.x=T)
  
  # 7.1.6) Creation de la colonne echantillonne total
  for (i in 1:dim(Vue_Tutti)[1]){
    
    if(is.na(Vue_Tutti[i,"Ech_V_HV"])) { # cas pas de sous echantillon V/HV
      if(is.na(Vue_Tutti[i,"Ech_Class_Tri"])){ # cas pas de sous echantillon par sous categorie G,P
        if(is.na(Vue_Tutti[i,"Ech_Sexe"])){ # cas pas de sous echantillon pas sous categorie sexe
          if(is.na(Vue_Tutti[i,"Somme_poids_ech"])){
            Vue_Tutti[i,"Ech_V_HV"]=0
          }else{
            Vue_Tutti[i,"Ech_V_HV"]=Vue_Tutti[i,"Somme_poids_ech"]
          }  
        }else{
          Vue_Tutti[i,"Ech_V_HV"]=Vue_Tutti[i,"Ech_Sexe"]
        }
      }else{
        Vue_Tutti[i,"Ech_V_HV"]=Vue_Tutti[i,"Ech_Class_Tri"]
      }
    }else{
      Vue_Tutti[i,"Ech_V_HV"]=Vue_Tutti[i,"Ech_V_HV"]
    } 
    
  }
  
  # 7.1.7) Mise en forme de la table pour qu'elle ressemble a  la vue Tutti
  Vue_Tutti_Finale=Vue_Tutti[,c("Code_station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Pds_tot_ech_V_HV","Class_Tri","Tot_Class_Tri","Sexe","Tot_Sexe","Ech_V_HV","nb_ech")]
  
  
  # 7.2) Creation de la variable somme des freres des tri
  Frere_tri=Vue_Tutti_Finale[,c("Code_station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Class_Tri","Tot_Class_Tri")]
  Frere_tri=unique(Frere_tri)
  Frere_tri$key=paste(Frere_tri$Code_station,Frere_tri$Code_Espece_Campagne,Frere_tri$Nom_Scientifique,Frere_tri$V_HV,sep="_")
  res_somme_frere_tri=aggregate(Frere_tri$Tot_Class_Tri,list(Frere_tri$key),sum)
  colnames(res_somme_frere_tri)=c("key","Somme_frere_tri")
  
  # 7.3) Creation de la variable somme des freres des sexe
  Frere_sexe=Vue_Tutti_Finale[,c("Code_station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Class_Tri","Sexe","Tot_Sexe")]
  Frere_sexe=unique(Frere_sexe)
  Frere_sexe$key=paste(Frere_sexe$Code_station,Frere_sexe$Code_Espece_Campagne,Frere_sexe$Nom_Scientifique,Frere_sexe$V_HV,Frere_sexe$Class_Tri,sep="_")
  res_somme_frere_sexe=aggregate(Frere_sexe$Tot_Sexe,list(Frere_sexe$key),sum)
  colnames(res_somme_frere_sexe)=c("key3","Somme_frere_sexe")
  
  # 7.4) ajout des deux varaibles ? la vue finale
  Vue_Tutti_Finale$key=paste(Vue_Tutti_Finale$Code_station,Vue_Tutti_Finale$Code_Espece_Campagne,Vue_Tutti_Finale$Nom_Scientifique,Vue_Tutti_Finale$V_HV,sep="_")
  Vue_Tutti_Finale$key3=paste(Vue_Tutti_Finale$Code_station,Vue_Tutti_Finale$Code_Espece_Campagne,Vue_Tutti_Finale$Nom_Scientifique,Vue_Tutti_Finale$V_HV,Vue_Tutti_Finale$Class_Tri,sep="_")
  
  Vue_Tutti_Finale=merge(Vue_Tutti_Finale,res_somme_frere_tri,by="key",all.x=T)
  Vue_Tutti_Finale=merge(Vue_Tutti_Finale,res_somme_frere_sexe,by="key3",all.x=T)
  
  Vue_Tutti_Finale=merge(Vue_Tutti_Finale,temp2[,c("key2","Poids_Total","Poids_Total_Vrac","Poids_Total_Espece_Vrac","Poids_Total_Espece_Vrac_Trie","Poids_Total_HorsVrac","Poids_Total_Espece_HorsVrac")],by.x="key",by.y="key2",all.x=T)
  Vue_Tutti_Finale=unique(Vue_Tutti_Finale)
  
  
  # 7.2) Creation de la variable nombre elevee
  
  # 7.2.1) formules
  
  for (i in 1:dim(Vue_Tutti_Finale)[1]){
    
    #debut des conditions
    #VRAC
    if(Vue_Tutti_Finale[i,"V_HV"]=="V"){
      
      if(!is.na(Vue_Tutti_Finale[i,"Ech_V_HV"]) & (Vue_Tutti_Finale[i,"Ech_V_HV"]==0.0)){
        if(is.na(Vue_Tutti_Finale[i,"Tot_Sexe"])){
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / ((Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"]))
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / ((Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"]))
          }
        }else{
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"]) *((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / ((Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"]))
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / ((Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"]))
          }
        }
      }else  if(!is.na(Vue_Tutti_Finale[i,"Ech_V_HV"]) & (Vue_Tutti_Finale[i,"Ech_V_HV"]!=0.0)){
        if(is.na(Vue_Tutti_Finale[i,"Tot_Sexe"])){
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]* (Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / ((Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"]))
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / ((Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"]))
          }
        }else{
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Sexe"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"]) *((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / ((Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"]))
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Sexe"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / ((Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Espece_HorsVrac"]))
          }
        }
      }else if (is.na(Vue_Tutti_Finale[i,"Ech_V_HV"])){
        Vue_Tutti_Finale[i,"nb_eleve"]=NA
      }
      
    }else{
      if(!is.na(Vue_Tutti_Finale[i,"Ech_V_HV"]) & (Vue_Tutti_Finale[i,"Ech_V_HV"]==0.0)){
        if(is.na(Vue_Tutti_Finale[i,"Tot_Sexe"])){
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / ((Vue_Tutti_Finale[i,"Poids_Total_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Vrac"]))
          }
        }else{
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])* ( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / ((Vue_Tutti_Finale[i,"Poids_Total_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Vrac"]))
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / ((Vue_Tutti_Finale[i,"Poids_Total_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Vrac"]))
          }
        }
      }else if (is.na(Vue_Tutti_Finale[i,"Ech_V_HV"])){
        Vue_Tutti_Finale[i,"nb_eleve"]=NA
      }else if(is.na(Vue_Tutti_Finale[i,"Tot_Sexe"])){
        if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
          Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / ((Vue_Tutti_Finale[i,"Poids_Total_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Vrac"]))
        }else{
          Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / ((Vue_Tutti_Finale[i,"Poids_Total_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Vrac"]))
        }
      }else if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
        Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Sexe"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / ((Vue_Tutti_Finale[i,"Poids_Total_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Vrac"]))
      }else{
        Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Sexe"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / ((Vue_Tutti_Finale[i,"Poids_Total_Vrac"]+Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])-Vue_Tutti_Finale[i,"Poids_Total_Vrac"]))
      }
      
    }#fin des conditions
  }#fin du for
  
  
  
  # 7.2.2) Mise au format de la variable
  Vue_Tutti_Finale$nb_eleve=round(Vue_Tutti_Finale$nb_eleve)
  
  # 7.3) Pour le fichier resultat rentrant dans Echo-R il ne faut pas de la sous-categorie Sexe
  Res_nb_eleve=aggregate(Vue_Tutti_Finale$nb_eleve,list(Vue_Tutti_Finale$key3),sum)
  colnames(Res_nb_eleve)=c("key3","Nb_eleve")
  
  
  # 7.4) Calcul de la moyenne ponderee des longueurs
  Long_moy=CATCH[,c(5,9,10,12,18,32,37)]
  colnames(Long_moy)=c("Code_station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Class_Tri","Taille","Nbr")
  
  Long_moy$Code_Espece_Campagne=paste(substring(Long_moy$Code_Espece_Campagne,1,4),substring(Long_moy$Code_Espece_Campagne,5,8),sep="-") 
  Long_moy$Class_Tri2=substring(Long_moy$Class_Tri,1,1)
  Long_moy$V_HV=as.character(Long_moy$V_HV)
  Long_moy[Long_moy$V_HV=="Vrac","V_HV"]=""
  Long_moy[Long_moy$V_HV=="Hors Vrac","V_HV"]="H"
  Long_moy[is.na(Long_moy$Class_Tri2),"Class_Tri2"]=""
  Long_moy$Class_Tri3=paste(Long_moy$V_HV,Long_moy$Class_Tri2,sep="")
  Long_moy[Long_moy$Class_Tri3=="","Class_Tri3"]="0"
  Long_moy$Class_Tri3=as.factor(Long_moy$Class_Tri3)
  Long_moy$Class_Tri=Long_moy$Class_Tri3
  
  # suppression des variables temporaires
  Long_moy=Long_moy[,!colnames(Long_moy)%in%c("Class_Tri2","Class_Tri3")]
  Long_moy=unique(Long_moy)
  
  # creation de la cle
  Long_moy$key=paste(Long_moy$Code_station,Long_moy$Code_Espece_Campagne,Long_moy$Nom_Scientifique,Long_moy$V_HV,Long_moy$Class_Tri,sep="_")
  Res_long_moy=Long_moy$key 
  Res_long_moy=data.frame(Res_long_moy)
  Res_long_moy$moy=rep(0,dim(Res_long_moy)[1])        
  Res_long_moy=unique(Res_long_moy)
  colnames(Res_long_moy)=c("key","moy")
  
  for(i in Long_moy$key){
    Data=Long_moy[which(Long_moy$key==i),]
    moy_ponderee=weighted.mean(Data[,"Taille"],Data[,"Nbr"],na.rm=T)*10 #pour mettre en mm
    Res_long_moy[which(Res_long_moy$key==i),"moy"]=moy_ponderee
  }                     
  
  
  # 7.5) Regroupement des differentes variables pour creation du fichier final
  #refaire une cle dans temp2
  temp2$key3=paste(temp2$Code_station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,temp2$V_HV,temp2$Tot_Class_Tri,sep="_")
  temp2[temp2$V_HV=="V","V_HV"]=""
  temp2$key=paste(temp2$Code_station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,temp2$V_HV,temp2$Tot_Class_Tri,sep="_")
  
  
  #poids echantillonne + poids eleve
  TotSamples4EchoR=temp2[,c(1,2,5,7,18,19,20)]
  
  #nombre eleve
  TotSamples4EchoR=merge(TotSamples4EchoR,Res_nb_eleve,by="key3",all.x=T)
  
  #moyenne des longueurs
  TotSamples4EchoR=merge(TotSamples4EchoR,Res_long_moy,by="key",all.x=T)
  
  # moyenne des poids
  TotSamples4EchoR$meanWeight=(TotSamples4EchoR$Poids_eleve/TotSamples4EchoR$Nb_eleve)
  # passer de kg a  g
  TotSamples4EchoR$meanWeight=TotSamples4EchoR$meanWeight*1000
  
  #nombre par kg
  TotSamples4EchoR$noPerKg=(TotSamples4EchoR$Nb_eleve/TotSamples4EchoR$Poids_eleve)
  
  
  #modifie sommepoids avec le vrai poids sous-?chantillonn? tri?
  sauv_somme_pds_trie$key=paste(sauv_somme_pds_trie$Code_station,sauv_somme_pds_trie$Code_Espece_Campagne,sauv_somme_pds_trie$Nom_Scientifique,sauv_somme_pds_trie$V_HV,sauv_somme_pds_trie$Class_Tri,sep="_")
  TotSamples4EchoR=merge(TotSamples4EchoR,sauv_somme_pds_trie[,c("key","Tot_Class_Tri")],by="key",all.x=T)
  
  
  # 7.6) Mise en forme du fichier resultat
  TotSamples4EchoR=TotSamples4EchoR[,c(3:5,7:12)]
  #TotSamples4EchoR=TotSamples4EchoR[,c("Code_station","Code_Espece_Campagne","Tot_Class_Tri","Poids_eleve","Nb_eleve","moy","meanWeight","noPerKg","SommePds")]
  colnames(TotSamples4EchoR)=c("operationID","baracoudacode","sizeCategory","sampleWeight","numberSampled","meanLength","meanWeight","noPerKg","sortedWeight")
  
  # arrondir a  trois decimales
  TotSamples4EchoR$sampleWeight=round(TotSamples4EchoR$sampleWeight,3)
  TotSamples4EchoR$meanLength=round(TotSamples4EchoR$meanLength,3)
  TotSamples4EchoR$meanWeight=round(TotSamples4EchoR$meanWeight,3)
  TotSamples4EchoR$noPerKg=round(TotSamples4EchoR$noPerKg,3)
  TotSamples4EchoR$sortedWeight=round(TotSamples4EchoR$sortedWeight,3)
  TotSamples4EchoR$baracoudacode=gsub('--','-',TotSamples4EchoR$baracoudacode)
  TotSamples4EchoR$vessel=unique(OPERATION$Navire)

  return(list(SubSamples4EchoR,TotSamples4EchoR))
  
}  


Tutti2EchoR_Part12016=function(CATCH,OPERATION,SAMPLECATEGORY){
  
  #  OBJET ........: Script permettant la transformation du format generique issu de TUTTI au format EchoR              #
  # ------------------------------------------------------------------------------------------------------------------- #
  #
  #  INSTITUT........:  IFREMER Nantes/EMH
  #  DATE ...........:  26 f?vrier 2016 /6 mai 2016
  #  CREE PAR .......:  Cornou Anne-Sophie  
  # ------------------------------------------------------------------------------------------------------------------- #
  
  
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  #                                                                                                                     #
  #                         ETAPE 1 : Validation du format generique                                                    #
  #                                                                                                                     #
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  
  
  # 1) Table CATCH
  # n lignes 51 colonnes avec deux sous-categories, Class_tri et Sexe
  
  # Suppression des sous_categorie >2, ici conserve que les deux premieres
  if(dim(CATCH)[2]>=52){
    Borne_max=max(SAMPLECATEGORY$order)
    CATCH=CATCH[,c(1:32,(dim(CATCH)[2]-18):dim(CATCH)[2]-(Borne_max*2))]
  }
  
  
  # 2) Detection des cas de Benthos Saisie et des especes saisies sans Nom de campagne
  Resume_CATCH=CATCH[,c("Code_Station","Id_Operation","Code_Taxon","Code_Espece_Campagne","Nom_Scientifique","V_HV","Num_Ordre_V_HV_H2")]
  
  for (i in 1:dim(Resume_CATCH)[1]){
    if(Resume_CATCH[i,"Num_Ordre_V_HV_H2"]>=100){
      print("Attention, saisie de Benthos dans le trait")
      print(Resume_CATCH[i,"Code_Station"])
      print("Espece")
      print(Resume_CATCH[i,"Nom_Scientifique"])
    }
    
    
    if(Resume_CATCH[i,"Code_Espece_Campagne"]==""){
      print("Attention, une espece sans nom de campagne a ete detectee dans le trait : ")
      print(as.character(Resume_CATCH[i,"Code_Station"]))
      print("Espece")
      print(as.character(Resume_CATCH[i,"Nom_Scientifique"]))
    }
  }  
  
  # 3) Detection des cas de "Poids Inerte tries" et des "Poids vivants non detailles tries"
  
  for (i in 1:dim(OPERATION)[1]){
    if(OPERATION[i,"Poids_Total_Espece_Inerte_Trie"]!=0 & (OPERATION[i,"Poids_Total_Espece_Inerte_Trie"]!=-9)){
      print("Attention, saisie d'un poids inerte trie dans le trait")
      print(OPERATION[i,"Code_Station"])
    }
    if(OPERATION[i,"Poids_Total_Espece_Vivant_non_detaille_trie"]!=-9 & (OPERATION[i,"Poids_Total_Espece_Vivant_non_detaille_trie"]!=0)){
      print("Attention, saisie d'un poids d'espece vivant non detaille trie dans le trait : ")
      print(OPERATION[i,"Code_Station"])
    }
  }
  
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  #                                                                                                                     #
  #                         ETAPE 2 : Construction du fichier SubSamples4EchoR.csv                                      #
  #                                                                                                                     #
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  
  # 1) Suppression des lignes concernant le Benthos
  id_benthos=which(CATCH[,"Benthos"]=="Y")
  if(length(id_benthos)>0 & id_benthos[1]>=1){
    CATCH=CATCH[-id_benthos,]
  } 
  
  # 2) selection des variables dans la table CATCH
  SubSamples4EchoR=CATCH[,c(4,5,8,12,14,15,19,26,35,37,38,39,40,42)]
  
  # 3) Correction de la variable "Class_Tri" nom original mais possible d'etre modifie variable 7
  SubSamples4EchoR$Class_Tri2=substring(SubSamples4EchoR[,7],1,1)
  SubSamples4EchoR$V_HV=as.character(SubSamples4EchoR$V_HV)
  SubSamples4EchoR[SubSamples4EchoR$V_HV=="Vrac","V_HV"]=""
  SubSamples4EchoR[SubSamples4EchoR$V_HV=="Hors Vrac","V_HV"]="H"
  SubSamples4EchoR[is.na(SubSamples4EchoR$Class_Tri2),"Class_Tri2"]=""
  SubSamples4EchoR$Class_Tri3=paste(SubSamples4EchoR$V_HV,SubSamples4EchoR$Class_Tri2,sep="")
  SubSamples4EchoR[SubSamples4EchoR$Class_Tri3=="","Class_Tri3"]="0"
  SubSamples4EchoR$Class_Tri3=as.factor(SubSamples4EchoR$Class_Tri3)
  SubSamples4EchoR$Class_Tri=SubSamples4EchoR$Class_Tri3
  
  #suppression des variables temporaires
  SubSamples4EchoR=SubSamples4EchoR[,!colnames(SubSamples4EchoR)%in%c("Class_Tri2","Class_Tri3")]
  
  # 4) Correction de la variable "Sexe" nom original mais possible d'etre modifie variable 8
  SubSamples4EchoR$Sexe=as.character(SubSamples4EchoR[,8])
  SubSamples4EchoR[is.na(SubSamples4EchoR$Sexe),"Sexe"]="N"
  SubSamples4EchoR[SubSamples4EchoR$Sexe=="UNK - Indetermine","Sexe"]="I"
  SubSamples4EchoR[SubSamples4EchoR$Sexe=="Femelle","Sexe"]="F"
  SubSamples4EchoR[SubSamples4EchoR$Sexe=="Male","Sexe"]="M"
  SubSamples4EchoR[SubSamples4EchoR$Sexe=="Non sexe","Sexe"]="I"
  SubSamples4EchoR$Sexe=as.factor(SubSamples4EchoR$Sexe)
  
  
  # 5) Cr?ation du poids ?chantillonn? de plus bas niveau (Poids_reference)
  temp=SubSamples4EchoR
  temp=data.frame(temp,Annee=CATCH$Annee,Serie=CATCH$Serie,Tot_Class_Tri=CATCH[,20],Ech_Class_Tri=CATCH[,21],Tot_Sexe=CATCH[,26],Ech_Sexe=CATCH[,27])
  temp$key=paste(temp$Annee,temp$Serie,temp$Code_Station,temp$Id_Operation,temp$Code_Espece_Campagne,temp$V_HV,temp$Class_Tri,temp$Sexe,sep="_")
  
  
  #faire la somme des poids sous-taille par sous categorie
  id_pds_s_taille=which(!is.na(temp$Poids_Classe_Taille))
  if (length(id_pds_s_taille)>0){
    pds_s_taille=temp[id_pds_s_taille,]
    pds_s_taille$key=paste(pds_s_taille$Annee,pds_s_taille$Serie,pds_s_taille$Code_Station,pds_s_taille$Id_Operation,
                           pds_s_taille$Code_Espece_Campagne,pds_s_taille$V_HV,pds_s_taille$Class_Tri,pds_s_taille$Sexe,
                           sep="_")
    temp2=aggregate(pds_s_taille$Poids_Classe_Taille,list(pds_s_taille$key),"sum")
    colnames(temp2)=c("key","Somme_poids_ech")
    
    #affecte la somme des poids si le plus bas niveau est renseigne
    temp=merge(temp,temp2,by="key",all.x=T)
  }else{
    temp$Somme_poids_ech=NA
  }
  
  # colonne poids_reference va devenir notre sampleweight, le poids de plus bas niveau ?chantillonn?
  for (i in 1:dim(temp)[1]){
    #si pds_classe_taille != NA alors pds_ref=somme(pds_classe_taille)
    if(!(is.na(temp[i,"Poids_Classe_Taille"]))){
      temp[i,"Poids_Reference"]=temp[i,"Somme_poids_ech"]
    }     
  }
  
  
  # suppresssion des variables temporaires
  temp=temp[,!colnames(temp)%in%c("key","V_HV","Annee","Serie","Tot_V_HV","Tot_Class_Tri","Ech_Class_Tri","Tot_Sexe","Ech_Sexe","Somme_poids_ech","Ech_V_HV")]
  SubSamples4EchoR=temp
  
  # 6) Creation de la variable NumberSampled
  #creation d'une cle
  SubSamples4EchoR$key=paste(SubSamples4EchoR$Code_Station, SubSamples4EchoR$Id_Operation, SubSamples4EchoR$Code_Espece_Campagne,SubSamples4EchoR$Class_Tri, SubSamples4EchoR$Sexe,sep="_")
  res=aggregate(SubSamples4EchoR$Nbr,list(SubSamples4EchoR$key),sum)
  colnames(res)=c("key","nb_ech")
  SubSamples4EchoR=merge(SubSamples4EchoR,res,by="key",all.x=T)
  
  
  # 7) Correction Poids_Classe_Taille
  SubSamples4EchoR[is.na(SubSamples4EchoR$Poids_Classe_Taille),"Poids_Classe_Taille"]=""
  
  
  # 8) Correction Unite_Taille
  SubSamples4EchoR$Unite_Taille=as.character(SubSamples4EchoR$Unite_Taille)
  SubSamples4EchoR[SubSamples4EchoR$Unite_Taille=="cm","Unite_Taille"]="1"
  SubSamples4EchoR[SubSamples4EchoR$Unite_Taille=="mm","Unite_Taille"]="0"
  SubSamples4EchoR$Unite_Taille=as.factor(SubSamples4EchoR$Unite_Taille)
  
  
  # 9) Correction Precision_Mesure
  SubSamples4EchoR[is.na(SubSamples4EchoR$Precision_Mesure),"Precision_Mesure"]=""
  SubSamples4EchoR[SubSamples4EchoR$Precision_Mesure==0.5,"Precision_Mesure"]=5
  SubSamples4EchoR[SubSamples4EchoR$Precision_Mesure==1.0,"Precision_Mesure"]=1
  SubSamples4EchoR$Precision_Mesure=as.factor(SubSamples4EchoR$Precision_Mesure)
  
  
  # 10) mise au propre du fichier et changement des noms de colonnes pour coller au format EchoR
  #suppression des variables inutiles
  SubSamples4EchoR=SubSamples4EchoR[,!colnames(SubSamples4EchoR)%in%c("key")]
  
  #mise en l'ordre des variables
  SubSamples4EchoR=SubSamples4EchoR[,c("Code_Station","Id_Operation","Code_Espece_Campagne","Class_Tri","Sexe","Poids_Reference","nb_ech","Taille","Nbr","Poids_Classe_Taille","Unite_Taille","Precision_Mesure")]
  
  #renommer les variables
  colnames(SubSamples4EchoR)=c("operationID","subHaul","baracoudacode","sizeCategory","sexCategory","sampleWeight","numberSampled","lengthClass","numberAtLength","weightAtLength","units","round")
  
  # ordonner par espece, trait et taille
  SubSamples4EchoR=SubSamples4EchoR[order(SubSamples4EchoR$operationID,SubSamples4EchoR$baracoudacode,SubSamples4EchoR$sizeCategory,SubSamples4EchoR$lengthClass),]
  
  # ajout de l'immatriculation du navire
  SubSamples4EchoR$vessel=unique(OPERATION$Navire)
  
  
  # 11) Verification : le fichier final doit contenir
  #dim(SubSamples4EchoR)
  # n lignes et 13 colonnes
  
  # 12) Sauvegarde du fichier
  #path="C:/Campagne/Format_g?n?rique_Tutti_Vers_EchoR/PELGAS_2016/"
  #write.table(SubSamples4EchoR,"SubSamples4EchoR.csv",row.names=F,quote=F,sep=";")
  
  
  
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  #                                                                                                                     #
  #                         ETAPE 3 : Construction du fichier TotalSamples4EchoR.csv                                   #
  #                                                                                                                     #
  # ------------------------------------------------------------------------------------------------------------------- #
  # ------------------------------------------------------------------------------------------------------------------- #
  
  
  # 1) Donnees dans CATCH
  temp2=CATCH[,c(4,8,9,12,19,26,14,21,28)] #Info : ici on repart de la table CATCH avec les lignes de Benthos supprimees
  colnames(temp2)=c("Code_Station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Class_Tri","Sexe","Tot_V_HV","Tot_Class_Tri","Tot_Sexe")
  
  
  # 3) Correction de la variable "Class_Tri" nom original mais possible d'etre modifie variable 7 (idem que pour SubSample)
  temp2$Class_Tri2=substring(temp2$Class_Tri,1,1)
  temp2$V_HV=as.character(temp2$V_HV)
  temp2[temp2$V_HV=="Vrac","V_HV"]=""
  temp2[temp2$V_HV=="Hors Vrac","V_HV"]="H"
  temp2[is.na(temp2$Class_Tri2),"Class_Tri2"]=""
  temp2$Class_Tri3=paste(temp2$V_HV,temp2$Class_Tri2,sep="")
  temp2[temp2$Class_Tri3=="","Class_Tri3"]="0"
  temp2$Class_Tri3=as.factor(temp2$Class_Tri3)
  temp2$Class_Tri=temp2$Class_Tri3
  
  # suppression des variables temporaires
  temp2=temp2[,!colnames(temp2)%in%c("Class_Tri2","Class_Tri3")]
  
  #recodage de sexe
  temp2$Sexe=as.character(temp2[,"Sexe"])
  temp2[is.na(temp2$Sexe),"Sexe"]="N"
  temp2[temp2$Sexe=="UNK - Indetermine","Sexe"]="I"
  temp2[temp2$Sexe=="Femelle","Sexe"]="F"
  temp2[temp2$Sexe=="Male","Sexe"]="M"
  temp2[temp2$Sexe=="Non sexe","Sexe"]="I"
  temp2$Sexe=as.factor(temp2$Sexe)
  temp2=unique(temp2) #permet de valider quon n'a plus de doublon mais qu'on conserve bien les lignes sous-?chantillonn?es Sexe
  
  #conserver un tableau avec les valeurs totales pour les classes de tri ou poids total vrac
  # le sexe n'est pas une cat?gorie donc il faut conserver toutes les valeurs pour pouvoir les sommer et obtenir le poids total de la cat?gorie ou du vrac total esp?ce
  # pas faire de unique, risque de perdre des valeurs identiques 
  temp2=temp2[,c(1:5,7:9)] #suppression de la colonne sexe
  
  if(length(which((is.na(temp2$Tot_V_HV))&(is.na(temp2$Tot_Class_Tri))))>=1){
    extract=temp2[which( (is.na(temp2$Tot_V_HV)) & (is.na(temp2$Tot_Class_Tri)) ),]
    extract2=unique(temp2[-which( (is.na(temp2$Tot_V_HV)) & (is.na(temp2$Tot_Class_Tri)) ),])
    temp2=rbind(extract,extract2)
  }else{
    temp2=unique(temp2)
  }
  
  
  #CAS ou il n'y a pas de sous-?chantillon (sous-?chantillon=?chantillon)
  for (i in 1:dim(temp2)[1]){
    
    #cas : pas de sous cat?gorie, un seul sous-?chantillon
    if( (is.na(temp2[i,"Tot_Class_Tri"])) & (is.na(temp2[i,"Tot_Sexe"])) & (is.na(temp2[i,"Tot_V_HV"])) ) {
      
      #affecter le poids de reference (sommes des poids-?chantillonn? comme poids total vrac)
      temp2[i,"Tot_V_HV"]=unique(temp[which(temp$Code_Station==temp2[i,"Code_Station"] & temp$Code_Espece_Campagne==temp2[i,"Code_Espece_Campagne"]),"Poids_Reference"])
      
    }
  }
  
  
  
  # 2) remplir la colonne Tot_Class_Tri
  for (i in 1:dim(temp2)[1]){
    
    #1er cas : Sous categorie tri mais pas de sexe
    if(is.na(temp2[i,"Tot_Class_Tri"])) {
      if(is.na(temp2[i,"Tot_Sexe"])){
        temp2[i,"Tot_Class_Tri"]=temp2[i,"Tot_V_HV"]
      }else{
        temp2[i,"Tot_Class_Tri"]=temp2[i,"Tot_Sexe"]
      }  
    }
  }
  
  # 3) remplir la colonne Tot_V_HV
  for (i in 1:dim(temp2)[1]){
    
    #1er cas : Sous categorie tri mais pas de sexe
    if(is.na(temp2[i,"Tot_V_HV"])) {
      if(is.na(temp2[i,"Tot_Sexe"])){
        temp2[i,"Tot_V_HV"]=temp2[i,"Tot_Class_Tri"]
      }else{
        temp2[i,"Tot_V_HV"]=temp2[i,"Tot_Sexe"]
      }  
    }
  }
  
  
  sauv_somme_pds_trie=temp2
  
  # 3) Somme des poids tries par cle unique
  temp2$key=paste(temp2$Code_Station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,temp2$Class_Tri,sep="_")
  temp2$key2=paste(temp2$Code_Station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,sep="_")
  
  #cas d'un sous-echantillon de poids trie (elever le poids)
  final=data.frame()
  
  for (i in unique(temp2$key2)){
    
    sel=temp2[which(temp2$key2==i),]
    if(any(sel$Class_Tri=="0")){
      if( (sum(sel[,"Tot_Class_Tri"])) < (sum(sel[,"Tot_V_HV"])) ) {
        sel[,"Tot_Class_Tri"]=sel[,"Tot_Class_Tri"]*unique(sel[,"Tot_V_HV"])/sum(sel[,"Tot_Class_Tri"])
      } 
    }else{
      if( (sum(sel[,"Tot_Class_Tri"])) < (sum(unique(sel[,"Tot_V_HV"]))) ) {
        sel[,"Tot_Class_Tri"]=sel[,"Tot_Class_Tri"]*unique(sel[,"Tot_V_HV"])/sum(sel[,"Tot_Class_Tri"])
      }
    }
    final=rbind(final,sel)
    
  }
  
  temp2=final
  Somme_pds_trie=aggregate(temp2$Tot_Class_Tri,list(temp2$key),sum)
  colnames(Somme_pds_trie)=c("key","SommePds")
  temp2=merge(temp2,Somme_pds_trie,by="key",all.x=T)
  
  
  # 4) suppression des variables temporaires
  temp2=temp2[,!colnames(temp2)%in%c("key","Tot_V_HV","Tot_Class_Tri","Tot_Sexe")]
  colnames(temp2)[5]="Tot_Class_Tri"
  temp2=unique(temp2)
  
  # 5) Verifie la somme HV = au nombre dans la table Operation idem pour le vrac
  OP=OPERATION[,c("Code_Station","Poids_Total","Poids_Total_Vrac","Poids_Total_Espece_Vrac_Trie","Poids_Total_HorsVrac","Poids_Total_Espece_Vrac","Poids_Total_Benthos_Vrac","Poids_Total_Espece_HorsVrac","Poids_Total_Benthos_HorsVrac","Poids_Total_Espece_Inerte_Trie","Poids_Total_Espece_Vivant_non_detaille_trie")]
  #cas des valeurs manquantes notees a  -9, a  remplacer par 0
  OP[OP$Poids_Total==-9,"Poids_Total"]=0
  OP[OP$Poids_Total_Vrac==-9,"Poids_Total_Vrac"]=0
  OP[OP$Poids_Total_Espece_Vrac_Trie==-9,"Poids_Total_Espece_Vrac_Trie"]=0
  OP[OP$Poids_Total_HorsVrac==-9,"Poids_Total_HorsVrac"]=0
  OP[OP$Poids_Total_Espece_Vrac==-9,"Poids_Total_Espece_Vrac"]=0
  OP[OP$Poids_Total_Benthos_Vrac==-9,"Poids_Total_Benthos_Vrac"]=0
  OP[OP$Poids_Total_Espece_HorsVrac==-9,"Poids_Total_Espece_HorsVrac"]=0
  OP[OP$Poids_Total_Benthos_HorsVrac==-9,"Poids_Total_Benthos_HorsVrac"]=0
  OP[OP$Poids_Total_Espece_Inerte_Trie==-9,"Poids_Total_Espece_Inerte_Trie"]=0
  OP[OP$Poids_Total_Espece_Vivant_non_detaille_trie==-9,"Poids_Total_Espece_Vivant_non_detaille_trie"]=0
  OP$Poids_Total=round(OP$Poids_Total,3)
  OP$Poids_Total_Vrac=round(OP$Poids_Total_Vrac,3)
  OP$Poids_Total_Espece_Vrac_Trie=round(OP$Poids_Total_Espece_Vrac_Trie,3)
  OP$Poids_Total_HorsVrac=round(OP$Poids_Total_HorsVrac,3)
  OP$Poids_Total_Espece_Vrac=round(OP$Poids_Total_Espece_Vrac,3)
  OP$Poids_Total_Benthos_Vrac=round(OP$Poids_Total_Benthos_Vrac,3)
  OP$Poids_Total_Espece_HorsVrac=round(OP$Poids_Total_Espece_HorsVrac,3)
  OP$Poids_Total_Benthos_HorsVrac=round(OP$Poids_Total_Benthos_HorsVrac,3)
  OP$Poids_Total_Espece_Inerte_Trie=round(OP$Poids_Total_Espece_Inerte_Trie,3)
  OP$Poids_Total_Espece_Vivant_non_detaille_trie=round(OP$Poids_Total_Espece_Vivant_non_detaille_trie,3)
  
  temp2[temp2$V_HV=="","V_HV"]="V"
  temp3=aggregate(temp2$SommePds,list(temp2$Code_Station,temp2$V_HV),sum)
  colnames(temp3)=c("Code_Station","V_HV","Somme_poids_trie")
  temp3$Somme_poids_trie=round(temp3$Somme_poids_trie,3)
  #indicateur Somme_poids_trie correspond au poids total espece echantillonne ne prend pas en compte les poids inerte trie et vivant non detaille trie 
  temp3_OP=merge(temp3,OP,by="Code_Station",all.x=T)
  temp3_OP[is.na(temp3_OP$Somme_poids_trie),"Somme_poids_trie"]=0
  temp3_OP[,"Total_vrac"]=temp3_OP[,"Poids_Total_Espece_Vrac"]-temp3_OP[,"Poids_Total_Espece_Inerte_Trie"]-temp3_OP[,"Poids_Total_Espece_Vivant_non_detaille_trie"]
  temp3_OP$Total_vrac=round(temp3_OP$Total_vrac,3)
  temp3_OP[,"Poids_Total_Espece_Vrac_Trie"]=temp3_OP[,"Poids_Total_Espece_Vrac_Trie"]-temp3_OP[,"Poids_Total_Espece_Inerte_Trie"]-temp3_OP[,"Poids_Total_Espece_Vivant_non_detaille_trie"]
  temp3_OP$Poids_Total_Espece_Vrac_Trie=round(temp3_OP$Poids_Total_Espece_Vrac_Trie,3)
  
  for (i in 1:nrow(temp3_OP)){
    if(temp3_OP[i,"V_HV"]=="H"){
      if(temp3_OP[i,"Somme_poids_trie"]==temp3_OP[i,"Poids_Total_Espece_HorsVrac"]){ # ne pas comparer au Pds total Hors-Vrac causes des cas de Benthos,  Pds tot HV = Pds tot esp HV + Pds tot benthos HV
        temp3_OP[i,"Valide_HorsVrac"]="TRUE"
      }else{
        temp3_OP[i,"Valide_HorsVrac"]="FALSE"
      }
    }else{
      if(temp3_OP[i,"Somme_poids_trie"]==temp3_OP[i,"Poids_Total_Espece_Vrac_Trie"]){ # Pds tot V = Pds tot esp V + Pds tot benthos V, Pds tot esp V = Pds esp V + Pds inerte trie + Pds vivant non detaille trie 
        temp3_OP[i,"Valide_Vrac"]="TRUE"
      }else{
        temp3_OP[i,"Valide_Vrac"]="FALSE"
      }
    }
  }
  
  
  # visualise les lignes  FALSE pour le VRAC
  temp3_OP[which(temp3_OP$Valide_Vrac=="FALSE"),]
  
  # visualise les lignes FALSE pour le HORS - VRAC
  temp3_OP[which(temp3_OP$Valide_HorsVrac=="FALSE"),]
  
  
  #----------------------------------------
  # ELEVATIONS
  #----------------------------------------  
  
  # 6) Si pas d'erreur "FALSE", on peut continuer et elever les poids.
  # variable Poids eleve
  temp2=merge(temp2,OP,by.x="Code_Station",by.y="Code_Station",all.x=T)
  
  for (i in 1:nrow(temp2)){
    if(temp2[i,"V_HV"]=="H"){
      temp2[i,"Poids_eleve"]=temp2[i,"SommePds"]*( (temp2[i,"Poids_Total"]-(temp2[i,"Poids_Total"]-temp2[i,"Poids_Total_HorsVrac"]))/((temp2[i,"Poids_Total_Vrac"]+temp2[i,"Poids_Total_HorsVrac"])-temp2[i,"Poids_Total_Vrac"])) #Poids total echantillonne HV * ( (Pds total - (total -Pds total Vrac)/(Pds total trie - Pds total trie Vrac) )
    }else{
      temp2[i,"Poids_eleve"]=temp2[i,"SommePds"]*(temp2[i,"Poids_Total_Vrac"]/ temp2[i,"Poids_Total_Espece_Vrac_Trie"]) *( (temp2[i,"Poids_Total"]-temp2[i,"Poids_Total_HorsVrac"]) / temp2[i,"Poids_Total_Vrac"])  #Poids total echantillonne V * ( (Pds total - Pds total HVrac)/Pds total trie HVrac) )
    }
  }
  
  
  # 7) Elevation du nombre d'individus
  # 7.1.1) constitution d'une table correspondant a  la vu de tutti avec elevation des poids pour faciliter les calculs et la comprehension
  
  Vue_Tutti=CATCH[,c(4,8,9,12,14,15,19,21,22,26,28,29,37,40)]
  colnames(Vue_Tutti)=c("Code_Station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Tot_V_HV","Ech_V_HV","Class_Tri","Tot_Class_Tri","Ech_Class_Tri","Sexe","Tot_Sexe","Ech_Sexe","Poids_Classe_Taille","Nbr")
  Vue_Tutti$Class_Tri2=substring(Vue_Tutti$Class_Tri,1,1)
  Vue_Tutti$V_HV=as.character(Vue_Tutti$V_HV)
  Vue_Tutti[Vue_Tutti$V_HV=="Vrac","V_HV"]=""
  Vue_Tutti[Vue_Tutti$V_HV=="Hors Vrac","V_HV"]="H"
  Vue_Tutti[is.na(Vue_Tutti$Class_Tri2),"Class_Tri2"]=""
  Vue_Tutti$Class_Tri3=paste(Vue_Tutti$V_HV,Vue_Tutti$Class_Tri2,sep="")
  Vue_Tutti[Vue_Tutti$Class_Tri3=="","Class_Tri3"]="0"
  Vue_Tutti$Class_Tri3=as.factor(Vue_Tutti$Class_Tri3)
  Vue_Tutti$Class_Tri=Vue_Tutti$Class_Tri3
  
  Vue_Tutti$Sexe=as.character(Vue_Tutti$Sexe)
  Vue_Tutti[is.na(Vue_Tutti$Sexe),"Sexe"]="N"
  Vue_Tutti[Vue_Tutti$Sexe=="UNK - Indetermine","Sexe"]="I"
  Vue_Tutti[Vue_Tutti$Sexe=="Femelle","Sexe"]="F"
  Vue_Tutti[Vue_Tutti$Sexe=="Male","Sexe"]="M"
  Vue_Tutti[Vue_Tutti$Sexe=="Non sexe","Sexe"]="I"
  Vue_Tutti$Sexe=as.factor(Vue_Tutti$Sexe)
  
  
  # 7.1.2) suppression des variables temporaires
  Vue_Tutti=Vue_Tutti[,!colnames(Vue_Tutti)%in%c("Class_Tri2","Class_Tri3")]
  
  # 7.1.3) Faire l'elevation des nombres
  Vue_Tutti$key=paste(Vue_Tutti$Code_Station, Vue_Tutti$Code_Espece_Campagne,Vue_Tutti$Nom_Scientifique, Vue_Tutti$Class_Tri, Vue_Tutti$Sexe,sep="_")
  res=aggregate(Vue_Tutti$Nbr,list(Vue_Tutti$key),sum)
  colnames(res)=c("key","nb_ech")
  Vue_Tutti=merge(Vue_Tutti,res,by="key",all.x=T)
  
  # 7.1.4) Faire l'elevation des poids echantillonne
  #faire la somme des poids sous-taille par sous categorie
  id_pds_s_taille=which(!is.na(Vue_Tutti$Poids_Classe_Taille))
  if (length(id_pds_s_taille)>0){
    pds_s_taille=Vue_Tutti[id_pds_s_taille,]
    pds_s_taille$key=paste(pds_s_taille$Code_Station,pds_s_taille$Code_Espece_Campagne,pds_s_taille$Nom_Scientifique,pds_s_taille$Class_Tri,pds_s_taille$Sexe,sep="_")
    tempo=aggregate(pds_s_taille$Poids_Classe_Taille,list(pds_s_taille$key),"sum")
    colnames(tempo)=c("key","Somme_poids_ech")
    #affecte la somme des poids si le plus bas niveau est renseigne
    Vue_Tutti=merge(Vue_Tutti,tempo,by="key",all.x=T)
  }else{
    Vue_Tutti$Somme_poids_ech=NA
  }
  
  
  #supprime la variable nbr
  Vue_Tutti=Vue_Tutti[,!colnames(Vue_Tutti)%in%c("Nbr","Poids_Classe_Taille")]
  Vue_Tutti=unique(Vue_Tutti)
  
  # 7.1.5) Creation de la colonne total Poids Vrac/Hors Vrac echantillonne
  #reprendre le fichier temp2 precedent et calculer le poids total V/HV
  temp2$key2=paste(temp2$Code_Station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,temp2$V_HV,sep="_")
  Pds_tot_V_HV=aggregate(temp2$SommePds,list(temp2$key2),sum)
  colnames(Pds_tot_V_HV)=c("key2","Pds_tot_ech_V_HV")
  
  Vue_Tutti[Vue_Tutti$V_HV=="","V_HV"]="V"
  Vue_Tutti$key2=paste(Vue_Tutti$Code_Station,Vue_Tutti$Code_Espece_Campagne,Vue_Tutti$Nom_Scientifique,Vue_Tutti$V_HV,sep="_")
  Vue_Tutti=merge(Vue_Tutti,Pds_tot_V_HV,by="key2",all.x=T)
  
  # 7.1.6) Creation de la colonne echantillonne total
  for (i in 1:dim(Vue_Tutti)[1]){
    
    if(is.na(Vue_Tutti[i,"Ech_V_HV"])) { # cas pas de sous echantillon V/HV
      if(is.na(Vue_Tutti[i,"Ech_Class_Tri"])){ # cas pas de sous echantillon par sous categorie G,P
        if(is.na(Vue_Tutti[i,"Ech_Sexe"])){ # cas pas de sous echantillon pas sous categorie sexe
          if(is.na(Vue_Tutti[i,"Somme_poids_ech"])){
            Vue_Tutti[i,"Ech_V_HV"]=0
          }else{
            Vue_Tutti[i,"Ech_V_HV"]=Vue_Tutti[i,"Somme_poids_ech"]
          }  
        }else{
          Vue_Tutti[i,"Ech_V_HV"]=Vue_Tutti[i,"Ech_Sexe"]
        }
      }else{
        Vue_Tutti[i,"Ech_V_HV"]=Vue_Tutti[i,"Ech_Class_Tri"]
      }
    }else{
      Vue_Tutti[i,"Ech_V_HV"]=Vue_Tutti[i,"Ech_V_HV"]
    } 
    
  }
  
  
  # 7.1.7) Mise en forme de la table pour qu'elle ressemble a  la vue Tutti
  Vue_Tutti_Finale=Vue_Tutti[,c("Code_Station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Pds_tot_ech_V_HV","Class_Tri","Tot_Class_Tri","Sexe","Tot_Sexe","Ech_V_HV","nb_ech")]
  
  # 7.2) Creation de la variable somme des freres des tri
  Frere_tri=Vue_Tutti_Finale[,c("Code_Station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Class_Tri","Tot_Class_Tri")]
  Frere_tri=unique(Frere_tri)
  Frere_tri$key=paste(Frere_tri$Code_Station,Frere_tri$Code_Espece_Campagne,Frere_tri$Nom_Scientifique,Frere_tri$V_HV,sep="_")
  res_somme_frere_tri=aggregate(Frere_tri$Tot_Class_Tri,list(Frere_tri$key),sum)
  colnames(res_somme_frere_tri)=c("key","Somme_frere_tri")
  
  # 7.3) Creation de la variable somme des freres des sexe
  Frere_sexe=Vue_Tutti_Finale[,c("Code_Station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Class_Tri","Sexe","Tot_Sexe")]
  Frere_sexe=unique(Frere_sexe)
  Frere_sexe$key=paste(Frere_sexe$Code_Station,Frere_sexe$Code_Espece_Campagne,Frere_sexe$Nom_Scientifique,Frere_sexe$V_HV,Frere_sexe$Class_Tri,sep="_")
  res_somme_frere_sexe=aggregate(Frere_sexe$Tot_Sexe,list(Frere_sexe$key),sum)
  colnames(res_somme_frere_sexe)=c("key3","Somme_frere_sexe")
  
  # 7.4) ajout des deux variables ? la vue finale
  Vue_Tutti_Finale$key=paste(Vue_Tutti_Finale$Code_Station,Vue_Tutti_Finale$Code_Espece_Campagne,Vue_Tutti_Finale$Nom_Scientifique,Vue_Tutti_Finale$V_HV,sep="_")
  Vue_Tutti_Finale$key3=paste(Vue_Tutti_Finale$Code_Station,Vue_Tutti_Finale$Code_Espece_Campagne,Vue_Tutti_Finale$Nom_Scientifique,Vue_Tutti_Finale$V_HV,Vue_Tutti_Finale$Class_Tri,sep="_")
  
  Vue_Tutti_Finale=merge(Vue_Tutti_Finale,res_somme_frere_tri,by="key",all.x=T)
  Vue_Tutti_Finale=merge(Vue_Tutti_Finale,res_somme_frere_sexe,by="key3",all.x=T)
  
  Vue_Tutti_Finale=merge(Vue_Tutti_Finale,temp2[,c("key2","Poids_Total","Poids_Total_Vrac","Poids_Total_Espece_Vrac","Poids_Total_Espece_Vrac_Trie","Poids_Total_HorsVrac","Poids_Total_Espece_HorsVrac")],by.x="key",by.y="key2",all.x=T)
  Vue_Tutti_Finale=unique(Vue_Tutti_Finale)
  
  # 7.2) Creation de la variable nombre elevee
  
  # 7.2.1) formules
  
  for (i in 1:dim(Vue_Tutti_Finale)[1]){
    
    #debut des conditions
    #VRAC
    if(Vue_Tutti_Finale[i,"V_HV"]=="V"){
      
      if(!is.na(Vue_Tutti_Finale[i,"Ech_V_HV"]) & (Vue_Tutti_Finale[i,"Ech_V_HV"]==0.0)){
        if(is.na(Vue_Tutti_Finale[i,"Tot_Sexe"])){
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"])
          }
        }else{
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"]) *((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"])
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"])
          }
        }
      }else  if(!is.na(Vue_Tutti_Finale[i,"Ech_V_HV"]) & (Vue_Tutti_Finale[i,"Ech_V_HV"]!=0.0)){
        if(is.na(Vue_Tutti_Finale[i,"Tot_Sexe"])){
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]* (Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"])
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"])
          }
        }else{
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Sexe"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"]) *((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"])
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Sexe"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*(Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"]/ Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac_Trie"])*((Vue_Tutti_Finale[i,"Poids_Total"]-Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"]) / Vue_Tutti_Finale[i,"Poids_Total_Espece_Vrac"])
          }
        }
      }else if (is.na(Vue_Tutti_Finale[i,"Ech_V_HV"])){
        Vue_Tutti_Finale[i,"nb_eleve"]=NA
      }
      
    }else{
      if(!is.na(Vue_Tutti_Finale[i,"Ech_V_HV"]) & (Vue_Tutti_Finale[i,"Ech_V_HV"]==0.0)){
        if(is.na(Vue_Tutti_Finale[i,"Tot_Sexe"])){
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])
          }
        }else{
          if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])* ( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])
          }else{
            Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])
          }
        }
      }else if (is.na(Vue_Tutti_Finale[i,"Ech_V_HV"])){
        Vue_Tutti_Finale[i,"nb_eleve"]=NA
      }else if(is.na(Vue_Tutti_Finale[i,"Tot_Sexe"])){
        if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
          Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])
        }else{
          Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])
        }
      }else if(is.na(Vue_Tutti_Finale[i,"Tot_Class_Tri"])){
        Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Sexe"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) / Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])
      }else{
        Vue_Tutti_Finale[i,"nb_eleve"]=Vue_Tutti_Finale[i,"nb_ech"]*(Vue_Tutti_Finale[i,"Tot_Sexe"]/Vue_Tutti_Finale[i,"Ech_V_HV"])*(Vue_Tutti_Finale[i,"Tot_Class_Tri"]/Vue_Tutti_Finale[i,"Somme_frere_sexe"])*(Vue_Tutti_Finale[i,"Pds_tot_ech_V_HV"]/Vue_Tutti_Finale[i,"Somme_frere_tri"])*( (Vue_Tutti_Finale[i,"Poids_Total"]- (Vue_Tutti_Finale[i,"Poids_Total"]- Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])) /Vue_Tutti_Finale[i,"Poids_Total_HorsVrac"])
      }
      
    }#fin des conditions
  }#fin du for
  
  
  
  # 7.2.2) Mise au format de la variable
  Vue_Tutti_Finale$nb_eleve=round(Vue_Tutti_Finale$nb_eleve)
  
  # 7.3) Pour le fichier resultat rentrant dans Echo-R il ne faut pas de la sous-categorie Sexe
  Res_nb_eleve=aggregate(Vue_Tutti_Finale$nb_eleve,list(Vue_Tutti_Finale$key3),sum)
  colnames(Res_nb_eleve)=c("key3","Nb_eleve")
  
  
  # 7.4) Calcul de la moyenne ponderee des longueurs
  Long_moy=CATCH[,c(4,8,9,12,19,35,40)]
  colnames(Long_moy)=c("Code_Station","Code_Espece_Campagne","Nom_Scientifique","V_HV","Class_Tri","Taille","Nbr")
  
  Long_moy$Class_Tri2=substring(Long_moy$Class_Tri,1,1)
  Long_moy$V_HV=as.character(Long_moy$V_HV)
  Long_moy[Long_moy$V_HV=="Vrac","V_HV"]=""
  Long_moy[Long_moy$V_HV=="Hors Vrac","V_HV"]="H"
  Long_moy[is.na(Long_moy$Class_Tri2),"Class_Tri2"]=""
  Long_moy$Class_Tri3=paste(Long_moy$V_HV,Long_moy$Class_Tri2,sep="")
  Long_moy[Long_moy$Class_Tri3=="","Class_Tri3"]="0"
  Long_moy$Class_Tri3=as.factor(Long_moy$Class_Tri3)
  Long_moy$Class_Tri=Long_moy$Class_Tri3
  
  # suppression des variables temporaires
  Long_moy=Long_moy[,!colnames(Long_moy)%in%c("Class_Tri2","Class_Tri3")]
  Long_moy=unique(Long_moy)
  
  # creation de la cle
  Long_moy$key=paste(Long_moy$Code_Station,Long_moy$Code_Espece_Campagne,Long_moy$Nom_Scientifique,Long_moy$V_HV,Long_moy$Class_Tri,sep="_")
  Res_long_moy=Long_moy$key 
  Res_long_moy=data.frame(Res_long_moy)
  Res_long_moy$moy=rep(0,dim(Res_long_moy)[1])        
  Res_long_moy=unique(Res_long_moy)
  colnames(Res_long_moy)=c("key","moy")
  
  for(i in Long_moy$key){
    Data=Long_moy[which(Long_moy$key==i),]
    moy_ponderee=weighted.mean(Data[,"Taille"],Data[,"Nbr"],na.rm=T)*10 #pour mettre en mm
    Res_long_moy[which(Res_long_moy$key==i),"moy"]=moy_ponderee
  }                     
  
  
  # 7.5) Regroupement des differentes variables pour creation du fichier final
  #refaire une cle dans temp2
  temp2$key3=paste(temp2$Code_Station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,temp2$V_HV,temp2$Tot_Class_Tri,sep="_")
  temp2[temp2$V_HV=="V","V_HV"]=""
  temp2$key=paste(temp2$Code_Station,temp2$Code_Espece_Campagne,temp2$Nom_Scientifique,temp2$V_HV,temp2$Tot_Class_Tri,sep="_")
  
  
  #poids echantillonne + poids eleve
  TotSamples4EchoR=temp2[,c(1,2,5,7,18,19,20)]
  
  #nombre eleve
  TotSamples4EchoR=merge(TotSamples4EchoR,Res_nb_eleve,by="key3",all.x=T)
  
  #moyenne des longueurs
  TotSamples4EchoR=merge(TotSamples4EchoR,Res_long_moy,by="key",all.x=T)
  
  # moyenne des poids
  TotSamples4EchoR$meanWeight=(TotSamples4EchoR$Poids_eleve/TotSamples4EchoR$Nb_eleve)
  # passer de kg a  g
  TotSamples4EchoR$meanWeight=TotSamples4EchoR$meanWeight*1000
  
  #nombre par kg
  TotSamples4EchoR$noPerKg=(TotSamples4EchoR$Nb_eleve/TotSamples4EchoR$Poids_eleve)
  
  #modifie sommepoids avec le vrai poids sous-echantillonne trie
  sauv_somme_pds_trie$key=paste(sauv_somme_pds_trie$Code_Station,sauv_somme_pds_trie$Code_Espece_Campagne,sauv_somme_pds_trie$Nom_Scientifique,sauv_somme_pds_trie$V_HV,sauv_somme_pds_trie$Class_Tri,sep="_")
  
  #calcul le poids ?chantillonn?e par cat?gorie de tri de plus bas niveau 
  if( (length(which( (sauv_somme_pds_trie$Class_Tri=="0")  & !(is.na(sauv_somme_pds_trie$Tot_Sexe)) ) >=1 ) )) {
    extraction=sauv_somme_pds_trie[which( (sauv_somme_pds_trie$Class_Tri=="0") & !(is.na(sauv_somme_pds_trie$Tot_Sexe)) ),]
    extraction_pds_tri=aggregate(extraction$Tot_V_HV,list(extraction$key),sum)
    colnames(extraction_pds_tri)=c("key","Tot_Class_Tri")
    extraction2=sauv_somme_pds_trie[-which( (sauv_somme_pds_trie$Class_Tri=="0") & !(is.na(sauv_somme_pds_trie$Tot_Sexe)) ),]
    extraction2=unique(extraction2[,c("key","Tot_Class_Tri")])
    
    sauv_somme_pds_trie=rbind(extraction_pds_tri,extraction2)
  }else{
    sauv_somme_pds_trie=unique(sauv_somme_pds_trie[,c("key","Tot_Class_Tri")])
  }
  
  TotSamples4EchoR=merge(TotSamples4EchoR,sauv_somme_pds_trie,by="key",all.x=T)
  
  # 7.6) Mise en forme du fichier resultat
  TotSamples4EchoR=TotSamples4EchoR[,c(3:5,7:12)]
  #TotSamples4EchoR=TotSamples4EchoR[,c("Code_Station","Code_Espece_Campagne","Tot_Class_Tri","Poids_eleve","Nb_eleve","moy","meanWeight","noPerKg","SommePds")]
  colnames(TotSamples4EchoR)=c("operationID","baracoudacode","sizeCategory","sampleWeight","numberSampled","meanLength","meanWeight","noPerKg","sortedWeight")
  
  # arrondir a  trois decimales
  TotSamples4EchoR$sampleWeight=round(TotSamples4EchoR$sampleWeight,3)
  TotSamples4EchoR$meanLength=round(TotSamples4EchoR$meanLength,3)
  TotSamples4EchoR$meanWeight=round(TotSamples4EchoR$meanWeight,3)
  TotSamples4EchoR$noPerKg=round(TotSamples4EchoR$noPerKg,3)
  TotSamples4EchoR$sortedWeight=round(TotSamples4EchoR$sortedWeight,3)
  TotSamples4EchoR$vessel=unique(OPERATION$Navire)
  
  # 8) Sauvegarde du fichier
  #dim(TotSamples4EchoR)
  # x lignes 10 colonnes
  #write.table(TotSamples4EchoR,"TotSamples4EchoR.csv",row.names=F,quote=F,sep=";")
  
  
  return(list(SubSamples4EchoR,TotSamples4EchoR))
  
}

