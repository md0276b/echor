infl2=function (x, y, z, dlim, ndisc, pol = NA, visu = FALSE, opt = 0, 
          mode = 0) 
{
  miss <- function(x) {
    length(x) == 1 && is.na(x)
  }
  extend <- rev(sort(c((2 * dlim)/diff(range(x)), (2 * dlim)/diff(range(y)))))[1]
  nd <- length(x)
  if (length(dlim) == 1){ 
    dlim <- rep(dlim, 2)
  }else opt <- 1
  xrange <- range(x)
  yrange <- range(y)
  dx <- xrange[2] - xrange[1]
  dy <- yrange[2] - yrange[1]
  xmin <- xrange[1] - extend * dx
  xmax <- xrange[2] + extend * dx
  ymin <- yrange[1] - extend * dy
  ymax <- yrange[2] + extend * dy
  nx <- ndisc
  ny <- ndisc
  dx <- (xmax - xmin)/nx
  dy <- (ymax - ymin)/ny
  ng <- nx * ny
  maille <- dx * dy
  xg <- rep(xmin + dx * seq(0, nx - 1), ny)
  yg <- rep(ymin + dy * seq(0, ny - 1), rep(nx, ny))
  if (!miss(pol)) {
    sel.pol <- inout(data.frame(x = xg, y = yg), data.frame(x = pol$x, 
                                                            y = pol$y))
    xg[!sel.pol] <- NA
    yg[!sel.pol] <- NA
  }
  argin <- function(x, test = 1.234e+30) {
    if (length(x) > 0) 
      x[is.na(x)] <- test
    x
  }
  argout <- function(x, test = 1.234e+30) {
    if (length(x) > 0) 
      x[x == test] <- NA
    x
  }
  x <- argin(x)
  y <- argin(y)
  z <- argin(z)
  xg <- argin(xg)
  yg <- argin(yg)
  dlim <- argin(dlim)
  xxmin <- min(x[x != argin(NA)])
  xxmax <- max(x[x != argin(NA)])
  deltax <- xxmax - xxmin
  yymin <- min(y[y != argin(NA)])
  yymax <- max(y[y != argin(NA)])
  deltay <- yymax - yymin
  if (dlim[1] == argin(NA)) 
    dlim[1] <- deltax * extend
  if (dlim[2] == argin(NA)) 
    dlim[2] <- deltax * extend
  if (opt == 0) 
    dmax <- sqrt(dlim[1] * dlim[2])
  if (opt == 1) 
    dmax <- sqrt(dlim[1] * dlim[1] + dlim[2] * dlim[2])
  surf <- rep(0, nd)
  zg <- rep(argin(NA), ng)
  xd <- matrix(x, ng, nd, byrow = TRUE)
  yd <- matrix(y, ng, nd, byrow = TRUE)
  rkd <- matrix(1:nd, ng, nd, byrow = TRUE)
  xxg <- matrix(xg, ng, 1)
  yyg <- matrix(yg, ng, 1)
  rkg <- matrix(1:ng, ng, nd, byrow = FALSE)
  dxx <- abs(sweep(xd, 1, xxg, "-"))
  dyy <- abs(sweep(yd, 1, yyg, "-"))
  dist <- sqrt(dxx^2 + dyy^2)
  misval.dist <- sqrt((1.234e+30)^2 + (1.234e+30)^2)
  valid.dg.f <- function(d, g, dist, dxx, dyy) {
    valid <- FALSE
    if ((dxx[g, d] <= dlim[1] & dyy[g, d] <= dlim[2]) == 
        TRUE) 
      valid <- TRUE
    return(valid)
  }
  valid.g.f <- function(g, dist, dxx, dyy) {
    sapply(1:nd, valid.dg.f, g, dist, dxx, dyy)
  }
  valid.f <- function(dist, dxx, dyy) {
    t(sapply(1:ng, valid.g.f, dist, dxx, dyy))
  }
  if (opt == 0) 
    cond <- dist == apply(dist, 1, min) & dist != misval.dist & 
    dist <= dmax
  if (opt == 1) {
    dist[!valid.f(dist, dxx, dyy)] <- misval.dist
    cond <- dist == apply(dist, 1, min) & dist != misval.dist
  }
  proxd <- rkd[cond]
  proxg <- rkg[cond]
  nbn <- as.data.frame(table(proxd))
  surf[as.numeric(levels(nbn$proxd))] <- nbn$Freq * maille
  if (mode == 0) 
    zg[proxg] <- z[proxd]
  if (mode == 1) 
    zg[proxg] <- proxd
  if (mode == 2) 
    zg[proxg] <- surf[proxd]
  zg <- argout(zg)
  surf <- argout(surf)
  x <- argout(x)
  y <- argout(y)
  z <- argout(z)
  zg <- matrix(zg, nrow = nx, ncol = ny, byrow = F)
  xg <- xmin + dx * seq(0, (nx - 1))
  yg <- ymin + dy * seq(0, (ny - 1))
  if (visu) {
    image(xg, yg, zg, col = c("blue", rev(rainbow(abs(diff(range(z))), 
                                                  start = 0, end = 1/6))), asp = 1)
    symbols(x, y, sqrt(surf), fg = 3, inches = 0.2, add = T)
    if (!miss(pol)) 
      lines(pol)
  }
  list(surf=surf,xg=xg,yg=yg,zg=zg)
}