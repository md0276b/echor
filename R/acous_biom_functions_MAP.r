#***************************
#Functions for mapping biomass assessment results
#***************************

# Methods for data random averaging over a common grid 
#***************************
# P.Petitgas, Ifremer, 26 June 2013
####

####
# Define GRID and MASK (default: that of WGACEGG08)
####


# blocking_wgacegg.r
# ICES WGACEGG 2010, 22 nov
# Definition of common grid
# Definition of mask for not mapping coastal cells with centre on land
# Pierre Petitgas, IFREMER

####
# GRID (that of WGACEGG08)
####

define.grid.poly.mask=function(x1=-10.2,x2=-1,y1=35.8,y2=48,
                               ax=0.25,ay=0.25,u=2,ni=200,
                               maskit=FALSE,poligonit=TRUE,
                               shelf=TRUE){
  
  #set.seed(3.14)   # seed for random origin of grid
  
  nx<-length(seq(x1,x2,ax)); ny<-length(seq(y1,y2,ay))
  xg<-x1+ax/2+(0:nx)*ax; yg<-y1+ay/2+(0:ny)*ay
  xg<-xg[-length(xg)]; yg<-yg[-length(yg)]
  
  assign("nx", nx, envir=globalenv())
  assign("ny", ny, envir=globalenv())
  assign("xg", xg, envir=globalenv())
  assign("yg", yg, envir=globalenv())
  
  vecxg<-rep(xg,ny); vecig<-rep(1:nx,ny)
  vecyg<-NULL; for (j in 1:ny) {vecyg<-c(vecyg,rep(yg[j],nx))}
  vecjg<-NULL; for (j in 1:ny) {vecjg<-c(vecjg,rep(    j,nx))}
  matxg<-matrix(vecxg,nrow=nx,ncol=ny)
  matyg<-matrix(vecyg,nrow=nx,ncol=ny)
  matig<-matrix(vecig,nrow=nx,ncol=ny)
  matjg<-matrix(vecjg,nrow=nx,ncol=ny)
  
  assign("vecxg", vecxg, envir=globalenv())
  assign("vecig", vecig, envir=globalenv())
  assign("vecyg", vecyg, envir=globalenv())
  assign("vecjg", vecjg, envir=globalenv())
  assign("matxg", matxg, envir=globalenv())
  assign("matyg", matyg, envir=globalenv())
  assign("matig", matig, envir=globalenv())
  assign("matjg", matjg, envir=globalenv())
  
  if (maskit){
    ####
    # Mask for image.plot of matz
    # in matz, the map line k is in column k
    ####
    
    matmask<-matrix(F,nrow=ny,ncol=nx)
    matmask[1,1:18]<-T
    matmask[2,1:17]<-T
    matmask[3,1:16]<-T
    matmask[4:5,1:15]<-T
    matmask[6,c(1:5,12:13)]<-T
    matmask[7:10,1:6]<-T
    matmask[11,c(1:4,6)]<-T
    matmask[12,1:4]<-T
    matmask[13,1:3]<-T
    matmask[14:15,1:4]<-T
    matmask[16:17,1:5]<-T
    matmask[18:27,1:6]<-T
    matmask[28,1:5]<-T
    matmask[29,1:4]<-T
    matmask[30,1:5]<-T
    matmask[31,c(1:8,22:25,28:34)]<-T
    matmask[32,c(1:9,12:35)]<-T
    matmask[33,1:35]<-T
    matmask[34:41,1:36]<-T
    matmask[42,1:35]<-T
    matmask[43,1:34]<-T
    matmask[44,1:33]<-T
    matmask[45:46,1:32]<-T
    matmask[47,1:31]<-T
    matmask[48,1:28]<-T
    matmask[49,1:23]<-T
    matmask<-t(matmask)
    assign("matmask", matmask, envir=globalenv())
  }
  
  if (poligonit){
    #***************************
    # if TRUE, apply WGACEGG polygon
    #***************************
    # blocking_wgacegg.r
    # ICES WGACEGG 2013, July 
    # Definition of common grid
    # Definition of polygon for not retaining blocks outside polygon
    # Pierre Petitgas, IFREMER
    
    ####
    # GRID (that of WGACEGG08)
    ####
    # 
    nx<-length(seq(x1,x2,ax)); ny<-length(seq(y1,y2,ay))
    xg<-x1+ax/2+(0:nx)*ax; yg<-y1+ay/2+(0:ny)*ay
    #
    # Polygon
    #
    if (shelf){
      # if TRUE, remove cells outside of continental shelf (WGACEGG grid)
      jp=c(50,50,44,36,35,33,33,34,34,35,35,32,28,18,15,11,10,4,4,1,1,3,3,7,7,12,12,13,15,21,24,29,29,30,31,32,32,31,31,30,31,35,40,43,45,49,49,50,50,50)
      ip=c(24,20,20,30,32,32,24,24,16,16,9,3,3,3,1,1,4,4,13,15,19,19,18,15,7,7,6,5,5,8,7,7,6,6,9,9,12,12,31,31,36,37,38,38,35,32,30,29,25,24)
    }else{
      jp=c(50,50,44,42,34,34,35,35,32,28,18,15,11,10,4,4,1,1,3,3,7,7,12,12,13,15,21,24,29,29,30,31,32,32,31,31,30,31,35,40,43,45,49,49,50,50,50)
      ip=c(24,20,20,22,22,16,16,9,3,3,3,1,1,4,4,13,15,19,19,18,15,7,7,6,5,5,8,7,7,6,6,9,9,12,12,31,31,36,37,38,38,35,32,30,29,25,24)
    }
    xpol=xg[ip]; ypol=yg[jp]
    
    # this must be (re)done after polygon definition (??) to avoid NA in xpol
    # for AC_SPRING_IBBB surveys  
    xg<-xg[-length(xg)]; yg<-yg[-length(yg)] 
    # vecxg<-rep(xg,ny); vecig<-rep(1:nx,ny)
    # vecyg<-NULL; for (j in 1:ny) {vecyg<-c(vecyg,rep(yg[j],nx))}
    # vecjg<-NULL; for (j in 1:ny) {vecjg<-c(vecjg,rep(    j,nx))}
    # matxg<-matrix(vecxg,nrow=nx,ncol=ny)
    # matyg<-matrix(vecyg,nrow=nx,ncol=ny)
    # matig<-matrix(vecig,nrow=nx,ncol=ny)
    # matjg<-matrix(vecjg,nrow=nx,ncol=ny)
    # assign("vecxg", vecxg, envir=globalenv())
    # assign("vecig", vecig, envir=globalenv())
    # assign("vecyg", vecyg, envir=globalenv())
    # assign("vecjg", vecjg, envir=globalenv())
    # assign("matxg", matxg, envir=globalenv())
    # assign("matyg", matyg, envir=globalenv())
    # assign("matig", matig, envir=globalenv())
    # assign("matjg", matjg, envir=globalenv())

    assign("xpol", xpol, envir=globalenv())
    assign("ypol", ypol, envir=globalenv())
  }else{
    xpol=c(min(xg-a),max(xg+a),max(xg-a),min(xg+a))
    ypol=c(min(yg-a),min(yg-a),max(yg+a),max(yg+a))
    assign("xpol", xpol, envir=globalenv())
    assign("ypol", ypol, envir=globalenv())
  }
  #plot grid
  #   plot(matxg,matyg,main='Defined grid and polygon',xlab='',ylab='')
  #   #add polygon
  #   lines(xpol,ypol)
  #   plot(matig,matjg)
  #   lines(ip,jp)
  #   coast()
  #   identify(matxg,matyg,labels=matig)
  #   identify(matxg,matyg,labels=matjg)
}

#### WGACEGG CRR
# Function to:
#  Reads grid files
#  Plots the maps with colour palette of Miguel on screen
#  Saves the plots in jpeg files
# 

grid.plot=function(pat,input='gridFile',id=NULL,pat2=NULL,a=0.5,ux11=FALSE,
                   splot=c(.15,.19, .3,.7),namz=NULL,namn=NULL,nams=NULL,
                   yeari=NA,lon1=NULL,lon2=NULL,lat1=NULL,lat2=NULL,
                   resolution=4,newbathy=FALSE,default.grid=FALSE,lyears=NA,
                   deep=-450,shallow=-50,bstep=450,bcol='grey50',
                   drawlabels=FALSE,bathy.plot=TRUE,cex.labs=1,tcls=-0.5,
                   filenamei='id_year_type',shelf=FALSE,width=1000,
                   height=1000,units='px',res=72,mgps=c(3, 1, 0),
                   mars=c(3,3,4,1),epsit=FALSE,xlim=NULL,ylim=NULL,
                   plotit=list(
                     zmean=TRUE,zstd=TRUE,nz=TRUE,plot1=TRUE,mosaic=TRUE),
                   pcol=NULL,mapit=TRUE,dfraw=NULL,ggnrow=3,
                   mptitle=TRUE,...){
  
  #loads libraries and data
  data(gradient_colors) 
  
  #create missing folders
  if (!is.null(pat2)){
    if (!file.exists(paste(pat2,'meanMaps',sep=""))){dir.create(paste(pat2,'meanMaps',sep=""))}
    if (!file.exists(paste(pat2,'stdevMaps',sep=""))){dir.create(paste(pat2,'stdevMaps',sep=""))}
    if (!file.exists(paste(pat2,'NdataMaps',sep=""))){dir.create(paste(pat2,'NdataMaps',sep=""))}
  } 
  #check/create file name
  if (!is.null(pat2)&is.null(id)){
    if (class(pat)[1]=='character'){
      id0=strsplit(pat,split='[.]')
      N=length(id0[[1]])
      id1=strsplit(id0[[1]][N-1],split='/')
      id=id1[[1]][length(id1[[1]])]
    }else{
      id=paste(unique(pat$sp),namz,sep='_')
    }
  }
  
  if (input=='gridFile'){
    # read grid file
    mat=read.table(file=pat,header=T,sep=";")
  }else if (input=='Echobase'){
    mat=Echobase2grid(mat=pat)
  }else if (input=='gridDataframe'){
    mat=pat
    # !!!! order dataframe properly !!!
    mat=mat[order(mat$J,mat$I),]
  }
  
  # Check if a grid is defined
  # if (default.grid), define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
  if (default.grid&shelf){
    # Define grid limits
    x1<-(-10.2); x2<-(-1); y1<-35.8; y2<-48
    # Define cell dimensions
    ax=0.25;ay=0.25
    define.grid.poly.mask(x1,x2,y1,y2,ax,ay,shelf=shelf,poligonit = FALSE)
  }else if (default.grid&!shelf){
    # Define grid limits
    x1<-(-10.2); x2<-(-1); y1<-35.8; y2<-48
    # Define cell dimensions
    ax=0.25;ay=0.25
    define.grid.poly.mask(x1,x2,y1,y2,ax,ay,
                          shelf=shelf,
                          poligonit = FALSE)
  }else{
    cat('Grid definition based on input positions range','\n')
    # Define grid limits
    x1.0<-min(mat$Xgd); x2.0<-max(mat$Xgd)
    y1.0<-min(mat$Ygd); y2.0<-max(mat$Ygd)
    # Define cell dimensions
    ax=max(diff(mat$Xgd));ay=max(diff(mat$Ygd))
    if (!exists('x1')){
      # define.grid.poly.mask(x1.0,x2.0,y1.0,y2.0,ax,ay,
      #                       shelf=shelf,
      #                       poligonit = FALSE)
      x1=x1.0;x2=x2.0;y1=y1.0;y2=y2.0
    }else{
      if(x1>x1.0){x1=x1.0}else{x1=x1}
      if(y1>y1.0){y1=y1.0}else{y1=y1}
      if(x2>x2.0){x2=x2.0}else{x2=x2}
      if(y2>y2.0){y2=y2.0}else{y2=y2}
    }
  }  
  if (!is.null(lat1)) y1.0=lat1
  if (!is.null(lat2)) y2.0=lat2
  if (!is.null(lon1)) x1.0=lon1
  if (!is.null(lon2)) x2.0=lon2
  
  # convert Z, std and N values to numeric and define ranges
  head(mat)
  if (plotit$zmean){
    mat$Zvalue=as.numeric(mat$Zvalue)
    maxz=max(mat$Zvalue,na.rm=T)
    minz=min(mat$Zvalue,na.rm=T)
  }
  if (plotit$zstd){
    mat$Zstdev=as.numeric(mat$Zstdev)
    if (sum(!is.na(mat$Zstdev))>0){
      maxs=max(mat$Zstdev,na.rm=T)
      mins=min(mat$Zstdev,na.rm=T)
    }else{
      # case when std dev==NA
      maxs=NA
      mins=NA
    }
  }
  if (plotit$nz){
    mat$Nsample=as.numeric(mat$Nsample)
    maxn=max(mat$Nsample,na.rm=T)
    minn=min(mat$Nsample,na.rm=T)
  }
  if (!is.na(yeari)){
    mat=mat[mat$Year%in%yeari,]
  }  
  nI=max(mat$I); nJ=max(mat$J)
  year=sort(unique(mat$Year))
  xp=sort(unique(mat$Xg)); yp=sort(unique(mat$Yg))
  
  if(is.na(lyears[1])){
    lyears=year
  }
  
  if (plotit$zmean){
    if (plotit$plot1){
      # plot figures: mean values ----
      for (i in 1:length(lyears)) {
        
        aux=mat[mat$Year==lyears[i],]
        if (dim(aux)[1]==0){
          cat('No data in ',pat2,lyears[i],'\n')
          matz=matrix(rep(NA,nJ*nI),ncol=nJ,byrow=F)
        }else{
          matz=matrix(aux$Zvalue,ncol=nJ,byrow=F)
          if (!is.null(namz)){
            tit=paste(lyears[i],namz)
          }else if (is.null(namz)){
            tit=""
          }else{
            tit=lyears[i]
          }
          if (ux11) x11()
          par(bg="white",mar=mars)
          if (is.null(xlim)){
            xlim=c(x1-a,x2+a)
          }
          if (is.null(ylim)){
            ylim=c(y1-a,y2+2*a)
          }
          if (is.null(pcol)){
            pcol=gradient.colors
          }
          image(xp,yp,matz,xlab="",ylab="", main=tit,
                col=pcol, xlim=xlim,ylim=ylim, 
                zlim=c(minz,maxz),asp=1/cos((mean(c(y1,y2))*pi)/180),
                cex.axis=cex.labs,cex.lab=cex.labs,cex.main=cex.labs,
                tcl=tcls,mgp=mgps)
          # if bathy.plot==TRUE, add bathymetry
          if (bathy.plot){
            if (is.null(lon1)|is.null(lon2)|is.null(lat1)|is.null(lat2)){
              lon1=floor(min(xp));lon2=ceiling(max(xp));lat1=floor(min(yp))
              lat2=ceiling(max(yp))
            }
            MBathyPlot(lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution,newbathy=newbathy,add=TRUE,deep=deep,shallow=shallow,
                       bstep=bstep,bcol=bcol,drawlabels=drawlabels)
          }
          if (mapit){
            map(database="worldHires",xlim=c(x1-a,x2+a),ylim=c(y1-a,y2+2*a),
                col=gray(.8),fill=T,bg="white",add=T)
          }
          box()    #trace un cadre autour de la carte
          # Add raw data points, optional
          if (!is.null(dfraw)){
            dfraws=dfraw[dfraw$year==lyears[i],]
            points(dfraw$LONG,dfraw$LAT,col='grey50')
          }
          #map.axes()    #trace des axes en bas et gauche
          #image.plot(matz)
          # add legend
          if (minz==maxz){
            image.plot(xp,yp,matz,xlab="",ylab="", main=tit,
                       col=pcol, xlim=c(x1-a,x2+a),ylim=c(y1-a,y2+2*a),
                       asp=1/cos((mean(c(y1,y2))*pi)/180),legend.only=T,
                       smallplot=splot,axis.args = list(cex.axis = cex.labs))
          }else{
            image.plot(xp,yp,matz,xlab="",ylab="", main=tit,
                       col=pcol, xlim=c(x1-a,x2+a),ylim=c(y1-a,y2+2*a), 
                       zlim=c(minz,maxz),asp=1/cos((mean(c(y1,y2))*pi)/180),
                       legend.only=T,smallplot=splot,
                       axis.args = list(cex.axis = cex.labs))
          }
          if (!is.null(pat2)&ux11){
            if (filenamei=='Coser'){
              if ('mission'%in%names(mat)){
                mission=unique(mat$mission)
              }else{
                n1=grep(FALSE,is.letters(unique(mat$Survey)))[1]
                mission=unique(substr(unique(mat$Survey),1,(n1-1)))
              }
              species=gsub('-','',unique(pat$sp))
              filei=paste(
                pat2,'meanMaps/',paste(mission,species,sep='_'),".png",sep="")
            }else{
              filei=paste(pat2,'meanMaps/',paste(id,lyears[i],'mean',sep='_'),
                          ".png",sep="")        
            }
            dev.print(
              device=png,file=filei, width=width, height=height,bg = "white",
              res=res,units=units)
            cat('Plot saved as:',filei,'\n')
            dev.off()
            if (epsit){
              filei=paste(pat2,'meanMaps/',paste(id,lyears[i],'mean',sep='_'),
                          ".eps",sep="")
              dev.copy2eps(file=filei)
              
              if (ux11) x11()
              par(bg="white",mar=mars)
              if (is.null(xlim)){
                xlim=c(x1.0-a,x2.0+a)
              }
              if (is.null(ylim)){
                ylim=c(y1.0-a,y2.0+2*a)
              }
              if (is.null(pcol)){
                pcol=gradient.colors
              }
              image(xp,yp,matz,xlab="",ylab="", main=tit,
                    col=pcol, xlim=xlim,ylim=ylim, 
                    zlim=c(minz,maxz),asp=1/cos((mean(c(y1.0,y2.0))*pi)/180),
                    cex.axis=cex.labs,cex.lab=cex.labs,cex.main=cex.labs,
                    tcl=tcls,mgp=mgps)
              # if bathy.plot==TRUE, add bathymetry
              if (bathy.plot){
                if (is.null(lon1)|is.null(lon2)|is.null(lat1)|is.null(lat2)){
                  lon1=floor(min(xp));lon2=ceiling(max(xp));lat1=floor(min(yp))
                  lat2=ceiling(max(yp))
                }
                MBathyPlot(lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution,newbathy=newbathy,add=TRUE,deep=deep,shallow=shallow,
                           bstep=bstep,bcol=bcol,drawlabels=drawlabels)
              }
              if (mapit){
                map(database="worldHires",xlim=c(x1.0-a,x2.0+a),ylim=c(y1.0-a,y2.0+2*a),
                    col=gray(.8),fill=T,bg="white",add=T)
              }
              box()    #trace un cadre autour de la carte
              # Add raw data points, optional
              if (!is.null(dfraw)){
                dfraws=dfraw[dfraw$year==lyears[i],]
                points(dfraw$LONG,dfraw$LAT,col='grey50')
              }
              #map.axes()    #trace des axes en bas et gauche
              #image.plot(matz)
              # add legend
              if (minz==maxz){
                image.plot(xp,yp,matz,xlab="",ylab="", main=tit,
                           col=pcol, xlim=c(x1.0-a,x2.0+a),ylim=c(y1.0-a,y2.0+2*a),
                           asp=1/cos((mean(c(y1.0,y2.0))*pi)/180),legend.only=T,
                           smallplot=splot,axis.args = list(cex.axis = cex.labs))
              }else{
                image.plot(xp,yp,matz,xlab="",ylab="", main=tit,
                           col=pcol, xlim=c(x1.0-a,x2.0+a),ylim=c(y1.0-a,y2.0+2*a), 
                           zlim=c(minz,maxz),asp=1/cos((mean(c(y1.0,y2.0))*pi)/180),
                           legend.only=T,smallplot=splot,
                           axis.args = list(cex.axis = cex.labs))
              }
              if (!is.null(pat2)&ux11){
                if (filenamei=='Coser'){
                  if ('mission'%in%names(mat)){
                    mission=unique(mat$mission)
                  }else{
                    n1=grep(FALSE,is.letters(unique(mat$Survey)))[1]
                    mission=unique(substr(unique(mat$Survey),1,(n1-1)))
                  }
                  species=gsub('-','',unique(pat$sp))
                  filei=paste(
                    pat2,'meanMaps/',paste(mission,species,sep='_'),".png",sep="")
                }else{
                  filei=paste(pat2,'meanMaps/',paste(id,lyears[i],'mean',sep='_'),
                              ".png",sep="")        
                }
                dev.print(
                  device=png,file=filei, width=width, height=height,bg = "white",
                  res=res,units=units)
                cat('Plot saved as:',filei,'\n')
                if (epsit){
                  filei=paste(pat2,'meanMaps/',paste(id,lyears[i],'mean',sep='_'),
                              ".eps",sep="")
                  dev.copy2eps(file=filei)
                  cat('Plot saved as:',filei,'\n')
                }
                dev.off()
              }else if (!is.null(pat2)&!ux11){
                cat('Set ux11 to TRUE if you want to save the plots as images','\n')
              }
              dev.off()
            }
          }
        }
    }
 
  }
    if (plotit$mosaic){
      if (mptitle){
        ptitle=paste(namz,'mean')
      }else{
        ptitle=NULL
      }
      gmdf.msd.meanZ=grid.ggplot(gmdf=mat,path.grids1=pat2,ux11=ux11,
                                 zvar='Zvalue',
                                 ptitle=ptitle,ggnrow = ggnrow)
    }
  }
  if (plotit$zstd){
    if (plotit$plot1){
      # plot figures: SD maps -----
      for (i in 1:length(lyears)) {
        
        aux=mat[mat$Year==lyears[i],]
        if (dim(aux)[1]==0){
          mats=matrix(rep(NA,nJ*nI),ncol=nJ,byrow=F)
        }else{
          mats=matrix(aux$Zstdev,ncol=nJ,byrow=F)
          if (!is.null(nams)){
            tit=paste(lyears[i],nams)
          }else if (is.null(nams)){
            tit=""
          }else{
            tit=lyears[i]
          }
          
          if (sum(!is.na(mats))!=0){
            if (ux11) x11()
            par(bg="white",mar=mars)
            if (is.null(xlim)){
              xlim=c(x1.0-a,x2.0+a)
            }
            if (is.null(ylim)){
              ylim=c(y1.0-a,y2.0+2*a)
            }
            if (is.null(pcol)){
              pcol=gradient.colors
            }
            image(xp,yp,mats,xlab="",ylab="", main=tit,
                  col=pcol, xlim=xlim,ylim=ylim,zlim=c(mins,maxs),
                  asp=1/cos((mean(c(y1.0,y2.0))*pi)/180),add=F,cex.axis=cex.labs,
                  cex.lab=cex.labs,cex.main=cex.labs,tcl=tcls,mgp=mgps) 
            # if bathy.plot==TRUE, add bathymetry
            if (bathy.plot){
              if (is.null(lon1)|is.null(lon2)|is.null(lat1)|is.null(lat2)){
                lon1=floor(min(xp));lon2=ceiling(max(xp));lat1=floor(min(yp));lat2=ceiling(max(yp))
              }
              MBathyPlot(lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution,newbathy=newbathy,add=TRUE,deep=deep,shallow=shallow,
                         bstep=bstep,bcol=bcol,drawlabels=drawlabels)
            }
            if (mapit){
              map(database="worldHires",xlim=c(x1.0-a,x2.0+a),ylim=c(y1.0-a,y2.0+2*a),col=gray(.8),fill=T,bg="white",add=T)
            }
            box()    #trace un cadre autour de la carte
            # Add raw data points, optional
            if (!is.null(dfraw)){
              dfraws=dfraw[dfraw$year==lyears[i],]
              points(dfraw$LONG,dfraw$LAT,col='grey50')
            }
            #map.axes()    #trace des axes en bas et ? gauche
            #re-add legend on top of land
            if (mins==maxs){
              image.plot(xp,yp,mats,xlab="",ylab="", main=tit,
                         col=gradient.colors, xlim=c(x1.0-a,x2.0+a),ylim=c(y1.0-a,y2.0+2*a),
                         asp=1/cos((mean(c(y1.0,y2.0))*pi)/180),legend.only=T,smallplot=splot,
                         axis.args = list(cex.axis = cex.labs))
            }else{
              image.plot(xp,yp,mats,xlab="",ylab="", main=tit,
                         col=gradient.colors, xlim=c(x1.0-a,x2.0+a),ylim=c(y1.0-a,y2.0+2*a), zlim=c(mins,maxs),
                         asp=1/cos((mean(c(y1.0,y2.0))*pi)/180),add=T,legend.only=T,smallplot=splot,
                         axis.args = list(cex.axis = cex.labs))
            }
            if (!is.null(pat2)&ux11){
              if (filenamei=='Coser'){
                if ('mission'%in%names(mat)){
                  mission=unique(mat$mission)
                }else{
                  n1=grep(FALSE,is.letters(unique(mat$Survey)))[1]
                  mission=unique(substr(unique(mat$Survey),1,(n1-1)))
                }
                species=gsub('-','',unique(pat$sp))
                filei=paste(pat2,'stdevMaps/',paste(mission,species,sep='_'),".png",sep="")
              }else{
                filei=paste(pat2,'stdevMaps/',paste(id,lyears[i],'stdev',sep='_'),".png",sep="")        
              }
              dev.print(device=png,file=filei, width=width, height=height,bg = "white",res=res,units=units)
              cat('Plot saved as:',filei,'\n')
              if (epsit){
                filei=paste(pat2,'stdevMaps/',paste(id,lyears[i],'stdev',sep='_'),".eps",sep="")   
                dev.copy2eps(file=filei)
                cat('Plot saved as:',filei,'\n')
              }
              dev.off()
            }else if (!is.null(pat2)&!ux11){
              cat('Set ux11 to TRUE if you want to save the plots as images','\n')
            }
          }
        }
      }
    }    
    if (plotit$mosaic){
      if (mptitle){
        ptitle=paste(namz,'stdev')
      }else{
        ptitle=NULL
      }
      gmdf.msd.stdZ=grid.ggplot(gmdf=mat,path.grids1=pat2,ux11=ux11,
                                 zvar='Zstdev',
                                ptitle=ptitle,ggnrow = ggnrow)
    }
  }
  if (plotit$nz){
    if (plotit$plot1){
      # plot figures: nb obs -------------
      for (i in 1:length(lyears)) {
        
        aux=mat[mat$Year==lyears[i],]
        if (dim(aux)[1]==0){
          matn=matrix(rep(NA,nJ*nI),ncol=nJ,byrow=F)
        }else{
          matn=matrix(aux$Nsample,ncol=nJ,byrow=F)
          if (!is.null(namn)){
            tit=paste(lyears[i],namn)
          }else if (is.null(namn)){
            tit=""
          }else{
            tit=lyears[i]
          }
          
          if (ux11) x11()
          par(bg="white",mar=mars)
          if (is.null(xlim)){
            xlim=c(x1.0-a,x2.0+a)
          }
          if (is.null(ylim)){
            ylim=c(y1.0-a,y2.0+2*a)
          }
          if (is.null(pcol)){
            pcol=gradient.colors
          }
          image(xp,yp,matn,xlab="",ylab="", main=tit,
                col=pcol, xlim=xlim,ylim=ylim, zlim=c(minn,maxn),
                asp=1/cos((mean(c(y1.0,y2.0))*pi)/180),add=F,
                cex.axis=cex.labs,
                cex.lab=cex.labs,cex.main=cex.labs,tcl=tcls,mgp=mgps) 
          # if bathy.plot==TRUE, add bathymetry
          if (bathy.plot){
            if (is.null(lon1)|is.null(lon2)|is.null(lat1)|is.null(lat2)){
              lon1=floor(min(xp));lon2=ceiling(max(xp));lat1=floor(min(yp));lat2=ceiling(max(yp))
            }
            MBathyPlot(lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution,newbathy=newbathy,add=TRUE,deep=deep,shallow=shallow,
                       bstep=bstep,bcol=bcol,drawlabels=drawlabels)
          }
          if (mapit){
            map(database="worldHires",xlim=c(x1.0-a,x2.0+a),ylim=c(y1.0-a,y2.0+2*a),col=gray(.8),fill=T,bg="white",add=T)
          }
          box()    #trace un cadre autour de la carte
          # Add raw data points, optional
          if (!is.null(dfraw)){
            dfraws=dfraw[dfraw$year==lyears[i],]
            points(dfraw$LONG,dfraw$LAT,col='grey50')
          }
          #map.axes()    #trace des axes en bas et ? gauche
          #re-add legend on top of land
          if (minn==maxn){
            image.plot(xp,yp,matn,xlab="",ylab="", main=tit,
                       col=gradient.colors, xlim=c(x1-a,x2+a),
                       ylim=c(y1-a,y2+2*a),
                       asp=1/cos((mean(c(y1,y2))*pi)/180),legend.only=T,
                       smallplot=splot,
                       axis.args = list(cex.axis = cex.labs))
          }else{
            image.plot(xp,yp,matn,xlab="",ylab="", main=tit,
                       col=gradient.colors, xlim=c(x1-a,x2+a),
                       ylim=c(y1-a,y2+2*a), zlim=c(minn,maxn),
                       asp=1/cos((mean(c(y1,y2))*pi)/180),add=T,
                       legend.only=T,smallplot=splot,
                       axis.args = list(cex.axis = cex.labs))
          }
          if (!is.null(pat2)&ux11){
            if (filenamei=='Coser'){
              if ('mission'%in%names(mat)){
                mission=unique(mat$mission)
              }else{
                n1=grep(FALSE,is.letters(unique(mat$Survey)))[1]
                mission=unique(substr(unique(mat$Survey),1,(n1-1)))
              }
              species=gsub('-','',unique(pat$sp))
              filei=paste(pat2,'NdataMaps/',paste(mission,species,sep='_'),".png",sep="")
            }else{
              filei=paste(pat2,'NdataMaps/',paste(id,lyears[i],'Ndata',sep='_'),".png",sep="")        
            }
            dev.print(device=png,file=filei, width=width, height=height,bg = "white",res=res,units=units)
            cat('Plot saved as:',filei,'\n')
            if (epsit){
              filei=paste(pat2,'NdataMaps/',paste(id,lyears[i],'Ndata',sep='_'),".eps",sep="") 
              dev.copy2eps(file=filei)
              cat('Plot saved as:',filei,'\n')
            }
            dev.off()
          }else if (!is.null(pat2)&!ux11){
            cat('Set ux11 to TRUE if you want to save the plots as images','\n')
          }
        }
      }
    }
    if (plotit$mosaic){
      if (mptitle){
        ptitle=paste(namz,'Nsample')
      }else{
        ptitle=NULL
      }
      gmdf.msd.Nsamples=grid.ggplot(gmdf=mat,path.grids1=pat2,ux11=ux11,
                                    zvar='Nsample',
                                    ptitle=ptitle,
                                    ggnrow = ggnrow)
    }
  }
  if(!exists("gmdf.msd.meanZ")){
    gmdf.msd.meanZ=NULL
  }
  if(!exists("gmdf.msd.stdZ")){
    gmdf.msd.stdZ=NULL
  }
  if(!exists("gmdf.msd.Nsamples")){
    gmdf.msd.Nsamples=NULL
  }
list(mat=mat,gmdf.msd.meanZ=gmdf.msd.meanZ,gmdf.msd.stdZ=gmdf.msd.stdZ,
       gmdf.msd.Nsamples=gmdf.msd.Nsamples)}

grid.ggplot=function(gmdf,path.grids1=NULL,ux11=TRUE,zvar='Zvalue',
                     ptitle=NULL,ggnrow=3){
  library(ggplot2)
  if (!'sp'%in%names(gmdf)){
    gmdf$sp='Dummy'
  }
  # Add mean and SD maps ------
  lz=c('Xsample','Ysample','DateAver','DateStdev',
       'TimeAver','TimeStdev','Nsample','Zvalue','Zstdev')
  lzs=lz[lz%in%names(gmdf)]
  gmdf.am=aggregate(gmdf[,lzs],
    list(Survey=gmdf$Survey,sp=gmdf$sp,I=gmdf$I,J=gmdf$J,
         Xgd=gmdf$Xgd,Ygd=gmdf$Ygd),mean,na.rm=TRUE)
  names(gmdf.am)[7:dim(gmdf.am)[2]]=lzs
  gmdf.m.name=paste('mean.',min(gmdf$Year),'-',max(gmdf$Year),sep='')
  gmdf.am$Year=gmdf.m.name
  names(gmdf.am)
  gmdf.asd=aggregate(gmdf[,lzs],
    list(Survey=gmdf$Survey,sp=gmdf$sp,I=gmdf$I,
         J=gmdf$J,Xgd=gmdf$Xgd,Ygd=gmdf$Ygd),sd,na.rm=TRUE)
  names(gmdf.asd)[7:dim(gmdf.asd)[2]]=lzs
  gmdf.sd.name=paste('SD.',min(gmdf$Year),'-',max(gmdf$Year),sep='')
  gmdf.asd$Year=gmdf.sd.name
  lnames=c(c("sp","Year","I","J","Survey","Xgd","Ygd"),lzs)
  gmdf.msd=rbind(gmdf[,lnames],gmdf.am[,lnames],gmdf.asd[,lnames])
  unique(gmdf.msd$Year)
  gmdf.msd$Xgd=as.numeric(gmdf.msd$Xgd)
  gmdf.msd$Ygd=as.numeric(gmdf.msd$Ygd)
  head(gmdf.msd)
  gmdf.lsp=unique(gmdf.msd$sp)
  gmdf.lyears=unique(gmdf$Year)

  # Mosaic plots ------------
  # Mosaic plot with all years (plots saved in path.grids1)
  for (i in 1:length(gmdf.lsp)){
    gmdfi=gmdf.msd[
      gmdf.msd$sp==gmdf.lsp[i],]
    gmdfi$z=gmdfi[,zvar]
    if(is.null(ptitle)){
      ptitlei=gmdf.lsp[i]
    }else{
      ptitlei=paste(ptitle,gmdf.lsp[i])
    }
    if (ux11) x11()
    par(bg='white')
    p1<-ggplot() +
      geom_raster(data = gmdfi, 
                  aes(x = Xgd, y = Ygd, fill = z))+
      labs(title = ptitlei) +
      theme(axis.title.x = element_blank(),
            axis.title.y = element_blank())+
      annotation_map(map_data("world"))+ #Add the map as a base layer before the points
      coord_quickmap()+  #Sets aspect ratio
      scale_fill_gradientn(colours=gradient.colors)+
      facet_wrap(.~factor(Year),nrow = ggnrow)
    print(p1)
    if ((!is.null(path.grids1))&ux11){
      # Save figure
      dev.print(png, file=paste(path.grids1,'gridmapMosaic_',
                                zvar,'_',gmdf.lsp[i],'_',min(gmdf$Year),'-',
                                max(gmdf$Year),".png",sep=""), 
                units='cm',width=40, height=30,res=300)
      print(paste('Mosaic ggplot saved as:',
                  paste(path.grids1,'gridmapMosaic_',
                        zvar,'_',gmdf.lsp[i],'_',min(gmdf$Year),'-',
                        max(gmdf$Year),".png",sep="")))
      dev.off()
    }else if (!is.null(path.grids1)&!ux11){
      cat('Set ux11 to TRUE and provide an output path if you want to save the plots as images','\n')
    }
  }
list(mat=gmdf,gmdf.msd=gmdf.msd)
}

gridNplot=function(df,vart,varname,path.grids,spname='CodEsp',
                   tname='year',xname='LONG',yname='LAT',
                   sname='CAMPAGNE',zfilter='none',eps=1e-4,
                   p=0.98,discrete=FALSE,lon1=NULL,lon2=NULL,
                   lat1=NULL,lat2=NULL,resolution=4,
                   newbathy=FALSE,bathy.plot=TRUE,deep=NULL,
                   shallow=NULL,bstep=20,bcol='grey50',
                   drawlabels=FALSE,grid.plotit=TRUE,
                   onefilet=FALSE,corNA=TRUE,addRaw=FALSE,
                   xycor=FALSE,centerit=FALSE,mini=-1,maxi=1,
                   selPolygon=NULL,wname=NULL,...){
  
  #loads libraries and data
  library(splancs)
  
  #set paths and create dataframe
  wd0=getwd()
  setwd(path.grids)   
  lspcodes=unique(df[,spname])
  if (is.null(wname)){
    df=df[,c(sname,tname,yname,xname,vart,spname)]
    names(df)=c('CAMPAGNE','year','LAT','LONG',vart,spname)
  }else{
    df=df[,c(sname,tname,yname,xname,vart,spname,wname)]
    names(df)=c('CAMPAGNE','year','LAT','LONG',vart,spname,wname)    
  }
  # Select only cells in selPolygon
  if (!is.null(selPolygon)){
    # Remove cells outside polygon defined by selPolygon
    sel=splancs::inout(pts=cbind(df$LONG,df$LAT),poly=selPolygon,
              bound=T,quiet=T)
    if (grid.plotit){
      plot(df$LONG,df$LAT)
      points(dfs$LONG,dfs$LAT,pch=16,col=2)
      lines(selPolygon$x,selPolygon$y)
    }
    dfs=df[sel,]
    df[!sel,vart]=NA
    df[sel&(df[,vart]<0|is.na(df[,vart])),vart]=0
    corNA=FALSE
    dfs=df[sel,]
    #plot(dfs$LONG,dfs$LAT,cex=0.1+log(dfs[,vart]+1)/10)
  }
  
  for (j in 1:length(lspcodes)){
    cspi=as.character(lspcodes[j])
    #cat('Processing',as.character(cspi),'\n')
    dat1=df[df[,spname]==cspi,]
    
    #set negative values to zero and warns
    dat1.pb=dat1[(dat1[,vart]<0)|(is.na(dat1[,vart])),]
    if (dim(dat1.pb)[1]>0&corNA&!discrete){
      ulabs=unique(dat1.pb[,c('CAMPAGNE',spname,'year')])
      cat('Negative or NA',vart,'in',as.character(ulabs[,'CAMPAGNE']),
          as.character(ulabs[,spname]),
          as.character(ulabs[,'year']),'set to zero','\n')
      dat1[(dat1[,vart]<0)|(is.na(dat1[,vart])),vart]=0
    }    
    #str(dat1)
    if (mean(dat1[,vart],na.rm=TRUE)!=0|discrete){
      ### Define response variable: must be numeric
      if (!discrete){
        response_variable <- as.double(dat1[,vart])
        if (!is.null(wname)){
          w=as.double(dat1[,wname])
        }else{
          w=NULL
        }
      }else{
        response_variable <- dat1[,vart]
        w=NULL
      }
      ficname <- cspi 
      
      ### Create a vector with year levels
      yearsi=unlist(strsplit(as.character(dat1[,'year']),
                             split='-'))[seq(1,2*length(dat1[,'year']),2)]
      #yea1 <- as.double(dat1[,'year'])
      yea1 <- as.double(yearsi)
      if (length(yea1)==0) stop ('No time column in dataset')
      #
      year <- unique(yea1)[order(unique(yea1))]
      ltimes <- unique(dat1[,'year'])[order(unique(dat1[,'year']))]
      
      # Select the columns in the following sequence : 
      # Campaign, Year, Latitude, Longitude, Response Variable
      select_columns <- c('CAMPAGNE','year','LAT','LONG',vart)
      
      # quantile choice
      if (zfilter=='quantile'){
        if (grid.plotit){
          x11()
          par(bg='white')
          hist(response_variable[response_variable>0],
               breaks=seq(0,ceiling(max(response_variable,na.rm=TRUE)),
                          length.out=50),
               las=1,main=ficname,xlab=varname)
          abline(v=find_saturation(response_variable,my_quantile=0.90),
                 col="green")
          abline(v=find_saturation(response_variable,my_quantile=0.95),
                 col="red")
          abline(v=find_saturation(response_variable,my_quantile=0.98),
                 col="blue")
          dev.print(
            device=png,file=paste(path.grids,'/',cspi,"-quantilePlot.png",
                                  sep=""), width=1000, height=1000,
            bg = "white")
          cat('Plot saved as:',
              paste(path.grids,'/',cspi,"-quantilePlot.png",sep=""),
              '\n')
          dev.off()
        }
        saturation <- find_saturation(response_variable,my_quantile=p)
      }else if (zfilter=='cumFreq'){
        cat(as.character(cspi),'max',varname,'definition','\n')
        saturation=cumFreq.filter(
          z=response_variable,varname=varname,spi=cspi,group=yea1,
          path.grids=path.grids,p=p,eps=eps,plotit=grid.plotit)
      }else if (zfilter=='none'&!discrete){
        saturation=max(response_variable,na.rm=TRUE)
      }
      #plot max values per year distribution
      if (zfilter!='none'){
        qsaturation <- find_saturation(response_variable,my_quantile=p)
        tcff=try(cumFreq.filter(z=response_variable,varname=varname,
                                spi=cspi,group=yea1,
                                path.grids=path.grids,p=p,eps=eps,
                                plotit=grid.plotit),TRUE)
        if (class(tcff)=="try-error") {
          fsaturation=NA
        }else{
          fsaturation=cumFreq.filter(
            z=response_variable,varname=varname,spi=cspi,group=yea1,
                                     path.grids=path.grids,p=p,eps=eps,
            plotit=grid.plotit)
        }
      }else{
        if (!discrete){
          fsaturation=max(response_variable,na.rm=TRUE)
        }else{
          fsaturation=NA
        }  
      }
      
      if (zfilter=='cumFreq') {zmax2lab='quantile';zmax2=qsaturation
      }else if (zfilter=='quantile') {zmax2lab='cumFreq';zmax2=fsaturation
      }else if (zfilter=='none'&!discrete) {zmax2lab='cumFreq';zmax2=fsaturation}      
      if (!discrete){
        if (grid.plotit){
          histMaxvalues(z=response_variable,varname=varname,spi=cspi,
                        group=yea1,path.grids=path.grids,Bmax=Bmax,
                        zmax=saturation,
                        zmax2=zmax2,zmaxlab=zfilter,zmax2lab=zmax2lab)
        }
      }
      cat('Thresholding:',zfilter,'\n')
      if (!discrete){
        cat(as.character(cspi),'max',varname,'threshold',':',saturation,'\n')
        saturations=data.frame(
          sp=cspi,period=paste(as.character(min(year)),
                               as.character(max(year)),sep='-'),
          saturation=as.character(saturation),method=zfilter)
      }else{
        saturation=NA
      }

      ### Blocking
      # Path and titles definition
      if (zfilter=='quantile'){
        filterid=paste("q_",substr(paste(p),3,4),sep='')
      }else if (zfilter=='none'){
        filterid=zfilter
      }else if (zfilter=='cumFreq'){
        filterid=zfilter
      }
      pat=paste(getwd(),"/","grid_",cspi,"_",varname,"_Filter",filterid,".txt",sep="")
      pat2=paste(path.grids,'/',sep='')
      
      #for (y in ltimes){
      for (y in 1:length(ltimes)){  
        ### Make Blocking
        writeLines(paste("Processing",ficname,ltimes[y],sep=" "))
        make_blocking(my_data=dat1[dat1$year==ltimes[y],],
                      select=select_columns,
                      var_name=varname,
                      tab_name=ficname,
                      saturation=saturation, # high values are truncated
                      append_file=ifelse(y==1,FALSE,TRUE),
                      p=p,
                      discrete=discrete,zfilter=zfilter,
                      xycor=xycor,centerit=centerit,mini=mini,
                      maxi=maxi,w=w)
        if (onefilet){
          filname=paste(path.grids,"/",paste("grid_",ficname,"_",varname,"_Filter",filterid,".txt",sep=""),sep="")
          filname2=paste(path.grids,"/",paste("grid_",ficname,"_",varname,"_",ltimes[y],"_Filter",filterid,".txt",sep=""),sep="")
          system(paste('cp',filname,filname2))
        }
      }
      
      zsum.year=NULL
      mats=NULL
      
      for (y in 1:length(ltimes)){
        # 8.3.3. Plot gridded data and check annual max values
        ##***************************
        #Set plot titles
        cruise=unique(dat1[yea1==ltimes[y],'CAMPAGNE'])
        namz=paste('mean',varname,cspi)
        nams=paste('stdev',varname,cspi)
        namn=paste('No. of data',cspi)
        #Plot gridded data
        if (grid.plotit){
          cat('Plotting...','\n')
          if (addRaw){
            dfraw=dat1
          }else{
            dfraw=NULL
          }
          mat=grid.plot(pat=pat,pat2=pat2,a=0,ux11=TRUE,namz=namz,
                        namn=namn,nams=nams,yeari=ltimes[y],
                        lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,
                        resolution=resolution,newbathy=newbathy,
                        deep=deep,shallow=shallow,bstep=bstep,bcol=bcol,
                        drawlabels=drawlabels,
                        bathy.plot=bathy.plot,dfraw=dfraw,
                        plotit=list(zmean=TRUE,zstd=TRUE,nz=TRUE,
                                    plot1=TRUE,
                                    mosaic=FALSE),...)
        }  
        #Check annual z distributions
        mat=read.table(file=pat,header=T,sep=";")
        if (grid.plotit) plot(mat$Xgd,mat$Ygd,col=is.na(mat$Zvalue))
        mati=mat[mat$Year==ltimes[y],]
        mati$Zvalue=as.numeric(mati$Zvalue)
        sumz=t(as.matrix(summary(mati$Zvalue,na.rm=T)));sumz=cbind(sumz,var=var(mati$Zvalue,na.rm=TRUE))
        sumN=t(as.matrix(summary(mati$Nsample,na.rm=T)));sumN=cbind(sumN,var=var(mati$Nsample,na.rm=TRUE))
        head(mati)
        zsum=data.frame(sp=cspi,year=ltimes[y],sumz)
        mat$sp=cspi
        # Keep all years
        if (dim(zsum)[1]>0){
          zsum.year=plyr::rbind.fill(zsum.year,zsum)
        }
        mats=rbind(mats,mat)
        if (grid.plotit){
          x11()
          par(bg='white')
          boxplot(zsum.year$Max.,main=paste(cspi,'distribution of gridded',varname,'annual maxima'))
          dev.print(device=png,file=paste(path.grids,'/',cspi,"-zmaxAnnualDistribution.png",sep=""), width=1000, height=1000,
                    bg = "white")
          cat('Plot saved as:',paste(path.grids,'/',cspi,"-zmaxAnnualDistribution.png",sep=""),'\n')
          dev.off()
        }
      }
    }else {
      cat (as.character(cspi),'has null biomass','\n')
      saturations=data.frame(sp=cspi,
                             period=paste(
                               as.character(min(year)),as.character(max(year)),
                               sep='-'),saturation=0,method=zfilter)
      zsum.yeari=data.frame(sp=cspi,year=NA,Min.=NA,X1st.Qu.=NA,Median=NA,
                            Mean=NA,X3rd.Qu.=NA,Max.=NA,NA.s=NA,var=NA)
      zsum.year=zsum.yeari[FALSE,]
    }
    
    if (j==1){
      saturations.db=saturations
      zsum.year.db=zsum.year
      matall=mats
    }else{
      saturations.db=rbind(saturations.db,saturations)
      zsum.year.db=plyr::rbind.fill(zsum.year.db,zsum.year)
      matall=rbind(matall,mats)      
    }  
  }
  setwd(wd0)   
  list(saturations.db=saturations.db,zsum.year.db=zsum.year.db,
       gridDataframe=unique(matall))
}

cumFreq.filter=function(z,varname=NULL,group=NULL,spi=NULL,method='auto',
                        eps=1e-4,path.grids=NULL,Bmax=952,p=.98,plotit=TRUE){
  
  lag=(max(z)-min(z))/100
  hz=hist(z,breaks=seq(0,max(z)+lag,lag),plot=FALSE)
  summary(z)
  fq=data.frame(mids=hz$mids,fqz=cumsum(hz$counts/sum(hz$counts)))
  fq$tac=c(100*(fq$fqz[2:(length(fq$fqz))]-fq$fqz[1:(length(fq$fqz)-1)])/fq$fqz[1:(length(fq$fqz)-1)],NA)
  fq$dfq=c((fq$fqz[2:(length(fq$fqz))]-fq$fqz[1:(length(fq$fqz)-1)]),NA)
  zmax=fq[fq$dfq<eps,'mids'][1]
  pq=quantile(z,p)
  old.par <- par(no.readonly = TRUE)
  if (plotit){
    x11()
    par(mfrow=c(2,1),mar=c(4,4,1,1),bg='white')
    plot(hz$mids,cumsum(hz$counts/sum(hz$counts)),xlab=varname,ylab=paste('Cumulative frequency'))
    abline(v=Bmax,lwd=2,col=2);abline(v=zmax,lwd=2,col=3);abline(v=pq,lwd=2,col=4)
    legend('bottomright',legend=c('Theoretical max','Extreme values threshold',paste('Quantile',p)),lty=1,col=c(2,3,4),bg='white')
    plot(fq$mids,fq$tac,xlab=varname,ylab=paste('Cum. freq. increasing rate (%)'))
    abline(v=Bmax,lwd=2,col=2);abline(v=zmax,lwd=2,col=3);abline(v=pq,lwd=2,col=4)
    legend('topright',legend=c('Theoretical max','Extreme values threshold',paste('Quantile',p)),lty=1,col=c(2,3,4),bg='white')
    if (!is.null(path.grids)){
      dev.print(device=png,file=paste(path.grids,'/',spi,"-zCumFreq.png",sep=""), width=800, height=800,
                bg = "white")
      cat('Plot saved as:',paste(path.grids,'/',spi,"-zCumFreq.png",sep=""),'\n')
    }
    if (method!='auto'){
      cat('Please click in the plot area to define the maximum value to be used','\n')
      loc=locator(n = 1, type = "p")
      zmax=loc$x
    }
    dev.off()
    #Check
    histMaxvalues(z,varname,spi,group,path.grids,Bmax,zmax,zmax2=pq,
                  zmaxlab='cumFreq threshold',zmax2lab='quantile threshold')
  }
  zmax
}

#Function to plot 2 fish populations distributions on 2 neighbouring graphs, together with their center of gravity and inertia axis 

plot2pop=function(FishData1,FishData2,title1=NULL,title2=NULL){
  library(SpatialIndices)
  x11(14,7)
  layout(matrix(c(1,2),1,2,byrow=TRUE))
  symbols(FishData1$LONG, FishData1$LAT, sqrt(FishData1$BB), inches=0.25, fg=2, asp=1/cos((mean(FishData1$LAT)*pi)/180), 
          xlab="Longitude", ylab="Latitude", main=title1)
  lines(poly,col=8)
  polygon(map,col=8);box()
  cgi(x=FishData1$LONG, y=FishData1$LAT, z=FishData1$BB, w=FishData2$surf, modproj="mean", mlong=mean(FishData1$LONG), mlat=mean(FishData1$LAT), col=4, plot=TRUE)
  symbols(FishData2$LONG, FishData2$LAT, sqrt(FishData2$BB), inches=0.25, fg=2, asp=1/cos((mean(FishData2$LAT)*pi)/180), 
          xlab="Longitude", ylab="Latitude", main=title2)
  lines(poly,col=8)
  polygon(map,col=8);box()
  cgi(x=FishData2$LONG, y=FishData2$LAT, z=FishData2$BB, w=FishData2$surf, modproj="mean", mlong=mean(FishData2$LONG), mlat=mean(FishData2$LAT), col=4, plot=TRUE)
}

# Add legend to bubbleplot

legend.bubbleplot=function(z,col=1,pch=1,pt.cex=0,zmult=1/max(z),x.intersp=2,y=NULL,
                           y.intersp=3,cex=1,lpos="bottomright",prect=NULL,bty="n",bg='white',...){  
  
  l1=min(z)
  l4=max(z)
  l2=l1+(l4-l1)/4
  l3=l1+(l4-l1)/2  
  llim=c(l1,l2,l3,l4)
  
  if (!is.null(prect)){
    rect(prect[1],prect[2],prect[3],prect[4],border=col,col=bg)
  }
  
  ln=legend(x=lpos,y=y,legend=format(llim,digit=1),pch=rep(pch,4),col=col,
            bty=bty,pt.cex=pt.cex+llim*zmult,bg=bg, 
            x.intersp = x.intersp ,y.intersp = y.intersp ,cex=cex,...)
  
}

# Add bathymetry to map

MBathyPlot=function(lon1=-6,lon2=1,lat1=43,lat2=49,resolution =4,newbathy=TRUE,
                    add=TRUE,deep=-450,shallow=-50,bstep=450,bcol='grey50',
                    drawlabels = TRUE){
  library(marmap)
  if (newbathy|!(exists("bathy"))){
    getNOAA.bathy(lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution) -> bathy
    assign("bathy", bathy, envir = .GlobalEnv)
  }
  if(is.null(deep)){
    deep=min(bathy)
  }
  if(is.null(shallow)){
    shallow=max(bathy)
  }
  plot.bathy(bathy,add=add,deepest.isobath=deep,shallowest.isobath=shallow, step=bstep,col=bcol,
             drawlabels =drawlabels )
}

# bubbleplot with bathymetry 

sp.bubbleplot=function(dfi,xname='LONG',yname='LAT',zname='BB',inches=.25,spci=NULL,cruise=NULL,xlim=NULL,ylim=NULL,zcol=2,zfill=NA,
                       bathy.plot=TRUE,lon1=NULL,lon2=NULL,lat1=NULL,lat2=NULL,resolution=4,newbathy=FALSE,
                       add=TRUE,deep=NULL,shallow=NULL,bstep=20,bcol='grey50',y.intersp=2,lpos="bottomleft",
                       fwidth=12,fheight=12,res=300,pfilename="Rplot%03d.png",display='pane',bg='white',y=NULL,bty='n',prect=NULL,
                       ptitle=expression(sqrt(Biomass (t/MN^2))),drawlabels=FALSE,...){
  data(map)
  
  # Set up plot display
  if (display=='x11') x11()
  else if (display=='png') png(filename=pfilename,width=fwidth,height=fheight, units="in",res=res)
  par(bg='white')
  
  # Bubbleplot
  symbols(dfi[,xname], dfi[,yname], sqrt(dfi[,zname]), 
          inches=inches, asp=1/cos((mean(dfi[,yname])*pi)/180), 
          xlab="Longitude", ylab="Latitude", main=paste(cruise,spci,'biomass'),xlim=xlim,ylim=ylim,fg=zcol,bg=zfill)
  if (bathy.plot){
    if (is.null(lon1)|is.null(lon2)|is.null(lat1)|is.null(lat2)){
      lon1=floor(min(dfi[,xname]));lon2=ceiling(max(dfi[,xname]));lat1=floor(min(dfi[,yname]));lat2=ceiling(max(dfi[,yname]))
    }
    MBathyPlot(lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution,newbathy=newbathy,add=TRUE,deep=deep,shallow=shallow,
               bstep=bstep,bcol=bcol,drawlabels=drawlabels)
    symbols(dfi[,xname], dfi[,yname], sqrt(dfi[,zname]),add=TRUE, 
            inches=inches, asp=1/cos((mean(dfi[,yname])*pi)/180), 
            xlab="Longitude", ylab="Latitude", main=paste(cruise,spci,'biomass'),xlim=xlim,ylim=ylim,fg=zcol,bg=zfill)
  }
  polygon(map,col=8);box()
  if (!is.na(zfill)) {zpch=16
  }else{zpch=1}
  legend.bubbleplot(z=sqrt(dfi[,zname]),col=zcol,pch=zpch,pt.cex=0,zmult=(inches/0.05)/max(sqrt(dfi[,zname])),x.intersp=2,bty=bty,
                    y.intersp=y.intersp,cex=1,lpos=lpos,y=y,prect=prect,bg=NA,title=ptitle)
  if (display=='png'){
    dev.off()
    cat('Map saved to',as.character(pfilename),'\n')
  } 
}

histMaxvalues=function(z,varname,spi,group,path.grids,Bmax,zmax,zmax2=NULL,
                       zmaxlab='Extreme values threshold',
                       zmax2lab='Extreme values threshold 2'){
  if (!is.null(group)){
    za=aggregate(z,list(group),max)
    names(za)=c('year','zmax')
    x11()
    par(mfrow=c(1,1))
    boxplot(z,main=paste(spi,varname,'distribution and annual maxima'))
    abline(h=za$zmax,col='grey50')
    abline(h=Bmax,lwd=2,col=2)
    abline(h=zmax,lwd=2,col=3)
    if (!is.null(zmax2)) abline(h=zmax2,lwd=2,col=4) 
    text(rep(0.55,length(za$zmax)),za$zmax+30,za$year,col='grey50')
    text(0.7,Bmax+30,'Theoretical maximum',col=2)
    text(0.7,zmax+30,zmaxlab,col=3)
    if (!is.null(zmax2)) text(1.3,zmax2+20,zmax2lab,col=4) 
    if (!is.null(path.grids)){
      dev.print(device=png,file=paste(path.grids,'/',spi,"-zMaxYearDist.png",sep=""), width=800, height=800,
                bg = "white")
    }
    dev.off()
  }else stop('Please provide a grouping variable')
}  

Echobase2grid=function(mat){
  head(mat)
  xg<-sort(unique(mat$x)); yg<-sort(unique(mat$y))
  nx<-length(xg); ny<-length(yg)
  vecxg<-rep(xg,ny); vecig<-rep(1:nx,ny)
  vecyg<-NULL; for (j in 1:ny) {vecyg<-c(vecyg,rep(yg[j],nx))}
  vecjg<-NULL; for (j in 1:ny) {vecjg<-c(vecjg,rep(    j,nx))}
  matxg<-matrix(vecxg,nrow=nx,ncol=ny);matyg<-matrix(vecyg,nrow=nx,ncol=ny)
  matig<-matrix(vecig,nrow=nx,ncol=ny);matjg<-matrix(vecjg,nrow=nx,ncol=ny)
  matixg=unique(data.frame(x=c(matxg),I=c(matig)))
  matjyg=unique(data.frame(y=c(matyg),J=c(matjg)))
  #matixg=unique(data.frame(x=vecxg,I=vecig))
  #matjyg=unique(data.frame(y=vecyg,J=vecjg))    
  mat=merge(mat,matixg,by.x='x',by.y='x')
  mat=merge(mat,matjyg,by.x='y',by.y='y')
  names(mat)
  mat=mat[,c("x","y","I","J","value.meanMapcellBiomass","value.stdevMapcellBiomass","value.NsampleMapcell","voyage",
             "year")]
  names(mat)=c('Xgd','Ygd','I','J','Zvalue','Nsample','Zstdev','Survey','Year')
  mat=mat[order(mat$Year,mat$J,mat$I),]
  mat
}


# Max clupeid fish biomass based on max fish packing density

lm=18
mw=50
# Misund formula
l=18
d=(2.44*(l/100))^(-3)
# MAximum packing density
dmax=20
# No. of schools per cluster (Petitgas, 2003)
Nmax.shl.cluster=11
# No. of schools per 1 NM² ESDU
Nmax.shl.esdu=Nmax.shl.cluster^2
# Max. school area (Villalobos?)
AreaMax.shl=150
# Max. school width (Paramo & Gerlotto, 2005)
Width.max=21
#Max. fish abundance in 1 nm²
Amax=dmax*AreaMax.shl*Nmax.shl.esdu*Width.max
Bmax=Amax*mw*1e-6

# mean weight
a=0.00317
b=3.26
l=14
(w=a*l^b)

# Function for creating and plotting raster stack from grid files -----------------------------
#***************************

grid2rasterStack=function(pat,path1=NULL,varid='',ux11=TRUE,
                          palette='gradient.colors',
                          anomaly=TRUE,bathyBoB=FALSE,plotit=TRUE,
                          crsi=CRS('+proj=longlat +datum=WGS84 +no_defs'),
                          crs2=NULL,aspit=FALSE,aspi=NULL,fwidth=30,
                          fheight=30,fres=300){
  library(raster)
  library(rasterVis)
  if (class(palette)=='character'){
    if (palette=='gradient.colors'){
      data(gradient_colors)
      palette=rasterTheme(region=gradient.colors)
    }
  }
  if (class(pat)=='character'){
    df=read.table(pat,sep=';',header=TRUE)
  }else if (class(pat)=='data.frame'){
    df=pat
    # !!!! order dataframe properly !!!
    df=df[order(df$J,df$I),]
  }else{
    stop('Please provide a valid pat argument')
  }
  head(df)
  lyears=unique(df$Year)
  fid1=paste(varid,'_',unique(df$Survey),sep='')
  if (length(fid1)>1) fid1=unique(gsub("[[:digit:]]","",fid1))
  fid=paste(fid1,'_',min(lyears),'-',max(lyears),sep='')
  fids=paste(varid,'_',min(lyears),'-',max(lyears),sep='')
  
  # Reshape to cast Zvalues on the same grid for all years
  dfw=reshape(df[,c('Xgd','Ygd','Zvalue','Year')],v.names = "Zvalue",
              idvar = c('Xgd','Ygd'),timevar = "Year", direction = "wide")

  # create raster stack
  lyears=unique(df$Year)
  for (j in 1:length(lyears)){
    dfi=dfw[,c(1:2,j+2)]
    # ri <- raster(ncol=length(unique(df$I)), nrow=length(unique(df$J)),
    #              xmn=min(unique(df$Xgd)),xmx=max(unique(df$Xgd)), 
    #              ymn=min(unique(df$Ygd)), ymx=max(unique(df$Ygd)))
    # dim(df)
    # dim(dfi)
    # length(df[df$Year==lyears[j],'Zvalue'])
    # values(ri) <- df[df$Year==lyears[j],'Zvalue']
    #ri=flip(ri, 2)
    # better:
    ri <- rasterFromXYZ(dfi)
    #plot(ri)
    # Set raster CRS if crsi different from default
    #crs0=CRS('+proj=longlat +datum=WGS84 +no_defs')
    crs(ri)=crsi  # set default CRS
    # if (!compareCRS(crsi,CRS('+proj=longlat +datum=WGS84 +no_defs'))[1]){
    #   crs(ri)=crsi
    #   #ri=projectRaster(ri,crs=crsi)
    # }else{
    #   crs0=CRS('+proj=longlat +datum=WGS84 +no_defs')
    #   crs(ri)=crs0  # set default CRS
    # }
     dim(ri)
    idi=paste(varid,unique(df[df$Year==lyears[j],'Survey']),lyears[j])
    names(ri)=lyears[j]
    if (j==1){
      r=ri
    }else{
      r=stack(r,ri)
    }
  }
  # Add mean an stdev maps to stack
  if (dim(r)[3]>1){
    mr=calc(r,mean,na.rm=TRUE)
  }else{
    mr=r
  }
  names(mr)=paste('Avg',paste(min(lyears),max(lyears),sep='-'))
  # or avgMap2=calc(r, fun=mean)
  omr=cellStats(mr,'mean')
  if (dim(r)[3]>1){
    stdr=calc(r, fun=sd,na.rm=TRUE)+omr
  }else{
    stdr=mr
    values(stdr)=NA
  }
  names(stdr)=paste('SD.',paste(min(lyears),max(lyears),'+',round(omr),sep='-'),sep='')
  #r=stack(r,mr)
  r=stack(r,mr,stdr)
  
  # # Project raster into CRS crs2 if crs2 non null
  if (!is.null(crs2)){
    #crs(ri)=crsi
    r=projectRaster(r,crs=crs2)
  }
  
  if (!is.null(path1)){
    # Save raster
    pat2=paste(path1,'Rasters/',sep='')
    dir.create(paste(path1,'Rasters/',sep=''),showWarnings = FALSE)
    idi=paste(fid1,paste(min(lyears),max(lyears),sep='-'),sep = "_")
    filei2 = paste(pat2,paste('rasterStack',idi,sep='_'),'.grd',sep='')
    writeRaster(r,filei2,overwrite=TRUE)
    cat("Raster stack saved as:", filei2, "\n")
    path1=pat2
  }
  
  # Plot raster stack as mosaic
  if (plotit){
    rasterStackGridPlot(r,ux11=ux11,palette=palette,path1=path1,
                        fid1=fid1,plotit=list(MosaicSameScale=TRUE,
                                              IndSameScale=TRUE,
                                              MosaicDiffScale=FALSE),
                        bathyBoB=bathyBoB,aspit=aspit,fwidth=fwidth,
                        fheight=fheight,fres=fres,aspi=aspi)
  }
  
  if (anomaly){
    # Compute and plot anomaly maps
    names(r)
    indy=which(!substr(names(r),1,1)%in%c('A','S'))
    indym=which(substr(names(r),1,1)%in%c('A'))
    ar=r[[indy]]-r[[indym]]
    #ar=stack(ar,r[[indym]])
    #names(ar)=names(r)[c(indy,indym)]
    names(ar)=names(r)[c(indy)]
    fid2=paste('anom',fid1,sep='')
    if (!is.null(path1)){
      # Save raster
      pat3=paste(path1,'Anomaly/',sep='')
      dir.create(pat3,showWarnings = FALSE,recursive=TRUE)
      idi=paste(paste('anom',fid1,sep=''),paste(min(lyears),max(lyears),sep='-'),sep = "_")
      filei3 = paste(pat3,paste('rasterStack',idi,sep='_'),'.grd',sep='')
      writeRaster(ar,filei3,overwrite=TRUE)
      cat("Raster stack with anomalies saved as:", filei3, "\n")
      path1=pat3
    }
    if (plotit){
      mini=min(cellStats(ar,min))
      maxi=max(cellStats(ar,max))
      minmax=max(c(abs(mini),abs(maxi)))
      rasterStackGridPlot(ar,ux11=ux11,path1=path1,fid1=fid2,
                          plotit=list(MosaicSameScale=TRUE,IndSameScale=TRUE,
                                      MosaicDiffScale=FALSE),
                          bathyBoB=bathyBoB,
                          palette=BuRdTheme,zlimi = c(-minmax,minmax),
                          aspit=aspit,fwidth=fwidth,fheight=fheight,fres=fres,
                          aspi=aspi)
    }
  }
  r
}

rasterStackGridPlot=function(r,ux11=TRUE,palette='gradient.colors',
                             path1=NULL,fid1='',
                             fmargin = list(FUN = 'median'),
                             plotit=list(MosaicSameScale=TRUE,
                                         IndSameScale=TRUE,
                                         MosaicDiffScale=TRUE),
                             pmain=NULL,nrows=3,mcex=0.5,
                             crsi=CRS("+proj=longlat +datum=WGS84"),
                             zlimi=NULL,xcex=1,ycex=1,cexlab=1,
                             xlabs=if(isLonLat(r)) 'Longitude' else NULL,
                             ylabs=if(isLonLat(r)) 'Latitude' else NULL,
                             xtck=1,ytck=1,
                             tcklab=1,
                             fwidth=30,fheight=30,fres=300,nattri=NULL,
                             bathyBoB=FALSE,aspit=FALSE,aspi=NULL){
  if (class(palette)=='character'){
    if (palette=='gradient.colors'){
      data(gradient_colors)
      palette=rasterTheme(region=gradient.colors)
    }
  }
  # Plot raster stack as mosaic
  #get countries boundaries in proper format
  library(raster)
  library(maps)
  library(mapdata)
  library(sf)
  library(rasterVis)
  library(sp)
  library(latticeExtra)
  library(lattice)
  mt=map(database = "worldHires", plot = F,xlim=c(extent(r)[1],extent(r)[2]),
         ylim=c(extent(r)[3],extent(r)[4]),fill=TRUE)
  crsi=CRS(projection(r))
  mt.sf<-st_as_sf(mt)
  mt.sp=as(mt.sf, "Spatial")
  mt.sp=spTransform(mt.sp, CRS=crsi)
  data("BoBisobaths")
  BoBisobath.mat=cbind(BoBisobaths$x,BoBisobaths$y)
  BoBisobath.mat=BoBisobath.mat[complete.cases(BoBisobath.mat),]
  BoBisobath=Line(BoBisobath.mat)
  # assign mt.sp and BoBisobath to global environment, eitherway they do not appear in plots
  assign("mt.sp", mt.sp, envir = .GlobalEnv)
  assign("BoBisobath", BoBisobath, envir = .GlobalEnv)
  if (is.null(zlimi)){
    zlimi=range(values(r),na.rm=TRUE)      
  }
  if (is.null(aspi)){
    # get longitudes and latitudes and calculate aspect ratio
    xi=seq(extent(r)[1],extent(r)[2],res(r)[1])
    yi=seq(extent(r)[3],extent(r)[4],res(r)[2])
    aspi=1/cos((mean(yi)*pi)/180)
  }
  if (plotit$MosaicSameScale){
    # plot all layers with gridmap color palette
    if (ux11) x11()
    if (is.null(nattri)){
      nattri=gsub('X','',names(r))
    }else if (nattri==''){
      nattri=rep('',length(names(r)))
    }
    if (!is.null(pmain)){
      mainit=list(label=pmain,cex=mcex)
      dpad=0
    }else{
      mainit=pmain
      dpad=1
    }
    #nattri=gsub('[...]','+',nattri)
    ats=seq(zlimi[1], zlimi[2], length.out=100)
    if (length(unique(ats))==1){
      p=levelplot(r,par.settings=palette,names.attr=nattri,
                  margin=fmargin,scales = list(x = list(cex = ycex,tck=ytck),
                                               y = list(cex = xcex,tck=xtck)),
                  xlab=xlabs,ylab=ylabs,colorkey=list(labels=list(cex=cexlab),
                                                      tck=tcklab),main=mainit)
    }else{
      p=levelplot(r,par.settings=palette,names.attr=nattri,at=ats,
                  margin=fmargin,
                  scales = list(x = list(cex = ycex,tck=ytck),
                                y = list(cex = xcex,tck=xtck)),xlab=xlabs,
                  ylab=ylabs,colorkey=list(labels=list(cex=cexlab),
                                           tck=tcklab),main=mainit)
    }
    p=p + latticeExtra::layer(sp.polygons(mt.sp,fill=gray(0.8)))
    if (bathyBoB){
      # add isobaths
      p=p + latticeExtra::layer(sp.lines(BoBisobath,col='grey70'))
    }
    print(p)
    if (aspit){
      update(p, aspect=aspi)
    }
    lyears=as.numeric(gsub('X','',names(r)))
    lyears=lyears[!is.na(lyears)]
    if (ux11&!is.null(path1)){
      # Export raster stack plot
      pat2=path1
      idi=paste(fid1,paste(min(lyears),max(lyears),sep='-'),sep = "_")
      filei=paste(pat2,paste('rasterMosaic',idi,sep='_'),'.png',sep='')
      dev.print(device = png, file = filei,
                units='cm',width=fwidth, height=fheight,res=fres, bg = "white")
      cat("Raster mosaic saved as:", filei, "\n")
      dev.off()
    }
  }
  res(r)[1]=0.25/2
  if (plotit$IndSameScale){
    # plot each layer separetely with gridmap color palette
    dim(r)
    for (i in 1:dim(r)[3]){
      if (sum(!is.na(values(r[[i]])))>0){
        if (ux11) x11()
        ats=seq(zlimi[1], zlimi[2], length.out=100)
        if (length(unique(ats))==1){
          p=levelplot(r,layers = i,par.settings=palette,margin = fmargin, 
                      contour=FALSE,main=gsub('X','',names(r))[i],
                      scales = list(x = list(cex = ycex,tck=ytck),
                                    y = list(cex = xcex,tck=xtck)),xlab=xlabs,
                      ylab=ylabs,colorkey=list(labels=list(cex=cexlab),tck=tcklab))
        }else{
          p=levelplot(r,layers = i,par.settings=palette,margin = fmargin, 
                      contour=FALSE,
                      main=gsub('X','',names(r))[i],at=ats,crs=crsi,
                      scales = list(x = list(cex = ycex,tck=ytck),
                                    y = list(cex = xcex,tck=xtck)),xlab=xlabs,
                      ylab=ylabs,colorkey=list(labels=list(cex=cexlab),tck=tcklab))
        }  
        p=p + latticeExtra::layer(sp.polygons(mt.sp,fill=gray(0.8)))
        if (bathyBoB){
          # add isobaths
          p=p + latticeExtra::layer(sp.lines(BoBisobath,col='grey70'))
        }
        print(p)  
        if (aspit){
          update(p, aspect=aspi)
        }
        print(p) 
        if (ux11&!is.null(path1)){
          # Export raster stack and plot
          pat2=path1
          idi=paste('raster',fid1,gsub('X','',names(r))[i],sep = "_")
          filei = paste(pat2, idi, ".png", sep = "")
          dev.print(device = png, file = filei, units='cm',width=fwidth, 
                    height=fheight,res=fres, bg = "white")
          cat("Raster map saved as:", filei, "\n")
          dev.off()
        }
      }
       #plot(r)
    }
  }  
  if (plotit$MosaicDiffScale){
    nl <- nlayers(r)
    m <- matrix(1:(nrows*round(nl/nrows)), nrow=nrows,byrow=TRUE)
    if (ux11) x11()
    for (i in 1:nl){
      if (sum(!is.na(values(r[[i]])))>0){
        if (!is.null(pmain)){
          mainit=list(label=MosaicDiffScale.names[i],cex=mcex)
          names(r)
          dpad=0
        }else{
          mainit=pmain
          dpad=1
        }
        if (is.null(nattri)){
          nattri=gsub('X','',names(r))
        }else if (nattri==''){
          nattri=rep('',length(names(r)))
        }
        # define zlimi for this layer
        zlimi=range(values(r[[i]]),na.rm=TRUE)      
        if (sum(is.infinite(zlimi))==0){
          ats=seq(zlimi[1], zlimi[2], length.out=100)
          if (length(unique(ats))==1){
            palette$layout.heights=list(top.padding=-dpad,bottom.padding=-dpad)
            palette$layout.widths=list(left.padding=-1,right.padding=-1)
            p=levelplot(r,layers = i,par.settings=palette,margin = FALSE, contour=FALSE,
                        main=mainit,crs=crsi,names.attr=nattri,
                        scales = list(x = list(cex = ycex,tck=ytck),y = list(cex = xcex,tck=xtck)),xlab=xlabs,
                        ylab=ylabs,colorkey=list(labels=list(cex=cexlab),tck=tcklab))
          }else{
            palette$layout.heights=list(top.padding=-dpad,bottom.padding=-dpad)
            palette$layout.widths=list(left.padding=-1,right.padding=-1)
            p=levelplot(r,layers = i,par.settings=palette,margin = FALSE, contour=FALSE,
                        main=mainit,at=ats,crs=crsi,names.attr=nattri,
                        scales = list(x = list(cex = ycex,tck=ytck),y = list(cex = xcex,tck=xtck)),xlab=xlabs,
                        ylab=ylabs,colorkey=list(labels=list(cex=cexlab),tck=tcklab))
          }
          p=p + latticeExtra::layer(sp.polygons(mt.sp,fill=gray(0.8)))
          p=p + latticeExtra::layer(sp.polygons(mt.sp,fill=gray(0.8)))
          if (bathyBoB){
            # add isobaths
            p=p + latticeExtra::layer(sp.lines(BoBisobath,col='grey70'))
          }
          print(p, split=c(col(m)[i], row(m)[i], ncol(m), nrow(m)), more=(i<nl))
          if (aspit){
            update(p, aspect=aspi)
          }
        }
      }  
    }
    if (ux11&!is.null(path1)){
      # Export raster stack plot
      pat2=path1
      idi0=data.frame(t(data.frame(strsplit(names(r),split='[.]'))))
      r1=unlist(idi0[1,])
      Ncolv=match(c('Avg','CV'),unlist(idi0[1,]))
      Ncolv=Ncolv[!is.na(Ncolv)]
      Ncoly=match(200,substr(r1,1,3))
      idi=paste(unique(idi0[,Ncolv]),min(as.numeric(as.character(idi0[,Ncoly]))),
                max(as.numeric(as.character(idi0[,Ncoly]))),sep='-')
      filei=paste(pat2,paste('rasterMosaic',idi,sep='_'),'.png',sep='')
      dev.print(device = png, file = filei,
                units='cm',width=fwidth, height=fheight,res=fres, bg = "white")
      cat("Raster mosaic with varying scales saved as:", filei, "\n")
      dev.off()
    }
  }
}

raster.levelplot=function(r,ux11=TRUE,lptheme='gradient.colors',
                          path1=NULL,fid1=NULL,
                          marginf = list(FUN = 'median'),
                          nattri=gsub('X','',names(r)),ats=NULL,
                          fwidth=30,fheight=15,fres=300,bathy.plot=TRUE,
                          resolution=4,newbathy=FALSE,deep=-450,
                          shallow=-50,bstep=450,coastit=TRUE,...){
  library(maps)
  library(sf)
  library(rasterVis)
  if (class(lptheme)=='character'){
    if (lptheme=='gradient.colors'){
      data(gradient_colors)
      lptheme=rasterTheme(region=gradient.colors)
    }
  }
  mt=map(database = "worldHires", 
         plot = F,xlim=c(extent(r)[1],extent(r)[2]),
         ylim=c(extent(r)[3],extent(r)[4]),fill=TRUE)
  crsi=CRS(projection(r))
  mt.sf<-st_as_sf(mt)
  mt.sp=as(mt.sf, "Spatial")
  mt.sp=spTransform(mt.sp, CRS=crsi)
  if (bathy.plot){
    rext=extent(r)
    lon1=rext[1];lon2=rext[2];lat1=rext[3];lat2=rext[4]
    library(marmap)
    if (newbathy|!(exists("bathy"))){
      getNOAA.bathy(lon1=lon1,lon2=lon2,lat1=lat1,lat2=lat2,resolution=resolution) -> bathy
      assign("bathy", bathy, envir = .GlobalEnv)
    }
    # get bathy data and isobaths levels
    misobaths=plot.bathy.M(x=bathy,deepest.isobath=deep,shallowest.isobath=shallow,step=bstep,
                           plotit=FALSE)
    # extract isobaths coordinates
    isobaths=contourLines(x=misobaths$x,y=misobaths$y,z=misobaths$mat,levels=misobaths$lparam)
    for (i in 1:length(isobaths)){
      isoi=isobaths[[i]]
      isoi.df=data.frame(id=i,x=isoi$x,y=isoi$y,level=isoi$level)
      lisobathi=Line(cbind(isoi$x,isoi$y))
      if (i==1){
        isobaths.df=isoi.df
        lisobaths=list(lisobathi)
      }else{
        isobaths.df=rbind(isobaths.df,isoi.df)
        lisobaths=c(lisobaths,lisobathi)
      }
    }
    Lisobaths=Lines(lisobaths,ID=1)
    assign("Lisobaths", Lisobaths, envir = .GlobalEnv)
  }
  # assign mt.sp and BoBisobath to global environment, eitherway they do not appear in plots
  assign("mt.sp", mt.sp, envir = .GlobalEnv)
  # plot all layers with gridmap color palette
  zlimi=range(values(r),na.rm=TRUE)
  if (is.null(ats)){
    ats=seq(zlimi[1], zlimi[2], length.out=100)    
  }
  if (ux11) {
    x11()
  }  
  if (length(unique(ats))==1){
    p=levelplot(r,par.settings=lptheme,margin=marginf,...)
  }else{
    p=levelplot(r,par.settings=lptheme,at=ats,margin=marginf,...)
  }
  if (coastit){
    p=p + latticeExtra::layer(sp.polygons(mt.sp,fill=gray(0.8)))
  }
  # add isobaths
  if (bathy.plot){
    p=p + latticeExtra::layer(sp.lines(Lisobaths,col='grey70'))
  }
  print(p)
  if (ux11&!is.null(path1)){
    # Export raster stack plot
    pat2=path1
    filei=paste(pat2,paste('rasterLevelPlot',fid1,sep='_'),'.png',sep='')
    dev.print(device = png, file = filei,units='cm',width=fwidth, height=fheight,res=fres,
              bg = "white")
    cat("Raster levelplot saved as:", filei, "\n")
    dev.off()
  }
}

raster.levelplot.PELGAS=function(r,ux11=TRUE,lptheme='gradient.colors',
                                 path1=NULL,fid1=NULL,margin = list(FUN = 'median'),
                                 nattri=gsub('X','',names(r)),ats=NULL,fwidth=30,fheight=15,fres=300,
                                 bathy.BoB=TRUE,...){
  library(maps)
  library(sf)
  library(rasterVis)
  if (class(lptheme)=='character'){
    if (lptheme=='gradient.colors'){
      data(gradient_colors)
      lptheme=rasterTheme(region=gradient.colors)
    }
  }
  mt=map(database = "worldHires", plot = F,xlim=c(extent(r)[1],extent(r)[2]),
         ylim=c(extent(r)[3],extent(r)[4]),fill=TRUE)
  mt.sf<-st_as_sf(mt)
  mt.sp=as(mt.sf, "Spatial")
  data("BoBisobaths")
  BoBisobath.mat=cbind(BoBisobaths$x,BoBisobaths$y)
  BoBisobath.mat=BoBisobath.mat[complete.cases(BoBisobath.mat),]
  BoBisobath=Line(BoBisobath.mat)
  # assign mt.sp and BoBisobath to global environment, eitherway they do not appear in plots
  assign("mt.sp", mt.sp, envir = .GlobalEnv)
  assign("BoBisobath", BoBisobath, envir = .GlobalEnv)
  # plot all layers with gridmap color palette
  zlimi=range(values(r),na.rm=TRUE)
  if (is.null(ats)){
    ats=seq(zlimi[1], zlimi[2], length.out=100)    
  }
  if (ux11) {
    x11()
  }  
  if (length(unique(ats))==1){
    p=levelplot(r,par.settings=lptheme,margin=margin,...)
  }else{
    p=levelplot(r,par.settings=lptheme,at=ats,margin=margin,...)
  }
  p=p + latticeExtra::layer(sp.polygons(mt.sp,fill=gray(0.8)))
  # add isobaths
  if (bathy.BoB){
    p=p + latticeExtra::layer(sp.lines(BoBisobath,col='grey70'))
  }
  print(p)
  if (ux11&!is.null(path1)){
    # Export raster stack plot
    pat2=path1
    filei=paste(pat2,paste('rasterLevelPlot',fid1,sep='_'),'.png',sep='')
    dev.print(device = png, file = filei,units='cm',width=fwidth, height=fheight,res=fres,
              bg = "white")
    cat("Raster levelplot saved as:", filei, "\n")
    dev.off()
  }
}

catches.piePlot=function(EsduDevi=NULL,Pechelsi=NULL,Pechei=NULL,dcol=c(2,4,1,3,7,8,6,5),lcol=NULL,legpos='topright',
                         add.layer=NULL,export.plot=NULL,radSA=0.2,pradius=1,scale.SA=1,xlim=NULL,
                         ylim=NULL,smax=1,corr=cos(46.5*pi/180),logit=FALSE,labelit=FALSE,
                         nid='NOCHAL',cex.lab=1,ux11=TRUE,...){

  lsps=names(Pechelsi)[!names(Pechelsi)%in%c('CAMPAGNE',
                                             'NOCHAL','NOSTA','LONF','LATF','STRATE','SONDE','GEAR')]
  lsps2=substr(lsps,4,11)
  
  if (is.null(lcol)){
    spcol=data.frame(sp=c("ENGR.ENC","MICR.POU","SARD.PIL","SCOM.SCO",
                          "SPRA.SPR","TRAC.TRU","CAPR.APE","SCOM.COL","TRAC.MED",
                          "CLUP.HAR","COMP.LEM","MERL.MNG","MERL.MCC"),
                     sp2=c("ENGR-ENC","MICR-POU","SARD-PIL","SCOM-SCO",
                           "SPRA-SPR","TRAC-TRU","CAPR-APE","SCOM-COL","TRAC-MED",
                           "CLUP-HAR","COMP-LEM","MERL-MNG","MERL-MCC"),
                     lcol=c(3,8,4,2,1,7,6,5,0,0,0,0,0))
    
    #select species colors
    #***************************
    #lsps2=substr(lsps,4,11)
    lcol=spcol[,'lcol'][match(lsps2,spcol$sp)]
  }
  Pechelsis=Pechelsi
  
  if (!is.null(export.plot)){
    png(file=paste(export.plot,cruise,'TotSaHauls.png',sep=''),
        width=800,height=800)
  }else if (ux11) x11()
  
  par(mar=c(2,2,3,1))
  if (!is.null(EsduDevi)&!is.null(Pechelsis)){
    xrb=c(EsduDevi$LONG,Pechelsis$LONF)
    yrb=c(EsduDevi$LAT,Pechelsis$LATF)
  }else if (is.null(EsduDevi)&!is.null(Pechelsis)){
    xrb=c(Pechelsis$LONF)
    yrb=c(Pechelsis$LATF)
  }else if (!is.null(EsduDevi)&is.null(Pechelsis)){
    xrb=c(EsduDevi$LONG)
    yrb=c(EsduDevi$LAT)
  }
  if (is.null(xlim)){
    xlim=c(min(xrb)-abs(max(xrb)-min(xrb))/10,
           max(xrb)+abs(max(xrb)-min(xrb))/10)
  }
  if (!is.null(EsduDevi)){
    Esdus=EsduDevi[EsduDevi$LONG>=xlim[1]&EsduDevi$LONG<=xlim[2],]
  }
  if (!is.null(Pechelsis)){
    Pecheis=Pechelsis[Pechelsis$LONF>=xlim[1]&Pechelsis$LONF<=xlim[2],]
  }  
  
  if (is.null(ylim)){
    ylim=c(min(yrb)-abs(max(yrb)-min(yrb))/10,
           max(yrb)+abs(max(yrb)-min(yrb))/10)
  }
  if (!is.null(EsduDevi)){
    Esdus=Esdus[Esdus$LAT>=ylim[1]&Esdus$LAT<=ylim[2],]
    sa=Esdus[,'TOTAL']
    plot(Esdus$LONG,Esdus$LAT,
         cex=radSA+log(sa+1)/scale.SA,
         pch=1,col='grey50',xlim=xlim,ylim=ylim)
    coast()
  }
  if (!is.null(Pechelsis)){
    Pecheis=Pecheis[Pecheis$LATF>=ylim[1]&Pecheis$LATF<=ylim[2],]
  }
  #if (!is.null(add.map)) plot(add.map,add=FALSE,col=1)
  #if (!is.null(add.map)) plot(add.map,add=FALSE,col=1)
  if (!is.null(EsduDevi)&!is.null(Pechelsis)){
    if (dim(Pecheis)[1]>0){
      if (logit){
        z=as.matrix(log(Pecheis[,lsps]+1))
      }else  {z=as.matrix(Pecheis[,lsps])}
      pie.xy(x=Pecheis$LONF,y=Pecheis$LATF,
             z=z,pcoast=FALSE,pcol=lcol,draw1=FALSE,
             pradius=pradius,smax=smax)
      legend(legpos,legend=substr(lsps,4,11),
             fill=lcol,bg='white')
      if (labelit) text(Pecheis$LONF,y=Pecheis$LATF,Pecheis[,nid],
                        cex=cex.lab,col='grey50')
      if (!is.null(add.layer)) plot(add.layer,add=TRUE,col=2)
    }
  }else if (is.null(EsduDevi)&!is.null(Pechelsis)){
    if (dim(Pecheis)[1]>0){
      if (logit){
        z=as.matrix(log(Pecheis[,lsps]+1))
      }else  {z=as.matrix(Pecheis[,lsps])}
      pie.xy(x=Pecheis$LONF,y=Pecheis$LATF,
             z=z,pcoast=FALSE,pcol=lcol,draw1=FALSE,
             pradius=pradius,smax=1e-6,...)
      legend(legpos,legend=substr(lsps,4,11),
             fill=lcol,bg='white')
      if (labelit) text(Pecheis$LONF,y=Pecheis$LATF,Pecheis[,nid],
                        cex=cex.lab,col='grey50')
      if (!is.null(add.layer)) plot(add.layer,add=TRUE,col=2)
    }
  }
  if (!is.null(export.plot)){
    cat('Catches pie plot saved to:',export.plot,'\n')
    dev.off()
  }		
}

plot.bathy.M=function (x, image = FALSE, bpal = NULL, land = FALSE, deepest.isobath, 
                       shallowest.isobath, step, n = 20, lwd = 1, lty = 1, col = "black", 
                       default.col = "white", drawlabels = FALSE, xlab = "Longitude", 
                       ylab = "Latitude", asp = 1, plotit=FALSE,...) 
{
  mat <- x
  if (!missing("deepest.isobath")) {
    n.levels <- length(deepest.isobath)
  }
  else {
    n.levels <- 1
  }
  lon <- unique(as.numeric(rownames(mat)))
  lat <- unique(as.numeric(colnames(mat)))
  if (land == FALSE) 
    mat[mat > 0] <- 0
  if (image == FALSE) {
    if (n.levels == 1) {
      if (!missing("deepest.isobath") | !missing("shallowest.isobath") | 
          !missing("step")) {
        level.param <- seq(deepest.isobath, shallowest.isobath, 
                           by = step)
      }
      else {
        level.param <- pretty(range(mat, na.rm = TRUE), 
                              n = n)
      }
      if (plotit){
        contour(lon, lat, mat, levels = level.param, lwd = lwd, 
                lty = lty, col = col, drawlabels = drawlabels, 
                xlab = xlab, ylab = ylab, asp = asp, ...)
      }
    }
    if (n.levels > 1) {
      level.param <- seq(deepest.isobath[1], shallowest.isobath[1], 
                         by = step[1])
      if (plotit){
        contour(lon, lat, mat, levels = level.param, lwd = lwd[1], 
                lty = lty[1], col = col[1], drawlabels = drawlabels[1], 
                xlab = xlab, ylab = ylab, asp = asp, ...)
      }  
      for (i in 2:n.levels) {
        level.param <- seq(deepest.isobath[i], shallowest.isobath[i], 
                           by = step[i])
        if (plotit){
          contour(lon, lat, mat, levels = level.param, 
                  lwd = lwd[i], lty = lty[i], col = col[i], drawlabels = drawlabels[i], 
                  add = TRUE)
        }  
      }
    }
  }
  if (image == TRUE) {
    if (is.null(bpal)) {
      ramp <- colorRampPalette(c("#245372", "#4871D9", 
                                 "#7D86A1", "white"))
      bpal <- ramp(100)
    }
    if (is.list(bpal)) 
      bpal <- palette.bathy(mat, layers = bpal, land = land, 
                            default.col = default.col)
    if (n.levels == 1) {
      if (!missing("deepest.isobath") | !missing("shallowest.isobath") | 
          !missing("step")) {
        level.param <- seq(deepest.isobath, shallowest.isobath, 
                           by = step)
      }
      else {
        level.param <- pretty(range(mat, na.rm = TRUE), 
                              n = n)
      }
      if (plotit){
        image(lon, lat, mat, col = bpal, xlab = xlab, ylab = ylab, 
              asp = asp, ...)
        contour(lon, lat, mat, levels = level.param, lwd = lwd, 
                lty = lty, col = col, drawlabels = drawlabels, 
                add = TRUE)
      }  
    }
    if (n.levels > 1) {
      level.param <- seq(deepest.isobath[1], shallowest.isobath[1], 
                         by = step[1])
      if (plotit){
        image(lon, lat, mat, col = bpal, xlab = xlab, ylab = ylab, 
              asp = asp, ...)
        contour(lon, lat, mat, levels = level.param, lwd = lwd[1], 
                lty = lty[1], col = col[1], drawlabels = drawlabels[1], 
                add = TRUE)
      }  
      for (i in 2:n.levels) {
        level.param <- seq(deepest.isobath[i], shallowest.isobath[i], 
                           by = step[i])
        if (plotit){
          contour(lon, lat, mat, levels = level.param, 
                  lwd = lwd[i], lty = lty[i], col = col[i], drawlabels = drawlabels[i], 
                  add = TRUE)
        }  
      }
    }
  }
  list(x=lon,y=lat,mat=mat,lparam=level.param)
}

EBgrid2gridMap=function(df,rasterit=TRUE,path1=NULL,varid=varid,ux11=TRUE,
                        palette='gradient.colors',
                        anomaly=TRUE,bathyBoB=FALSE,plotit=TRUE,
                        crsi=CRS("+proj=longlat +datum=WGS84")){
  Ixdf=data.frame(xs=sort(unique(df$x)))
  Ixdf$I=seq(length(Ixdf$xs))
  Jydf=data.frame(ys=sort(unique(df$y)))
  Jydf$J=seq(length(Jydf$ys))
  dfg=merge(df,Ixdf,by.x='x',by.y='xs')
  dfg=merge(dfg,Jydf,by.x='y',by.y='ys')
  if (!'Year'%in%names(dfg)){
    dfg$Year=substr(dfg$voyage,7,10)
  }
  names(dfg)[names(dfg)=='voyage']='Survey'
  names(dfg)[names(dfg)=='x']='Xgd'
  names(dfg)[names(dfg)=='y']='Ygd'
  names(dfg)[names(dfg)=='value']='Zvalue'
  if(rasterit){
    dfr=grid2rasterStack(pat=dfg,path1=path1,varid=varid,ux11=ux11,
                         palette=palette,anomaly=anomaly,bathyBoB=bathyBoB,
                         plotit=plotit,crsi=crsi)
  }
  list(dfg=dfg,dfr=dfr)
}

# utmzone
# source: https://stackoverflow.com/questions/9186496/determining-utm-zone-to-convert-from-longitude-latitude

utmzone <- function(lon,lat) {
  ## Special Cases for Norway & Svalbard
  if (lat > 55 & lat < 64 & lon > 2 & lon < 6){ 
    band <- 32
  } else {
    if (lat > 71 & lon >= 6 & lon < 9){
      band <- 31
    } else {
      if (lat > 71 & lon >= 9 & lon < 12){
        band <- 33
      } else {
        if (lat > 71 & lon >= 18 & lon < 21){
          band <- 33
        } else {
          if (lat > 71 & lon >= 21 & lon < 24){
            band <- 35
          } else {
            if (lat > 71 & lon >= 30 & lon < 33){
              band <- 35
            } else {
              ## Rest of the world
              if (lon >= -180 & lon <= 180){
                band <- (floor((lon + 180)/6) %% 60) + 1
              } else {
                band <- "something is wrong"
              }}}}}}}
  return(band)
}
