	#-------------------------------------------------------------
	#Toolbox for acoustic fish biomass assessment
	#-------------------------------------------------------------
  sel.name=function(x,n,singleton=TRUE){
    if (TRUE%in%x){
      y=n[x]
      if (singleton){
        if (length(y)>1){
          y=y[1]
          cat('Esdu in more than 1 region, allocated to first region','\n')
        }
      }  
    }else {y=NA}
    y
  }

  colmult=function(x){
			y=x[-1]*x[1]
			y
		}

 na.null=function(x){
		x[is.na(x)]=0
	x}

	NA.remove=function(x,sel=NULL){
		if (FALSE%in%is.na(x)){
			xs=x[!is.na(x)]
			if (!is.null(sel)){xs=xs[sel]}			
		}else{
			xs=NA
		}
	xs
	}

	isna.null=function(x){
		x[is.na(x)]=0
	x}
  
#   slashit=function(x){
#     x=gsub('\\','/',x,fixed=TRUE)
#   x}
  
  SelfirstDuplicated=function(x,cname){
    dx=x[duplicated(x[,cname]),]
    dxs=dx[seq(1,dim(dx)[1],2),]
    dxs
  }
  
	listIt=function(df,gname){
	  lg=unique(df[,gname])
	  for (i in 1:length(lg)){
	    dfi=df[df[,gname]==lg[i],]
	    li=list(dfi)
	    names(li)=lg[i]
	    if (i==1){lf=li
	    }else{lf=c(lf,li)}          
	  }
	  lf
	}
  
	AddCommunities=function(df,fishCom,spname='GENR_ESP'){
	  dim(df)
	  for (i in 1:length(fishCom)){
	    fci=data.frame(names(fishCom)[i],fishCom[[i]])
	    names(fci)=c(paste('Com',i,sep=''),'sp')
	    df=merge(df,unique(fci),by.x=spname,by.y='sp',all.x=TRUE)
	    if (i==1) fc=fci
	    else fc=merge(fc,unique(fci),by.x='sp',by.y='sp',all.x=TRUE)
	  }
	  dim(df)
	  list(df=df,fc=fc)
	}
	
	is.letters=function(x){
	  for (i in 1:nchar(x)){
	    isli=substr(x,i,i)%in%c(LETTERS,letters)
	    if (i==1) isl=isli
	    else isl=c(isl,isli)
	  }
	  isl
	}  
  
  # function to correct Baracouda strange positions formats
  
	correct.positions=function(df,xname=NA,yname=NA,asNewColumn=FALSE){
	  dim(df)
	  if (!is.na(xname)){
	    if (is.null(dim(df))){
	      x=as.numeric(gsub('W','',df))      
	    }else{
	      x=as.numeric(gsub('W','',df[,xname]))
	    }  
	    x1=floor(x/100)
	    unique(x1)
	    x2=floor(x-x1*100)/60
	    unique(x2)
	    x3=((x-x1*100)-x2*60)/100
	    x=-(x1+x2+x3)
	    if (!asNewColumn){
	      if (is.null(dim(df))){
	        df=x
	      }else{
	        df[,xname]=x
	      }  
	    }else{
	      df$x=x
	    }
	  }
	  if (!is.na(yname)){
	    if (is.null(dim(df))){
	      y=as.numeric(gsub('N','',df))      
	    }else{
	      y=as.numeric(gsub('N','',df[,yname]))
	    }
	    y1=floor(y/100)
	    unique(y1)
	    y2=floor(y-y1*100)/60
	    unique(y2)
	    y3=((y-y1*100)-y2*60)/100
	    y=y1+y2+y3
	    if (!asNewColumn){
	      if (is.null(dim(df))){
	        df=y
	      }else{
	        df[,yname]=y
	      }
	    }else{
	      df$y=y
	    }
	  }  
	  df
	}
	
	mkdirs <- function(fp) {
	  if(!file.exists(fp)) {
	    mkdirs(dirname(fp))
	    dir.create(fp)
	  }
	} 
	
	present.na=function(x){
	  y=(sum(is.na(x))>0)
	  y
	}

	# function to cbind matrix or data.frame while adding NA is missing rows
	
# 	cbind.fill <- function(...){
# 	  nm <- list(...) 
# 	  dfdetect <- grepl("data.frame|matrix", unlist(lapply(nm, function(cl) paste(class(cl), collapse = " ") )))
# 	  # first cbind vectors together 
# 	  vec <- data.frame(nm[!dfdetect])
# 	  n <- max(sapply(nm[dfdetect], nrow)) 
# 	  vec <- data.frame(lapply(vec, function(x) rep(x, n)))
# 	  if (nrow(vec) > 0) nm <- c(nm[dfdetect], list(vec))
# 	  nm <- lapply(nm, as.data.frame)
# 	  
# 	  do.call(cbind, lapply(nm, function (df1) 
# 	    rbind(df1, as.data.frame(matrix(NA, ncol = ncol(df1), nrow = n-nrow(df1), dimnames = list(NULL, names(df1))))) )) 
# 	}
	
	# Multiple plot function
	#
	# ggplot objects can be passed in ..., or to plotlist (as a list of ggplot objects)
	# - cols:   Number of columns in layout
	# - layout: A matrix specifying the layout. If present, 'cols' is ignored.
	#
	# If the layout is something like matrix(c(1,2,3,3), nrow=2, byrow=TRUE),
	# then plot 1 will go in the upper left, 2 will go in the upper right, and
	# 3 will go all the way across the bottom.
	#
	multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
	  library(grid)
	  
	  # Make a list from the ... arguments and plotlist
	  plots <- c(list(...), plotlist)
	  
	  numPlots = length(plots)
	  
	  # If layout is NULL, then use 'cols' to determine layout
	  if (is.null(layout)) {
	    # Make the panel
	    # ncol: Number of columns of plots
	    # nrow: Number of rows needed, calculated from # of cols
	    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
	                     ncol = cols, nrow = ceiling(numPlots/cols))
	  }
	  
	  if (numPlots==1) {
	    print(plots[[1]])
	    
	  } else {
	    # Set up the page
	    grid.newpage()
	    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
	    
	    # Make each plot, in the correct location
	    for (i in 1:numPlots) {
	      # Get the i,j matrix positions of the regions that contain this subplot
	      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
	      
	      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
	                                      layout.pos.col = matchidx$col))
	    }
	  }
	}

# weighted.var function taken from https://stat.ethz.ch/pipermail/r-help/2008-July/168762.html		
	
	weighted.var <- function(x, w, na.rm = FALSE) {
	  if (na.rm) {
	    w <- w[i <- !is.na(x)]
	    x <- x[i]
	  }
	  sum.w <- sum(w)
	  sum.w2 <- sum(w^2)
	  mean.w <- sum(x * w) / sum(w)
	  (sum.w / (sum.w^2 - sum.w2)) * sum(w * (x - mean.w)^2, na.rm =
	                                       na.rm)
	}
	
# Function to get decimal places, taken from https://stackoverflow.com/questions/5173692/how-to-return-number-of-decimal-places-in-r 
		
	decimalplaces <- function(x) {
	  ifelse(abs(x - round(x)) > .Machine$double.eps^0.5,
	         nchar(sub('^\\d+\\.', '', sub('0+$', '', as.character(x)))),
	         0)
	}	
	
	
	# Adapted from:
	# http://www.cookbook-r.com/Graphs/Multiple_graphs_on_one_page_(ggplot2)/
	
	# objects can be passed in ..., or to plotlist (as a list of ggplot objects)
	# - cols:   Number of columns in layout
	# - layout: A matrix specifying the layout. If present, 'cols' is ignored.
	#
	# If the layout is something like matrix(c(1,2,3,3), nrow=2, byrow=TRUE),
	# then plot 1 will go in the upper left, 2 will go in the upper right, and
	# 3 will go all the way across the bottom.
	# 
	# Title can be passed as c("title1", "title2"). If only one title is
	# provided it will be used as single title for all columns. If several titles
	# are provided each column will then have it's own title.
	#
	# Title size, font and face can also be provided
	
	multiplot <- function(..., plotlist = NULL, cols = 1, layout = NULL, title = NULL, 
	                      fontsize = 14, fontfamily = "Helvetica", fontface = "bold") {
	  require(grid)
	  plots <- c(list(...), plotlist)
	  numPlots = length(plots)
	  if (is.null(layout)) {
	    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
	                     ncol = cols, nrow = ceiling(numPlots/cols))
	  }
	  if (length(title)>0){
	    layout <- rbind(rep(0, ncol(layout)), layout)
	  }
	  if (numPlots==1) {
	    print(plots[[1]])
	  } else {
	    grid.newpage()
	    pushViewport(viewport(layout = grid.layout(nrow(layout), 
	                                               ncol(layout), 
	                                               heights = if (length(title)>0) {unit(c(0.5, rep(5,nrow(layout)-1)), "null")}
	                                               else {unit(c(rep(5, nrow(layout))), "null")})))
	    if(length(title) > 1){
	      ncols <- 1:ncol(layout)
	      for(i in seq(ncols)){
	        grid.text(title[i], 
	                  vp = viewport(layout.pos.row = 1, layout.pos.col = i),
	                  gp = gpar(fontsize = fontsize, fontfamily = fontfamily, fontface = fontface))
	      }
	    } else {
	      grid.text(title, 
	                vp = viewport(layout.pos.row = 1, layout.pos.col = 1:ncol(layout)),
	                gp = gpar(fontsize = fontsize, fontfamily = fontfamily, fontface = fontface))
	    }
	    for (i in 1:numPlots) {
	      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
	      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
	                                      layout.pos.col = matchidx$col))
	    }
	  }
	}	
	
	wt2.cor=function(x,wx,y,wy){
	  mu_x = sum(x * wx,na.rm=TRUE) / sum(wx,na.rm=TRUE)
	  mu_y = sum(y * wy,na.rm=TRUE) / sum(wy,na.rm=TRUE)
	  cov_xy = sum((x - mu_x) * (y - mu_y) * wx * wy,na.rm=TRUE) / 
	    sum(wx * wy,na.rm=TRUE)
	  cor_xy = cov_xy / (sqrt(sum((x - mu_x)^2 * wx,na.rm=TRUE) / 
	                            sum(wx,na.rm=TRUE)) * 
	                       sqrt(sum((y - mu_y)^2 * wy,na.rm=TRUE) / 
	                              sum(wy,na.rm=TRUE)))
	  cor_xy
	}