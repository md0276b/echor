# nearest neighbour distances, ids and min, mean, max 

nnd<-function(x,y) {
  # col j nearest to each line i
  d<-sqrt(outer(x,x,"-")*outer(x,x,"-")+
            outer(y,y,"-")*outer(y,y,"-"))
  diag(d)<-NA
  dd<-apply(d,1,min,na.rm=T)
  
  is.min=(d==dd) 
  
  ids<-outer(seq(length(x)),seq(length(x)),paste)
  diag(ids)<-NA
  nnd.ids<-ids[is.min]
  nnd.idsf=t(data.frame(strsplit(nnd.ids[!is.na(nnd.ids)],split=' ')))
  nnd.idsf=apply(nnd.idsf,2,as.numeric)
  
  list(d=d,dd=dd,nnd.idsf=nnd.idsf,rmdd=c(min(dd),mean(dd),max(dd)) )
}
