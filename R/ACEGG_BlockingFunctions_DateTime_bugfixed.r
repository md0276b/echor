### Bug fixed thanks to Maria Santos
### The bug came from excluind NA values in the data, which did not percolate to time and date data.
### Fixes are to be found on lines 21-22, 27 & 38 below.

find_saturation <- function(x,my_quantile){
  x <- x[x>0]
  return(as.numeric(quantile(x,probs=my_quantile,na.rm=TRUE)))
}

make_blocking <- function(my_data, # data frame for a given year
                          select, # 1st column SURVEY, 2nd Year, 3rd Lat, 4th Lon and 5th Response Variable
                          var_name,
                          tab_name,
                          saturation, # high values are truncated
                          append_file,
                          my_date=NULL,
                          my_time=NULL,p=0.98,
                          discrete=FALSE,zfilter='none',
                          xycor=FALSE,centerit=FALSE,mini=0,maxi=1,
                          w=NULL){
  
  my_data <- my_data[,select]
  names(my_data) <- c("Survey","Year","Lat","Long","Y")
  k <- which(is.na(my_data$Y)==FALSE)
  my_data <- my_data[k,]
  xd <- my_data$Long
  yd <- my_data$Lat
  zd <- my_data$Y
  if (!is.null(w)){
    ws=w[k]
    if (length(ws)!=dim(my_data)[1]){
      stop("Data and weights must have same length")
    }
  }else{
    ws=NULL
  }

  if(!is.null(my_date)){
    my_date <- my_date[k]
    if(!is.numeric(my_date)){
      my_date <- as.character(my_date) 
      wd <- sapply(1:length(my_date),function(ii){as.numeric(julian(ymd(my_date)[ii])[1])}) 
    }else{ wd <- my_date }
    matzday <- make_matrix_mean(n_iter=ni,cbind(xd,yd,wd),mini=mini,maxi=maxi)
    matzday <- round(as.vector(matzday),0)
    matsday <- make_matrix_sd(n_iter=ni,cbind(xd,yd,wd),mini=mini,maxi=maxi)
    matsday <- round(as.vector(matsday),0)
  }else{ matzday <- matsday <- rep(NA,length(as.vector(matig))) }

  if(!is.null(my_time)){
    my_time <- my_time[k]
    if(!is.numeric(my_time)){ my_time <- as.character(my_time) ; vd <- hour(hms(my_time))+minute(hms(my_time))/60 }
    else{ vd <- my_time }
    matztime <- make_matrix_mean(n_iter=ni,cbind(xd,yd,vd),mini=mini,maxi=maxi)
    matztime <- round(as.vector(matztime),2)
    matstime <- make_matrix_sd(n_iter=ni,cbind(xd,yd,vd),mini=mini,maxi=maxi)
    matstime <- round(as.vector(matstime),2)
  }else{ matztime <- matstime <- rep(NA,length(as.vector(matig))) }

  survey <- NULL
  survey_name <- unique(my_data$Survey)[order(unique(my_data$Survey))]
  for(i in 1:length(survey_name)){
    if(i==1){survey <- survey_name[i]}
    else{survey <- paste(survey,survey_name[i],sep="/")}
  }
  
  if (!discrete){
    zd[zd<0] <- NA     # do not allow negative values
    zd[zd>saturation] <- saturation   # saturation : high values are truncated
  }

  matn <- make_matrix_nb(n_iter=ni,xy=cbind(xd,yd),mini=mini,maxi=maxi)
  matx <- make_matrix_x(n_iter=ni,xy=cbind(xd,yd),mini=mini,maxi=maxi)
  maty <- make_matrix_y(n_iter=ni,xy=cbind(xd,yd),mini=mini,maxi=maxi)
  if (!discrete){
    matz <- make_matrix_mean(n_iter=ni,cbind(xd,yd,zd),
                             mini=mini,maxi=maxi)
    mats <- make_matrix_sd(n_iter=ni,cbind(xd,yd,zd),
                           mini=mini,maxi=maxi)
    if (!is.null(ws)){
      matwz <- make_matrix_wmean(n_iter=ni,xyz=cbind(xd,yd,zd,ws),
                             mini=mini,maxi=maxi)
    }
  }else{
    #matNmax <- make_matrix_Nmax(n_iter=ni,xyz=cbind(xd,yd,zd))
    matz <- make_matrix_Nmax(n_iter=ni,xyz=cbind(xd,yd,zd),mini=mini,
                             maxi=maxi)
    matz[matz=='NULL']=NA
    matz=apply(matz,2,as.numeric)
    mats <- make_matrix_sd(n_iter=ni,xyz=cbind(xd,yd,zd),mini=mini,
                           maxi=maxi)
  }
  # Build final dataframe
  # matig, matjg, matxg, matyg produced by define.grid.poly.mask function
  if (is.null(w)){
    db <- data.frame(Survey=rep(survey,length(as.vector(matig))),
                     Year=rep(unique(my_data$Year),
                              length(as.vector(matig))),
                     I=as.vector(matig),
                     J=as.vector(matjg),
                     Xgd=as.vector(matxg),
                     Ygd=as.vector(matyg),
                     Xsample=round(as.vector(matx),4),
                     Ysample=round(as.vector(maty),4),
                     DateAver=matzday,
                     DateStdev=matsday,
                     TimeAver=matztime,
                     TimeStdev=matstime,
                     Nsample=round(as.vector(matn),1),
                     Zvalue=as.vector(matz),
                     Zstdev=as.vector(mats))
  }else{
    db <- data.frame(Survey=rep(survey,length(as.vector(matig))),
                     Year=rep(unique(my_data$Year),
                              length(as.vector(matig))),
                     I=as.vector(matig),
                     J=as.vector(matjg),
                     Xgd=as.vector(matxg),
                     Ygd=as.vector(matyg),
                     Xsample=round(as.vector(matx),4),
                     Ysample=round(as.vector(maty),4),
                     DateAver=matzday,
                     DateStdev=matsday,
                     TimeAver=matztime,
                     TimeStdev=matstime,
                     Nsample=round(as.vector(matn),1),
                     Zvalue=as.vector(matwz),
                     Zstdev=as.vector(mats))
  }  
  if (centerit){
    db$Xgd=db$Xgd+(ax/2)
    db$Ygd=db$Ygd+(ax/2)
  }

  # Translate coordinates
  db2=db
  db2$Xgd2=db2$Xgd+ax*floor(u/2)
  db2$Ygd2=db2$Ygd+ay*floor(u/2)
  # Remove cells out of bounds
  db2=db2[db2$Xgd2<=x2&db2$Ygd2<=y2,]
  names(db2)
  db3=merge(db2[,c("Xgd2","Ygd2","Xsample","Ysample","DateAver","DateStdev",
                     "TimeAver","TimeStdev","Nsample","Zvalue","Zstdev",
                     "Year")],
             db[,c("Survey","Year","I","J","Xgd","Ygd")],
             by.x=c("Xgd2","Ygd2","Year"),
            by.y=c("Xgd","Ygd","Year"),
             all.y=TRUE)  # all.y to add missing translated cells in db2
  dim(db);dim(db2);dim(db3)
  names(db3)[names(db3)%in%c("Xgd2","Ygd2")]=c("Xgd","Ygd")
  # set db3 columns order as in db
  db3=db3[,names(db)]
  db3=db3[order(db3$Year,db3$J,db3$I),]
  head(db3)
  # Remove cells outside polygon defined by c(xpol,ypol)
  sel=inout(pts=cbind(db3$Xgd,db3$Ygd),poly=cbind(xpol,ypol),
            bound=T,quiet=T)
  db3[!sel,7:15]=NA
  if (centerit){
    db3$Xgd=db3$Xgd+(ax/2)
    db3$Ygd=db3$Ygd+(ax/2)
  }
  
  ####
  # SAVE grid file
  #  db file name specific for each year
  ####
  if (zfilter=='quantile'){
    filterid=paste("q_",substr(paste(p),3,4),sep='')
  }else if (zfilter=='none'){
    filterid=zfilter
  }else if (zfilter=='cumFreq'){
    filterid=zfilter
  }
  resname <- paste(getwd(),"/",paste("grid_",tab_name,"_",var_name,"_Filter",filterid,".txt",sep=""),sep="")
  
  if (xycor){
    dbo=db3
  }else{
    dbo=db
  }
  write.table(dbo,resname,sep=";",
              row.names=FALSE,
              col.names=ifelse(append_file,FALSE,TRUE),
              append=ifelse(append_file,TRUE,FALSE)
  )
}

# draw random x,y locations and calculate number of samples

vecn <- function(xd,yd,mini=0,maxi=1){
  # draw random x location:
  x0 <- x1+u*ax*runif(1,min=mini,max=maxi) 
  # draw random y location:
  y0 <- y1+u*ay*runif(1,min=mini,max=maxi)
  # propagate random shift on all x-values to shift grid 
  # nx: no. of lags, ax:lags 
  # from define.grid.poly.mask function):
  xi <- x0+(0:nx)*ax # on x
  # propagate random shift on all y-values to shift grid
  yi <- y0+(0:ny)*ay # on y
  return(as.vector(block.nb.f(xd,yd,xi,yi)))
}

# draw random x,y locations and calculate mean z values

vecz <- function(xd,yd,zd,mini=0,maxi=1){
  x0 <- x1+u*ax*runif(1,min=mini,max=maxi)
  y0 <- y1+u*ay*runif(1,min=mini,max=maxi)
  xi <- x0+(0:nx)*ax # on x
  yi <- y0+(0:ny)*ay # on y
  return(as.vector(block.mean.f(xx=xd,yy=yd,zz=zd,xxg=xi,yyg=yi)))
}

# draw random x,y locations and calculate weighted mean z values

vecwz <- function(xd,yd,zd,ws,mini=0,maxi=1){
  # Draw one random location for grid x origin within mini and maxi lags
  x0 <- x1+u*ax*runif(1,min=mini,max=maxi)
  # Draw one random location for grid y origin within mini and maxi lags
  y0 <- y1+u*ay*runif(1,min=mini,max=maxi)
  xi <- x0+(0:nx)*ax # grid origin random location on x
  yi <- y0+(0:ny)*ay # grid origin random location on y
  return(as.vector(block.wmean.f(xx=xd,yy=yd,zz=zd,ww=ws,
                                 xxg=xi,yyg=yi)))
}

# draw random x,y locations and calculate max no. of samples

vecNz <- function(xd,yd,zd,mini=0,maxi=1){
  x0 <- x1+u*ax*runif(1,min=mini,max=maxi)
  y0 <- y1+u*ay*runif(1,min=mini,max=maxi)
  xi <- x0+(0:nx)*ax # on x
  yi <- y0+(0:ny)*ay # on y
  return(as.vector(block.Nmax.f(xx=xd,yy=yd,zz=zd,xxg=xi,yyg=yi)))
}

# draw random x,y locations and calculate mean x values

vecx <- function(xd,yd,mini=0,maxi=1){
  x0 <- x1+u*ax*runif(1,min=mini,max=maxi)
  y0 <- y1+u*ay*runif(1,min=mini,max=maxi)
  xi <- x0+(0:nx)*ax # on x
  yi <- y0+(0:ny)*ay # on y
  return(as.vector(block.mean.f(xd,yd,xd,xi,yi)))
}

# draw random x,y locations and calculate mean y values

vecy <- function(xd,yd,mini=0,maxi=1){
  x0 <- x1+u*ax*runif(1,min=mini,max=maxi)
  y0 <- y1+u*ay*runif(1,min=mini,max=maxi)
  xi <- x0+(0:nx)*ax # on x
  yi <- y0+(0:ny)*ay # on y
  return(as.vector(block.mean.f(xd,yd,yd,xi,yi)))
}

# draw random x,y locations and calculate SD values

vecs <- function(xd,yd,zd,mini=0,maxi=1){
  x0 <- x1+u*ax*runif(1,min=mini,max=maxi)
  y0 <- y1+u*ay*runif(1,min=mini,max=maxi)
  xi <- x0+(0:nx)*ax # on x
  yi <- y0+(0:ny)*ay # on y
  return(as.vector(block.sdev.f(xd,yd,zd,xi,yi)))
}

make_matrix_nb <- function(n_iter,xy,mini=0,maxi=1){
  xd <- xy[,1]
  yd <- xy[,2]
  return(matrix(apply(replicate(n_iter,vecn(xd,yd,mini=mini,maxi=maxi)),1,mean,na.rm=TRUE),
                nrow=nx,ncol=ny)
  )
}

make_matrix_x <- function(n_iter,xy,mini=0,maxi=1){
  xd <- xy[,1]
  yd <- xy[,2]
  return(matrix(apply(replicate(n_iter,vecx(xd,yd,mini=mini,maxi=maxi)),1,mean,na.rm=TRUE),
                nrow=nx,ncol=ny))
}

make_matrix_y <- function(n_iter,xy,mini=0,maxi=1){
  xd <- xy[,1]
  yd <- xy[,2]
  return(matrix(apply(replicate(n_iter,vecy(xd,yd,mini=mini,maxi=maxi)),1,mean,na.rm=TRUE),
                nrow=nx,ncol=ny))
}

make_matrix_mean <- function(n_iter,xyz,mini=0,maxi=1){
  xd <- xyz[,1]
  yd <- xyz[,2]
  zd <- xyz[,3]
  return(matrix(
    apply(replicate(n_iter,vecz(xd,yd,zd,mini=mini,maxi=maxi)),1,
          mean,na.rm=TRUE),nrow=nx,ncol=ny))
}

make_matrix_wmean <- function(n_iter,xyz,mini=0,maxi=1){
  xd <- xyz[,1]
  yd <- xyz[,2]
  zd <- xyz[,3]
  ws <- xyz[,4]
  return(matrix(
    apply(replicate(n_iter,vecwz(xd,yd,zd,ws=ws,mini=mini,maxi=maxi)),
          1,mean,na.rm=TRUE),nrow=nx,ncol=ny))
}

make_matrix_Nmax <- function(n_iter,xyz,mini=0,maxi=1){
  xd <- xyz[,1]
  yd <- xyz[,2]
  zd <- xyz[,3]
  return(matrix(apply(
    replicate(n_iter,vecNz(xd,yd,zd,mini=mini,maxi=maxi)),1,getNmax),
                nrow=nx,ncol=ny))
}

make_matrix_sd <- function(n_iter,xyz,mini=0,maxi=1){
  xd <- xyz[,1]
  yd <- xyz[,2]
  zd <- xyz[,3]
  return(matrix(apply(replicate(n_iter,vecs(xd,yd,zd,mini=mini,maxi=maxi)),1,mean,na.rm=TRUE),
                nrow=nx,ncol=ny))
}

block.mean.f<-function(xx,yy,zz,xxg,yyg) { 
  ix <- cut(xx, xxg, include.lowest = TRUE)
  iy <- cut(yy, yyg, include.lowest = TRUE)
  z.mat <- tapply(zz, list(ix, iy), FUN = mean, na.rm=TRUE)
  return(z.mat)
}

block.wmean.f<-function(xx,yy,zz,xxg,yyg,ww) { 
  ix <- cut(xx, xxg, include.lowest = TRUE)
  iy <- cut(yy, yyg, include.lowest = TRUE)
  # Calculate weights denominator (sum of weighting variable in cell) 
  w.mat <- tapply(ww, list(ix, iy), FUN = sum,na.rm=TRUE)
  # Calculate weights numerator times response variable
  wzi.mat <- tapply(zz*ww, list(ix, iy), FUN = sum,
                   na.rm=TRUE)
  # Calculate weighted mean by dividing weights numerator times 
  # response variable by weights  denumerator 
  wz.mat=wzi.mat/w.mat
  return(wz.mat)
}

block.Nmax.f<-function(xx,yy,zz,xxg,yyg) { 
  ix <- cut(xx, xxg, include.lowest = TRUE)
  iy <- cut(yy, yyg, include.lowest = TRUE)
  
  z.mat <- tapply(zz, list(ix, iy), FUN = getNmax, na.rm=TRUE)
  return(z.mat)
}

getNmax=function(x,na.rm=TRUE){
  if (na.rm){
    x=x[!is.na(x)]
  }
  tx=table(x)
  Nmax=names(tx)[tx==max(tx)]
  if (length(Nmax)>1){
    Nmax=paste(Nmax,collapse='&')
  }
  Nmax
}


block.sum.f<-function(xx,yy,zz,xxg,yyg) { 
  ix <- cut(xx, xxg, include.lowest = TRUE)
  iy <- cut(yy, yyg, include.lowest = TRUE)
  z.mat <- tapply(zz, list(ix, iy), FUN = sum, na.rm=TRUE)
  return(z.mat)
}

block.nb.f<-function(xx,yy,xxg,yyg) { 
  ix <- cut(xx, xxg, include.lowest = TRUE)
  iy <- cut(yy, yyg, include.lowest = TRUE)
  z.mat <- tapply(xx, list(ix, iy), FUN = length)
  return(z.mat)
}

block.sdev.f<-function(xx,yy,zz,xxg,yyg) { 
  ix <- cut(xx, xxg, include.lowest = TRUE)
  iy <- cut(yy, yyg, include.lowest = TRUE)
  z.mat <- tapply(zz, list(ix, iy), FUN = var, na.rm=TRUE)
  return(sqrt(z.mat))
}

make_blocking.discrete <- function(my_data, # data frame for a given year
                          select, # 1st column SURVEY, 2nd Year, 3rd Lat, 4th Lon and 5th Response Variable
                          var_name,
                          tab_name,
                          saturation, # high values are truncated
                          append_file,
                          my_date=NULL,
                          my_time=NULL,p=0.98,
                          xycor=TRUE,mini=0,maxi=1){
  
  my_data <- my_data[,select]
  names(my_data) <- c("Survey","Year","Lat","Long","Y")
  k <- which(is.na(my_data$Y)==FALSE)
  my_data <- my_data[k,]
  xd <- my_data$Long
  yd <- my_data$Lat
  zd <- my_data$Y
  if(!is.null(my_date)){
    my_date <- my_date[k]
    if(!is.numeric(my_date)){ my_date <- as.character(my_date) ;
    wd <- sapply(1:length(my_date),function(ii){as.numeric(julian(ymd(my_date)[ii])[1])}) }
    else{ wd <- my_date }
    matzday <- make_matrix_mean(n_iter=ni,cbind(xd,yd,wd),mini=mini,maxi=maxi)
    matzday <- round(as.vector(matzday),0)
    matsday <- make_matrix_sd(n_iter=ni,cbind(xd,yd,wd),mini=mini,maxi=maxi)
    matsday <- round(as.vector(matsday),0)
  }
  else{ matzday <- matsday <- rep(NA,length(as.vector(matig))) }
  
  if(!is.null(my_time)){
    my_time <- my_time[k]
    if(!is.numeric(my_time)){ my_time <- as.character(my_time) ; vd <- hour(hms(my_time))+minute(hms(my_time))/60 }
    else{ vd <- my_time }
    matztime <- make_matrix_mean(n_iter=ni,cbind(xd,yd,vd),mini=mini,maxi=maxi)
    matztime <- round(as.vector(matztime),2)
    matstime <- make_matrix_sd(n_iter=ni,cbind(xd,yd,vd),mini=mini,maxi=maxi)
    matstime <- round(as.vector(matstime),2)
  }
  else{ matztime <- matstime <- rep(NA,length(as.vector(matig))) }
  
  survey <- NULL
  survey_name <- unique(my_data$Survey)[order(unique(my_data$Survey))]
  for(i in 1:length(survey_name)){
    if(i==1){survey <- survey_name[i]}
    else{survey <- paste(survey,survey_name[i],sep="/")}
  }
  
  zd[zd<0] <- NA     # do not allow negative values
  zd[zd>saturation] <- saturation      # saturation : high values are truncated
  
  matn <- make_matrix_nb(n_iter=ni,cbind(xd,yd),mini=mini,maxi=maxi)
  matx <- make_matrix_x(n_iter=ni,cbind(xd,yd),mini=mini,maxi=maxi)
  maty <- make_matrix_y(n_iter=ni,cbind(xd,yd),mini=mini,maxi=maxi)
  matz <- make_matrix_mean(n_iter=ni,cbind(xd,yd,zd),mini=mini,maxi=maxi)
  mats <- make_matrix_sd(n_iter=ni,cbind(xd,yd,zd),mini=mini,maxi=maxi)
  
  db <- data.frame(Survey=rep(survey,length(as.vector(matig))),
                   Year=rep(unique(my_data$Year),length(as.vector(matig))),
                   I=as.vector(matig),
                   J=as.vector(matjg),
                   Xgd=as.vector(matxg),
                   Ygd=as.vector(matyg),
                   Xsample=round(as.vector(matx),4),
                   Ysample=round(as.vector(maty),4),
                   DateAver=matzday,
                   DateStdev=matsday,
                   TimeAver=matztime,
                   TimeStdev=matstime,
                   Nsample=round(as.vector(matn),1),
                   Zvalue=as.vector(matz),
                   Zstdev=as.vector(mats)
  )

  # Translate coordinates
  db2=db
  db2$Xgd2=db2$Xgd+ax*floor(u/2)
  db2$Ygd2=db2$Ygd+ay*floor(u/2)
  # Remove cells out of bounds
  db2=db2[db2$Xgd2<=x2&db2$Ygd2<=y2,]
  names(db2)
  db3=merge(db2[,c("Xgd2","Ygd2","Xsample","Ysample","DateAver","DateStdev",
                   "TimeAver","TimeStdev","Nsample","Zvalue","Zstdev",
                   "Year")],
            db[,c("Survey","Year","I","J","Xgd","Ygd")],
            by.x=c("Xgd2","Ygd2","Year"),
            by.y=c("Xgd","Ygd","Year"),
            all.y=TRUE)  # all.y to add missing translated cells in db2
  dim(db);dim(db2);dim(db3)
  names(db3)[names(db3)%in%c("Xgd2","Ygd2")]=c("Xgd","Ygd")
  # set db3 columns order as in db
  db3=db3[,names(db)]
  db3=db3[order(db3$Year,db3$J,db3$I),]
  head(db3)
  # Remove cells outside polygon
  sel=inout(pts=cbind(db$Xgd,db$Ygd),poly=cbind(xpol,ypol),bound=T,quiet=T)
  db[!sel,7:15]=NA
  
  ####
  # SAVE grid file
  #  db file name specific for each year
  ####
  resname <- paste(getwd(),"/",paste("grid_","q_",substr(paste(p),3,4),"_",tab_name,"_",var_name,".txt",sep=""),sep="")
  
  if (xycor){
    dbo=db3
  }else{
    dbo=db
  }
  write.table(dbo,resname,sep=";",
              row.names=FALSE,
              col.names=ifelse(append_file,FALSE,TRUE),
              append=ifelse(append_file,TRUE,FALSE)
  )
}

