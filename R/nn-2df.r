	nd.2df=function(backgrnd,foregrnd,xname='x',yname='y'){

		backgrnd.xy.sp=c(apply(backgrnd[,c(xname,yname)],1,c))
		foregrnd.xy.sp=c(apply(foregrnd[,c(xname,yname)],1,c))

		#------------------------
		#computes nnd between esu and sla
		#------------------------	
		library(splancs)

		s1=spoints(backgrnd.xy.sp)
		s2=spoints(foregrnd.xy.sp)
				
		nd=n2dist(s1,s2)
		nd.nr=nd$neighs
		nd.nd=nd$dists

	list(nn.ID=nd.nr,nn.dist=nd.nd)}
