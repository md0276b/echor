#distance between adjacent points (x,y)

	d2p.proc=function(x,y){
		n=length(x)
		d2p=rep(0,(n-1))
		for (i in 1:(n-1)){
			d2p[i]=sqrt((x[i]-x[i+1])^2+(y[i]-y[i+1])^2)
		}
	d2p
	}		
