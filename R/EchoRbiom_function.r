#-------------------------------------
# Fish acoustic biomass assessment
#-------------------------------------
#--------------------------------------------------------------------------------
# EchoRbiom: acoustics+catches=biomass
#-----------  
# Developer: Mathieu.Doray@ifremer.fr since 11/02/2010
# Original core code by Pierre Petitgas, 02/06/2003
#--------------------------------------------------------------------------------

EchoR.biom=function(cruise,fishRview=FALSE,
                    path.EspDev=NULL,path.cas=NULL,path.man=NULL,path.raptri=NULL,path.raptri.pro=NULL,
                    path.metapro=NULL,path.mens=NULL,path.mens.pro=NULL,path.TS=NULL,
                    path.export=NULL,stp.events=c('HQ','DPLON'),nid='NOCHAL',appendit=FALSE,raptri.fusion=0,
                    WL.recompute=3,pradius=0.5,smax=1,path.discard=NULL,datestart=NULL,dateend=NULL,
                    totalSamples=NULL,subSamples=NULL,acou=NULL,echotypes=NULL,convert.header=FALSE,
                    sep=';',plotit=TRUE,complementit=TRUE,ux11=FALSE,hradius=6,path.results=NULL,
                    link=list(near=TRUE,ref=FALSE),showlink='near',add.charef=FALSE,xlim=NULL,ylim=NULL,
                    legpos='topleft',radSA=0.05,scale.SA=2,logit=TRUE,labelit=TRUE,saveIt=TRUE,
                    save.plot=TRUE,sp.sel=FALSE,polygoni=NULL,projectit=TRUE,bathy=FALSE,
                    ylat=NULL,sp1='ENGR-ENC',sp2='SARD-PIL'){
  
  #Load EchoR package
  #-------------------------------------------------------------------------
  #library(EchoR)
  
  if (fishRview){
    #*******************************************************************************************************
    # STEP 0: IMPORT AND FUSION RAW DATA
    #*******************************************************************************************************
    #---------------------------------------------
    #Run FishRview
    #--------------
    #With fishing data in 2 .txt files and uncomplete metadata
    raptri.fusion=0
    FishRview.res=FishRview(path.EspDev=path.EspDev,path.cas=path.cas,
                            path.man=path.man,path.raptri=path.raptri,path.raptri.pro=path.raptri.pro,
                            path.mens=path.mens,path.mens.pro=path.mens.pro,
                            path.export=path.export,cruise=cruise,stp.events=stp.events,
                            nid=nid,appendit=appendit,path.TS=path.TS,raptri.fusion=raptri.fusion,
                            WL.recompute=WL.recompute,pradius=pradius,path.discard=path.discard,datestart=datestart,
                            dateend=dateend)
    names(FishRview.res)
    Peche=FishRview.res$Peche
    Peche.wide=FishRview.res$Peche.wide
    chaman=FishRview.res$chaman
    EspDev=FishRview.res$EspDev
    mens=FishRview.res$mens
    
  }else{
    Peche=totalSamples
    Peche.wide=NULL
    chaman=acou
    EspDev=echotypes
    mens=subSamples
  }
  
  #*******************************************************************************************************
  # STEP 1: IMPORT AND CHECK DATA
  #*******************************************************************************************************
  # Import and check data:
  #------------------------
  if (fishRview){
    totalSamples=Peche
    subSamples=mens
    echotypes=EspDev
    acou=chaman
  }    
  echorbiom1=EchoRbiom.import(fishRview=fishRview,totalSamples=totalSamples,subSamples=subSamples,acou=acou,
                              echotypes=echotypes,convert.header=convert.header,sep=sep,plotit=plotit,
                              cruise=cruise,complementit=complementit,nid=nid,ux11=ux11)
  names(echorbiom1)
  Pechef=echorbiom1$Pechef
  Pechels=echorbiom1$Pechels
  Pechels2=echorbiom1$Pechels2
  mens=echorbiom1$mens
  EspDev=echorbiom1$EspDev
  ESDUDEVs=echorbiom1$ESDUDEVs
 
  #*******************************************************************************************************
  # STEP 2: PRE-PROCESSING: COMPUTE AND CHECK XE SCALING FACTORS, DEFINE AND CHECK ESDU~HAULS ASSOCIATIONS
  #*******************************************************************************************************
  if (!(link$near)&!(link$ref)){
    stop('Please select an ESDU-haul association strategy')
  }
  
  echorbiom2=EchoRbiom.preproc(ESDUDEVs=ESDUDEVs,Pechef=Pechef,Pechels=Pechels,EspDev=EspDev,hradius=hradius,
                               plotit=plotit,nid=nid,
                               path.results=path.export,link=link,showlink=showlink,cruise=cruise,
                               add.charef=add.charef,xlim=xlim,ylim=xlim,legpos=legpos,radSA=radSA,pradius=pradius,
                               scale.SA=scale.SA,logit=logit,labelit=labelit,ux11=ux11)
  names(echorbiom2)
  
  list.XeB=echorbiom2$list.XeB
  list.XeN=echorbiom2$list.XeN 
  XeB.df=echorbiom2$XeB.df
  XeN.df=echorbiom2$XeN.df
  ESDUDEVs2=echorbiom2$ESDUDEVs
  list.Assoc.year.near=echorbiom2$list.Assoc.year.near
  list.Assoc.year.ref=echorbiom2$list.Assoc.year.ref
  list.mSa.radius.db=echorbiom2$list.mSa.radius.db
  
  #*******************************************************************************************************
  # STEP 3: BIOMASS PER ESDU: COMPUTE BIOMASS PER ESDU AND SPECIES AND TOTAL BIOMASS ESTIMATES IN ESTIMATION POLYGON
  #*******************************************************************************************************
  if(link$near){
    list.Assoc=list.Assoc.year.near
  }else if(link$ref){
    list.Assoc=list.Assoc.year.ref
  }  
  
  echorbiom3=EchoRbiom.BiomassEsdu(EspDev=EspDev,ESDUDEVs=ESDUDEVs2,Pechef=Pechef,list.Assoc=list.Assoc,
                                   list.XeB=list.XeB,list.XeN=list.XeN,
                                   saveIt=saveIt,save.plot=save.plot,sp.sel=sp.sel,plotit=plotit,
                                   path.export=path.export,pradius=pradius,smax=smax,legpos=legpos,ux11=ux11,
                                   logit=logit,polygoni=polygoni,projectit=projectit,bathy=bathy,
                                   ylat=ylat,cruise=cruise,sp1=sp1,sp2=sp2)
  
  names(echorbiom3)
  B.esdu.sp.res=echorbiom3$B.esdu.sp.res
  B.dev.sp.esdu.df=echorbiom3$B.dev.sp.esdu.df
  BB.dev.sp.esdu.df.wide=echorbiom3$BB.dev.sp.esdu.df.wide
  BN.dev.sp.esdu.df.wide=echorbiom3$BN.dev.sp.esdu.df.wide
  B.sp.esdu.df=echorbiom3$B.sp.esdu.df
  mB.sp.esdu.df=echorbiom3$mB.sp.esdu.df
  mB.spsize.df=echorbiom3$mB.spsize.df
  B.sp.esdu.df.AN=echorbiom3$B.sp.esdu.df.AN
  B.sp.esdu.df.SA=echorbiom3$B.sp.esdu.df.SA
  biomtot.sp.area=echorbiom3$biomres.sp.esdu
  
  list(Peche=Peche,Peche.wide=Peche.wide,chaman=chaman,EspDev=EspDev,mens=mens,Pechef=Pechef,Pechels=Pechels,
       Pechels2=Pechels2,ESDUDEVs=ESDUDEVs,list.XeB=list.XeB,list.XeN=list.XeN,XeB.df=XeB.df,XeN.df=XeN.df,
       ESDUDEVs2=ESDUDEVs2,list.Assoc.year.near=list.Assoc.year.near,list.Assoc.year.ref=list.Assoc.year.ref,
       list.mSa.radius.db=list.mSa.radius.db,B.esdu.sp.res=B.esdu.sp.res,B.dev.sp.esdu.df=B.dev.sp.esdu.df,
       BB.dev.sp.esdu.df.wide=BB.dev.sp.esdu.df.wide,BN.dev.sp.esdu.df.wide=BN.dev.sp.esdu.df.wide,
       B.sp.esdu.df=B.sp.esdu.df,mB.sp.esdu.df=mB.sp.esdu.df,mB.spsize.df=mB.spsize.df,
       B.sp.esdu.df.AN=B.sp.esdu.df.AN,B.sp.esdu.df.SA=B.sp.esdu.df.SA,biomtot.sp.area=biomtot.sp.area)
}
  
  
  
  EchoR.bioman=function(totalSamples,subSamples,echotypes,mantota.wide1,mantot1,cruise=NA,suffix=NA,nid='NOCHAL',
                        plotit=FALSE,fishdev=NULL){
    
    if (is.null(totalSamples)|is.null(subSamples)|is.null(mantota.wide1)|is.null(echotypes)){
      stop('Please provide valid acoustics, fishing and echotype data for biomass assessment')
    }
    if (is.null(fishdev)){
      fishdev=as.character(unique(echotypes$DEV))
    }
    #Convert mantota.wide to chaman format
    chaman1=mantota.wide2chaman(mantota.wide1=mantota.wide1,mantot1=mantot1,cruise=cruise,suffix=suffix,
                                fishdev) 
    
    rbiom1=EchoR.biom(cruise=cruise,nid=nid,totalSamples=totalSamples,subSamples=subSamples,acou=chaman1,
                      echotypes=echotypes,plotit=plotit,link=list(near=TRUE,ref=FALSE))

    names(rbiom1)
    B.esdu.sp.res=rbiom1$B.esdu.sp.res
    B.dev.sp.esdu.df=rbiom1$B.dev.sp.esdu.df
    BB.dev.sp.esdu.df.wide=rbiom1$BB.dev.sp.esdu.df.wide
    BN.dev.sp.esdu.df.wide=rbiom1$BN.dev.sp.esdu.df.wide
    B.sp.esdu.df=rbiom1$B.sp.esdu.df
    mB.sp.esdu.df=rbiom1$mB.sp.esdu.df
    mB.spsize.df=rbiom1$mB.spsize.df
    B.sp.esdu.df.AN=rbiom1$B.sp.esdu.df.AN
    B.sp.esdu.df.SA=rbiom1$B.sp.esdu.df.SA
    biomtot.sp.area=rbiom1$biomtot.sp.area
    
    list(B.esdu.sp.res=B.esdu.sp.res,B.dev.sp.esdu.df=B.dev.sp.esdu.df,
         BB.dev.sp.esdu.df.wide=BB.dev.sp.esdu.df.wide,BN.dev.sp.esdu.df.wide=BN.dev.sp.esdu.df.wide,
         B.sp.esdu.df=B.sp.esdu.df,mB.sp.esdu.df=mB.sp.esdu.df,mB.spsize.df=mB.spsize.df,
         B.sp.esdu.df.AN=B.sp.esdu.df.AN,B.sp.esdu.df.SA=B.sp.esdu.df.SA,biomtot.sp.area=biomtot.sp.area)
  }    
    
  