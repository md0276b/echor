# ********************
#Functions for acoustic biomass assessment
# ********************
#IMPORT
###
# ****************************
#Front-end functions
# ****************************

#path.XXX: absolute path to data
#year.sel: select year 'YY' or 'ALL' for all years
#catcruise: select only a cruise category: e.g. 'PEL' for PELGAS
#selstrate=c('CLAS','SURF','NULL')
#correctit=TRUE: attempt to correct fields with NA, 
#correctit=FALSE: remove fields with NA

acoubiom.data.import=function(dsource='Baracouda',path.tri=NULL,
	path.chdes=NULL,path.ESDUDEV=NULL,
	path.chref,path.EspDev,	path.TSk,path.zones=NULL,
	path.deszones=NULL,
	path.CHvalid=NULL,year.sel='ALL',catcruise=NA,Fishview.chadoc=TRUE,
	path.mensEsdu=NULL,path.size=NULL,path.size.weight=NULL,
	path.size.age=NULL,selstrate=c('CLAS','SURF','NULL'),correctit=TRUE){
	
	#import data in Fishview format (deprecated)
	if (dsource=='Fishview'){

		fv.imp=fishview.import(dsource,path.tri,path.chdes,path.ESDUDEV,
			path.chref,path.EspDev,	path.TSk,path.zones,
			path.deszones=NULL,path.CHvalid,year=year.sel,catcruise,
			Fishview.chadoc,path.mensEsdu,path.size,
			path.size.weight,path.size.age)

		TRI=fv.imp$TRI
		CHADOC=fv.imp$CHADOC
		ESDUDEV=fv.imp$ESDUDEV
		CHAREF=fv.imp$CHAREF
		EspDev=fv.imp$EspDev
		TSk=fv.imp$TSk
		EsduDevZones=fv.imp$EsduDevZones
		DesZones=fv.imp$DesZones
		mens=fv.imp$mens
		LW=fv.imp$LW
		LA=fv.imp$LA
		CHvalid=fv.imp$CHvalid

	}else if (dsource=='Baracouda'){

		# ***********************		
		#Haul catches
		# ***********************
		if (!is.null(path.tri)){
			captures.ALL=read.table(path.tri,header=TRUE,sep=';')
			head(captures.ALL)
			dim(captures.ALL);dim(unique(captures.ALL))
			ucALL=unique(captures.ALL[,c('CAMPAGNE','NOCHAL')])
			barplot(table(ucALL$CAMPAGNE)[table(ucALL$CAMPAGNE)>0],
				horiz=TRUE,las=2,main='No of hauls, all cruises')

			#Selection cruise selection
			# ***********************
			if (!is.na(catcruise)){
				captures=captures.ALL[substr(captures.ALL$CAMPAGNE,
					1,4)==paste(catcruise,'0',sep='')|
					substr(captures.ALL$CAMPAGNE,1,4)==
					paste(catcruise,'1',sep=''),]
			}else{
				captures=captures.ALL
			}
			if (year.sel!='ALL'){
				captures=captures[captures$CAMPAGNE==paste(catcruise,
					year.sel,sep=''),]
			}
			uc1=unique(captures[,c('CAMPAGNE','NOCHAL')])
			x11()
			barplot(table(uc1$CAMPAGNE)[table(uc1$CAMPAGNE)>0],
				horiz=TRUE,las=2,main='No of hauls, Pelgas cruises')
		
			#remove trash but not unidentified 
			#trash = "DECH-ET1" (remains from previous haul)
			#unidentified = c("DIVE-RS1","DIVE-RS2","DIVE-RS3","DIVE-RS4",
			#	"DIVE-RS5","DIVE-RS6", "DIVE-RS7")
			# ***********************-
			captures=captures[!captures$GENR_ESP%in%c("DECH-ET1"),]

			#Time correction
			# ***********************
			captures=correc.date(captures,ndatetime='DHTUDEB')

			#STRATE correction/selection
			# ***********************---
			captures$STRATE=substr(as.character(captures$STRATE),1,4)
			unique(captures$STRATE)
			captures=captures[captures$STRATE%in%selstrate,]
			#head(captures)
			#sort(unique(captures$GENR_ESP))			

			#Creates new factorial variable combining GENRE.ESP and 
			#STRATE (surface/bottom haul?) in df Peche et EspDev
			# ***********************---
			captures$CodEsp=as.factor(paste(as.character(captures$GENR_ESP),
				as.character(captures$STRATE),as.character(captures$SIGNEP),
				sep="-"))

			#Hauls position check
			#If correctit=TRUE, correct haul with similar positions by adding epsilon value (jitter)
			# ***********************---
			TRI=haul.positions.check(captures,correctit=correctit)
			sort(unique(TRI$GENR_ESP))
			TRI$CAMPAGNE=as.character(TRI$CAMPAGNE)
		}else{TRI=NULL}

		# ***********************
		#Biological sampling data import from Baracouda table 'MENSURATION' export
		# ***********************
		if (!is.null(path.size)){

			mens.ALL=read.table(path.size,header=TRUE,sep=';')

			#Cruise Selection
			# ***********************
			if (!is.na(catcruise)){
				mens=mens.ALL[substr(mens.ALL$CAMPAGNE,
					1,4)==paste(catcruise,'0',sep='')|
					substr(mens.ALL$CAMPAGNE,1,4)==
					paste(catcruise,'1',sep=''),]
			}else{
				mens=mens.ALL
			}
			if (year.sel!='ALL'){
				mens=mens[mens$CAMPAGNE==paste(catcruise,
					year.sel,sep=''),]
			}
			head(mens)
			#STRATE correction/selection
			# ***********************---
			mens$STRATE=substr(as.character(mens$STRATE),1,4)
			unique(mens$STRATE)
			mens=mens[mens$STRATE%in%selstrate,]

			mens$CodEsp=as.factor(paste(as.character(mens$GENR_ESP),
				as.character(mens$STRATE),as.character(mens$SIGNEP),
				sep="-"))

			#Remove length-class zero
			mens=mens[mens$TAILLE!=0,]

		}else{mens=NULL}

		# ***********************
		#Table with hauls selected for biomass assessment		
		# ***********************
		if (!is.null(path.keep)){
			CHvalid.ALL=read.table(path.keep,header=TRUE,sep=';')
			#Cruise Selection
			# ***********************
			if (!is.na(catcruise)){
				CHvalid=CHvalid.ALL[substr(CHvalid.ALL$CAMPAGNE,
					1,4)==paste(catcruise,'0',sep='')|
					substr(CHvalid.ALL$CAMPAGNE,1,4)==
					paste(catcruise,'1',sep=''),]
			}else{
				CHvalid=CHvalid.ALL
			}
			if (year.sel!='ALL'){
				CHvalid=CHvalid[CHvalid$CAMPAGNE==paste(catcruise,
					year.sel,sep=''),]
			}
			names(CHvalid)=c('CAMPAGNE','DATEHEURE','NOSTA','DEV',
				'CRUISE.REGION','DLAY','keep')
			CHvalid$DATEHEURE=as.character(CHvalid$DATEHEURE)
			CHvalid$CAMPAGNE=as.character(CHvalid$CAMPAGNE)
			CHvalid$CRUISE.REGION=as.character(CHvalid$CRUISE.REGION)
			ID2=unlist(strsplit(as.character(CHvalid$CRUISE.REGION),
				split='\\_'))
			CHvalid$Region=ID2[seq(2,length(ID2),2)]
			CHvalid=correc.date(df=CHvalid,ndatetime='DATEHEURE')
			CHvalid=merge(CHvalid,unique(TRI[,c('NOSTA','NOCHAL')]),
				by.x='NOSTA',by.y='NOSTA',all.x=TRUE)	
			no.haul=CHvalid[is.na(CHvalid$NOCHAL),]
			if (dim(no.haul)[1]>0){
				print(no.haul[is.na(no.haul$NOCHAL),])
				stop('Trawl haul missing in captures table')
				}
			head(CHvalid)
			CHAREF=reshape(CHvalid[,c('CAMPAGNE','NOCHAL','DATEHEURE',
				'DEV')],idvar=c('CAMPAGNE','DATEHEURE'),
				timevar="DEV",direction="wide")
			head(CHAREF)
			names(CHAREF)[-seq(2)]=paste(unique(CHvalid$DEV),'CREF',
				sep='.')
		}else{
			CHvalid=NULL
			CHAREF=NULL			
		}

		#CHvalidi=CHvalid[CHvalid$CAMPAGNE=='PEL09',]
		#CHvalidi[CHvalidi$DLAY=='SURF','Region']

		# ***********************
		#Echotype description
		# ***********************
		if (!is.null(path.EspDev)){

			EspDev.ALL=read.table(path.EspDev,header=TRUE,sep=';')
			head(EspDev.ALL)
			names(EspDev.ALL)=c('CAMPAGNE','DEV','STRATE','DESCR','NUM',
				'GENR_ESP')
			#Cruise Selection
			# ***********************
			if (!is.na(catcruise)){
				EspDev=EspDev.ALL[substr(EspDev.ALL$CAMPAGNE,
					1,4)==paste(catcruise,'0',sep='')|
					substr(EspDev.ALL$CAMPAGNE,1,4)==
					paste(catcruise,'1',sep=''),]
			}else{
				EspDev=EspDev.ALL
			}
			if (year.sel!='ALL'){
				EspDev=EspDev[EspDev$CAMPAGNE==paste(catcruise,
					year.sel,sep=''),]
			}
			EspDev$SIGNEP=0
			dim(EspDev)
			EspDevP=EspDev[EspDev$GENR_ESP=='TRAC-TRU',]
			EspDevP$SIGNEP='P'
			EspDevG=EspDev[EspDev$GENR_ESP=='TRAC-TRU',]
			EspDevG$SIGNEP='G'
			EspDev=rbind(EspDev,EspDevP,EspDevG)
			dim(EspDev)
			Nstrate=unique(EspDev$STRATE)
			(Ndev=sort(unique(as.numeric(substr(EspDev$DEV,2,3)))))
			#unique(EspDev$GENR_ESP)
			#Creates new factorial variable combining GENRE.ESP and 
			#STRATE (surface/bottom haul?) in df Peche et EspDev
			EspDev$CodEsp=as.factor(paste(as.character(EspDev$GENR_ESP),as.character(EspDev$STRATE),
				as.character(EspDev$SIGNEP),sep="-"))
		}else{EspDev=NULL}

		# ***********************
		#Acoustic density per echotype
		# ***********************
		if (!is.null(path.ESDUDEV)){
			#acoustic data	
			ESDUDEV.ALL=read.table(path.ESDUDEV,header=TRUE,sep=';')
			#head(ESDUDEV.ALL)

			#Cruise Selection
			# ***********************
			if (!is.na(catcruise)){
				ESDUDEV1=ESDUDEV.ALL[substr(ESDUDEV.ALL$CAMPAGNE,
					1,4)==paste(catcruise,'0',sep='')|
					substr(ESDUDEV.ALL$CAMPAGNE,1,4)==
					paste(catcruise,'1',sep=''),]
			}else{
				ESDUDEV1=ESDUDEV.ALL
			}
			if (year.sel!='ALL'){
				ESDUDEV1=ESDUDEV1[ESDUDEV1$CAMPAGNE==paste(catcruise,
					year.sel,sep=''),]
			}
			#formats date/time
			# ***********************
			ESDUDEV1$DATE=as.character(ESDUDEV1$DATE)
			ESDUDEV1$H=as.character(ESDUDEV1$H)
			ESDUDEV1$TC=paste(ESDUDEV1$DATE,ESDUDEV1$H)
			#convert positions
			ESDUDEV1$LONG=-coord2ddegree2(gsub('W','',ESDUDEV1$LGDMC))
			ESDUDEV1$LAT=coord2ddegree2(gsub('N','',ESDUDEV1$LATDMC))

			if (!is.null(path.keep)){
				ESDUDEV1=merge(ESDUDEV1,CHvalid[,c('DATEHEURE','DEV',
					'DLAY','Region')],by.x=c('TC','DEV'),
					by.y=c('DATEHEURE','DEV'),all.x=TRUE)
				no.region=ESDUDEV1[is.na(ESDUDEV1$Region),]
				uno.region=unique(ESDUDEV1[is.na(ESDUDEV1$Region),
					c('TC','DEV','LONG','LAT')])
				if (dim(uno.region)[1]>0){
					print(paste(uno.region$TC,
						uno.region$LIBELLE_DEVIATION))
					stop('ESDU:echotypes not allocated to any region')
				}
				dim(ESDUDEV1)
				dim(unique(ESDUDEV1))	
				head(ESDUDEV1)
				ESDUDEV.wide1=reshape(ESDUDEV1[,c('TC',
					'DLAY','Region')],idvar=c("TC"),
					timevar=c('DLAY'),direction="wide")
				head(ESDUDEV.wide1)
				names(ESDUDEV.wide1)[names(ESDUDEV.wide1)%in%
					c('Region.CLAS','Region.SURF')]=c('zonesCLAS',
					'zonesSURF')
			}

			#In wide format
			# ***********************
			ESDUDEV.wide=reshape(ESDUDEV1[,c('TC','DEV',
				'ENERGIE')],idvar=c("TC"),
				timevar=c("DEV"),direction="wide")
			names(ESDUDEV.wide)[-1]=as.character(unique(
				ESDUDEV1$DEV))
			repNA=function(x){
				x[is.na(x)]=0
				x}
			ESDUDEV.wide[,-1]=apply(ESDUDEV.wide[,-1],2,repNA)
			head(ESDUDEV.wide)
			ESDUDEV=merge(unique(ESDUDEV1[,c('CAMPAGNE','TC','LONG',
				'LAT','FLAG')]),ESDUDEV.wide,by.x='TC',by.y='TC')
			ESDUDEV$TOTAL=apply(ESDUDEV[,-seq(5)],1,sum)
			head(ESDUDEV)
			ESDUDEV$esdu.id=seq(dim(ESDUDEV)[1])
			if (!is.null(path.keep)){
				ESDUDEV=merge(ESDUDEV,ESDUDEV.wide1,by.x='TC',
					by.y='TC')
				head(ESDUDEV)
				ESDUDEV=merge(ESDUDEV,CHAREF,by.x=c('CAMPAGNE','TC'),
					by.y=c('CAMPAGNE','DATEHEURE'))
				head(ESDUDEV)
			}			
		}else{ESDUDEV=NULL}

		# ***********************
		#TS~length equations
		# ***********************
		if (!is.null(path.TSk)){
			TSk=read.table(path.TSk,header=TRUE,sep=';')
			head(TSk)
			#adds TS coefficients to Peche df
			if (!is.null(path.tri)){
				TRI=merge(TRI,TSk[TSk$Id_sondeur==1,c('Genre_esp','CAC','BAC')],
					by.x='GENR_ESP',by.y='Genre_esp',all.x=TRUE)
			}
			sort(unique(TRI$GENR_ESP))
		}

		# ***********************
		#Region~esdu~dev correspondence
		# ***********************
		if (!is.null(path.deszones)){
			strates.ALL=read.table(path.deszones,header=TRUE,
				sep=';')
			#Cruise Selection
			# ***********************
			if (!is.na(catcruise)){
				strates=strates.ALL[substr(strates.ALL$CAMPAGNE,
					1,4)==paste(catcruise,'0',sep='')|
					substr(strates.ALL$CAMPAGNE,1,4)==
					paste(catcruise,'1',sep=''),]
			}else{
				strates=strates.ALL
			}
			if (year.sel!='ALL'){
				strates=strates[strates$CAMPAGNE==paste(catcruise,
					year.sel,sep=''),]
			}
			ID2=unlist(strsplit(as.character(strates$ID),split='\\_'))
			strates$ID2=ID2[seq(2,length(ID2),2)]
			head(strates)
			DesZones=unique(strates[,c('CAMPAGNE','ID','ID2','TYPE',
				'ZONE','SURFACE')])
			names(DesZones)=c('CAMPAGNE','ID2','ID','TYPE','ZONE','AREA')			
			strates.wide=reshape(strates[,c('CAMPAGNE','TC','ID','TYPE')],
				,idvar=c('CAMPAGNE','TC'),timevar="TYPE",
				direction="wide")
			head(strates.wide)
			names(strates.wide)=gsub('ID.','zones',names(strates.wide))
			EsduDevZones=strates.wide
		}else {
			EsduDevZones=NULL
			DesZones=NULL
		}

		#mens/Esdu correspondance
		# ***********************

		if (!is.null(path.mensEsdu)){
			mensEsdu.ALL=read.csv(path.mensEsdu,header=TRUE,sep=';')
			mensEsdu=mensEsdu.ALL[mensEsdu.ALL$CAMPAGNE==paste('PEL',year.sel,sep=''),]
			mensEsdu=mensEsdu[mensEsdu$NOSTA!=0,]
			names(mensEsdu)[names(mensEsdu)=='CHAL_REF_Mensuration']='NOSTA.mens'
			rm(mensEsdu.ALL)
		}else{mensEsdu=NULL}

		#Length-weight equations
		# ***********************

		if (!is.null(path.size.weight)){
			LW.ALL=read.csv(path.size.weight,header=TRUE,sep=';')
			head(LW.ALL)
			LW=LW.ALL[LW.ALL$Campagne==paste('PEL',year.sel,sep=''),]
			head(LW)
			rm(LW.ALL)
		}else{LW=NULL}

		#Length-age equations
		# ***********************

		if (!is.null(path.size.age)){
			LA.ALL=read.csv(path.size.age,header=TRUE,sep=';')
			head(LA.ALL)
			LA=LA.ALL[LA.ALL$CAMPAGNE==paste('PEL',year.sel,sep=''),]
			head(LA)
			rm(LA.ALL)
		}else{LA=NULL}

		CHADOC=NULL
	}
	list(TRI=TRI,CHADOC=CHADOC,ESDUDEV=ESDUDEV,CHAREF=CHAREF,EspDev=EspDev,
		TSk=TSk,EsduDevZones=EsduDevZones,DesZones=DesZones,mens=mens,
		LW=LW,LA=LA,CHvalid=CHvalid)  
	}


# ***********************
#Low-level functions
# ***********************


	# ****************************
	#Toolbox
	# ****************************
	correc.date=function(df,ndatetime=NA,ndate=NA,ntime=NA){
    if (!is.na(ndatetime)){
      df[,ndatetime]=as.character(df[,ndatetime])
      df[nchar(df[,ndatetime])==17,ndatetime]=paste('0',
      df[nchar(df[,ndatetime])==17,ndatetime],sep='')
      df[nchar(df[,ndatetime])==18,ndatetime]=paste(paste(
			    substr(df[nchar(df[,ndatetime])==18,
          ndatetime],1,3),paste('0',substr(df[nchar(df[,ndatetime])==18,
          ndatetime],4,9),sep=''),sep=''),
          substr(df[nchar(df[,ndatetime])==18,
					ndatetime],11,18))
    }
    if (!is.na(ndate)){
	      df[,ndate]=as.character(df[,ndate])
        df[nchar(df[,ndate])==8,ndate]=paste('0',substr(
        df[nchar(df[,ndate])==8,ndate],1,2),'0',
          substr(df[nchar(df[,ndate])==8,ndate],3,8),sep='')
          }
    if (!is.na(ntime)){
        df[,ntime]=as.character(df[,ntime])
        df[nchar(df[,ntime])==4,ntime]=paste('0',substr(
          df[nchar(df[,ntime])==4,ntime],1,1),':',
          substr(df[nchar(df[,ntime])==4,ntime],3,4),':00',sep='')
        df[nchar(df[,ntime])==5,ntime]=paste(df[nchar(df[,ntime])==5,ntime],
          ':00',sep='')
        df[nchar(df[,ntime])==7,ntime]=paste('0',substr(
            df[nchar(df[,ntime])==8,ntime],1,2),'0',
            substr(df[nchar(df[,ntime])==8,ntime],3,8),sep='')
    }
    df
  }

	coord2ddegree2=function(x){
		x=as.numeric(as.character(x))
		deg=floor(x/100)
		minut=(x/100-deg)*100
		minut=minut/60
	cx=deg+minut
	cx
	}

	colmult=function(x){
			y=x[-1]*x[1]
			y
		}

	na.null=function(x){
		x[is.na(x)]=0
	x}

	NA.remove=function(x,sel=NULL){
		if (FALSE%in%is.na(x)){
			xs=x[!is.na(x)]
			if (!is.null(sel)){xs=xs[sel]}			
		}else{
			xs=NA
		}
	xs
	}


	get.metadata=function(EspDev){
		if (!is.null(EspDev)){
			lyears=unique(EspDev$CAMPAGNE)
			#Number of echotypes to be considered
			for (i in 1: length(lyears)){
				EspDevi=EspDev[EspDev$CAMPAGNE==lyears[i],]
				Ndevi=sort(unique(as.numeric(gsub('D','',EspDevi$DEV))))
				Ndevi=list(Ndevi)
				names(Ndevi)=lyears[i]
				if (i==1){
					lNdev=Ndevi
				}else lNdev=c(lNdev,Ndevi)
			}
		}else{
			stop('No input file')
		}			
	list(lyears=lyears,lNdev=lNdev)
	}


	# ****************************
	#Aggregates non-assessed species into a "complement" category
	# ****************************

	multi.complement.it=function(captures,EspDev,wmens=FALSE){

		#Aggregate all species not found in echotype definitions 
		#into 'complement'
		# ***********************
		lyears=unique(captures$CAMPAGNE)
		Nyears=length(lyears)
		captures$CAMPAGNE=as.character(captures$CAMPAGNE)
		EspDev$CAMPAGNE=as.character(EspDev$CAMPAGNE)

		for (i in 1:Nyears){
			capturesi=captures[captures$CAMPAGNE==lyears[i],]
			EspDevi=EspDev[EspDev$CAMPAGNE==lyears[i],]
			compi=complement(capturesi,EspDevi,wmens)
			compi$CAMPAGNE=lyears[i]
			if (i==1){
				captures.comp=compi
			}else{
				captures.comp=rbind(captures.comp,compi)
			}
		}
		dim(captures)
		dim(captures.comp)
		dim(unique(captures.comp))
		#names(captures);names(captures.comp)
	captures.comp	
	}	

#capturesi=Pechei
#EspDevi=EspDev

	complement=function(capturesi,EspDevi,wmens=FALSE){

		capturesi$complem=as.character(capturesi$GENR_ESP)
		# all species that are not in EspDev are complement
		capturesi$complem[!capturesi$GENR_ESP%in%unique(
			EspDevi$GENR_ESP[EspDevi$GENR_ESP!='COMP-LEM'])]='COMP-LEM'	
		capturesi$GENR_ESP=capturesi$complem
		capturesi[capturesi$GENR_ESP=='COMP-LEM','SIGNEP']='0'
		capturesi[capturesi$GENR_ESP=='COMP-LEM','LM']=20
		capturesi[capturesi$GENR_ESP=='COMP-LEM','MOULE']=20
		capturesi[capturesi$GENR_ESP=='COMP-LEM','PM']=0.05
		capturesi[capturesi$GENR_ESP=='COMP-LEM','CAC']=20
		capturesi[capturesi$GENR_ESP=='COMP-LEM','BAC']=67
		capturesi$CodEsp=as.factor(paste(as.character(capturesi$GENR_ESP),
			as.character(capturesi$STRATE),
			as.character(capturesi$SIGNEP),sep="-"))
		dim(capturesi);dim(unique(capturesi))
		head(capturesi)
		if (wmens){

			#Average length and weight of complement computed as the
			#average of species length and weight, 
			#weighted by species catch 
			#NT=sum(NTi) PT =sum(PTi)
			Ctot.NOCHAL=aggregate(capturesi$PT,list(capturesi$NOCHAL),
				sum)
			names(Ctot.NOCHAL)=c('NOCHAL','Ctot')
			dim(capturesi)
			Peche.precomp=merge(capturesi,Ctot.NOCHAL,by.x='NOCHAL',
				by.y='NOCHAL')
			dim(Peche.precomp)
			Peche.precomp$wLMi=Peche.precomp$LM*Peche.precomp$PT/
				Peche.precomp$Ctot
			Peche.precomp$wPMi=Peche.precomp$PM*Peche.precomp$PT/
				Peche.precomp$Ctot
			Peche.precomp$wMOULi=Peche.precomp$MOULE*Peche.precomp$PT/
				Peche.precomp$Ctot
			names(Peche.precomp)	
			Peche.comp=aggregate(Peche.precomp[,c('PT','PM','LM',
				'MOULE','NT','wPMi','wLMi','wMOULi')],
				list(Peche.precomp$NOCHAL,Peche.precomp$CodEsp),
				sum,na.rm=TRUE)
			#use mean length and weight, weighted by proportions of initial species, 
			#for 'complement'
			head(Peche.comp)
			names(Peche.comp)[1:2]=c('NOCHAL','CodEsp')
			Peche.comp=merge(unique(Peche.precomp[,names(Peche.precomp)
				[!names(Peche.precomp)%in%c('PT','PM','LM',
				'MOULE','NT','wPMi','wLMi','wMOULi')]]),
				Peche.comp,by.x=c('NOCHAL','CodEsp'),by.y=c('NOCHAL',
				'CodEsp'))
			Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','LM']=
				Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','wLMi']
			Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','PM']=
				Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','wPMi']
			Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','MOULE']=
				Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','wMOULi']
		}else{
			#Average length of complement = 20
			#Average no. of fish per kg of complement = 20
			#CAC and BAC of complement: physoclystous fish
			#NT=sum(NTi) PT =sum(PTi)
			Peche.precomp=capturesi
			dim(Peche.precomp)
      if ('PTRI'%in%names(Peche.precomp)){
			  Peche.comp=aggregate(Peche.precomp[,c('PT','NT','PTRI')],
				  list(Peche.precomp$NOCHAL,Peche.precomp$CodEsp),sum,na.rm=TRUE)
      }else{
        Peche.comp=aggregate(Peche.precomp[,c('PT','NT')],
                             list(Peche.precomp$NOCHAL,Peche.precomp$CodEsp),
                             sum,na.rm=TRUE)
      }  
			names(Peche.comp)[1:2]=c('NOCHAL','CodEsp')	
			dim(Peche.comp);dim(unique(Peche.comp))
			head(Peche.comp)
			head(Peche.precomp)
			uprecomp=unique(Peche.precomp[,names(Peche.precomp)
				[!names(Peche.precomp)%in%c('PT','NT','PTRI','scat','PMENS')]])
      dim(uprecomp)
      head(uprecomp)      
			Peche.compf=merge(uprecomp,Peche.comp,
				by.x=c('NOCHAL','CodEsp'),by.y=c('NOCHAL',
				'CodEsp'))
			if(dim(Peche.compf)[1]!=dim(unique(Peche.compf))[1])
        stop('Invalid number of rows in complemented file')
			head(Peche.compf)
			Peche.compf=Peche.compf[order(Peche.compf$NOCHAL),]
			Peche.compf[Peche.compf$GENR_ESP=='COMP-LEM',]
		}
	Peche.compf
	}

	#Deprecated:

	multi.complement=function(k,Peche.ALL,EspDev.ALL,wmens=FALSE,lyears){

		Peche.ALL$CAMPAGNE=as.character(Peche.ALL$CAMPAGNE)
		EspDev.ALL$CRUISE=as.character(EspDev.ALL$CRUISE)

		Peche=Peche.ALL[Peche.ALL$CAMPAGNE==lyears[k],]
		EspDev=EspDev.ALL[EspDev.ALL$CRUISE==lyears[k],]

		Peche$complem=as.character(Peche$GENR_ESP)
		Peche$complem[!Peche$GENR_ESP%in%unique(EspDev$GENR_ESP[EspDev$GENR_ESP!=
			'COMP-LEM'])]='COMP-LEM'	
		Peche$GENR_ESP=Peche$complem
		Peche[Peche$GENR_ESP=='COMP-LEM','SIGNEP']='0'
		Peche[Peche$GENR_ESP=='COMP-LEM','LM']=20
		Peche[Peche$GENR_ESP=='COMP-LEM','MOULE']=20
		Peche$CodEsp=as.factor(paste(as.character(Peche$GENR_ESP),
			as.character(Peche$STRATE),as.character(Peche$SIGNEP),sep="-"))
		if (wmens){
			Ctot.NOCHAL=aggregate(Peche$PT,list(Peche$NOCHAL),sum)
			names(Ctot.NOCHAL)=c('NOCHAL','Ctot')
			dim(Peche)
			Peche.precomp=merge(Peche,Ctot.NOCHAL)

			dim(Peche.precomp)
			Peche.precomp$wLMi=Peche.precomp$LM*Peche.precomp$PT/Peche.precomp$Ctot
			Peche.precomp$wPMi=Peche.precomp$PM*Peche.precomp$PT/Peche.precomp$Ctot
			Peche.precomp$wMOULi=Peche.precomp$MOULE*Peche.precomp$PT/Peche.precomp$Ctot
			names(Peche.precomp)	
			Peche.comp=aggregate(Peche.precomp[,c('PT','PM','LM','MOULE','NT','wPMi','wLMi')],
				list(Peche.precomp$NOCHAL,Peche.precomp$CodEsp),sum,na.rm=TRUE)
			#use mean length and weight, weighted by proportions of initial species, 
			#for 'complement'
			head(Peche.comp)
			names(Peche.comp)[1:2]=c('NOCHAL','CodEsp')
			Peche.comp=merge(Peche.precomp[,c('NOCHAL','CodEsp','GENR_ESP','SIGNEP',
				'NOSTA','STRATE','LATF','LONF')],Peche.comp,by.x=c('NOCHAL','CodEsp'),
				by.y=c('NOCHAL','CodEsp'))
			Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','LM']=
				Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','wLMi']
			Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','PM']=
				Peche.comp[Peche.comp$GENR_ESP=='COMP-LEM','wPMi']
		}else{
			Peche.precomp=Peche
			Peche.comp=aggregate(Peche.precomp[,c('PT','NT')],
				list(Peche.precomp$NOCHAL,Peche.precomp$CodEsp),sum,na.rm=TRUE)
			#use mean length and weight for 'complement'
			head(Peche.comp)
			names(Peche.comp)[1:2]=c('NOCHAL','CodEsp')
			Peche.comp=merge(unique(Peche.precomp[,c('CAMPAGNE','NOCHAL','CodEsp','GENR_ESP',
				'SIGNEP','NOSTA','STRATE','LATF','LONF','LM','MOULE','CAC','BAC')]),
				Peche.comp,by.x=c('NOCHAL','CodEsp'),
				by.y=c('NOCHAL','CodEsp'))
			head(Peche.comp)
		}
	Peche.comp
	}


	fishview.import=function(dsource='Fishview',path.tri=NULL,
		path.chdes=NULL,path.ESDUDEV=NULL,
		path.chref,path.EspDev,	path.TSk,path.zones=NULL,
		path.deszones=NULL,
		path.CHvalid=NULL,year=NA,catcruise=NA,Fishview.chadoc=TRUE,
		path.mensEsdu=NULL,path.size=NULL,path.size.weight=NULL,
		path.size.age=NULL){

		if (Fishview.chadoc){
			#Requested column names in path.chdes (R name: CHADOC):
			#'LATFDD': haul latitude
			#'LGFDD': haul longitude
			#'NOSTA','NOCHAL','STRATE','TYPECHAL'

			if (!is.null(path.chdes)){
				CHADOC=read.csv(path.chdes,sep=';')
				CHADOC$LATF=CHADOC$LATFDD
				CHADOC$LONF=CHADOC$LGFDD

			#discard extra columns

				CHADOC=CHADOC[,c('NOSTA','NOCHAL','STRATE','TYPECHAL','LATF','LONF')]
			}else{CHADOC=NULL}
			#names(CHADOC)
		}

		#fishing data
		# ***********************
		#Requested column names in path.tri (PELXX_ds.dbf, R name: TRI):
		#'NOSTA': fishiung station ID

		if (!is.null(path.tri)){
			#TRI=read.dbf(path.tri)
			TRI=read.csv(path.tri,sep=',')
			#correspondence between 'NOSTA' and 'NOCHAL', haul positions...

			if (dsource=='Fishview'){
				TRI=merge(TRI,CHADOC[,
					c('NOSTA','NOCHAL','STRATE','TYPECHAL','LATF','LONF')],
					by.x='NOSTA',by.y='NOSTA')
			}
		#discard extra columns

			TRI=TRI[,c('GENR_ESP','SIGNEP','PT','PM','LM','MOULE','NOSTA','NOCHAL',
				'STRATE','LATF','LONF')]
		#names(TRI)
			if (!'NT'%in%names(TRI)){
				TRI$NT=TRI$PT/(TRI$PM/1000)
			}

		}else{TRI=NULL}

		#mens/Esdu correspondance
		# ***********************

		if (!is.null(path.mensEsdu)){
			mensEsdu.ALL=read.csv(path.mensEsdu,sep=',')
			mensEsdu=mensEsdu.ALL[mensEsdu.ALL$CAMPAGNE==paste('PEL',year,sep=''),]
			mensEsdu=mensEsdu[mensEsdu$NOSTA!=0,]
			names(mensEsdu)[names(mensEsdu)=='CHAL_REF_Mensuration']='NOSTA.mens'
			rm(mensEsdu.ALL)
		}else{mensEsdu=NULL}

		#Biological sampling data
		# ***********************

		if (!is.null(path.size)){
			mens.ALL=read.csv(path.size,sep=',')
			head(mens.ALL)
			mens=mens.ALL[mens.ALL$CAMPAGNE==paste('PEL',year,sep=''),]
			mens=mens[mens$NOSTA!=0,]
			head(mens)
			rm(mens.ALL)
		}else{mens=NULL}

		#Length-weight equations
		# ***********************

		if (!is.null(path.size.weight)){
			LW.ALL=read.csv(path.size.weight,sep=',')
			head(LW.ALL)
			LW=LW.ALL[LW.ALL$Campagne==paste('PEL',year,sep=''),]
			head(LW)
			rm(LW.ALL)
		}else{LW=NULL}

		#Length-age equations
		# ***********************

		if (!is.null(path.size.age)){
			LA.ALL=read.csv(path.size.age,sep=',')
			head(LA.ALL)
			LA=LA.ALL[LA.ALL$CAMPAGNE==paste('PEL',year,sep=''),]
			head(LA)
			rm(LA.ALL)
		}else{LA=NULL}

		#Species in deviations
		# ***********************
		#Requested column names in path.EspDev (R name: EspDev):
		#'CRUISE','DEV','STRATE','N','DESCR','NUM','GENR_ESP'

		EspDev.ALL=read.csv(path.EspDev,header=F,sep=',')

		#EspDev.ALL[1:10,]
		names(EspDev.ALL)=c('CRUISE','DEV','STRATE','DESCR','NUM','GENR_ESP')
		#
		EspDev=EspDev.ALL[EspDev.ALL$CRUISE==paste('PEL',year,sep=''),]
		#selection of species:size_class defined for the cruise
		EspDev=EspDev.ALL[EspDev.ALL$CRUISE==paste(catcruise,
			year,sep=''),]
		EspDev$SIGNEP=0
		Nstrate=unique(EspDev$STRATE)

		#Requested column names in path.ESDUDEV (PELXX_ec.dbf, R name: ESDUDEV):
		#'DATE', in fishview format: dd/mm/yy
		#'H' in fishview format: hh:mm:ss

		if (!is.null(path.ESDUDEV)){
			#acoustic data	
			#ESDUDEV=read.dbf(path.ESDUDEV)
			#
			ESDUDEV=read.csv(path.ESDUDEV,sep=',')
			head(ESDUDEV)
			#ESDUDEV[1:10,]
			#formats date/time
			ESDUDEV$DATE=as.character(ESDUDEV$DATE)
			ESDUDEV$H=as.character(ESDUDEV$H)
			ESDUDEV$TC=paste(paste(substr(as.character(ESDUDEV$DATE),1,6),paste('20',	
				substr(as.character(ESDUDEV$DATE),7,8),sep=''),sep=''),
				as.character(ESDUDEV$H))
			ESDUDEV$esdu.id=seq(dim(ESDUDEV)[1])
		#discard extra columns
			ESDUDEV=ESDUDEV[,c('TC','FLAG','LAT','LONG',
				as.character(unique(EspDev$DEV)),'TOTAL')]
		}else{ESDUDEV=NULL}
	
		#ref. hauls
		#Requested column names in path.chref (CHAREFXX, R name: CHAREF):
		#"D1"... "Dn" 

		if (!is.null(path.chref)){
			#CHAREF=read.dbf(path.chref)
			CHAREF=read.csv(path.chref)
			N=length(names(CHAREF))
			
			names(CHAREF)[names(CHAREF)%in%unique(EspDev$DEV)]=
				paste(names(CHAREF)[names(CHAREF)%in%unique(EspDev$DEV)],
				'CREF',sep='.')
			#CHAREF$D7.CREF=CHAREF$D8.CREF
			CHAREF$esdu.id=seq(dim(CHAREF)[1])
			#CHAREF[1:10,]
		#discard extra columns
			CHAREF=CHAREF[,c("DATEHEURE","LAT","LONG",
				paste(unique(EspDev$DEV),'CREF',sep='.'),"esdu.id" )]
		}else{CHAREF=NULL}

		#Requested column names in path.CHvalid ('SCENARIO.txt' R name: CHvalid):
		#'DATEHEURE','DEV','ID_EI','NOSTA','CRUISE.REGION','DLAY','keep','NOTE'

		#Table with hauls selected for biomass assessment		
		if (!is.null(path.CHvalid)){
			CHvalid.ALL=read.csv(path.CHvalid,sep=',',header=FALSE)
			names(CHvalid.ALL)=c('DATEHEURE','DEV','ID_EI','NOSTA','CRUISE.REGION',
				'DLAY','keep','NOTE')
			CHvalid.ALL$DATEHEURE=as.character(CHvalid.ALL$DATEHEURE)
			CHvalid.ALL$CRUISE=as.character(CHvalid.ALL$CRUISE)
			CHvalid.ALL$CRUISE.REGION=as.character(CHvalid.ALL$CRUISE.REGION)
			CHvalid.ALL$CRUISE=substr(CHvalid.ALL$CRUISE.REGION,1,5)
			CHvalid.ALL$Region=substr(CHvalid.ALL$CRUISE.REGION,7,8)
			CHvalid.ALL=correc.date(CHvalid.ALL,ndate='DATEHEURE')
			CHvalid=CHvalid.ALL[CHvalid.ALL$CRUISE==paste('PEL',year,sep=''),]
		
			CHvalid=merge(CHvalid,CHADOC[,c('NOSTA','NOCHAL')],by.x='NOSTA',
				by.y='NOSTA')
			unique(CHvalid[CHvalid$DLAY=='SURF','DEV'])
			names(CHvalid)
			#CHvalid[1:10,]
		}else{CHvalid=NULL}

		#TS equations
		TSk=read.csv(path.TSk,header=T,sep=',')

		#Region definition
		#Requested column names in path.deszones ('strateXX.dbf' R name: DesZones):
		#"ID"   "ZONE" "TYPE" "AREA" "D1"   "D2"   "D3"   "D4"   "D6"

		if (!is.null(path.zones)&!is.null(path.deszones)){
			#DesZones=read.dbf(path.deszones)
			DesZones=read.csv(path.deszones)			
			#DesZones$region=as.numeric(substr(DesZones$ID,7,8))
			if (is.null(DesZones$TYPE)){
				DesZones$TYPE='CLAS'
				dzs=DesZones
				dzs$TYPE='SURF'
				DesZones=rbind(DesZones,dzs)
				DesZones$ID=seq(dim(DesZones)[1])
				DesZones$ID2=rep(seq(dim(dzs)[1]),2)
			}else{
				DesZones$ID2=DesZones$ID}
			DesZones=DesZones[order(DesZones$ID),]

		#Region~esdu~dev correspondence
		#Requested column names in path.zones ('zonesuXX.dbf' R name: EsduDevZones):
		#  "DATEHEURE"  "Z1" ... "Zk"       

			#EsduDevZones=read.dbf(path.zones)
			EsduDevZones=read.csv(path.zones)
			dim(EsduDevZones)

			if (dim(EsduDevZones)[2]>8){
				N=dim(EsduDevZones)[2]
				narm.row=apply(EsduDevZones[,paste('Z',unique(DesZones$ID_EI),sep='')],
					1,NA.remove)
				Nzones.esdu=sapply(narm.row,length)

				if (TRUE%in%is.na(narm.row)){
					cat(paste('No region defined for esdu',
						row.names(EsduDevZones[is.na(narm.row),]),
						'- esdu removed','\n')) 
				}
				if (TRUE%in%(Nzones.esdu>2)){
					cat(paste('Invalid number of regions associated to esdu',
						row.names(EsduDevZones[Nzones.esdu>2,]),
						'- esdu removed','\n')) 
				}
				 
				EsduDevZones=EsduDevZones[!is.na(narm.row)&(Nzones.esdu<3),]
				lzones1=apply(EsduDevZones[,7:N],1,NA.remove,sel=1)
				#if surface and bottom echotypes
				if (length(Nstrate)>1){
					lzones2=apply(EsduDevZones[,paste('Z',unique(DesZones$ID),sep='')],1,NA.remove,sel=2)
					lzones2[is.na(lzones2)]=lzones1[lzones1%in%DesZones[DesZones$TYPE=='SURF','ID']]			
					lzones1[lzones1%in%DesZones[DesZones$TYPE=='SURF','ID']]=0
					EsduDevZones=data.frame(EsduDevZones,zonesCLAS=lzones1,
						zonesSURF=lzones2)
				}else{
				#if only bottom echotypes
				#sort CLAS and SURF regions
					lzones1[lzones1%in%DesZones[DesZones$TYPE=='SURF','ID']]=0
					EsduDevZones=data.frame(EsduDevZones,zonesCLAS=lzones1,
						zonesSURF=0)
					lzones2=rep(NA,length(lzones1))
				}
				if (0%in%unique(lzones1)){
					erow=DesZones[1,]
					erow$ID=0;erow$ZONE='CLAS non attribue';erow$TYPE='CLAS'
					erow$ID2=0;erow$AREA=0
					DesZones=rbind(DesZones,erow)
				}
				if (NA%in%(unique(lzones2))){
					erow=DesZones[1,]
					erow$ID=0;erow$ZONE='SURF non attribue';erow$TYPE='SURF'
					erow$ID2=0;erow$AREA=0
					DesZones=rbind(DesZones,erow)
				}
			}else if (dim(EsduDevZones)[2]==8){
				names(EsduDevZones)[names(EsduDevZones)=="CLAS"]="zonesCLAS"
				names(EsduDevZones)[names(EsduDevZones)=="SURF"]="zonesSURF"
			}
		}else {
			EsduDevZones=NULL
			DesZones=NULL
		}
		names(EsduDevZones)

	list(TRI=TRI,CHADOC=CHADOC,ESDUDEV=ESDUDEV,CHAREF=CHAREF,EspDev=EspDev,TSk=TSk,
		EsduDevZones=EsduDevZones,DesZones=DesZones,mens=mens,LW=LW,LA=LA,
		CHvalid=CHvalid)  
	}

# ***********************
#function to import sA data in chaman format
# ***********************
#path.chaman='Q:/Projects/Acoustic biomass assessment/Data/PELGAS11/ESDUS.mdb'
#path.chaman='Q:/Projects/Acoustic biomass assessment/Data/PELGAS11/chaman_ref.txt'
#path.chamens='Q:/Projects/Acoustic biomass assessment/Data/PELGAS11/chamens.txt'

  chaman.import=function(path.chaman,path.chamens=NA,cruise,lNdev){

    if (strsplit(basename(path.chaman),split="\\.")[[1]][2]=='mdb'){
      #chaman in access format
      library(RODBC)
      ## open ODBC connection with raptri database
      channel <- odbcConnectAccess(path.chaman)
      ESDUDEV1 <- sqlFetch(channel = channel,## nom de la table ? importer 
        sqtable = 'Chaman_ref')
      chamens.wide <- sqlFetch(channel = channel,## nom de la table ? importer 
        sqtable = 'Chamens')
      ## close ODBC connection 
      odbcClose(channel)
    }else{
      ESDUDEV1=read.table(path.chaman,header=TRUE,sep=';')
      ESDUDEV1$TC
      chamens.wide=read.table(path.chamens,header=TRUE,sep=';')
      chamens.wide=correc.date(chamens.wide,ndatetime='TC')
    }
    ESDUDEV1$CAMPAGNE=cruise
    names(ESDUDEV1)[names(ESDUDEV1)%in%paste('D',lNdev[[1]],'CREF',sep='')]=
      paste('D',lNdev[[1]],'.','CREF',sep='')    
    names(ESDUDEV1)[names(ESDUDEV1)=='LONG_']='LONG'
    names(ESDUDEV1)[names(ESDUDEV1)=='Flag']='FLAG'
    names(ESDUDEV1)[names(ESDUDEV1)=='esduid']='esdu.id'
  list(ESDUDEV1=ESDUDEV1,chamens.wide=chamens.wide)
  }

#undebug(LW.compute)
 #Compute and plot length-weight relationships
  # ***********************---
LW.compute=function(catch,lspcodes.mens=NULL,codespname='CodEsp2',
                    Lname='Lcm',a=0.003,b=3,
                    Wname='PM',wname=NA,plotit=TRUE,compute.LW=TRUE,
                    gname=NA,Wscaling=1000,Nmin=0,Ncol=3,verbose=FALSE,
                    ux11=FALSE,path.export=NULL,cruise=NA,
                    plabs=NULL,subsetit=FALSE,minqx=0.07,maxqy=0.95,maxqx=.99,
                    NminFit=10,rem.NA=FALSE){
  #If argument lspcodes.mens not provided, compute LW equations for all species in 'mens'
  if (is.null(lspcodes.mens)){
    lspcodes.mens=unique(as.character(catch[,codespname]))
  }
  #graphics.off()
  if (plotit){
    if (ux11){x11()}
    par(mfrow=c(2,Ncol),bg='white')
  }
  catch0=catch[catch[,codespname]%in%lspcodes.mens,]
  table(catch0[,Wname])
  # select rows with non null, non NA sample weights
  catchs=catch0[catch0[,Wname]!=0&!is.na(catch0[,Wname]),]
  dim(catch);dim(catch0);dim(catchs)
  np=0
  spOK=table(as.character(catchs[,codespname]))>=Nmin
  lspOK=names(spOK)[spOK]
  N=length(lspOK)
  if (Nmin>0){  
    cat('No length~weight equation calculated for',
        sort(unique(as.character(catch[,codespname]))[!unique(as.character(catch[,codespname]))%in%lspOK]),
        ', not enough measurements','\n')
  }
  for (i in 1:N){
    dsubset=FALSE
    cat(lspOK[i],'\n')
    mensi=catchs[catchs[,codespname]==lspOK[i],]
    umensi=unique(catchs[,c(Lname,Wname)])
    dim(mensi);dim(umensi)
    if (sum(is.na(mensi[,Lname]))>0){
      if (rem.NA){
        cat('NA found in length data removed','\n')
        mensi=mensi[!is.na(mensi[,Lname]),]
      }else{
        stop('NA found in length data, please correct','\n')
      }
    }
    if (sum(is.na(mensi[,Wname]))>0){
      if (rem.NA){
        cat('NA found in weight data removed','\n')
        mensi=mensi[!is.na(mensi[,Wname]),]
      }else{
        stop('NA found in weight data, please correct','\n')
      }
    }
    LW.formula=as.formula(paste(Wscaling,"*",Wname,"~","a*",Lname,"^b"))
    logLW.formula=as.formula(paste("log(",Wscaling,"*",Wname,")",
                                   "~","log(",Lname,")"))
    if (compute.LW){
      #If number of measurements <Nmin, no length~weight equation computation
      if (is.na(wname)){
        mod1 <- lm(logLW.formula, data = mensi)
        lwi=try(nls(LW.formula,mensi,start=list(a=exp(coef(mod1)[1]),
                                                b=coef(mod1)[2])),
        silent=!verbose)
      }else{
        mod1 <- lm(logLW.formula, data = mensi,weights=mensi[,wname])
        #If error in nls, class(lwi)=="try-error" but R carries on
        lwi=try(nls(LW.formula,mensi,start=list(a=a,b=b),
                    weights=mensi[,wname]),silent=!verbose)
      }
      # re-try with data subset if no fit if subsetit==TRUE
      if (class(lwi)!="nls"){
        if (subsetit){
          mensis=mensi[mensi[,Lname]>quantile(mensi[,Lname],minqx)&
                         mensi[,Lname]<quantile(mensi[,Lname],maxqx)&   
                         mensi[,Wname]<quantile(mensi[,Wname],maxqy),]
          # if enough data, refit on subset
          if (dim(mensis)[1]>NminFit){
            dsubset=TRUE
            cat('Re-fitting using subset','\n')
            if (is.na(wname)){
              lwi=try(nls(LW.formula,mensis,start=list(a=a,b=b)),silent=!verbose)
            }else{
              #If error in nls, class(lwi)=="try-error" but R carries on
              lwi=try(nls(LW.formula,mensis,start=list(a=a,b=b),
                          weights=mensis[,wname]),silent=!verbose)
            }
          }
          if (class(lwi)=="nls"){
            cat('LW curve fitted on data subset for',lspOK[i],'\n')
          }else{
            cat('No fit for',lspOK[i],'\n')
          }
        }else{
          cat('No fit for',lspOK[i],'\n')
        }
      }
    }  
    if (plotit){
      if (T%in%(np%in%c(Ncol*2*seq(1:20)))){
        if (ux11) x11()
        par(mfrow=c(2,Ncol),bg='white')
      }
      if (is.null(plabs)){
        plot(mensi[,Lname],Wscaling*mensi[,Wname],main=lspOK[i],
             xlab='Length (cm)',ylab='Weight (g)')
        if (dsubset){
          points(mensis[,Lname],Wscaling*mensis[,Wname],pch=16)
        }
      }else{
        plot(mensi[,Lname],Wscaling*mensi[,Wname],main=lspOK[i],
             xlab='Length (cm)',ylab='Weight (g)',type='n')
        text(mensi[,Lname],Wscaling*mensi[,Wname],mensi[,plabs])
        if  (dsubset){
          points(mensis[,Lname],Wscaling*mensis[,Wname],pch=16)
        }
      } 
      np=np+1
      if (compute.LW){
        if (class(lwi)=="nls"){
          if  (dsubset){
            lines(sort(mensis[,Lname]), predict(lwi)[order(mensis[,Lname])], 
                  col=2)
          }else{
            lines(sort(mensi[,Lname]), predict(lwi)[order(mensi[,Lname])], 
                  col=2)
          }  
        }else{
          cat('No fit for',lspOK[i],'\n')
        }
      }  
      if (!is.na(gname)){
        points(mensi[mensi[,gname]!=0,Lname],
               Wscaling*mensi[mensi[,gname]!=0,Wname],col=2,pch=16)
      }
      if (!is.null(path.export)){
#         savePlot(filename = paste(path.export,lspOK[i],"_","LW.png",sep=''),
#                  type = "png")
#         dev.print(device=png,
#                   filename=paste(path.export,lspOK[i],"_","LW.png",sep=''),
#                   width=800,height=800)
        #cat('Plots saved in:',path.export,'\n')
      }
    }  
    #legend('topleft',legend=paste('W =',round(coef(lwi)[1],2),'* L ^',
    #  round(coef(lwi)[2],2)))
    if (compute.LW){
      #if (dim(mensi)[1]>=Nmin){
      if (class(lwi)=="nls"){
        resi=data.frame(CAMPAGNE=cruise,CATEG=0,a=coef(lwi)[1],b=coef(lwi)[2],
                        CodEsp2=lspOK[i])
      }else if (class(lwi)=="try-error"){
        resi=data.frame(CAMPAGNE=cruise,CATEG=0,a=NA,b=NA,
                        CodEsp2=lspOK[i])          
      }  
      if (i==1){
        LW=resi
      }else{   
        LW=rbind(LW,resi)
      }
    }else{
      LW=NULL   
    }
  }    
  LW
}

  
#    aggregate.catches=function(Pechei,spname='CodEsp',stname='NOSTA',path.export=NULL){
#       ID=paste(Pechei[,spname],Pechei[,stname])
#       dID=ID[duplicated(ID)]
#           if (length(dID)>0){                       
#           cat ('Duplicated species',dID,'aggregated','\n')                           
#           dcsp=Pechei[paste(Pechei[,spname],Pechei[,stname])%in%dID,]
#           if (!is.null(path.export)){
#             write.table(dcsp,paste(path.export,"DuplicatedSpecies.csv",sep=''),sep=';',row.names=FALSE)
#           }
#           sdcsp=aggregate(dcsp[,c('PT','NT')],list(dcsp[,stname],dcsp[,spname]),sum)
#           names(sdcsp)[1:2]=c(stname,spname)
#           #Compute length and weight averages, weighted by total catch
#           wtot=aggregate(dcsp$PT,list(dcsp[,stname]),sum)
#           names(wtot)=c(stname,'wtot')
#           dcsp=merge(dcsp,wtot,by.x=stname,by.y=stname)
#           dcsp$w=dcsp$PT/dcsp$wtot
#           dcsp$wL=dcsp$w*dcsp$LM
#           dcsp$wW=dcsp$w*dcsp$PM
#           dcsp$wM=dcsp$w*dcsp$MOULE
#           #arithmetical mean
#           mdcsp1=aggregate(dcsp[,c('LM','PM','MOULE')],list(dcsp[,stname],dcsp[,spname]),
#                           mean)
#           #weighted mean
#           mdcsp=aggregate(dcsp[,c('wL','wW','wM')],list(dcsp[,stname],dcsp[,spname]),
#                            sum)
#           names(mdcsp)=c(stname,spname,'LM','PM','MOULE')
#           Pechei=Pechei[!duplicated(ID),]
#           for (i in 1:length(dID)){
#             Pechei[paste(Pechei[,spname],Pechei[,stname])==dID[i],c('PT','NT')]=
#               sdcsp[paste(sdcsp[,spname],sdcsp[,stname])==dID[i],c('PT','NT')]
#             Pechei[paste(Pechei[,spname],Pechei[,stname])==dID[i],c('LM','PM','MOULE')]=
#               mdcsp[paste(mdcsp[,spname],mdcsp[,stname])==dID[i],c('LM','PM','MOULE')]
#           }  
#         }  
#         list(dID=dID,Pechei=Pechei)
#     }

aggregate.catches=function (Pecheic, spname = "CodEsp", stname = "NOSTA", 
          svnames=c("PT", "NT"),mvnames=c("LM", "PM", "MOULE"),
          path.export = NULL,wmethod='biomass'){
  if (wmethod=='biomass'){
    # weight = biomass
    ID = paste(Pecheic[, spname], Pecheic[, stname])
    dID = ID[duplicated(ID)]
    if (length(dID) > 0) {
      cat("Duplicated species", dID, "aggregated", "\n")
      dcsp = Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) %in% 
                       dID, ]
      if (!is.null(path.export)) {
        write.table(dcsp, paste(path.export, "DuplicatedSpecies.csv", 
                                sep = ""), sep = ";", row.names = FALSE)
      }
      sdcsp = aggregate(dcsp[,svnames],list(dcsp[,stname],dcsp[, spname]), sum,na.rm=TRUE)
      names(sdcsp)[1:4] = c(stname, spname,"wtot","Ntot")
      #wtot = aggregate(dcsp$PT, list(dcsp[, stname]), sum)
      #names(wtot) = c(stname, "wtot")
      dcsp = merge(dcsp, sdcsp, by.x = c(stname,spname), by.y = c(stname,spname))
      dcsp$w = dcsp[,svnames[1]]/dcsp$wtot
      dcsp$wL = dcsp$w * dcsp[,mvnames[1]]
      dcsp$wW = dcsp$w * dcsp[,mvnames[2]]
      dcsp$wM = dcsp$w * dcsp[,mvnames[3]]
      mdcsp1 = aggregate(dcsp[, mvnames], list(dcsp[,stname], dcsp[, spname]), mean,na.rm=TRUE)
      mdcsp = aggregate(dcsp[, c("wL", "wW", "wM")], list(dcsp[, 
                                                               stname], dcsp[, spname]), sum,na.rm=TRUE)
      names(mdcsp) = c(stname, spname, mvnames)
      Pecheic = Pecheic[!duplicated(ID), ]
      for (i in 1:length(dID)) {
        Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == dID[i], svnames] = 
          sdcsp[paste(sdcsp[,spname], sdcsp[, stname]) == dID[i], c("wtot","Ntot")]
        Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == dID[i], mvnames] = 
          mdcsp[paste(mdcsp[,spname], mdcsp[, stname]) == dID[i], mvnames]
      }
    }else{
      cat("No row aggregated", "\n")
    }  
  }else if (wmethod=='abundance'){
    # weight = abundance
    ID = paste(Pecheic[, spname], Pecheic[, stname])
    dID = ID[duplicated(ID)]
    if (length(dID) > 0) {
      cat("Duplicated species", dID, "aggregated", "\n")
      dcsp = Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) %in% 
                       dID, ]
      if (!is.null(path.export)) {
        write.table(dcsp, paste(path.export, "DuplicatedSpecies.csv", 
                                sep = ""), sep = ";", row.names = FALSE)
      }
      sdcsp = aggregate(dcsp[, svnames], list(dcsp[, 
                                                   stname], dcsp[, spname]), sum,na.rm=TRUE)
      names(sdcsp)[1:4] = c(stname, spname,"wtot","Ntot")
      dcsp = merge(dcsp, sdcsp, by.x = c(stname,spname), by.y = c(stname,spname))
      dcsp$w = dcsp$NT/dcsp$Ntot
      dcsp$wL = dcsp$w * dcsp$LM
      mdcsp = aggregate(dcsp[, c("wL")], list(dcsp[, 
                                                   stname], dcsp[, spname]), sum,na.rm=TRUE)
      names(mdcsp) = c(stname, spname, mvnames[1])
      Pecheic = Pecheic[!duplicated(ID), ]
      for (i in 1:length(dID)) {
        Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == 
                  dID[i], svnames] = sdcsp[paste(sdcsp[, 
                                                       spname], sdcsp[, stname]) == dID[i], c("wtot", 
                                                                                              "Ntot")]
        Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == 
                  dID[i], mvnames[1]] = mdcsp[paste(mdcsp[, 
                                                          spname], mdcsp[, stname]) == dID[i], mvnames[1]]
        Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == 
                  dID[i],mvnames[2]]<-1000*Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == 
                                                     dID[i],svnames[1]]/Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == 
                                                                                  dID[i],svnames[2]]
        Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == 
                  dID[i],mvnames[3]]<-Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == 
                                                dID[i],svnames[2]]/Pecheic[paste(Pecheic[, spname], Pecheic[, stname]) == 
                                                                             dID[i],svnames[1]]
      }
    }else{
      cat("No row aggregated", "\n")
    }  
  }  
  list(dID = dID, Pechei = Pecheic)
}

sizecat.split.totalSamples= function (Pecheic, mens, spname = "GENR_ESP", 
                                      stname = "NOSTA",LWts=NULL,lbig='G',
                                      recalculate=FALSE){
    
    # Calculate size category proportions and weighted mean lengths 
    # based on subsamples
    head(mens)
    mensa1=aggregate(mens[,c('NBIND','POIDSECHANT')],
                     list(NOSTA=mens[,stname],GENR_ESP=mens[,spname],
                          CATEG=mens$CATEG),sum,na.rm=TRUE)
    names(mensa1)[4:5]=c("NtotCodespStation","BtotCodespStation")
    mensa2=aggregate(mens[,c('NBIND','POIDSECHANT')],
                     list(NOSTA=mens[,stname],GENR_ESP=mens[,spname]),sum,
                     na.rm=TRUE)
    names(mensa2)[3:4]=c('NtotSpStation','BtotSpStation')
    mensa=merge(mensa1,mensa2)
    head(mensa)
    lstspG=paste(mensa[mensa$CATEG==lbig,c(stname),],
                 mensa[mensa$CATEG==lbig,c(spname),])
    mensa$PM=1000*mensa$BtotCodespStation/mensa$NtotCodespStation
    mensa$pBCodeSpStation=mensa$BtotCodespStation/mensa$BtotSpStation
    mensa$pNCodeSpStation=mensa$NtotCodespStation/mensa$NtotSpStation
    mensaL=aggregate(mens[,c('NBIND','POIDSECHANT')],
                     list(NOSTA=mens[,stname],GENR_ESP=mens[,spname],
                          CATEG=mens$CATEG,Lcm=mens$Lcm),sum,na.rm=TRUE)
    names(mensaL)[5:6]=c('NtotL','BtotL')
    mensaL=merge(mensaL,mensa,by.x=c('NOSTA','GENR_ESP','CATEG'),
                 by.y=c('NOSTA','GENR_ESP','CATEG'))
    head(mensaL)
    mensaL$wLiB=mensaL$Lcm*mensaL$NtotL/mensaL$NtotCodespStation
    mensaL$wLiN=mensaL$Lcm*mensaL$BtotL/mensaL$BtotCodespStation
    
    mensawL=aggregate(mensaL[,c('wLiB','wLiN')],
                      list(NOSTA=mensaL[,stname],GENR_ESP=mensaL[,spname],
                                                     CATEG=mensaL$CATEG),sum,
                      na.rm=TRUE)
    names(mensawL)[4:5]=c('wLB','wLN')
    head(mensawL)
    mensa.check.pB=aggregate(mensa$pBCodeSpStation,
              list(NOSTA=mensa[,stname],GENR_ESP=mensa[,spname]),sum)
    mensa.check.pB[round(mensa.check.pB$x,0)!=1,]
    mensa.check.pN=aggregate(mensa$pNCodeSpStation,
                             list(NOSTA=mensa[,stname],
                                  GENR_ESP=mensa[,spname]),sum)
    mensa.check.pN.pb=mensa.check.pN[round(mensa.check.pN$x,0)!=1,]
    # check that mean lengths weighted by number and mass are equal 
    # library(ggplot2)
    #  ggplot(mensawL[mensawL$GENR_ESP%in%lsp.assess,],
    #         aes(x=wLB,y=wLN))+geom_point()+geom_abline(slope=1,intercept = 0)+
    #    facet_grid(GENR_ESP~CATEG,scales = "free")
    mensa=merge(mensa,mensawL)
    head(mensa)
    head(Pecheic)
    Pecheica=aggregate(Pecheic[,c('PT','NT','PTRI')],
                       list(NOSTA=Pecheic[,stname],
                            GENR_ESP=Pecheic[,spname]),sum)
    names(Pecheica)[3:5]=c('Ptot','Ntot','PTRItot')
    dim(Pecheica)
    Pecheic2=merge(Pecheica,mensa,by.x=c('NOSTA','GENR_ESP'),
                   by.y=c('NOSTA','GENR_ESP'),all.x=TRUE)
    dim(Pecheic2)
    head(Pecheic2)
    if (!is.null(LWts)){
      Pecheic2=merge(Pecheic2,LWts[,c('GENR_ESP','a','b')],
                     by.x='GENR_ESP',by.y='GENR_ESP')
      Pecheic2$PM=Pecheic2$a*Pecheic2$wLN^(Pecheic2$b)
    }
    # weight = biomass
    # Calculate new total weights and numbers in new size categories based on
    # biomass and abundance proportions 
    Pecheic2$PT2B=Pecheic2$Ptot*Pecheic2$pBCodeSpStation
    Pecheic2$NT2B=round(Pecheic2$Ntot*Pecheic2$pBCodeSpStation,digits = 0)
    Pecheic2$PTRI2B=Pecheic2$PTRItot*Pecheic2$pBCodeSpStation
    Pecheic2$PT2N=Pecheic2$Ptot*Pecheic2$pNCodeSpStation
    Pecheic2$NT2N=round(Pecheic2$Ntot*Pecheic2$pNCodeSpStation,digits = 0)
    Pecheic2$PTRI2N=Pecheic2$PTRItot*Pecheic2$pNCodeSpStation
    Pecheic2$MOULE=1000/Pecheic2$PM
    Pecheic2$ID = paste(Pecheic2[, spname], Pecheic2$CATEG,Pecheic2[, stname])
    # Correct old and new total weights and numbers in new size categories
    head(Pecheic2)
    Pecheic2diff=Pecheic2[Pecheic2$Ptot!=Pecheic2$PT2N,]
    Pecheic2diff[,c('NOSTA','GENR_ESP','CATEG','Ptot','PT2N','Ntot','NT2N',
                    'wLN','PM')]
    library(ggplot2)
    ggplot(Pecheic2diff,
             aes(x=wLB,y=PM))+geom_point()+
        facet_grid(GENR_ESP~CATEG,scales = "free")
    ggplot(Pecheic2diff,
           aes(x=wLN,y=PM))+geom_point()+
      facet_grid(GENR_ESP~CATEG,scales = "free")
    
    if (dim(Pecheic2diff)[1] > 0) {
      cat("Total weights and numbers recalculated in new size categories for:",
          paste(Pecheic2diff$ID,collapse =' / '), "\n")
    }else{
      cat("No total weights and numbers recalculated", "\n")
    }
    names(Pecheic);names(Pecheic2)
    mnames=names(Pecheic)[!names(Pecheic)%in%names(Pecheic2)&
                            !names(Pecheic)%in%c("CodEsp","SIGNEP","PT","NT",
                                                 "LM","PM","MOULE","PTRI")]
    dim(Pecheic2)
    Pecheicf=merge(Pecheic2[
      ,c("NOSTA","GENR_ESP","CATEG","wLN","PT2N","NT2N","PTRI2N","PM","MOULE")],
                   unique(Pecheic[,c("GENR_ESP","NOSTA",mnames)]),
                                  by.x=c("GENR_ESP","NOSTA"),
                   by.y=c("GENR_ESP","NOSTA"))
    dim(Pecheicf)
    head(Pecheicf)
    Pecheicf$CodEsp=paste(Pecheicf$GENR_ESP,Pecheicf$STRATE,
                          Pecheicf$CATEG,sep='-')
    names(Pecheicf)[names(Pecheicf)%in%
                      c("CATEG","wLN","PT2N","NT2N","PTRI2N")]=
      c("SIGNEP","LM","PT","NT","PTRI")
    sort(names(Pecheic))
    sort(names(Pecheicf))
    Pecheicf=Pecheicf[,names(Pecheic)]
    if (!recalculate){
      Pecheicfs=Pecheicf[paste(Pecheicf$NOSTA,Pecheicf$GENR_ESP)%in%lstspG,]
      Pecheinr=Pecheic[!paste(Pecheic$NOSTA,Pecheic$GENR_ESP)%in%lstspG,]
      Pecheicf=rbind(Pecheinr,Pecheicfs)
    }
    list(Pechei = Pecheicf,Pecheic2=Pecheic2)
  }

 NOCHAL.conversion=function(Pechei,proPref){
    #NOCHAL conversion
    Pechei=Pechei[order(Pechei$NOSTA),]
    Pechei$NOCHAL2=Pechei$NOCHAL
    #dim(Pechei)[1]-(dim(Pechei.pro)[1]+dim(Pechei.tha)[1])
    lNOCHAL2=unique(Pechei[substr(Pechei$NOSTA,1,2)==proPref,'NOCHAL'])
    lNOSTA2=unique(Pechei[substr(Pechei$NOSTA,1,2)==proPref,'NOSTA'])
    cNOCHAL=data.frame(NOCHAL2=lNOCHAL2,NOCHAL=paste(
      '9',substr(lNOSTA2,4,5),sep=''))
    #If NOCHAL conversion
    Pechei.pro=Pechei[substr(Pechei$NOSTA,1,2)==proPref,]
    Pechei.pro=Pechei.pro[,names(Pechei.pro)!='NOCHAL']
    Pechei.tha=Pechei[substr(Pechei$NOSTA,1,2)!=proPref,]
    Pechei.pro=merge(Pechei.pro,cNOCHAL,by.x='NOCHAL2',by.y='NOCHAL2')  
    Pechei=rbind(Pechei.tha,Pechei.pro)
    dim(Pechei)
    #Pechei[,c('NOSTA','NOCHAL','NOCHAL2')]  
    Pechei$NOCHAL=as.numeric(as.character(Pechei$NOCHAL))
    Pechei=unique(Pechei)
    ID=paste(Pechei$CodEsp,Pechei$NOCHAL)
    lnochal=Pechei[duplicated(ID),'NOCHAL']
    dPechei=Pechei[Pechei$NOCHAL%in%lnochal,]
    ID=paste(Pechei$CodEsp,Pechei$NOCHAL)
    lnochal=Pechei[duplicated(ID),'NOCHAL']
    if (length(lnochal)[1]>0) stop('Duplicated species in catches')
 Pechei
}

#Function for reshaping lay data into long format
# ***********************

  reshape.lay=function(lay,lname,vnames){
    nsl=lay[1,'NSL']
    nbl=lay[1,'NBL']
    saS.long=reshape(lay[,c('t',paste(vnames[1],seq(nsl),sep=''))],
      varying=list(2:(nsl+1)),v.names=lname, idvar="t",direction="long",
      times=c(paste('SL',seq(nsl),sep='')))
    saS.long=saS.long[order(saS.long$t),]
    saS.long$cellType=rep(c(rep(0,nsl)),length(saS.long$t)/length(c(rep(0,nsl))))
    head(saS.long)
    saB.long=reshape(lay[,c('t',paste(vnames[2],seq(nbl),sep=''),'Ltot')],
       varying=list(2:(nbl+2)),v.names=lname, idvar="t",direction="long",
      times=c(paste('BL',seq(nbl),sep=''),'Ltot'))
    saB.long=saB.long[order(saB.long$t),]
    saB.long$cellType=rep(c(rep(1,nbl),nbl),length(saB.long$t)/
      length(c(rep(1,nbl),nbl)))
    head(saB.long)
    if (lname=='z'){
      saB.long[saB.long$time=='Ltot','z']=lay$dpth
    }
    list(layS.long=saS.long,layB.long=saB.long)
  }

#function for converting a vector x of haul id's nid1,
# into corresponding hauls id's nid2

NOSTACHAL.conversion=function(x,peche,nid1='NOSTA',nid2='NOCHAL'){
  x2=data.frame(x,ox=seq(length(x)))
  y=merge(x2,unique(peche[,c(nid1,nid2)]),by.x='x',by.y=nid1,all.x=TRUE)
  #sort dataframe according to initial ranks
  y=y[order(y$ox),]
  z=y[,nid2]
  z  
}

# Function for putting catches in "wide" format for piecharts

widen.catch=function(Pechef,nid,nsp='GENR_ESP',
                     lsp=c('PT.ENGR.ENC','PT.MICR.POU',
                           'PT.SARD.PIL','PT.SCOM.SCO','PT.SPRA.SPR',
                           'PT.TRAC.TRU','PT.TRAC.MED')){
  lsp=gsub('-','.',lsp)
  Pechef$PT=as.numeric(as.character(Pechef$PT))
  Pechefa=aggregate(Pechef[,'PT'],list(CAMPAGNE=Pechef$CAMPAGNE,nid=Pechef[,nid],
                                       nsp=Pechef[,nsp]),sum,na.rm=TRUE)
  names(Pechefa)=c('CAMPAGNE',nid,nsp,'PT')                    
  Pechel=reshape(Pechefa[,c('CAMPAGNE',nid,nsp,'PT')],
                 idvar=c("CAMPAGNE",nid),timevar=nsp, direction="wide")
  #head(Pechel)
  #names(Pechel)
  Pechel=data.frame(CAMPAGNE=Pechel$CAMPAGNE,NOCHAL=Pechel[,nid],Pechel[,3:dim(Pechel)[2]])
  for (i in 3:dim(Pechel)[2]){
    Pechel[,i]=as.numeric(na.null(Pechel[,i]))
  }  
  names(Pechel)[2]=nid
  #head(Pechel)
  dim(Pechel)
  sps=substr(names(Pechel),1,11)%in%c('CAMPAGNE',nid,paste('PT',lsp,sep='.'))
  Pechels=Pechel[,sps]
  #head(Pechels)
  #NOCHAL is OK in by.x
  if (sum('GEAR'%in%names(Pechef)==0)){
    Pechef$GEAR=NA
  }
  Pechels=merge(
    Pechels,unique(
      Pechef[,c('CAMPAGNE',nid,'LONF','LATF','STRATE','SONDE','GEAR')]),
                by.x=c('CAMPAGNE',nid),by.y=c('CAMPAGNE',nid))
  lsps=names(Pechels)[!names(Pechels)%in%c(nid,'LONF','LATF')]
  names(Pechels)[2]=nid
  Pechels
}

# function to convert data headers to and from input/echoR formats

header.converter=function(x,direction='base2R'){
  data(EchoRinputFilesNames)
  #EchoRinputFilesNames=read.table('C:/Users/mdoray.IFR/Documents/R/echor/trunk/EchoRpackage/EchoR/data/EchoRinputFilesNames.csv',
  #                 sep=';',header=TRUE)
  cname=EchoRinputFilesNames
  head(cname)
  if (direction=='base2R'){
    if (T%in%(tolower(cname$inputName)%in%tolower(names(x)))){
      if ('tsb'%in%tolower(names(x))){
        cnames=cname[cname$inputFile=='TotalSamples',]
      }else if ('lengthclass'%in%tolower(names(x))){
        cnames=cname[cname$inputFile=='SubSamples',]
      }else if ('echotype'%in%tolower(names(x))){
        cnames=cname[cname$inputFile=='Echotypes',]
      }else{
        cnames=cname[cname$inputFile=='AcousticData',]
      }
      names(cnames)=tolower(names(cnames))
      cnames$inputname=tolower(cnames$inputname)
      names(x)=tolower(names(x))
      tnames=merge(cnames,data.frame(inames=names(x),dum=1),by.x='inputname',
                   by.y='inames') 
      head(tnames)
      x=x[,as.character(tnames$inputname)]
      names(x)=tnames$echorname
    }
  }else if (direction=='R2base'){
    if (T%in%(names(x)%in%cname$echorName)){
      if ('CAC'%in%names(x)){
        cnames=cname[cname$inputFile=='TotalSamples',]
      }else if ('Lcm'%in%names(x)){
        cnames=cname[cname$inputFile=='SubSamples',]
      }else if ('DEV'%in%names(x)){
        cnames=cname[cname$inputFile=='Echotypes',]
      }else{
        cnames=cname[cname$inputFile=='AcousticData',]
      }
      tnames=merge(cnames,data.frame(inames=names(x),dum=1),by.x='echorName',by.y='inames') 
      x=x[,as.character(tnames$echorName)]
      #names(x)[!names(x)%in%tnames$echorName]
      #tnames$echorName[!tnames$echorName%in%names(x)]
      names(x)=tnames$inputName
    }
  }
  x
}

# function to automatically define catches size categories
# original code: claire.saraux@ifremer.fr, 03/06/2013
# modified: mathieu.doray@ifremer.fr, 17/06/2013

sizecat<-function(menst,spsel=NULL,nsize='Lcm',minLag=10,plotit=FALSE,
                  method='cluster',breaks='means',lbks=NULL){
  
  library(Rmixmod)
  
  if (!is.null(spsel)){
    cat('Process selected species','\n')
    # select species not in spsel that will not be processed
    mens2=menst[!menst$GENR_ESP%in%spsel,]
    if (dim(mens2)[1]>0){
      mens2$CATEG='0'
      mens2$CodEsp2=paste(mens2$GENR_ESP,mens2$CATEG,sep='-')
    }
    mensts=menst[menst$GENR_ESP%in%spsel,]
  }else{
    cat('Process all species','\n')
    mens2<-NULL
    mensts=menst
  }
  lsp=unique(mensts$GENR_ESP)
  if (!is.null(spsel)){
    lsp=as.character(lsp)
    if (sum((!spsel%in%lsp))>0){
      stop("Species",spsel[!spsel%in%lsp],"not found in subsamples, please check")
    }else{
      lsp=spsel
    }
  }
  
  for(i in 1:length(lsp)){
    
    mensi<-menst[menst$GENR_ESP==lsp[i],]
    
    if(lsp[i]!='DIVE-RS1'&length(mensi[,1])>2){
      if (!is.null(lbks)){
        if (class(lbks)!='list'){
          if (is.na(lbks[i])){
            mensi$CodEsp2<-paste(mensi$GENR_ESP,'0',sep='-')
            mensi$CATEG=0
            lmi=NA
            cuti=NA
            cuti2=NA
            cat(paste('no size category for', lsp[i],'\n'))
          }else{
            #Set cluster labels
            mensi$CATEG='0'
            mensi[mensi[,nsize]>lbks[i],'CATEG']='G'
            mensi$CodEsp2=paste(mensi$GENR_ESP,mensi$CATEG,sep='-')
            cat(paste('2 different size categories for',lsp[i], 'cut at',lbks[i],'\n')) 
          }
          if (plotit){
            mensai=aggregate(mensi$NBIND,list(mensi$Lcm),sum)
            names(mensai)=c(nsize,'NBIND')
            if (i%in%c(1,7,13,19)){
              x11()
              par(mfrow=c(2,3),bg='white')
            }
            par(bg='white')
            plot(mensai[,nsize],mensai$NBIND,type='l',
                 main=paste(lsp[i],'N =',sum(mensai$NBIND)),xlab='Length',
                 ylab='Numbers')
            if (!is.na(lbks[i])){
              # manual cut point
              abline(v=lbks[i],lty=2)
            }
          }
        }else if (class(lbks)=='list'){
          lbksi=lbks[[i]]
          for (j in 1:length(lbksi)){
            if (is.na(lbksi[j])){
              mensi$CodEsp2<-paste(mensi$GENR_ESP,'0',sep='-')
              mensi$CATEG=0
              lmi=NA
              cuti=NA
              cuti2=NA
              cat(paste('no size category for', lsp[i],'\n'))
            }else{
              #Set cluster labels
              if (j==1){
                if (length(lbksi)==1){
                  mensi$CATEG='0'
                  mensi[mensi[,nsize]>lbksi[j],'CATEG']='G'
                  mensi$CodEsp2=paste(mensi$GENR_ESP,mensi$CATEG,sep='-')
                  cat(paste('2 different size categories for',lsp[i], 'cut at',
                            lbksi[j],'\n'))
                }
                if (length(lbksi)==2){
                  mensi$CATEG='P'
                  mensi[mensi[,nsize]>lbksi[j],'CATEG']='0'
                  mensi$CodEsp2=paste(mensi$GENR_ESP,mensi$CATEG,sep='-')
                  cat(paste('3 different size categories for',lsp[i], 'cut at',
                            paste(lbksi,collapse=','),'\n'))
                }
              }
              if (j==2){
                mensi[mensi[,nsize]>lbksi[j],'CATEG']='G'
                mensi$CodEsp2=paste(mensi$GENR_ESP,mensi$CATEG,sep='-')
              }
            }
          }
          if (plotit){
              mensai=aggregate(mensi$NBIND,list(mensi$Lcm),sum)
              names(mensai)=c(nsize,'NBIND')
              if (i%in%c(1,7,13,19)){
                x11()
                par(mfrow=c(2,3),bg='white')
              }
              par(bg='white')
              plot(mensai[,nsize],mensai$NBIND,type='l',
                     main=paste(lsp[i],'N =',sum(mensai$NBIND)),xlab='Length',
                     ylab='Numbers')
              if (!is.na(lbks[[i]][1])){
                  # manual cut point
                  abline(v=lbks[[i]],lty=2)
              }
           }
        }
      }else{
        modi <- mixmodCluster(mensi[,nsize], nbCluster=1:2,weight=mensi$NBIND,
                              strategy=new("Strategy", nbIterationInAlgo=10000))
        
        if(modi@bestResult@nbCluster==1){
          mensi$CodEsp2<-paste(mensi$GENR_ESP,'0',sep='-')
          mensi$CATEG=0
          lmi=NA
          cuti=NA
          cuti2=NA
          cat(paste('no size category for', lsp[i],'\n'))
        }
        if(modi@bestResult@nbCluster==2){
          if(diff(modi@bestResult@parameters@mean[order(modi@bestResult@parameters@mean)])<minLag){
            mensi$CodEsp2<-paste(mensi$GENR_ESP,'0',sep='-')
            mensi$CATEG=0
            lmi=NA
            cuti=NA
            cuti2=NA
            cat(paste('no size category for', lsp[i],'\n'))
          }
          if(diff(modi@bestResult@parameters@mean[order(modi@bestResult@parameters@mean)])>minLag){
            if(modi@bestResult@parameters@mean[1]<modi@bestResult@parameters@mean[2]){
              #Compute breaks
              ma<-max(modi@data[modi@bestResult@partition==1])
              mi<-min(modi@data[modi@bestResult@partition==2])
              q2.5pc<-quantile(modi@data[modi@bestResult@partition==2],0.05)
              q2.95pc<-quantile(modi@data[modi@bestResult@partition==2],0.95)
              q1.5pc<-quantile(modi@data[modi@bestResult@partition==1],0.05)
              q1.95pc<-quantile(modi@data[modi@bestResult@partition==1],0.95)
              cuti2=mean(c(q1.95pc,q2.5pc))
              lmi=mean(c(ma,mi))
              cuti=mean(c(modi@bestResult@parameters@mean[1],modi@bestResult@parameters@mean[2]))
              #Set cluster labels
              if (method=='cluster'){
                #Set clusters labels as clusters belongings
                mensi[modi@data[modi@bestResult@partition==1],]$CodEsp2<-
                  paste(mensi[modi@data[modi@bestResult@partition==1],]$GENR_ESP,'0',
                        sep='-')
                mensi[modi@data[modi@bestResult@partition==2],]$CodEsp2<-
                  paste(mensi[modi@data[modi@bestResult@partition==2],]$GENR_ESP,'G',
                        sep='-')
                mensi[modi@data[modi@bestResult@partition==1],]$CATEG<-'0'
                mensi[modi@data[modi@bestResult@partition==2],]$CATEG<-'G'
              }else if (method=='breaks'){
                mensi$CATEG='0'
                if (breaks=='means'){
                  mensi[mensi[,nsize]>cuti,'CATEG']='G'
                }else if (breaks=='quantiles'){
                  mensi[mensi[,nsize]>cuti2,'CATEG']='G'                
                }  
                mensi$CodEsp2=paste(mensi$GENR_ESP,mensi$CATEG,sep='-')
              }
              cat(paste('2 different size categories for',lsp[i], 'cut at',mean(c(ma,mi)),'\n')) 
            }  		
            if(modi@bestResult@parameters@mean[1]>modi@bestResult@parameters@mean[2]){
              #Compute breaks
              ma<-max(modi@data[modi@bestResult@partition==1])
              mi<-min(modi@data[modi@bestResult@partition==2])
              q2.5pc<-quantile(modi@data[modi@bestResult@partition==2],0.05)
              q2.95pc<-quantile(modi@data[modi@bestResult@partition==2],0.95)
              q1.5pc<-quantile(modi@data[modi@bestResult@partition==1],0.05)
              q1.95pc<-quantile(modi@data[modi@bestResult@partition==1],0.95)
              cuti2=mean(c(q1.5pc,q2.95pc))
              lmi=mean(c(ma,mi))
              cuti=mean(c(modi@bestResult@parameters@mean[1],modi@bestResult@parameters@mean[2]))
              #Set cluster labels
              if (method=='cluster'){
                #Set clusters labels as clusters belongings
                mensi[modi@data[modi@bestResult@partition==1],]$CodEsp2<-
                  paste(mensi[modi@data[modi@bestResult@partition==1],]$GENR_ESP,'G',
                        sep='-')
                mensi[modi@data[modi@bestResult@partition==2],]$CodEsp2<-
                  paste(mensi[modi@data[modi@bestResult@partition==2],]$GENR_ESP,'0',
                        sep='-')
                mensi[modi@data[modi@bestResult@partition==1],]$CATEG<-'G'
                mensi[modi@data[modi@bestResult@partition==2],]$CATEG<-'0'
              }else if (method=='breaks'){
                mensi$CATEG='0'
                if (breaks=='means'){
                  mensi[mensi[,nsize]>cuti,'CATEG']='G'
                }else if (breaks=='quantiles'){
                  mensi[mensi[,nsize]>cuti2,'CATEG']='G'                
                } 
                mensi$CodEsp2=paste(mensi$GENR_ESP,mensi$CATEG,sep='-')
              }
              cat(paste('2 different size categories for',lsp[i], 'cut at',mean(c(ma,mi)),'\n')) 
            }
          }
        }
        if (plotit){
          mensai=aggregate(mensi$NBIND,list(mensi$Lcm),sum)
          names(mensai)=c(nsize,'NBIND')
          if (i%in%c(1,7,13,19)){
            x11()
            par(mfrow=c(2,3),bg='white')
          } 
          par(bg='white')
          plot(mensai[,nsize],mensai$NBIND,type='l',
               main=paste(lsp[i],'N =',sum(mensai$NBIND)),xlab='Length',ylab='Numbers')
          if (i%in%c(1,7,13,19)){
            legend('topright',legend=c('Clusters means','Mean of means',
                                       'Quantiles mean'),lty=c(3,2,1))
          }  
          if (!is.na(lmi)){
            #average of cluster mean length means
            abline(v=cuti,lty=2)
            #average of clusters quantiles 5 and 95%
            abline(v=cuti2,lty=1)
            abline(v=modi@bestResult@parameters@mean[1],lty=3)
            abline(v=modi@bestResult@parameters@mean[2],lty=3)
            
          }
        }
      }
    }
    mens2<-rbind(mens2,mensi)
    if (is.null(lbks)){
      dfi=data.frame(sp=lsp[i],mBreaks=lmi,mMeans=cuti,mQuant=cuti2)
    }else{
      if (class(lbks)!='list'){
        dfi=data.frame(sp=lsp[i],mBreaks=lbks[i])        
      }else if (class(lbks)=='list'){
        dfi=data.frame(sp=lsp[i],mBreaks=lbks[[i]])
      }  
    }
    if (i==1){
      dflm=dfi
    }else{
      dflm=rbind(dflm,dfi)
    }
  }
  mens2<-mens2[!is.na(mens2$NOSTA),]
  names(mens2)
  
  # Aggregate duplicated rows
  mens2.id=paste(mens2$NOSTA,mens2$GENR_ESP,mens2$CATEG,mens2$Lcm)
  mens2dup.id=mens2.id[duplicated(mens2.id)]
  mens2.ndup=mens2[!mens2.id%in%mens2dup.id,]
  mens2.dup=mens2[mens2.id%in%mens2dup.id,]
  if (dim(mens2.dup)[1]>0){
    mens2.dupa=aggregate(mens2.dup[,c('NBIND','POIDSTAILLE')],
                         list(NOSTA=mens2.dup$NOSTA,GENR_ESP=mens2.dup$GENR_ESP,
                              CATEG=mens2.dup$CATEG,Lcm=mens2.dup$Lcm),sum,
                         na.rm=TRUE)
    names(mens2.dupa)
    metaNames=names(mens2.dup)[names(mens2.dup)%in%c("CAMPAGNE","NOSTA",
                                                     "POCHE","SEXE","GENR_ESP","CATEG",
                                                     "UNITE","INC","VESSEL","TAILLE",
                                                     "Lcm","CodEsp2","discard")]
    mens2.dup.meta=unique(mens2.dup[,names(mens2.dup)[names(mens2.dup)%in%
                                                        metaNames]])
    mens2.dupa=merge(mens2.dupa,mens2.dup.meta,
                     by.x=c("NOSTA","GENR_ESP","CATEG","Lcm"),
                     by.y=c("NOSTA","GENR_ESP","CATEG","Lcm"))
    mens2.dupa$PM=mens2.dupa$POIDSTAILLE/mens2.dupa$NBIND
    mens2.dupa$CodEsp2=paste(mens2.dupa$GENR_ESP,mens2.dupa$CATEG,sep='-')
    mens2.dupa$POIDSECHANT=NA
    mens2.dupa$NECHANT=NA
    names(mens2.dupa)
    mens2=plyr::rbind.fill(mens2.ndup,mens2.dupa)
    dim(mens2)
    mens2.id=paste(mens2$NOSTA,mens2$GENR_ESP,mens2$CATEG,mens2$TAILLE)
    mens2dup.id=mens2.id[duplicated(mens2.id)]
  }
 
  # Re-calculate sample numbers and weights
  mens2c=subsampleConsistencyCheck(mens4Echobase=mens2,tnames='RAPTRI')
  dim(mens2c)
  list(mens2=mens2c,dflm=dflm)
}


corBACclup4Depth=function(Pechei){
  #Ona's 2003 TS equation with depth correction:
  # TS = 20*log10(L) - 2.3*log10(1+z/10) - 65.4
  # correct CLAS anchovy TS with depth=seabed depth+20m  
  Pechei[Pechei$GENR_ESP%in%c('ENGR-ENC')&
           substr(Pechei$STRATE,1,4)=='CLAS','BAC']=
    2.3*log10(1+(Pechei[Pechei$GENR_ESP%in%c('ENGR-ENC')&
                          substr(Pechei$STRATE,1,4)=='CLAS','SONDE']+20)/10)+65.4
  # correct SURF anchovy TS with depth=20m  
  Pechei[Pechei$GENR_ESP%in%c('ENGR-ENC')&
           substr(Pechei$STRATE,1,4)=='SURF','BAC']=2.3*log10(1+20/10)+65.4
  # correct CLAS sprat and sardine TS with depth=seabed depth+10m  
  Pechei[Pechei$GENR_ESP%in%c('SARD-PIL','SPRA-SPR')&
           substr(Pechei$STRATE,1,4)=='CLAS','BAC']=
    2.3*log10(1+(Pechei[Pechei$GENR_ESP%in%c('SARD-PIL','SPRA-SPR')&
                          substr(Pechei$STRATE,1,4)=='CLAS','SONDE']+10)/10)+65.4
  # correct SURF sprat and sardine TS with depth=20m  
  Pechei[Pechei$GENR_ESP%in%c('SARD-PIL','SPRA-SPR')&
           substr(Pechei$STRATE,1,4)=='SURF','BAC']=2.3*log10(1+20/10)+65.4  
  Pechei  
}



TSdephCorrection=function(Pechei,equation='Ona03zcor',b20=NULL){

  if (equation=='Ona03zcor'){
    if (is.null(b20)) b20=65.4
    Pecheis1=Pechei[Pechei$GENR_ESP%in%c('ENGR-ENC','SARD-PIL','SPRA-SPR'),]
    Pecheis1.pb=Pecheis1[Pecheis1$SONDE==0|is.na(Pecheis1$SONDE),]
    if (dim(Pecheis1.pb)[1]>0){
      stop('Erroneous seabed depth at: ',paste(unique(Pecheis1.pb$TC),collapse=', '))
    }
    Pecheis2=Pechei[!Pechei$GENR_ESP%in%c('ENGR-ENC','SARD-PIL','SPRA-SPR'),]
    Pecheis1[substr(Pecheis1$STRATE,1,4)=='SURF','BAC']=b20+2.3*log10(1+(10/10))
    Pecheis1[Pecheis1$GENR_ESP=='ENGR-ENC','BAC']=b20+
        2.3*log10(1+(Pecheis1[Pecheis1$GENR_ESP=='ENGR-ENC','SONDE']-20)/10)
    Pecheis1[Pecheis1$GENR_ESP=='SARD-PIL','BAC']=b20+
        2.3*log10(1+(Pecheis1[Pecheis1$GENR_ESP=='SARD-PIL','SONDE']-20)/10)
    Pecheis1[Pecheis1$GENR_ESP=='SPRA-SPR','BAC']=b20+
        2.3*log10(1+(Pecheis1[Pecheis1$GENR_ESP=='SPRA-SPR','SONDE']-15)/10)
    Pecheis1[substr(Pecheis1$STRATE,1,4)=='CLAS'&Pecheis1$SONDE<20,'BAC']=b20+2.3*log10(1+(10/10))
  }else if (equation=='changeb20only'){
    if (is.null(b20)) stop('Please provide a b20 value')
    Pecheis1=Pechei[Pechei$GENR_ESP%in%c('ENGR-ENC','SARD-PIL','SPRA-SPR'),]
    Pecheis2=Pechei[!Pechei$GENR_ESP%in%c('ENGR-ENC','SARD-PIL','SPRA-SPR'),]
    Pecheis1[,'BAC']=b20
  }else if (equation=='Zhao2008'){
    if (is.null(b20)) b20=67.6
    Pecheis1=Pechei[Pechei$GENR_ESP%in%c('ENGR-ENC'),]
    Pecheis2=Pechei[!Pechei$GENR_ESP%in%c('ENGR-ENC'),]
    #TS = 20log10L -(20/3)log10(1+z/10)-67.6
    Pecheis1[substr(Pecheis1$STRATE,1,4)=='SURF','BAC']=b20+(20/3)*log10(1+(10/10))
    Pecheis1[Pecheis1$GENR_ESP=='ENGR-ENC','BAC']=b20+
      (20/3)*log10(1+(Pecheis1[Pecheis1$GENR_ESP=='ENGR-ENC','SONDE']-20)/10)
  }  
  Pechei=rbind(Pecheis1,Pecheis2)
  Pechei
}


Pechetransf<-function(mens=mens,Pechei=Pechei){
  head(mens)
  mens$nbL<-mens$NBIND*mens$Lcm
  mens2<-aggregate(mens[,c('POIDSTAILLE','NBIND','nbL')],
                   by=list(NOSTA=mens$NOSTA,CodEsp2=mens$CodEsp2,
                           GENR_ESP=mens$GENR_ESP),sum)
  menstot<-aggregate(mens$POIDSTAILLE,
                     by=list(NOSTA=mens$NOSTA,GENR_ESP=mens$GENR_ESP),sum)
  mensused<-merge(mens2,menstot,by.x=c(1,3),by.y=c(1:2))
  head(mensused)
  mensused$prop<-mensused$POIDSTAILLE/mensused$x
  cat('Missing subsample weight in: ',paste(mensused[is.na(mensused$prop),'NOSTA'],mensused[is.na(mensused$prop),'CodEsp2']),'\n')
  mensused$PM2<-mensused$POIDSTAILLE/mensused$NBIND*1000
  mensused$LM2<-mensused$nbL/mensused$NBIND
  head(Pechei)
  Pecheiagg<-aggregate(Pechei$PT,
                       by=list(GENR_ESP=Pechei$GENR_ESP,NOSTA=Pechei$NOSTA),sum)
  Pechei2<-merge(Pechei,Pecheiagg)
  Pechetransf<-merge(Pechei2,mensused[,c(1:3,8:10)],by.x=c('NOSTA','GENR_ESP'),
                     c('NOSTA','GENR_ESP'),all.y=T,all.x=T)
  head(Pechetransf)
  dim(Pechetransf)
  Pechetransf$CodEsp=as.character(Pechetransf$CodEsp)
  Pechetransf$SIGNEP=as.character(Pechetransf$SIGNEP)
  Pechetransf[!is.na(Pechetransf$prop),]$PT<-Pechetransf[!is.na(Pechetransf$prop),]$x*Pechetransf[!is.na(Pechetransf$prop),]$prop
  Pechetransf[!is.na(Pechetransf$prop),]$PTRI<-Pechetransf[!is.na(Pechetransf$prop),]$PT
  Pechetransf[!is.na(Pechetransf$prop),]$LM<-Pechetransf[!is.na(Pechetransf$prop),]$LM2
  Pechetransf[!is.na(Pechetransf$prop),]$PM<-Pechetransf[!is.na(Pechetransf$prop),]$PM2
  Pechetransf[!is.na(Pechetransf$prop),]$NT<-round(1000*Pechetransf[!is.na(Pechetransf$prop),]$PT/Pechetransf[!is.na(Pechetransf$prop),]$PM)
  Pechetransf[!is.na(Pechetransf$prop),]$MOULE<-1000/Pechetransf[!is.na(Pechetransf$prop),]$PM
  #levels(Pechetransf$SIGNEP)<-c("0","G")
  Pechetransf[!is.na(Pechetransf$prop),]$SIGNEP<-as.character(substr(Pechetransf[!is.na(Pechetransf$prop),]$CodEsp2,10,10))
  Pechetransf$scat<-Pechetransf$SIGNEP
  substr(Pechetransf[!is.na(Pechetransf$prop),]$CodEsp,15,15)<-as.character(Pechetransf[!is.na(Pechetransf$prop),]$SIGNEP)
  Pechetransf<-unique(Pechetransf[,!names(Pechetransf)%in%c('x','CodEsp2','prop','PM2','LM2','scat')])
  head(Pechetransf)
  dim(Pechetransf);dim(unique(Pechetransf))
  return(Pechetransf)
} 

# function to correct for assessed species in catches and not in echotype/species table
# ****************************--

EspDevCor=function(EspDev,Pecheis,lsp.assess,dev.add=NULL,
                   dev.surf='D4'){
  uPecheis=unique(Pecheis[,c('GENR_ESP','STRATE','CodEsp')])
  mspic=uPecheis[(uPecheis$GENR_ESP%in%lsp.assess)&
                   (!uPecheis$CodEsp%in%unique(EspDev$CodEsp)),
                 c('GENR_ESP','CodEsp','STRATE')]
  if(dim(mspic)[1]>0){
    cat("Assessed species in catches and not in echotype/species table, pass 1",'\n')  
    #add missing species in EspDev
    names(mspic)[2]='CodEsp2'
    espdeva2=merge(mspic,EspDev,by.x=c('GENR_ESP','STRATE'),
                   by.y=c('GENR_ESP','STRATE'),
                   all.x=TRUE)    
    espdeva2$CodEsp=espdeva2$CodEsp2
    espdeva2$STRATE2=substr(espdeva2$CodEsp,10,13)
    espdeva2$SIGNEP=substr(espdeva2$CodEsp,15,15)
    espdeva2$DESCR='sp ajoutes'
    espdeva2$NUM=0
    # correct stratum name
    espdeva2$STRATE=as.character(espdeva2$STRATE)
    espdeva2[!espdeva2$STRATE%in%c('ALLS','ALLC'),'STRATE']=
      espdeva2[!espdeva2$STRATE%in%c('ALLS','ALLC'),'STRATE2']
    #espdeva2[espdeva2$STRATE=='SURF','DEV']='D4'
    #espdeva2=espdeva2[!(espdeva2$STRATE=='CLAS'&espdeva2$DEV=='D4'),]
    espdeva2=unique(espdeva2)
    nids=paste(espdeva2$CodEsp,espdeva2$STRATE,espdeva2$DEV)
    if (length(nids)!=length(unique(nids))) stop ('Duplicated new species in EspDev')
    espdeva2[,c('CodEsp','STRATE','DEV')]
    espdeva2=espdeva2[,names(EspDev)]
    EspDev=rbind(EspDev,espdeva2)
    EspDev=EspDev[!is.na(EspDev$CAMPAGNE),]
    mspic2=uPecheis[(uPecheis$GENR_ESP%in%lsp.assess)&
                      (!uPecheis$CodEsp%in%unique(EspDev$CodEsp)),
                    c('GENR_ESP','CodEsp')]
    dim(mspic);dim(mspic2)
    if(dim(mspic2)[1]>0){
      if (is.null(dev.add)){
        stop("Assessed species in catches and not in echotype/species table: ",paste(mspic2$CodEsp,collapse=' '))  
      }else{
        #add missing species in EspDev
        names(mspic2)[2]='CodEsp2'
        espdeva3=NULL
        for (i in 1:length(dev.add)){
          espdeva3i=cbind(mspic2,DEV=dev.add[i])
          espdeva3=rbind(espdeva3,espdeva3i)
        }
        espdeva3$CodEsp=espdeva3$CodEsp2
        espdeva3$STRATE=substr(espdeva3$CodEsp,10,13)
        espdeva3$SIGNEP=substr(espdeva3$CodEsp,15,15)
        espdeva3$DESCR='sp ajoutes 2'
        espdeva3$NUM=0
        espdeva3$CAMPAGNE=unique(EspDev$CAMPAGNE)
        espdeva3=espdeva3[,names(EspDev)]
        espdeva3$DEV=as.character(espdeva3$DEV)
        #espdeva3[espdeva3$STRATE=='SURF','DEV']=dev.surf
        EspDev=rbind(EspDev,espdeva3)
        mspic3=uPecheis[(uPecheis$GENR_ESP%in%lsp.assess)&
                          (!uPecheis$CodEsp%in%unique(EspDev$CodEsp)),
                        c('GENR_ESP','CodEsp')]
        if(dim(mspic3)[1]>0){
            stop("Assessed species in catches and not in echotype/species table: ",paste(mspic3$CodEsp,collapse=' '))  
        }else{
          cat("All assessed species in catches are now in echotype/species table",'\n')  
        }  
      }
    }else{
      cat("All assessed species in catches are now in echotype/species table",'\n')  
    }  
    sort(unique(EspDev$CodEsp))
  }else{
    espdeva2=NULL
  }
  # Select only species found in lsp.assess
  EspDev=EspDev[EspDev$GENR_ESP%in%lsp.assess,]
  EspDev=EspDev[order(EspDev$DEV),]
  
  list(EspDev=EspDev,espdeva2=espdeva2)
}

importBaracoudaTotalSamples=function(path.fish){
  fbara=read.table(path.fish,sep=';',header=TRUE)
  unique(fbara$ETAT)
  fbaras=fbara[fbara$CAMPAGNE==cruise,]
  fbaras$PTRI=fbaras$PT
  fbaraH=fbaras[fbaras$SIGNEP=='H',]
  unique(fbaras$ETAT)
  head(fbaras)
  unique(fbaras$STRATE)
  dim(fbaras)
  names(fbaras)=c('CAMPAGNE','NOCHAL','STRATE','LATF','LONF','SONDE','TC','ETAT','NOSTA',
                  'GENR_ESP','SIGNEP','PT','PMENS','PBIOM','PM','LM','NT','MOULE','PTRI')
  fbaras$CodEsp=paste(fbaras$GENR_ESP,fbaras$STRATE,fbaras$SIGNEP,sep='-')
  fbaras
}

importBaracoudaEchotypes=function(path.echotypes,path.echotypage,cruise){
  scrut=read.table(path.echotypage,sep=';',header=TRUE)
  head(scrut)
  udevs=unique(scrut[,c('CAMPAGNE','ID_DESCRIPTION_DEVIATION')])
  echotypes=read.table(path.echotypes,sep=';',header=TRUE)
  head(echotypes)
  echot=merge(echotypes,udevs)
  echots=echot[echot$CAMPAGNE==cruise,]
  echots[order(echots$ECHOS),]
  echots2=unique(echots[,!names(echots)%in%c('LIBELLE_DESCRIPTION_DEVIATION','ID_DESCRIPTION_DEVIATION')])
  head(echots2)
  # remove classic D4
  echots2=echots2[!(echots2$ECHOS=='D4'&echots2$IMAGES=='CLAS'),]
  names(echots2)=c('DEV','STRATE','GENR_ESP','CAMPAGNE')
  echots2$DESCR=NA
  echots2$NUM=NA
  echots2$SIGNEP=0
  echots2$CodEsp=paste(echots2$GENR_ESP,echots2$STRATE,echots2$SIGNEP,sep='-')
  echots2=echots2[order(echots2$DEV),]
  echots2
}

importBaracoudaScrutinising=function(path.echotypage,cruise,allCols=TRUE){
  scrut=read.table(path.echotypage,sep=';',header=TRUE)
  head(scrut)
  if (allCols){scruts=scrut[scrut$CAMPAGNE==cruise,]
  }else{
    scruts=scrut[scrut$ID_EI_ESDU==cruise,]
  }
  length(unique(scruts$DHTU_ESDU))
  head(scruts)
  names(scruts)
  names(scruts)[1:5]=c("esdu.id","LONG","LAT","depth","TC")
  scruts$esdu.id=gsub(',','.',scruts$esdu.id)
  scruts$depth=as.numeric(gsub(',','.',scruts$depth))
  scruts$R=as.integer(gsub(',','.',scruts$R))
  scruts$FLAG=as.integer(gsub(',','.',scruts$FLAG))
  scruts$TOTAL=as.numeric(gsub(',','.',scruts$TOTAL))
  scruts$ENERGIE=as.numeric(gsub(',','.',scruts$ENERGIE))
  scruts$LONG=correct.positions(
    df=scruts$LONG,xname="LONG",asNewColumn=FALSE)
  scruts$LAT=correct.positions(
    df=scruts$LAT,yname="LAT",asNewColumn=FALSE)
  names(scruts)[8:13]=c("RAD","f","FLAG","TOTAL","DEV","ENERGIE")
  scruts.wide1=reshape(scruts[,c('TC','DEV','ENERGIE')],direction='wide',idvar='TC',timevar='DEV')
  head(scruts.wide1)
  names(scruts.wide1)=c('TC',substr(names(scruts.wide1)[-1],9,11))
  if (allCols){
    scruts.wide2=reshape(scruts[,c('TC','DEV','NOCHA')],direction='wide',idvar='TC',timevar='DEV')
    head(scruts.wide2)
    names(scruts.wide2)=c('TC',paste(substr(names(scruts.wide2)[-1],7,9),'.CREF',sep=''))
    scruts.wide3=reshape(scruts[,c('TC','DEV','POST_STRATE_SCENARIO')],direction='wide',idvar='TC',timevar='DEV')
    head(scruts.wide3s)
    scruts.wide3s=scruts.wide3[,c('TC','POST_STRATE_SCENARIO.D1','POST_STRATE_SCENARIO.D4')]
    names(scruts.wide3s)=c('TC','zonesCLAS','zonesSURF')
    head(scruts.wide3s)
  }
  scruts.wide=merge(unique(scruts[,c('TC','esdu.id','LONG','LAT','depth','TOTAL','RAD','f','FLAG')]),scruts.wide1,by.x='TC',by.y='TC')
  if (allCols){
    scruts.wide=merge(scruts.wide,scruts.wide2,by.x='TC',by.y='TC')
    scruts.wide=merge(scruts.wide,scruts.wide3s,by.x='TC',by.y='TC')
    scruts.wide$CAMPAGNE=cruise
  }
  # check for ESDU unicity
  head(scruts.wide)
  if (length(scruts.wide$TC)>length(unique(scruts.wide$TC))) stop('Duplicated ESDUs')
  scruts.wide=scruts.wide[order(scruts.wide$TC),]
  scruts.wide$esdu.id=seq(length(scruts.wide$TC))
  #scruts2=scruts.wide[,7:(6+length(unique(scruts$DEV)))]
  #     head(scruts2)
  #     scruts2[,1]
  #     TOTAL2=apply(scruts2,1,sum)
  #     scruts2-TOTAL2
  # TOTAL = total NASC
  scruts.wide
}


aggregate.subsamples=function(mens4Echobase){
  ss.ids=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,mens4Echobase$sizeCategory,
               mens4Echobase$lengthClass,mens4Echobase$sexCategory)
  #sort(ss.ids[duplicated(ss.ids)])
  if (sum(duplicated(ss.ids))>0){
    mens4Echobasea=aggregate(mens4Echobase[,c('sampleWeight','numberSampled',
                                              'numberAtLength',
                                              'weightAtLength')],
                             list(operationId=mens4Echobase$operationId,
                                  baracoudaCode=mens4Echobase$baracoudaCode,
                                  sizeCategory=mens4Echobase$sizeCategory,
                                  lengthClass=mens4Echobase$lengthClass,
                                  sexCategory=mens4Echobase$sexCategory),
                             sum,na.rm=TRUE)
    head(mens4Echobasea)
    mens4Echobaseamax=aggregate(mens4Echobase[,c('sampleWeight','numberSampled')],
                                list(operationId=mens4Echobase$operationId,
                                     baracoudaCode=mens4Echobase$baracoudaCode,
                                     sizeCategory=mens4Echobase$sizeCategory,
                                     sexCategory=mens4Echobase$sexCategory),
                                max,na.rm=TRUE)
    names(mens4Echobaseamax)[5:6]=paste(names(mens4Echobaseamax)[5:6],'Max',sep='')
    mens4Echobasea=merge(mens4Echobasea,mens4Echobaseamax)
    head(mens4Echobasea)
    mens4Echobasea$sampleWeight=mens4Echobasea$sampleWeightMax
    mens4Echobasea$numberSampled=mens4Echobasea$numberSampledMax
    mens4Echobasea[mens4Echobasea$weightAtLength==0,'weightAtLength']=NA
    mens4Echobase=merge(unique(mens4Echobase[,c('operationId','baracoudaCode',
                                                'sizeCategory',
                                                'sexCategory','units','round')]),
                        mens4Echobasea,by.x=c('operationId','baracoudaCode',
                                              'sizeCategory','sexCategory'),
                        by.y=c('operationId','baracoudaCode','sizeCategory','sexCategory'))
    mens4Echobase=mens4Echobase[,!names(mens4Echobase)%in%c('sampleWeightMax','numberSampledMax')]
  }else{
    cat('No duplicates found in sub-samples','\n')
  }
  ss.ids=paste(mens4Echobase$operationId,mens4Echobase$baracoudaCode,mens4Echobase$sizeCategory,
               mens4Echobase$lengthClass,mens4Echobase$sexCategory)
  length(ss.ids)
  length(unique(ss.ids))
  if (length(ss.ids)>length(unique(ss.ids))){
    cat('Duplicated sub-samples in: ',paste(ss.ids[duplicated(ss.ids)],collapse=' '),'\n')
  }
  mens4Echobase
}
