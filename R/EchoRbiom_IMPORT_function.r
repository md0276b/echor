#--------------------------------------------------------------------------------
# EchoRbiom: acoustics+catches=biomass
#-----------  
# Original code by Pierre Petitgas, 02/ 06/ 2003
# Adapted by Mathieu Doray, 11/02/2010, 2011, 2012
#--------------------------------------------------------------------------------
#  Acoustic fish biomass assessment, Ifremer 'expert' methodology 
###-------------------------------------------------------------------------

#*******************************************************************************************************
# STEP 1: IMPORT AND CHECK DATA
#*******************************************************************************************************

#-------------------------------------------------------------------------

EchoRbiom.import=function(fishRview=FALSE,
                          totalSamples,subSamples,acou,echotypes,path.results=NULL,
                          convert.header=FALSE,sep=';',plotit=TRUE,cruise=cruise,
                          complementit=TRUE,nid='NOCHAL',ux11=FALSE){

#-------------------------------------------------------------------------
#I. Import and check FishRview outputs
#---------------------------------------------------------------

#---------------------------------  
#1. Import and check catch data
#---------------------------------
if (class(totalSamples)=='character'){
  Pechei=read.table(totalSamples,sep=sep,header=TRUE)
  head(Pechei);dim(Pechei)
}else{
  Pechei=totalSamples
}  
#convert headers from Echobase to EchoR formats
if (convert.header){
  Pechei=header.converter(x=Pechei,direction='base2R')
}
head(Pechei);dim(Pechei)

#1.1. No. of fishing operations
#---------------------------------
length(unique(Pechei$NOSTA))

#1.2. Check for duplicated species in catches
#---------------------------------
duplicated.speciesIncatches.check(Pechei)

#1.3. Check depth strata
#---------------------------------  
unique(Pechei$STRATE)

#1.4. Check (and eventually correct) size categories
#---------------------------------
unique(Pechei$SIGNEP)
table(Pechei$SIGNEP)
table(Pechei$GENR_ESP,Pechei$SIGNEP)

#Pechei[,'SIGNEP']=0
Pechei$CodEsp=paste(Pechei$GENR_ESP,Pechei$STRATE,Pechei$SIGNEP,sep='-')

#1.6. Aggregate species with same species code
#---------------------------------
dim(Pechei)
rac=aggregate.catches(Pechei,spname='CodEsp',stname='NOSTA')
names(rac)
Pechei=rac$Pechei
dim(Pechei)
head(Pechei)

#1.7. Check length-weight relationships from catch data
#---------------------------------
#Select main species
lspcodes.Pechef=c("ENGR-ENC","SARD-PIL","SCOM-JAP",
                  "SCOM-SCO","SPRA-SPR","TRAC-MED","TRAC-TRU")
#Compute and display length-weight relationships for all species
x11();dev.off()
LW=LW.compute(catch=Pechei,lspcodes.mens=NULL,
              codespname='GENR_ESP',Lname='LM',Wname='PM',gname=NA,
              plotit=plotit,compute.LW=TRUE,Wscaling=1,ux11=ux11,cruise=cruise)
#Eventually, export plots
if (plotit&!is.null(path.results)){
  dev.print(device=png,
          filename=paste(path.results,cruise,"_TotalCatchLW1.png",sep=''),
          width=800,height=800)
  dev.set(which = dev.next())
  dev.print(device=png,
          filename=paste(path.results,cruise,"_TotalCatchLW2.png",sep=''),
          width=800,height=800)
}

#1.8. Remove invalid hauls before biomass assessment
#---------------------------------  
Pechei=Pechei[Pechei$STRATE!='NULL',]
unique(Pechei$STRATE)

#-----------------------------------  
#2. Import and check biological measurements (subsamples)
#-----------------------------------
#2.1. data import
#-----------------------------------
if (class(subSamples)=='character'){
  mens=read.table(subSamples,sep=sep,header=TRUE)
}else{
  mens=subSamples
}  
head(mens);dim(mens)
#convert headers from Echobase to EchoR formats
if (convert.header){
  mens=header.converter(x=mens,direction='base2R')
}  
head(mens);dim(mens)

#2.2. check size categories and species codes
#   species code = species+size category
#   each species code will be assessed separately 
#-----------------------------------
unique(mens$CATEG)  
table(mens$CATEG)
table(mens$GENR_ESP,mens$CATEG)
mens$CodEsp2=paste(mens$GENR_ESP,mens$CATEG,sep='-')
unique(mens$CodEsp2)

#2.3. check length distributions
#-----------------------------------
mens.range.check(mens,ux11=ux11,plotit=plotit)


#2.4. Eventually, correct size category
#-----------------------------------
#
mens[,'CATEG']=0
#
mens$CodEsp2=paste(mens$GENR_ESP,mens$CATEG,sep='-')

#2.5. Compute and check length-weight relationships
#-----------------------------------
#Select main species
lspcodes.mens=c("ENGR-ENC-0","SARD-PIL-0","SCOM-JAP-0",
                "SCOM-SCO-0","SPRA-SPR-0","TRAC-MED-0","TRAC-TRU-0")  
#Compute and display length-weight relationships for all species
x11();dev.off()
LW=LW.compute(catch=mens,codespname='CodEsp2',
              Lname='Lcm',Wname='PM',wname='NBIND',plotit=plotit,Ncol=4,cruise=cruise)
#Compute and display length-weight relationships for main species
x11();dev.off()
LW=LW.compute(catch=mens,lspcodes.mens=lspcodes.mens,codespname='CodEsp2',
              Lname='Lcm',Wname='PM',wname='NBIND',plotit=plotit,Nmin=20,cruise=cruise)
LW$GENR_ESP=substr(LW$CodEsp2,1,8)

#2.6. Remove invalid biological measurements
#----------------------------------- 
mens.cor=mens[mens$NBIND>0|!is.na(mens$NBIND),]

#2.7. Eventually, add haul numbers
#-----------------------------------
dim(mens)
mens=merge(mens,unique(Pechei[,c('NOSTA','NOCHAL')]),by.x='NOSTA',
           by.y='NOSTA')
dim(mens)

#---------------------------------  
# 3. Import echotypes definitions
#---------------------------------
if (class(echotypes)=='character'){
  EspDev=read.table(echotypes,sep=sep,header=TRUE)
}else{
  EspDev=echotypes
}  
head(EspDev);dim(EspDev)

#convert headers from Echobase to EchoR formats
if (convert.header){
  EspDev=header.converter(x=EspDev,direction='base2R')
}  
head(EspDev);dim(EspDev)

EspDev$CodEsp=paste(EspDev$GENR_ESP,EspDev$STRATE,EspDev$SIGNEP,sep='-')

#3.1. Extract metadata
#--------------------------------- 
meta=get.metadata(EspDev)
lyears=cruise
lNdev=meta$lNdev
EspDev$CAMPAGNE=cruise
Ndevi=lNdev[[1]]

#---------------------------------
# 4. Import NASC per echotype data
#---------------------------------
# 4.1. OR direct import from text file
#---------------------------------
if (class(acou)=='character'){
  charef=read.table(acou,sep=sep,header=TRUE)
}else{
  charef=acou
}  
#convert headers from Echobase to EchoR formats
head(charef)
if (convert.header){
  charef=header.converter(x=charef,direction='base2R')
}  
head(charef);dim(charef)
names(charef)[names(charef)%in%'esduid']='esdu.id'
names(charef)[names(charef)%in%paste('D',lNdev[[1]],'CREF',sep='')]=
  paste('D',lNdev[[1]],'.CREF',sep='')

#check that no reference haul is missing
if (T%in%apply(charef[,names(charef)%in%paste('D',lNdev[[1]],'.CREF',sep='')],2,is.na)) cat('Reference hauls missing')

ESDUDEV=charef

#some ESDUs have duplicated times?
length(ESDUDEV$TC);length(unique(ESDUDEV$TC))

# 4.2. adds total NASC per ESDU
#--------------------
lecho=paste('D',lNdev[[1]],sep='')
if (sum(!lecho%in%names(ESDUDEV))>0) stop ('Echotype',lecho[!lecho%in%names(ESDUDEV)],'missing in scrutinising files')
ESDUDEV$TOTAL=rowSums(ESDUDEV[,paste('D',lNdev[[1]],sep='')])
ESDUDEV$CAMPAGNE=cruise

# 4.3. Check geographical coverage and total no. of ESDUs
#--------------------
if (plotit){
  par(mfrow=c(1,1))
  plot(ESDUDEV$LONG,ESDUDEV$LAT,main=paste(cruise,'ESDUs,','N =',dim(ESDUDEV)[1]),
       xlab='',ylab='',cex=0.1+log(ESDUDEV$TOTAL+1)/10)
  coast()
}

# library(ggmap)
# library(mapproj)
# map <- get_map(location = 'Portugal', zoom =7,source='osm')
# ggmap(map)

# 4.4. Keep only flagged ESDUs
#--------------------
#Eventually, set flagged ESDU to be kept
ESDUDEV$FLAG=1  
ESDUDEV[duplicated(ESDUDEV$TC),'FLAG']=0
ESDUDEVs=ESDUDEV[ESDUDEV$FLAG==1,]
head(ESDUDEVs)
dim(ESDUDEV);dim(ESDUDEVs)
#Adds dummy region variables if missing
ESDUDEVs$zonesSURF=NA
ESDUDEVs$zonesCLAS=NA

# 4.5. check NASC per echotypes
#---------------------------------
# look for negative NASC
check.esdudev(ESDUDEVs,Ndev=lNdev[[1]])
# display NASC quantiles per echotypes, to be compared with previous years
apply(ESDUDEVs[,paste('D',lNdev[[1]],sep='')],2,summary)
# check postratification regions
unique(ESDUDEVs$zonesSURF)
unique(ESDUDEVs$zonesCLAS)

# 4.6. Total fish NASC map
#---------------------------------
if (plotit){
  par(mfrow=c(1,1))
  plot(ESDUDEV$LONG,ESDUDEV$LAT,xlab='',ylab='',main='PELGAS2012, total fish NASC',
       type='n')
  coast()
  points(ESDUDEV$LONG,ESDUDEV$LAT,pch=16,cex=0.1+log(ESDUDEV[,'TOTAL']+1)/5)
  points(ESDUDEV[ESDUDEV$FLAG==1,'LONG'],ESDUDEV[ESDUDEV$FLAG==1,
                                                 'LAT'],cex=0.1+log(ESDUDEV[ESDUDEV$FLAG==1,'TOTAL']+1)/5,pch=16,col=2)
  legend('bottomleft',legend=c('All ESDUS','Flagged ESDUs'),pch=16,col=seq(2))
}

# 4.7. Check NASC per echotype distributions ('profiles')
#---------------------------------
head(ESDUDEVs)
ESDUDEVs$TOTAL=rowSums(ESDUDEVs[,as.character(unique(EspDev$DEV))])
N=dim(ESDUDEVs)[1]
Ds=c(unlist(ESDUDEVs[,paste('D',Ndevi,sep='')]))
Dl=rep(Ndevi,each=N)
Dsum=data.frame(Ds=Ds,Dl=Dl)

if (plotit){
  par(mfrow=c(2,2))
  boxplot(ESDUDEVs$TOTAL+1,main='Fish NASC distribution',log='y')
  boxplot(Ds+1~Dl,Dsum,main='Fish NASC distribution per echotype',log='y')
  plot(Ndevi,colMeans(ESDUDEVs[,as.character(unique(EspDev$DEV))]),
       xlab='Echotype',ylab='Mean NASC')
}

#---------------------------------------------------------------
# 5. IF "complement" in echotype definition,
# pools non assessed species into a fake "complement" ('COMP-LEM') 
# species with:
#  'SIGNEP'='0'
#  'LM'=20
#  'MOULE'=20
#  'PM'=0.05
#	 'CAC'=20
#  'BAC'=67
#---------------------------------------------------------------
lsp.assess=c('SARD-PIL','ENGR-ENC','MICR-POU','SCOM-SCO','TRAC-TRU')

# 5.1. assessed species in catches and not in echotype/species table
#---------------------------------------------------------------  
unique(Pechei$CodEsp)[(substr(unique(Pechei$CodEsp),1,8)%in%lsp.assess)&
                        (!unique(Pechei$CodEsp)%in%unique(EspDev$CodEsp))]

# 5.2. Compute "complement"
#---------------------------------------------------------------
dim(Pechei)

if (complementit){
  Pechei.comp=complement(Pechei,EspDev,wmens=FALSE)
  Pechei=Pechei.comp
  dim(Pechei.comp)
}
  
#---------------------------------------------------------------
# 6. Fishing data correction and selection
#---------------------------------------------------------------
# 6.1. remove or correct catches with null 'MOULE'n, mean length or total weight
# and returns dataframes with corrected and bad data
#---------------------------------------------------------------

Pechei.cor=check.catches(captures.comp=Pechei,path.checkit=path.results,
                         correctit=TRUE,removeit=TRUE,LW=NULL)
dim(Pechei);dim(Pechei.cor$captures.OK)

# 6.2. Select catches data between several flavors:
# - complemented: Peche.comp
# - or not: Peche
# - corrected but not complemented: Peche.cor
# - corrected and complemented: Peche.comp.cor
#---------------------------------------------------------------
#Pechef=Pechei.cor$captures.OK
Pechef=Pechei
Pechei.pb=Pechei.cor$captures.PB
dim(Pechef)  
dim(Pechei.pb)

# 6.3. Check for duplicated species 
#---------------------------------------------------------------
duplicated.speciesIncatches.check(Pechef)

# 6.4. Check length-weight relationships of corrected species
#---------------------------------------------------------------
x11();dev.off()
LWcor=LW.compute(catch=Pechef,lspcodes.mens=NULL,
                 codespname='GENR_ESP',Lname='LM',Wname='PM',gname=NA,
                 plotit=plotit,compute.LW=FALSE,Wscaling=1000,cruise=cruise)
#export plots
if (plotit&!is.null(path.results)){
  dev.print(device=png,
          filename=paste(path.results,cruise,"_TotalCorCatchLW1.png",sep=''),
          width=800,height=800)
  dev.set(which = dev.next())
  dev.print(device=png,
          filename=paste(path.results,cruise,"_TotalCorCatchLW2.png",sep=''),
          width=800,height=800)
}

# 6.5. Check catches,length and weight ranges of corrected catches
#---------------------------------------------------------------
if (plotit){
  par(mfrow=c(1,1))
}  
captures.range.check(Pechef,EspDev,plotit=plotit)
if (plotit&!is.null(path.results)){
dev.print(device=png,
          filename=paste(path.results,cruise,"_TotalCorCatchLWsum.png",sep=''),
          width=800,height=800)
}

# 6.6. Check haul geographic positions
#--------------------------------------------------------------- 
hpc.res=haul.positions.check(Pechef,correctit=TRUE,
                             path.checkit=path.results)

#-------------------------------------------------------------------
# 7. Put catches in "wide" format for piecharts
#-------------------------------------------------------------------
# catches per species
Pechels=widen.catch(Pechef,nid=nid,nsp='GENR_ESP')
# catches per species code
Pechels2=widen.catch(Pechef,nid=nid,nsp='CodEsp')

# Grand average of catches per hauls
#---------------------------
if (plotit){
  par(mfrow=c(1,1))
  head(Pechels)
  # catches per species
  pie(colMeans(Pechels[,!names(Pechels)%in%c('CAMPAGNE',nid,'LONF','LATF','STRATE','SONDE')]))
  # catches per species code
  pie(colMeans(Pechels2[,!names(Pechels2)%in%c('CAMPAGNE',nid,'LONF','LATF','STRATE','SONDE')]))
}  

list(Pechef=Pechef,mens=mens,EspDev=EspDev,ESDUDEVs=ESDUDEVs,Pechels=Pechels,Pechels2=Pechels2)

}