#***************************
#Functions for acoustic biomass assessment
#***************************
#ESDU~HAULS ASSOCIATION
#***************************

	#Longitudes correction factor for 
  #***************************	
	corr=cos(46*pi/180)

	#***************************	
	#nearest haul method
	#***************************	
	nearest.haul=function(Pechei,EsduDevi,EspDevi,Ndevi,
	                      corr=cos(46*pi/180),nid='NOCHAL',depthClass=NULL,
	                      nsp='CodEsp',forceFind=FALSE){
	  
	  if (!is.null(depthClass)){
	    #selection of haul positions in Peche, by depth strata
	    for (i in 1:(length(depthClass$zlim)-1)){
	      czn=cut(depthClass$zlim,c(0,depthClass$zlim),right=TRUE)
	      cat('Depth class',as.character(czn[i+1]),'m','\n')
	      cz=cut(depthClass$zlim,c(0,depthClass$zlim),right=TRUE)
	      pcz=cut(Pechei$SONDE,c(0,depthClass$zlim),right=TRUE)
	      if (sum(is.na(pcz))>0){
	        stop(paste(unique(Pechei[is.na(pcz),nid]),'Haul seabed depth not in depthClass','\n'))
	      } 
	      Pecheis=Pechei[pcz==as.character(cz[i+1]),]
	      ecz=cut(EsduDevi$depth,c(0,depthClass$zlim),right=TRUE)
	      if (sum(is.na(ecz))>0){
	        stop(EsduDevi[is.na(ecz),],'ESDU seabed depth not in depthClass','\n')
	      } 
	      EsduDevis=EsduDevi[ecz%in%as.character(cz[i+1]),]

	      if(dim(EsduDevis)[1]>0){
	        
	        list.nearchal.devi=lapply(X=seq(length(Ndevi)),
	                                  FUN=nearchal.DevSpcheck,
	                                  Ndevi,EsduDevi=EsduDevis,
	                                  EspDevi,Pechei=Pecheis,
	                                  nid=nid,depthClass=depthClass,nsp=nsp)
	        if (!is.null(depthClass)){ # check if depth limits have been provided...
	          if (sum(!is.na(depthClass$dlim))>0){ # check if distance limits within depth limits have been provided...
	            # simple, distance-based selection of haul positions in Peche
	            list.nearchal.devic=lapply(X=seq(length(Ndevi)),
	                                     FUN=nearchal.DevSpcheck,
	                                     Ndevi,EsduDevi=EsduDevi,EspDevi,Pechei=Pechei,nid=nid,depthClass=NULL,nsp=nsp)
	            # replace NA by nearest haul
	            for (j in 1:length(list.nearchal.devi)){
	              if (sum(!is.na(list.nearchal.devi[[j]]$num.chal.haut))>0){
	                nchi1=list.nearchal.devi[[j]]$num.chal.haut
	                nchi2=list.nearchal.devic[[j]]$num.chal.haut
	                nes=EsduDevi$esdu.id%in%EsduDevis$esdu.id
	                nchi2s=nchi2[nes]
	                nchi1[is.na(nchi1)]=nchi2s[is.na(nchi1)]
	                list.nearchal.devi[[j]]$num.chal.haut=nchi1
	              }
	              if (!is.null(list.nearchal.devi[[j]]$Chal.haut)){
	                chi1=list.nearchal.devi[[j]]$Chal.haut
	                chi2=list.nearchal.devic[[j]]$Chal.haut
	                list.nearchal.devi[[j]]$Chal.haut=unique(rbind(chi1,chi2[chi2$NOCHAL%in%nchi1,]))
	              }
	              if (sum(!is.na(list.nearchal.devi[[j]]$num.chal.bas))>0){
	                nchi1=list.nearchal.devi[[j]]$num.chal.bas
	                nchi2=list.nearchal.devic[[j]]$num.chal.bas
	                length(nchi1);length(nchi2)
	                nes=EsduDevi$esdu.id%in%EsduDevis$esdu.id
	                nchi2s=nchi2[nes]
	                nchi1[is.na(nchi1)]=nchi2s[is.na(nchi1)]
	                list.nearchal.devi[[j]]$num.chal.bas=nchi1
	              }
	              if (!is.null(list.nearchal.devi[[j]]$Chal.bas)){
	                chi1=list.nearchal.devi[[j]]$Chal.bas
	                chi2=list.nearchal.devic[[j]]$Chal.bas
	                list.nearchal.devi[[j]]$Chal.bas=unique(rbind(chi1,chi2[chi2$NOCHAL%in%nchi1,]))
	              }
	            }
	          }
	        }
	        
	        #list of hauls/ESDUs pairs
	        if ('SURF'%in%unique(EspDevi$STRATE)){
	          Dsurf=unique(EspDevi[EspDevi$STRATE=='SURF','DEV'])
	        }else{
	          Dsurf=NULL
	        }
	        
	        list.Associ=lapply(X=seq(length(Ndevi)),
	                           FUN=assoc.k,EsduDevi=EsduDevis,
	                           list.nearchal.dev=list.nearchal.devi,
	                           Dsurf=Dsurf,Ndevi=Ndevi,forceFind=forceFind)
	        if (i==1){
	          list.nearchal.dev=list(list.nearchal.devi)
	          names(list.nearchal.dev)=as.character(czn[i+1])
	          list.Assoc=list.Associ
	        }else{
	          list.nearchal.dev=c(list.nearchal.dev,list(list.nearchal.devi))
	          names(list.nearchal.dev)
	          names(list.nearchal.dev)[i]=as.character(czn[i+1])
	          for (j in 1:length(list.Assoc)){
	            # 	            if (!is.null(list.nearchal.devi[[j]]$Chal.haut)){
	            # 	              list.nearchal.dev[[j]]$Chal.haut==rbind(list.nearchal.dev[[j]]$Chal.haut,
	            # 	                                                      list.nearchal.devi[[j]]$Chal.haut)
	            # 	              list.nearchal.dev[[j]]$num.chal.haut=c(list.nearchal.dev[[j]]$num.chal.haut,
	            # 	                                                     list.nearchal.devi[[j]]$num.chal.haut)
	            # 	            }
	            # 	            if (!is.null(list.nearchal.devi[[j]]$Chal.bas)){
	            # 	              list.nearchal.dev[[j]]$Chal.bas==rbind(list.nearchal.dev[[j]]$Chal.bas,
	            # 	                                                      list.nearchal.devi[[j]]$Chal.bas)
	            # 	              list.nearchal.dev[[j]]$num.chal.bas=c(list.nearchal.dev[[j]]$num.chal.bas,
	            # 	                                                     list.nearchal.devi[[j]]$num.chal.bas)
	            # 	            }
	            list.Assoc[[j]]=rbind(list.Assoc[[j]],list.Associ[[j]])
	            dim(EsduDevi)
	            dim(list.Assoc[[j]])
	          }
	        }
	      }else{
	        cat('No ESDU in depth class',as.character(czn[i+1]),'\n')
	      }
	    }
	  }else{
	    #selection of haul positions in Peche
	    list.nearchal.dev=lapply(X=seq(length(Ndevi)),
	                             FUN=nearchal.DevSpcheck,
	                             Ndevi,EsduDevi,EspDevi,Pechei,nid=nid,nsp=nsp)
	    #list of hauls/ESDUs pairs
	    if ('SURF'%in%unique(EspDevi$STRATE)){
	      Dsurf=unique(EspDevi[EspDevi$STRATE=='SURF','DEV'])
	    }else{
	      Dsurf=NULL
	    }
	    list.Assoc=lapply(X=seq(length(Ndevi)),
	                      FUN=assoc.k,EsduDevi,list.nearchal.dev,
	                      Dsurf=Dsurf,Ndevi=Ndevi,forceFind=forceFind)
	  }
	  
	  list(list.nearchal.dev=list.nearchal.dev,list.Assoc=list.Assoc)
	}
	
	
	

	nearchal.DevSpcheck=function(k,Ndevi,EsduDevi,EspDevi,Pechei,
		corr=cos(46*pi/180),nid=nid,nsp='CodEsp',depthClass=NULL){

		cat('Dev',Ndevi[k],"\n")
	  Pechei$CodEsp2=paste(Pechei$GENR_ESP,Pechei$SIGNEP,sep='-')
	  EspDevi$CodEsp2=paste(EspDevi$GENR_ESP,EspDevi$SIGNEP,sep='-')
		Pechek=Pechei[Pechei[,nsp]%in%EspDevi[EspDevi$DEV==Ndevi[k],nsp],]
		if (!is.null(depthClass)){
		  dlimk=depthClass$dlim[k]
		}else{
		  dlimk=NA
		}
		if (dim(Pechek)[1]>0){
			j=match(unique(Pechek[,nid]),Pechek[,nid])
			Chalk=data.frame(NOCHAL=Pechek[,nid][j],LG=Pechek$LONF[j],
				LAT=Pechek$LATF[j],TYP=Pechek$STRATE[j])

			Chal.haut=split(Chalk,Chalk$TYP=='SURF')$'TRUE'
			if (!is.null(Chal.haut)){
				num.chal.haut=Chal.haut$NOCHAL[sapply(seq(along=EsduDevi[,1]),
					y.plus.proche.x.i,x1=EsduDevi$LONG*corr,
					x2=EsduDevi$LAT,y1=Chal.haut$LG*corr,
					y2=Chal.haut$LAT)]
				d.chal.haut=sapply(seq(along=EsduDevi[,1]),
				                  y.plus.proche.x.i.d,x1=EsduDevi$LONG*corr,
				                  x2=EsduDevi$LAT,y1=Chal.haut$LG*corr,
				                  y2=Chal.haut$LAT)
				if (!is.na(dlimk)){
				  num.chal.haut[d.chal.haut>(dlimk/60)]=NA
				  Chal.haut=Chal.haut[Chal.haut$NOCHAL%in%num.chal.haut[!is.na(num.chal.haut)],]
				}
			}else num.chal.haut=NA

			Chal.bas=split(Chalk,Chalk$TYP=='CLAS')$'TRUE'
			if (!is.null(Chal.bas)){
				num.chal.bas=Chal.bas$NOCHAL[sapply(seq(along=EsduDevi[,1]),
					y.plus.proche.x.i,x1=EsduDevi$LONG*corr,
					x2=EsduDevi$LAT,y1=Chal.bas$LG*corr,
					y2=Chal.bas$LAT)]
				d.chal.bas=sapply(seq(along=EsduDevi[,1]),
				                                    y.plus.proche.x.i.d,x1=EsduDevi$LONG*corr,
				                                    x2=EsduDevi$LAT,y1=Chal.bas$LG*corr,
				                                    y2=Chal.bas$LAT)
				if (!is.null(dlimk)){
				  num.chal.bas[d.chal.bas>(dlimk/60)]=NA
				  Chal.bas=Chal.bas[Chal.bas$NOCHAL%in%num.chal.bas[!is.na(num.chal.bas)],]
				}
			}else num.chal.bas=NA	
		}else{
			Chal.haut=NULL
			num.chal.haut=NULL
			Chal.bas=NULL
			num.chal.bas=NULL
		}
		list(Chal.haut=Chal.haut,num.chal.haut=num.chal.haut,
			Chal.bas=Chal.bas,num.chal.bas=num.chal.bas)
	}

	y.plus.proche.x.i=function(i,x1,x2,y1,y2) {
		d=sqrt(outer(x1[i],y1,"-")**2+outer(x2[i],y2,"-")**2)
		nn=order(d)[1]
	nn
	}
	
	y.plus.proche.x.i.d=function(i,x1,x2,y1,y2) {
	  d=sqrt(outer(x1[i],y1,"-")**2+outer(x2[i],y2,"-")**2)
	  dd=d[order(d)][1]
	  dd
	}

	#***************************	
	#reference haul method
	#***************************	

	assoc.k=function(k,EsduDevi,list.nearchal.dev,Dsurf=NULL,Ndevi,forceFind=FALSE){
		cat(Ndevi[k],"\n")
		Di=Ndevi[k]
		if ((!is.null(list.nearchal.dev[[k]]$Chal.haut))|
			(!is.null(list.nearchal.dev[[k]]$Chal.bas))){
			#if (!is.null(Dsurf)){
        if (Di%in%Dsurf){
          if (!Di%in%names(EsduDevi)){
            vdev=NA
          }else{
            vdev=EsduDevi[,Di]
          }
				  assoc.k.res=data.frame(Esdu=EsduDevi$esdu.id,
                        	Dev=vdev,
					  NOCHAL=list.nearchal.dev[[k]]$num.chal.haut,
					  Region.CLAS=EsduDevi$zonesCLAS,
					  Region.SURF=EsduDevi$zonesSURF)
        }else{  
			#}
          if (!Di%in%names(EsduDevi)){
            vdev=NA
          }else{
            vdev=EsduDevi[,Di]
          }
          assoc.k.res=data.frame(Esdu=EsduDevi$esdu.id,
                         	Dev=vdev,
   					NOCHAL=list.nearchal.dev[[k]]$num.chal.bas,
   					Region.CLAS=EsduDevi$zonesCLAS,
   					Region.SURF=EsduDevi$zonesSURF)
        }
		  if (forceFind&sum(!is.na(assoc.k.res$NOCHAL))==0){
		    if (sum(!is.na(list.nearchal.dev[[k]]$num.chal.bas))>0){
		      nochals=list.nearchal.dev[[k]]$num.chal.bas
		      cat('Bottom hauls taken as sub-sample reference hauls for surface echotype ',Di,'\n')
		    }else if (sum(!is.na(list.nearchal.dev[[k]]$num.chal.haut))>0){
		      nochals=list.nearchal.dev[[k]]$num.chal.haut
		      cat('Surface hauls taken as sub-sample reference hauls for bottom echotype ',Di,'\n')
		    }
		    assoc.k.res=data.frame(Esdu=EsduDevi$esdu.id,
		                           Dev=vdev,
		                           NOCHAL=nochals,
		                           Region.CLAS=EsduDevi$zonesCLAS,
		                           Region.SURF=EsduDevi$zonesSURF)
		  }
		}else{
		  if (!Di%in%names(EsduDevi)){
		    vdev=NA
		  }else{
		    vdev=EsduDevi[,Di]
		  }
			assoc.k.res=data.frame(Esdu=EsduDevi$esdu.id,
                        	Dev=vdev,
					NOCHAL=NA,Region.CLAS=EsduDevi$zonesCLAS,
					Region.SURF=EsduDevi$zonesSURF)
		}
	assoc.k.res
	}

	assoc.k.gen=function(k,Ndevi,EsduDevi,inc.zones=TRUE) {
		cat('Dev',Ndevi[k],"\n")
	  Di=Ndevi[k]		  
		Dir=paste(Ndevi[k],'.CREF',sep='')
		if (inc.zones){
			assoc.res=data.frame(Esdu=EsduDevi$esdu.id,
                	        Dev=EsduDevi[,Di],NOCHAL=EsduDevi[,Dir],
					Region.CLAS=EsduDevi$zonesCLAS,
					Region.SURF=EsduDevi$zonesSURF)
		}else{
			assoc.res=data.frame(Esdu=EsduDevi$esdu.id,
                	        Dev=EsduDevi[,Di],NOCHAL=EsduDevi[,Dir])
		}
	assoc.res	
	}

	#***************************
	#Plots showing associations btw. deviations (Dev) in Esdu (Esdu) 
	#to reference haul (Nosta)
	#***************************
i=1
	show.esdu.refhaul=function(Ndevi,list.Associ,Pechelsi,Pechei,EsduDevi,
		dcol=c(2,4,1,3,7,8,6,5),lcol=NULL,legpos='topright',add.layer=NULL,
		export.plot=NULL,radSA=0.2,pradius=1,scale.SA=1,xlim=NULL,
		ylim=NULL,smax=1,corr=cos(46.5*pi/180),logit=FALSE,labelit=FALSE,
    nid='NOCHAL',cex.lab=1,ux11=TRUE,nfile=NA){

    lsps=names(Pechelsi)[!names(Pechelsi)%in%c('CAMPAGNE',
      'NOCHAL','NOSTA','LONF','LATF','STRATE','SONDE','GEAR')]
		lsps2=substr(lsps,4,11)

		if (is.null(lcol)){
			spcol=data.frame(sp=c("ENGR.ENC","MICR.POU","SARD.PIL","SCOM.SCO",
				"SPRA.SPR","TRAC.TRU","CAPR.APE","SCOM.JAP","TRAC.MED",
				"CLUP.HAR","COMP.LEM","MERL.MNG","MERL.MER"),
				sp2=c("ENGR-ENC","MICR-POU","SARD-PIL","SCOM-SCO",
				"SPRA-SPR","TRAC-TRU","CAPR-APE","SCOM-JAP","TRAC-MED",
				"CLUP-HAR","COMP-LEM","MERL-MNG","MERL-MER"),
				lcol=c(3,8,4,2,1,7,6,5,0,0,0,0,0))

			#select species colors
			#***************************
			#lsps2=substr(lsps,4,11)
			lcol=spcol[,'lcol'][match(lsps2,spcol$sp)]
		}
		
		for (i in 1:length(Ndevi)){
			Di=Ndevi[i]
			lai=list.Associ[[i]]
			lnochai=unique(lai$NOCHAL)
			Pechelsis=Pechelsi[Pechelsi[,nid]%in%lnochai,]
			alls=(lnochai%in%Pechelsi[,nid])
			if (FALSE%in%alls) {
				cat('Dev',i,'Haul',lnochai[!alls],
					'has no valid geographic position','\n')
			}
			esduch=merge(lai,unique(Pechei[,c(nid,'LONF','LATF')]),
				by.x='NOCHAL',by.y=nid)
			esduch=merge(esduch,unique(EsduDevi[,
				c('esdu.id','LONG','LAT',Di)]),by.x='Esdu',by.y='esdu.id')
			if (sum(!is.na(esduch$Dev))==0){
			   esduch$Dev=esduch[,Di]
			}
			esduch.nn=esduch[esduch$Dev!=0,]
			
			if (!is.null(export.plot)){
			  if(!is.na(nfile)){
			    nfilei=nfile
			  }else{
			    nfilei=Di
			  }
				png(file=paste(export.plot,'devCH-',nfilei,'.png',sep=''),
					width=800,height=800)
			}else if (ux11) x11()

      par(mar=c(2,2,3,1))
			xrb=c(EsduDevi$LONG,Pechelsis$LONF)
			yrb=c(EsduDevi$LAT,Pechelsis$LATF)
			if (is.null(xlim)){
			  xlim=c(min(xrb)-abs(max(xrb)-min(xrb))/10,
				  max(xrb)+abs(max(xrb)-min(xrb))/10)
			}
      Esdus=EsduDevi[EsduDevi$LONG>=xlim[1]&EsduDevi$LONG<=xlim[2],]
      Pecheis=Pechelsis[Pechelsis$LONF>=xlim[1]&Pechelsis$LONF<=xlim[2],]
      
			if (is.null(ylim)){
				ylim=c(min(yrb)-abs(max(yrb)-min(yrb))/10,
					max(yrb)+abs(max(yrb)-min(yrb))/10)
			}    
      Esdus=Esdus[Esdus$LAT>=ylim[1]&Esdus$LAT<=ylim[2],]
      Pecheis=Pecheis[Pecheis$LATF>=ylim[1]&Pecheis$LATF<=ylim[2],]
      #if (!is.null(add.map)) plot(add.map,add=FALSE,col=1)
      if (!Di%in%names(Esdus)){
        sa=rep(.1,dim(Esdus)[1])
      }else{
        sa=Esdus[,Di]
      }
      plot(Esdus$LONG,Esdus$LAT,
				cex=radSA+log(sa+1)/scale.SA,
				pch=1,col='grey50',main=Di,xlim=xlim,ylim=ylim)
			coast()
      
			if (dim(esduch.nn)[1]>0){
				segments(esduch.nn$LONG,esduch.nn$LAT,
					esduch.nn$LONF,esduch.nn$LATF,col=1)
				if (dim(Pecheis)[1]>0){
          if (logit){
            z=as.matrix(log(Pecheis[,lsps]+1))
          }else  {z=as.matrix(Pecheis[,lsps])}
					pie.xy(x=Pecheis$LONF,y=Pecheis$LATF,
						z=z,pcoast=FALSE,pcol=lcol,draw1=FALSE,
						pradius=pradius,smax=smax)
					legend(legpos,legend=substr(lsps,4,11),
						fill=lcol,bg='white')
          if (labelit) text(Pecheis$LONF,y=Pecheis$LATF,Pecheis[,nid],
            cex=cex.lab,col='grey50')
          if (!is.null(add.layer)) plot(add.layer,add=TRUE,col=2)
				}
			}else mtext('All esdus associated to hauls with no geographic position')
			if (!is.null(export.plot)){
        cat('Haul-esdu association plot saved to:',export.plot,'\n')
        dev.off()}		
		}
	}


