#-----------------
#Functions for acoustic biomass assessment
#-----------------
#COMPARE
###
#-------------------------------------------------------------------------
#Compare R and baracouda results
#-------------------------------------------------------------------------

	#Import of baracouda results
	#----------------------------------

	#res.bara=baracouda.import(data.path,year)
	#Biomres.EsduSp.bara=res.bara[[1]]
	#dim(Biomres.EsduSp.bara)
	#B.sp.bara=res.bara[[2]]
	#B.spr.bara=res.bara[[3]]
	#Xe.baras=res.bara[[4]]
	#post.baras=res.bara[[5]]


#rm(Biomres.EsduSp.bara)
	baracouda.import=function(data.path,year){

		Biomres.EsduSp.bara=read.table(path.Besu,sep=';',header=TRUE)
		head(Biomres.EsduSp.bara)
		names(Biomres.EsduSp.bara)=c('CAMPAGNE','ID_EI_ESDU','DATEHEURE',
			'ESDU','AFFECT','LAT','LON','DEV',
			'GENRE_ESP','CHAREF','CATEGORIE','BiomW','LM','PM','Abun','PrAcoust',
			'Energie')
		Biomres.EsduSp.bara$year=substr(Biomres.EsduSp.bara$CAMPAGNE,4,5)
		Biomres.EsduSp.baras=Biomres.EsduSp.bara[Biomres.EsduSp.bara$year==year,]

		Biomres.EsduSp.baras$DATEHEURE=as.character(Biomres.EsduSp.baras$DATEHEURE)
		dim(Biomres.EsduSp.baras)
		Biomres.EsduSp.baras[nchar(Biomres.EsduSp.baras$DATEHEURE)<18,
			'DATEHEURE']=paste('0',Biomres.EsduSp.baras[nchar(Biomres.EsduSp.baras$DATEHEURE)<18,'DATEHEURE'],
			sep='')
		Biomres.EsduSp.baras$DATEHEURE=paste(substr(Biomres.EsduSp.baras$DATEHEURE,1,3),
			'0',substr(Biomres.EsduSp.baras$DATEHEURE,4,18),sep='')
		dim(Biomres.EsduSp.baras)
		#Biomres.EsduSp.bara[1:10,]

		path.Bsp=paste(data.path,'Biom_Strate_Res.txt',sep='')
		Biomres.sp.bara=read.table(path.Bsp,sep=',',header=FALSE)
		names(Biomres.sp.bara)=c('CAMPAGNE','METH','GROUPE','sp','Biom','Ab',
			'VarBiom','CVBiom','VarAb','CVAb')
		Biomres.sp.bara$year=substr(Biomres.sp.bara$CAMPAGNE,4,5)
		B.sp.bara=Biomres.sp.bara[Biomres.sp.bara$year==year,]

		path.Bsp.region=paste(data.path,'Msys_Res_Biomasse_strate.txt',sep='')
		Biomres.sp.region.bara=read.table(path.Bsp.region,sep=',',header=FALSE)
		#Biomres.sp.region.bara[1:10,]
		names(Biomres.sp.region.bara)=c('CAMPAGNE','METH','CHmeth','Region','LRegion',
			'DLAY','GROUPE','sp','taille','Dev','Biom.region','Ab.region','vBiom.region',
			'CVBiom','vAb','CVAb','mXe.B','mXe.A','vXe.B','vXe.A','Area','NCH','NESU',
			'mSA','vSA','x1','x2','x3','x4')
		Biomres.sp.region.bara$year=substr(Biomres.sp.region.bara$CAMPAGNE,4,5)
		B.spr.bara=Biomres.sp.region.bara[Biomres.sp.region.bara$year==year,]

		path.Xe=paste(data.path,'Msys_espece_Xe.txt',sep='')
		Xe.bara=read.table(path.Xe,sep=',',header=FALSE)
		names(Xe.bara)=c('CAMPAGNE','ID_EI_ESDU','METH','NOSTA','DEV','GROUPE','sp','CATEG',
			'Ntot','PT','LM','PM','CAC','BAC','keN','keW','sigmaN','sigmaW','XeN','XeW',
			'x1','x2','x3','x4','date.calc')
		Xe.bara$year=substr(Xe.bara$CAMPAGNE,4,5)	
		Xe.baras=Xe.bara[Xe.bara$year==year,]

		path.post=paste(data.path,'POST_STRATE.txt',sep='')
		post.bara=read.table(path.post,sep=',',header=FALSE)
		names(post.bara)=c('STRATE','DLAY','LSTRATE','AREA','METH')
		post.baras=post.bara[substr(post.bara$STRATE,4,5)==year,]
		post.baras$ID=substr(post.baras$STRATE,7,7)
		post.baras$ID2=	post.baras$ID

	list(Biomres.EsduSp.baras,B.sp.bara,B.spr.bara,Xe.baras,post.baras)

	}

	#------------------------------------------------	
	#compare LIEN_ECHO_ESU_STATION	
	#------------------------------------------------

	Esdu.dev.haul.link.comp=function(data.path,EsduDev,chadoc,year){

		bara.esdu.CH=read.csv(paste(data.path,'LIEN_ECHO_ESU_STATION.txt',sep=''),
			sep=',',header=FALSE)
		names(bara.esdu.CH)=c('NOSTA','CRUISE','DATEHEURE','DEV','METH')
		bara.esdu.CHs=bara.esdu.CH[bara.esdu.CH$CRUISE==paste('EIT_',year,'_38',sep=''),]
		bara.esdu.CHs=correc.date(bara.esdu.CHs,ndate='DATEHEURE')
		#bara.esdu.CHs[1:10,]

		names(EsduDev)
		EsduDev.long=reshape(EsduDev[,c('TC',paste('D',Ndev,'.CREF',sep=''))],
			idvar="TC",
			varying=list(paste('D',Ndev,'.CREF',sep='')),timevar='NDEV',
			v.names="NOCHAL", direction="long",times=paste('D',Ndev,sep=''))
		unique(EsduDev.long$NDEV)
		names(EsduDev.long)
		#EsduDev.long[1:100,]

		cat(paste('Number of Esdus in R:',length(unique(EsduDev.long$TC))),"\n")
		cat(paste('Number of Esdus in baracouda:',length(unique(bara.esdu.CHs$DATEHEURE))),
			"\n")
	
		#summary esdu:dev/haul in R
		dim(EsduDev.long)
		EsduDev.long=merge(EsduDev.long,chadoc[,c('NOCHAL','NOSTA')],by.x='NOCHAL',
			by.y='NOCHAL')
		dim(EsduDev.long)

		setdiff(EsduDev.long$NOCHAL,chadoc$NOCHAL)
		table(is.element(EsduDev.long$NOCHAL,chadoc$NOCHAL))
		table(is.element(chadoc$NOCHAL,EsduDev.long$NOCHAL))
		chadoc[is.element(chadoc$NOCHAL,EsduDev.long$NOCHAL),'NOCHAL']

  (x <- c(sort(sample(1:20, 9)),NA))
     (y <- c(sort(sample(3:23, 7)),NA))
     union(x, y)
     intersect(x, y)
     setdiff(x, y)
     setdiff(y, x)
     setequal(x, y)


		names(bara.esdu.CHs)
		EsduDev.long$id=paste(EsduDev.long$TC,EsduDev.long$NDEV,EsduDev.long$NOSTA)
		bara.esdu.CHs$id=paste(bara.esdu.CHs$DATEHEURE,bara.esdu.CHs$DEV,
			bara.esdu.CHs$NOSTA)

		cat(paste('Number of unique deviations in R:',
			length(unique(EsduDev.long$NDEV))),"\n")
		cat(paste('Number of unique deviations in baracouda:',
			length(unique(bara.esdu.CHs$DEV))),"\n")

		cat(paste('Number of unique hauls in R:',
			length(unique(EsduDev.long$NOCHAL))),"\n")
		cat(paste('Number of unique hauls in baracouda:',
			length(unique(bara.esdu.CHs$NOSTA))),"\n")

		names(Peche)
		unique(Peche$NOSTA)

		length(unique(bara.esdu.CHs$id))
		length(unique(EsduDev.long$id))

		cat(paste('Esdu:dev:haul associations in bara and in R: (TRUE/FALSE)'),"\n")
		cat(table(bara.esdu.CHs$id%in%EsduDev.long$id),"\n")

	bara.esdu.CHsd=bara.esdu.CHs[!bara.esdu.CHs$id%in%EsduDev.long$id,]
	unique(bara.esdu.CHsd$DEV)
	unique(bara.esdu.CHsd$NOSTA)
	lesdu.barau=unique(bara.esdu.CHsd$DATEHEURE)

	#if not, which esdus?
	#
	plot(EsduDev[!EsduDev$TC%in%lesdu.barau,'LONG'],EsduDev[!EsduDev$TC%in%lesdu.barau,'LAT'])
	#
	coast()

	names(EsduDev.long)
	EsduDev.long[!bara.esdu.CHs$id%in%EsduDev.long$id,c('NOCHAL','NDEV')]

	EsduDev.long[EsduDev.long$TC%in%lesdu.barau,'NOSTA']

	#Hauls in LIEN_ECHO_ESU_STATION in Xe.db, in Xe.bara? 

	names(bara.esdu.CHs)
	unique(bara.esdu.CHs$NOSTA)%in%unique(Xe.db$NOSTA)
	unique(bara.esdu.CHs$NOSTA)[!(unique(bara.esdu.CHs$NOSTA)%in%unique(TRI$NOSTA))]
	unique(bara.esdu.CHs$NOSTA)%in%unique(CHAREF$NOSTA)
	#PEL00 -> one extra haul in bara: not in TRI... associated with 427 esdus...
	#surface haul, D4, AN/SA... same haul as haul E0333...

	#Esdu in EsduDev in LIEN_ECHO_ESU_STATION? 
	table(unique(bara.esdu.CHs$DATEHEURE)%in%unique(EsduDev$TC))
	table(unique(bara.esdu.CHs$DATEHEURE)%in%unique(ESDUDEV$TC))
	table(unique(bara.esdu.CHs$DATEHEURE)%in%unique(CHvalid$DATEHEURE))
	table(unique(bara.esdu.CHs$DATEHEURE)%in%unique(CHAREF$DATEHEURE))
	table(unique(bara.esdu.CHs$DATEHEURE)%in%unique(zones$DATEHEURE))
	
	#List of esdus in bara and not in R
	lesdus.out=bara.esdu.CHs$DATEHEURE[!(unique(bara.esdu.CHs$DATEHEURE)%in%unique(EsduDev$TC))]
	unique(bara.esdu.CHs[bara.esdu.CHs$DATEHEURE%in%lesdus.out,'NOSTA'])
	#plot(ESDUDEV[,'LONG'],ESDUDEV[,'LAT'])
	#points(ESDUDEV[ESDUDEV$TC%in%lesdus.out,'LONG'],
	#	ESDUDEV[ESDUDEV$TC%in%lesdus.out,'LAT'],pch=16,col=2)	
	#coast()

	}


	#----------------------------------------------------------------------
	#compare Xe's
	#R Xe in ton.m-2
	#baracouda Xe in g m-2
	#----------------------------------------------------------------------

	Xe.comparison=function(list.Xe,Ndev,Peche,Xe.baras){
		Xe.db=Xe.check(list.Xe,Ndev)
		dim(Xe.db)
		Xe.db=merge(Xe.db,unique(Peche[,c('NOSTA','NOCHAL')]),by.x='Nosta',
			by.y='NOCHAL')
		dim(Xe.db)
		names(Xe.db)
		
		Xe.db$csp=paste(substr(Xe.db$CodEsp,1,8),substr(Xe.db$CodEsp,15,15))
		Xe.db$id=paste(Xe.db$NOSTA,Xe.db$csp,Xe.db$NDEV)
		Xe.baras$id=paste(Xe.baras$NOSTA,Xe.baras$sp,Xe.baras$CATEG,Xe.baras$DEV)

		#haul:sp:dev:sclass in bara and not in R
		Xe.baram=Xe.baras[!Xe.baras$id%in%Xe.db$id,c('NOSTA','id','sp','CATEG','DEV')]
		dim(Xe.baras)
		dim(Xe.baram)
		names(Xe.baras)

		#Xe comp
		Xe.comp=merge(Xe.db,Xe.baras[,c('id','XeW','keW')],by.x='id',by.y='id')
		Xe.comp[,c('id','Xe','XeW','keW')]
		names(Xe.comp)
		Xe.comp$dke=Xe.comp$ke-Xe.comp$keW
		Xe.comp$rXes=Xe.comp$Xe/Xe.comp$XeW
		summary(Xe.comp$Xe)
		summary(Xe.comp$rXes)
		Xe.comp$dXes=Xe.comp$Xe-Xe.comp$XeW/1e6
		#hist(Xe.comp$dXes)

		cat('Summary of differences between species proportions (R-bara)',"\n")
		print(summary(Xe.comp$dke))
		print(Xe.comp[Xe.comp$dke>0.01,])

		cat('Summary of differences between Xe (R-bara)',"\n")
		print(summary(Xe.comp$dXes))
		dim(Xe.comp[Xe.comp$dXes>quantile(Xe.comp$dXes,0.95),])	
		cat('ESDUs with large differences in Xe',"\n")
		print(Xe.comp[Xe.comp$dXes>quantile(Xe.comp$dXes,0.95),])

		cat('hauls from baracouda not in R Xe.db','\n')
		uhaul=unique(Xe.baram$NOSTA)[!unique(Xe.baram$NOSTA)%in%unique(Xe.db$NOSTA)]
		print(Peche[Peche$NOSTA==as.character(uhaul),])
		cat('species:length from baracouda not in Xe.db','\n')
		unique(paste(Xe.baram$sp,Xe.baram$CATEG))[!unique(paste(Xe.baram$sp,
			Xe.baram$CATEG))%in%unique(Xe.db$csp)]
		cat('deviation from baracouda not in Xe.db','\n')
		print(unique(Xe.baram$DEV)[!unique(Xe.baram$DEV)%in%unique(Xe.db$NDEV)])

	Xe.comp
	}


	#Compare regions
	#-----------------------------------------------
	#R uses fishiview output : zoneXX.dbf and strateXX.dbf (zones et deszones)	
	#and 'SCENARIO' table from baracouda for haul selection (CHvalid)
	#same nb. of esdus in both files?

	regionsDef.comp=function(zones,CHvalid){
		length(unique(zones$DATEHEURE))
		length(unique(CHvalid$DATEHEURE))

		CHvalid$id=paste(CHvalid$DATEHEURE,CHvalid$DEV,CHvalid$Region)

		zones.idB=paste(rep(zones$DATEHEURE,each=4),paste('D',Ndev[-4],sep=''),
			rep(zones$zonesCLAS,each=4))
		zones.idS=paste(rep(zones$DATEHEURE,each=1),paste('D',Ndev[4],sep=''),
			rep(zones$zonesSURF,each=1))
		id.zones=c(zones.idB,zones.idS)

		cat('number of esdus:deviation combinations in Fishview','\n')
		print(length(unique(id.zones)))
		cat('number of esdus:deviation combinations in Baracouda','\n')
		print(length(unique(CHvalid$id)))

		cat('All baracouda zones in Fishview?','\n')
		print(table(CHvalid$id%in%id.zones))
	
		CHvalid.no=CHvalid[!(CHvalid$id%in%id.zones),]
		lesdu.no=unique(CHvalid.no$DATEHEURE)

		plot(zones$LONG,zones$LAT,main='Baracouda regions not in Fishview in red')
		points(zones[zones$DATEHEURE%in%lesdu.no,'LONG'],
			zones[zones$DATEHEURE%in%lesdu.no,'LAT'],pch=16,col=2)
		coast()

		cat('Number of esdus per zones in baracouda','\n')
		print(table(CHvalid$Region))
		length(lesdu.no)

		cat('Baracouda regions not in Fishview','\n')	
		unique(CHvalid[CHvalid$DATEHEURE%in%lesdu.no,c('Region')])
	
		#zones[zones$DATEHEURE%in%lesdu.no,c('zonesCLAS','zonesSURF')]
	}


	mSa.area.comp=function(B.spr.bara,Biom.region.db){

		B.spr.bara.area=aggregate(B.spr.bara[,'Area'],list(B.spr.bara$Region),
			unique)
		names(B.spr.bara.area)=c('region','area.bara')
		B.spr.bara.area$region=substr(B.spr.bara.area$region,7,7)
		
		B.R.area=aggregate(Biom.region.db[,'AREA'],list(Biom.region.db$Region),
			unique)
		names(B.R.area)=c('region','area.R')

		area.comp=merge(B.spr.bara.area,B.R.area)


		B.spr.bara.mDev=aggregate(B.spr.bara[,'mSA'],list(B.spr.bara$Region,B.spr.bara$Dev),
			mean)
		names(B.spr.bara.mDev)=c('Region','DEV','mSA.bara')
		B.spr.bara.mDev$Region=substr(B.spr.bara.mDev$Region,7,7)
		mSa.regions=aggregate(Biom.region.db[,'Dev'],
			list(Biom.region.db$Region,Biom.region.db$NDEV),
			mean)
		names(mSa.regions)=c('Region','DEV','mSA.R')

		mSa.regions.comp=merge(B.spr.bara.mDev,mSa.regions)

		#Same mean Sa's?
		mSa.regions.comp$dSa=mSa.regions.comp$mSA.bara-mSa.regions.comp$mSA.R
		summary(mSa.regions.comp$dSa)
		mean(mSa.regions.comp$dSa)

		#comp
		par(mfrow=c(1,2))
		plot(area.comp$area.bara,area.comp$area.R)
		plot(mSa.regions.comp$mSA.bara,mSa.regions.comp$mSA.R)
		
		list(area.comp,mSa.regions.comp)
	}



	#Compare biomass per region and sp
	#------------------------------------------------	
	biomass.comp=function(B.spr.bara,Biom.region.db){

		B.spr.bara$CodEsp=paste(B.spr.bara$sp,B.spr.bara$DLAY,B.spr.bara$taille,sep='-')
		B.spr.bara$Region=substr(B.spr.bara$Region,7,8)

		Biom.region.db$id=paste(Biom.region.db$CodEsp,Biom.region.db$Region,
			Biom.region.db$NDEV)
		B.spr.bara$id=paste(B.spr.bara$CodEsp,B.spr.bara$Region,
			B.spr.bara$Dev)

		cat('unique species:size id, same number of unique id in R outputs?','\n')
		print(length(Biom.region.db$id));print(length(unique(Biom.region.db$id)))
		cat('unique species:size id, same number of unique id in baracouda outputs?','\n')	
		print(length(B.spr.bara$id));print(length(unique(B.spr.bara$id)))
	
		#B.spr.bara[!B.spr.bara$id%in%Biom.region.db$id,]
		cat('unique species:size id in baracouda and not in R outputs','\n')
		print(B.spr.bara[!B.spr.bara$id%in%Biom.region.db$id&
			B.spr.bara$CodEsp!='COMP-LEM-CLAS-0',])

		cat('Total biomass of species:size in baracouda and not in R outputs','\n')		
		print(sum(B.spr.bara[!B.spr.bara$id%in%Biom.region.db$id&
			B.spr.bara$CodEsp!='COMP-LEM-CLAS-0','Biom.region']))

		#merger R and bara results
		names(B.spr.bara)
		dim(B.spr.bara)
		B.region.comp=merge(Biom.region.db,B.spr.bara[,c('id',
			'DLAY','Biom.region','mXe.B','Area','mSA')],by.x='id',by.y='id')
		dim(B.region.comp)
		dim(Biom.region.db)
		names(B.region.comp)

		#R and bara biomass comparison
		B.region.comp$dwBs=B.region.comp$Biom.region-B.region.comp$wbioms
		summary(B.region.comp$dwBs)
		hist(B.region.comp$dwBs)
		cat('Mean biomass differences','\n')
		print(aggregate(B.region.comp$dwBs,list(B.region.comp[,'GENR_ESP']),mean))
		print(aggregate(B.region.comp$dwBs,list(B.region.comp[,'Region']),mean))
		print(aggregate(B.region.comp$dwBs,list(B.region.comp$Region,
			B.region.comp[,c('GENR_ESP')]),mean))

		B.region.comp$dB=B.region.comp$Biom.region-B.region.comp$biom
		summary(B.region.comp$dB)
		hist(B.region.comp$dB)
		aggregate(B.region.comp$dB,list(B.region.comp[,'GENR_ESP']),mean)
		aggregate(B.region.comp$dB,list(B.region.comp[,'Region']),mean)
	
		#plot(B.region.comp$Biom.region,B.region.comp$bioms)
		plot(B.region.comp$Biom.region,B.region.comp$wbioms,xlab='Baracouda biomass',
			ylab='R biomass')
	
		#compare biomass per sp
		#-------------------
		B.sp.comp=merge(Biom.sp,B.sp.bara,by.x='sp',by.y='sp')
		B.sp.comp$Bdiff.exp=B.sp.comp$wbioms-B.sp.comp$Biom

	list(B.region.comp,B.sp.comp)
	}

	#Compare biomass per echotype, esdu and sp
	#------------------------------------------------	

	plot.Bdiff.sp=function(Bdev.db,Biomres.EsduSp.bara,Ndev,nsp='ENGR.ENC',scaleit=10){	
	
		names(Bdev.db)
		lsp=substr(names(Bdev.db),1,8)
		biom.res.R.AN=Bdev.db[,c(TRUE,lsp[2:16]%in%nsp,rep(TRUE,4))]
		biom.res.R.AN$ENGR.ENC.R=apply(biom.res.R.AN[,2:3],1,sum)
		names(biom.res.R.AN)

		biom.comp.AN=merge(biom.res.R.AN[,c('TC','DEV','ENGR.ENC.R')],
			Biomres.EsduSp.bara[Biomres.EsduSp.bara[,
			'GENRE_ESP']=='ENGR-ENC',],by.x=c('TC','DEV'),
			by.y=c('DATEHEURE','DEV'))
		names(biom.comp.AN)
		dim(biom.comp.AN)	
		dim(biom.res.R.AN)

		names(biom.comp.AN)
		biom.comp.AN$B.BmR=biom.comp.AN$Biom-biom.comp.AN[,'ENGR.ENC.R']
		summary(biom.comp.AN$B.BmR)
		biom.comp.AN$B.BoR=biom.comp.AN$Biom/biom.comp.AN[,'ENGR.ENC.R']
		sumrB.R.AN=tapply(biom.comp.AN$B.BoR,biom.comp.AN$DEV,summary)
		summB.R.AN=tapply(biom.comp.AN$B.BmR,biom.comp.AN$DEV,summary)
		mrB.R.AN=tapply(biom.comp.AN$B.BoR,biom.comp.AN$DEV,mean)
		mmB.R.AN=tapply(biom.comp.AN$B.BmR,biom.comp.AN$DEV,mean)
			
		Ndevs=unique(biom.comp.AN$DEV)

		for (i in 1:length(Ndevs)){
			devi=biom.comp.AN[biom.comp.AN$DEV==Ndevs[i],]
			devi$pcol=1
			devi[devi$B.BmR<0,'pcol']=2
			devi[devi$B.BmR>0,'pcol']=3
			if (i>1){x11()}	
			plot(devi$LON,devi$LAT,cex=0.1+abs(devi$B.BmR)/scaleit,
				xlab='N',ylab='W',
				main=paste(Ndevs[i],',',nsp,',',
				'Baracouda-R biomass differences per ESDU (t), 
				mean differenceB-R =',
				round(mmB.R.AN[i],2)),pch=21,col=devi$pcol)
			coast()
			legend('bottomleft',legend=c('Baracouda biomass greater than R biomass',
				'Baracouda biomass equal to R biomass',
				'Baracouda biomass lesser than R biomass'),pch=21,col=c(3,1,2))
		}

		biom.comp.AN
	}

	Bdev.sp.comp=function(k,Biomres.EsduSp.bara,biom.esdu.dev.sp,Ndev){

		devi=paste('D',Ndev[k],sep='')

		#Extract biomass per deviation and species from R outputs	
		
		biom.D1=data.frame(biom.esdu.dev.sp[[k]])
		biom.D1$TRAC.TRU.CLAS.0=biom.D1$TRAC.TRU.CLAS.P+biom.D1$TRAC.TRU.CLAS.G
		dimnames(biom.D1)
		Btot.D1=apply(biom.D1,2,sum)
		Btot.D1s=Btot.D1[Btot.D1>0][-1]
		Btot.D1l=data.frame(CodEsp=names(Btot.D1s),Btot=Btot.D1s)
		Btot.D1l$CodEsp=gsub('\\.', '-',Btot.D1l$CodEsp)
		Btot.D1l[,'GENR_ESP']=substr(Btot.D1l$CodEsp,1,8)
		#Btot.D1l=merge(Btot.D1l,unique(EspDev[,c('CodEsp','GENR_ESP')]))

		#Extract biomass per deviation and species from Baracouda outputs
		biom.D1b=Biomres.EsduSp.bara[Biomres.EsduSp.bara$DEV==devi,]
		names(biom.D1b)
		unique(biom.D1b$GENRE_ESP)
		Btot.D1b=aggregate(biom.D1b$BiomW,list(biom.D1b$GENRE_ESP),sum)
		names(Btot.D1b)=c('GENR_ESP','Btotb')	

		Btot.D1.comp=merge(Btot.D1l,Btot.D1b)
		Btot.D1.comp$BRdiffB=Btot.D1.comp$Btot-Btot.D1.comp$Btotb

		Btot.D1.comp

	}

#-------------------------------------------------------------------------
#Compare R and baracouda results
#-------------------------------------------------------------------------

  baracoudaR=function(Biom.sp){

  #Biomass per species: comparison of R and baracouda results
	#------------------------------------------------
	B.sp.comp=merge(Biom.sp,B.sp.bara,by.x='sp',by.y='GROUPE')
	B.sp.comp$mbiom=apply(B.sp.comp[,2:5],1,mean)
	B.sp.comp$sdbiom=apply(B.sp.comp[,2:5],1,sd)
	names(B.sp.comp)

	mcomp=as.matrix(B.sp.comp[,c(2:5,9,16)])
	dimnames(mcomp)[[1]]=B.sp.comp$sp


	#Barplot of biomass estimates per species and method with mean estimate of 4 methods 
	# (mbiom) and +/- 2*sd confidence intervals
	# red bar: baracouda biomass estimate with +/- 2*sd confidence intervals derived 
	# from haul allocation 

	png(file=paste(path.res.charef,'2D-2R-Bsp-comp.png',sep=''),width=800,
		height=800)

	par(mar=c(4,6,3,1))
	bpo=barplot(t(mcomp),beside=TRUE,horiz=TRUE,las=2,legend.text=TRUE,
		main='PEL00, 5 deviations, 9 regions, ref hauls',
		col=rev(c('white','red','grey90','green','grey50','grey30')))

	arrows(mcomp[,6]+2*B.sp.comp$sdbiom,bpo[6,],mcomp[,6]-2*B.sp.comp$sdbiom,bpo[6,],
		code=3,angle=90,length=0.05)
	arrows(mcomp[,5]+2*sqrt(B.sp.comp$VarBiom),bpo[5,],mcomp[,5]-2*sqrt(B.sp.comp$VarBiom),
		bpo[5,],code=3,angle=90,length=0.05,col=1)

	dev.off()

	#------------------------------------------------	
	#compare LIEN_ECHO_ESU_STATION	
	#------------------------------------------------

	Esdu.dev.haul.link.comp(data.path,EsduDev,chadoc)

	#*******************************************************
	#PEL00: Same Esdu/dev/haul associations in baracouda and R...
	#*******************************************************
	#PEL00 -> 99 Esdus in bara and not in R... Pb in zones: esdus w/o CLAS regions... 

	#----------------------------------------------------------------------
	#compare Xe's
	#R Xe in ton.m-2
	#baracouda Xe in g m-2
	#----------------------------------------------------------------------
	
	Xecomp.res=Xe.comparison(list.Xe,Ndev,Peche,Xe.baras)

	names(Xecomp.res)
	summary(Xecomp.res$Xe)
	summary(Xecomp.res$dke)		
	summary(Xecomp.res$dXes)
		

	#-> very few differences btw Xe's per haul:species:size

	#Compare regions definitions
	#-----------------------------------------------
	#R uses fishiview output : zoneXX.dbf and strateXX.dbf (zones et deszones)	
	#and 'SCENARIO' table from baracouda for haul selection (CHvalid)
	#same nb. of esdus in both files?

	regionsDef.comp(zones,CHvalid)

	#Area and mean deviations differences? 
	#-------------------------------------
	mSa.area.compres=mSa.area.comp(B.spr.bara,Biom.region.db)

	#*******************************
	#PEL00: same areas, mean Sa per regions
	#*******************************

	#Compare biomass per region and sp
	#------------------------------------------------	
	biomcomp.res=biomass.comp(B.spr.bara,Biom.region.db)

	#----------------------------------------------------------------------------------------
	#Biomass per Esdu, deviation and species comparisons
	#----------------------------------------------------------------------------------------	
	#import Baracouda
	names(Biomres.EsduSp.bara)
	Biomres.EsduSp.bara$spL=paste(Biomres.EsduSp.bara[,'GENRE_ESP'],
		Biomres.EsduSp.bara$CATEGORIE,sep='-')

	biom.dev.sp.comp=lapply(X=seq(length(Ndev)),FUN=Bdev.sp.comp,Biomres.EsduSp.bara,
		biom.esdu.dev.sp,Ndev)

	for (k in seq(length(biom.dev.sp.comp))){
		compi=biom.dev.sp.comp[[k]]
		compis=compi[!compi$CodEsp%in%c('TRAC-TRU-CLAS-P','TRAC-TRU-CLAS-G'),]
		compis$DEV=paste('D',Ndev[k],sep='')
		if (k==1){		
			compis.db=compis
		}else{
			compis.db=rbind(compis.db,compis)
		}				
	}				

	compis.db

	summary(compis.db$BRdiffB)

	compis.db[compis.db$BRdiffB>quantile(compis.db$BRdiffB,0.8),]
	compis.db[compis.db$BRdiffB>=10,]

	#Maps of biomass differences between R and baracouda per esdu
	#--------------------------------------------------------------
	
	biom.res.R.SA=Bdev.db[,c(1,6,7,17:20)]
	biom.res.R.SA$SARD.PIL.R=apply(biom.res.R.SA[,2:3],1,sum)
	names(biom.res.R.AN)

	#fails
	#biom.res.R.long=reshape(biom.res.R[1:10,],idvar=c("ESDU","LAT","LONG"),
	#	varying=list(4:14),timevar="spL", direction="long")

	#merge bara and R output files
	
	#Anchovy
	#--------------------

	biom.comp.AN=plot.Bdiff.sp(Bdev.db,Biomres.EsduSp.bara,Ndev,nsp='ENGR.ENC')


	#Sardine
	#--------------------

	biom.comp.SA=plot.Bdiff.sp(Bdev.db,Biomres.EsduSp.bara,Ndev,nsp='SARD.PIL',scaleit=20)


	Biomres.EsduSp.bara[1:10,]
  }

  #----------------------------------------------------------------------------
	#Barplots for biomass/Xe per codEsp/region comparison
	#----------------------------------------------------------------------------

	compare.methods.regions=function(df1,df2=NULL,Ndev,teval=2,nvar='wbioms',
		DEV=FALSE,df.names=c('df1','df2')){

		if (DEV==TRUE){
			for (i in Ndev){ 
		
				list.Xe.regions.neari=df1[[i]][[teval]]
				list.Xe.regions.neari=list.Xe.regions.neari[,c('Region','CodEsp',
					nvar)]
				list.Xe.regions.near.wide=reshape(list.Xe.regions.neari,
					idvar=names(list.Xe.regions.neari)[1],timevar='CodEsp',
					direction="wide")
				if (!is.null(df2)){
					list.Xe.regions.charefi=df2[[i]][[teval]]
					list.Xe.regions.charefi=list.Xe.regions.charefi[,
						c('Region','CodEsp',nvar)]
					list.Xe.regions.charef.wide=reshape(list.Xe.regions.charefi,
						idvar=names(list.Xe.regions.charefi)[1],timevar='CodEsp',
						direction="wide")
				}
				x11()
				if (!is.null(df2)){par(mar=c(4,9,2,3),mfrow=c(2,1))
				}else{par(mar=c(4,9,2,3))}
				barplot(as.matrix(list.Xe.regions.near.wide[,-1]),
					horiz = TRUE,las=2,
					beside = TRUE,
					legend.text = list.Xe.regions.near.wide[,1],
					names.arg = substr(names(list.Xe.regions.near.wide[,-1]),
						7,22),
					main=paste(nvar,', D',i,', ',df.names[1],
					sep=''))
				if (!is.null(df2)){
				barplot(as.matrix(list.Xe.regions.charef.wide[,-1]),horiz = TRUE,las=2,
					beside = TRUE,legend.text = list.Xe.regions.charef.wide[,1],
					names.arg = substr(names(list.Xe.regions.charef.wide[,-1]),
						7,22),
					main=paste(nvar,', D',i,', ',df.names[2],
					sep=''))
				}
			}
		}else{

		list.Xe.regions.neari=df1[,c('Region','sp',nvar)]
		list.Xe.regions.near.wide=reshape(list.Xe.regions.neari,
			idvar=names(list.Xe.regions.neari)[1],timevar='sp',
			direction="wide")
		x11()
		par(mar=c(4,9,2,3))
		barplot(as.matrix(list.Xe.regions.near.wide[,-1]),horiz = TRUE,las=2,
			beside = TRUE,legend.text = list.Xe.regions.near.wide[,1],
			names.arg = substr(names(list.Xe.regions.near.wide[,-1]),
			8,22),main=paste(nvar,', ',df.names[1],sep=''))

		if (!is.null(df2)){
			list.Xe.regions.charefi=df2[,c('Region','sp',nvar)]
			list.Xe.regions.charef.wide=reshape(list.Xe.regions.charefi,
				idvar=names(list.Xe.regions.charefi)[1],timevar='sp',
				direction="wide")
		x11()
		par(mar=c(4,9,2,3))
		barplot(as.matrix(list.Xe.regions.charef.wide[,-1]),horiz = TRUE,las=2,
			beside = TRUE,legend.text = list.Xe.regions.charef.wide[,1],
			names.arg = substr(names(list.Xe.regions.charef.wide[,-1]),
			8,22),main=paste(nvar,', ',df.names[2],sep=''))
		}
		}
	}



