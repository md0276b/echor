EchoR is maintained by mathieu.doray[at]ifremer.fr and has benefited (so far) from codes from:

* pierre.petitgas[at]ifremer.fr
* claire.saraux[at]ifremer.fr
* anne.sophie.cornou[at]ifremer.fr

Check your results carefully, and report any bug/problems. Comments or complaints can be directed at/to mathieu.doray[at]ifremer.fr 