install.packages(c("gridBase","PBSmapping","splancs","sp","Rmixmod",
                   "fields","maps","mapdata","ncdf","ncdf4","marmap",
                   "maptools","RPostgreSQL","alphahull","plyr","foreign",
                   "abind","rgdal","R.utils")) 

install.packages(c('FactoMineR','rasterVis','latticeExtra','vegan','viridisLite',
                   'ggplot2','scales','corrplot','factoextra','grid','gridExtra',
                   'stargazer')) 
