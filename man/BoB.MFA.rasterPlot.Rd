\name{BoB.MFA.rasterPlot}
\alias{BoB.MFA.rasterPlot}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Function to plot maps of Multiple Factor Analysis coordinates in the Bay of Biscay
}
\description{
Function to plot maps of Multiple Factor Analysis coordinates in the Bay of Biscay. Plot types: mean maps, inertia maps.
}
\usage{
BoB.MFA.rasterPlot(MFAres, nMFA = 3, cellsel, pipo, ptitle = "", path.export = NULL, plotit = list(MFA123coord = TRUE, MFA123timevar = TRUE, MFA123contrib = TRUE, MFA123cos2 = TRUE, MFA123coordMcos2 = TRUE, MFA123coordMcontrib = TRUE, MFA123pind = FALSE), funSel = "mean", anomalit = FALSE, sanomalit = FALSE, ux11 = FALSE, lptheme = viridisTheme, margin = FALSE, Ndim = list(dim(MFAres$ind$coord)[2]), thrMFAs = NULL, crs = CRS("+proj=longlat +datum=WGS84"), ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{MFAres}{
An object of class "MFA" "list" returned by the FactoMineR's 'MFA' function.
}
  \item{nMFA}{
Numeric: number of MFA components to plot.
}
  \item{cellsel}{
A dataframe with the map cells selection.
}
  \item{pipo}{
A dataframe with the complete map cell grid.
}
  \item{ptitle}{
Character. An optional title for the plots.
}
  \item{path.export}{
Path for exporting plots.
}
  \item{plotit}{
  A booelan list with the types of plots to display: 
  \itemize{
    \item MFA123coord = maps of individuals mean coordinates on 1:nMFA MFA planes;
    \item MFA123timevar = maps of individuals inertia on MFA planes specified by Ndim argument;
    \item MFA123contrib = maps of individuals contributions on 1:nMFA MFA planes;
    \item MFA123cos2 = maps of individuals quality of representation on 1:nMFA MFA planes;
    \item MFA123coordMcos2 = maps of individuals mean coordinates of cells whose quality of representation on 1:nMFA MFA planes is higher than the results returned by the application of funSel to MFA123cos2 = MFA coordinates of mean individuals filtered by their quality of representation;
    \item MFA123coordMcontrib = maps of individuals mean coordinates of cells whose contribution on 1:nMFA MFA planes is higher than the results returned by the application of funSel to MFA123contrib = MFA coordinates  of mean individuals filtered by their contribution;
    \item MFA123pind = maps of partial individuals coordinates 1:nMFA MFA planes.
  }
}
  \item{funSel}{
An R function applied to MFA123cos2 or MFA123contrib to define the mask applied to mean individuals MFA coordinates maps (cf. plotit argument).
}
  \item{anomalit}{
Boolean. Return or not return partial individuals anomaly maps? (partial individual MFA coordinate minus mean individual MFA coordinate).
}
  \item{sanomalit}{
Boolean. Standardize partial individuals anomaly maps by eigenvalues? 
}
  \item{ux11}{
Boolean. Display plots in x11 device?
}
  \item{lptheme}{
%%     ~~Describe \code{lptheme} here~~
}
  \item{margin}{
Boolean. Add marginal statistics (median) to plot?
}
  \item{Ndim}{
A list of numeric vectors giving the indices of MFA components for which to compute the inertia maps. 
}
  \item{thrMFAs}{
A list of values to standardise partial individuals anomalies maps.
}
  \item{crs}{
sp projection definition for plots. By default: longitude correction by cos(mean(latitude)) in WGS84 datum.
}
  \item{\dots}{
Extra arguments to be passed to the rasterVis levelplot function.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
A list with the raster stacks objects corresponding to the plot specified in "plotit" argument:
\item{MFAcoord.rasterStack}{}
\item{MFAcontrib.rasterStack}{}
\item{MFAcos2.rasterStack}{}
\item{MFAcoordMcos2.rasterStack}{}
\item{MFAcoordMcontrib.rasterStack}{}
\item{MFApcoord.rasterStack.list}{}
\item{MFAinertia}{}
\item{MFAinertia.raster}{}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
mathieu.doray@ifremer.fr
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
