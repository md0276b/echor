\name{N.esdu.sp.age.long.summary}
\alias{N.esdu.sp.age.long.summary}
\title{
Fish number and biomass at age per Elementary Sampling Unit
}
\description{
A function to produce a dataframe with abundances in no. of fish and biomass per species, age class and ESDUs.
}
\usage{
N.esdu.sp.age.long.summary(list.N.esdu.ANSA.age, wmLA = NULL, LW = NULL)
}
\arguments{
  \item{list.N.esdu.ANSA.age}{
A list of no. of fish per species, age class and ESDUs produced by the "age.ventilator.sp" function.
}
  \item{wmLA}{
Dataframe with the weighted mean lengths at age per species, generally derived from the age.at.length.import function
}
  \item{LW}{
Dataframe with length-weight equations
}
}
\details{
Fish biomass at age per Elementary Sampling Unit and species are computed by multiplying the fish number at age by their mean weight at age, computed by inputing the weighted mean length in age class in the species length-weight equation provided in LW.  
}
\value{
A dataframe with the following columns: 
\item{sp}{species names}
\item{Age}{age class}
\item{Esdu}{ESDU id}
\item{Ntot}{total number of fish in initial length classes}
\item{N}{total number of fish in age classes}
\item{wmL}{mean length weighted by age proportions-at-lengths in age class (cm)}
\item{a}{a in length-weight equation W=a*L^b}
\item{b}{b in length-weight equation W=a*L^b}
\item{wmW}{mean weight from  length-weight equation (g)}
\item{BA}{biomass-at-age in the esdu (tons)} 
}
\references{
Doray Mathieu, Massé Jacques, Petitgas Pierre (2010). Pelagic fish stock assessment by acoustic methods at Ifremer. http://archimer.ifremer.fr/doc/00003/11446/
}
\author{
mathieu.doray@ifremer.fr
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
